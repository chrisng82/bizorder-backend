<?php

use Faker\Generator as Faker;

$factory->define(App\Item::class, function (Faker $faker) {
    return [
        // 'id'=>$faker->text(),
        'code'=>$faker->company().$faker->randomLetter().$faker->boolean(),
        'ref_code_01'=>$faker->randomLetter(),
        'desc_01'=>$faker->sentence(),
        'desc_02'=>$faker->sentence(),
        'item_group_01_id'=>$faker->randomNumber(),
        'item_group_02_id'=>$faker->randomNumber(),
        'item_group_03_id'=>$faker->randomNumber(),
        'item_group_04_id'=>$faker->randomNumber(),
        'item_group_05_id'=>$faker->randomNumber(),
        'qty_scale'=>$faker->randomNumber(),
        'item_type'=>$faker->randomNumber(),
        'scan_mode'=>$faker->randomNumber(),
        'retrieval_method'=>$faker->randomNumber(),
        'unit_uom_id'=>$faker->randomNumber(),
        'case_uom_rate'=>$faker->randomNumber(),
        'pallet_uom_rate'=>$faker->randomNumber(),
        'case_ext_length'=>$faker->randomNumber(),
        'case_ext_width'=>$faker->randomNumber(),
        'case_ext_height'=>$faker->randomNumber(),
        'case_gross_weight'=>$faker->randomNumber(),
        'is_length_allowed_vertical'=>$faker->boolean(),
        'is_width_allowed_vertical'=>$faker->boolean(),
        'is_height_allowed_vertical'=>$faker->boolean(),
    ];
});
