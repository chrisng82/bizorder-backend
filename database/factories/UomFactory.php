<?php

use Faker\Generator as Faker;

$factory->define(App\Uom::class, function (Faker $faker) {
    return [
        'code'=>$faker->postcode(),
        'ref_code_01'=>$faker->sentence(),
        'desc_01'=>$faker->sentence(),
        'desc_02'=>$faker->sentence(),
    ];
});
