<?php

use Faker\Generator as Faker;

$factory->define(App\OutbOrdHdr::class, function (Faker $faker) {
    return [
        //need relation
        'site_flow_id'=>$faker->randomNumber(),
        'company_id'=>$faker->randomNumber(),
        'division_id'=>$faker->randomNumber(),
        'delivery_point_id'=>$faker->randomNumber(),
        'salesman_id'=>$faker->randomNumber(),
        'currency_id'=>$faker->randomNumber(),
        'tax_hdr_id'=>$faker->randomNumber(),

        'doc_code' => $faker->unique()->word,
        'ref_code_01' => $faker->sentence(),
        'ref_code_02' => $faker->sentence(),
        'doc_date' => date('Y-m-d'),
        'desc_01' => $faker->sentence(),
        'desc_02' => $faker->sentence(),
        'currency_rate'=>$faker->randomNumber(),
        'gross_amt'=>$faker->randomNumber(),
        'gross_local_amt'=>$faker->randomNumber(),
        'disc_val_01'=>$faker->randomNumber(),
        'disc_perc_01'=>$faker->randomNumber(),
        'disc_val_02'=>$faker->randomNumber(),
        'disc_perc_02'=>$faker->randomNumber(),
        'disc_val_03'=>$faker->randomNumber(),
        'disc_perc_03'=>$faker->randomNumber(),
        'disc_val_04'=>$faker->randomNumber(),
        'disc_perc_04'=>$faker->randomNumber(),
        'disc_val_05'=>$faker->randomNumber(),
        'disc_perc_05'=>$faker->randomNumber(),
        'disc_amt'=>$faker->randomNumber(),
        'disc_local_amt'=>$faker->randomNumber(),
        'tax_val_01'=>$faker->randomNumber(),
        'tax_perc_01'=>$faker->randomNumber(),
        'tax_val_02'=>$faker->randomNumber(),
        'tax_perc_02'=>$faker->randomNumber(),
        'tax_val_03'=>$faker->randomNumber(),
        'tax_perc_03'=>$faker->randomNumber(),
        'tax_val_04'=>$faker->randomNumber(),
        'tax_perc_04'=>$faker->randomNumber(),
        'tax_val_05'=>$faker->randomNumber(),
        'tax_perc_05'=>$faker->randomNumber(),
        'tax_amt'=>$faker->randomNumber(),
        'tax_local_amt'=>$faker->randomNumber(),
        'net_amt'=>$faker->randomNumber(),
        'net_local_amt'=>$faker->randomNumber(),
        'doc_status'=>$faker->randomNumber(),
    ];
});
