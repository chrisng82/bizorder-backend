<?php

use Faker\Generator as Faker;

$factory->define(App\OutbOrdDtl::class, function (Faker $faker) {
    return [
        // 'hdr_id'=>$faker->text(),
        'line_no'=>$faker->randomNumber(),
        'promo_hdr_id'=>$faker->randomNumber(),

        'desc_01'=>$faker->sentence(),
        'desc_02'=>$faker->sentence(),
        'uom_rate'=>$faker->randomFloat(),
        'sale_price'=>$faker->randomFloat(),
        'qty'=>$faker->randomNumber(),
        'gross_amt'=>$faker->randomFloat(),
        'gross_local_amt'=>$faker->randomFloat(),
        
        'disc_val_01'=>$faker->randomFloat(),
        'disc_perc_01'=>$faker->randomFloat(),
        'disc_val_02'=>$faker->randomFloat(),
        'disc_perc_02'=>$faker->randomFloat(),
        'disc_val_03'=>$faker->randomFloat(),
        'disc_perc_03'=>$faker->randomFloat(),
        'disc_val_04'=>$faker->randomFloat(),
        'disc_perc_04'=>$faker->randomFloat(),
        'disc_val_05'=>$faker->randomFloat(),
        'disc_perc_05'=>$faker->randomFloat(),
        'disc_amt'=>$faker->randomFloat(),
        'disc_local_amt'=>$faker->randomFloat(),
        'tax_val_01'=>$faker->randomFloat(),
        'tax_perc_01'=>$faker->randomFloat(),
        'tax_val_02'=>$faker->randomFloat(),
        'tax_perc_02'=>$faker->randomFloat(),
        'tax_val_03'=>$faker->randomFloat(),
        'tax_perc_03'=>$faker->randomFloat(),
        'tax_val_04'=>$faker->randomFloat(),
        'tax_perc_04'=>$faker->randomFloat(),
        'tax_val_05'=>$faker->randomFloat(),
        'tax_perc_05'=>$faker->randomFloat(),
        'tax_amt'=>$faker->randomFloat(),
        'tax_local_amt'=>$faker->randomFloat(),
        'net_amt'=>$faker->randomFloat(),
        'net_local_amt'=>$faker->randomFloat(),
    ];
});
