<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocTxnVoidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_txn_voids', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->smallInteger('proc_type')->unsigned();

            $table->string('fr_doc_hdr_type');
            $table->unsignedBigInteger('fr_doc_hdr_id');
            $table->string('fr_doc_hdr_code');
            $table->string('to_doc_hdr_type');
            $table->unsignedBigInteger('to_doc_hdr_id');
            $table->boolean('is_closed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_txn_voids');
    }
}
