<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotion_variants', function (Blueprint $table) {
            //
            $table->bigIncrements('id');

            $table->unsignedBigInteger('promotion_id');
            
            $table->string('variant_type', 30);
            $table->unsignedBigInteger('item_id')->nullable();
            $table->unsignedBigInteger('item_group_01_id')->nullable();
            $table->unsignedBigInteger('item_group_02_id')->nullable();
            $table->unsignedBigInteger('item_group_03_id')->nullable();

            $table->unsignedDecimal('disc_perc_01', 9, 5)->default(0);
            $table->unsignedDecimal('disc_perc_02', 9, 5)->default(0);
            $table->unsignedDecimal('disc_perc_03', 9, 5)->default(0);
            $table->unsignedDecimal('disc_perc_04', 9, 5)->default(0);
            $table->unsignedDecimal('disc_fixed_price', 25, 8)->default(0);
            $table->unsignedInteger('stock')->nullable();
            $table->unsignedInteger('sold')->default(0);
            $table->unsignedDecimal('min_qty', 14, 6)->nullable();
            $table->unsignedDecimal('max_qty', 14, 6)->nullable();
            $table->boolean('enabled')->default(1);

            $table->string('promotion_type', 50);
            $table->boolean('is_main');
            $table->boolean('is_add_on');

            $table->json('rest')->nullable();

            $table->timestamps();

            $table->index('promotion_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotion_variants');
    }
}
