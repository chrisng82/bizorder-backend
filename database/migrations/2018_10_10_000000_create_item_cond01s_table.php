<?php

use App\ItemCond01;
use App\Services\Env\ItemCondStatus;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemCond01sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_cond01s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 35)->unique();
            $table->string('desc_01');
            $table->string('desc_02');

            $table->smallInteger('item_cond_status')->unsigned();

            $table->timestamps();
        });

        $itemCond01 = new ItemCond01();
        $itemCond01->code = 'GOOD';
        $itemCond01->desc_01 = 'GOOD';
        $itemCond01->desc_02 = '';
        $itemCond01->item_cond_status = ItemCondStatus::$MAP['GOOD'];
        $itemCond01->save();

        $itemCond01 = new ItemCond01();
        $itemCond01->code = 'BAD';
        $itemCond01->desc_01 = 'BAD';
        $itemCond01->desc_02 = '';
        $itemCond01->item_cond_status = ItemCondStatus::$MAP['BAD'];
        $itemCond01->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_cond01s');
    }
}
