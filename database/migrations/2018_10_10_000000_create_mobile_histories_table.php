<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobileHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobile_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('mobile_profile_id');
            $table->string('table_name');
            $table->unsignedBigInteger('record_id');
            $table->timestamps();

            //MobileService::indexData
            $table->index(array('mobile_profile_id', 'table_name', 'record_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobile_histories');
    }
}
