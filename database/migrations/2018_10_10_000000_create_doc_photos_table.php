<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_photos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('doc_hdr_type');
            $table->unsignedBigInteger('doc_hdr_id');
            $table->unsignedBigInteger('doc_dtl_id');
            $table->unsignedBigInteger('item_id');
            $table->string('path');
            $table->string('desc_01');
            $table->string('desc_02');
            $table->string('old_filename');
            $table->unsignedInteger('old_width');
            $table->unsignedInteger('old_height');
            $table->timestamps();

            $table->index(array('doc_hdr_type','doc_hdr_id','doc_dtl_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_photos');
    }
}
