<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatchJobStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batch_job_status', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->smallInteger('proc_type')->unsigned();
            $table->unsignedBigInteger('user_id');

            $table->unsignedDecimal('status_number', 25, 8);
            $table->timestamps();

            $table->index(array('proc_type', 'user_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batch_job_status');
    }
}
