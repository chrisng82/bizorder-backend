<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFailDelCDtlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fail_del_c_dtls', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('hdr_id');
            $table->smallInteger('line_no')->unsigned();

            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('item_id');
            $table->string('desc_01');
            $table->string('desc_02');

            $table->unsignedBigInteger('uom_id');
            $table->unsignedDecimal('uom_rate', 14, 6);
            $table->unsignedDecimal('qty', 14, 6);
    
            $table->string('whse_job_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fail_del_c_dtls');
    }
}
