<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvTxnFlowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inv_txn_flows', function (Blueprint $table) {
            $table->primary(['division_id', 'proc_type']);  
            $table->unsignedBigInteger('division_id');
            $table->smallInteger('txn_flow_type')->unsigned();
            $table->smallInteger('proc_type')->unsigned();           
            $table->smallInteger('step')->unsigned();

            $table->string('controller');
            $table->string('action');
            $table->smallInteger('scan_mode')->unsigned();
            $table->string('fr_res_type', 2);
            $table->string('to_res_type', 2);

            $table->smallInteger('to_doc_status')->unsigned();
            $table->timestamps();

            $table->index(array('division_id', 'txn_flow_type'));
            $table->index(array('division_id', 'fr_res_type'));            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inv_txn_flows');
    }
}
