<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->smallInteger('location_type')->unsigned();
            $table->string('code', 35)->unique();
            $table->string('ref_code_01', 35);
            $table->string('desc_01');
            $table->string('desc_02');

            $table->unsignedBigInteger('site_id');
            $table->timestamps();

            $table->index(array('site_id', 'code'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
