<?php

use App\Uom;
use App\Item;
use App\ItemUom;
use App\Services\Env\ItemType;
use App\Services\Env\ScanMode;
use App\Services\Env\StorageClass;
use App\Services\Env\ResStatus;
use App\Services\Env\RetrievalMethod;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 35);
            $table->string('ref_code_01', 35);
            $table->string('desc_01');
            $table->string('desc_02');

            $table->smallInteger('batch_serial_control')->unsigned();
            $table->smallInteger('inspection_control')->unsigned();

            $table->smallInteger('storage_class')->unsigned(); //storage class
            $table->unsignedBigInteger('item_group_01_id'); //brand
            $table->unsignedBigInteger('item_group_02_id'); //category
            $table->unsignedBigInteger('item_group_03_id'); //principal
            $table->unsignedBigInteger('item_group_04_id'); //market segment
            $table->unsignedBigInteger('item_group_05_id');
            //denormalize from group table
            $table->string('item_group_01_code'); //brand
            $table->string('item_group_02_code'); //category
            $table->string('item_group_03_code'); //principal
            $table->string('item_group_04_code'); //market segment
            $table->string('item_group_05_code');

            $table->smallInteger('s_storage_class')->unsigned(); //storage class
            $table->unsignedBigInteger('s_item_group_01_id'); //brand
            $table->unsignedBigInteger('s_item_group_02_id'); //category
            $table->unsignedBigInteger('s_item_group_03_id'); //principal
            $table->unsignedBigInteger('s_item_group_04_id'); //market segment
            $table->unsignedBigInteger('s_item_group_05_id');
            //denormalize from group table
            $table->string('s_item_group_01_code'); //brand
            $table->string('s_item_group_02_code'); //category
            $table->string('s_item_group_03_code'); //principal
            $table->string('s_item_group_04_code'); //market segment            
            $table->string('s_item_group_05_code');

            $table->smallInteger('qty_scale')->unsigned();
            $table->smallInteger('item_type')->unsigned();
            $table->smallInteger('scan_mode')->unsigned();
            $table->smallInteger('retrieval_method')->unsigned();
            
            $table->unsignedBigInteger('unit_uom_id');
            $table->unsignedBigInteger('case_uom_id'); //case_uom_id default to be 2
            $table->unsignedDecimal('case_uom_rate', 14, 6);
            $table->unsignedDecimal('pallet_uom_rate', 14, 6);  //pallet_uom_id always is 1

            $table->unsignedInteger('case_ext_length'); //mm
            $table->unsignedInteger('case_ext_width'); //mm
            $table->unsignedInteger('case_ext_height'); //mm
            $table->unsignedDecimal('case_gross_weight', 25, 4); //kg

            $table->smallInteger('cases_per_pallet_length')->unsigned(); //number
            $table->smallInteger('cases_per_pallet_width')->unsigned(); //number
            $table->smallInteger('no_of_layers')->unsigned(); //number

            $table->boolean('is_length_allowed_vertical');
            $table->boolean('is_width_allowed_vertical');
            $table->boolean('is_height_allowed_vertical');
            $table->smallInteger('status')->unsigned();

            $table->timestamps();
        });

        //add a pallet item
        $item = new Item;
        $item->code = 'PALLET';
        $item->ref_code_01 = '';
        $item->desc_01 = 'PALLET';
        $item->desc_02 = '';
        $item->storage_class = StorageClass::$MAP['NULL'];
        $item->item_group_01_id = 0;
        $item->item_group_02_id = 0;
        $item->item_group_03_id = 0;
        $item->item_group_04_id = 0;
        $item->item_group_05_id = 0;
        $item->item_group_01_code = '';
        $item->item_group_02_code = '';
        $item->item_group_03_code = '';
        $item->item_group_04_code = '';
        $item->item_group_05_code = '';
        $item->s_storage_class = StorageClass::$MAP['NULL'];
        $item->s_item_group_01_id = 0;
        $item->s_item_group_02_id = 0;
        $item->s_item_group_03_id = 0;
        $item->s_item_group_04_id = 0;
        $item->s_item_group_05_id = 0;
        $item->s_item_group_01_code = '';
        $item->s_item_group_02_code = '';
        $item->s_item_group_03_code = '';
        $item->s_item_group_04_code = '';
        $item->s_item_group_05_code = '';
        $item->qty_scale = 0;
        $item->item_type = ItemType::$MAP['NULL'];
        $item->scan_mode = ScanMode::$MAP['NULL'];
        $item->retrieval_method = RetrievalMethod::$MAP['NULL'];
        $item->unit_uom_id = Uom::$PALLET;
        $item->case_uom_id = Uom::$PALLET;
        $item->case_uom_rate = 1;
        $item->pallet_uom_rate = 0;
        $item->case_ext_length = 1200;
        $item->case_ext_width = 1000;
        $item->case_ext_height = 145;
        $item->case_gross_weight = 30;
        $item->cases_per_pallet_length = 0;
        $item->cases_per_pallet_width = 0;
        $item->no_of_layers = 0;
        $item->is_length_allowed_vertical = 0;
        $item->is_width_allowed_vertical = 0;
        $item->is_height_allowed_vertical = 1;
        $item->status = ResStatus::$MAP['ACTIVE'];
        $item->save();

        $itemUom = new ItemUom;
        $itemUom->item_id = $item->id;
        $itemUom->uom_id = Uom::$PALLET;
        $itemUom->barcode = '';
        $itemUom->uom_rate = 1;
        $itemUom->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
