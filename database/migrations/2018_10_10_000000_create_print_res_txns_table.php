<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrintResTxnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('print_res_txns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('res_type');
            $table->unsignedBigInteger('res_id');
            $table->unsignedBigInteger('user_id');

            $table->timestamps();

            $table->index(array('res_type', 'res_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('print_res_txns');
    }
}
