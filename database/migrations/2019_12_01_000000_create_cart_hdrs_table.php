<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartHdrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_hdrs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('doc_code', 35)->unique();
            
            $table->string('ref_code_01', 35);
            $table->string('ref_code_02', 35);
            $table->date('doc_date');
            $table->string('desc_01');
            $table->string('desc_02');

            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('division_id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('salesman_id');
            $table->unsignedBigInteger('debtor_id');
            // $table->unsignedInteger('delivery_point_id');
            
            $table->string('delivery_point_unit_no');
            $table->string('delivery_point_building_name');
            $table->string('delivery_point_street_name');
            $table->string('delivery_point_district_01');
            $table->string('delivery_point_district_02');
            $table->string('delivery_point_postcode');
            $table->string('delivery_point_area_name');
            $table->string('delivery_point_state_name');
            $table->string('delivery_point_country_name');
            $table->string('delivery_point_attention');
            $table->string('delivery_point_phone_01');
            $table->string('delivery_point_phone_02');
            $table->string('delivery_point_fax_01');
            $table->string('delivery_point_fax_02');
            $table->string('delivery_point_email_01');
            $table->string('delivery_point_email_02');

            // $table->string('del_address_01', 255);
            // $table->string('del_address_02', 255);
            // $table->string('del_address_03', 255);
            // $table->string('del_address_04', 255);
            // $table->string('del_phone_01', 255);
            // $table->string('del_phone_02', 255);
            // $table->string('del_fax_01', 255);
            // $table->string('del_fax_02', 255);

            $table->unsignedBigInteger('credit_term_id');
            $table->date('est_del_date');

            $table->unsignedBigInteger('currency_id');
            $table->unsignedDecimal('currency_rate', 25, 8);
            $table->unsignedDecimal('gross_amt', 25, 8);
            $table->unsignedDecimal('gross_local_amt', 25, 8);

            $table->unsignedDecimal('disc_amt', 25, 8);
            $table->unsignedDecimal('disc_local_amt', 25, 8);

            $table->unsignedDecimal('tax_amt', 25, 8);
            $table->unsignedDecimal('tax_local_amt', 25, 8);

            $table->boolean('is_round_adj');
            $table->decimal('round_adj_amt', 25, 8);
            $table->decimal('round_adj_local_amt', 25, 8);

            $table->unsignedDecimal('net_amt', 25, 8);
            $table->unsignedDecimal('net_local_amt', 25, 8);
            
            $table->smallInteger('doc_status')->unsigned();
            $table->timestamps();

            $table->index(array('division_id', 'doc_code'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_hdrs');
    }
}
