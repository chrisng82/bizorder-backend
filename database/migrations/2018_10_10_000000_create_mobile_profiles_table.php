<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobileProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobile_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('device_uuid');
            $table->smallInteger('mobile_app')->unsigned();
            $table->smallInteger('mobile_os')->unsigned();
            $table->string('os_version');
            $table->string('app_version');
            $table->timestamp('synced_at');
            $table->timestamps();

            $table->unique(array('device_uuid', 'mobile_app'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobile_profiles');
    }
}
