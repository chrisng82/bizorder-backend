<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlsInvCDtlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sls_inv_c_dtls', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('hdr_id'); 

            $table->smallInteger('line_no')->unsigned();
            $table->unsignedBigInteger('item_id');
            $table->string('desc_01');
            $table->string('desc_02');

            $table->unsignedBigInteger('location_id');
            
            $table->unsignedBigInteger('uom_id');
            $table->unsignedDecimal('uom_rate', 14, 6);
            $table->unsignedDecimal('sale_price', 25, 8);
            $table->unsignedDecimal('price_disc', 25, 8);
            $table->unsignedDecimal('qty', 14, 6);

            $table->unsignedDecimal('gross_amt', 25, 8);
            $table->unsignedDecimal('gross_local_amt', 25, 8);

            $table->unsignedBigInteger('promo_hdr_id');
            $table->uuid('promo_uuid');
            $table->unsignedDecimal('dtl_disc_val_01', 25, 8);
            $table->unsignedDecimal('dtl_disc_perc_01', 9, 5);
            $table->unsignedDecimal('dtl_disc_val_02', 25, 8);
            $table->unsignedDecimal('dtl_disc_perc_02', 9, 5);
            $table->unsignedDecimal('dtl_disc_val_03', 25, 8);
            $table->unsignedDecimal('dtl_disc_perc_03', 9, 5);
            $table->unsignedDecimal('dtl_disc_val_04', 25, 8);
            $table->unsignedDecimal('dtl_disc_perc_04', 9, 5);
            $table->unsignedDecimal('dtl_disc_val_05', 25, 8);
            $table->unsignedDecimal('dtl_disc_perc_05', 9, 5);

            $table->unsignedDecimal('dtl_disc_amt', 25, 8);
            $table->unsignedDecimal('dtl_disc_local_amt', 25, 8);

            $table->unsignedDecimal('hdr_disc_val_01', 25, 8);
            $table->unsignedDecimal('hdr_disc_perc_01', 9, 5);
            $table->unsignedDecimal('hdr_disc_val_02', 25, 8);
            $table->unsignedDecimal('hdr_disc_perc_02', 9, 5);
            $table->unsignedDecimal('hdr_disc_val_03', 25, 8);
            $table->unsignedDecimal('hdr_disc_perc_03', 9, 5);
            $table->unsignedDecimal('hdr_disc_val_04', 25, 8);
            $table->unsignedDecimal('hdr_disc_perc_04', 9, 5);
            $table->unsignedDecimal('hdr_disc_val_05', 25, 8);
            $table->unsignedDecimal('hdr_disc_perc_05', 9, 5);

            $table->unsignedDecimal('hdr_disc_amt', 25, 8);
            $table->unsignedDecimal('hdr_disc_local_amt', 25, 8);

            $table->unsignedBigInteger('tax_hdr_id');
            $table->unsignedDecimal('dtl_taxable_amt_01', 25, 8);
            $table->boolean('dtl_tax_incl_01');
            $table->unsignedDecimal('dtl_tax_val_01', 25, 8);
            $table->unsignedDecimal('dtl_tax_perc_01', 9, 5);
            $table->decimal('dtl_tax_adj_01', 25, 8);
            $table->unsignedDecimal('dtl_tax_amt_01', 25, 8);
            $table->unsignedDecimal('dtl_tax_local_amt_01', 25, 8);

            $table->unsignedDecimal('dtl_taxable_amt_02', 25, 8);
            $table->boolean('dtl_tax_incl_02');
            $table->unsignedDecimal('dtl_tax_val_02', 25, 8);
            $table->unsignedDecimal('dtl_tax_perc_02', 9, 5);
            $table->decimal('dtl_tax_adj_02', 25, 8);
            $table->unsignedDecimal('dtl_tax_amt_02', 25, 8);
            $table->unsignedDecimal('dtl_tax_local_amt_02', 25, 8);

            $table->unsignedDecimal('dtl_taxable_amt_03', 25, 8);
            $table->boolean('dtl_tax_incl_03');
            $table->unsignedDecimal('dtl_tax_val_03', 25, 8);
            $table->unsignedDecimal('dtl_tax_perc_03', 9, 5);
            $table->decimal('dtl_tax_adj_03', 25, 8);
            $table->unsignedDecimal('dtl_tax_amt_03', 25, 8);
            $table->unsignedDecimal('dtl_tax_local_amt_03', 25, 8);

            $table->unsignedDecimal('dtl_taxable_amt_04', 25, 8);
            $table->boolean('dtl_tax_incl_04');
            $table->unsignedDecimal('dtl_tax_val_04', 25, 8);
            $table->unsignedDecimal('dtl_tax_perc_04', 9, 5);
            $table->decimal('dtl_tax_adj_04', 25, 8);
            $table->unsignedDecimal('dtl_tax_amt_04', 25, 8);
            $table->unsignedDecimal('dtl_tax_local_amt_04', 25, 8);

            $table->unsignedDecimal('dtl_taxable_amt_05', 25, 8);
            $table->boolean('dtl_tax_incl_05');
            $table->unsignedDecimal('dtl_tax_val_05', 25, 8);
            $table->unsignedDecimal('dtl_tax_perc_05', 9, 5);
            $table->decimal('dtl_tax_adj_05', 25, 8);
            $table->unsignedDecimal('dtl_tax_amt_05', 25, 8);
            $table->unsignedDecimal('dtl_tax_local_amt_05', 25, 8);

            $table->unsignedDecimal('hdr_taxable_amt_01', 25, 8);
            $table->boolean('hdr_tax_incl_01');
            $table->unsignedDecimal('hdr_tax_val_01', 25, 8);
            $table->unsignedDecimal('hdr_tax_perc_01', 9, 5);
            $table->decimal('hdr_tax_adj_01', 25, 8);
            $table->unsignedDecimal('hdr_tax_amt_01', 25, 8);
            $table->unsignedDecimal('hdr_tax_local_amt_01', 25, 8);

            $table->unsignedDecimal('hdr_taxable_amt_02', 25, 8);
            $table->boolean('hdr_tax_incl_02');
            $table->unsignedDecimal('hdr_tax_val_02', 25, 8);
            $table->unsignedDecimal('hdr_tax_perc_02', 9, 5);
            $table->decimal('hdr_tax_adj_02', 25, 8);
            $table->unsignedDecimal('hdr_tax_amt_02', 25, 8);
            $table->unsignedDecimal('hdr_tax_local_amt_02', 25, 8);

            $table->unsignedDecimal('hdr_taxable_amt_03', 25, 8);
            $table->boolean('hdr_tax_incl_03');
            $table->unsignedDecimal('hdr_tax_val_03', 25, 8);
            $table->unsignedDecimal('hdr_tax_perc_03', 9, 5);
            $table->decimal('hdr_tax_adj_03', 25, 8);
            $table->unsignedDecimal('hdr_tax_amt_03', 25, 8);
            $table->unsignedDecimal('hdr_tax_local_amt_03', 25, 8);

            $table->unsignedDecimal('hdr_taxable_amt_04', 25, 8);
            $table->boolean('hdr_tax_incl_04');
            $table->unsignedDecimal('hdr_tax_val_04', 25, 8);
            $table->unsignedDecimal('hdr_tax_perc_04', 9, 5);
            $table->decimal('hdr_tax_adj_04', 25, 8);
            $table->unsignedDecimal('hdr_tax_amt_04', 25, 8);
            $table->unsignedDecimal('hdr_tax_local_amt_04', 25, 8);

            $table->unsignedDecimal('hdr_taxable_amt_05', 25, 8);
            $table->boolean('hdr_tax_incl_05');
            $table->unsignedDecimal('hdr_tax_val_05', 25, 8);
            $table->unsignedDecimal('hdr_tax_perc_05', 9, 5);
            $table->decimal('hdr_tax_adj_05', 25, 8);
            $table->unsignedDecimal('hdr_tax_amt_05', 25, 8);
            $table->unsignedDecimal('hdr_tax_local_amt_05', 25, 8);

            $table->unsignedDecimal('net_amt', 25, 8);
            $table->unsignedDecimal('net_local_amt', 25, 8);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sls_inv_c_dtls');
    }
}
