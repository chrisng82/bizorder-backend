<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteDocNosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_doc_nos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('site_id');
            $table->string('doc_no_type');
            $table->unsignedBigInteger('doc_no_id');
            $table->smallInteger('seq_no')->unsigned();
            
            $table->timestamps();

            $table->index(array('site_id', 'doc_no_type', 'doc_no_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_doc_nos');
    }
}
