<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDivisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('divisions', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('site_id');
            $table->unsignedBigInteger('site_flow_id');
            $table->unsignedBigInteger('company_id');
            
            $table->string('code', 35)->unique();
            $table->string('ref_code_01', 35);
            $table->string('name_01');
            $table->string('name_02');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('divisions');
    }
}
