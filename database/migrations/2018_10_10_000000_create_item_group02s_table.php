<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemGroup02sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_group02s', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 35);
            $table->string('desc_01');
            $table->string('desc_02');
            $table->string('image_path');
            $table->string('image_desc_01');
            $table->string('image_desc_02');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_group02s');
    }
}
