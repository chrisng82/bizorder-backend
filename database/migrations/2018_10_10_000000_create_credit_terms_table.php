<?php

use App\CreditTerm;
use App\Services\Env\CreditTermType;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_terms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 35)->unique();
            $table->smallInteger('credit_term_type')->unsigned();
            $table->string('desc_01');
            $table->string('desc_02');

            $table->smallInteger('period_01');
            $table->smallInteger('period_02');
            
            $table->timestamps();
        });

        $creditTerm = new CreditTerm();
        $creditTerm->code = 'CASH';
        $creditTerm->credit_term_type = CreditTermType::$MAP['CASH'];
        $creditTerm->desc_01 = '';
        $creditTerm->desc_02 = '';
        $creditTerm->period_01 = 0;
        $creditTerm->period_02 = 0;
        $creditTerm->save();

        $creditTerm = new CreditTerm();
        $creditTerm->code = 'COD';
        $creditTerm->credit_term_type = CreditTermType::$MAP['COD'];
        $creditTerm->desc_01 = '';
        $creditTerm->desc_02 = '';
        $creditTerm->period_01 = 0;
        $creditTerm->period_02 = 0;
        $creditTerm->save();

        $creditTerm = new CreditTerm();
        $creditTerm->code = '7DAYS';
        $creditTerm->credit_term_type = CreditTermType::$MAP['DUE_IN_NO_OF_DAYS'];
        $creditTerm->desc_01 = '';
        $creditTerm->desc_02 = '';
        $creditTerm->period_01 = 7;
        $creditTerm->period_02 = 0;
        $creditTerm->save();

        $creditTerm = new CreditTerm();
        $creditTerm->code = '14DAYS';
        $creditTerm->credit_term_type = CreditTermType::$MAP['DUE_IN_NO_OF_DAYS'];
        $creditTerm->desc_01 = '';
        $creditTerm->desc_02 = '';
        $creditTerm->period_01 = 14;
        $creditTerm->period_02 = 0;
        $creditTerm->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_terms');
    }
}
