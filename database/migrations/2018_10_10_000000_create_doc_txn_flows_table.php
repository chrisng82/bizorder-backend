<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocTxnFlowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_txn_flows', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->smallInteger('proc_type')->unsigned();

            $table->string('fr_doc_hdr_type');
            $table->unsignedBigInteger('fr_doc_hdr_id');
            $table->string('fr_doc_hdr_code');
            $table->string('to_doc_hdr_type');
            $table->unsignedBigInteger('to_doc_hdr_id');
            $table->boolean('is_closed');

            $table->timestamps();

            //DocTxnFlowRepository::findByProcTypeAndFrHdrId
            //PickListHdrRepository::findAllNotExistPackList01Txn
            $table->index(array('proc_type', 'fr_doc_hdr_type', 'fr_doc_hdr_id', 'is_closed'), 'fr_doc_attr_index');

            //DocTxnFlowRepository::findAllByFrHdrTypeAndToHdrId
            //DocTxnFlowRepository::findAllByFrHdrTypeAndToHdrIds
            $table->index(array('fr_doc_hdr_type', 'to_doc_hdr_type', 'to_doc_hdr_id'), 'to_doc_attr_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_txn_flows');
    }
}
