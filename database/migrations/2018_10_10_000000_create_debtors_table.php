<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDebtorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debtors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 35);
            $table->string('ref_code_01', 35);
            $table->string('company_name_01');
            $table->string('company_name_02');
            $table->string('cmpy_register_no');
            $table->string('tax_register_no');
            
            $table->unsignedBigInteger('debtor_category_01_id');
            $table->unsignedBigInteger('debtor_category_02_id');
            $table->unsignedBigInteger('debtor_category_03_id');

            $table->unsignedBigInteger('area_id');
            $table->string('area_code', 35);
            $table->unsignedBigInteger('state_id');
            $table->string('address_01', 255);
            $table->string('address_02', 255);
            $table->string('address_03', 255);
            $table->string('address_04', 255);
            $table->string('unit_no');
            $table->string('building_name');
            $table->string('street_name');
            $table->string('district_01');
            $table->string('district_02');
            $table->string('postcode');
            $table->string('state_name');
            $table->string('country_name');

            $table->string('attention');
            $table->string('phone_01');
            $table->string('phone_02');
            $table->string('fax_01');
            $table->string('fax_02');
            $table->string('email_01');
            $table->string('email_02');
            $table->smallInteger('status')->unsigned();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debtors');
    }
}
