<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemBatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_batches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('item_id');
            $table->string('batch_serial_no', 100); //barcode
            $table->date('expiry_date');
            $table->date('receipt_date');
            
            $table->timestamps();

            //QuantBalRepository::commitStockReceipt
            $table->index(array('item_id', 'expiry_date', 'batch_serial_no', 'receipt_date'), 'item_batches_attr_index');
            $table->index(array('batch_serial_no'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_batches');
    }
}
