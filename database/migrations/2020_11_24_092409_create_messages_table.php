<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->comment('Message recipient user id');
            $table->unsignedBigInteger('division_id')->comment('Message recipient division id');
            $table->unsignedBigInteger('company_id')->comment('Message recipient company id');
            $table->dateTime('msg_date')->comment('Date time of message created');
            $table->string('title')->comment('Title of message');
            $table->text('content')->comment('Content of message');
            $table->smallInteger('priority')->unsigned()->default(0)->comment('0 = Normal, 1 = Urgent');
            $table->smallInteger('status')->unsigned()->default(0)->comment('0 = Unread, 2 = Read');
            $table->timestamps();

            $table->index(array('user_id', 'division_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
