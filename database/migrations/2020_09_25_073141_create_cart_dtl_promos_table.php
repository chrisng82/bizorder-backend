<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartDtlPromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_dtl_promotions', function (Blueprint $table) {
            //
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cart_dtl_id'); 
            $table->unsignedBigInteger('promotion_id');
            $table->uuid('promo_uuid');
            $table->unsignedDecimal('sale_price', 25, 8);
            $table->unsignedDecimal('price_disc', 25, 8);
            $table->unsignedDecimal('disc_val_01', 25, 8);
            $table->unsignedDecimal('disc_perc_01', 9, 5);
            $table->unsignedDecimal('disc_val_02', 25, 8);
            $table->unsignedDecimal('disc_perc_02', 9, 5);
            $table->unsignedDecimal('disc_val_03', 25, 8);
            $table->unsignedDecimal('disc_perc_03', 9, 5);
            $table->unsignedDecimal('disc_val_04', 25, 8);
            $table->unsignedDecimal('disc_perc_04', 9, 5);
            $table->boolean('is_main');
            $table->boolean('is_add_on');
            $table->timestamps();
            $table->index(array('cart_dtl_id', 'promotion_id'));
        });

        Schema::create('sls_ord_dtl_promotions', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->unsignedBigInteger('sls_ord_dtl_id'); 
            $table->unsignedBigInteger('promotion_id');
            $table->uuid('promo_uuid');
            $table->unsignedDecimal('sale_price', 25, 8);
            $table->unsignedDecimal('price_disc', 25, 8);
            $table->unsignedDecimal('disc_val_01', 25, 8);
            $table->unsignedDecimal('disc_perc_01', 9, 5);
            $table->unsignedDecimal('disc_val_02', 25, 8);
            $table->unsignedDecimal('disc_perc_02', 9, 5);
            $table->unsignedDecimal('disc_val_03', 25, 8);
            $table->unsignedDecimal('disc_perc_03', 9, 5);
            $table->unsignedDecimal('disc_val_04', 25, 8);
            $table->unsignedDecimal('disc_perc_04', 9, 5);
            $table->boolean('is_main');
            $table->boolean('is_add_on');
            $table->timestamps();
            $table->index(array('sls_ord_dtl_id', 'promotion_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_dtl_promotions');
        Schema::dropIfExists('sls_ord_dtl_promotions');
    }
}
