<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToCartDtlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cart_dtls', function (Blueprint $table) {
            $table->smallInteger('status')->unsigned()->default('100')->after('net_local_amt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cart_dtls', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
