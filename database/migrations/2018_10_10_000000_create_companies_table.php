<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 35)->unique();
            $table->string('co_reg_no');
            $table->string('tax_reg_no');
            $table->string('name_01');
            $table->string('name_02');

            $table->string('unit_no');
            $table->string('building_name');
            $table->string('street_name');
            $table->string('district_01');
            $table->string('district_02');
            $table->string('postcode');
            $table->string('state_name');
            $table->string('country_name');

            $table->string('attention');
            $table->string('phone_01');
            $table->string('phone_02');
            $table->string('fax_01');
            $table->string('fax_02');
            $table->string('email_01');
            $table->string('email_02');

            $table->decimal('latitude', 12, 8);
            $table->decimal('longitude', 12, 8);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
