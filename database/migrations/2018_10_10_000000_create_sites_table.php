<?php

use App\Site;
use App\SiteFlow;
use App\WhseTxnFlow;
use App\SWhseTxnFlow;
use App\UserSiteFlow;
use App\DocNo;
use App\SiteDocNo;
use App\Services\Env\TxnFlowType;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 35)->unique();
            $table->string('ref_code_01', 35);
            $table->string('desc_01');
            $table->string('desc_02');
            $table->smallInteger('max_floor')->unsigned();
            //SI base unit
            $table->unsignedDecimal('layout_width', 25, 8); //m
            $table->unsignedDecimal('layout_depth', 25, 8); //m
            $table->unsignedDecimal('layout_height', 25, 8); //m
            $table->unsignedDecimal('latitude', 12, 8);
            $table->unsignedDecimal('longitude', 12, 8);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites');
    }
}
