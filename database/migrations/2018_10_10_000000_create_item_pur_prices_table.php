<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemPurPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_pur_prices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('price_group_id');
            $table->unsignedBigInteger('currency_id');
            $table->unsignedBigInteger('item_id');
            $table->unsignedBigInteger('uom_id');
            $table->unsignedDecimal('sale_price', 25, 8);
            $table->date('valid_from');
            $table->date('valid_to');

            $table->timestamps();

            $table->index(array('item_id', 'uom_id', 'price_group_id', 'currency_id', 'valid_from'), 'item_price_attr_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_pur_prices');
    }
}
