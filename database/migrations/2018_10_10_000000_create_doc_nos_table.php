<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocNosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_nos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('doc_type');
            $table->string('doc_prefix', 35);
            $table->unsignedInteger('running_no');
            $table->smallInteger('running_no_length')->unsigned();
            $table->string('doc_suffix', 35);
            $table->boolean('is_enforce');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_nos');
    }
}
