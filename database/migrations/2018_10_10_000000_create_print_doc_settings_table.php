<?php

use App\PrintDocSetting;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrintDocSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('print_doc_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('doc_hdr_type');
            $table->unsignedBigInteger('site_flow_id');
            $table->unsignedBigInteger('division_id');
            $table->smallInteger('seq_no')->unsigned();

            $table->string('view_name');

            $table->string('page_orientation');
            $table->unsignedDecimal('page_width');
            $table->unsignedDecimal('page_height');

            $table->timestamps();

            $table->index(array('doc_hdr_type'));
        });

        //pick list whse job
        $printDocSetting = new PrintDocSetting;
        $printDocSetting->doc_hdr_type = \App\WhseJobHdr::class.'03';
        $printDocSetting->seq_no = 1;
        $printDocSetting->site_flow_id = 1;
        $printDocSetting->division_id = 0;
        $printDocSetting->view_name = 'reports.whseJob03BatchPrint';
        $printDocSetting->page_orientation = 'portrait';
        $printDocSetting->page_width = 595.28; //A4
        $printDocSetting->page_height = 841.89; //A4
        $printDocSetting->save();

        //gds rcpt whse job
        $printDocSetting = new PrintDocSetting;
        $printDocSetting->doc_hdr_type = \App\WhseJobHdr::class.'14';
        $printDocSetting->seq_no = 1;
        $printDocSetting->site_flow_id = 1;
        $printDocSetting->division_id = 0;
        $printDocSetting->view_name = 'reports.whseJob14BatchPrint';
        $printDocSetting->page_orientation = 'portrait';
        $printDocSetting->page_width = 595.28; //A4
        $printDocSetting->page_height = 841.89; //A4
        $printDocSetting->save();

        //put away whse job
        $printDocSetting = new PrintDocSetting;
        $printDocSetting->doc_hdr_type = \App\WhseJobHdr::class.'15';
        $printDocSetting->seq_no = 1;
        $printDocSetting->site_flow_id = 1;
        $printDocSetting->division_id = 0;
        $printDocSetting->view_name = 'reports.whseJob15BatchPrint';
        $printDocSetting->page_orientation = 'portrait';
        $printDocSetting->page_width = 595.28; //A4
        $printDocSetting->page_height = 841.89; //A4
        $printDocSetting->save();

        $printDocSetting = new PrintDocSetting;
        $printDocSetting->doc_hdr_type = \App\PickListHdr::class;
        $printDocSetting->seq_no = 1;
        $printDocSetting->site_flow_id = 1;
        $printDocSetting->division_id = 0;
        $printDocSetting->view_name = 'reports.pickListBatchPrint';
        $printDocSetting->page_orientation = 'portrait';
        $printDocSetting->page_width = 595.28; //A4
        $printDocSetting->page_height = 841.89; //A4
        $printDocSetting->save();

        $printDocSetting = new PrintDocSetting;
        $printDocSetting->doc_hdr_type = \App\SlsInvHdr::class;
        $printDocSetting->seq_no = 1;
        $printDocSetting->site_flow_id = 0;
        $printDocSetting->division_id = 1;
        $printDocSetting->view_name = 'reports.slsInvBatchPrint';
        $printDocSetting->page_orientation = 'portrait';
        $printDocSetting->page_width = 612.00; //Letter
        $printDocSetting->page_height = 792.00; //Letter
        $printDocSetting->save();

        $printDocSetting = new PrintDocSetting;
        $printDocSetting->doc_hdr_type = \App\HandlingUnit::class.'01';
        $printDocSetting->seq_no = 1;
        $printDocSetting->site_flow_id = 1;
        $printDocSetting->division_id = 0;
        $printDocSetting->view_name = 'reports.handlingUnit01BatchPrint';
        $printDocSetting->page_orientation = 'landscape';
        $printDocSetting->page_width = 595.28; //A4
        $printDocSetting->page_height = 841.89; //A4
        $printDocSetting->save();

        $printDocSetting = new PrintDocSetting;
        $printDocSetting->doc_hdr_type = \App\HandlingUnit::class.'02';
        $printDocSetting->seq_no = 1;
        $printDocSetting->site_flow_id = 1;
        $printDocSetting->division_id = 0;
        $printDocSetting->view_name = 'reports.handlingUnit02BatchPrint';
        $printDocSetting->page_orientation = 'landscape';
        $printDocSetting->page_width = 595.28; //A4
        $printDocSetting->page_height = 841.89; //A4
        $printDocSetting->save();

        //bin trf02 whse job
        $printDocSetting = new PrintDocSetting;
        $printDocSetting->doc_hdr_type = \App\BinTrfHdr::class.'02';
        $printDocSetting->seq_no = 1;
        $printDocSetting->site_flow_id = 1;
        $printDocSetting->division_id = 0;
        $printDocSetting->view_name = 'reports.binTrf02BatchPrint';
        $printDocSetting->page_orientation = 'portrait';
        $printDocSetting->page_width = 595.28; //A4
        $printDocSetting->page_height = 841.89; //A4
        $printDocSetting->save();

        $printDocSetting = new PrintDocSetting;
        $printDocSetting->doc_hdr_type = \App\WhseJobHdr::class.'030102';
        $printDocSetting->seq_no = 1;
        $printDocSetting->site_flow_id = 1;
        $printDocSetting->division_id = 0;
        $printDocSetting->view_name = 'reports.whseJob030102BatchPrint';
        $printDocSetting->page_orientation = 'portrait';
        $printDocSetting->page_width = 612.00; //A4
        $printDocSetting->page_height = 792.00; //A4
        $printDocSetting->save();

        $printDocSetting = new PrintDocSetting;
        $printDocSetting->doc_hdr_type = \App\WhseJobHdr::class.'160102';
        $printDocSetting->seq_no = 1;
        $printDocSetting->site_flow_id = 1;
        $printDocSetting->division_id = 0;
        $printDocSetting->view_name = 'reports.whseJob160102BatchPrint';
        $printDocSetting->page_orientation = 'portrait';
        $printDocSetting->page_width = 612.00; //Letter
        $printDocSetting->page_height = 792.00; //Letter
        $printDocSetting->save();

        $printDocSetting = new PrintDocSetting;
        $printDocSetting->doc_hdr_type = \App\GdsRcptHdr::class.'02';
        $printDocSetting->seq_no = 1;
        $printDocSetting->site_flow_id = 1;
        $printDocSetting->division_id = 0;
        $printDocSetting->view_name = 'reports.gdsRcpt02BatchPrint';
        $printDocSetting->page_orientation = 'portrait';
        $printDocSetting->page_width = 612.00; //Letter
        $printDocSetting->page_height = 792.00; //Letter
        $printDocSetting->save();

        $printDocSetting = new PrintDocSetting;
        $printDocSetting->doc_hdr_type = \App\WhseJobHdr::class.'160102';
        $printDocSetting->seq_no = 1;
        $printDocSetting->site_flow_id = 1;
        $printDocSetting->division_id = 0;
        $printDocSetting->view_name = 'reports.whseJob160102BatchPrint';
        $printDocSetting->page_orientation = 'portrait';
        $printDocSetting->page_width = 595.28; //A4
        $printDocSetting->page_height = 841.89; //A4
        $printDocSetting->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('print_doc_settings');
    }
}
