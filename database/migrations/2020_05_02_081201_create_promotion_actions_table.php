<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotion_actions', function (Blueprint $table) {
            //
            $table->bigIncrements('id');
            $table->unsignedBigInteger('promotion_id');
            $table->string('type');
            $table->json('config')->nullable();
            $table->timestamps();

            $table->index('promotion_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotion_actions');
    }
}
