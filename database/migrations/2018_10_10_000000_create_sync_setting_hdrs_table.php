<?php

use App\SyncSettingHdr;
use App\SyncSettingDtl;
use App\Services\Env\ProcType;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSyncSettingHdrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sync_setting_hdrs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->smallInteger('proc_type')->unsigned();
            $table->unsignedBigInteger('site_flow_id');
            $table->unsignedBigInteger('division_id');
            $table->string('url');
            $table->unsignedInteger('page_size');
            $table->unsignedInteger('last_total');
            $table->timestamp('last_synced_at');
            $table->unsignedBigInteger('last_user_id');

            $table->timestamps();

            $table->index(array('proc_type', 'site_flow_id', 'division_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sync_setting_hdrs');
    }
}
