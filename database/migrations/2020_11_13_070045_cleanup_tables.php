<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CleanupTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::dropIfExists('adv_ship_c_dtls');
        Schema::dropIfExists('adv_ship_dtls');
        Schema::dropIfExists('adv_ship_hdrs');
        Schema::dropIfExists('adv_ship_items');
        Schema::dropIfExists('bin_trf_c_dtls');
        Schema::dropIfExists('bin_trf_dtls');
        Schema::dropIfExists('bin_trf_hdrs');
        Schema::dropIfExists('count_adj_c_dtls');
        Schema::dropIfExists('count_adj_dtls');
        Schema::dropIfExists('count_adj_hdrs');
        Schema::dropIfExists('cycle_count_c_dtls');
        Schema::dropIfExists('cycle_count_dtls');
        Schema::dropIfExists('cycle_count_hdrs');
        Schema::dropIfExists('equipment');
        Schema::dropIfExists('gds_rcpt_c_dtls');
        Schema::dropIfExists('gds_rcpt_dtls');
        Schema::dropIfExists('gds_rcpt_hdrs');
        Schema::dropIfExists('gds_rcpt_inb_ords');
        Schema::dropIfExists('gds_rcpt_items');
        Schema::dropIfExists('handling_units');
        Schema::dropIfExists('inb_ord_c_dtls');
        Schema::dropIfExists('inb_ord_dtls');
        Schema::dropIfExists('inb_ord_hdrs');
        Schema::dropIfExists('load_list_c_dtls');
        Schema::dropIfExists('load_list_dtls');
        Schema::dropIfExists('load_list_hdrs');
        Schema::dropIfExists('outb_ord_c_dtls');
        Schema::dropIfExists('outb_ord_dtls');
        Schema::dropIfExists('outb_ord_hdrs');
        Schema::dropIfExists('pack_list_c_dtls');
        Schema::dropIfExists('pack_list_dtls');
        Schema::dropIfExists('pack_list_hdrs');
        Schema::dropIfExists('pack_list_outb_ords');
        Schema::dropIfExists('picking_criterias');
        Schema::dropIfExists('pick_face_strategies');
        Schema::dropIfExists('pick_list_c_dtls');
        Schema::dropIfExists('pick_list_dtls');
        Schema::dropIfExists('pick_list_hdrs');
        Schema::dropIfExists('pick_list_outb_ords');
        Schema::dropIfExists('prf_del_c_dtls');
        Schema::dropIfExists('prf_del_dtls');
        Schema::dropIfExists('prf_del_hdrs');
        Schema::dropIfExists('promo_debtors');
        Schema::dropIfExists('promo_divisions');
        Schema::dropIfExists('promo_dtls');
        Schema::dropIfExists('promo_hdrs');
        Schema::dropIfExists('pur_inv_c_dtls');
        Schema::dropIfExists('pur_inv_dtls');
        Schema::dropIfExists('pur_inv_hdrs');
        Schema::dropIfExists('pur_ord_c_dtls');
        Schema::dropIfExists('pur_ord_dtls');
        Schema::dropIfExists('pur_ord_hdrs');
        Schema::dropIfExists('pur_rtn_c_dtls');
        Schema::dropIfExists('pur_rtn_dtls');
        Schema::dropIfExists('pur_rtn_hdrs');
        Schema::dropIfExists('put_away_c_dtls');
        Schema::dropIfExists('put_away_dtls');
        Schema::dropIfExists('put_away_hdrs');
        Schema::dropIfExists('put_away_inb_ords');
        Schema::dropIfExists('quant_bals');
        Schema::dropIfExists('quant_bal_adv_txns');
        Schema::dropIfExists('quant_bal_conflicts');
        Schema::dropIfExists('quant_bal_rsvd_txns');
        Schema::dropIfExists('quant_bal_txns');
        Schema::dropIfExists('quant_bal_voids');
        Schema::dropIfExists('rtn_del_c_dtls');
        Schema::dropIfExists('rtn_del_dtls');
        Schema::dropIfExists('rtn_del_hdrs');
        Schema::dropIfExists('rtn_rcpt_c_dtls');
        Schema::dropIfExists('rtn_rcpt_dtls');
        Schema::dropIfExists('rtn_rcpt_hdrs');
        Schema::dropIfExists('storage_bays');
        Schema::dropIfExists('storage_bins');
        Schema::dropIfExists('storage_rows');
        Schema::dropIfExists('storage_strategies');
        Schema::dropIfExists('storage_types');
        Schema::dropIfExists('s_inv_txn_flows');
        Schema::dropIfExists('s_item_group01s');
        Schema::dropIfExists('s_item_group02s');
        Schema::dropIfExists('s_item_group03s');
        Schema::dropIfExists('s_item_group04s');
        Schema::dropIfExists('s_item_group05s');
        Schema::dropIfExists('s_mobile_schemas');
        Schema::dropIfExists('s_pallet_types');
        Schema::dropIfExists('s_whse_txn_flows');
        Schema::dropIfExists('whse_job_c_dtls');
        Schema::dropIfExists('whse_job_dtls');
        Schema::dropIfExists('whse_job_hdrs');
        Schema::dropIfExists('whse_job_items');
        Schema::dropIfExists('whse_txn_flows');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
