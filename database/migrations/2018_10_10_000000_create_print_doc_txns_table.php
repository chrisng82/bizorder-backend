<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrintDocTxnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('print_doc_txns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('doc_hdr_type');
            $table->unsignedBigInteger('doc_hdr_id');
            $table->unsignedBigInteger('user_id');

            $table->timestamps();

            $table->index(array('doc_hdr_type', 'doc_hdr_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('print_doc_txns');
    }
}
