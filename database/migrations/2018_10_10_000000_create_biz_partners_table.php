<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBizPartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biz_partners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->smallInteger('biz_partner_type')->unsigned();
            $table->unsignedBigInteger('creditor_id');  
            $table->unsignedBigInteger('debtor_id');
            $table->unsignedBigInteger('price_group_id');

            $table->string('code', 35)->unique();
            $table->string('ref_code_01', 35);
            $table->string('company_name_01');
            $table->string('company_name_02');
            $table->string('cmpy_register_no');
            $table->string('tax_register_no');

            $table->string('unit_no');
            $table->string('building_name');
            $table->string('street_name');
            $table->string('district_01');
            $table->string('district_02');
            $table->string('postcode');
            $table->string('state_name');
            $table->string('country_name');

            $table->string('attention');
            $table->string('phone_01');
            $table->string('phone_02');
            $table->string('fax_01');
            $table->string('fax_02');
            $table->string('email_01');
            $table->string('email_02');

            $table->smallInteger('status')->unsigned();
            $table->timestamps();
            
            $table->index(array('biz_partner_type'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('biz_partners');
    }
}
