<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 35)->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->timestamp('last_login')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('password_changed_at')->nullable();            
            $table->string('first_name');
            $table->string('last_name');
            $table->string('login_type', 2)->default('w');
            $table->unsignedBigInteger('debtor_id')->nullable();
            $table->smallInteger('status')->unsigned();
            $table->string('timezone', 50)->default('Asia/Singapore');
            $table->rememberToken();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
