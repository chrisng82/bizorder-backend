<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_points', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->smallInteger('delivery_point_type')->unsigned();
            $table->unsignedBigInteger('debtor_id');
            $table->unsignedBigInteger('price_group_id');
            $table->string('code', 35);
            $table->string('ref_code_01', 35);
            $table->string('company_name_01');
            $table->string('company_name_02');

            $table->unsignedBigInteger('debtor_group_01_id'); //category
            $table->unsignedBigInteger('debtor_group_02_id'); //chain
            $table->unsignedBigInteger('debtor_group_03_id'); //channel
            $table->unsignedBigInteger('debtor_group_04_id');
            $table->unsignedBigInteger('debtor_group_05_id');

            //delivery address
            $table->unsignedBigInteger('area_id');
            $table->string('area_code', 35);
            
            $table->unsignedBigInteger('state_id');

            $table->string('unit_no');
            $table->string('building_name');
            $table->string('street_name');
            $table->string('district_01');
            $table->string('district_02');
            $table->string('postcode');
            $table->string('state_name');
            $table->string('country_name');
            
            $table->string('attention');
            $table->string('phone_01');
            $table->string('phone_02');
            $table->string('fax_01');
            $table->string('fax_02');
            $table->string('email_01');
            $table->string('email_02');
            $table->smallInteger('status')->unsigned();

            $table->timestamps();

            $table->index(array('debtor_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_points');
    }
}
