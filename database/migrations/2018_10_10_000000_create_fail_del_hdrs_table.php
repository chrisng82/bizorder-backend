<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFailDelHdrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fail_del_hdrs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('doc_code', 35)->unique();

            $table->smallInteger('proc_type')->unsigned();
            
            $table->string('ref_code_01', 35);
            $table->string('ref_code_02', 35);
            $table->date('doc_date');
            $table->string('desc_01');
            $table->string('desc_02');
            
            $table->unsignedBigInteger('site_flow_id');
            $table->smallInteger('doc_status')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fail_del_hdrs');
    }
}
