<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 80);
        
            $table->string('name')->nullable();
            $table->string('desc_01')->nullable();
            $table->string('desc_02')->nullable();
            $table->string('cover')->nullable();
            $table->string('asset_url')->nullable();

            $table->string('photo_desc_01')->nullable();
            $table->string('photo_desc_02')->nullable();
            $table->string('old_filename')->nullable();
            $table->integer('old_width')->nullable();
            $table->integer('old_height')->nullable();
        
            $table->integer('position')->default(0);
            $table->string('type', 50);
            $table->unsignedBigInteger('division_id');
        
            $table->json('config')->nullable();
        
            $table->timestamp('valid_from')->default('2010-01-01 00:00:00');
            $table->timestamp('valid_to')->default('2038-01-19 00:00:00');
        
            $table->smallInteger('status')->unsigned();
            $table->timestamps();
            
            $table->index(array('status', 'valid_from', 'valid_to'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
