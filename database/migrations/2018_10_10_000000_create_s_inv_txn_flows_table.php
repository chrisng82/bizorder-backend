<?php

use App\Services\Env\DocStatus;
use App\Services\Env\TxnFlowType;
use App\Services\Env\ScanMode;
use App\Services\Env\ProcType;
use App\Services\Env\ResType;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSInvTxnFlowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_inv_txn_flows', function (Blueprint $table) {
            $table->primary(['txn_flow_type', 'flow_id', 'proc_type']);  
            $table->smallInteger('txn_flow_type')->unsigned(); 
            $table->unsignedBigInteger('flow_id');
            $table->smallInteger('proc_type')->unsigned();             
            $table->smallInteger('step')->unsigned(); 

            $table->string('controller');
            $table->string('action');
            $table->smallInteger('scan_mode')->unsigned(); 
            $table->string('fr_res_type', 2);
            $table->string('to_res_type', 2);

            $table->smallInteger('to_doc_status')->unsigned(); 
            $table->timestamps();
        });

        // DB::table('s_inv_txn_flows')->insert(array(
        //     //FLOW 1: SO -> OO -> INV
        //     array(
        //         'txn_flow_type' => TxnFlowType::$MAP['SALES'],
        //         'flow_id' => 1,
        //         'proc_type' => ProcType::$MAP['SLS_ORD_SYNC_01'],
        //         'step' => 1,
        //         'controller' => 'salesOrd',
        //         'action' => 'indexProcess',
        //         'scan_mode' => ScanMode::$MAP['NULL'],
        //         'fr_res_type' => ResType::$MAP['NULL'],
        //         'to_res_type' => ResType::$MAP['SLS_ORD'],
        //         'to_doc_status' => DocStatus::$MAP['COMPLETE'],
        //         'created_at' => '1983-07-09 00:00:00',
        //         'updated_at' => '1983-07-09 00:00:00'
        //     ),
        //     array(
        //         'txn_flow_type' => TxnFlowType::$MAP['SALES'],
        //         'flow_id' => 1,
        //         'proc_type' => ProcType::$MAP['OUTB_ORD_01'],
        //         'step' => 2,
        //         'controller' => 'outbOrd',
        //         'action' => 'indexProcess',
        //         'scan_mode' => ScanMode::$MAP['NULL'],
        //         'fr_res_type' => ResType::$MAP['SLS_ORD'],
        //         'to_res_type' => ResType::$MAP['OUTB_ORD'],
        //         'to_doc_status' => DocStatus::$MAP['COMPLETE'],
        //         'created_at' => '1983-07-09 00:00:00',
        //         'updated_at' => '1983-07-09 00:00:00'
        //     ),
        //     array(
        //         'txn_flow_type' => TxnFlowType::$MAP['SALES'],
        //         'flow_id' => 1,
        //         'proc_type' => ProcType::$MAP['SLS_INV_01'], //outb ord -> sls inv
        //         'step' => 3,
        //         'controller' => 'slsInv',
        //         'action' => 'indexProcess',
        //         'scan_mode' => ScanMode::$MAP['NULL'],
        //         'fr_res_type' => ResType::$MAP['OUTB_ORD'],
        //         'to_res_type' => ResType::$MAP['SLS_INV'],
        //         'to_doc_status' => DocStatus::$MAP['COMPLETE'],
        //         'created_at' => '1983-07-09 00:00:00',
        //         'updated_at' => '1983-07-09 00:00:00'
        //     ),
        //     array(
        //         'txn_flow_type' => TxnFlowType::$MAP['SALES'],
        //         'flow_id' => 1,
        //         'proc_type' => ProcType::$MAP['SLS_INV_SYNC_01'],
        //         'step' => 4,
        //         'controller' => 'slsInv',
        //         'action' => 'indexProcess',
        //         'scan_mode' => ScanMode::$MAP['NULL'],
        //         'fr_res_type' => ResType::$MAP['SLS_INV'],
        //         'to_res_type' => ResType::$MAP['NULL'],
        //         'to_doc_status' => DocStatus::$MAP['COMPLETE'],
        //         'created_at' => '1983-07-09 00:00:00',
        //         'updated_at' => '1983-07-09 00:00:00'
        //     ),

        //     //FLOW 2: SO -> OO -> DO -> INV
        //     array(
        //         'txn_flow_type' => TxnFlowType::$MAP['SALES'],
        //         'flow_id' => 2,
        //         'proc_type' => ProcType::$MAP['SLS_ORD_01'],
        //         'step' => 1,
        //         'controller' => 'slsOrd',
        //         'action' => 'indexProcess',
        //         'scan_mode' => ScanMode::$MAP['NULL'],
        //         'fr_res_type' => ResType::$MAP['NULL'],
        //         'to_res_type' => ResType::$MAP['SLS_ORD'],
        //         'to_doc_status' => DocStatus::$MAP['COMPLETE'],
        //         'created_at' => '1983-07-09 00:00:00',
        //         'updated_at' => '1983-07-09 00:00:00'
        //     ),
        //     array(
        //         'txn_flow_type' => TxnFlowType::$MAP['SALES'],
        //         'flow_id' => 2,
        //         'proc_type' => ProcType::$MAP['OUTB_ORD_01'],
        //         'step' => 2,
        //         'controller' => 'outbOrd',
        //         'action' => 'indexProcess',
        //         'scan_mode' => ScanMode::$MAP['NULL'],
        //         'fr_res_type' => ResType::$MAP['SLS_ORD'],
        //         'to_res_type' => ResType::$MAP['OUTB_ORD'],
        //         'to_doc_status' => DocStatus::$MAP['COMPLETE'],
        //         'created_at' => '1983-07-09 00:00:00',
        //         'updated_at' => '1983-07-09 00:00:00'
        //     ),
        //     array(
        //         'txn_flow_type' => TxnFlowType::$MAP['SALES'],
        //         'flow_id' => 2,
        //         'proc_type' => ProcType::$MAP['DEL_ORD_01'], //outbound order -> delivery order
        //         'step' => 3,
        //         'controller' => 'delOrd',
        //         'action' => 'indexProcess',
        //         'scan_mode' => ScanMode::$MAP['NULL'],
        //         'fr_res_type' => ResType::$MAP['OUTB_ORD'],
        //         'to_res_type' => ResType::$MAP['DEL_ORD'],
        //         'to_doc_status' => DocStatus::$MAP['COMPLETE'],
        //         'created_at' => '1983-07-09 00:00:00',
        //         'updated_at' => '1983-07-09 00:00:00'
        //     ),
        //     array(
        //         'txn_flow_type' => TxnFlowType::$MAP['SALES'],
        //         'flow_id' => 2,
        //         'proc_type' => ProcType::$MAP['SLS_INV_02'], //DO, PRF -> sales invoice
        //         'step' => 4,
        //         'controller' => 'slsInv',
        //         'action' => 'indexProcess',
        //         'scan_mode' => ScanMode::$MAP['NULL'],
        //         'fr_res_type' => ResType::$MAP['DEL_ORD'],
        //         'to_res_type' => ResType::$MAP['SLS_INV'],
        //         'to_doc_status' => DocStatus::$MAP['COMPLETE'],
        //         'created_at' => '1983-07-09 00:00:00',
        //         'updated_at' => '1983-07-09 00:00:00'
        //     ),

        //     //FLOW 1: INB_ORD -> PUR_INV
        //     array(
        //         'txn_flow_type' => TxnFlowType::$MAP['PURCHASE'],
        //         'flow_id' => 1,
        //         'proc_type' => ProcType::$MAP['ADV_SHIP_SYNC_01'],
        //         'step' => 1,
        //         'controller' => 'advShip',
        //         'action' => 'indexProcess',
        //         'scan_mode' => ScanMode::$MAP['NULL'],
        //         'fr_res_type' => ResType::$MAP['NULL'],
        //         'to_res_type' => ResType::$MAP['ADV_SHIP'],
        //         'to_doc_status' => DocStatus::$MAP['COMPLETE'],
        //         'created_at' => '1983-07-09 00:00:00',
        //         'updated_at' => '1983-07-09 00:00:00'
        //     ),
        //     array(
        //         'txn_flow_type' => TxnFlowType::$MAP['PURCHASE'],
        //         'flow_id' => 1,
        //         'proc_type' => ProcType::$MAP['INB_ORD_01'],
        //         'step' => 2,
        //         'controller' => 'inbOrd',
        //         'action' => 'indexProcess',
        //         'scan_mode' => ScanMode::$MAP['NULL'],
        //         'fr_res_type' => ResType::$MAP['ADV_SHIP'],
        //         'to_res_type' => ResType::$MAP['INB_ORD'],
        //         'to_doc_status' => DocStatus::$MAP['COMPLETE'],
        //         'created_at' => '1983-07-09 00:00:00',
        //         'updated_at' => '1983-07-09 00:00:00'
        //     ),
        //     array(
        //         'txn_flow_type' => TxnFlowType::$MAP['PURCHASE'],
        //         'flow_id' => 1,
        //         'proc_type' => ProcType::$MAP['PUR_INV_01'],
        //         'step' => 3,
        //         'controller' => 'purInv',
        //         'action' => 'indexProcess',
        //         'scan_mode' => ScanMode::$MAP['NULL'],
        //         'fr_res_type' => ResType::$MAP['INB_ORD'],
        //         'to_res_type' => ResType::$MAP['PUR_INV'],
        //         'to_doc_status' => DocStatus::$MAP['DRAFT'],
        //         'created_at' => '1983-07-09 00:00:00',
        //         'updated_at' => '1983-07-09 00:00:00'
        //     ),
        //     array(
        //         'txn_flow_type' => TxnFlowType::$MAP['PURCHASE'],
        //         'flow_id' => 1,
        //         'proc_type' => ProcType::$MAP['PUR_INV_01_01'],
        //         'step' => 4,
        //         'controller' => 'purInv',
        //         'action' => 'indexProcess',
        //         'scan_mode' => ScanMode::$MAP['NULL'],
        //         'fr_res_type' => ResType::$MAP['PUR_INV'],
        //         'to_res_type' => ResType::$MAP['NULL'],
        //         'to_doc_status' => DocStatus::$MAP['COMPLETE'],
        //         'created_at' => '1983-07-09 00:00:00',
        //         'updated_at' => '1983-07-09 00:00:00'
        //     ),

        //     //FLOW 1: SR -> IO -> RR
        //     array(
        //         'txn_flow_type' => TxnFlowType::$MAP['SALES_RETURN'],
        //         'flow_id' => 1,
        //         'proc_type' => ProcType::$MAP['SLS_RTN_SYNC_01'],
        //         'step' => 1,
        //         'controller' => 'salesOrd',
        //         'action' => 'indexProcess',
        //         'scan_mode' => ScanMode::$MAP['NULL'],
        //         'fr_res_type' => ResType::$MAP['NULL'],
        //         'to_res_type' => ResType::$MAP['SLS_ORD'],
        //         'to_doc_status' => DocStatus::$MAP['COMPLETE'],
        //         'created_at' => '1983-07-09 00:00:00',
        //         'updated_at' => '1983-07-09 00:00:00'
        //     ),
        //     array(
        //         'txn_flow_type' => TxnFlowType::$MAP['SALES_RETURN'],
        //         'flow_id' => 1,
        //         'proc_type' => ProcType::$MAP['INB_ORD_02'],
        //         'step' => 2,
        //         'controller' => 'inbOrd',
        //         'action' => 'indexProcess',
        //         'scan_mode' => ScanMode::$MAP['NULL'],
        //         'fr_res_type' => ResType::$MAP['SLS_RTN'],
        //         'to_res_type' => ResType::$MAP['INB_ORD'],
        //         'to_doc_status' => DocStatus::$MAP['COMPLETE'],
        //         'created_at' => '1983-07-09 00:00:00',
        //         'updated_at' => '1983-07-09 00:00:00'
        //     ),
        //     array(
        //         'txn_flow_type' => TxnFlowType::$MAP['SALES_RETURN'],
        //         'flow_id' => 1,
        //         'proc_type' => ProcType::$MAP['RTN_RCPT_01'], //inb ord -> rtn rcpt
        //         'step' => 3,
        //         'controller' => 'rtnRcpt',
        //         'action' => 'indexProcess',
        //         'scan_mode' => ScanMode::$MAP['NULL'],
        //         'fr_res_type' => ResType::$MAP['INB_ORD'],
        //         'to_res_type' => ResType::$MAP['RTN_RCPT'],
        //         'to_doc_status' => DocStatus::$MAP['COMPLETE'],
        //         'created_at' => '1983-07-09 00:00:00',
        //         'updated_at' => '1983-07-09 00:00:00'
        //     ),
        //     array(
        //         'txn_flow_type' => TxnFlowType::$MAP['SALES_RETURN'],
        //         'flow_id' => 1,
        //         'proc_type' => ProcType::$MAP['RTN_RCPT_SYNC_01'], //rtn rcpt sync
        //         'step' => 4,
        //         'controller' => 'rtnRcpt',
        //         'action' => 'indexProcess',
        //         'scan_mode' => ScanMode::$MAP['NULL'],
        //         'fr_res_type' => ResType::$MAP['RTN_RCPT'],
        //         'to_res_type' => ResType::$MAP['NULL'],
        //         'to_doc_status' => DocStatus::$MAP['COMPLETE'],
        //         'created_at' => '1983-07-09 00:00:00',
        //         'updated_at' => '1983-07-09 00:00:00'
        //     ),
        // ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_inv_txn_flows');
    }
}
