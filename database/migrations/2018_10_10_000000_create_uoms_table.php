<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uoms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 35)->unique();
            $table->string('ref_code_01', 35);
            $table->string('desc_01');
            $table->string('desc_02');

            $table->timestamps();
        });

        DB::table('uoms')->insert(array(
            array(
                'id' => 1,
                'code' => 'PALLET',
                'ref_code_01' => '',
                'desc_01' => 'PALLET',
                'desc_02' => '',
                'created_at' => '1983-07-09 00:00:00',
                'updated_at' => '1983-07-09 00:00:00'
            ),
            array(
                'id' => 2,
                'code' => 'CASE',
                'ref_code_01' => '',
                'desc_01' => 'CASE',
                'desc_02' => '',
                'created_at' => '1983-07-09 00:00:00',
                'updated_at' => '1983-07-09 00:00:00'
            ),
            array(
                'id' => 3,
                'code' => 'UNIT',
                'ref_code_01' => '',
                'desc_01' => 'UNIT',
                'desc_02' => '',
                'created_at' => '1983-07-09 00:00:00',
                'updated_at' => '1983-07-09 00:00:00'
            )
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uoms');
    }
}
