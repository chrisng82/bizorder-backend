<?php

use Illuminate\Database\Seeder;
use App\ItemGroup01Division;
use Carbon\Carbon;

class ItemGroup01DivisionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $divisions = DB::select('select id from divisions');

        foreach($divisions as $division) {
            $items = DB::select('
                select distinct item_group01s.*, item_divisions.division_id 
                from items left join item_group01s on items.item_group_01_id = item_group01s.id 
                left join item_divisions on items.id = item_divisions.item_id 
                where item_group01s.id is not null 
                and item_divisions.division_id = '.$division->id.' 
            ');
            
            $loop = 1;
            foreach($items as $item) {
                DB::table('item_group01_divisions')->insert([
                    'item_group01_id' => $item->id,
                    'division_id' => $item->division_id,
                    'ref_code_01' => '',
                    'ref_code_02' => '',
                    'sequence' => $loop,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString()
                ]);
                $loop++;
            }
        }
    }
}
