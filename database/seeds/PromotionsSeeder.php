<?php

use App\PromoHdr;
use App\PromoDtl;
use App\PromoDebtor;
use App\PromoDivision;
use App\Services\Env\DocStatus;
use Illuminate\Database\Seeder;

class PromotionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $promoHdr = new PromoHdr;
        $promoHdr->code = '00001';
        $promoHdr->desc_01 = 'TEST PROMO 1';
        $promoHdr->desc_02 = '';
        $promoHdr->valid_from = '2019-01-01';
        $promoHdr->valid_to = '2020-01-01';
        $promoHdr->doc_status = DocStatus::$MAP['COMPLETE'];
        $promoHdr->save();

        //start of promoDtl
        $promoDtl = new PromoDtl;
        $promoDtl->hdr_id = $promoHdr->id;
        $promoDtl->valid_from = '2019-01-01';
        $promoDtl->valid_to = '2020-01-01';
        $promoDtl->doc_status = DocStatus::$MAP['COMPLETE'];
        $promoDtl->level_no = 1;
        $promoDtl->line_no = 1;
        $promoDtl->group_no = 1;
        $promoDtl->group_01_id = 3;
        $promoDtl->group_02_id = 0;
        $promoDtl->group_03_id = 0;
        $promoDtl->group_04_id = 0;
        $promoDtl->group_05_id = 0;
        $promoDtl->item_id = 0;
        $promoDtl->desc_01 = 'BUY 200 RED BULL FOC 24 POKKA';
        $promoDtl->desc_02 = '';
        $promoDtl->min_net_amt = 0;
        $promoDtl->min_unit_qty = 200;
        $promoDtl->is_package = 0;

        $promoDtl->is_foc_multiple = 0;
        $promoDtl->foc_uom_id = 0;
        $promoDtl->foc_uom_rate = 0;
        $promoDtl->foc_qty = 0;
        $promoDtl->foc_ratio = 0.12;

        $promoDtl->price_disc = 0;
            
        $promoDtl->min_disc_val_01 = 0;
        $promoDtl->max_disc_val_01 = 0;
        $promoDtl->min_disc_val_02 = 0;
        $promoDtl->max_disc_val_02 = 0;
        $promoDtl->min_disc_val_03 = 0;
        $promoDtl->max_disc_val_03 = 0;
        $promoDtl->min_disc_val_04 = 0;
        $promoDtl->max_disc_val_04 = 0;
        $promoDtl->min_disc_val_05 = 0;
        $promoDtl->max_disc_val_05 = 0;

        $promoDtl->min_disc_perc_01 = 0;
        $promoDtl->max_disc_perc_01 = 0;
        $promoDtl->min_disc_perc_02 = 0;     
        $promoDtl->max_disc_perc_02 = 0;
        $promoDtl->min_disc_perc_03 = 0;          
        $promoDtl->max_disc_perc_03 = 0;
        $promoDtl->min_disc_perc_04 = 0;          
        $promoDtl->max_disc_perc_04 = 0;
        $promoDtl->min_disc_perc_05 = 0;
        $promoDtl->max_disc_perc_05 = 0;
        $promoDtl->save();

        $promoDtl = new PromoDtl;
        $promoDtl->hdr_id = $promoHdr->id;
        $promoDtl->valid_from = '2019-01-01';
        $promoDtl->valid_to = '2020-01-01';
        $promoDtl->doc_status = DocStatus::$MAP['COMPLETE'];
        $promoDtl->level_no = 1;
        $promoDtl->line_no = 3;
        $promoDtl->group_no = 0;
        $promoDtl->group_01_id = 4;
        $promoDtl->group_02_id = 0;
        $promoDtl->group_03_id = 0;
        $promoDtl->group_04_id = 0;
        $promoDtl->group_05_id = 0;
        $promoDtl->item_id = 0;
        $promoDtl->desc_01 = 'BUY 200 RED BULL FOC 24 POKKA';
        $promoDtl->desc_02 = '';
        $promoDtl->min_net_amt = 0;
        $promoDtl->min_unit_qty = 0;
        $promoDtl->is_package = 0;

        $promoDtl->is_foc_multiple = 1;
        $promoDtl->foc_uom_id = 2;
        $promoDtl->foc_uom_rate = 24;
        $promoDtl->foc_qty = 1;
        $promoDtl->foc_ratio = 0.12;

        $promoDtl->price_disc = 0;
            
        $promoDtl->min_disc_val_01 = 0;
        $promoDtl->max_disc_val_01 = 0;
        $promoDtl->min_disc_val_02 = 0;
        $promoDtl->max_disc_val_02 = 0;
        $promoDtl->min_disc_val_03 = 0;
        $promoDtl->max_disc_val_03 = 0;
        $promoDtl->min_disc_val_04 = 0;
        $promoDtl->max_disc_val_04 = 0;
        $promoDtl->min_disc_val_05 = 0;
        $promoDtl->max_disc_val_05 = 0;

        $promoDtl->min_disc_perc_01 = 0;
        $promoDtl->max_disc_perc_01 = 0;
        $promoDtl->min_disc_perc_02 = 0;     
        $promoDtl->max_disc_perc_02 = 0;
        $promoDtl->min_disc_perc_03 = 0;          
        $promoDtl->max_disc_perc_03 = 0;
        $promoDtl->min_disc_perc_04 = 0;          
        $promoDtl->max_disc_perc_04 = 0;
        $promoDtl->min_disc_perc_05 = 0;
        $promoDtl->max_disc_perc_05 = 0;
        $promoDtl->save();
        //end of promoDtl

        //start of promoDebtor
        $promoDebtor = new PromoDebtor;
        $promoDebtor->hdr_id = $promoHdr->id;
        $promoDebtor->debtor_id = 1;
        $promoDebtor->group_01_id = 0;
        $promoDebtor->group_02_id = 0;
        $promoDebtor->group_03_id = 0;
        $promoDebtor->group_04_id = 0;
        $promoDebtor->group_05_id = 0;
        $promoDebtor->save();
        //end of promoDebtor

        //start of promoDivision
        $promoDivision = new PromoDivision;
        $promoDivision->hdr_id = $promoHdr->id;
        $promoDivision->division_id = 1;
        $promoDivision->save();
        //end of promoDivision

        $promoHdr = new PromoHdr;
        $promoHdr->code = '00002';
        $promoHdr->desc_01 = 'TEST PROMO 2';
        $promoHdr->desc_02 = '';
        $promoHdr->valid_from = '2019-01-01';
        $promoHdr->valid_to = '2020-01-01';
        $promoHdr->doc_status = DocStatus::$MAP['COMPLETE'];
        $promoHdr->save();

        //start of promoDtl
        $promoDtl = new PromoDtl;
        $promoDtl->hdr_id = $promoHdr->id;
        $promoDtl->valid_from = '2019-01-01';
        $promoDtl->valid_to = '2020-01-01';
        $promoDtl->doc_status = DocStatus::$MAP['COMPLETE'];
        $promoDtl->level_no = 1;
        $promoDtl->line_no = 1;
        $promoDtl->group_no = 2;
        $promoDtl->group_01_id = 2;
        $promoDtl->group_02_id = 0;
        $promoDtl->group_03_id = 0;
        $promoDtl->group_04_id = 0;
        $promoDtl->group_05_id = 0;
        $promoDtl->item_id = 0;
        $promoDtl->desc_01 = 'BUY 2 CTN BLEACH @ RM100';
        $promoDtl->desc_02 = '';
        $promoDtl->min_net_amt = 0;
        $promoDtl->min_unit_qty = 40;
        $promoDtl->is_package = 1;

        $promoDtl->is_foc_multiple = 0;
        $promoDtl->foc_uom_id = 0;
        $promoDtl->foc_uom_rate = 0;
        $promoDtl->foc_qty = 0;
        $promoDtl->foc_ratio = 0;

        $promoDtl->price_disc = 16.96;
            
        $promoDtl->min_disc_val_01 = 0;
        $promoDtl->max_disc_val_01 = 0;
        $promoDtl->min_disc_val_02 = 0;
        $promoDtl->max_disc_val_02 = 0;
        $promoDtl->min_disc_val_03 = 0;
        $promoDtl->max_disc_val_03 = 0;
        $promoDtl->min_disc_val_04 = 0;
        $promoDtl->max_disc_val_04 = 0;
        $promoDtl->min_disc_val_05 = 0;
        $promoDtl->max_disc_val_05 = 0;

        $promoDtl->min_disc_perc_01 = 0;
        $promoDtl->max_disc_perc_01 = 0;
        $promoDtl->min_disc_perc_02 = 0;     
        $promoDtl->max_disc_perc_02 = 0;
        $promoDtl->min_disc_perc_03 = 0;          
        $promoDtl->max_disc_perc_03 = 0;
        $promoDtl->min_disc_perc_04 = 0;          
        $promoDtl->max_disc_perc_04 = 0;
        $promoDtl->min_disc_perc_05 = 0;
        $promoDtl->max_disc_perc_05 = 0;
        $promoDtl->save();
        //end of promoDtl

        //start of promoDebtor
        $promoDebtor = new PromoDebtor;
        $promoDebtor->hdr_id = $promoHdr->id;
        $promoDebtor->debtor_id = 1;
        $promoDebtor->group_01_id = 0;
        $promoDebtor->group_02_id = 0;
        $promoDebtor->group_03_id = 0;
        $promoDebtor->group_04_id = 0;
        $promoDebtor->group_05_id = 0;
        $promoDebtor->save();
        //end of promoDebtor

        //start of promoDivision
        $promoDivision = new PromoDivision;
        $promoDivision->hdr_id = $promoHdr->id;
        $promoDivision->division_id = 1;
        $promoDivision->save();
        //end of promoDivision

        $promoHdr = new PromoHdr;
        $promoHdr->code = '00003';
        $promoHdr->desc_01 = 'TEST PROMO 3';
        $promoHdr->desc_02 = '';
        $promoHdr->valid_from = '2019-01-01';
        $promoHdr->valid_to = '2020-01-01';
        $promoHdr->doc_status = DocStatus::$MAP['COMPLETE'];
        $promoHdr->save();

        //start of promoDtl
        $promoDtl = new PromoDtl;
        $promoDtl->hdr_id = $promoHdr->id;
        $promoDtl->valid_from = '2019-01-01';
        $promoDtl->valid_to = '2020-01-01';
        $promoDtl->doc_status = DocStatus::$MAP['COMPLETE'];
        $promoDtl->level_no = 1;
        $promoDtl->line_no = 1;
        $promoDtl->group_no = 1;
        $promoDtl->group_01_id = 1;
        $promoDtl->group_02_id = 0;
        $promoDtl->group_03_id = 0;
        $promoDtl->group_04_id = 0;
        $promoDtl->group_05_id = 0;
        $promoDtl->item_id = 0;
        $promoDtl->desc_01 = 'BUY 10 CTN FOC 1 CTN';
        $promoDtl->desc_02 = '';
        $promoDtl->min_net_amt = 0;
        $promoDtl->min_unit_qty = 240;
        $promoDtl->is_package = 0;

        $promoDtl->is_foc_multiple = 0;
        $promoDtl->foc_uom_id = 0;
        $promoDtl->foc_uom_rate = 0;
        $promoDtl->foc_qty = 0;
        $promoDtl->foc_ratio = 0.1;

        $promoDtl->price_disc = 0;
            
        $promoDtl->min_disc_val_01 = 0;
        $promoDtl->max_disc_val_01 = 0;
        $promoDtl->min_disc_val_02 = 0;
        $promoDtl->max_disc_val_02 = 0;
        $promoDtl->min_disc_val_03 = 0;
        $promoDtl->max_disc_val_03 = 0;
        $promoDtl->min_disc_val_04 = 0;
        $promoDtl->max_disc_val_04 = 0;
        $promoDtl->min_disc_val_05 = 0;
        $promoDtl->max_disc_val_05 = 0;

        $promoDtl->min_disc_perc_01 = 0;
        $promoDtl->max_disc_perc_01 = 0;
        $promoDtl->min_disc_perc_02 = 0;     
        $promoDtl->max_disc_perc_02 = 0;
        $promoDtl->min_disc_perc_03 = 0;          
        $promoDtl->max_disc_perc_03 = 0;
        $promoDtl->min_disc_perc_04 = 0;          
        $promoDtl->max_disc_perc_04 = 0;
        $promoDtl->min_disc_perc_05 = 0;
        $promoDtl->max_disc_perc_05 = 0;
        $promoDtl->save();

        $promoDtl = new PromoDtl;
        $promoDtl->hdr_id = $promoHdr->id;
        $promoDtl->valid_from = '2019-01-01';
        $promoDtl->valid_to = '2020-01-01';
        $promoDtl->doc_status = DocStatus::$MAP['COMPLETE'];
        $promoDtl->level_no = 1;
        $promoDtl->line_no = 1;
        $promoDtl->group_no = 2;
        $promoDtl->group_01_id = 1;
        $promoDtl->group_02_id = 0;
        $promoDtl->group_03_id = 0;
        $promoDtl->group_04_id = 0;
        $promoDtl->group_05_id = 0;
        $promoDtl->item_id = 0;
        $promoDtl->desc_01 = 'BUY 10 CTN FOC 1 CTN';
        $promoDtl->desc_02 = '';
        $promoDtl->min_net_amt = 0;
        $promoDtl->min_unit_qty = 0;
        $promoDtl->is_package = 0;

        $promoDtl->is_foc_multiple = 1;
        $promoDtl->foc_uom_id = 2;
        $promoDtl->foc_uom_rate = 24;
        $promoDtl->foc_qty = 1;
        $promoDtl->foc_ratio = 0.1;

        $promoDtl->price_disc = 0;
            
        $promoDtl->min_disc_val_01 = 0;
        $promoDtl->max_disc_val_01 = 0;
        $promoDtl->min_disc_val_02 = 0;
        $promoDtl->max_disc_val_02 = 0;
        $promoDtl->min_disc_val_03 = 0;
        $promoDtl->max_disc_val_03 = 0;
        $promoDtl->min_disc_val_04 = 0;
        $promoDtl->max_disc_val_04 = 0;
        $promoDtl->min_disc_val_05 = 0;
        $promoDtl->max_disc_val_05 = 0;

        $promoDtl->min_disc_perc_01 = 0;
        $promoDtl->max_disc_perc_01 = 0;
        $promoDtl->min_disc_perc_02 = 0;     
        $promoDtl->max_disc_perc_02 = 0;
        $promoDtl->min_disc_perc_03 = 0;          
        $promoDtl->max_disc_perc_03 = 0;
        $promoDtl->min_disc_perc_04 = 0;          
        $promoDtl->max_disc_perc_04 = 0;
        $promoDtl->min_disc_perc_05 = 0;
        $promoDtl->max_disc_perc_05 = 0;
        $promoDtl->save();
        //end of promoDtl

        //start of promoDebtor
        $promoDebtor = new PromoDebtor;
        $promoDebtor->hdr_id = $promoHdr->id;
        $promoDebtor->debtor_id = 1;
        $promoDebtor->group_01_id = 0;
        $promoDebtor->group_02_id = 0;
        $promoDebtor->group_03_id = 0;
        $promoDebtor->group_04_id = 0;
        $promoDebtor->group_05_id = 0;
        $promoDebtor->save();
        //end of promoDebtor

        //start of promoDivision
        $promoDivision = new PromoDivision;
        $promoDivision->hdr_id = $promoHdr->id;
        $promoDivision->division_id = 1;
        $promoDivision->save();
        //end of promoDivision
    }
}
