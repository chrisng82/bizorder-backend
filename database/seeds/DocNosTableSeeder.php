<?php

use App\DocNo;
use Illuminate\Support\Carbon;

use Illuminate\Database\Seeder;

class DocNosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //SYSTEM DEFAULT DOC NO
        $docNo = new DocNo;
        $docNo->doc_type = 'App\CartHdr';
        $docNo->doc_prefix = 'BIZC-';
        $docNo->running_no = 1;
        $docNo->running_no_length = 6;
        $docNo->doc_suffix = '';
        $docNo->is_enforce = 0;
        $docNo->save();
        
        $docNo = new DocNo;
        $docNo->doc_type = 'App\SlsOrdHdr';
        $docNo->doc_prefix = 'BIZS-';
        $docNo->running_no = 1;
        $docNo->running_no_length = 6;
        $docNo->doc_suffix = '';
        $docNo->is_enforce = 0;
        $docNo->save();
    }
}
