<?php

use App\Site;
use App\SiteFlow;
use App\WhseTxnFlow;
use App\SWhseTxnFlow;
use App\DocNo;
use App\SiteDocNo;
use App\Services\Env\TxnFlowType;
use Illuminate\Database\Seeder;

class SitesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        $site = new Site();
        $site->code = 'KL_WAREHOUSE';
        $site->ref_code_01 = '';
        $site->desc_01 = 'KL WAREHOUSE';
        $site->desc_02 = '';
        $site->max_floor = 1;
        $site->layout_width = 1000;
        $site->layout_depth = 1000;
        $site->layout_height = 1000;
        $site->latitude = 0;
        $site->longitude = 0;
        
        $site->save();
        */

        $site = new Site();
        $site->code = 'DEFAULT';
        $site->ref_code_01 = '';
        $site->desc_01 = 'DEFAULT';
        $site->desc_02 = '';
        $site->max_floor = 1;
        $site->layout_width = 1000;
        $site->layout_depth = 1000;
        $site->layout_height = 1000;
        $site->latitude = 0;
        $site->longitude = 0;
        $site->save();

        //START siteFlow
        $siteFlow = new SiteFlow();
        $siteFlow->site_id = $site->id;
        $siteFlow->code = 'DEFAULT';
        $siteFlow->ref_code_01 = '';
        $siteFlow->desc_01 = 'DEFAULT';
        $siteFlow->desc_02 = '';
        $siteFlow->save();

        //create a normal whse flow for this site
        $sWhseTxnFlows = SWhseTxnFlow::where(array(
            //'txn_flow_type' => TxnFlowType::$MAP['OUTB_DEL'],
            'flow_id' => 1
        ))->get();
        foreach($sWhseTxnFlows as $sWhseTxnFlow)
        {
            $whseTxnFlow = new WhseTxnFlow();
            $whseTxnFlow->site_flow_id = $siteFlow->id;
            $whseTxnFlow->txn_flow_type = $sWhseTxnFlow->txn_flow_type;
            $whseTxnFlow->proc_type = $sWhseTxnFlow->proc_type;
            $whseTxnFlow->step = $sWhseTxnFlow->step;
            $whseTxnFlow->controller = $sWhseTxnFlow->controller;
            $whseTxnFlow->action = $sWhseTxnFlow->action;
            $whseTxnFlow->scan_mode = $sWhseTxnFlow->scan_mode;
            $whseTxnFlow->fr_res_type = $sWhseTxnFlow->fr_res_type;
            $whseTxnFlow->to_res_type = $sWhseTxnFlow->to_res_type;
            $whseTxnFlow->to_doc_status = $sWhseTxnFlow->to_doc_status;
            $whseTxnFlow->save();
        }

        //START Cart
        $docNo = new DocNo();
        $docNo->doc_type = 'App\\CartHdr';
        $docNo->doc_prefix = 'CART';
        $docNo->running_no = 1;
        $docNo->running_no_length = 6;
        $docNo->doc_suffix = '';
        $docNo->is_enforce = 0;
        $docNo->save();

        $siteDocNo = new SiteDocNo();
        $siteDocNo->site_id = $site->id;
        $siteDocNo->doc_no_type = 'App\\CartHdr';
        $siteDocNo->doc_no_id = $docNo->id;
        $siteDocNo->seq_no = 1;
        $siteDocNo->save();
        //END Cart

        
        //START Cart
        $docNo = new DocNo();
        $docNo->doc_type = 'App\\CartHdr';
        $docNo->doc_prefix = 'C';
        $docNo->running_no = 1;
        $docNo->running_no_length = 6;
        $docNo->doc_suffix = '';
        $docNo->is_enforce = 0;
        $docNo->save();

        $siteDocNo = new SiteDocNo();
        $siteDocNo->site_id = $site->id;
        $siteDocNo->doc_no_type = 'App\\CartHdr';
        $siteDocNo->doc_no_id = $docNo->id;
        $siteDocNo->seq_no = 1;
        $siteDocNo->save();
        //END Cart

        
        //START Sls Order
        $docNo = new DocNo();
        $docNo->doc_type = 'App\\SlsOrdHdr';
        $docNo->doc_prefix = 'S';
        $docNo->running_no = 1;
        $docNo->running_no_length = 6;
        $docNo->doc_suffix = '';
        $docNo->is_enforce = 0;
        $docNo->save();

        $siteDocNo = new SiteDocNo();
        $siteDocNo->site_id = $site->id;
        $siteDocNo->doc_no_type = 'App\\SlsOrdHdr';
        $siteDocNo->doc_no_id = $docNo->id;
        $siteDocNo->seq_no = 1;
        $siteDocNo->save();
        //END Sls Order

    }
}
