<?php

use App\Currency;
use Illuminate\Database\Seeder;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currency = new Currency;
        $currency->code = 'RM';
        $currency->symbol = 'RM';
        $currency->currency_rate = 1;
        $currency->save();
    }
}
