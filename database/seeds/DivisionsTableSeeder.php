<?php

use App\DocNo;
use App\DivisionDocNo;
use App\SInvTxnFlow;
use App\InvTxnFlow;
use App\Division;
use App\SyncSettingHdr;
use App\SyncSettingDtl;
use App\Services\Env\ProcType;
use Illuminate\Support\Carbon;

use Illuminate\Database\Seeder;

class DivisionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //SYSTEM DEFAULT DIVISION
        $division = new Division;
        $division->site_id = 1;
        $division->site_flow_id = 1;
        $division->company_id = 1;
        $division->code = 'ADMIN';
        $division->ref_code_01 = 'ADMIN';
        $division->name_01 = 'ADMIN';
        $division->name_02 = '';
        $division->save();

        // $divisions = array();
        // //KK GEN
        // $division = new Division;
        // $division->site_id = 1;
        // $division->site_flow_id = 1;
        // $division->company_id = 1;
        // $division->code = 'KK GEN';
        // $division->ref_code_01 = 'E';
        // $division->name_01 = 'KK GENERAL';
        // $division->name_02 = '';
        // $division->save();
        // $divisions[] = $division;
        
        // //KK DLM
        // $division = new Division;
        // $division->site_id = 1;
        // $division->site_flow_id = 1;
        // $division->company_id = 1;
        // $division->code = 'KK DLM';
        // $division->ref_code_01 = 'D';
        // $division->name_01 = 'KK DUTCH LADY';
        // $division->name_02 = '';
        // $division->save();
        // $divisions[] = $division;

        // //KK GBA
        // $division = new Division;
        // $division->site_id = 1;
        // $division->site_flow_id = 1;
        // $division->company_id = 1;
        // $division->code = 'KK GBA';
        // $division->ref_code_01 = 'G';
        // $division->name_01 = 'KK GBA';
        // $division->name_02 = '';
        // $division->save();
        // $divisions[] = $division;

        // //KK YEOS
        // $division = new Division;
        // $division->site_id = 1;
        // $division->site_flow_id = 1;
        // $division->company_id = 1;
        // $division->code = 'KK YHS';
        // $division->ref_code_01 = 'Y';
        // $division->name_01 = 'KK YEOS';
        // $division->name_02 = '';
        // $division->save();
        // $divisions[] = $division;
        
        // foreach($divisions as $division)
        // {
        //     //TLS sync SO, 1 site_flow_id, 0 division_id, 1 url
        //     //LKC sync SO, 1 site_flow_id, m division_id, m url
        //     //YEOS sync SO, 1 site_flow_id, m division_id, m url, m dtls
            
        //     //create sync setting hdr SLS_ORD_SYNC_01
        //     $syncSettingHdr = new SyncSettingHdr();
        //     $syncSettingHdr->proc_type = ProcType::$MAP['SYNC_EFICHAIN_01'];
        //     $syncSettingHdr->site_flow_id = 1;
        //     $syncSettingHdr->division_id = $division->id;
        //     $syncSettingHdr->url = 'http://tlsm.dyndns.biz:8000';
        //     $syncSettingHdr->page_size = 1000;
        //     $syncSettingHdr->last_total = 0;
        //     $syncSettingHdr->last_synced_at = Carbon::createFromTimestamp(0);
        //     $syncSettingHdr->last_user_id = 0;
        //     $syncSettingHdr->save();

        //     $syncSettingDtl = new SyncSettingDtl();
        //     $syncSettingDtl->hdr_id = $syncSettingHdr->id;
        //     $syncSettingDtl->field = 'username';
        //     $syncSettingDtl->value = 'admin';
        //     $syncSettingDtl->save();

        //     $syncSettingDtl = new SyncSettingDtl();
        //     $syncSettingDtl->hdr_id = $syncSettingHdr->id;
        //     $syncSettingDtl->field = 'password';
        //     $syncSettingDtl->value = 'm3rch$@tls.com';
        //     $syncSettingDtl->save();

        //     //create a normal whse flow for this site
        //     $sInvTxnFlows = SInvTxnFlow::where(array(
        //         //'txn_flow_type' => TxnFlowType::$MAP['OUTB_DEL'],
        //         'flow_id' => 1
        //     ))->get();
        //     foreach($sInvTxnFlows as $sInvTxnFlow)
        //     {
        //         $invTxnFlow = new InvTxnFlow();
        //         $invTxnFlow->division_id = $division->id;
        //         $invTxnFlow->txn_flow_type = $sInvTxnFlow->txn_flow_type;
        //         $invTxnFlow->proc_type = $sInvTxnFlow->proc_type;
        //         $invTxnFlow->step = $sInvTxnFlow->step;
        //         $invTxnFlow->controller = $sInvTxnFlow->controller;
        //         $invTxnFlow->action = $sInvTxnFlow->action;
        //         $invTxnFlow->scan_mode = $sInvTxnFlow->scan_mode;
        //         $invTxnFlow->fr_res_type = $sInvTxnFlow->fr_res_type;
        //         $invTxnFlow->to_res_type = $sInvTxnFlow->to_res_type;
        //         $invTxnFlow->to_doc_status = $sInvTxnFlow->to_doc_status;
        //         $invTxnFlow->save();
        //     }

        //     //START OutbOrd
        //     $docNo = new DocNo;
        //     $docNo->doc_type = \App\OutbOrdHdr::class;
        //     $docNo->doc_prefix = 'OU'.$division->ref_code_01;
        //     $docNo->running_no = 1;
        //     $docNo->running_no_length = 6;
        //     $docNo->doc_suffix = '';
        //     $docNo->is_enforce = 0;
        //     $docNo->save();

        //     $divisionDocNo = new DivisionDocNo;
        //     $divisionDocNo->division_id = $division->id;
        //     $divisionDocNo->doc_no_type = \App\OutbOrdHdr::class;
        //     $divisionDocNo->doc_no_id = $docNo->id;
        //     $divisionDocNo->seq_no = 1;
        //     $divisionDocNo->save();
        //     //END OutbOrd

        //     //START SlsInv
        //     $docNo = new DocNo;
        //     $docNo->doc_type = \App\SlsInvHdr::class;
        //     $docNo->doc_prefix = 'SI'.$division->ref_code_01;
        //     $docNo->running_no = 1;
        //     $docNo->running_no_length = 6;
        //     $docNo->doc_suffix = '';
        //     $docNo->is_enforce = 0;
        //     $docNo->save();

        //     $divisionDocNo = new DivisionDocNo;
        //     $divisionDocNo->division_id = $division->id;
        //     $divisionDocNo->doc_no_type = \App\SlsInvHdr::class;
        //     $divisionDocNo->doc_no_id = $docNo->id;
        //     $divisionDocNo->seq_no = 1;
        //     $divisionDocNo->save();
        //     //END SlsInv

        //     //START InbOrd
        //     $docNo = new DocNo;
        //     $docNo->doc_type = \App\InbOrdHdr::class;
        //     $docNo->doc_prefix = 'IN'.$division->ref_code_01;
        //     $docNo->running_no = 1;
        //     $docNo->running_no_length = 6;
        //     $docNo->doc_suffix = '';
        //     $docNo->is_enforce = 0;
        //     $docNo->save();

        //     $divisionDocNo = new DivisionDocNo;
        //     $divisionDocNo->division_id = $division->id;
        //     $divisionDocNo->doc_no_type = \App\InbOrdHdr::class;
        //     $divisionDocNo->doc_no_id = $docNo->id;
        //     $divisionDocNo->seq_no = 1;
        //     $divisionDocNo->save();
        //     //END InbOrd

        //     //START AdvShip
        //     $docNo = new DocNo;
        //     $docNo->doc_type = \App\AdvShipHdr::class;
        //     $docNo->doc_prefix = 'AS'.$division->ref_code_01;
        //     $docNo->running_no = 1;
        //     $docNo->running_no_length = 6;
        //     $docNo->doc_suffix = '';
        //     $docNo->is_enforce = 0;
        //     $docNo->save();

        //     $divisionDocNo = new DivisionDocNo;
        //     $divisionDocNo->division_id = $division->id;
        //     $divisionDocNo->doc_no_type = \App\AdvShipHdr::class;
        //     $divisionDocNo->doc_no_id = $docNo->id;
        //     $divisionDocNo->seq_no = 1;
        //     $divisionDocNo->save();
        //     //END AdvShip
        // }
    }
}
