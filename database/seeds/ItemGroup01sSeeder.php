<?php

use Illuminate\Database\Seeder;

class ItemGroup01sTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('item_group01s')->insert(array(
            array(
                'id' => 1,
                'code' => 'KAO_ATTACK',
                'desc_01' => 'KAO ATTACK',
                'desc_02' => '',
                'created_at' => '1980-01-01 00:00:00',
                'updated_at' => '1980-01-01 00:00:00'
            ),
            array(
                'id' => 2,
                'code' => 'KAO_BLEACH',
                'desc_01' => 'KAO BLEACH',
                'desc_02' => '',
                'created_at' => '1980-01-01 00:00:00',
                'updated_at' => '1980-01-01 00:00:00'
            ),
            array(
                'id' => 3,
                'code' => 'RED_BULL',
                'desc_01' => 'RED BULL',
                'desc_02' => '',
                'created_at' => '1980-01-01 00:00:00',
                'updated_at' => '1980-01-01 00:00:00'
            ),
            array(
                'id' => 4,
                'code' => 'POKKA',
                'desc_01' => 'POKKA',
                'desc_02' => '',
                'created_at' => '1980-01-01 00:00:00',
                'updated_at' => '1980-01-01 00:00:00'
            ),
            array(
                'id' => 5,
                'code' => 'PANADOL',
                'desc_01' => 'PANADOL',
                'desc_02' => '',
                'created_at' => '1980-01-01 00:00:00',
                'updated_at' => '1980-01-01 00:00:00'
            )
        ));
    }
}
