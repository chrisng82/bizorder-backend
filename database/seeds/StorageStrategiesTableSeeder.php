<?php

use App\StorageStrategy;
use Illuminate\Database\Seeder;

class StorageStrategiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $model = new StorageStrategy();
        $model->site_id = 1;
        $model->line_no = 1;
        $model->location_id = 1; //D06
        $model->storage_type_id = 0;
        $model->storage_row_id = 0;
        $model->storage_bay_id = 0;
        $model->storage_bin_id = 0;

        $model->storage_class = 0;
        $model->item_group_01_id = 0;
        $model->item_group_02_id = 0;
        $model->item_group_03_id = 0;
        $model->item_group_04_id = 0;
        $model->item_group_05_id = 0;
        $model->item_id = 0;

        $model->item_cond_01_id = 1; //GOOD
        $model->item_cond_02_id = 0;
        $model->item_cond_03_id = 0;
        $model->item_cond_04_id = 0;
        $model->item_cond_05_id = 0;

        $model->save();


        $model = new StorageStrategy();
        $model->site_id = 1;
        $model->line_no = 1;
        $model->location_id = 3; //BAD_STOCK
        $model->storage_type_id = 0;
        $model->storage_row_id = 0;
        $model->storage_bay_id = 0;
        $model->storage_bin_id = 0;

        $model->storage_class = 0;
        $model->item_group_01_id = 0;
        $model->item_group_02_id = 0;
        $model->item_group_03_id = 0;
        $model->item_group_04_id = 0;
        $model->item_group_05_id = 0;
        $model->item_id = 0;

        $model->item_cond_01_id = 2; //BAD
        $model->item_cond_02_id = 0;
        $model->item_cond_03_id = 0;
        $model->item_cond_04_id = 0;
        $model->item_cond_05_id = 0;

        $model->save();
    }
}
