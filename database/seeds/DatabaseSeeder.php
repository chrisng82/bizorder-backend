<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    protected $toTruncate = [
        //'users',
        //'outb_ord_hdrs',
        //'outb_ord_dtls',
        //'companies',
        //'sites',
        //'divisions',
        //'roles',
        //'role_user',
        //'permissions',
        //'permission_role',
        //'uoms',
        //'item_group01s',
        //'items',
        //'item_uoms',
        //'areas',
        //'debtors',
        //'delivery_points',
        //'batch_job_status',
    ];

    public function run()
    {
        // foreach ($this->toTruncate as $table) {
        //     DB::table($table)->truncate();
        // }
        $this->call(ItemGroup01DivisionsSeeder::class);
        // $this->call(CurrenciesTableSeeder::class);
        // $this->call(CompaniesTableSeeder::class);
        // $this->call(SitesTableSeeder::class);
        // $this->call(DivisionsTableSeeder::class);
        // $this->call(DocNosTableSeeder::class);
        // $this->call(UsersTableSeeder::class);
        // $this->call(StatesTableSeeder::class);
        // $this->call(AreasTableSeeder::class);
        //$this->call(MobilesTableSeeder::class);
        //$this->call(UomsTableSeeder::class);
        //$this->call(ItemGroup01sTableSeeder::class);
        //$this->call(ItemsTableSeeder::class);
        //$this->call(AreasTableSeeder::class);
        //$this->call(CreditorsTableSeeder::class);
        //$this->call(DebtorsTableSeeder::class);
        //$this->call(StorageBinsTableSeeder::class);
        //$this->call(OutbOrdsTableSeeder::class);
        //$this->call(InbOrdsTableSeeder::class);
        //$this->call(SlsInvsTableSeeder::class);
        //$this->call(PromotionsTableSeeder::class);
    }
}
