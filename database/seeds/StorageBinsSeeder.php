<?php

use App\ItemBatch;
use App\HandlingUnit;
use App\Location;
use App\StorageType;
use App\StorageRow;
use App\StorageBay;
use App\StorageBin;
use App\QuantBal;
use App\Services\Env\BinType;
use App\Services\Env\HandlingType;
use App\Services\Env\RackType;
use App\Services\Env\ResStatus;
use Illuminate\Database\Seeder;

class StorageBinsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    }
}
