<?php

use App\Services\Env\MobileOS;
use App\Services\Env\MobileApp;
use Illuminate\Database\Seeder;

class MobilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        DB::table('mobile_profiles')->insert(array(
            array(
                'id' => 1,
                'device_uuid' => 'ABCDEFG',
                'mobile_app' => MobileApp::$MAP['WHSE_WORKER'],
                'mobile_os' => MobileOS::$MAP['ANDROID'],
                'os_version' => '5.1.0',
                'app_version' => '0.0.1',
                'synced_at' => '1980-01-01 00:00:00',
                'created_at' => '1980-01-01 00:00:00',
                'updated_at' => '1980-01-01 00:00:00'
            )
        ));
        */
    }
}
