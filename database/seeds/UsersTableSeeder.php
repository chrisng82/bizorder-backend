<?php

use App\User;
use App\Permission;
use App\Role;
use App\UserSiteFlow;
use App\UserDivision;
use App\Division;
use App\Services\Env\ResStatus;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $divisions = Division::get();

        $user = new User;
        $user->username = 'admin';
        $user->email = 'admin@merchstack.com';
        $user->password = bcrypt('1234');
        $user->status = ResStatus::$MAP['ACTIVE'];
        $user->timezone = 'Asia/Singapore';
        $user->save();

        $user->assignRole('system_admin');

        foreach($divisions as $division)
        {
            //START userDivision
            $userDivision = new UserDivision;
            $userDivision->user_id = $user->id;
            $userDivision->division_id = $division->id;
            $userDivision->ref_code_01 = '';
            $userDivision->ref_code_02 = '';
            $userDivision->save();
            //END add userDivision
        }

        //START userSiteFlow
        $userSiteFlow = new UserSiteFlow();
        $userSiteFlow->user_id = $user->id;
        $userSiteFlow->site_flow_id = 1;
        $userSiteFlow->ref_code_01 = '';
        $userSiteFlow->ref_code_02 = '';
        $userSiteFlow->save();
        //END add userSiteFlow
    }
}
