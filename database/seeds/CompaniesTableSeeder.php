<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert(array(
            array(
                'id' => 1,
                'code' => 'ADMIN',
                'co_reg_no' => '',
                'tax_reg_no' => '',
                'name_01' => 'MERCHSTACK SOLUTIONS',
                'name_02' => '',
                'unit_no' => '-',
                'building_name' => '-',
                'street_name' => '-',
                'district_01' => '',
                'district_02' => '',
                'postcode' => '-',
                'state_name' => '',
                'country_name' => 'MALAYSIA',
                'attention' => '',
                'phone_01' => '',
                'phone_02' => '',
                'fax_01' => '',
                'fax_02' => '',
                'email_01' => '',
                'email_02' => '',
                'latitude' => 0,
                'longitude' => 0,

                'created_at' => '2000-01-01 00:00:00',
                'updated_at' => '2000-01-01 00:00:00'
            )
        ));
    }
}
