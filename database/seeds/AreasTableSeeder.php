<?php

// use Illuminate\Database\Seeder;
use JeroenZwart\CsvSeeder\CsvSeeder;

class AreasTableSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->file = '/database/seeds/csvs/areas.csv';
		$this->tablename = 'areas';
		$this->timestamps = '2000-01-01 00:00:00';
    }
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Recommended when importing larger CSVs
	    DB::disableQueryLog();
	    parent::run();
    }
}
