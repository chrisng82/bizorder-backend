<?php

use Illuminate\Database\Seeder;

class UomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('uoms')->insert(array(
            array(
                'id' => 4,
                'code' => 'STRIPE',
                'ref_code_01' => '',
                'desc_01' => 'STRIPE',
                'desc_02' => '',
                'created_at' => '1980-01-01 00:00:00',
                'updated_at' => '1980-01-01 00:00:00'
            ),
            array(
                'id' => 5,
                'code' => 'BOX',
                'ref_code_01' => '',
                'desc_01' => 'BOX',
                'desc_02' => '',
                'created_at' => '1980-01-01 00:00:00',
                'updated_at' => '1980-01-01 00:00:00'
            ),
            array(
                'id' => 6,
                'code' => 'PACK',
                'ref_code_01' => '',
                'desc_01' => 'PACK',
                'desc_02' => '',
                'created_at' => '1980-01-01 00:00:00',
                'updated_at' => '1980-01-01 00:00:00'
            )
        ));
    }
}
