<?php

use App\Debtor;
use App\DeliveryPoint;
use App\Services\Env\ResStatus;
use App\Services\Env\DeliveryPointType;
use Illuminate\Database\Seeder;

class DebtorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $debtor = new Debtor;
        $debtor->code = '300A/001';
        $debtor->ref_code_01 = '';
        $debtor->company_name_01 = 'INVERZE SDN BHD';
        $debtor->company_name_02 = '';
        $debtor->cmpy_register_no = '';
        $debtor->tax_register_no = '';
        $debtor->area_id = 1;
        $debtor->area_code = 'KUALA_LUMPUR';
        $debtor->unit_no = 'B-16-02';
        $debtor->building_name = 'KOMPLEKS RIMBUN SCOTT';
        $debtor->street_name = '289 JALAN KLANG LAMA';
        $debtor->district_01 = '';
        $debtor->district_02 = '';
        $debtor->postcode = '58000 KUALA LUMPUR';
        $debtor->state_name = '';
        $debtor->country_name = 'MALAYSIA';
        $debtor->attention = 'FOONG';
        $debtor->phone_01 = '';
        $debtor->phone_02 = '';
        $debtor->fax_01 = '';
        $debtor->fax_02 = '';
        $debtor->email_01 = '';
        $debtor->email_02 = '';
        $debtor->save();

        $deliveryPoint = new DeliveryPoint;
        $deliveryPoint->delivery_point_type = DeliveryPointType::$MAP['DEBTOR'];
        $deliveryPoint->debtor_id = $debtor->id;
        $deliveryPoint->price_group_id = 0;
        $deliveryPoint->code = '300A/001';
        $deliveryPoint->ref_code_01 = '';
        $deliveryPoint->company_name_01 = 'INVERZE SDN BHD';
        $deliveryPoint->company_name_02 = '';
        $deliveryPoint->area_id = 1;
        $deliveryPoint->area_code = 'KUALA_LUMPUR';
        $deliveryPoint->unit_no = 'B-16-02';
        $deliveryPoint->building_name = 'KOMPLEKS RIMBUN SCOTT';
        $deliveryPoint->street_name = '289 JALAN KLANG LAMA';
        $deliveryPoint->district_01 = '';
        $deliveryPoint->district_02 = '';
        $deliveryPoint->postcode = '58000 KUALA LUMPUR';
        $deliveryPoint->state_name = '';
        $deliveryPoint->country_name = 'MALAYSIA';
        $deliveryPoint->attention = 'FOONG';
        $deliveryPoint->phone_01 = '';
        $deliveryPoint->phone_02 = '';
        $deliveryPoint->fax_01 = '';
        $deliveryPoint->fax_02 = '';
        $deliveryPoint->email_01 = '';
        $deliveryPoint->email_02 = '';
        $deliveryPoint->status = ResStatus::$MAP['ACTIVE'];
        $deliveryPoint->save();
        
        $debtor = new Debtor;
        $debtor->code = '300A/002';
        $debtor->ref_code_01 = '';
        $debtor->company_name_01 = 'EFICHAIN SOLUTIONS SDN BHD';
        $debtor->company_name_02 = '';
        $debtor->cmpy_register_no = '';
        $debtor->tax_register_no = '';
        $debtor->area_id = 2;
        $debtor->area_code = 'BALAKONG';
        $debtor->unit_no = '46';
        $debtor->building_name = '';
        $debtor->street_name = 'JALAN KIRI LIMA';
        $debtor->district_01 = 'TMN TAMING JAYA';
        $debtor->district_02 = '';
        $debtor->postcode = '43300 SERI KEMBANGAN';
        $debtor->state_name = '';
        $debtor->country_name = 'MALAYSIA';
        $debtor->attention = 'FOONG';
        $debtor->phone_01 = '';
        $debtor->phone_02 = '';
        $debtor->fax_01 = '';
        $debtor->fax_02 = '';
        $debtor->email_01 = '';
        $debtor->email_02 = '';
        $debtor->save();
        
        $deliveryPoint = new DeliveryPoint;
        $deliveryPoint->delivery_point_type = DeliveryPointType::$MAP['DEBTOR'];
        $deliveryPoint->debtor_id = $debtor->id;
        $deliveryPoint->price_group_id = 0;
        $deliveryPoint->code = '300A/002';
        $deliveryPoint->ref_code_01 = '';
        $deliveryPoint->company_name_01 = 'EFICHAIN SOLUTIONS SDN BHD';
        $deliveryPoint->company_name_02 = '';
        $deliveryPoint->area_id = 2;
        $deliveryPoint->area_code = 'BALAKONG';
        $deliveryPoint->unit_no = '46';
        $deliveryPoint->building_name = '';
        $deliveryPoint->street_name = 'JALAN KIRI LIMA';
        $deliveryPoint->district_01 = 'TMN TAMING JAYA';
        $deliveryPoint->district_02 = '';
        $deliveryPoint->postcode = '43300 SERI KEMBANGAN';
        $deliveryPoint->state_name = '';
        $deliveryPoint->country_name = 'MALAYSIA';
        $deliveryPoint->attention = 'FOONG';
        $deliveryPoint->phone_01 = '';
        $deliveryPoint->phone_02 = '';
        $deliveryPoint->fax_01 = '';
        $deliveryPoint->fax_02 = '';
        $deliveryPoint->email_01 = '';
        $deliveryPoint->email_02 = '';
        $deliveryPoint->status = ResStatus::$MAP['ACTIVE'];
        $deliveryPoint->save();
    }
}
