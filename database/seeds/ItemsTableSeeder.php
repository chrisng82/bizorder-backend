<?php

use App\Uom;
use App\Item;
use App\ItemUom;
use App\ItemSalePrice;
use App\ItemPurPrice;
use App\Services\Env\StorageClass;
use App\Services\Env\ItemType;
use App\Services\Env\ResStatus;
use App\Services\Env\ScanMode;
use App\Services\Env\RetrievalMethod;
use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $item = new Item;
        $item->code = '288638';
        $item->ref_code_01 = '';
        $item->desc_01 = 'ATTACK ULTRA POWER 240Gx24';
        $item->desc_02 = '';
        $item->storage_class = StorageClass::$MAP['AMBIENT_01'];
        $item->item_group_01_id = 1; //KAO ATTACK
        $item->item_group_02_id = 0;
        $item->item_group_03_id = 0;
        $item->item_group_04_id = 0;
        $item->item_group_05_id = 0;
        $item->item_group_01_code = 'KAO_ATTACK';
        $item->item_group_02_code = '';
        $item->item_group_03_code = '';
        $item->item_group_04_code = '';
        $item->item_group_05_code = '';
        $item->s_storage_class = StorageClass::$MAP['AMBIENT_01'];
        $item->s_item_group_01_id = 1; //KAO ATTACK
        $item->s_item_group_02_id = 0;
        $item->s_item_group_03_id = 0;
        $item->s_item_group_04_id = 0;
        $item->s_item_group_05_id = 0;
        $item->s_item_group_01_code = 'KAO_ATTACK';
        $item->s_item_group_02_code = '';
        $item->s_item_group_03_code = '';
        $item->s_item_group_04_code = '';
        $item->s_item_group_05_code = '';
        $item->qty_scale = 0;
        $item->item_type = ItemType::$MAP['STOCK_ITEM'];
        $item->scan_mode = ScanMode::$MAP['ENTER_QTY'];
        $item->retrieval_method = RetrievalMethod::$MAP['FIRST_EXPIRED_FIRST_OUT'];
        $item->unit_uom_id = Uom::$UNIT;
        $item->case_uom_id = Uom::$CASE;
        $item->case_uom_rate = 24;
        $item->pallet_uom_rate = 0;
        $item->case_ext_length = 320;
        $item->case_ext_width = 230;
        $item->case_ext_height = 230;
        $item->case_gross_weight = 5.904;
        $item->cases_per_pallet_length = 0;
        $item->cases_per_pallet_width = 0;
        $item->no_of_layers = 0;
        $item->is_length_allowed_vertical = 0;
        $item->is_width_allowed_vertical = 0;
        $item->is_height_allowed_vertical = 1;
        $item->status = ResStatus::$MAP['ACTIVE'];
        $item->save();

        $itemUom = new ItemUom;
        $itemUom->item_id = $item->id;
        $itemUom->uom_id = Uom::$UNIT;
        $itemUom->barcode = '8851818163539';
        $itemUom->uom_rate = 1;
        $itemUom->save();

        $itemPrice = new ItemSalePrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = Uom::$UNIT;
        $itemPrice->sale_price = 2.79;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $itemPrice = new ItemPurPrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = Uom::$UNIT;
        $itemPrice->sale_price = 2.79;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $itemUom = new ItemUom;
        $itemUom->item_id = $item->id;
        $itemUom->uom_id = Uom::$CASE;
        $itemUom->barcode = '';
        $itemUom->uom_rate = 24;
        $itemUom->save();

        $itemPrice = new ItemSalePrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = Uom::$CASE;
        $itemPrice->sale_price = 66.96;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $itemPrice = new ItemPurPrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = Uom::$CASE;
        $itemPrice->sale_price = 66.96;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $item = new Item;
        $item->code = '294745';
        $item->ref_code_01 = '';
        $item->desc_01 = 'KAO BLEACH LEMON 600ml x 20';
        $item->desc_02 = '';
        $item->storage_class = StorageClass::$MAP['AMBIENT_01'];
        $item->item_group_01_id = 2; //KAO BLEACH
        $item->item_group_02_id = 0;
        $item->item_group_03_id = 0;
        $item->item_group_04_id = 0;
        $item->item_group_05_id = 0;
        $item->item_group_01_code = 'KAO_BLEACH';
        $item->item_group_02_code = '';
        $item->item_group_03_code = '';
        $item->item_group_04_code = '';
        $item->item_group_05_code = '';
        $item->s_storage_class = StorageClass::$MAP['AMBIENT_01'];
        $item->s_item_group_01_id = 2; //KAO BLEACH
        $item->s_item_group_02_id = 0;
        $item->s_item_group_03_id = 0;
        $item->s_item_group_04_id = 0;
        $item->s_item_group_05_id = 0;
        $item->s_item_group_01_code = 'KAO_BLEACH';
        $item->s_item_group_02_code = '';
        $item->s_item_group_03_code = '';
        $item->s_item_group_04_code = '';
        $item->s_item_group_05_code = '';
        $item->qty_scale = 0;
        $item->item_type = ItemType::$MAP['STOCK_ITEM'];
        $item->scan_mode = ScanMode::$MAP['ENTER_QTY'];
        $item->retrieval_method = RetrievalMethod::$MAP['FIRST_EXPIRED_FIRST_OUT'];
        $item->unit_uom_id = Uom::$UNIT;
        $item->case_uom_id = Uom::$CASE;
        $item->case_uom_rate = 20;
        $item->pallet_uom_rate = 0;
        $item->case_ext_length = 350;
        $item->case_ext_width = 320;
        $item->case_ext_height = 260;
        $item->case_gross_weight = 14.44;
        $item->cases_per_pallet_length = 0;
        $item->cases_per_pallet_width = 0;
        $item->no_of_layers = 0;
        $item->is_length_allowed_vertical = 0;
        $item->is_width_allowed_vertical = 0;
        $item->is_height_allowed_vertical = 1;
        $item->status = ResStatus::$MAP['ACTIVE'];
        $item->save();

        $itemUom = new ItemUom;
        $itemUom->item_id = $item->id;
        $itemUom->uom_id = Uom::$UNIT;
        $itemUom->barcode = '9556155210012';
        $itemUom->uom_rate = 1;
        $itemUom->save();

        $itemPrice = new ItemSalePrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = Uom::$UNIT;
        $itemPrice->sale_price = 3.10;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $itemPrice = new ItemPurPrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = Uom::$UNIT;
        $itemPrice->sale_price = 3.10;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $itemUom = new ItemUom;
        $itemUom->item_id = $item->id;
        $itemUom->uom_id = Uom::$CASE;
        $itemUom->barcode = '';
        $itemUom->uom_rate = 20;
        $itemUom->save();

        $itemPrice = new ItemSalePrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = Uom::$CASE;
        $itemPrice->sale_price = 62;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $itemPrice = new ItemPurPrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = Uom::$CASE;
        $itemPrice->sale_price = 62;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $item = new Item;
        $item->code = 'RBU00001';
        $item->ref_code_01 = '';
        $item->desc_01 = 'RED BULL GOLD CAN - 250ml x 24';
        $item->desc_02 = '';
        $item->storage_class = StorageClass::$MAP['AMBIENT_01'];
        $item->item_group_01_id = 3;
        $item->item_group_02_id = 0;
        $item->item_group_03_id = 0;
        $item->item_group_04_id = 0;
        $item->item_group_05_id = 0;
        $item->item_group_01_code = 'RED_BULL';
        $item->item_group_02_code = '';
        $item->item_group_03_code = '';
        $item->item_group_04_code = '';
        $item->item_group_05_code = '';
        $item->s_storage_class = StorageClass::$MAP['AMBIENT_01'];
        $item->s_item_group_01_id = 3; //RED_BULL
        $item->s_item_group_02_id = 0;
        $item->s_item_group_03_id = 0;
        $item->s_item_group_04_id = 0;
        $item->s_item_group_05_id = 0;
        $item->s_item_group_01_code = 'RED_BULL';
        $item->s_item_group_02_code = '';
        $item->s_item_group_03_code = '';
        $item->s_item_group_04_code = '';
        $item->s_item_group_05_code = '';
        $item->qty_scale = 0;
        $item->item_type = ItemType::$MAP['STOCK_ITEM'];
        $item->scan_mode = ScanMode::$MAP['ENTER_QTY'];
        $item->retrieval_method = RetrievalMethod::$MAP['FIRST_EXPIRED_FIRST_OUT'];
        $item->unit_uom_id = Uom::$UNIT;
        $item->case_uom_id = Uom::$CASE;
        $item->case_uom_rate = 24;
        $item->pallet_uom_rate = 0;
        $item->case_ext_length = 400;
        $item->case_ext_width = 270;
        $item->case_ext_height = 95;
        $item->case_gross_weight = 7.2;
        $item->cases_per_pallet_length = 0;
        $item->cases_per_pallet_width = 0;
        $item->no_of_layers = 0;
        $item->is_length_allowed_vertical = 0;
        $item->is_width_allowed_vertical = 0;
        $item->is_height_allowed_vertical = 1;
        $item->status = ResStatus::$MAP['ACTIVE'];
        $item->save();

        $itemUom = new ItemUom;
        $itemUom->item_id = $item->id;
        $itemUom->uom_id = Uom::$UNIT;
        $itemUom->barcode = '8888307882572';
        $itemUom->uom_rate = 1;
        $itemUom->save();

        $itemPrice = new ItemSalePrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = Uom::$UNIT;
        $itemPrice->sale_price = 3.00;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $itemPrice = new ItemPurPrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = Uom::$UNIT;
        $itemPrice->sale_price = 3.00;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $itemUom = new ItemUom;
        $itemUom->item_id = $item->id;
        $itemUom->uom_id = Uom::$CASE;
        $itemUom->barcode = '';
        $itemUom->uom_rate = 24;
        $itemUom->save();

        $itemPrice = new ItemSalePrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = Uom::$CASE;
        $itemPrice->sale_price = 63.00;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $itemPrice = new ItemPurPrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = Uom::$CASE;
        $itemPrice->sale_price = 63.00;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $item = new Item;
        $item->code = 'RBU00003';
        $item->ref_code_01 = '';
        $item->desc_01 = 'RED BULL BOTTLE - 150ml x 6 x 4';
        $item->desc_02 = '';
        $item->storage_class = StorageClass::$MAP['AMBIENT_01'];
        $item->item_group_01_id = 3;
        $item->item_group_02_id = 0;
        $item->item_group_03_id = 0;
        $item->item_group_04_id = 0;
        $item->item_group_05_id = 0;
        $item->item_group_01_code = 'RED_BULL';
        $item->item_group_02_code = '';
        $item->item_group_03_code = '';
        $item->item_group_04_code = '';
        $item->item_group_05_code = '';
        $item->s_storage_class = StorageClass::$MAP['AMBIENT_01'];
        $item->s_item_group_01_id = 3; //RED_BULL
        $item->s_item_group_02_id = 0;
        $item->s_item_group_03_id = 0;
        $item->s_item_group_04_id = 0;
        $item->s_item_group_05_id = 0;
        $item->s_item_group_01_code = 'RED_BULL';
        $item->s_item_group_02_code = '';
        $item->s_item_group_03_code = '';
        $item->s_item_group_04_code = '';
        $item->s_item_group_05_code = '';
        $item->qty_scale = 0;
        $item->item_type = ItemType::$MAP['STOCK_ITEM'];
        $item->scan_mode = ScanMode::$MAP['ENTER_QTY'];
        $item->retrieval_method = RetrievalMethod::$MAP['FIRST_EXPIRED_FIRST_OUT'];
        $item->unit_uom_id = Uom::$UNIT;
        $item->case_uom_id = Uom::$CASE;
        $item->case_uom_rate = 24;
        $item->pallet_uom_rate = 0;
        $item->case_ext_length = 340;
        $item->case_ext_width = 230;
        $item->case_ext_height = 150;
        $item->case_gross_weight = 7.2;
        $item->cases_per_pallet_length = 0;
        $item->cases_per_pallet_width = 0;
        $item->no_of_layers = 0;
        $item->is_length_allowed_vertical = 0;
        $item->is_width_allowed_vertical = 0;
        $item->is_height_allowed_vertical = 1;
        $item->status = ResStatus::$MAP['ACTIVE'];
        $item->save();

        $itemUom = new ItemUom;
        $itemUom->item_id = $item->id;
        $itemUom->uom_id = Uom::$UNIT;
        $itemUom->barcode = '8888307881506';
        $itemUom->uom_rate = 1;
        $itemUom->save();

        $itemPrice = new ItemSalePrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = Uom::$UNIT;
        $itemPrice->sale_price = 2.50;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $itemPrice = new ItemPurPrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = Uom::$UNIT;
        $itemPrice->sale_price = 2.50;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $itemUom = new ItemUom;
        $itemUom->item_id = $item->id;
        $itemUom->uom_id = Uom::$CASE;
        $itemUom->barcode = '';
        $itemUom->uom_rate = 24;
        $itemUom->save();

        $itemPrice = new ItemSalePrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = Uom::$CASE;
        $itemPrice->sale_price = 58;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $itemPrice = new ItemPurPrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = Uom::$CASE;
        $itemPrice->sale_price = 58;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $item = new Item;
        $item->code = 'P90001M';
        $item->ref_code_01 = '';
        $item->desc_01 = 'POKKA OOLONG TEA - 500ml X 24';
        $item->desc_02 = '';
        $item->storage_class = StorageClass::$MAP['AMBIENT_01'];
        $item->item_group_01_id = 4;
        $item->item_group_02_id = 0;
        $item->item_group_03_id = 0;
        $item->item_group_04_id = 0;
        $item->item_group_05_id = 0;
        $item->item_group_01_code = 'POKKA';
        $item->item_group_02_code = '';
        $item->item_group_03_code = '';
        $item->item_group_04_code = '';
        $item->item_group_05_code = '';
        $item->s_storage_class = StorageClass::$MAP['AMBIENT_01'];
        $item->s_item_group_01_id = 4; //POKKA
        $item->s_item_group_02_id = 0;
        $item->s_item_group_03_id = 0;
        $item->s_item_group_04_id = 0;
        $item->s_item_group_05_id = 0;
        $item->s_item_group_01_code = 'POKKA';
        $item->s_item_group_02_code = '';
        $item->s_item_group_03_code = '';
        $item->s_item_group_04_code = '';
        $item->s_item_group_05_code = '';
        $item->qty_scale = 0;
        $item->item_type = ItemType::$MAP['STOCK_ITEM'];
        $item->scan_mode = ScanMode::$MAP['ENTER_QTY'];
        $item->retrieval_method = RetrievalMethod::$MAP['FIRST_EXPIRED_FIRST_OUT'];
        $item->unit_uom_id = Uom::$UNIT;
        $item->case_uom_id = Uom::$CASE;
        $item->case_uom_rate = 24;
        $item->pallet_uom_rate = 0;
        $item->case_ext_length = 275;
        $item->case_ext_width = 410;
        $item->case_ext_height = 210;
        $item->case_gross_weight = 13.176;
        $item->cases_per_pallet_length = 0;
        $item->cases_per_pallet_width = 0;
        $item->no_of_layers = 0;
        $item->is_length_allowed_vertical = 0;
        $item->is_width_allowed_vertical = 0;
        $item->is_height_allowed_vertical = 1;
        $item->status = ResStatus::$MAP['ACTIVE'];
        $item->save();

        $itemUom = new ItemUom;
        $itemUom->item_id = $item->id;
        $itemUom->uom_id = Uom::$UNIT;
        $itemUom->barcode = '8888196120724';
        $itemUom->uom_rate = 1;
        $itemUom->save();

        $itemPrice = new ItemSalePrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = Uom::$UNIT;
        $itemPrice->sale_price = 2.40;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $itemPrice = new ItemPurPrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = Uom::$UNIT;
        $itemPrice->sale_price = 2.40;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $itemUom = new ItemUom;
        $itemUom->item_id = $item->id;
        $itemUom->uom_id = Uom::$CASE;
        $itemUom->barcode = '';
        $itemUom->uom_rate = 24;
        $itemUom->save();

        $itemPrice = new ItemSalePrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = Uom::$CASE;
        $itemPrice->sale_price = 60;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $itemPrice = new ItemPurPrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = Uom::$CASE;
        $itemPrice->sale_price = 60;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $item = new Item;
        $item->code = 'TSG209401';
        $item->ref_code_01 = '';
        $item->desc_01 = 'PANADOL SOLUBLE - 4s x 30card x 24 (NEW)';
        $item->desc_02 = '';
        $item->storage_class = StorageClass::$MAP['AMBIENT_01'];
        $item->item_group_01_id = 5;
        $item->item_group_02_id = 0;
        $item->item_group_03_id = 0;
        $item->item_group_04_id = 0;
        $item->item_group_05_id = 0;
        $item->item_group_01_code = 'PANADOL';
        $item->item_group_02_code = '';
        $item->item_group_03_code = '';
        $item->item_group_04_code = '';
        $item->item_group_05_code = '';
        $item->s_storage_class = StorageClass::$MAP['AMBIENT_01'];
        $item->s_item_group_01_id = 5; //PANADOL
        $item->s_item_group_02_id = 0;
        $item->s_item_group_03_id = 0;
        $item->s_item_group_04_id = 0;
        $item->s_item_group_05_id = 0;
        $item->s_item_group_01_code = 'PANADOL';
        $item->s_item_group_02_code = '';
        $item->s_item_group_03_code = '';
        $item->s_item_group_04_code = '';
        $item->s_item_group_05_code = '';
        $item->qty_scale = 0;
        $item->item_type = ItemType::$MAP['STOCK_ITEM'];
        $item->scan_mode = ScanMode::$MAP['ENTER_QTY'];
        $item->retrieval_method = RetrievalMethod::$MAP['FIRST_EXPIRED_FIRST_OUT'];
        $item->unit_uom_id = Uom::$UNIT;
        $item->case_uom_id = Uom::$CASE;
        $item->case_uom_rate = 720;
        $item->pallet_uom_rate = 0;
        $item->case_ext_length = 300;
        $item->case_ext_width = 390;
        $item->case_ext_height = 345;
        $item->case_gross_weight = 10.704;
        $item->cases_per_pallet_length = 0;
        $item->cases_per_pallet_width = 0;
        $item->no_of_layers = 0;
        $item->is_length_allowed_vertical = 0;
        $item->is_width_allowed_vertical = 0;
        $item->is_height_allowed_vertical = 1;
        $item->status = ResStatus::$MAP['ACTIVE'];
        $item->save();

        $itemUom = new ItemUom;
        $itemUom->item_id = $item->id;
        $itemUom->uom_id = Uom::$UNIT;
        $itemUom->barcode = '9999196120999';
        $itemUom->uom_rate = 1;
        $itemUom->save();

        $itemPrice = new ItemSalePrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = Uom::$UNIT;
        $itemPrice->sale_price = 2.50;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $itemPrice = new ItemPurPrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = Uom::$UNIT;
        $itemPrice->sale_price = 2.50;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $itemUom = new ItemUom;
        $itemUom->item_id = $item->id;
        $itemUom->uom_id = 5;
        $itemUom->barcode = '';
        $itemUom->uom_rate = 30;
        $itemUom->save();

        $itemPrice = new ItemSalePrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = 5;
        $itemPrice->sale_price = 75;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $itemPrice = new ItemPurPrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = 5;
        $itemPrice->sale_price = 75;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $itemUom = new ItemUom;
        $itemUom->item_id = $item->id;
        $itemUom->uom_id = 6;
        $itemUom->barcode = '';
        $itemUom->uom_rate = 720;
        $itemUom->save();

        $itemPrice = new ItemSalePrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = 6;
        $itemPrice->sale_price = 1800;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();

        $itemPrice = new ItemPurPrice;
        $itemPrice->price_group_id = 0;
        $itemPrice->currency_id = 1;
        $itemPrice->item_id = $item->id;
        $itemPrice->uom_id = 6;
        $itemPrice->sale_price = 1800;
        $itemPrice->valid_from = '2000-01-01';
        $itemPrice->valid_to = '2100-01-01';
        $itemPrice->save();
    }
}
