<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->insert(array(
            array(
                'id' => 1,
                'code' => 'JHR',
                'desc_01' => 'JOHOR',
                'desc_02' => '',
                'created_at' => '2000-01-01 00:00:00',
                'updated_at' => '2000-01-01 00:00:00'
            ),
            array(
                'id' => 2,
                'code' => 'KDH',
                'desc_01' => 'KEDAH',
                'desc_02' => '',
                'created_at' => '2000-01-01 00:00:00',
                'updated_at' => '2000-01-01 00:00:00'
            ),
            array(
                'id' => 3,
                'code' => 'KTN',
                'desc_01' => 'KELANTAN',
                'desc_02' => '',
                'created_at' => '2000-01-01 00:00:00',
                'updated_at' => '2000-01-01 00:00:00'
            ),
            array(
                'id' => 4,
                'code' => 'KUL',
                'desc_01' => 'WP KUALA LUMPUR',
                'desc_02' => '',
                'created_at' => '2000-01-01 00:00:00',
                'updated_at' => '2000-01-01 00:00:00'
            ),
            array(
                'id' => 5,
                'code' => 'LBN',
                'desc_01' => 'WP LABUAN',
                'desc_02' => '',
                'created_at' => '2000-01-01 00:00:00',
                'updated_at' => '2000-01-01 00:00:00'
            ),
            array(
                'id' => 6,
                'code' => 'MLK',
                'desc_01' => 'MELAKA',
                'desc_02' => '',
                'created_at' => '2000-01-01 00:00:00',
                'updated_at' => '2000-01-01 00:00:00'
            ),
            array(
                'id' => 7,
                'code' => 'NSN',
                'desc_01' => 'NEGERI SEMBILAN',
                'desc_02' => '',
                'created_at' => '2000-01-01 00:00:00',
                'updated_at' => '2000-01-01 00:00:00'
            ),
            array(
                'id' => 8,
                'code' => 'PHG',
                'desc_01' => 'PAHANG',
                'desc_02' => '',
                'created_at' => '2000-01-01 00:00:00',
                'updated_at' => '2000-01-01 00:00:00'
            ),
            array(
                'id' => 9,
                'code' => 'PJY',
                'desc_01' => 'WP PUTRAJAYA',
                'desc_02' => '',
                'created_at' => '2000-01-01 00:00:00',
                'updated_at' => '2000-01-01 00:00:00'
            ),
            array(
                'id' => 10,
                'code' => 'PLS',
                'desc_01' => 'PERLIS',
                'desc_02' => '',
                'created_at' => '2000-01-01 00:00:00',
                'updated_at' => '2000-01-01 00:00:00'
            ),
            array(
                'id' => 11,
                'code' => 'PNG',
                'desc_01' => 'PULAU PINANG',
                'desc_02' => '',
                'created_at' => '2000-01-01 00:00:00',
                'updated_at' => '2000-01-01 00:00:00'
            ),
            array(
                'id' => 12,
                'code' => 'PRK',
                'desc_01' => 'PERAK',
                'desc_02' => '',
                'created_at' => '2000-01-01 00:00:00',
                'updated_at' => '2000-01-01 00:00:00'
            ),
            array(
                'id' => 13,
                'code' => 'SBH',
                'desc_01' => 'SABAH',
                'desc_02' => '',
                'created_at' => '2000-01-01 00:00:00',
                'updated_at' => '2000-01-01 00:00:00'
            ),
            array(
                'id' => 14,
                'code' => 'SGR',
                'desc_01' => 'SELANGOR',
                'desc_02' => '',
                'created_at' => '2000-01-01 00:00:00',
                'updated_at' => '2000-01-01 00:00:00'
            ),
            array(
                'id' => 15,
                'code' => 'SRW',
                'desc_01' => 'SARAWAK',
                'desc_02' => '',
                'created_at' => '2000-01-01 00:00:00',
                'updated_at' => '2000-01-01 00:00:00'
            ),
            array(
                'id' => 16,
                'code' => 'TRG',
                'desc_01' => 'TERENGGANU',
                'desc_02' => '',
                'created_at' => '2000-01-01 00:00:00',
                'updated_at' => '2000-01-01 00:00:00'
            ),
        ));
    }
}
