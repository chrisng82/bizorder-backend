<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'item'], function() {
        Route::get('downloadItemV2', [
            'uses' => 'ItemController@downloadItemV2'
        ]);

        Route::post('uploadItemV2', [
            'uses' => 'ItemController@uploadItemV2'
        ]);

        Route::get('importItemV2', [
            'uses' => 'ItemController@importItemV2'
        ]);

        Route::get('test', [
            'uses' => 'ItemController@test'
        ]);
    });

    
    Route::get('/', 'ImageUploadController@home');

    Route::post('/upload/images', [
      'uses'   =>  'ImageUploadController@uploadImages',
      'as'     =>  'uploadImage'
    ]);