<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::middleware([
        'cors',
        'check.localization'
    ])->group(function () {
        Route::group(['prefix' => 'label'], function () {
            Route::get('getLabel/{controller}', [
                'uses' => 'LabelController@getLabel'
            ]);
        });

        Route::group(['prefix' => 'login'], function () {
            Route::post('authenticate', [
                'uses' => 'AuthController@authenticate'
            ]);
        });

        Route::group(['prefix' => 'register'], function () {
            Route::post('registerAndActivate', [
                'uses' => 'Auth\RegisterController@registerAndActivate'
            ]);
        });

        Route::group(['prefix' => 'label'], function () {
            Route::get('getLanguage', [
                'uses' => 'LabelController@getLanguage'
            ]);
        });
    });

    Route::middleware([
        'cors',
        'header.json',
        'auth:api',
        'check.localization'
    ])->group(function () {
        Route::group(['prefix' => 'login'], function () {
            Route::post('logout', [
                'uses' => 'AuthController@logout'
            ]);
        });

        Route::group(['prefix' => 'auth'], function () {
            Route::post('changePassword', [
                'uses' => 'AuthController@changePassword'
            ]);
        });

        Route::group(['prefix' => 'site'], function () {
            Route::get('indexSiteFlow', [
                'uses' => 'SiteController@indexSiteFlow'
            ]);

            Route::get('indexGdsDelFlow/{siteFlowId}', [
                'uses' => 'SiteController@indexGdsDelFlow'
            ]);

            Route::get('indexGdsRcptFlow/{siteFlowId}', [
                'uses' => 'SiteController@indexGdsRcptFlow'
            ]);

            Route::get('indexInventoryAuditFlow/{siteFlowId}', [
                'uses' => 'SiteController@indexInventoryAuditFlow'
            ]);

            Route::get('indexBinTransferFlow/{siteFlowId}', [
                'uses' => 'SiteController@indexBinTransferFlow'
            ]);

            Route::get('indexRtnRcptFlow/{siteFlowId}', [
                'uses' => 'SiteController@indexRtnRcptFlow'
            ]);
        });

        Route::group(['prefix' => 'message'], function () {
            Route::get('index/{userId}', [
                'uses' => 'MessageController@index'
            ]);

            Route::get('showModel/{id}', [
                'uses' => 'MessageController@showModel'
            ]);

            Route::get('initModel', [
                'uses' => 'MessageController@initModel'
            ]);

            Route::post('createModel', [
                'uses' => 'MessageController@createModel'
            ]);

            Route::put('updateModel', [
                'uses' => 'MessageController@updateModel'
            ]);

            Route::delete('deleteModel/{id}', [
                'uses' => 'MessageController@deleteModel'
            ]);

            Route::put('markAllAsSeen', [
                'uses' => 'MessageController@markAllAsSeen'
            ]);

            Route::delete('clearNotifications', [
                'uses' => 'MessageController@clearNotifications'
            ]);
        });

        Route::group(['prefix' => 'mobile'], function () {
            Route::post('register/{deviceUuid}/{mobileApp}/{mobileOs}/{osVersion}/{appVersion}', [
                'uses' => 'MobileController@register'
            ]);

            Route::get('indexTableSchema/{mobileProfileId}', [
                'uses' => 'MobileController@indexTableSchema'
            ]);

            Route::get('indexData/{mobileProfileId}/{tableName}', [
                'uses' => 'MobileController@indexData'
            ]);

            Route::get('indexHistory/{mobileProfileId}/{tableName}', [
                'uses' => 'MobileController@indexHistory'
            ]);

            Route::post('updateHistory/{mobileProfileId}/{tableName}', [
                'uses' => 'MobileController@updateHistory'
            ]);

            Route::post('resetHistory/{mobileProfileId}/{tableName}', [
                'uses' => 'MobileController@resetHistory'
            ]);

            Route::post('resetAllHistory/{mobileProfileId}', [
                'uses' => 'MobileController@resetAllHistory'
            ]);
        });

        Route::group(['prefix' => 'division'], function () {
            Route::get('index', [
                'uses' => 'DivisionController@index'
            ]);

            Route::get('indexAll', [
                'uses' => 'DivisionController@indexAll'
            ]);

            Route::get('indexGdsDelFlow/{divisionId}', [
                'uses' => 'DivisionController@indexGdsDelFlow'
            ]);

            Route::get('indexGdsRcptFlow/{divisionId}', [
                'uses' => 'DivisionController@indexGdsRcptFlow'
            ]);

            Route::get('indexPurchaseFlow/{divisionId}', [
                'uses' => 'DivisionController@indexPurchaseFlow'
            ]);

            Route::get('indexSalesFlow/{divisionId}', [
                'uses' => 'DivisionController@indexSalesFlow'
            ]);

            Route::get('indexSalesReturnFlow/{divisionId}', [
                'uses' => 'DivisionController@indexSalesReturnFlow'
            ]);

            Route::post('select2', [
                'uses' => 'DivisionController@select2'
            ]);

            Route::get('showModel/{divisionId}', [
                'uses' => 'DivisionController@showModel'
            ]);

            Route::get('initModel', [
                'uses' => 'DivisionController@initModel'
            ]);

            Route::post('createModel', [
                'uses' => 'DivisionController@createModel'
            ]);

            Route::put('updateModel', [
                'uses' => 'DivisionController@updateModel'
            ]);
        });
    });

    Route::middleware([
        'cors',
        'header.json',
        'auth:api',
        'check.password',
        'check.localization'
    ])->group(function () {
        Route::group(['prefix' => 'pickList'], function () {
            Route::get('indexProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'PickListController@indexProcess'
            ]);

            Route::post('createProcess/{strProcType}', [
                'uses' => 'PickListController@createProcess'
            ]);

            Route::post('revertProcess/{hdrId}', [
                'uses' => 'PickListController@revertProcess'
            ]);

            Route::post('submitProcess/{hdrId}', [
                'uses' => 'PickListController@submitProcess'
            ]);

            Route::post('completeProcess/{hdrId}', [
                'uses' => 'PickListController@completeProcess'
            ]);

            Route::get('printProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'PickListController@printProcess'
            ]);

            Route::post('transitionToStatus', [
                'uses' => 'PickListController@transitionToStatus'
            ]);

            Route::get('verifyTxn', [
                'uses' => 'PickListController@verifyTxn'
            ]);

            Route::get('syncProcess/{strProcType}/{divisionId}', [
                'uses' => 'PickListController@syncProcess'
            ]);

            Route::post('changeQuantBal', [
                'uses' => 'PickListController@changeQuantBal'
            ]);

            Route::post('changeItemUom', [
                'uses' => 'PickListController@changeItemUom'
            ]);

            Route::get('showHeader/{hdrId}', [
                'uses' => 'PickListController@showHeader'
            ]);

            Route::get('showDetails/{hdrId}', [
                'uses' => 'PickListController@showDetails'
            ]);

            Route::put('updateHeader', [
                'uses' => 'PickListController@updateHeader'
            ]);

            Route::post('createDetail/{hdrId}', [
                'uses' => 'PickListController@createDetail'
            ]);

            Route::delete('deleteDetails/{hdrId}', [
                'uses' => 'PickListController@deleteDetails'
            ]);

            Route::put('updateDetails/{hdrId}', [
                'uses' => 'PickListController@updateDetails'
            ]);

            Route::get('index/{siteFlowId}', [
                'uses' => 'PickListController@index'
            ]);

            Route::post('repostAll', [
                'uses' => 'PickListController@repostAll'
            ]);
        });

        Route::group(['prefix' => 'packList'], function () {
            Route::get('indexProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'PackListController@indexProcess'
            ]);

            Route::post('createProcess/{strProcType}', [
                'uses' => 'PackListController@createProcess'
            ]);

            Route::post('revertProcess/{hdrId}', [
                'uses' => 'PackListController@revertProcess'
            ]);

            Route::post('submitProcess/{hdrId}', [
                'uses' => 'PackListController@submitProcess'
            ]);

            Route::post('completeProcess/{hdrId}', [
                'uses' => 'PackListController@completeProcess'
            ]);
        });

        Route::group(['prefix' => 'loadList'], function () {
            Route::get('indexProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'LoadListController@indexProcess'
            ]);

            Route::post('createProcess/{strProcType}', [
                'uses' => 'LoadListController@createProcess'
            ]);

            Route::post('revertProcess/{hdrId}', [
                'uses' => 'LoadListController@revertProcess'
            ]);

            Route::post('submitProcess/{hdrId}', [
                'uses' => 'LoadListController@submitProcess'
            ]);

            Route::post('completeProcess/{hdrId}', [
                'uses' => 'LoadListController@completeProcess'
            ]);
        });

        Route::group(['prefix' => 'prfDel'], function () {
            Route::get('indexProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'PrfDelController@indexProcess'
            ]);

            Route::post('createProcess/{strProcType}', [
                'uses' => 'PrfDelController@createProcess'
            ]);

            Route::post('revertProcess/{hdrId}', [
                'uses' => 'PrfDelController@revertProcess'
            ]);

            Route::post('submitProcess/{hdrId}', [
                'uses' => 'PrfDelController@submitProcess'
            ]);

            Route::post('completeProcess/{hdrId}', [
                'uses' => 'PrfDelController@completeProcess'
            ]);
        });

        Route::group(['prefix' => 'failDel'], function () {
            Route::get('indexProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'FailDelController@indexProcess'
            ]);

            Route::post('createProcess/{strProcType}', [
                'uses' => 'FailDelController@createProcess'
            ]);

            Route::post('revertProcess/{hdrId}', [
                'uses' => 'FailDelController@revertProcess'
            ]);

            Route::post('submitProcess/{hdrId}', [
                'uses' => 'FailDelController@submitProcess'
            ]);

            Route::post('completeProcess/{hdrId}', [
                'uses' => 'FailDelController@completeProcess'
            ]);
        });

        Route::group(['prefix' => 'whseJob'], function () {
            Route::get('indexProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'WhseJobController@indexProcess'
            ]);

            Route::post('createProcess/{strProcType}', [
                'uses' => 'WhseJobController@createProcess'
            ]);

            Route::post('revertProcess/{hdrId}', [
                'uses' => 'WhseJobController@revertProcess'
            ]);

            Route::post('downloadProcess/{hdrId}/{mobileProfileId}/{worker01Id}', [
                'uses' => 'WhseJobController@downloadProcess'
            ]);

            Route::post('completeProcess/{hdrId}', [
                'uses' => 'WhseJobController@completeProcess'
            ]);

            Route::get('printProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'WhseJobController@printProcess'
            ]);

            Route::get('showHeader/{hdrId}', [
                'uses' => 'WhseJobController@showHeader'
            ]);

            Route::get('showDetails/{strResType}/{hdrId}', [
                'uses' => 'WhseJobController@showDetails'
            ]);

            Route::put('updateHeader', [
                'uses' => 'WhseJobController@updateHeader'
            ]);

            Route::post('createDetail/{strResType}/{hdrId}', [
                'uses' => 'WhseJobController@createDetail'
            ]);

            Route::delete('deleteDetails/{hdrId}', [
                'uses' => 'WhseJobController@deleteDetails'
            ]);

            Route::put('updateDetails/{strResType}/{hdrId}', [
                'uses' => 'WhseJobController@updateDetails'
            ]);

            Route::post('changeQuantBal', [
                'uses' => 'WhseJobController@changeQuantBal'
            ]);

            Route::post('changeItem', [
                'uses' => 'WhseJobController@changeItem'
            ]);

            Route::post('changeItemUom', [
                'uses' => 'WhseJobController@changeItemUom'
            ]);

            Route::post('changeItemBatch', [
                'uses' => 'WhseJobController@changeItemBatch'
            ]);

            Route::post('transitionToStatus', [
                'uses' => 'WhseJobController@transitionToStatus'
            ]);
        });

        Route::group(['prefix' => 'invDoc'], function () {
            Route::get('indexProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'InvDocController@indexProcess'
            ]);

            Route::post('createProcess/{strProcType}', [
                'uses' => 'InvDocController@createProcess'
            ]);

            Route::get('printProcess/{strProcType}', [
                'uses' => 'InvDocController@printProcess'
            ]);
        });

        Route::group(['prefix' => 'dashboard'], function () {
            Route::get('weeklyReport/{week}', [
                'uses' => 'DashboardController@weeklyReport'
            ]);
            Route::get('monthlyReport/{month}', [
                'uses' => 'DashboardController@monthlyReport'
            ]);
            Route::get('yearlyReport/{year}', [
                'uses' => 'DashboardController@yearlyReport'
            ]);
            Route::get('weeklyItemReport/{week}', [
                'uses' => 'DashboardController@weeklyReport'
            ]);
            Route::get('monthlyItemReport/{month}', [
                'uses' => 'DashboardController@monthlyReport'
            ]);
            Route::get('yearlyItemReport/{year}', [
                'uses' => 'DashboardController@yearlyReport'
            ]);
            Route::get('summaryReport', [
                'uses' => 'DashboardController@summaryReport'
            ]);
            Route::post('createModel', [
                'uses' => 'DashboardController@createModel'
            ]);
            Route::put('updateModel', [
                'uses' => 'DashboardController@updateModel'
            ]);
            Route::get('retrieveModel/{id}', [
                'uses' => 'DashboardController@retrieveModel'
            ]);
            Route::get('initId/{userId}', [
                'uses' => 'DashboardController@initId'
            ]);
        });

        Route::group(['prefix' => 'item'], function () {
            Route::get('indexProcess/{strProcType}', [
                'uses' => 'ItemController@indexProcess'
            ]);

            Route::get('indexDivision/{divisionId}', [
                'uses' => 'ItemController@indexDivision'
            ]);

            Route::post('uploadProcess/{strProcType}/{siteFlowId}/{divisionId}', [
                'uses' => 'ItemController@uploadProcess'
            ]);

            Route::get('syncProcess/{strProcType}/{siteFlowId}/{divisionId}', [
                'uses' => 'ItemController@syncProcess'
            ]);

            Route::get('downloadProcess/{strProcType}/{siteFlowId}/{divisionId}', [
                'uses' => 'ItemController@downloadProcess'
            ]);

            Route::post('select2Item', [
                'uses' => 'ItemController@select2Item'
            ]);

            Route::get('select2UpdateItem/{item_id}', [
                'uses' => 'ItemController@select2UpdateItem'
            ]);

            Route::get('getItemUom/{item_id}', [
                'uses' => 'ItemController@getItemUom'
            ]);

            Route::post('select2', [
                'uses' => 'ItemController@select2'
            ]);

            Route::post('select2Init', [
                'uses' => 'ItemController@select2Init'
            ]);

            Route::get('initModel', [
                'uses' => 'ItemController@initModel'
            ]);

            Route::get('showModel/{itemId}', [
                'uses' => 'ItemController@showModel'
            ]);

            Route::get('deleteModel/{itemId}', [
                'uses' => 'ItemController@deleteModel'
            ]);

            Route::post('createModel/{divisionId}', [
                'uses' => 'ItemController@createModel'
            ]);

            Route::put('updateModel', [
                'uses' => 'ItemController@updateModel'
            ]);

            Route::post('createPriceModel', [
                'uses' => 'ItemController@createPriceModel'
            ]);

            Route::put('updatePriceModel', [
                'uses' => 'ItemController@updatePriceModel'
            ]);

            Route::post('uploadPhotos/{itemId}', [
                'uses' => 'ItemController@uploadPhotos'
            ]);

            Route::post('deletePhotos/{itemId}', [
                'uses' => 'ItemController@deletePhotos'
            ]);

            Route::get('resetPhotos/{divisionId}', [
                'uses' => 'ItemController@resetPhotos'
            ]);

            Route::post('itemGroup01', [
                'uses' => 'ItemController@itemGroup01'
            ]);

            Route::post('itemGroup02', [
                'uses' => 'ItemController@itemGroup02'
            ]);

            Route::post('itemGroup03', [
                'uses' => 'ItemController@itemGroup03'
            ]);
        });

        Route::group(['prefix' => 'outbOrd'], function () {
            Route::get('index/{divisionId}', [
                'uses' => 'OutbOrdController@index'
            ]);

            Route::get('showHeader/{hdrId}', [
                'uses' => 'OutbOrdController@showHeader'
            ]);

            Route::get('showDetails/{hdrId}', [
                'uses' => 'OutbOrdController@showDetails'
            ]);

            Route::post('createHeader', [
                'uses' => 'OutbOrdController@createHeader'
            ]);

            Route::post('createDetail/{hdrId}', [
                'uses' => 'OutbOrdController@createDetail'
            ]);

            Route::delete('deleteDetails/{hdrId}', [
                'uses' => 'OutbOrdController@deleteDetails'
            ]);

            Route::put('updateDetails/{hdrId}', [
                'uses' => 'OutbOrdController@updateDetails'
            ]);

            Route::get('printOutbOrdHdr/{hdrId}', [
                'uses' => 'OutbOrdController@printOutbOrdHdr'
            ]);

            Route::post('testCreateDocuments', [
                'uses' => 'OutbOrdController@testCreateDocuments'
            ]);

            Route::get('indexProcess/{strProcType}/{divisionId}', [
                'uses' => 'OutbOrdController@indexProcess'
            ]);

            Route::post('createProcess/{strProcType}', [
                'uses' => 'OutbOrdController@createProcess'
            ]);

            Route::get('checkStock', [
                'uses' => 'OutbOrdController@checkStock'
            ]);

            Route::post('transitionToStatus', [
                'uses' => 'OutbOrdController@transitionToStatus'
            ]);

            Route::post('changeItem', [
                'uses' => 'OutbOrdController@changeItem'
            ]);

            Route::post('changeItemUom', [
                'uses' => 'OutbOrdController@changeItemUom'
            ]);

            Route::post('changeDeliveryPoint', [
                'uses' => 'OutbOrdController@changeDeliveryPoint'
            ]);

            Route::post('changeCurrency', [
                'uses' => 'OutbOrdController@changeCurrency'
            ]);
        });

        Route::group(['prefix' => 'inbOrd'], function () {
            Route::get('indexProcess/{strProcType}/{divisionId}', [
                'uses' => 'InbOrdController@indexProcess'
            ]);

            Route::post('testCreateDocuments', [
                'uses' => 'InbOrdController@testCreateDocuments'
            ]);

            Route::post('createProcess/{strProcType}', [
                'uses' => 'InbOrdController@createProcess'
            ]);

            Route::get('showHeader/{hdrId}', [
                'uses' => 'InbOrdController@showHeader'
            ]);

            Route::get('showDetails/{hdrId}', [
                'uses' => 'InbOrdController@showDetails'
            ]);

            Route::post('createDetail/{hdrId}', [
                'uses' => 'InbOrdController@createDetail'
            ]);

            Route::put('updateDetails/{hdrId}', [
                'uses' => 'InbOrdController@updateDetails'
            ]);

            Route::delete('deleteDetails/{hdrId}', [
                'uses' => 'InbOrdController@deleteDetails'
            ]);

            Route::post('transitionToStatus', [
                'uses' => 'InbOrdController@transitionToStatus'
            ]);

            Route::post('changeItem', [
                'uses' => 'InbOrdController@changeItem'
            ]);

            Route::post('changeItemUom', [
                'uses' => 'InbOrdController@changeItemUom'
            ]);

            Route::post('changeDeliveryPoint', [
                'uses' => 'InbOrdController@changeDeliveryPoint'
            ]);

            Route::post('changeCurrency', [
                'uses' => 'InbOrdController@changeCurrency'
            ]);

            Route::get('index/{divisionId}', [
                'uses' => 'InbOrdController@index'
            ]);
        });

        Route::group(['prefix' => 'slsInv'], function () {
            Route::get('index/{divisionId}', [
                'uses' => 'SlsInvController@index'
            ]);

            Route::get('showHeader/{hdrId}', [
                'uses' => 'SlsInvController@showHeader'
            ]);

            Route::get('showDetails/{hdrId}', [
                'uses' => 'SlsInvController@showDetails'
            ]);

            Route::post('createDetail/{hdrId}', [
                'uses' => 'SlsInvController@createDetail'
            ]);

            Route::put('updateDetails/{hdrId}', [
                'uses' => 'SlsInvController@updateDetails'
            ]);

            Route::delete('deleteDetails/{hdrId}', [
                'uses' => 'SlsInvController@deleteDetails'
            ]);

            Route::get('syncProcess/{strProcType}/{divisionId}', [
                'uses' => 'SlsInvController@syncProcess'
            ]);

            Route::post('transitionToStatus', [
                'uses' => 'SlsInvController@transitionToStatus'
            ]);

            Route::get('initHeader/{divisionId}', [
                'uses' => 'SlsInvController@initHeader'
            ]);

            Route::post('createHeader', [
                'uses' => 'SlsInvController@createHeader'
            ]);

            Route::put('updateHeader', [
                'uses' => 'SlsInvController@updateHeader'
            ]);
        });

        Route::group(['prefix' => 'rtnRcpt'], function () {

            Route::get('syncProcess/{strProcType}/{divisionId}', [
                'uses' => 'RtnRcptController@syncProcess'
            ]);

            Route::get('index/{divisionId}', [
                'uses' => 'RtnRcptController@index'
            ]);

            Route::get('showHeader/{hdrId}', [
                'uses' => 'RtnRcptController@showHeader'
            ]);

            Route::get('showDetails/{hdrId}', [
                'uses' => 'RtnRcptController@showDetails'
            ]);

            Route::post('createDetail/{hdrId}', [
                'uses' => 'RtnRcptController@createDetail'
            ]);

            Route::put('updateDetails/{hdrId}', [
                'uses' => 'RtnRcptController@updateDetails'
            ]);

            Route::delete('deleteDetails/{hdrId}', [
                'uses' => 'RtnRcptController@deleteDetails'
            ]);

            Route::post('transitionToStatus', [
                'uses' => 'RtnRcptController@transitionToStatus'
            ]);

            Route::get('initHeader/{divisionId}', [
                'uses' => 'RtnRcptController@initHeader'
            ]);

            Route::post('createHeader', [
                'uses' => 'RtnRcptController@createHeader'
            ]);

            Route::put('updateHeader', [
                'uses' => 'RtnRcptController@updateHeader'
            ]);
        });

        Route::group(['prefix' => 'slsOrd'], function () {
            Route::get('index/{divisionId}', [
                'uses' => 'SlsOrdController@index'
            ]);

            Route::get('indexProcess/{strProcType}/{divisionId}', [
                'uses' => 'SlsOrdController@indexProcess'
            ]);

            Route::get('showHeader/{hdrId}', [
                'uses' => 'SlsOrdController@showHeader'
            ]);

            Route::get('showDetails/{hdrId}', [
                'uses' => 'SlsOrdController@showDetails'
            ]);

            Route::post('createDetail/{hdrId}', [
                'uses' => 'SlsOrdController@createDetail'
            ]);

            Route::put('updateDetails/{hdrId}', [
                'uses' => 'SlsOrdController@updateDetails'
            ]);

            Route::delete('deleteDetails/{hdrId}', [
                'uses' => 'SlsOrdController@deleteDetails'
            ]);

            Route::get('syncProcess/{strProcType}/{divisionId}', [
                'uses' => 'SlsOrdController@syncProcess'
            ]);

            Route::post('transitionToStatus', [
                'uses' => 'SlsOrdController@transitionToStatus'
            ]);

            Route::post('changeItem', [
                'uses' => 'SlsOrdController@changeItem'
            ]);

            Route::post('changeItemUom', [
                'uses' => 'SlsOrdController@changeItemUom'
            ]);

            Route::post('changeDeliveryPoint', [
                'uses' => 'SlsOrdController@changeDeliveryPoint'
            ]);

            Route::post('changeCurrency', [
                'uses' => 'SlsOrdController@changeCurrency'
            ]);

            Route::get('initHeader/{divisionId}', [
                'uses' => 'SlsOrdController@initHeader'
            ]);

            Route::post('createHeader', [
                'uses' => 'SlsOrdController@createHeader'
            ]);

            Route::put('updateHeader', [
                'uses' => 'SlsOrdController@updateHeader'
            ]);
        });

        Route::group(['prefix' => 'batchJobStatus'], function () {
            Route::get('showBatchJobStatus/{strProcType}', [
                'uses' => 'BatchJobStatusController@showBatchJobStatus'
            ]);

            Route::get('resetBatchJobStatus/{strProcType}', [
                'uses' => 'BatchJobStatusController@resetBatchJobStatus'
            ]);
        });

        Route::group(['prefix' => 'syncSetting'], function () {
            Route::get('showSiteFlowSetting/{strProcType}/{siteFlowId}', [
                'uses' => 'SyncSettingController@showSiteFlowSetting'
            ]);

            Route::get('showDivisionSetting/{strProcType}/{divisionId}', [
                'uses' => 'SyncSettingController@showDivisionSetting'
            ]);

            Route::put('updateModel', [
                'uses' => 'SyncSettingController@updateModel'
            ]);
        });

        Route::group(['prefix' => 'storageBin'], function () {
            Route::get('indexProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'StorageBinController@indexProcess'
            ]);

            Route::get('downloadProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'StorageBinController@downloadProcess'
            ]);

            Route::post('uploadProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'StorageBinController@uploadProcess'
            ]);

            Route::post('select2', [
                'uses' => 'StorageBinController@select2'
            ]);

            Route::post('select2Init', [
                'uses' => 'StorageBinController@select2Init'
            ]);
        });

        Route::group(['prefix' => 'deliveryPoint'], function () {
            Route::get('indexProcess/{strProcType}', [
                'uses' => 'DeliveryPointController@indexProcess'
            ]);

            Route::get('downloadProcess/{strProcType}/{siteFlowId}/{divisionId}', [
                'uses' => 'DeliveryPointController@downloadProcess'
            ]);

            Route::post('uploadProcess/{strProcType}/{siteFlowId}/{divisionId}', [
                'uses' => 'DeliveryPointController@uploadProcess'
            ]);

            Route::get('syncProcess/{strProcType}/{siteFlowId}/{divisionId}', [
                'uses' => 'DeliveryPointController@syncProcess'
            ]);

            Route::post('select2', [
                'uses' => 'DeliveryPointController@select2'
            ]);

            Route::get('showModel/{deliveryPointId}', [
                'uses' => 'DeliveryPointController@showModel'
            ]);

            Route::delete('deleteModel/{deliveryPointId}', [
                'uses' => 'DeliveryPointController@deleteModel'
            ]);

            Route::get('initModel', [
                'uses' => 'DeliveryPointController@initModel'
            ]);

            Route::post('createModel', [
                'uses' => 'DeliveryPointController@createModel'
            ]);

            Route::put('updateModel', [
                'uses' => 'DeliveryPointController@updateModel'
            ]);

            Route::delete('deleteModel/{deliveryPointId}', [
                'uses' => 'DeliveryPointController@deleteModel'
            ]);
        });

        Route::group(['prefix' => 'gdsRcpt'], function () {
            Route::get('indexProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'GdsRcptController@indexProcess'
            ]);

            Route::post('createProcess/{strProcType}', [
                'uses' => 'GdsRcptController@createProcess'
            ]);

            Route::post('transitionToStatus', [
                'uses' => 'GdsRcptController@transitionToStatus'
            ]);

            Route::get('verifyTxn', [
                'uses' => 'GdsRcptController@verifyTxn'
            ]);

            Route::get('showHeader/{hdrId}', [
                'uses' => 'GdsRcptController@showHeader'
            ]);

            Route::get('showDetails/{hdrId}', [
                'uses' => 'GdsRcptController@showDetails'
            ]);

            Route::put('updateHeader', [
                'uses' => 'GdsRcptController@updateHeader'
            ]);

            Route::post('createDetail/{hdrId}', [
                'uses' => 'GdsRcptController@createDetail'
            ]);

            Route::delete('deleteDetails/{hdrId}', [
                'uses' => 'GdsRcptController@deleteDetails'
            ]);

            Route::put('updateDetails/{hdrId}', [
                'uses' => 'GdsRcptController@updateDetails'
            ]);

            Route::post('changeItem', [
                'uses' => 'GdsRcptController@changeItem'
            ]);

            Route::post('changeItemUom', [
                'uses' => 'GdsRcptController@changeItemUom'
            ]);

            Route::post('changeItemBatch', [
                'uses' => 'GdsRcptController@changeItemBatch'
            ]);

            Route::get('index/{siteFlowId}', [
                'uses' => 'GdsRcptController@index'
            ]);

            Route::get('printProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'GdsRcptController@printProcess'
            ]);
        });

        Route::group(['prefix' => 'putAway'], function () {
            Route::get('indexProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'PutAwayController@indexProcess'
            ]);

            Route::post('createProcess/{strProcType}', [
                'uses' => 'PutAwayController@createProcess'
            ]);

            Route::post('transitionToStatus', [
                'uses' => 'PutAwayController@transitionToStatus'
            ]);

            Route::get('verifyTxn', [
                'uses' => 'PutAwayController@verifyTxn'
            ]);

            Route::get('showHeader/{hdrId}', [
                'uses' => 'PutAwayController@showHeader'
            ]);

            Route::get('showDetails/{hdrId}', [
                'uses' => 'PutAwayController@showDetails'
            ]);

            Route::put('updateHeader', [
                'uses' => 'PutAwayController@updateHeader'
            ]);

            Route::post('createDetail/{hdrId}', [
                'uses' => 'PutAwayController@createDetail'
            ]);

            Route::delete('deleteDetails/{hdrId}', [
                'uses' => 'PutAwayController@deleteDetails'
            ]);

            Route::put('updateDetails/{hdrId}', [
                'uses' => 'PutAwayController@updateDetails'
            ]);

            Route::post('changeQuantBal', [
                'uses' => 'PutAwayController@changeQuantBal'
            ]);

            Route::get('index/{siteFlowId}', [
                'uses' => 'PutAwayController@index'
            ]);

            Route::post('changeItemUom', [
                'uses' => 'PutAwayController@changeItemUom'
            ]);
        });

        Route::group(['prefix' => 'cycleCount'], function () {
            Route::get('indexProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'CycleCountController@indexProcess'
            ]);

            Route::post('createProcess/{strProcType}', [
                'uses' => 'CycleCountController@createProcess'
            ]);

            Route::post('transitionToStatus', [
                'uses' => 'CycleCountController@transitionToStatus'
            ]);

            Route::post('uploadProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'CycleCountController@uploadProcess'
            ]);

            Route::get('downloadProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'CycleCountController@downloadProcess'
            ]);

            Route::get('showHeader/{hdrId}', [
                'uses' => 'CycleCountController@showHeader'
            ]);

            Route::get('showDetails/{hdrId}', [
                'uses' => 'CycleCountController@showDetails'
            ]);

            Route::get('showRecountDetails/{hdrId}', [
                'uses' => 'CycleCountController@showRecountDetails'
            ]);

            Route::put('updateHeader', [
                'uses' => 'CycleCountController@updateHeader'
            ]);

            Route::post('createDetail/{hdrId}', [
                'uses' => 'CycleCountController@createDetail'
            ]);

            Route::delete('deleteDetails/{hdrId}', [
                'uses' => 'CycleCountController@deleteDetails'
            ]);

            Route::put('updateDetails/{hdrId}', [
                'uses' => 'CycleCountController@updateDetails'
            ]);

            Route::post('changeItem', [
                'uses' => 'CycleCountController@changeItem'
            ]);

            Route::post('changeItemUom', [
                'uses' => 'CycleCountController@changeItemUom'
            ]);

            Route::post('changeItemBatch', [
                'uses' => 'CycleCountController@changeItemBatch'
            ]);

            Route::post('changeQuantBal', [
                'uses' => 'CycleCountController@changeQuantBal'
            ]);

            Route::post('createHeader', [
                'uses' => 'CycleCountController@createHeader'
            ]);

            Route::get('initHeader/{siteFlowId}', [
                'uses' => 'CycleCountController@initHeader'
            ]);

            Route::post('createJobDetail/{hdrId}', [
                'uses' => 'CycleCountController@createJobDetail'
            ]);

            Route::delete('deleteJobDetails/{hdrId}', [
                'uses' => 'CycleCountController@deleteJobDetails'
            ]);

            Route::put('updateRecountDetails/{id}/{physicalCountStatus}', [
                'uses' => 'CycleCountController@updateRecountDetails'
            ]);

            Route::get('index/{siteFlowId}', [
                'uses' => 'CycleCountController@index'
            ]);

            Route::post('autoConfirmDetails/{hdrId}', [
                'uses' => 'CycleCountController@autoConfirmDetails'
            ]);
        });

        Route::group(['prefix' => 'countAdj'], function () {
            Route::get('indexProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'CountAdjController@indexProcess'
            ]);

            Route::post('createProcess/{strProcType}', [
                'uses' => 'CountAdjController@createProcess'
            ]);

            Route::post('transitionToStatus', [
                'uses' => 'CountAdjController@transitionToStatus'
            ]);

            Route::get('showHeader/{hdrId}', [
                'uses' => 'CountAdjController@showHeader'
            ]);

            Route::get('showDetails/{hdrId}', [
                'uses' => 'CountAdjController@showDetails'
            ]);

            Route::put('updateHeader', [
                'uses' => 'CountAdjController@updateHeader'
            ]);

            Route::post('createDetail/{hdrId}', [
                'uses' => 'CountAdjController@createDetail'
            ]);

            Route::delete('deleteDetails/{hdrId}', [
                'uses' => 'CountAdjController@deleteDetails'
            ]);

            Route::put('updateDetails/{hdrId}', [
                'uses' => 'CountAdjController@updateDetails'
            ]);

            Route::get('index/{siteFlowId}', [
                'uses' => 'CountAdjController@index'
            ]);

            Route::post('changeQuantBal', [
                'uses' => 'CountAdjController@changeQuantBal'
            ]);

            Route::post('changeItem', [
                'uses' => 'CountAdjController@changeItem'
            ]);

            Route::post('changeItemBatch', [
                'uses' => 'CountAdjController@changeItemBatch'
            ]);

            Route::post('changeItemUom', [
                'uses' => 'CountAdjController@changeItemUom'
            ]);

            Route::get('initHeader/{siteFlowId}', [
                'uses' => 'CountAdjController@initHeader'
            ]);

            Route::post('createHeader', [
                'uses' => 'CountAdjController@createHeader'
            ]);

            Route::get('downloadDetails/{hdrId}', [
                'uses' => 'CountAdjController@downloadDetails'
            ]);

            Route::post('uploadDetails/{hdrId}', [
                'uses' => 'CountAdjController@uploadDetails'
            ]);
        });

        Route::group(['prefix' => 'quantBal'], function () {
            Route::get('verifyTxn', [
                'uses' => 'QuantBalController@verifyTxn'
            ]);

            Route::post('select2', [
                'uses' => 'QuantBalController@select2'
            ]);

            Route::post('select2Init', [
                'uses' => 'QuantBalController@select2Init'
            ]);
        });

        Route::group(['prefix' => 'company'], function () {
            Route::get('index', [
                'uses' => 'CompanyController@index'
            ]);

            Route::post('select2', [
                'uses' => 'CompanyController@select2'
            ]);

            Route::post('select2Init', [
                'uses' => 'CompanyController@select2Init'
            ]);

            Route::get('showModel/{companyId}', [
                'uses' => 'CompanyController@showModel'
            ]);

            Route::get('initModel', [
                'uses' => 'CompanyController@initModel'
            ]);

            Route::post('createModel', [
                'uses' => 'CompanyController@createModel'
            ]);

            Route::put('updateModel', [
                'uses' => 'CompanyController@updateModel'
            ]);
        });

        Route::group(['prefix' => 'handlingUnit'], function () {
            Route::get('indexProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'HandlingUnitController@indexProcess'
            ]);

            Route::get('printProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'HandlingUnitController@printProcess'
            ]);

            Route::post('createProcess/{strProcType}', [
                'uses' => 'HandlingUnitController@createProcess'
            ]);

            Route::post('select2', [
                'uses' => 'HandlingUnitController@select2'
            ]);

            Route::post('select2Init', [
                'uses' => 'HandlingUnitController@select2Init'
            ]);
        });

        Route::group(['prefix' => 'advShip'], function() {
//tanda
            Route::get('indexProcess/{strProcType}/{divisionId}', [
                'uses' => 'AdvShipController@indexProcess'
            ]);

            Route::get('initHeader/{divisionId}', [
                'uses' => 'AdvShipController@initHeader'
            ]);

            Route::get('showHeader/{hdrId}', [
                'uses' => 'AdvShipController@showHeader'
            ]);

            Route::get('showDetails/{hdrId}', [
                'uses' => 'AdvShipController@showDetails'
            ]);

            Route::post('createHeader', [
                'uses' => 'AdvShipController@createHeader'
            ]);

            Route::put('updateHeader', [
                'uses' => 'AdvShipController@updateHeader'
            ]);

            Route::post('createDetail/{hdrId}', [
                'uses' => 'AdvShipController@createDetail'
            ]);

            Route::delete('deleteDetails/{hdrId}', [
                'uses' => 'AdvShipController@deleteDetails'
            ]);

            Route::put('updateDetails/{hdrId}', [
                'uses' => 'AdvShipController@updateDetails'
            ]);

            Route::post('changeBizPartner', [
                'uses' => 'AdvShipController@changeBizPartner'
            ]);

            Route::post('changeCurrency', [
                'uses' => 'AdvShipController@changeCurrency'
            ]);

            Route::post('changeItem', [
                'uses' => 'AdvShipController@changeItem'
            ]);

            Route::post('changeItemUom', [
                'uses' => 'AdvShipController@changeItemUom'
            ]);

            Route::post('transitionToStatus', [
                'uses' => 'AdvShipController@transitionToStatus'
            ]);

            Route::get('syncProcess/{strProcType}/{divisionId}', [
                'uses' => 'AdvShipController@syncProcess'
            ]);

            Route::get('index/{divisionId}', [
                'uses' => 'AdvShipController@index'
            ]);
        });

        Route::group(['prefix' => 'bizPartner'], function () {
            Route::post('select2', [
                'uses' => 'BizPartnerController@select2'
            ]);

            Route::post('select2Init', [
                'uses' => 'BizPartnerController@select2Init'
            ]);
        });

        Route::group(['prefix' => 'user'], function () {
            Route::post('select2', [
                'uses' => 'UserController@select2'
            ]);

            Route::post('select2Init', [
                'uses' => 'UserController@select2Init'
            ]);

            Route::get('indexProcess/{strProcType}', [
                'uses' => 'UserController@indexProcess'
            ]);

            Route::get('showRoles/{userId}', [
                'uses' => 'UserController@showRoles'
            ]);

            Route::put('updateRoles/{userId}', [
                'uses' => 'UserController@updateRoles'
            ]);

            Route::delete('deleteRoles/{userId}', [
                'uses' => 'UserController@deleteRoles'
            ]);

            Route::get('showModel/{userId}', [
                'uses' => 'UserController@showModel'
            ]);

            Route::get('initModel', [
                'uses' => 'UserController@initModel'
            ]);

            Route::post('createModel', [
                'uses' => 'UserController@createModel'
            ]);

            Route::put('updateModel', [
                'uses' => 'UserController@updateModel'
            ]);

            Route::post('changePassword/{userId}', [
                'uses' => 'UserController@changePassword'
            ]);

            Route::get('showDivisions/{userId}', [
                'uses' => 'UserController@showDivisions'
            ]);

            Route::put('updateDivisions/{userId}', [
                'uses' => 'UserController@updateDivisions'
            ]);

            Route::put('createDivisions/{userId}', [
                'uses' => 'UserController@createDivisions'
            ]);

            Route::delete('deleteDivisions/{userId}', [
                'uses' => 'UserController@deleteDivisions'
            ]);

            Route::post('select2LoginType', [
                'uses' => 'UserController@select2LoginType'
            ]);

            Route::post('uploadProcess/{strProcType}/{siteFlowId}/{divisionId}', [
                'uses' => 'UserController@uploadProcess'
            ]);
        });

        Route::group(['prefix' => 'efiChain'], function () {
            Route::get('syncProcess/{strProcType}/{divisionId}', [
                'uses' => 'EfiChainController@syncProcess'
            ]);
        });

        Route::group(['prefix' => 'purInv'], function () {
            Route::get('indexProcess/{strProcType}/{divisionId}', [
                'uses' => 'PurInvController@indexProcess'
            ]);

            Route::post('createProcess/{strProcType}', [
                'uses' => 'PurInvController@createProcess'
            ]);
        });

        Route::group(['prefix' => 'itemGroup01'], function () {
            Route::post('select2', [
                'uses' => 'ItemGroup01Controller@select2'
            ]);

            Route::post('select2ByDivision', [
                'uses' => 'ItemGroup01Controller@select2ByDivision'
            ]);

            Route::get('index', [
                'uses' => 'ItemGroup01Controller@index'
            ]);

            Route::post('uploadPhoto/{id}', [
                'uses' => 'ItemGroup01Controller@uploadPhoto'
            ]);

            Route::post('deletePhoto/{id}', [
                'uses' => 'ItemGroup01Controller@deletePhoto'
            ]);
        });

        Route::group(['prefix' => 'itemGroup01Division'], function () {
            Route::post('reorderBrand', [
                'uses' => 'ItemGroup01DivisionController@reorderBrand'
            ]);
        });

        Route::group(['prefix' => 'itemGroup02'], function () {
            Route::post('select2', [
                'uses' => 'ItemGroup02Controller@select2'
            ]);

            Route::post('select2ByDivision', [
                'uses' => 'ItemGroup02Controller@select2ByDivision'
            ]);

            Route::get('index', [
                'uses' => 'ItemGroup02Controller@index'
            ]);

            Route::post('uploadPhoto/{id}', [
                'uses' => 'ItemGroup02Controller@uploadPhoto'
            ]);

            Route::post('deletePhoto/{id}', [
                'uses' => 'ItemGroup02Controller@deletePhoto'
            ]);
        });

        Route::group(['prefix' => 'itemGroup03'], function () {
            Route::post('select2', [
                'uses' => 'ItemGroup03Controller@select2'
            ]);

            Route::post('select2ByDivision', [
                'uses' => 'ItemGroup03Controller@select2ByDivision'
            ]);

            Route::get('index', [
                'uses' => 'ItemGroup03Controller@index'
            ]);

            Route::post('uploadPhoto/{id}', [
                'uses' => 'ItemGroup03Controller@uploadPhoto'
            ]);

            Route::post('deletePhoto/{id}', [
                'uses' => 'ItemGroup03Controller@deletePhoto'
            ]);
        });

        Route::group(['prefix' => 'storageRow'], function () {
            Route::post('select2', [
                'uses' => 'StorageRowController@select2'
            ]);
        });

        Route::group(['prefix' => 'storageBay'], function () {
            Route::post('select2', [
                'uses' => 'StorageBayController@select2'
            ]);
        });

        Route::group(['prefix' => 'location'], function () {
            Route::post('select2', [
                'uses' => 'LocationController@select2'
            ]);
        });

        Route::group(['prefix' => 'whseReport'], function () {
            Route::post('stockBalance/{siteFlowId}', [
                'uses' => 'WhseReportController@stockBalance'
            ]);

            Route::get('initStockBalance/{siteFlowId}', [
                'uses' => 'WhseReportController@initStockBalance'
            ]);

            Route::post('cycleCountAnalysis/{siteFlowId}', [
                'uses' => 'WhseReportController@cycleCountAnalysis'
            ]);

            Route::get('initCycleCountAnalysis/{siteFlowId}', [
                'uses' => 'WhseReportController@initCycleCountAnalysis'
            ]);

            Route::post('stockCard/{siteFlowId}', [
                'uses' => 'WhseReportController@stockCard'
            ]);

            Route::get('initStockCard/{siteFlowId}', [
                'uses' => 'WhseReportController@initStockCard'
            ]);

            Route::post('countAdjAnalysis/{siteFlowId}', [
                'uses' => 'WhseReportController@countAdjAnalysis'
            ]);

            Route::get('initCountAdjAnalysis/{siteFlowId}', [
                'uses' => 'WhseReportController@initCountAdjAnalysis'
            ]);

            Route::post('reservedStock/{siteFlowId}', [
                'uses' => 'WhseReportController@reservedStock'
            ]);

            Route::get('initReservedStock/{siteFlowId}', [
                'uses' => 'WhseReportController@initReservedStock'
            ]);
        });

        Route::group(['prefix' => 'binTrf'], function () {
            Route::get('indexProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'BinTrfController@indexProcess'
            ]);

            Route::post('createProcess/{strProcType}', [
                'uses' => 'BinTrfController@createProcess'
            ]);

            Route::post('transitionToStatus', [
                'uses' => 'BinTrfController@transitionToStatus'
            ]);

            Route::get('printProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'BinTrfController@printProcess'
            ]);

            Route::get('showHeader/{hdrId}', [
                'uses' => 'BinTrfController@showHeader'
            ]);

            Route::get('showDetails/{hdrId}', [
                'uses' => 'BinTrfController@showDetails'
            ]);

            Route::post('createHeader', [
                'uses' => 'BinTrfController@createHeader'
            ]);

            Route::put('updateHeader', [
                'uses' => 'BinTrfController@updateHeader'
            ]);

            Route::post('createDetail/{hdrId}', [
                'uses' => 'BinTrfController@createDetail'
            ]);

            Route::delete('deleteDetails/{hdrId}', [
                'uses' => 'BinTrfController@deleteDetails'
            ]);

            Route::put('updateDetails/{hdrId}', [
                'uses' => 'BinTrfController@updateDetails'
            ]);

            Route::get('initHeader/{siteFlowId}', [
                'uses' => 'BinTrfController@initHeader'
            ]);

            Route::get('index/{siteFlowId}', [
                'uses' => 'BinTrfController@index'
            ]);

            Route::post('changeQuantBal', [
                'uses' => 'BinTrfController@changeQuantBal'
            ]);

            Route::post('changeItemUom', [
                'uses' => 'BinTrfController@changeItemUom'
            ]);
        });

        Route::group(['prefix' => 'creditTerm'], function () {
            Route::post('select2', [
                'uses' => 'CreditTermController@select2'
            ]);
        });

        Route::group(['prefix' => 'currency'], function () {
            Route::post('select2', [
                'uses' => 'CurrencyController@select2'
            ]);
        });

        Route::group(['prefix' => 'uom'], function () {
            Route::post('select2', [
                'uses' => 'UomController@select2'
            ]);
        });

        Route::group(['prefix' => 'itemUom'], function () {
            Route::post('select2', [
                'uses' => 'ItemUomController@select2'
            ]);

            Route::get('deleteItemUom/{item_sale_prices_id}',[
                'uses' => 'ItemUomController@deleteItemUom'
            ]);
        });

        Route::group(['prefix' => 'itemCond01'], function () {
            Route::post('select2', [
                'uses' => 'ItemCond01Controller@select2'
            ]);
        });

        Route::group(['prefix' => 'itemBatch'], function () {
            Route::post('select2', [
                'uses' => 'ItemBatchController@select2'
            ]);
        });

        Route::group(['prefix' => 'slsRtn'], function () {
            Route::get('syncProcess/{strProcType}/{divisionId}', [
                'uses' => 'SlsRtnController@syncProcess'
            ]);

            Route::get('showHeader/{hdrId}', [
                'uses' => 'SlsRtnController@showHeader'
            ]);

            Route::get('showDetails/{hdrId}', [
                'uses' => 'SlsRtnController@showDetails'
            ]);

            Route::post('createDetail/{hdrId}', [
                'uses' => 'SlsRtnController@createDetail'
            ]);

            Route::put('updateDetails/{hdrId}', [
                'uses' => 'SlsRtnController@updateDetails'
            ]);

            Route::delete('deleteDetails/{hdrId}', [
                'uses' => 'SlsRtnController@deleteDetails'
            ]);

            Route::post('transitionToStatus', [
                'uses' => 'SlsRtnController@transitionToStatus'
            ]);

            Route::post('changeItem', [
                'uses' => 'SlsRtnController@changeItem'
            ]);

            Route::post('changeItemUom', [
                'uses' => 'SlsRtnController@changeItemUom'
            ]);

            Route::post('changeDeliveryPoint', [
                'uses' => 'SlsRtnController@changeDeliveryPoint'
            ]);

            Route::post('changeCurrency', [
                'uses' => 'SlsRtnController@changeCurrency'
            ]);

            Route::get('index/{divisionId}', [
                'uses' => 'SlsRtnController@index'
            ]);
        });

        Route::group(['prefix' => 'salesReport'], function () {
            Route::post('outbOrdAnalysis/{siteFlowId}', [
                'uses' => 'SalesReportController@outbOrdAnalysis'
            ]);

            Route::get('initOutbOrdAnalysis/{siteFlowId}', [
                'uses' => 'SalesReportController@initOutbOrdAnalysis'
            ]);
        });

        Route::group(['prefix' => 'pickFaceStrategy'], function () {
            Route::get('indexProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'PickFaceStrategyController@indexProcess'
            ]);

            Route::get('downloadProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'PickFaceStrategyController@downloadProcess'
            ]);

            Route::post('uploadProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'PickFaceStrategyController@uploadProcess'
            ]);
        });

        Route::group(['prefix' => 'role'], function () {
            Route::get('indexProcess/{strProcType}', [
                'uses' => 'RoleController@indexProcess'
            ]);

            Route::get('showPermissions/{id}', [
                'uses' => 'RoleController@showPermissions'
            ]);

            Route::post('uploadProcess/{strProcType}/{siteFlowId}', [
                'uses' => 'RoleController@uploadProcess'
            ]);

            Route::post('select2', [
                'uses' => 'RoleController@select2'
            ]);

            Route::put('updatePermissions/{id}', [
                'uses' => 'RoleController@updatePermissions'
            ]);

            Route::delete('deletePermissions/{id}', [
                'uses' => 'RoleController@deletePermissions'
            ]);
        });

        Route::group(['prefix' => 'permission'], function () {
            Route::post('select2', [
                'uses' => 'PermissionController@select2'
            ]);
        });

        Route::group(['prefix' => 'delOrd'], function () {
            Route::get('index/{divisionId}', [
                'uses' => 'DelOrdController@index'
            ]);

            Route::get('showHeader/{hdrId}', [
                'uses' => 'DelOrdController@showHeader'
            ]);

            Route::get('showDetails/{hdrId}', [
                'uses' => 'DelOrdController@showDetails'
            ]);

            Route::post('createDetail/{hdrId}', [
                'uses' => 'DelOrdController@createDetail'
            ]);

            Route::put('updateDetails/{hdrId}', [
                'uses' => 'DelOrdController@updateDetails'
            ]);

            Route::delete('deleteDetails/{hdrId}', [
                'uses' => 'DelOrdController@deleteDetails'
            ]);

            Route::post('transitionToStatus', [
                'uses' => 'DelOrdController@transitionToStatus'
            ]);

            Route::get('initHeader/{divisionId}', [
                'uses' => 'DelOrdController@initHeader'
            ]);

            Route::post('createHeader', [
                'uses' => 'DelOrdController@createHeader'
            ]);

            Route::put('updateHeader', [
                'uses' => 'DelOrdController@updateHeader'
            ]);
        });

        Route::group(['prefix' => 'audit'], function () {

            Route::post('auditResource', [
                'uses' => 'AuditController@auditResource'
            ]);

            Route::post('auditUser', [
                'uses' => 'AuditController@auditUser'
            ]);

            Route::post('auditLog', [
                'uses' => 'AuditController@auditLog'
            ]);

            Route::get('showAuditTypes/{resType}', [
                'uses' => 'AuditController@showAuditTypes'
            ]);
        });

        Route::group(['prefix' => 'cart'], function () {
            Route::get('index/{divisionId}', [
                'uses' => 'CartController@index'
            ]);

            Route::get('showHeader/{hdrId}', [
                'uses' => 'CartController@showHeader'
            ]);

            Route::get('showDetails/{hdrId}', [
                'uses' => 'CartController@showDetails'
            ]);

            Route::post('createDetail/{hdrId}', [
                'uses' => 'CartController@createDetail'
            ]);

            Route::put('updateDetails/{hdrId}', [
                'uses' => 'CartController@updateDetails'
            ]);

            Route::delete('deleteDetails/{hdrId}', [
                'uses' => 'CartController@deleteDetails'
            ]);

            Route::get('syncProcess/{strProcType}/{divisionId}', [
                'uses' => 'CartController@syncProcess'
            ]);

            Route::post('transitionToStatus', [
                'uses' => 'CartController@transitionToStatus'
            ]);

            Route::post('changeItem', [
                'uses' => 'CartController@changeItem'
            ]);

            Route::post('changeItemUom', [
                'uses' => 'CartController@changeItemUom'
            ]);

            Route::post('changeDeliveryPoint', [
                'uses' => 'CartController@changeDeliveryPoint'
            ]);

            Route::post('changeCurrency', [
                'uses' => 'CartController@changeCurrency'
            ]);

            Route::get('initHeader/{divisionId}', [
                'uses' => 'CartController@initHeader'
            ]);

            Route::post('createHeader', [
                'uses' => 'CartController@createHeader'
            ]);

            Route::put('updateHeader', [
                'uses' => 'CartController@updateHeader'
            ]);

            Route::post('findOrCreateDraftHeader', [
                'uses' => 'CartController@findOrCreateDraftHeader'
            ]);

            Route::post('updateCartItem/{hdrId}', [
                'uses' => 'CartController@updateCartItem'
            ]);

            Route::post('reapplyCartPromotions/{hdrId}', [
                'uses' => 'CartController@reapplyCartPromotions'
            ]);

            Route::post('updateDeliveryPoint', [
                'uses' => 'CartController@updateDeliveryPoint'
            ]);

            Route::post('placeOrder/{hdrId}', [
                'uses' => 'CartController@placeOrder'
            ]);

            Route::get('checkPromotion/{dtlId}', [
                'uses' => 'CartController@checkPromotion'
            ]);

            Route::post('applyConflict', [
                'uses' => 'CartController@applyConflict'
            ]);
        });

        Route::group(['prefix' => 'debtor'], function () {
            Route::get('showHeader/{hdrId}', [
                'uses' => 'DebtorController@showHeader'
            ]);

            Route::put('updateHeader', [
                'uses' => 'DebtorController@updateHeader'
            ]);

            Route::delete('deleteHeader', [
                'uses' => 'DebtorController@deleteHeader'
            ]);

            Route::post('createHeader/{divisionId}', [
                'uses' => 'DebtorController@createHeader'
            ]);

            Route::get('initHeader/{divisionId}', [
                'uses' => 'DebtorController@initHeader'
            ]);

            Route::put('changeStatus', [
                'uses' => 'DebtorController@changeStatus'
            ]);

            Route::get('indexProcess/{strProcType}', [
                'uses' => 'DebtorController@indexProcess'
            ]);

            Route::get('indexDivision/{divisionId}', [
                'uses' => 'DebtorController@indexDivision'
            ]);

            Route::get('syncProcess/{strProcType}/{siteFlowId}/{divisionId}', [
                'uses' => 'DebtorController@syncProcess'
            ]);

            Route::post('select2Debtor', [
                'uses' => 'DebtorController@select2Debtor'
            ]);

            Route::get('select2UpdateDebtor/{debtor_id}', [
                'uses' => 'DebtorController@select2UpdateDebtor'
            ]);

            Route::get('getDeliveryPoint/{debtor_id}', [
                'uses' => 'DebtorController@getDeliveryPoint'
            ]);

            Route::post('select2', [
                'uses' => 'DebtorController@select2'
            ]);

            Route::post('select2Init', [
                'uses' => 'DebtorController@select2Init'
            ]);

            Route::get('showDivisions/{debtorId}', [
                'uses' => 'DebtorController@showDivisions'
            ]);

            Route::put('updateDivisions/{debtorId}', [
                'uses' => 'DebtorController@updateDivisions'
            ]);

            Route::put('createDivisions/{debtorId}', [
                'uses' => 'DebtorController@createDivisions'
            ]);

            Route::delete('deleteDivisions/{debtorId}', [
                'uses' => 'DebtorController@deleteDivisions'
            ]);

            Route::get('showModel/{debtorId}', [
                'uses' => 'DebtorController@showModel'
            ]);
        });

        Route::group(['prefix' => 'debtorGroup01'], function () {
            Route::post('select2', [
                'uses' => 'DebtorGroup01Controller@select2'
            ]);
        });

        Route::group(['prefix' => 'area'], function () {
            Route::post('select2', [
                'uses' => 'AreaController@select2'
            ]);
        });

        Route::group(['prefix' => 'state'], function () {
            Route::post('select2', [
                'uses' => 'StateController@select2'
            ]);
        });

        Route::group(['prefix' => 'promotion'], function () {
            Route::post('select2', [
                'uses' => 'PromotionController@select2'
            ]);

            Route::get('findAllActiveByItem/{itemId}', [
                'uses' => 'PromotionController@findAllActiveByItem'
            ]);

            Route::get('findAllRelatedItems', [
                'uses' => 'PromotionController@findAllRelatedItems'
            ]);

            Route::get('findPromotion', [
                'uses' => 'PromotionController@findPromotion'
            ]);

            Route::get('findPromotionItems', [
                'uses' => 'PromotionController@findPromotionItems'
            ]);

            Route::get('index/{divisionId}', [
                'uses' => 'PromotionController@index'
            ]);

            Route::get('index/disc/{divisionId}', [
                'uses' => 'PromotionController@indexDisc'
            ]);

            Route::get('index/foc/{divisionId}', [
                'uses' => 'PromotionController@indexFoc'
            ]);

            Route::get('showModel/{promotionId}', [
                'uses' => 'PromotionController@showModel'
            ]);

            Route::get('initModel/{divisionId}/{strPromoType}', [
                'uses' => 'PromotionController@initModel'
            ]);

            Route::post('createModel/{divisionId}/{strPromoType}', [
                'uses' => 'PromotionController@createModel'
            ]);

            Route::put('updateModel', [
                'uses' => 'PromotionController@updateModel'
            ]);

            Route::delete('deleteModel', [
                'uses' => 'PromotionController@deleteModel'
            ]);

            Route::get('showVariants/{promotionId}', [
                'uses' => 'PromotionController@showVariants'
            ]);

            Route::get('showMainVariants/{promotionId}', [
                'uses' => 'PromotionController@showMainVariants'
            ]);

            Route::get('showAddOnVariants/{promotionId}', [
                'uses' => 'PromotionController@showAddOnVariants'
            ]);

            Route::post('createVariant/{promotionId}', [
                'uses' => 'PromotionController@createVariant'
            ]);

            Route::put('updateVariant/{promotionId}', [
                'uses' => 'PromotionController@updateVariant'
            ]);

            Route::delete('deleteVariant/{promotionId}', [
                'uses' => 'PromotionController@deleteVariant'
            ]);

            Route::get('showRules/{promotionId}', [
                'uses' => 'PromotionController@showRules'
            ]);

            Route::post('createRule/{promotionId}', [
                'uses' => 'PromotionController@createRule'
            ]);

            Route::post('updateRule/{promotionId}', [
                'uses' => 'PromotionController@updateRule'
            ]);

            Route::delete('deleteRule/{promotionId}', [
                'uses' => 'PromotionController@deleteRule'
            ]);

            Route::post('changeItem', [
                'uses' => 'PromotionController@changeItem'
            ]);

            Route::post('changeItemGroup', [
                'uses' => 'PromotionController@changeItemGroup'
            ]);

            Route::post('uploadPhotos/{promotionId}', [
                'uses' => 'PromotionController@uploadPhotos'
            ]);

            Route::post('deletePhotos/{promotionId}', [
                'uses' => 'PromotionController@deletePhotos'
            ]);

            Route::get('syncProcess/{strProcType}/{siteFlowId}/{divisionId}', [
                'uses' => 'PromotionController@syncProcess'
            ]);

            Route::post('reorderPromo', [
                'uses' => 'PromotionController@reorderPromo'
            ]);

            Route::post('broadcastPromo', [
                'uses' => 'PromotionController@broadcastPromo'
            ]);
        });

        Route::group(['prefix' => 'notifications'], function () {
            Route::post('register', [
                'uses' => 'NotificationController@register'
            ]);
            Route::post('unregister', [
                'uses' => 'NotificationController@unregister'
            ]);
            Route::post('notify', [
                'uses' => 'NotificationController@notify'
            ]);
        });
    });
});
