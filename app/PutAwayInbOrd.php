<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PutAwayInbOrd extends Model implements Auditable
{
    //
    use \OwenIt\Auditing\Auditable;

    public function inbOrdHdr()
    {
        return $this->belongsTo(\App\InbOrdHdr::class, 'inb_ord_hdr_id', 'id');
    }

    public function putAwayHdr()
    {
        return $this->belongsTo(\App\PutAwayHdr::class, 'hdr_id', 'id');
    }

    public function generateTags(): array
    {
        $docCode = $this->putAwayHdr->doc_code;
        unset($this->putAwayHdr);
        return array(
            $docCode,
        );
    }
}
