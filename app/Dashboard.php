<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Dashboard extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'dashboard';

    public function generateTags(): array
    {
        return array(
            $this->code
        );
    }

    public function userId()
    {
        return $this->belongsTo(\App\User::class, 'user_id', 'id');
    }
}
