<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class AdvShipItem extends Model implements Auditable
{
    //
    use \OwenIt\Auditing\Auditable;

    protected $casts = [
        'case_uom_rate' => 'decimal:6',
    ];

    public function advShipHdr()
    {
        return $this->belongsTo(\App\AdvShipHdr::class, 'hdr_id', 'id');
    }

    public function generateTags(): array
    {
        $docCode = $this->advShipHdr->doc_code;
        unset($this->advShipHdr);
        return array(
            $docCode,
        );
    }
}
