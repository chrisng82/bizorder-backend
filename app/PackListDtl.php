<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PackListDtl extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    public function packListHdr()
    {
        return $this->belongsTo(\App\PackListHdr::class, 'hdr_id', 'id');
    }

    public function item()
    {
        return $this->belongsTo(\App\Item::class, 'item_id', 'id');
    }

    public function quantBal()
    {
        return $this->belongsTo(\App\QuantBal::class, 'quant_bal_id', 'id');
    }

    public function generateTags(): array
    {
        $docCode = $this->packListHdr->doc_code;
        unset($this->packListHdr);
        return array(
            $docCode
        );
    }
}
