<?php

namespace App\Observers;

use App\Models\Message;
use App\Http\Controllers\NotificationController;
use App\Notifications\MessageProcessingNotification;

class MessageObserver
{
    public function created(Message $message) {
        $payload = json_decode($message->payload);

        if($payload->navigation == 'PromotionsScreen') {
            $message->notify(new MessageProcessingNotification($message));
        }
    }
}