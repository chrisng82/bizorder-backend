<?php

namespace App\Observers;

use App\SlsOrdHdr;
use App\Http\Controllers\NotificationController;
use App\Notifications\OrderProcessingNotification;

class SlsOrdHdrObserver
{
    public function updated(SlsOrdHdr $slsOrdHdr) {
        if($slsOrdHdr->wasChanged('doc_status')) {
            $slsOrdHdr->notify(new OrderProcessingNotification($slsOrdHdr));
        }
    }
}
