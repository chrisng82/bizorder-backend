<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class WhseJobItem extends Model implements Auditable
{
    //
    use \OwenIt\Auditing\Auditable;

    protected $casts = [
        'case_uom_rate' => 'decimal:6',
    ];
    
    public function docItem()
    {
        return $this->morphTo();
    }

    public function whseJobHdr()
    {
        return $this->belongsTo(\App\WhseJobHdr::class, 'hdr_id', 'id');
    }

    public function generateTags(): array
    {
        $docCode = $this->whseJobHdr->doc_code;
        unset($this->whseJobHdr);
        return array(
            $docCode,
        );
    }
}
