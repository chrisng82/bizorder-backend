<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PickFaceStrategy extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //

    public function site()
    {
        return $this->belongsTo(\App\Site::class, 'site_id', 'id');
    }

    public function storageBin()
    {
        return $this->belongsTo(\App\StorageBin::class, 'storage_bin_id', 'id');
    }

    public function item()
    {
        return $this->belongsTo(\App\Item::class, 'item_id', 'id');
    }
}
