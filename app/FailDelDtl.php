<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class FailDelDtl extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //
    protected $casts = [
        'uom_rate' => 'decimal:6',
        'qty' => 'decimal:6',
    ];

    public function failDelHdr()
    {
        return $this->belongsTo(\App\FailDelHdr::class, 'hdr_id', 'id');
    }

    public function generateTags(): array
    {
        $docCode = $this->failDelHdr->doc_code;
        unset($this->failDelHdr);
        return array(
            $docCode
        );
    }
}
