<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ItemPurPrice extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //
    protected $casts = [
        'sale_price' => 'decimal:8',
    ];

    public function item()
    {
        return $this->belongsTo(\App\Item::class, 'item_id', 'id');
    }

    public function generateTags(): array
    {
        $code = $this->item->code;
        unset($this->item);
        return array(
            $code
        );
    }
}
