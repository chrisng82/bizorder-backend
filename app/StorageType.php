<?php

namespace App;

use App\Services\Env\BinType;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class StorageType extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $casts = [
        'hu_min_load_weight' => 'decimal:4',
        'hu_max_load_weight' => 'decimal:4',
    ];

    public function generateTags(): array
    {
        return array(
            $this->code
        );
    }

    public function isPickFace()
    {
        if($this->bin_type == BinType::$MAP['CASE_STORAGE']
        || $this->bin_type == BinType::$MAP['BROKEN_CASE_STORAGE'])
        {
            return true;
        }
        return false;
    }

    public function isPallet()
    {
        if($this->bin_type == BinType::$MAP['BULK_STORAGE']
        || $this->bin_type == BinType::$MAP['PALLET_STORAGE']
        || $this->bin_type == BinType::$MAP['BROKEN_PALLET_STORAGE'])
        {
            return true;
        }
        return false;
    }
}
