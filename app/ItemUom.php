<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ItemUom extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $casts = [
        'uom_rate' => 'decimal:6',
    ];

    protected $appends = ['salePrice'];

    public $salePrice;

    public function getSalePriceAttribute() {
        return $this->salePrice;
    }
    
    public function item()
    {
        return $this->belongsTo(\App\Item::class, 'item_id', 'id');
    }

    public function uom()
    {
        return $this->belongsTo(\App\Uom::class, 'uom_id', 'id');
    }

    public function generateTags(): array
    {
        $code = $this->item->code;
        unset($this->item);
        return array(
            $code
        );
    }
}
