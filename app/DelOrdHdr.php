<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class DelOrdHdr extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $casts = [
        'doc_date' => 'date',
        'est_del_date' => 'date',
        'currency_rate' => 'decimal:8',
        'gross_amt' => 'decimal:8',
        'gross_local_amt' => 'decimal:8',
        'disc_amt' => 'decimal:8',
        'disc_local_amt' => 'decimal:8',
        'tax_amt' => 'decimal:8',
        'tax_local_amt' => 'decimal:8',
        'round_adj_amt' => 'decimal:8',
        'round_adj_local_amt' => 'decimal:8',
        'net_amt' => 'decimal:8',
        'net_local_amt' => 'decimal:8',
    ];
    
    public function siteFlow()
    {
        return $this->belongsTo(\App\SiteFlow::class, 'site_flow_id', 'id');
    }

    public function generateTags(): array
    {
        return array(
            $this->doc_code
        );
    }
}
