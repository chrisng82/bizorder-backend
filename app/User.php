<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Contracts\Auth\Access\Authorizable;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements Auditable, Authorizable 
{
    use HasRoles;
    use HasApiTokens;
    use Notifiable;
    use \OwenIt\Auditing\Auditable;
    use \Illuminate\Foundation\Auth\Access\Authorizable;
    
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'password',
        'first_name',
        'last_name',
        'desc_01',
        'desc_02',
        'password_changed_at',
        'status',
        'login_type',
        'debtor_id',
    ];

    protected $loginNames = ['username','email'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getAuthIdentifier()
    {
        return $this->id;
    }

    public function userDivisions()
    {
        return $this->hasMany(\App\UserDivision::class);
    }

    public function devices() {
        return $this->hasMany(\App\Device::class);
    }
    
    public function generateTags(): array
    {
        return array(
            $this->username
        );
    }

    public function findForPassport($username)
    {
        return $this->where('username', $username)->first();
    }


    public function validateForPassportPasswordGrant($password)
    {
        return Hash::check($password, $this->password);
    }

    protected $auditExclude = [
        'password',
        'remember_token'
    ];
}
