<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class RtnRcptDtl extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //
    public function rtnRcptHdr()
    {
        return $this->belongsTo(\App\RtnRcptHdr::class, 'hdr_id', 'id');
    }

    public function item()
    {
        return $this->belongsTo(\App\Item::class, 'item_id', 'id');
    }

    public function uom()
    {
        return $this->belongsTo(\App\Uom::class, 'uom_id', 'id');
    }

    public function location()
    {
        return $this->belongsTo(\App\Location::class, 'location_id', 'id');
    }

    public function reason01()
    {
        return $this->belongsTo(\App\Reason01::class, 'reason_01_id', 'id');
    }

    public function generateTags(): array
    {
        $docCode = $this->rtnRcptHdr->doc_code;
        unset($this->rtnRcptHdr);
        return array(
            $docCode,
        );
    }
}
