<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuantBal extends Model
{
    protected $casts = [
        'balance_unit_qty' => 'decimal:8',
    ];

    public function site()
    {
        return $this->belongsTo(\App\Site::class, 'site_id', 'id');
    }

    public function company()
    {
        return $this->belongsTo(\App\Company::class, 'company_id', 'id');
    }

    public function item()
    {
        return $this->belongsTo(\App\Item::class, 'item_id', 'id');
    }

    public function itemBatch()
    {
        return $this->belongsTo(\App\ItemBatch::class, 'item_batch_id', 'id');
    }

    public function storageBin()
    {
        return $this->belongsTo(\App\StorageBin::class, 'storage_bin_id', 'id');
    }

    public function storageRow()
    {
        return $this->belongsTo(\App\StorageRow::class, 'storage_row_id', 'id');
    }

    public function storageType()
    {
        return $this->belongsTo(\App\StorageType::class, 'storage_type_id', 'id');
    }

    public function storageBay()
    {
        return $this->belongsTo(\App\StorageBay::class, 'storage_bay_id', 'id');
    }

    public function handlingUnit()
    {
        return $this->belongsTo(\App\HandlingUnit::class, 'handling_unit_id', 'id');
    }
}
