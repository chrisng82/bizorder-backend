<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PickListOutbOrd extends Model implements Auditable
{
    //
    use \OwenIt\Auditing\Auditable;

    public function outbOrdHdr()
    {
        return $this->belongsTo(\App\OutbOrdHdr::class, 'outb_ord_hdr_id', 'id');
    }

    public function pickListHdr()
    {
        return $this->belongsTo(\App\PickListHdr::class, 'hdr_id', 'id');
    }

    public function generateTags(): array
    {
        $docCode = $this->pickListHdr->doc_code;
        unset($this->pickListHdr);
        return array(
            $docCode
        );
    }
}
