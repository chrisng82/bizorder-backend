<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class DebtorGroup03 extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $fillable = ['code','desc_01','desc_02'];

    public function generateTags(): array
    {
        return array(
            $this->code
        );
    }
}
