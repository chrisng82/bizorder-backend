<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PrfDelDtl extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //
    protected $casts = [
        'uom_rate' => 'decimal:6',
        'qty' => 'decimal:6',
    ];

    public function prfDelHdr()
    {
        return $this->belongsTo(\App\PrfDelHdr::class, 'hdr_id', 'id');
    }

    public function generateTags(): array
    {
        $docCode = $this->prfDelHdr->doc_code;
        unset($this->prfDelHdr);
        return array(
            $docCode
        );
    }
}
