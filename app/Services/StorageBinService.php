<?php

namespace App\Services;

use App\Services\Env\ProcType;
use App\Services\Env\ResStatus;
use App\Imports\StorageBinExcel01Import;
use App\Repositories\SiteFlowRepository;
use App\Repositories\StorageBinRepository;
use App\Repositories\StorageRowRepository;
use App\Repositories\BatchJobStatusRepository;
use App\Repositories\StorageBayRepository;
use App\Repositories\QuantBalRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class StorageBinService 
{
    public static $STORAGE_BIN_EXCEL_01 = array(        
        'CODE' => 'code',
        'BARCODE' => 'barcode',
        'DESC_01' => 'desc_01',
        'DESC_02' => 'desc_02',
        'STATUS' => 'status',
        'AISLE' => 'aisle',
        'AISLE_SEQ' => 'aisle_sequence',
        'AISLE_POS' => 'aisle_position',
        'ROW' => 'row',
        'BAY' => 'bay',
        'FLOOR' => 'floor',
        'LEVEL' => 'level',
        'LOCATION' => 'location',
        'LOCATION_TYPE' => 'location_type',
        'STORAGE_TYPE' => 'storage_type',
        'HANDLING_TYPE' => 'handling_type',
        'BIN_TYPE' => 'bin_type',
        'RACK_TYPE' => 'rack_type',
        'MIN_LOAD_LENGTH' => 'min_load_length',
        'MIN_LOAD_WIDTH' => 'min_load_width',
        'MIN_LOAD_HEIGHT' => 'min_load_height',
        'MIN_LOAD_WEIGHT' => 'min_load_weight',
        'MAX_LOAD_LENGTH' => 'max_load_length',
        'MAX_LOAD_WIDTH' => 'max_load_width',
        'MAX_LOAD_HEIGHT' => 'max_load_height',
        'MAX_LOAD_WEIGHT' => 'max_load_weight',
        'MAX_PALLET' => 'max_pallet',
    );

    public function __construct() 
	{
    }
    
    public function indexProcess($strProcType, $siteFlowId, $sorts, $filters = array(), $pageSize = 20)
    {
        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['STORAGE_BIN_LIST_01'])
			{
				//storage bin listing
				$storageBins = $this->indexStorageBinList01($siteFlowId, $sorts, $filters, $pageSize);
				return $storageBins;
            }
            elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['STORAGE_BIN_LIST_02'])
            {
                //storage bin by storage row/level
				$storageBins = $this->indexStorageBinList02($siteFlowId, $sorts, $filters, $pageSize);
				return $storageBins;
            }
		}	
    }

    protected function indexStorageBinList01($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
            array(
                'storage_bin_read'
            )
        );

		$storageBins = StorageBinRepository::findAll($sorts, $filters, $pageSize);
        
        $storageBins->load('site', 'location', 'storageBay', 'storageRow', 'storageType');
		foreach($storageBins as $storageBin)
		{
            $storageBin->str_bin_status = ResStatus::$MAP[$storageBin->bin_status];

            $storageRow = $storageBin->storageRow;

            $storageBin->site_code = !empty($storageBin->site) ? $storageBin->site->code : '';            
            $storageBin->location_code = !empty($storageBin->location) ? $storageBin->location->code : '';                 
            $storageBin->storage_bay_code = !empty($storageBin->storageBay) ? $storageBin->storageBay->code : '';              
            $storageBin->storage_row_code = $storageRow->code;
            $storageBin->aisle_code = $storageRow->aisle_code;
            $storageBin->storage_type_code = !empty($storageBin->storageType) ? $storageBin->storageType->code : '';
            
            unset($storageBin->site);
            unset($storageBin->location);
            unset($storageBin->storageBay);
            unset($storageBin->storageRow);
            unset($storageBin->storageType);
		}
        return $storageBins;
    }

    protected function convertBinIndexToBinAbbreviation($binIndex)
    {
        if($binIndex == 0)
        {
            return 'A';
        }
        elseif($binIndex == 1)
        {
            return 'B';
        }
        elseif($binIndex == 2)
        {
            return 'C';
        }
        elseif($binIndex == 3)
        {
            return 'D';
        }
        elseif($binIndex == 4)
        {
            return 'E';
        }
        elseif($binIndex == 5)
        {
            return 'F';
        }
        elseif($binIndex == 6)
        {
            return 'G';
        }
        elseif($binIndex == 7)
        {
            return 'H';
        }
        return '';
    }

    protected function indexStorageBinList02($siteFlowId, $sorts, $filters = array(), $pageSize = 2)
	{
        AuthService::authorize(
            array(
                'stock_balance_report'
            )
        );

        $siteFlow = SiteFlowRepository::findByPk($siteFlowId);
        
        $rowLevels = StorageBinRepository::queryByRowLevel($siteFlow->site_id, $sorts, $filters, $pageSize);

        //calculate the columns label (bayAbbreviation, bin)
        $maxBinCountHashByBayAbbreviation = array();
        $maxRowBinCount = 0;
        foreach($rowLevels as $rowLevel)
        {
            $storageBinHashByBayAbbreviation = array();
            //order by bayCode
            $storageBins = StorageBinRepository::findAllByRowLevel($rowLevel->storage_row_id, $rowLevel->level);
            //format storageBins hash by bayAbbreviation
            foreach($storageBins as $storageBin) 
            {
                //get the bayAbbreviation from bayCode
                $bayCodeParts = explode('-', $storageBin->storage_bay_code);
                $bayAbbreviation = $bayCodeParts[count($bayCodeParts) - 1];

                $tmpDtls = array();
                if(array_key_exists($bayAbbreviation, $storageBinHashByBayAbbreviation))
                {
                    $tmpDtls = $storageBinHashByBayAbbreviation[$bayAbbreviation];
                }
                $tmpDtls[] = $storageBin;
                $storageBinHashByBayAbbreviation[$bayAbbreviation] = $tmpDtls;
            }

            //loop the storageBinHashByBayAbbreviation, to get the bayAbbreviation binCount
            foreach($storageBinHashByBayAbbreviation as $bayAbbreviation => $bayStorageBins)
            {
                $maxBayBinCount = 0;
                if(array_key_exists($bayAbbreviation, $maxBinCountHashByBayAbbreviation))
                {
                    $maxBayBinCount = $maxBinCountHashByBayAbbreviation[$bayAbbreviation];
                }

                $bayBinCount = count($bayStorageBins);
                if(bccomp($bayBinCount, $maxBayBinCount) > 0)
                {
                    $maxBayBinCount = $bayBinCount;
                }
                $maxBinCountHashByBayAbbreviation[$bayAbbreviation] = $maxBayBinCount;
            }

            $rowBinCount = count($storageBins);
            if(bccomp($rowBinCount, $maxRowBinCount) > 0)
            {
                $maxRowBinCount = $rowBinCount;
            }
        }

		foreach($rowLevels as $rowLevel)
		{
            $rowLevel->max_row_bin_count = $maxRowBinCount;

            $storageBinHashByBayAbbreviation = array();
            $storageBins = StorageBinRepository::findAllByRowLevel($rowLevel->storage_row_id, $rowLevel->level);
            //format storageBins hash by bayAbbreviation
            foreach($storageBins as $storageBin) 
            {
                //get the bayAbbreviation from bayCode
                $bayCodeParts = explode('-', $storageBin->storage_bay_code);
                $bayAbbreviation = $bayCodeParts[count($bayCodeParts) - 1];

                $tmpDtls = array();
                if(array_key_exists($bayAbbreviation, $storageBinHashByBayAbbreviation))
                {
                    $tmpDtls = $storageBinHashByBayAbbreviation[$bayAbbreviation];
                }
                $tmpDtls[] = $storageBin;
                $storageBinHashByBayAbbreviation[$bayAbbreviation] = $tmpDtls;
            }

            //format the quantBals by storageBinId
            $quantBalsHashByStorageBinId = array();
            $quantBals = QuantBalRepository::findAllActiveByStorageRowIdAndLevel($rowLevel->storage_row_id, $rowLevel->level);
            $quantBals->load('item', 'itemBatch', 'handlingUnit');
            foreach($quantBals as $quantBal)
            {
                $item = $quantBal->item;
                $itemBatch = $quantBal->itemBatch;
                $handlingUnit = $quantBal->handlingUnit;

                $quantBal->handling_unit_barcode = empty($handlingUnit) ? '' : $handlingUnit->getBarcode();
                $quantBal->handling_unit_ref_code_01 = empty($handlingUnit) ? '' : $handlingUnit->ref_code_01;

                $quantBal->expiry_date = $itemBatch->expiry_date;
                $quantBal->receipt_date = $itemBatch->receipt_date;
                $quantBal->batch_serial_no = $itemBatch->batch_serial_no;

                $quantBal = ItemService::processCaseLoose($quantBal, $item, 1);
                unset($quantBal->item);

                $tmpDtls = array();
                if(array_key_exists($quantBal->storage_bin_id, $quantBalsHashByStorageBinId))
                {
                    $tmpDtls = $quantBalsHashByStorageBinId[$quantBal->storage_bin_id];
                }
                $tmpDtls[] = $quantBal;
                $quantBalsHashByStorageBinId[$quantBal->storage_bin_id] = $tmpDtls;
            }
            
            //rowLevel->bays
            //$baysStatus = array();
            //first column is level
            $bayAbbreviationCountHash = array();
            $bayAbbreviationCountHash[''] = 1;
            $binAbbreviationLabels = array();
            $binAbbreviationLabels[] = array(
                'label'=>__('StorageBin.level_abbreviation'),
                'col_index'=>0
            );       
            $binsModel = array();
            $binsModel[] = array('level'=>'L'.$rowLevel->level);
            $binsQuantBals = array();
            $binsQuantBals[] = array('level'=>'L'.$rowLevel->level);
            $colIndex = 1;
            foreach($maxBinCountHashByBayAbbreviation as $bayAbbreviation => $maxBinCount)
            {
                $bayStorageBins = array();
                if(array_key_exists($bayAbbreviation, $storageBinHashByBayAbbreviation))
                {
                    $bayStorageBins = $storageBinHashByBayAbbreviation[$bayAbbreviation];
                }
                for($a = 0; $a < $maxBinCount; $a++)
                {
                    $bayAbbreviationCount = 0;
                    if(array_key_exists($bayAbbreviation, $bayAbbreviationCountHash))
                    {
                        $bayAbbreviationCount = $bayAbbreviationCountHash[$bayAbbreviation];
                    }
                    $bayAbbreviationCount++;
                    $bayAbbreviationCountHash[$bayAbbreviation] = $bayAbbreviationCount;

                    $binAbbreviationLabels[] = array(
                        'label'=>$this->convertBinIndexToBinAbbreviation($a),
                        'col_index'=>$colIndex
                    );

                    $storageBin = array_shift($bayStorageBins);;
                    $binsModel[] = $storageBin;

                    $tmpQuantBals = array();
                    if(!empty($storageBin))
                    {
                        if(array_key_exists($storageBin->id, $quantBalsHashByStorageBinId))
                        {
                            $tmpQuantBals = $quantBalsHashByStorageBinId[$storageBin->id];
                        }
                    }
                    $binsQuantBals[] = $tmpQuantBals;
                    $colIndex++;
                }
            }
            $bayAbbreviationLabels = array();
            foreach($bayAbbreviationCountHash as $bayAbbreviation => $count)
            {
                $bayAbbreviationLabels[] = array('label'=>$bayAbbreviation, 'col_span'=>$count);
            }
            //$rowLevel->bays_status = $baysStatus;
            $rowLevel->bay_abbreviation_labels = $bayAbbreviationLabels;
            $rowLevel->bin_abbreviation_labels = $binAbbreviationLabels;
            $rowLevel->bins_model = $binsModel;
            $rowLevel->bins_quant_bals = $binsQuantBals;
		}
        return $rowLevels;
    }
    
    public function uploadProcess($strProcType, $siteFlowId, $file)
    {
        $user = Auth::user();

        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['STORAGE_BIN_EXCEL_01']) 
			{
                //process storage bin excel
                $path = Storage::putFileAs('upload/', $file, 'STORAGE_BIN_EXCEL_01.XLSX');
				return $this->uploadStorageBinExcel01($siteFlowId, $user->id, $path);
			}
		}
    }

    public function uploadStorageBinExcel01($siteFlowId, $userId, $path)
    {
        AuthService::authorize(
            array(
                'storage_bin_import'
            )
        );

        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['STORAGE_BIN_EXCEL_01'], $userId);

        $siteFlow = SiteFlowRepository::findByPk($siteFlowId);

        $storageBinExcel01Import = new StorageBinExcel01Import($siteFlow->site_id, $userId, $batchJobStatusModel);
        $excel = Excel::import($storageBinExcel01Import, $path);

        //update the bay_sequence
        $baySequence = 1;
        //get all storageRow, sort by aisle_sequence, aisle_position
        $storageRows = StorageRowRepository::findAllBySiteId($siteFlow->site_id);
        foreach($storageRows as $storageRow)
        {
            if($storageRow->aisle_position % 2 == 1)
            {
                //odd, the sequence assign ASC
                //get all storageBay, sort by code ASC
                $storageBays = StorageBayRepository::findAllByStorageRowId($storageRow->id);
                foreach($storageBays as $storageBay)
                {
                    $storageBay = StorageBayRepository::updateBaySequence($storageBay, $baySequence);
                    $baySequence++;
                }
            }
            else
            {
                //even, the sequence assign DESC
                //get all storageBay, sort by code DESC
                $storageBays = StorageBayRepository::findAllByStorageRowIdDesc($storageRow->id);
                foreach($storageBays as $storageBay)
                {
                    $storageBay = StorageBayRepository::updateBaySequence($storageBay, $baySequence);
                    $baySequence++;
                }
            }
        }

        $message = __('StorageBin.file_successfully_uploaded', ['total'=>$storageBinExcel01Import->getTotal()]);

		return array(
			'data' => $storageBinExcel01Import->getTotal(),
			'message' => $message
		);
    }

    public function select2($search, $filters)
    {
        $storageBins = StorageBinRepository::select2($search, $filters);
        return $storageBins;
    }

    public function select2Init($id, $filters)
    {
        $storageBin = null;
        if($id > 0) 
        {
            $storageBin = StorageBinRepository::findByPk($id);
        }
        else
        {
            $storageBin = StorageBinRepository::findTop($filters);
        }
        $option = array('value'=>0, 'label'=>'');
        if(!empty($storageBin))
        {
            $option = array(
                'value'=>$storageBin->id, 
                'label'=>$storageBin->code.' '.$storageBin->desc_01
            );
        }
        return $option;
    }
}
