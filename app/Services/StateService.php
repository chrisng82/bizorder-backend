<?php

namespace App\Services;

use App\Repositories\StateRepository;

class StateService 
{
    public function __construct() 
	{
    }
    
    public function select2($search, $filters, $pageSize = 20)
    {
        $uoms = StateRepository::select2($search, $filters, $pageSize);
        return $uoms;
    }
}
