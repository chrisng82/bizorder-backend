<?php

namespace App\Services;

use App\Repositories\CreditTermRepository;
class CreditTermService 
{
    public function __construct() 
	{
    }
    
    public function select2($search, $filters)
    {
        $creditTerms = CreditTermRepository::select2($search, $filters);
        return $creditTerms;
    }
}
