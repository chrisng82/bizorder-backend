<?php

namespace App\Services;

use App\Uom;
use App\GdsRcptHdr;
use App\Services\Env\ResStatus;
use App\Services\Env\BinType;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Services\Env\WhseJobType;
use App\Services\ItemService;
use App\Services\Utils\ApiException;
use App\Repositories\InbOrdHdrRepository;
use App\Repositories\InbOrdDtlRepository;
use App\Repositories\AdvShipDtlRepository;
use App\Repositories\StorageTypeRepository;
use App\Repositories\SiteDocNoRepository;
use App\Repositories\WhseTxnFlowRepository;
use App\Repositories\GdsRcptHdrRepository;
use App\Repositories\GdsRcptDtlRepository;
use App\Repositories\GdsRcptItemRepository;
use App\Repositories\StorageBinRepository;
use App\Repositories\ItemUomRepository;
use App\Repositories\ItemBatchRepository;
use App\Repositories\ItemRepository;
use App\Repositories\AreaRepository;
use App\Repositories\PrintDocTxnRepository;
use App\Repositories\PrintDocSettingRepository;
use App\Repositories\DocTxnFlowRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GdsRcptService extends InventoryService
{
    public function __construct() 
	{
    }
    
    public function indexProcess($strProcType, $siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $user = Auth::user();

        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['GDS_RCPT_01']) 
            {
                //ADV_SHIP, INB_ORD -> GDS_RCPT
                $inbOrdHdrs = $this->indexGdsRcpt01($siteFlowId, $sorts, $filters, $pageSize = 20);
                return $inbOrdHdrs;
            }
            elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['GDS_RCPT_02']) 
            {
                //SLS_RTN, INB_ORD -> GDS_RCPT
                $inbOrdHdrs = $this->indexGdsRcpt02($user, $siteFlowId, $sorts, $filters, $pageSize = 20);
                return $inbOrdHdrs;
            }
            elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['GDS_RCPT_02_01']) 
            {
                //WIP SLS_RTN, INB_ORD -> GDS_RCPT
                $inbOrdHdrs = $this->indexGdsRcpt0201($user, $siteFlowId, $sorts, $filters, $pageSize = 20);
                return $inbOrdHdrs;
            }
		}

        return array();
    }

    protected function indexGdsRcpt01($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
			array(
				'inb_ord_read'
			)
        );
        
        //DB::connection()->enableQueryLog();
        $inbOrdHdrs = InbOrdHdrRepository::findAllNotExistGdsRcpt01Txn($siteFlowId, DocStatus::$MAP['COMPLETE'], $sorts, $filters, $pageSize);
        //Log::error(DB::getQueryLog());
        $inbOrdHdrs->load('division', 'bizPartner', 'purchaser');
		//query the docDtls, and format into group details
		foreach($inbOrdHdrs as $inbOrdHdr)
		{
			$inbOrdHdr->str_doc_status = DocStatus::$MAP[$inbOrdHdr->doc_status];

			$division = $inbOrdHdr->division;
			$inbOrdHdr->division_code = $division->code;

			$bizPartner = $inbOrdHdr->bizPartner;
			$inbOrdHdr->biz_partner_code = $bizPartner->code;
			$inbOrdHdr->biz_partner_company_name_01 = $bizPartner->company_name_01;

			$purchaser = $inbOrdHdr->purchaser;
			$inbOrdHdr->purchaser_username = $purchaser->username;

			$inbOrdDtls = InbOrdDtlRepository::queryShipmentDetailsGroupByItemId($inbOrdHdr->id);
			//calculate the case_qty, gross_weight and cubic_meter
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			foreach($inbOrdDtls as $inbOrdDtl)
			{
				$inbOrdDtl->case_qty = round($inbOrdDtl->case_qty, 8);
				$inbOrdDtl->gross_weight = round($inbOrdDtl->gross_weight, 8);
				$inbOrdDtl->cubic_meter = round($inbOrdDtl->cubic_meter, 8);
				$caseQty = bcadd($caseQty, $inbOrdDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $inbOrdDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $inbOrdDtl->cubic_meter, 8);
			}

			$inbOrdHdr->case_qty = $caseQty;
			$inbOrdHdr->gross_weight = $grossWeight;
			$inbOrdHdr->cubic_meter = $cubicMeter;
			$inbOrdHdr->details = $inbOrdDtls;

			//unset the item, so it wont send in json
			unset($inbOrdHdr->division);
			unset($inbOrdHdr->creditor);
			unset($inbOrdHdr->purchaser);
		}
        return $inbOrdHdrs;
    }
    
    public function createProcess($strProcType, $hdrIds, $toStorageBinId)
    {
        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['GDS_RCPT_01']) 
            {
                //INB_ORD -> GDS_RCPT
                $result = $this->createGdsRcpt01(ProcType::$MAP[$strProcType], $hdrIds, $toStorageBinId);
                return $result;
            }
            elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['GDS_RCPT_02']) 
            {
                //INB_ORD -> GDS_RCPT
                $result = $this->createGdsRcpt02(ProcType::$MAP[$strProcType], $hdrIds);
                return $result;
            }
		}
    }

    protected function createGdsRcpt01($procType, $hdrIds, $toStorageBinId)
    {
        AuthService::authorize(
			array(
				'gds_rcpt_create'
			)
        );

        //1 inbOrd -> 1 gdsRcpt
        $gdsRcptHdrs = array();

        //verify the toStorageBinId
        $toStorageBin = StorageBinRepository::findByPk($toStorageBinId);
        $toStorageType = $toStorageBin->storageType;
        if($toStorageType->bin_type != BinType::$MAP['UNLOADING_AREA']
        && $toStorageType->bin_type != BinType::$MAP['UNLOADING_LOADING_AREA'])
        {
            $exc = new ApiException(__('GdsRcpt.bin_is_not_a_unloading_area', ['binCode'=>$toStorageBin->code]));
            //$exc->addData(\App\StorageBin::class, $toStorageBin->id);
            throw $exc;
        }

        $inbOrdDtlsHashByHdrId = array();
        $inbOrdHdrHashByHdrId = array();
        //format item into hashtable by itemId
		$itemHashByItemId = array();

		$siteFlow = null;
        $lineNo = 1;
        $tmpHandlingUnitId = 1;

        $allInbOrdDtls = InbOrdDtlRepository::findAllByHdrIds($hdrIds);
        $allInbOrdDtls->load('inbOrdHdr', 'item', 'inbOrdHdr.siteFlow');
        //1) format item to hash by itemId
        //1) find the siteId of the inbOrdHdrs
        //1) format allInbOrdDtls to hash by key
        //1) format inbOrdHdrs to hash by key
		foreach($allInbOrdDtls as $allInbOrdDtl) 
		{
            $inbOrdHdr = $allInbOrdDtl->inbOrdHdr;
            $siteFlow = $inbOrdHdr->siteFlow;

            $key = $inbOrdHdr->id;

            $item = $allInbOrdDtl->item;
            $itemHashByItemId[$item->id] = $item;

            //format to inbOrdDtlsHashByHdrId
			$tmpInbOrdDtls = array();
			if(array_key_exists($key, $inbOrdDtlsHashByHdrId))
			{
				$tmpInbOrdDtls = $inbOrdDtlsHashByHdrId[$key];
			}
			$tmpInbOrdDtls[] = $allInbOrdDtl;
            $inbOrdDtlsHashByHdrId[$key] = $tmpInbOrdDtls;

            //format to inbOrdHdrHashByHdrId
            $inbOrdHdrHashByHdrId[$inbOrdHdr->id] = $inbOrdHdr;
        }
        
        //2) find the site pallet config (min/max of pallet weight/length/height)
        $sitePalletConfig = StorageTypeRepository::findPalletConfiguration($siteFlow->site_id);
        
        //4) create the docTxnFlow for the selected inbOrdHdr
        foreach($inbOrdHdrHashByHdrId as $hdrId => $keyInbOrdHdr)
        {
			$bizPartnerId = $keyInbOrdHdr->biz_partner_id;
            $companyId = $keyInbOrdHdr->company_id;

            //format keyInbOrdDtls to inbOrdDtlsHashByItemId
            $inbOrdDtlsHashByItemId = array();

            $keyInbOrdDtls = $inbOrdDtlsHashByHdrId[$hdrId];
            foreach($keyInbOrdDtls as $keyInbOrdDtl)
            {
                //format to inbOrdDtlsHashByItemId
                $tmpInbOrdDtls = array();
                if(array_key_exists($keyInbOrdDtl->item_id, $inbOrdDtlsHashByItemId))
                {
                    $tmpInbOrdDtls = $inbOrdDtlsHashByItemId[$keyInbOrdDtl->item_id];
                }
                $tmpInbOrdDtls[] = $keyInbOrdDtl;
                $inbOrdDtlsHashByItemId[$keyInbOrdDtl->item_id] = $tmpInbOrdDtls;
            }

            //loop 2 times, 1st loop is for non inspection items, 2nd is for inspection items
            for($a = 0; $a < 2; $a++)
            {
                //hold the newly created items, to notify for details fill in
                $newItems = array();
                
                $hdrData = array();
                $dtlDataListHashByHandlingUnitId = array();            
                $inbOrdHdrIds = array();
                $docTxnFlowDataList = array();
                //gdsRcptItem
                $itemDataList = array();

                //3) loop itemInbOrdDtls by itemId (group by itemId)
                $isFrDocClosed = true;
                foreach($inbOrdDtlsHashByItemId as $itemId => $itemInbOrdDtls)
                {
                    $item = $itemHashByItemId[$itemId];
                    if($a == 0)
                    {
                        if($item->inspection_control > 0)
                        {
                            //1st loop for non inspection items only
                            $isFrDocClosed = false;
                            continue;
                        }
                    }
                    else
                    {
                        if($item->inspection_control == 0)
                        {
                            //2nd loop for inspection items only
                            continue;
                        }
                    }

                    if($item->status == ResStatus::$MAP['NEW'])
                    {
                        $newItems[] = $item;
                    }
                    
                    //3.1) calculate the ttlUnitQty
                    $ttlUnitQty = 0;
                    foreach($itemInbOrdDtls as $itemInbOrdDtl)
                    {
                        $tmpUnitQty = bcmul($itemInbOrdDtl->uom_rate, $itemInbOrdDtl->qty, $item->qty_scale);
                        $ttlUnitQty = bcadd($ttlUnitQty, $tmpUnitQty, $item->qty_scale);
                    }

                    //3.2) calculate the item's pallet configuration, based on site and item settings
                    $itemPalletConfig = ItemService::calculatePalletConfiguration($sitePalletConfig, $item);
                    if(bccomp($itemPalletConfig['max_ttl_cases_per_pallet'], 1) == 0
                    && bccomp($item->pallet_uom_rate, 1) != 0)
                    {
                        //for big bulk item, pallet_uom_rate is 1
                        //here is to make sure the pallet configuration is set
                        //if not set, then throw exception
                        $exc = new ApiException(__('GdsRcpt.pallet_configuration_is_not_set', [
                                'itemCode'=>$item->code,
                                'maxCasesPerPalletLength'=>$itemPalletConfig['max_cases_per_pallet_length'],
                                'maxCasesPerPalletWidth'=>$itemPalletConfig['max_cases_per_pallet_width'],
                                'maxNoOfLayers'=>$itemPalletConfig['max_no_of_layers'],
                            ]));
                        //$exc->addData(\App\GdsRcptDtl::class, $toStorageBin->id);
                        throw $exc;
                    }
                    
                    $remainTtlUnitQty = $ttlUnitQty;
                    //3.3) allocate item unitQty to full pallet
                    $noOfPallets = 0;
                    if(bccomp($itemPalletConfig['max_ttl_unit_per_pallet'], 0, 0) > 0)
                    {
                        $noOfPallets = bcdiv($ttlUnitQty, $itemPalletConfig['max_ttl_unit_per_pallet'], 0);
                    }
                    for($b = 0; $b < $noOfPallets; $b++)
                    {
                        $dtlData = array(
                            'line_no' => $lineNo,
                            'company_id' => $companyId,
                            'item_id' => $item->id,
                            'desc_01' => $item->desc_01,
                            'desc_02' => $item->desc_02,
                            'uom_id' => $item->unit_uom_id,
                            'uom_rate' => 1,
                            'qty' => $itemPalletConfig['max_ttl_unit_per_pallet'],
                            'to_storage_bin_id' => $toStorageBin->id,
                            'to_handling_unit_id' => $tmpHandlingUnitId,
                            'item_cond_01_id' => $item->inspection_control >= 1 ? 1 : 0,
                            'item_cond_02_id' => $item->inspection_control >= 2 ? 1 : 0,
                            'item_cond_03_id' => $item->inspection_control >= 3 ? 1 : 0,
                            'item_cond_04_id' => $item->inspection_control >= 4 ? 1 : 0,
                            'item_cond_05_id' => $item->inspection_control >= 5 ? 1 : 0,
                            'cases_per_pallet_length' => $itemPalletConfig['max_cases_per_pallet_length'],
                            'cases_per_pallet_width' => $itemPalletConfig['max_cases_per_pallet_width'],
                            'no_of_layers' => $itemPalletConfig['max_no_of_layers'],
                            'layer_no' => 1,
                            'whse_job_type' => WhseJobType::$MAP['GOODS_RECEIPT_PALLET']
                        );
                        $tmpDtlDataList = array();
                        if(array_key_exists($tmpHandlingUnitId, $dtlDataListHashByHandlingUnitId))
                        {
                            $tmpDtlDataList = $dtlDataListHashByHandlingUnitId[$tmpHandlingUnitId];
                        }
                        $tmpDtlDataList[] = $dtlData;
                        $dtlDataListHashByHandlingUnitId[$tmpHandlingUnitId] = $tmpDtlDataList;

                        $lineNo++;
                        $tmpHandlingUnitId++;
                        $remainTtlUnitQty = bcsub($remainTtlUnitQty, $itemPalletConfig['max_ttl_unit_per_pallet'], $item->qty_scale);
                    }

                    //3.4) check remaining qty, see can fit into half pallet or not
                    if(bccomp($remainTtlUnitQty, $itemPalletConfig['min_ttl_unit_per_pallet'], 5) > 0)
                    {
                        //enought for half pallet
                        $dtlData = array(
                            'line_no' => $lineNo,
                            'company_id' => $companyId,
                            'item_id' => $item->id,
                            'desc_01' => $item->desc_01,
                            'desc_02' => $item->desc_02,
                            'uom_id' => $item->unit_uom_id,
                            'uom_rate' => 1,
                            'qty' => $remainTtlUnitQty,
                            'to_storage_bin_id' => $toStorageBin->id,
                            'to_handling_unit_id' => $tmpHandlingUnitId,
                            'item_cond_01_id' => $item->inspection_control >= 1 ? 1 : 0,
                            'item_cond_02_id' => $item->inspection_control >= 2 ? 1 : 0,
                            'item_cond_03_id' => $item->inspection_control >= 3 ? 1 : 0,
                            'item_cond_04_id' => $item->inspection_control >= 4 ? 1 : 0,
                            'item_cond_05_id' => $item->inspection_control >= 5 ? 1 : 0,
                            'cases_per_pallet_length' => $itemPalletConfig['min_cases_per_pallet_length'],
                            'cases_per_pallet_width' => $itemPalletConfig['min_cases_per_pallet_width'],
                            'no_of_layers' => $itemPalletConfig['min_no_of_layers'],
                            'layer_no' => 1,
                            'whse_job_type' => WhseJobType::$MAP['GOODS_RECEIPT_PALLET']
                        );
                        $tmpDtlDataList = array();
                        if(array_key_exists($tmpHandlingUnitId, $dtlDataListHashByHandlingUnitId))
                        {
                            $tmpDtlDataList = $dtlDataListHashByHandlingUnitId[$tmpHandlingUnitId];
                        }
                        $tmpDtlDataList[] = $dtlData;
                        $dtlDataListHashByHandlingUnitId[$tmpHandlingUnitId] = $tmpDtlDataList;

                        $lineNo++;
                        $tmpHandlingUnitId++;
                        $remainTtlUnitQty = 0;
                    }
                    //3.5) if still have remaining qty, then no assign any handling unit id
                    //3.5) because this will put away to pickFace
                    if(bccomp($remainTtlUnitQty, 0, 5) > 0)
                    {
                        $looseHandlingUnitId = 0;
                        $dtlData = array(
                            'line_no' => $lineNo,
                            'company_id' => $companyId,
                            'item_id' => $item->id,
                            'desc_01' => $item->desc_01,
                            'desc_02' => $item->desc_02,
                            'uom_id' => $item->unit_uom_id,
                            'uom_rate' => 1,
                            'qty' => $remainTtlUnitQty,
                            'to_storage_bin_id' => $toStorageBin->id,
                            'to_handling_unit_id' => $looseHandlingUnitId,
                            'item_cond_01_id' => $item->inspection_control >= 1 ? 1 : 0,
                            'item_cond_02_id' => $item->inspection_control >= 2 ? 1 : 0,
                            'item_cond_03_id' => $item->inspection_control >= 3 ? 1 : 0,
                            'item_cond_04_id' => $item->inspection_control >= 4 ? 1 : 0,
                            'item_cond_05_id' => $item->inspection_control >= 5 ? 1 : 0,
                            'cases_per_pallet_length' => 0,
                            'cases_per_pallet_width' => 0,
                            'no_of_layers' => 0,
                            'layer_no' => 1,
                            'whse_job_type' => WhseJobType::$MAP['GOODS_RECEIPT_PALLET']
                        );
                        //handlingUnitId is 0, mean is not assigned any pallet id
                        $tmpDtlDataList = array();
                        if(array_key_exists($looseHandlingUnitId, $dtlDataListHashByHandlingUnitId))
                        {
                            $tmpDtlDataList = $dtlDataListHashByHandlingUnitId[$looseHandlingUnitId];
                        }
                        $tmpDtlDataList[] = $dtlData;
                        $dtlDataListHashByHandlingUnitId[$looseHandlingUnitId] = $tmpDtlDataList;

                        $lineNo++;
                        //$tmpHandlingUnitId++;
                        $remainTtlUnitQty = 0;
                    }
                }

                if(count($dtlDataListHashByHandlingUnitId) > 0)
                {
                    $tmpDocTxnFlowData = array(
                        'fr_doc_hdr_type' => \App\InbOrdHdr::class,
                        'fr_doc_hdr_id' => $keyInbOrdHdr->id,
                        'fr_doc_hdr_code' => $keyInbOrdHdr->doc_code,
                        'is_closed' => $isFrDocClosed
                    );
                    $docTxnFlowDataList[] = $tmpDocTxnFlowData;

                    $inbOrdHdrIds[] = $keyInbOrdHdr->id;

                    foreach($newItems as $newItem)
                    {
                        $itemData = array(
                            'item_id' => $newItem->id,
                            'code' => $newItem->code,
                            'ref_code_01' => $newItem->ref_code_01,
                            'desc_01' => $newItem->desc_01,
                            'desc_02' => $newItem->desc_02,
                            'item_status' => $newItem->status,
                            'unit_barcode' => '',
                            'case_barcode' => '',
                            'case_uom_rate' => 1
                        );
                        $itemDataList[] = $itemData;
                    }
                    
                    //build the hdrData
                    $hdrData = array(
                        'ref_code_01' => '',
                        'ref_code_02' => '',
                        'doc_date' => date('Y-m-d'),
                        'desc_01' => '',
                        'desc_02' => '',
                        'site_flow_id' => $siteFlow->id
                    );

                    $siteDocNo = SiteDocNoRepository::findSiteDocNo($siteFlow->site_id, \App\GdsRcptHdr::class);
                    if(empty($siteDocNo))
                    {
                        $exc = new ApiException(__('SiteDocNo.doc_no_not_found', ['siteId'=>$siteFlow->site_id, 'docType'=>\App\GdsRcptHdr::class]));
                        //$exc->addData(\App\GdsRcptHdr::class, $siteFlow->site_id);
                        throw $exc;
                    }

                    $whseTxnFlow = WhseTxnFlowRepository::findByPk($siteFlow->id, $procType);

                    //DRAFT
                    $gdsRcptHdr = GdsRcptHdrRepository::createProcess($procType, $siteDocNo->doc_no_id, $hdrData, $dtlDataListHashByHandlingUnitId, $inbOrdHdrIds, $docTxnFlowDataList, $itemDataList);

                    if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
                    {
                        $gdsRcptHdr = self::transitionToWip($gdsRcptHdr->id);
                    }
                    elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
                    {
                        $gdsRcptHdr = self::transitionToComplete($gdsRcptHdr->id);
                    }
                    $gdsRcptHdrs[] = $gdsRcptHdr;
                }
            }
        }

        $c = 0;
        $docCodes = '';
        foreach($gdsRcptHdrs as $gdsRcptHdr)
        {
            if ($c == 0) 
            {
                $docCodes .= $gdsRcptHdr->doc_code;
            }
            else
            {
                $docCodes .= ', '.$gdsRcptHdr->doc_code;
            }
            $c++;
        }
        $message = __('GdsRcpt.document_successfully_created', ['docCode'=>$docCodes]);

		return array(
			'data' => $gdsRcptHdrs,
			'message' => $message
		);
    }

    public function transitionToStatus($hdrId, $strDocStatus)
    {
        $hdrModel = null;
        if(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['WIP'])
        {
            $hdrModel = self::transitionToWip($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['COMPLETE'])
        {
            $hdrModel = self::transitionToComplete($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['DRAFT'])
        {
            $hdrModel = self::transitionToDraft($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['VOID'])
        {
            $hdrModel = self::transitionToVoid($hdrId);
        }
        
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $message = __('GdsRcpt.document_successfully_transition', ['docCode'=>$documentHeader->doc_code,'strDocStatus'=>$strDocStatus]);
        
        return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>array(),
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }

    static public function transitionToWip($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = GdsRcptHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status >= DocStatus::$MAP['WIP'])
		{
			$exc = new ApiException(__('GdsRcpt.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
            throw $exc;
		}

		//commit the document
		$hdrModel = GdsRcptHdrRepository::commitToWip($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('GdsRcpt.commit_to_wip_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
        throw $exc;
    }
    
    public function completeProcess($hdrId)
    {
		$hdrModel = GdsRcptHdrRepository::txnFindByPk($hdrId);

		$whseTxnFlow = WhseTxnFlowRepository::findByPk($hdrModel->site_flow_id, $hdrModel->proc_type);

		if($hdrModel->doc_status < DocStatus::$MAP['WIP'])
		{
			$exc = new ApiException(__('GdsRcpt.doc_status_is_not_wip', ['docCode'=>$hdrModel->doc_code]));
			$exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
			throw $exc;
		}
		if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
		{
			$exc = new ApiException(__('GdsRcpt.doc_status_is_complete', ['docCode'=>$hdrModel->doc_code]));
			$exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
			throw $exc;
		}

		$hdrModel = self::transitionToComplete($hdrModel->id);

		return $hdrModel;
	}

	static public function transitionToComplete($hdrId)
    {
        AuthService::authorize(
            array(
                'gds_rcpt_confirm'
            )
        );

		$isSuccess = false;
		//use transaction to make sure this is latest value
		$hdrModel = GdsRcptHdrRepository::txnFindByPk($hdrId);

		//only DRAFT or above can transition to COMPLETE
		if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE']
		|| $hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('GdsRcpt.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
            throw $exc;
		}

		$dtlModels = GdsRcptDtlRepository::findAllByHdrId($hdrModel->id);
		//preliminary checking
		//check the detail stock make sure the detail quant_bal_id > 0
		foreach($dtlModels as $dtlModel)
		{
            if(bccomp($dtlModel->qty, 0, 5) == 0)
            {
                continue;
            }
            
			if($dtlModel->item_id == 0)
			{
				$exc = new ApiException(__('GdsRcpt.item_is_required', ['lineNo'=>$dtlModel->line_no]));
				$exc->addData(\App\GdsRcptDtl::class, $dtlModel->id);
				throw $exc;
            }

            if(strcmp($dtlModel->expiry_date, '0000-00-00') == 0)
            {
                //expiry date is required, if qty is more than 0
                $exc = new ApiException(__('GdsRcpt.expiry_date_is_required', ['lineNo'=>$dtlModel->line_no]));
                $exc->addData(\App\GdsRcptDtl::class, $dtlModel->id);
                throw $exc;
            }
            
            if($dtlModel->item_cond_01_id == 0)
			{
				$exc = new ApiException(__('GdsRcpt.item_cond_01_is_required', ['lineNo'=>$dtlModel->line_no]));
				$exc->addData(\App\GdsRcptDtl::class, $dtlModel->id);
				throw $exc;
			}
        }

		//commit the document
		$hdrModel = GdsRcptHdrRepository::commitToComplete($hdrModel->id, false);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
        }
        
		$exc = new ApiException(__('GdsRcpt.commit_to_complete_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
        throw $exc;
    }
    
    static public function transitionToDraft($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = GdsRcptHdrRepository::txnFindByPk($hdrId);

		//only WIP or COMPLETE can transition to DRAFT
		if($hdrModel->doc_status == DocStatus::$MAP['WIP'])
		{
            $hdrModel = GdsRcptHdrRepository::revertWipToDraft($hdrModel->id);
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		elseif($hdrModel->doc_status == DocStatus::$MAP['COMPLETE'])
		{
            AuthService::authorize(
                array(
                    'gds_rcpt_revert'
                )
            );

            $hdrModel = GdsRcptHdrRepository::revertCompleteToDraft($hdrModel->id);
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
        }
        elseif($hdrModel->doc_status == DocStatus::$MAP['VOID'])
		{
            AuthService::authorize(
                array(
                    'gds_rcpt_confirm'
                )
            );

			$hdrModel = GdsRcptHdrRepository::commitVoidToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
        
        $exc = new ApiException(__('GdsRcpt.doc_status_is_not_wip_or_complete', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
		throw $exc;
    }
    
    public function verifyTxn() 
	{
        $invalidRows = GdsRcptHdrRepository::verifyTxn();        
        return $invalidRows;
    }

    static public function processOutgoingHeader($model, $isShowPrint = false)
	{
        $docFlows = self::processDocFlows($model);
        $model->doc_flows = $docFlows;
        
		if($isShowPrint)
		{
			$printDocTxn = PrintDocTxnRepository::queryPrintDocTxn(\App\GdsRcptHdr::class, $model->id);
			$model->print_count = $printDocTxn->print_count;
			$model->first_printed_at = $printDocTxn->first_printed_at;
			$model->last_printed_at = $printDocTxn->last_printed_at;
		}

		$model->str_doc_status = DocStatus::$MAP[$model->doc_status];
		return $model;
	}

	static public function processOutgoingDetail($model)
    {
        //relationships:
        $company = $model->company;
		$item = $model->item;
        $uom = $model->uom;
        $itemCond01 = $model->itemCond01;
		$toStorageBin = $model->toStorageBin;
		$toHandlingUnit = $model->toHandlingUnit;
		
		$model->str_whse_job_type = WhseJobType::$MAP[$model->whse_job_type];
		$model->str_whse_job_type = __('WhseJob.'.strtolower($model->str_whse_job_type));

        $model->company_code = '';
        $model->item_cond_01_code = '';
		$model->to_storage_bin_code = '';
		$model->to_storage_row_code = '';
        $model->to_storage_bay_code = '';
        $model->to_handling_unit_barcode = '';
        
		//this detail is receipt
		if(!empty($toStorageBin))
		{
			$toStorageRow = $toStorageBin->storageRow;
			$toStorageBay = $toStorageBin->storageBay;

			$model->to_storage_bin_code = $toStorageBin->code;
			if(!empty($toStorageRow))
			{
				$model->to_storage_row_code = $toStorageRow->code;
			}
			$model->to_item_bay_sequence = 0;
			if(!empty($toStorageBay))
			{
				$model->to_storage_bay_code = $toStorageBay->code;
				$model->to_item_bay_sequence = $toStorageBay->bay_sequence;
			}
        }
        
        if(!empty($toHandlingUnit))
        {
            $model->to_handling_unit_barcode = $toHandlingUnit->getBarcode();
        }

        if(!empty($company))
        {
            $model->company_code = $company->code;
        }
        if(!empty($itemCond01))
        {
            $model->item_cond_01_code = $itemCond01->code;
        }

        if($model->expiry_date == '0000-00-00')
        {
            $model->expiry_date = '1970-01-01';
        }
        if($model->receipt_date == '0000-00-00')
        {
            $model->receipt_date = '1970-01-01';
        }
        $itemBatch = null;
        if(!empty($item))
        {
            $itemBatch = ItemBatchRepository::findByAttributes($item, $model->batch_serial_no, $model->expiry_date, $model->receipt_date);
        }
		if(!empty($itemBatch))
		{
			$model->batch_serial_no = $itemBatch->batch_serial_no;
			$model->expiry_date = $itemBatch->expiry_date;
			$model->receipt_date = $itemBatch->receipt_date;
		}

		//calculate the pallet qty, case qty, gross weight, and m3
        $model = ItemService::processCaseLoose($model, $item);

        //storageBin select2
        $initCompanyOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($company))
        {
            $initCompanyOption = array('value'=>$company->id,'label'=>$company->code);
        } 
        $model->company_select2 = $initCompanyOption;

        $initItemOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($item))
        {
            $initItemOption = array('value'=>$item->id,'label'=>$item->code);
        } 
        $model->item_select2 = $initItemOption;

        $initItemBatchOption = array(
            'value'=>0,
            'label'=>'',
        );
        if(!empty($itemBatch))
        {
            $initItemBatchOption = array(
                'value'=>$itemBatch->id,
                'label'=>$itemBatch->expiry_date,
                'batch_serial_no'=>$itemBatch->batch_serial_no,
                'expiry_date'=>$itemBatch->expiry_date,
                'receipt_date'=>$itemBatch->receipt_date,
            );
        }
        $model->item_batch_select2 = $initItemBatchOption;

        $initItemCond01Option = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($itemCond01))
        {
            $initItemCond01Option = array('value'=>$itemCond01->id,'label'=>$itemCond01->code);
        }        
		$model->item_cond_01_select2 = $initItemCond01Option;
        	
		$initToStorageBinOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($toStorageBin))
        {
            $initToStorageBinOption = array('value'=>$toStorageBin->id,'label'=>$toStorageBin->code);
        }        
		$model->to_storage_bin_select2 = $initToStorageBinOption;
		
		$initToHandlingUnitOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($toHandlingUnit))
        {
            $initToHandlingUnitOption = array('value'=>$toHandlingUnit->id,'label'=>$toHandlingUnit->getBarcode());
        }        
		$model->to_handling_unit_select2 = $initToHandlingUnitOption;
		
		$initUomOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($uom))
        {
            $initUomOption = array('value'=>$uom->id,'label'=>$uom->code);
        }        
		$model->uom_select2 = $initUomOption;
        
        unset($model->company);
		unset($model->item);
        unset($model->uom);
        unset($model->itemCond01);
		unset($model->toStorageBin);
        unset($model->toHandlingUnit);

		return $model;
    }

    static public function processIncomingDetail($data)
    {
        //preprocess the select2 and dropDown
        if(array_key_exists('item_select2', $data))
        {
			$select2Id = $data['item_select2']['value'];
			if($select2Id > 0)
			{
				$data['item_id'] = $select2Id;
			}
            unset($data['item_select2']);
        }
        $item = ItemRepository::findByPk($data['item_id']);
        if(empty($item))
        {
            $exc = new ApiException(__('GdsRcpt.item_is_required', ['lineNo'=>$data['line_no']]));
            $exc->addData(\App\GdsRcptDtl::class, $data['id']);
            throw $exc;
        }
        
		if(array_key_exists('qty', $data)
		&& !empty($item))
		{
			$data['qty'] = round($data['qty'], $item->qty_scale);
        }

        if(array_key_exists('company_select2', $data))
        {
			$select2Id = $data['company_select2']['value'];
			if($select2Id > 0)
			{
				$data['company_id'] = $select2Id;
			}
            unset($data['company_select2']);
        }        
        if(array_key_exists('item_batch_select2', $data))
        {
            //just unset itemBatch select2, because it will fill correct value in other fields, 
            //expiry_date, receipt_date, when the itemBatch is selected
            unset($data['item_batch_select2']);
        }
        if(array_key_exists('uom_select2', $data))
        {
			$select2Id = $data['uom_select2']['value'];
			if($select2Id > 0)
			{
				$data['uom_id'] = $select2Id;
			}
            unset($data['uom_select2']);
        }
        if(array_key_exists('item_cond_01_select2', $data))
        {
			$select2Id = $data['item_cond_01_select2']['value'];
			if($select2Id > 0)
			{
				$data['item_cond_01_id'] = $select2Id;
			}
            unset($data['item_cond_01_select2']);
		}
        if(array_key_exists('to_storage_bin_select2', $data))
        {
			$select2Id = $data['to_storage_bin_select2']['value'];
			if($select2Id > 0)
			{
				$data['to_storage_bin_id'] = $select2Id;
			}
            unset($data['to_storage_bin_select2']);
		}
		if(array_key_exists('to_handling_unit_select2', $data))
        {
			$select2Id = $data['to_handling_unit_select2']['value'];
			if($select2Id > 0)
			{
				$data['to_handling_unit_id'] = $select2Id;
			}
            unset($data['to_handling_unit_select2']);
        }

        if(array_key_exists('company_code', $data))
        {
            unset($data['company_code']);
        }
        if(array_key_exists('item_cond_01_code', $data))
        {            
            unset($data['item_cond_01_code']);
        }
		if(array_key_exists('item_bay_sequence', $data))
        {
            unset($data['item_bay_sequence']);
		}
		if(array_key_exists('to_storage_bin_code', $data))
        {
            unset($data['to_storage_bin_code']);
		}
		if(array_key_exists('to_storage_row_code', $data))
        {
            unset($data['to_storage_row_code']);
		}
		if(array_key_exists('to_storage_bay_code', $data))
        {
            unset($data['to_storage_bay_code']);
		}
		if(array_key_exists('to_item_bay_sequence', $data))
        {
            unset($data['to_item_bay_sequence']);
		}

		if(array_key_exists('str_whse_job_type', $data))
        {
            unset($data['str_whse_job_type']);
		}

        if(array_key_exists('to_handling_unit_barcode', $data))
        {
            unset($data['to_handling_unit_barcode']);
		}
		if(array_key_exists('to_handling_unit_ref_code_01', $data))
        {
            unset($data['to_handling_unit_ref_code_01']);
        }
        
        if(array_key_exists('item_code', $data))
        {
            unset($data['item_code']);
		}
		if(array_key_exists('item_ref_code_01', $data))
        {
            unset($data['item_ref_code_01']);
		}
		if(array_key_exists('item_desc_01', $data))
        {
            unset($data['item_desc_01']);
		}
		if(array_key_exists('item_desc_02', $data))
        {
            unset($data['item_desc_02']);
		}
		if(array_key_exists('storage_class', $data))
        {
            unset($data['storage_class']);
		}
		if(array_key_exists('item_group_01_code', $data))
        {
            unset($data['item_group_01_code']);
		}
		if(array_key_exists('item_group_02_code', $data))
        {
            unset($data['item_group_02_code']);
		}
		if(array_key_exists('item_group_03_code', $data))
        {
            unset($data['item_group_03_code']);
		}
		if(array_key_exists('item_group_04_code', $data))
        {
            unset($data['item_group_04_code']);
		}
		if(array_key_exists('item_group_05_code', $data))
        {
            unset($data['item_group_05_code']);
		}
		if(array_key_exists('item_cases_per_pallet_length', $data))
        {
            unset($data['item_cases_per_pallet_length']);
		}
		if(array_key_exists('item_cases_per_pallet_width', $data))
        {
            unset($data['item_cases_per_pallet_width']);
		}
		if(array_key_exists('item_no_of_layers', $data))
        {
            unset($data['item_no_of_layers']);
		}

		if(array_key_exists('unit_qty', $data))
        {
            unset($data['unit_qty']);
		}
		if(array_key_exists('item_unit_uom_code', $data))
        {
            unset($data['item_unit_uom_code']);
		}
		if(array_key_exists('loose_qty', $data))
        {
            unset($data['loose_qty']);
		}
		if(array_key_exists('item_loose_uom_code', $data))
        {
            unset($data['item_loose_uom_code']);
        }
        if(array_key_exists('loose_uom_id', $data))
        {
            unset($data['loose_uom_id']);
		}
		if(array_key_exists('loose_uom_rate', $data))
        {
            unset($data['loose_uom_rate']);
		}
		if(array_key_exists('case_qty', $data))
        {
            unset($data['case_qty']);
		}
		if(array_key_exists('case_uom_rate', $data))
        {
            unset($data['case_uom_rate']);
		}
        if(array_key_exists('item_case_uom_code', $data))
        {
            unset($data['item_case_uom_code']);
        }
        if(array_key_exists('uom_code', $data))
        {
            unset($data['uom_code']);
		}
		if(array_key_exists('item_unit_barcode', $data))
        {
            unset($data['item_unit_barcode']);
		}
		if(array_key_exists('item_case_barcode', $data))
        {
            unset($data['item_case_barcode']);
		}
		if(array_key_exists('gross_weight', $data))
        {
            unset($data['gross_weight']);
		}
		if(array_key_exists('cubic_meter', $data))
        {
            unset($data['cubic_meter']);
        }
        if(array_key_exists('item_batch_id', $data))
        {
            unset($data['item_batch_id']);
		}
        return $data;
    }

    public function changeItem($hdrId, $itemId)
    {
        AuthService::authorize(
            array(
                'gds_rcpt_update'
            )
        );

        $gdsRcptHdr = GdsRcptHdrRepository::findByPk($hdrId);

        $results = array();
        $item = ItemRepository::findByPk($itemId);
        if(!empty($item)
        && !empty($gdsRcptHdr))
        {
            $results['desc_01'] = $item->desc_01;
            $results['desc_02'] = $item->desc_02;
        }
        return $results;
    }
    
    public function changeItemBatch($hdrId, $itemBatchId)
    {
        AuthService::authorize(
            array(
                'gds_rcpt_update'
            )
        );

        $gdsRcptHdr = GdsRcptHdrRepository::findByPk($hdrId);

        $results = array();
        $itemBatch = ItemBatchRepository::findByPk($itemBatchId);
        if(!empty($itemBatch)
        && !empty($gdsRcptHdr))
        {
            $results['batch_serial_no'] = $itemBatch->batch_serial_no;
            $results['expiry_date'] = $itemBatch->expiry_date;
            $results['receipt_date'] = $itemBatch->receipt_date;
        }
        return $results;
    }
    
    public function changeItemUom($hdrId, $itemId, $uomId)
    {
        AuthService::authorize(
            array(
                'gds_rcpt_update'
            )
        );

        $gdsRcptHdr = GdsRcptHdrRepository::findByPk($hdrId);

        $results = array();
        $itemUom = ItemUomRepository::findByItemIdAndUomId($itemId, $uomId);
        if(!empty($itemUom)
        && !empty($gdsRcptHdr))
        {
            $results['item_id'] = $itemUom->item_id;
            $results['uom_id'] = $itemUom->uom_id;
            $results['uom_rate'] = $itemUom->uom_rate;

			$results = $this->updateWarehouseItemUom($results, 0, 0);
        }
        return $results;
    }
    
    public function showHeader($hdrId)
    {
        AuthService::authorize(
            array(
                'gds_rcpt_read',
				'gds_rcpt_update'
            )
        );

        $model = GdsRcptHdrRepository::findByPk($hdrId);
        if(!empty($model))
        {
            $model = self::processOutgoingHeader($model);
        }
        return $model;
    }

    public function showDetails($hdrId) 
	{
        AuthService::authorize(
            array(
                'gds_rcpt_read',
				'gds_rcpt_update'
            )
        );

		$gdsRcptDtls = GdsRcptDtlRepository::findAllByHdrId($hdrId);
        $gdsRcptDtls->load(
            'item', 'item.itemUoms', 'item.itemUoms.uom'
        );
        foreach($gdsRcptDtls as $gdsRcptDtl)
        {
			$gdsRcptDtl = self::processOutgoingDetail($gdsRcptDtl);
        }
        return $gdsRcptDtls;
    }
    
    public function createDetail($hdrId, $data)
	{
        AuthService::authorize(
            array(
                'gds_rcpt_update'
            )
        );

        $data = self::processIncomingDetail($data);		
		$data = $this->updateWarehouseItemUom($data, 0, 0);

        $gdsRcptHdr = GdsRcptHdrRepository::txnFindByPk($hdrId);
        if($gdsRcptHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('GdsRcpt.doc_status_is_wip_or_complete', ['docCode'=>$gdsRcptHdr->doc_code]));
            $exc->addData(\App\GdsRcptHdr::class, $gdsRcptHdr->id);
            throw $exc;
        }
		$gdsRcptHdrData = $gdsRcptHdr->toArray();

		$gdsRcptDtlArray = array();
		$gdsRcptDtlModels = GdsRcptDtlRepository::findAllByHdrId($hdrId);
		foreach($gdsRcptDtlModels as $gdsRcptDtlModel)
		{
			$gdsRcptDtlData = $gdsRcptDtlModel->toArray();
			$gdsRcptDtlData['is_modified'] = 0;
			$gdsRcptDtlArray[] = $gdsRcptDtlData;
		}
		$lastLineNo = count($gdsRcptDtlModels);

		//assign temp id
		$tmpUuid = uniqid();
		//append NEW here to make sure it will be insert in repository later
		$data['id'] = 'NEW'.$tmpUuid;
		$data['line_no'] = $lastLineNo + 1;
		$data['is_modified'] = 1;
		
		$gdsRcptDtlData = $this->initGdsRcptDtlData($data);

		$gdsRcptDtlArray[] = $gdsRcptDtlData;

		$result = GdsRcptHdrRepository::updateDetails($gdsRcptHdrData, $gdsRcptDtlArray, array(), array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('GdsRcpt.detail_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }
    
    public function updateDetails($hdrId, $dtlArray)
	{
        AuthService::authorize(
            array(
                'gds_rcpt_update'
            )
        );

        $gdsRcptHdr = GdsRcptHdrRepository::txnFindByPk($hdrId);
        if($gdsRcptHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('GdsRcpt.doc_status_is_wip_or_complete', ['docCode'=>$gdsRcptHdr->doc_code]));
            $exc->addData(\App\GdsRcptHdr::class, $gdsRcptHdr->id);
            throw $exc;
        }
		$gdsRcptHdrData = $gdsRcptHdr->toArray();

		//query the gdsRcptDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$gdsRcptDtlArray = array();
        $gdsRcptDtlModels = GdsRcptDtlRepository::findAllByHdrId($hdrId);
		foreach($gdsRcptDtlModels as $gdsRcptDtlModel)
		{
			$gdsRcptDtlData = $gdsRcptDtlModel->toArray();
			$gdsRcptDtlData['is_modified'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{                
				if($dtlData['id'] == $gdsRcptDtlData['id'])
				{
					$dtlData = self::processIncomingDetail($dtlData);
					
					if($dtlData['uom_id'] == Uom::$PALLET
                    && bccomp($dtlData['uom_rate'], 0, 5) == 0)
                    {
					}
					else
					{
						$dtlData = $this->updateWarehouseItemUom($dtlData, 0, 0);
					}

					foreach($dtlData as $fieldName => $value)
					{
						$gdsRcptDtlData[$fieldName] = $value;
					}

                    if($gdsRcptDtlData['whse_job_type'] == WhseJobType::$MAP['GOODS_RECEIPT_LOOSE'])
                    {
                        $gdsRcptDtlData['to_handling_unit_id'] = 0;
                    }
					$gdsRcptDtlData['is_modified'] = 1;

					break;
				}
			}
			$gdsRcptDtlArray[] = $gdsRcptDtlData;
		}
		
		$result = GdsRcptHdrRepository::updateDetails($gdsRcptHdrData, $gdsRcptDtlArray, array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = self::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('GdsRcpt.details_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }
    
    public function index($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
            array(
                'gds_rcpt_read'
            )
        );

        $gdsRcptHdrs = GdsRcptHdrRepository::findAll($siteFlowId, $sorts, $filters, $pageSize);
        $gdsRcptHdrs->load(
            'gdsRcptDtls', 
            'gdsRcptDtls.item', 'gdsRcptDtls.item.itemUoms', 'gdsRcptDtls.item.itemUoms.uom', 
            'gdsRcptDtls.toStorageBin', 'gdsRcptDtls.toStorageBin.storageBay', 'gdsRcptDtls.toStorageBin.storageRow'
        );
		foreach($gdsRcptHdrs as $gdsRcptHdr)
		{
			$gdsRcptHdr = GdsRcptService::processOutgoingHeader($gdsRcptHdr, false);

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$gdsRcptDtls = $gdsRcptHdr->gdsRcptDtls;
            $gdsRcptDtls = $gdsRcptDtls->sortBy('line_no');
			foreach($gdsRcptDtls as $gdsRcptDtl)
			{
                $gdsRcptDtl = GdsRcptService::processOutgoingDetail($gdsRcptDtl);

				$caseQty = bcadd($caseQty, $gdsRcptDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $gdsRcptDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $gdsRcptDtl->cubic_meter, 8);

				if($gdsRcptDtl->to_handling_unit_id > 0)
				{
					$palletHashByHandingUnitId[$gdsRcptDtl->to_handling_unit_id] = $gdsRcptDtl->to_handling_unit_id;
				}
				unset($gdsRcptDtl->toStorageBin);
				unset($gdsRcptDtl->item);
			}
			
			$gdsRcptHdr->pallet_qty = count($palletHashByHandingUnitId);
			$gdsRcptHdr->case_qty = $caseQty;
			$gdsRcptHdr->gross_weight = $grossWeight;
			$gdsRcptHdr->cubic_meter = $cubicMeter;

			$gdsRcptHdr->details = $gdsRcptDtls;
		}
    	return $gdsRcptHdrs;
    }
    
    static public function transitionToVoid($hdrId)
    {
        AuthService::authorize(
            array(
                'gds_rcpt_revert'
            )
        );

		//use transaction to make sure this is latest value
		$hdrModel = GdsRcptHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('GdsRcpt.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
            throw $exc;
		}

		//revert the document
		$hdrModel = GdsRcptHdrRepository::revertDraftToVoid($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('GdsRcpt.commit_to_void_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
        throw $exc;
    }

    public function updateHeader($hdrData)
	{
        AuthService::authorize(
            array(
                'gds_rcpt_update'
            )
        );

        $hdrData = self::processIncomingHeader($hdrData);

        $gdsRcptHdr = GdsRcptHdrRepository::txnFindByPk($hdrData['id']);
        if($gdsRcptHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('GdsRcpt.doc_status_is_wip_or_complete', ['docCode'=>$gdsRcptHdr->doc_code]));
            $exc->addData(\App\GdsRcptHdr::class, $gdsRcptHdr->id);
            throw $exc;
        }
        $gdsRcptHdrData = $gdsRcptHdr->toArray();
        //assign hdrData to model
        foreach($hdrData as $field => $value) 
        {
            $gdsRcptHdrData[$field] = $value;
        }

		//$gdsRcptHdr->load('worker01');
        //$worker01 = $gdsRcptHdr->worker01;
		
		//no detail to update
        $gdsRcptDtlArray = array();

        $result = GdsRcptHdrRepository::updateDetails($gdsRcptHdrData, $gdsRcptDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];

        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = self::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('GdsRcpt.document_successfully_updated', ['docCode'=>$documentHeader->doc_code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }
    
    static public function processIncomingHeader($data, $isClearHdrDiscTax=false)
    {
        if(array_key_exists('doc_flows', $data))
        {
            unset($data['doc_flows']);
        }

		if(array_key_exists('submit_action', $data))
        {
            unset($data['submit_action']);
        }
        //preprocess the select2 and dropDown
        
        if(array_key_exists('str_doc_status', $data))
        {
            unset($data['str_doc_status']);
		}
        
        return $data;
    }
    
    public function deleteDetails($hdrId, $dtlArray)
	{
        AuthService::authorize(
            array(
                'gds_rcpt_update'
            )
        );

        $gdsRcptHdr = GdsRcptHdrRepository::txnFindByPk($hdrId);
        if($gdsRcptHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('GdsRcpt.doc_status_is_wip_or_complete', ['docCode'=>$gdsRcptHdr->doc_code]));
            $exc->addData(\App\GdsRcptHdr::class, $gdsRcptHdr->id);
            throw $exc;
        }
		$gdsRcptHdrData = $gdsRcptHdr->toArray();

		//query the gdsRcptDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$delGdsRcptDtlArray = array();
		$gdsRcptDtlArray = array();
        $gdsRcptDtlModels = GdsRcptDtlRepository::findAllByHdrId($hdrId);
        $lineNo = 1;
		foreach($gdsRcptDtlModels as $gdsRcptDtlModel)
		{
			$gdsRcptDtlData = $gdsRcptDtlModel->toArray();
			$gdsRcptDtlData['is_modified'] = 0;
			$gdsRcptDtlData['is_deleted'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{
				if($dtlData['id'] == $gdsRcptDtlData['id'])
				{
					//this is deleted dtl
					$gdsRcptDtlData['is_deleted'] = 1;
					break;
				}
			}
			if($gdsRcptDtlData['is_deleted'] > 0)
			{
				$delGdsRcptDtlArray[] = $gdsRcptDtlData;
			}
			else
			{
                $gdsRcptDtlData['line_no'] = $lineNo;
                $gdsRcptDtlData['is_modified'] = 1;
                $gdsRcptDtlArray[] = $gdsRcptDtlData;
                $lineNo++;
			}
        }
		
		$result = GdsRcptHdrRepository::updateDetails($gdsRcptHdrData, $gdsRcptDtlArray, $delGdsRcptDtlArray);
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = self::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('GdsRcpt.details_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>$delGdsRcptDtlArray
            ),
			'message' => $message
		);
    }
    
    protected function indexGdsRcpt02($user, $siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
            array(
                'inb_ord_read'
            )
        );

        $divisionIds = array();
		$userDivisions = $user->userDivisions;
		foreach($userDivisions as $userDivision)
		{
			$divisionIds[] = $userDivision->division_id;
		}

        $inbOrdHdrs = InbOrdHdrRepository::findAllNotExistGdsRcpt02Txn($divisionIds, $siteFlowId, DocStatus::$MAP['COMPLETE'], $sorts, $filters, $pageSize);
        $inbOrdHdrs->load(
            'division', 'deliveryPoint', 'salesman',
			'inbOrdDtls', 'inbOrdDtls.item'
        );
		//query the docDtls, and format into group details
		foreach($inbOrdHdrs as $inbOrdHdr)
		{
			$inbOrdHdr->str_doc_status = DocStatus::$MAP[$inbOrdHdr->doc_status];

			$division = $inbOrdHdr->division;
			$inbOrdHdr->division_code = $division->code;

			$deliveryPoint = $inbOrdHdr->deliveryPoint;
			$inbOrdHdr->delivery_point_code = $deliveryPoint->code;
			$inbOrdHdr->delivery_point_company_name_01 = $deliveryPoint->company_name_01;
			$inbOrdHdr->delivery_point_area_code = $deliveryPoint->area_code;
			$area = AreaRepository::findByPk($deliveryPoint->area_id);
			$inbOrdHdr->delivery_point_area_desc_01 = $area->desc_01;

			$salesman = $inbOrdHdr->salesman;
			$inbOrdHdr->salesman_username = $salesman->username;

			$inbOrdDtls = $inbOrdHdr->inbOrdDtls;
			//calculate the case_qty, gross_weight and cubic_meter
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			foreach($inbOrdDtls as $inbOrdDtl)
			{
                $item = $inbOrdDtl->item;
                $inbOrdDtl = SlsRtnService::processOutgoingDetail($inbOrdDtl);
                
				$inbOrdDtl->case_qty = round($inbOrdDtl->case_qty, 8);
				$inbOrdDtl->gross_weight = round($inbOrdDtl->gross_weight, 8);
				$inbOrdDtl->cubic_meter = round($inbOrdDtl->cubic_meter, 8);
				$caseQty = bcadd($caseQty, $inbOrdDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $inbOrdDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $inbOrdDtl->cubic_meter, 8);
			}

			$inbOrdHdr->case_qty = $caseQty;
			$inbOrdHdr->gross_weight = $grossWeight;
			$inbOrdHdr->cubic_meter = $cubicMeter;
			$inbOrdHdr->details = $inbOrdDtls;

			//unset the item, so it wont send in json
			unset($inbOrdHdr->division);
			unset($inbOrdHdr->deliveryPoint);
			unset($inbOrdHdr->salesman);
		}
        return $inbOrdHdrs;
    }

    protected function createGdsRcpt02($procType, $hdrIds)
    {
        AuthService::authorize(
            array(
                'gds_rcpt_create'
            )
        );

        //1 slsRtn inbOrd -> 1 gdsRcpt
        //must be 1 to 1, so line_no can be used to identify
        $gdsRcptHdrs = array();

        $siteFlow = null;
        
        $inbOrdHdrs = InbOrdHdrRepository::findAllByHdrIds($hdrIds);
        $inbOrdHdrs->load('siteFlow', 'inbOrdDtls', 'inbOrdDtls.item');
        foreach($inbOrdHdrs as $inbOrdHdr)
        {
            $dtlDataListHashByHandlingUnitId = array();
            $inbOrdHdrIds = array();
            $docTxnFlowDataList = array();
            
            $hdrData = array();
            $dtlDataList = array();
            
            $siteFlow = $inbOrdHdr->siteFlow;
            $inbOrdDtls = $inbOrdHdr->inbOrdDtls;
            $inbOrdDtls = $inbOrdDtls->sortBy('line_no');
            foreach($inbOrdDtls as $inbOrdDtl)
            {
                $item = $inbOrdDtl->item;

                $dtlData = array(
                    'line_no' => $inbOrdDtl->line_no,
                    'company_id' => $inbOrdHdr->company_id,
                    'item_id' => $inbOrdDtl->item_id,
                    'desc_01' => $inbOrdDtl->desc_01,
                    'desc_02' => $inbOrdDtl->desc_02,
                    'uom_id' => $inbOrdDtl->uom_id,
                    'uom_rate' => $inbOrdDtl->uom_rate,
                    'qty' => $inbOrdDtl->qty,
                    'to_storage_bin_id' => 0,
                    'to_handling_unit_id' => 0,
                    'item_cond_01_id' => $item->inspection_control >= 1 ? 1 : 0,
                    'item_cond_02_id' => $item->inspection_control >= 2 ? 1 : 0,
                    'item_cond_03_id' => $item->inspection_control >= 3 ? 1 : 0,
                    'item_cond_04_id' => $item->inspection_control >= 4 ? 1 : 0,
                    'item_cond_05_id' => $item->inspection_control >= 5 ? 1 : 0,
                    'cases_per_pallet_length' => 0,
                    'cases_per_pallet_width' => 0,
                    'no_of_layers' => 0,
                    'layer_no' => 1,
                    'whse_job_type' => WhseJobType::$MAP['GOODS_RECEIPT_LOOSE']
                );
                $dtlDataList[] = $dtlData;
            }

            $dtlDataListHashByHandlingUnitId = array(
                0 => $dtlDataList
            );
            $inbOrdHdrIds[] = $inbOrdHdr->id;
            $tmpDocTxnFlowData = array(
                'fr_doc_hdr_type' => \App\InbOrdHdr::class,
                'fr_doc_hdr_id' => $inbOrdHdr->id,
                'fr_doc_hdr_code' => $inbOrdHdr->doc_code,
                'is_closed' => 1
            );
            $docTxnFlowDataList[] = $tmpDocTxnFlowData;

            //build the hdrData
            $hdrData = array(
                'ref_code_01' => '',
                'ref_code_02' => '',
                'doc_date' => date('Y-m-d'),
                'desc_01' => '',
                'desc_02' => '',
                'site_flow_id' => $siteFlow->id
            );

            $siteDocNo = SiteDocNoRepository::findSiteDocNo($siteFlow->site_id, \App\GdsRcptHdr::class);
            if(empty($siteDocNo))
            {
                $exc = new ApiException(__('SiteDocNo.doc_no_not_found', ['siteId'=>$siteFlow->site_id, 'docType'=>\App\GdsRcptHdr::class]));
                //$exc->addData(\App\GdsRcptHdr::class, $siteFlow->site_id);
                throw $exc;
            }

            $whseTxnFlow = WhseTxnFlowRepository::findByPk($siteFlow->id, $procType);

            //DRAFT
            $gdsRcptHdr = GdsRcptHdrRepository::createProcess($procType, $siteDocNo->doc_no_id, $hdrData, $dtlDataListHashByHandlingUnitId, $inbOrdHdrIds, $docTxnFlowDataList, array());

            if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
            {
                $gdsRcptHdr = self::transitionToWip($gdsRcptHdr->id);
            }
            elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
            {
                $gdsRcptHdr = self::transitionToComplete($gdsRcptHdr->id);
            }
            $gdsRcptHdrs[] = $gdsRcptHdr;
        }

        $c = 0;
        $docCodes = '';
        foreach($gdsRcptHdrs as $gdsRcptHdr)
        {
            if ($c == 0) 
            {
                $docCodes .= $gdsRcptHdr->doc_code;
            }
            else
            {
                $docCodes .= ', '.$gdsRcptHdr->doc_code;
            }
            $c++;
        }
        $message = __('GdsRcpt.document_successfully_created', ['docCode'=>$docCodes]);

		return array(
			'data' => $gdsRcptHdrs,
			'message' => $message
		);
    }

    protected function indexGdsRcpt0201($user, $siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
            array(
                'gds_rcpt_read'
            )
        );

        $divisionIds = array();
		$userDivisions = $user->userDivisions;
		foreach($userDivisions as $userDivision)
		{
			$divisionIds[] = $userDivision->division_id;
        }
        
		$gdsRcptHdrs = GdsRcptHdrRepository::findAllDraftAndWIP($divisionIds, $siteFlowId, ProcType::$MAP['GDS_RCPT_02'], $sorts, $filters, $pageSize);
		$gdsRcptHdrs->load(
            'gdsRcptInbOrds', 'gdsRcptInbOrds.inbOrdHdr',
            'gdsRcptDtls', 'gdsRcptDtls.item',
            'gdsRcptInbOrds', 'gdsRcptInbOrds.inbOrdHdr',
            'gdsRcptInbOrds.inbOrdHdr.deliveryPoint',
            'gdsRcptInbOrds.inbOrdHdr.deliveryPoint.area',
            'gdsRcptInbOrds.inbOrdHdr.salesman'
        );
		foreach($gdsRcptHdrs as $gdsRcptHdr)
		{
            $gdsRcptHdr = GdsRcptService::processOutgoingHeader($gdsRcptHdr, true);
            
            $areaHashById = array();
            $deliveryPointHashById = array();
			$salesmanHash = array();
            
            //find all the gdsRcptInbOrds
			$gdsRcptInbOrds = $gdsRcptHdr->gdsRcptInbOrds;
			$inbOrdHdrs = array();
			
			foreach($gdsRcptInbOrds as $gdsRcptInbOrd)
			{
				$inbOrdHdr = $gdsRcptInbOrd->inbOrdHdr;
				$deliveryPoint = $inbOrdHdr->deliveryPoint;
				$deliveryPointHashById[$deliveryPoint->id] = $deliveryPoint;

				$inbOrdHdr->delivery_point_code = $deliveryPoint->code;
				$inbOrdHdr->delivery_point_area_code = $deliveryPoint->area_code;
				$inbOrdHdr->delivery_point_area_desc_01 = $deliveryPoint->area_code;
				$area = $deliveryPoint->area;
				if(!empty($area))
				{
					$areaHashById[$area->id] = $area;
					$inbOrdHdr->delivery_point_area_desc_01 = $area->desc_01;
				}
				$inbOrdHdr->delivery_point_company_name_01 = $deliveryPoint->company_name_01;
                $inbOrdHdr->delivery_point_company_name_02 = $deliveryPoint->company_name_02;
                
                $salesman = $inbOrdHdr->salesman;
				$salesmanHash[$salesman->id] = $salesman;

				$gdsRcptHdr->net_amt = bcadd($gdsRcptHdr->net_amt, $inbOrdHdr->net_amt, 8);

				$inbOrdHdrs[] = $inbOrdHdr;
			}
			$gdsRcptHdr->inb_ord_hdrs = $inbOrdHdrs;
			$gdsRcptHdr->areas = array_values($areaHashById);
			$gdsRcptHdr->delivery_points = array_values($deliveryPointHashById);
			$gdsRcptHdr->salesmans = array_values($salesmanHash);
			
			//query the inbOrds
			$gdsRcptInbOrds = $gdsRcptHdr->gdsRcptInbOrds;
			for($a = 0; $a < count($gdsRcptInbOrds); $a++)
			{
				$gdsRcptInbOrd = $gdsRcptInbOrds[$a];
				$inbOrdHdr = $gdsRcptInbOrd->inbOrdHdr;
				$gdsRcptHdr->sls_rtn_hdr_code = $inbOrdHdr->sls_rtn_hdr_code;
                $gdsRcptHdr->ref_code_01 = $inbOrdHdr->ref_code_01;
                $gdsRcptHdr->ref_code_02 = $inbOrdHdr->ref_code_02;
                $gdsRcptHdr->ref_code_03 = $inbOrdHdr->ref_code_03;
                $gdsRcptHdr->ref_code_04 = $inbOrdHdr->ref_code_04;
                $gdsRcptHdr->ref_code_05 = $inbOrdHdr->ref_code_05;
                $gdsRcptHdr->ref_code_06 = $inbOrdHdr->ref_code_06;
                $gdsRcptHdr->ref_code_07 = $inbOrdHdr->ref_code_07;
                $gdsRcptHdr->ref_code_08 = $inbOrdHdr->ref_code_08;
                break;
			}

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$gdsRcptDtls = $gdsRcptHdr->gdsRcptDtls;
			foreach($gdsRcptDtls as $gdsRcptDtl)
			{
				$gdsRcptDtl = GdsRcptService::processOutgoingDetail($gdsRcptDtl);

				$caseQty = bcadd($caseQty, $gdsRcptDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $gdsRcptDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $gdsRcptDtl->cubic_meter, 8);

				unset($gdsRcptDtl->item);
			}
			
			$gdsRcptHdr->case_qty = $caseQty;
			$gdsRcptHdr->gross_weight = $grossWeight;
			$gdsRcptHdr->cubic_meter = $cubicMeter;

			$gdsRcptHdr->details = $gdsRcptDtls;
		}
    	return $gdsRcptHdrs;
    }
    
    public function printProcess($strProcType, $siteFlowId, $hdrIds)
  	{
        $user = Auth::user();

		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['GDS_RCPT_02']) 
			{
				//print pick list WHSE_JOB
				return $this->printGdsRcpt02($siteFlowId, $user, $hdrIds);
			}
		}
    }
    
    public function printGdsRcpt02($siteFlowId, $user, $hdrIds)
	{
        AuthService::authorize(
            array(
                'gds_rcpt_print'
            )
        );

		$docHdrType = \App\GdsRcptHdr::class.'02';
		$printDocSetting = PrintDocSettingRepository::findByDocHdrTypeAndSiteFlowId($docHdrType, $siteFlowId);
		if(empty($printDocSetting))
		{
			$exc = new ApiException(__('PrintDoc.report_template_not_found', ['docType'=>\App\GdsRcptHdr::class]));
			$exc->addData(\App\PrintDocSetting::class, \App\GdsRcptHdr::class);
			throw $exc;
		}

        $gdsRcptHdrs = GdsRcptHdrRepository::findAllByHdrIds($hdrIds);
        $gdsRcptHdrs->load(
            'gdsRcptDtls',
            'gdsRcptDtls.item',
            'gdsRcptInbOrds', 'gdsRcptInbOrds.inbOrdHdr',
            'gdsRcptInbOrds.inbOrdHdr.deliveryPoint',
            'gdsRcptInbOrds.inbOrdHdr.deliveryPoint.area',
            'gdsRcptInbOrds.inbOrdHdr.salesman'
        );
		$printDocTxnDataArray = array();
		foreach($gdsRcptHdrs as $gdsRcptHdr)
		{
			$gdsRcptHdr->view_name = $printDocSetting->view_name;

			$printDocTxnData = array();
			$printDocTxnData['doc_hdr_type'] = \App\GdsRcptHdr::class;
			$printDocTxnData['doc_hdr_id'] = $gdsRcptHdr->id;
			$printDocTxnData['user_id'] = $user->id;
			$printDocTxnDataArray[] = $printDocTxnData;
		}
		
		foreach($gdsRcptHdrs as $gdsRcptHdr)
		{
            $inbOrdHdrs = array();
            $deliveryPointHashById = array();
            $areaHashById = array();
            $salesmanHash = array();

            $gdsRcptHdr = self::processOutgoingHeader($gdsRcptHdr, false);
            
            //find all the gdsRcptInbOrds
			$gdsRcptInbOrds = $gdsRcptHdr->gdsRcptInbOrds;
			
			foreach($gdsRcptInbOrds as $gdsRcptInbOrd)
			{
				$inbOrdHdr = $gdsRcptInbOrd->inbOrdHdr;
				$deliveryPoint = $inbOrdHdr->deliveryPoint;
				$deliveryPointHashById[$deliveryPoint->id] = $deliveryPoint;

				$inbOrdHdr->delivery_point_code = $deliveryPoint->code;
				$inbOrdHdr->delivery_point_area_code = $deliveryPoint->area_code;
				$inbOrdHdr->delivery_point_area_desc_01 = $deliveryPoint->area_code;
				$area = $deliveryPoint->area;
				if(!empty($area))
				{
					$areaHashById[$area->id] = $area;
					$inbOrdHdr->delivery_point_area_desc_01 = $area->desc_01;
				}
				$inbOrdHdr->delivery_point_company_name_01 = $deliveryPoint->company_name_01;
                $inbOrdHdr->delivery_point_company_name_02 = $deliveryPoint->company_name_02;
                
                $salesman = $inbOrdHdr->salesman;
				$salesmanHash[$salesman->id] = $salesman;

				$gdsRcptHdr->net_amt = bcadd($gdsRcptHdr->net_amt, $inbOrdHdr->net_amt, 8);

				$inbOrdHdrs[] = $inbOrdHdr;
			}
			$gdsRcptHdr->inb_ord_hdrs = $inbOrdHdrs;
			$gdsRcptHdr->areas = array_values($areaHashById);
			$gdsRcptHdr->delivery_points = array_values($deliveryPointHashById);
			$gdsRcptHdr->salesmans = array_values($salesmanHash);
			
			//query the inbOrds
			$gdsRcptInbOrds = $gdsRcptHdr->gdsRcptInbOrds;
			for($a = 0; $a < count($gdsRcptInbOrds); $a++)
			{
				$gdsRcptInbOrd = $gdsRcptInbOrds[$a];
				$inbOrdHdr = $gdsRcptInbOrd->inbOrdHdr;
				$gdsRcptHdr->sls_rtn_hdr_code = $inbOrdHdr->sls_rtn_hdr_code;
                $gdsRcptHdr->ref_code_01 = $inbOrdHdr->ref_code_01;
                $gdsRcptHdr->ref_code_02 = $inbOrdHdr->ref_code_02;
                $gdsRcptHdr->ref_code_03 = $inbOrdHdr->ref_code_03;
                $gdsRcptHdr->ref_code_04 = $inbOrdHdr->ref_code_04;
                $gdsRcptHdr->ref_code_05 = $inbOrdHdr->ref_code_05;
                $gdsRcptHdr->ref_code_06 = $inbOrdHdr->ref_code_06;
                $gdsRcptHdr->ref_code_07 = $inbOrdHdr->ref_code_07;
                $gdsRcptHdr->ref_code_08 = $inbOrdHdr->ref_code_08;
                break;
			}

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
            //query the docDtls
            $gdsRcptDtls = $gdsRcptHdr->gdsRcptDtls;

			foreach($gdsRcptDtls as $gdsRcptDtl)
			{
				$gdsRcptDtl = GdsRcptService::processOutgoingDetail($gdsRcptDtl, false);

				$caseQty = bcadd($caseQty, $gdsRcptDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $gdsRcptDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $gdsRcptDtl->cubic_meter, 8);

				if($gdsRcptDtl->handling_unit_id > 0)
				{
					$palletHashByHandingUnitId[$gdsRcptDtl->handling_unit_id] = $gdsRcptDtl->handling_unit_id;
				}

				$gdsRcptDtl->str_whse_job_type = WhseJobType::$MAP[$gdsRcptDtl->whse_job_type];
				$gdsRcptDtl->whse_job_type_label = __('WhseJobType.'.strtolower($gdsRcptDtl->str_whse_job_type).'_label');

				unset($gdsRcptDtl->quantBal);
				unset($gdsRcptDtl->storageBin);
				unset($gdsRcptDtl->item);
			}

			$gdsRcptHdr->pallet_qty = count($palletHashByHandingUnitId);
			$gdsRcptHdr->case_qty = $caseQty;
			$gdsRcptHdr->gross_weight = $grossWeight;
			$gdsRcptHdr->cubic_meter = $cubicMeter;

			$gdsRcptHdr->details = $gdsRcptDtls;
			$gdsRcptHdr->barcode = '14'.str_pad($gdsRcptHdr->id, 5, '0', STR_PAD_LEFT);
		}

		$printedAt = PrintDocTxnRepository::createProcess($printDocTxnDataArray);

		$pdf = \Illuminate\Support\Facades\App::make('dompdf.wrapper');
		$pdf->getDomPDF()->set_option("enable_php", true);
		$pdf->setPaper(array(
			0,
			0,
			$printDocSetting->page_width, 
			$printDocSetting->page_height
		), $printDocSetting->page_orientation);

		//each data must have the view_name
		$pdf->loadHTML(
			view('batchPrintDocuments',
				array(
					'title' => 'Goods Receipt',
					'data' => $gdsRcptHdrs, 
					'printedAt' => $printedAt,
					'printedBy' => $user,
					'isPageBreakAfter' => true,
				)
			)
		);

		return $pdf->stream();
	}
}
