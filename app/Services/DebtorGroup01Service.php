<?php

namespace App\Services;

use App\Repositories\DebtorGroup01Repository;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;
use App\Services\Utils\SimpleImage;
use Milon\Barcode\DNS2D;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;

class DebtorGroup01Service
{
    public function __construct()
    {
    }

    public function select2($search, $filters)
    {
        //DB::connection()->enableQueryLog();
        $debtorGroup01s = DebtorGroup01Repository::select2($search, $filters);
        //dd(DB::getQueryLog());
        return $debtorGroup01s;
    }
}
