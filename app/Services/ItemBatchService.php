<?php

namespace App\Services;

use App\Repositories\ItemBatchRepository;
class ItemBatchService 
{
    public function __construct() 
	{
    }
    
    public function select2($search, $filters)
    {
        $itemBatches = ItemBatchRepository::select2($search, $filters);
        return $itemBatches;
    }
}
