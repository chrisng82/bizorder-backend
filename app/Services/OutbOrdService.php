<?php

namespace App\Services;

use App\OutbOrdDtl;
use App\OutbOrdHdr;
use App\Services\Env\DocStatus;
use App\Services\Env\ItemType;
use App\Services\Env\ResType;
use App\Services\Env\DeliveryPointType;
use App\Services\Env\ProcType;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;
use App\Repositories\OutbOrdHdrRepository;
use App\Repositories\OutbOrdDtlRepository;
use App\Repositories\SlsOrdHdrRepository;
use App\Repositories\SlsOrdDtlRepository;
use App\Repositories\UserRepository;
use App\Repositories\DeliveryPointRepository;
use App\Repositories\CurrencyRepository;
use App\Repositories\ItemRepository;
use App\Repositories\ItemUomRepository;
use App\Repositories\DivisionDocNoRepository;
use App\Repositories\QuantBalRepository;
use App\Repositories\InvTxnFlowRepository;
use App\Repositories\CreditTermRepository;
use App\Services\Utils\RepositoryUtils;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OutbOrdService extends InventoryService
{
    public function __construct()
    {
	}

    public function indexProcess($strProcType, $divisionId, $sorts, $filters = array(), $pageSize = 20)
    {
        if (ProcType::$MAP[$strProcType] == ProcType::$MAP['OUTB_ORD_01']) {
            //sales order -> outbound order, can issue partially
			$slsOrdHdrs = $this->indexOutbOrd01($divisionId, $sorts, $filters, $pageSize);
			return $slsOrdHdrs;
		}
    }

    protected function indexOutbOrd01($divisionId, $sorts, $filters = array(), $pageSize = 20)
    {
		AuthService::authorize(
            array(
                'sls_ord_read'
            )
		);
		
		//SLS_ORD -> OUTB_ORD
		//DB::connection()->enableQueryLog();
		$slsOrdHdrs = SlsOrdHdrRepository::findAllNotExistOutbOrd01Txn($divisionId, $sorts, $filters, $pageSize);
		//Log::error(DB::getQueryLog());
		$slsOrdHdrs->load(
			'siteFlow',
			'slsOrdDtls', 'slsOrdDtls.item',
			'division', 'deliveryPoint', 'deliveryPoint.area', 'salesman'
		);
		//query the docDtls, and format into group details
		foreach($slsOrdHdrs as $slsOrdHdr)
		{
			$siteFlow = $slsOrdHdr->siteFlow;
			$slsOrdHdr->is_out_of_stock = false;
			$slsOrdHdr->str_doc_status = DocStatus::$MAP[$slsOrdHdr->doc_status];

			$division = $slsOrdHdr->division;
			$slsOrdHdr->division_code = $division->code;

			$deliveryPoint = $slsOrdHdr->deliveryPoint;
			$slsOrdHdr->delivery_point_code = $deliveryPoint->code;
			$slsOrdHdr->delivery_point_company_name_01 = $deliveryPoint->company_name_01;
			$slsOrdHdr->delivery_point_area_code = $deliveryPoint->area_code;
			$area = $deliveryPoint->area;
			if(!empty($area))
			{
				$slsOrdHdr->delivery_point_area_desc_01 = $area->desc_01;
			}

			$salesman = $slsOrdHdr->salesman;
			$slsOrdHdr->salesman_username = $salesman->username;

			$slsOrdDtls = $slsOrdHdr->slsOrdDtls;
			$slsOrdDtls = SlsOrdService::checkStock($siteFlow, $slsOrdHdr, $slsOrdDtls);
			//calculate the case_qty, gross_weight and cubic_meter
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			foreach($slsOrdDtls as $slsOrdDtl)
			{
				$slsOrdDtl = SlsOrdService::processOutgoingDetail($slsOrdDtl);

				$slsOrdDtl->case_qty = round($slsOrdDtl->case_qty, 8);
				$slsOrdDtl->gross_weight = round($slsOrdDtl->gross_weight, 8);
				$slsOrdDtl->cubic_meter = round($slsOrdDtl->cubic_meter, 8);
				$caseQty = bcadd($caseQty, $slsOrdDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $slsOrdDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $slsOrdDtl->cubic_meter, 8);

				if(bccomp($slsOrdDtl->request_qty, $slsOrdDtl->available_qty, 5) > 0)
                {
					$slsOrdHdr->is_out_of_stock = true;
				}
			}

			$slsOrdHdr->case_qty = $caseQty;
			$slsOrdHdr->gross_weight = $grossWeight;
			$slsOrdHdr->cubic_meter = $cubicMeter;
			$slsOrdHdr->details = $slsOrdDtls;

			//unset the item, so it wont send in json
			unset($slsOrdHdr->division);
			unset($slsOrdHdr->debtor);
			unset($slsOrdHdr->salesman);
		}
        return $slsOrdHdrs;
    }

    public function index($divisionId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
            array(
                'outb_ord_read'
            )
		);

        //DB::connection()->enableQueryLog();
        $outbOrdHdrs = OutbOrdHdrRepository::findAll($divisionId, $sorts, $filters, $pageSize);
        $outbOrdHdrs->load(
            'outbOrdDtls'
            //'outbOrdDtls.item', 'outbOrdDtls.item.itemUoms', 'outbOrdDtls.item.itemUoms.uom', 
        );
		foreach($outbOrdHdrs as $outbOrdHdr)
		{
			$outbOrdHdr->str_doc_status = DocStatus::$MAP[$outbOrdHdr->doc_status];

			$deliveryPoint = $outbOrdHdr->deliveryPoint;
			$outbOrdHdr->delivery_point_code = $deliveryPoint->code;
			$outbOrdHdr->delivery_point_company_name_01 = $deliveryPoint->company_name_01;
			$outbOrdHdr->delivery_point_area_code = $deliveryPoint->area_code;
			$area = $deliveryPoint->area;
			if(!empty($area))
			{
				$outbOrdHdr->delivery_point_area_desc_01 = $area->desc_01;
			}

			$salesman = $outbOrdHdr->salesman;
			$outbOrdHdr->salesman_username = $salesman->username;

			$outbOrdDtls = $outbOrdHdr->outbOrdDtls;
			$outbOrdHdr->details = $outbOrdDtls;
        }
        //Log::error(DB::getQueryLog());
    	return $outbOrdHdrs;
	}

    public function showHeader($hdrId)
    {
		AuthService::authorize(
            array(
                'outb_ord_read',
				'outb_ord_update'
            )
		);

		$model = OutbOrdHdrRepository::findByPk($hdrId);
		if(!empty($model))
        {
            $model = self::processOutgoingHeader($model);
        }
		return $model;
    }

    public function showDetails($hdrId) 
	{
		AuthService::authorize(
            array(
                'outb_ord_read',
				'outb_ord_update'
            )
		);

		$outbOrdDtls = OutbOrdDtlRepository::findAllByHdrId($hdrId);
		$outbOrdDtls->load(
            'item', 'item.itemUoms', 'item.itemUoms.uom'
        );
        foreach($outbOrdDtls as $outbOrdDtl)
        {
            $outbOrdDtl = self::processOutgoingDetail($outbOrdDtl);
        }
        return $outbOrdDtls;
	}

	/*
	public function updateDetails($hdrId, $dtlArray)
	{
		AuthService::authorize(
            array(
                'outb_ord_update'
            )
		);

		//get the associated outbOrdHdr
		$outbOrdHdr = OutbOrdHdrRepository::txnFindByPk($hdrId);
		$outbOrdHdrData = $outbOrdHdr->toArray();
		$outbOrdHdr->load('deliveryPoint');
		$deliveryPoint = $outbOrdHdr->deliveryPoint;

		//query the outbOrdDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$promoUuidArray = array();
		$outbOrdDtlArray = array();
		$outbOrdDtlModels = OutbOrdDtlRepository::findAllByHdrId($hdrId);
		foreach($outbOrdDtlModels as $outbOrdDtlModel)
		{
			$outbOrdDtlData = $outbOrdDtlModel->toArray();
			$outbOrdDtlData['is_modified'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{
				if($dtlData['id'] == $outbOrdDtlData['id'])
				{
					$dtlData = $this->updateSaleItemUom($dtlData, $outbOrdHdr->doc_date, $deliveryPoint->price_group_id, $outbOrdHdr->currency_id, 0, 0);
					$uuid = $outbOrdDtlData['promo_uuid'];
					foreach($dtlData as $fieldName => $value)
					{
						$outbOrdDtlData[$fieldName] = $value;
					}
					$outbOrdDtlData = $this->clearPromotion($outbOrdDtlData);
					$outbOrdDtlData['is_modified'] = 1;

					if(!empty($uuid))
					{
						$promoUuidArray[] = $uuid;
					}
					break;
				}
			}
			$outbOrdDtlArray[] = $outbOrdDtlData;
		}

		//another loop to reset the promotion if this record is updated
		foreach($outbOrdDtlArray as $outbOrdDtlSeq => $outbOrdDtlData)
		{
			foreach($promoUuidArray as $promoUuid)
			{
				if($promoUuid == $outbOrdDtlData['promo_uuid'])
				{
					$outbOrdDtlData = $this->clearPromotion($outbOrdDtlData);
					$outbOrdDtlData['is_modified'] = 1;
					break;
				}
			}
			$outbOrdDtlArray[$outbOrdDtlSeq] = $outbOrdDtlData;
		}
		
		//promotion
		$modifiedDtlArray = $this->processPromotion($outbOrdDtlArray, $outbOrdHdr->doc_date, $outbOrdHdr->division_id, 
		$deliveryPoint->debtor_id, $deliveryPoint->debtor_group_01_id, $deliveryPoint->debtor_group_02_id, $deliveryPoint->debtor_group_03_id, $deliveryPoint->debtor_group_04_id, $deliveryPoint->debtor_group_05_id);
		foreach($outbOrdDtlArray as $key => $outbOrdDtlData)
		{
			foreach($modifiedDtlArray as $modifiedDtlData)
			{
				if($modifiedDtlData['id'] == $outbOrdDtlData['id'])
				{
					foreach($modifiedDtlData as $field => $value)
					{
						$outbOrdDtlData[$field] = $value;
					}
					$outbOrdDtlData['is_modified'] = 1;
					$outbOrdDtlArray[$key] = $outbOrdDtlData;
				}
			}
		}

		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($outbOrdHdrData, $outbOrdDtlArray);
		$outbOrdHdrData = $result['hdrData'];
		$outbOrdDtlArray = $result['dtlArray'];

		$result = OutbOrdHdrRepository::updateDetails($outbOrdHdrData, $outbOrdDtlArray, array());
		return $result;
	}

	public function createDetail($hdrId, $data)
	{
		AuthService::authorize(
            array(
                'outb_ord_update'
            )
		);

		$outbOrdHdr = OutbOrdHdrRepository::txnFindByPk($hdrId);
		$outbOrdHdrData = $outbOrdHdr->toArray();
		$outbOrdHdr->load('deliveryPoint');
		$deliveryPoint = $outbOrdHdr->deliveryPoint;

		$outbOrdDtlArray = array();
		$outbOrdDtlModels = OutbOrdDtlRepository::findAllByHdrId($hdrId);
		foreach($outbOrdDtlModels as $outbOrdDtlModel)
		{
			$outbOrdDtlData = $outbOrdDtlModel->toArray();
			$outbOrdDtlData['is_modified'] = 0;
			$outbOrdDtlArray[] = $outbOrdDtlData;
		}
		$lastLineNo = count($outbOrdDtlModels);

		//assign temp id
		$tmpUuid = uniqid();
		//append NEW here to make sure it will be insert in repository later
		$data['id'] = 'NEW'.$tmpUuid;
		$data['line_no'] = $lastLineNo + 1;
		$data['is_modified'] = 1;
		$data = $this->updateSaleItemUom($data, $outbOrdHdr['doc_date'], $deliveryPoint->price_group_id, $outbOrdHdr['currency_id'], 0, 0);
		$outbOrdDtlData = $this->initOutbOrdDtlData($data);
		$outbOrdDtlArray[] = $outbOrdDtlData;

		//promotion
		$modifiedDtlArray = $this->processPromotion($outbOrdDtlArray, $outbOrdHdr->doc_date, $outbOrdHdr->division_id, 
		$deliveryPoint->debtor_id, $deliveryPoint->debtor_group_01_id, $deliveryPoint->debtor_group_02_id, $deliveryPoint->debtor_group_03_id, $deliveryPoint->debtor_group_04_id, $deliveryPoint->debtor_group_05_id);
		foreach($outbOrdDtlArray as $key => $outbOrdDtlData)
		{
			foreach($modifiedDtlArray as $modifiedDtlData)
			{
				if($modifiedDtlData['id'] == $outbOrdDtlData['id'])
				{
					foreach($modifiedDtlData as $field => $value)
					{
						$outbOrdDtlData[$field] = $value;
					}
					$outbOrdDtlData['is_modified'] = 1;
					$outbOrdDtlArray[$key] = $outbOrdDtlData;
				}
			}
		}

		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($outbOrdHdrData, $outbOrdDtlArray);
		$outbOrdHdrData = $result['hdrData'];
		$outbOrdDtlArray = $result['dtlArray'];		

		$result = OutbOrdHdrRepository::updateDetails($outbOrdHdrData, $outbOrdDtlArray, array());
		return $result;
	}

	public function deleteDetails($hdrId, $dtlArray)
	{
		AuthService::authorize(
            array(
                'outb_ord_update'
            )
		);

		//get the associated outbOrdHdr
		$outbOrdHdr = OutbOrdHdrRepository::txnFindByPk($hdrId);
		$outbOrdHdrData = $outbOrdHdr->toArray();
		$outbOrdHdr->load('outbOrdHdr', 'deliveryPoint');
		$deliveryPoint = $outbOrdHdr->deliveryPoint;

		//query the outbOrdDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$promoUuidArray = array();
		$delOutbOrdDtlArray = array();
		$outbOrdDtlArray = array();
		$outbOrdDtlModels = OutbOrdDtlRepository::findAllByHdrId($hdrId);
		foreach($outbOrdDtlModels as $outbOrdDtlModel)
		{
			$outbOrdDtlData = $outbOrdDtlModel->toArray();
			$outbOrdDtlData['is_modified'] = 0;
			$outbOrdDtlData['is_deleted'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{
				if($dtlData['id'] == $outbOrdDtlData['id'])
				{
					//this is deleted dtl
					$uuid = $outbOrdDtlData['promo_uuid'];
					$outbOrdDtlData = $this->clearPromotion($outbOrdDtlData);
					$outbOrdDtlData['is_deleted'] = 1;

					if(!empty($uuid))
					{
						$promoUuidArray[] = $uuid;
					}
					break;
				}
			}
			if($outbOrdDtlData['is_deleted'] > 0)
			{
				$delOutbOrdDtlArray[] = $outbOrdDtlData;
			}
			else
			{
				$outbOrdDtlArray[] = $outbOrdDtlData;
			}
		}

		//another loop to reset the promotion if this record is updated
		//and update the line no also
		$lineNo = 1;
		foreach($outbOrdDtlArray as $outbOrdDtlSeq => $outbOrdDtlData)
		{
			foreach($promoUuidArray as $promoUuid)
			{
				if($promoUuid == $outbOrdDtlData['promo_uuid'])
				{
					$outbOrdDtlData = $this->clearPromotion($outbOrdDtlData);
					$outbOrdDtlData['is_modified'] = 1;
					break;
				}
			}
			$outbOrdDtlData['line_no'] = $lineNo++;
			$outbOrdDtlArray[$outbOrdDtlSeq] = $outbOrdDtlData;
		}
		
		//promotion
		$modifiedDtlArray = $this->processPromotion($outbOrdDtlArray, $outbOrdHdr->doc_date, $outbOrdHdr->division_id, 
		$deliveryPoint->debtor_id, $deliveryPoint->debtor_group_01_id, $deliveryPoint->debtor_group_02_id, $deliveryPoint->debtor_group_03_id, $deliveryPoint->debtor_group_04_id, $deliveryPoint->debtor_group_05_id);
		foreach($outbOrdDtlArray as $outbOrdDtlSeq => $outbOrdDtlData)
		{
			foreach($modifiedDtlArray as $modifiedDtlData)
			{
				if($modifiedDtlData['id'] == $outbOrdDtlData['id'])
				{
					foreach($modifiedDtlData as $field => $value)
					{
						$outbOrdDtlData[$field] = $value;
					}
					$outbOrdDtlData['is_modified'] = 1;
					$outbOrdDtlArray[$outbOrdDtlSeq] = $outbOrdDtlData;
					break;
				}
			}
		}

		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($outbOrdHdrData, $outbOrdDtlArray, $delOutbOrdDtlArray);
		$outbOrdHdrData = $result['hdrData'];
		$modifiedDtlArray = $result['dtlArray'];
		foreach($outbOrdDtlArray as $outbOrdDtlSeq => $outbOrdDtlData)
		{
			foreach($modifiedDtlArray as $modifiedDtlData)
			{
				if($modifiedDtlData['id'] == $outbOrdDtlData['id'])
				{
					foreach($modifiedDtlData as $field => $value)
					{
						$outbOrdDtlData[$field] = $value;
					}
					$outbOrdDtlData['is_modified'] = 1;
					$outbOrdDtlArray[$outbOrdDtlSeq] = $outbOrdDtlData;
					break;
				}
			}
		}

		$result = OutbOrdHdrRepository::updateDetails($outbOrdHdrData, $outbOrdDtlArray, $delOutbOrdDtlArray);
		return $result;
	}

	public function createHeader($procType, $divisionId, $data)
	{
		AuthService::authorize(
            array(
                'outb_ord_create'
            )
		);

		$divisionDocNo = DivisionDocNoRepository::findSiteDocNo($divisionId, \App\OutbOrdHdr::class);
		if(empty($divisionDocNo))
		{
			$exc = new ApiException(__('DivisionDocNo.doc_no_not_found', ['divisionId'=>$divisionId, 'docType'=>\App\OutbOrdHdr::class]));
			//$exc->addData(\App\DivisionDocNo::class, $divisionId);
			throw $exc;
		}

		$outbOrdHdr = OutbOrdHdrRepository::createHeader($procType, $divisionDocNo->doc_no_id, $outbOrdHdrData);
		return $outbOrdHdr;
	}

	public function testCreateDocuments($siteFlowId, $companyId, $divisionId)
    {
		AuthService::authorize(
            array(
                'outb_ord_create'
            )
		);

		$numOfDocs = mt_rand(2, 10);

		$ordHdrModels = array();

		$divisionDocNo = DivisionDocNoRepository::findDivisionDocNo($divisionId, \App\OutbOrdHdr::class);
		if(empty($divisionDocNo))
		{
			$exc = new ApiException(__('DivisionDocNo.doc_no_not_found', ['divisionId'=>$divisionId, 'docType'=>\App\OutbOrdHdr::class]));
			$exc->addData(\App\DivisionDocNo::class, $divisionId);
			throw $exc;
		}
		
		for($a = 0; $a < $numOfDocs; $a++)
		{
			//Start point of our date range.
			$start = strtotime("-5 day");
			//End point of our date range.
			$end = strtotime("+5 day");
			$timestamp = mt_rand($start, $end);
			$docDate = date('Y-m-d', $timestamp);
			
			$salesman = UserRepository::findRandom();
			$deliveryPoint = DeliveryPointRepository::findRandomByType(DeliveryPointType::$MAP['DEBTOR']);
			$currency = CurrencyRepository::findRandom();

			$ordHdr = array();
			$ordHdr['sign'] = 1;
			$ordHdr['cur_res_type'] = ResType::$MAP['SLS_ORD'];
			$ordHdr['ref_code_01'] = '';
			$ordHdr['ref_code_02'] = '';
			$ordHdr['doc_date'] = $docDate;
			$ordHdr['desc_01'] = '';
			$ordHdr['desc_02'] = '';
			$ordHdr['site_flow_id'] = $siteFlowId;
			$ordHdr['company_id'] = $companyId;
			$ordHdr['division_id'] = $divisionId;
			$ordHdr['salesman_id'] = $salesman->id;
			$ordHdr['debtor_id'] = $deliveryPoint->debtor_id;
			$ordHdr['delivery_point_id'] = $deliveryPoint->id;
			$ordHdr['est_del_date'] = $docDate;
			$ordHdr['currency_id'] = $currency->id;
			$ordHdr['currency_rate'] = $currency->currency_rate;
			$ordHdrModel = OutbOrdHdrRepository::createHeader(ProcType::$MAP['OUTB_ORD_01'], $divisionDocNo->doc_no_id, $ordHdr);

			$numOfDtls = mt_rand(2, 5);

			for($b = 0; $b < $numOfDtls; $b++)
			{
				$quantBal = QuantBalRepository::findRandomStorage();
				while(empty($quantBal))
				{
					$quantBal = QuantBalRepository::findRandomStorage();
				}
				$item = $quantBal->item;
				$itemUom = ItemUomRepository::findRandomByItemId($item->id);

				$ordDtl['item_id'] = $item->id;
				$ordDtl['desc_01'] = $item->desc_01;
				$ordDtl['desc_02'] = $item->desc_02;
				$ordDtl['uom_id'] = $itemUom->uom_id;
				$ordDtl['uom_rate'] = $itemUom->uom_rate;
				$ordDtl['qty'] = mt_rand(1, 10);

				$result = $this->createDetail($ordHdrModel->id, $ordDtl);
				$ordHdrModel = $result['hdrModel'];
			}

			$ordHdrModel = self::transitionToComplete($ordHdrModel->id);

			$ordHdrModels[] = $ordHdrModel;
		}
        
		return $ordHdrModels;
	}
	*/
	
	static public function transitionToComplete($hdrId)
	{
		AuthService::authorize(
            array(
                'outb_ord_confirm'
            )
		);

		//use transaction to make sure this is latest value
		$hdrModel = OutbOrdHdrRepository::txnFindByPk($hdrId);

		//only DRAFT or above can transition to COMPLETE
		if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE']
		|| $hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('OutbOrd.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
			$exc->addData(\App\OutbOrdHdr::class, $hdrModel->id);
			throw $exc;
		}
  
		//commit the document
		$hdrModel = OutbOrdHdrRepository::commitToComplete($hdrModel->id, true);
		if(!empty($hdrModel))
		{
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		
		$exc = new ApiException(__('OutbOrd.commit_to_complete_error', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\OutbOrdHdr::class, $hdrModel->id);
		throw $exc;
	}

	public function createProcess($strProcType, $hdrIds) 
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['OUTB_ORD_01'])
			{
				$result = $this->createOutbOrd01(ProcType::$MAP[$strProcType], $hdrIds);
				return $result;
			}
		}
	}

	protected function createOutbOrd01($procType, $hdrIds)
    {
		AuthService::authorize(
            array(
                'outb_ord_create'
            )
		);

		$outbOrdHdrs = array();
        //1) 1 SlsOrd -> 1 OutbOrd
		$slsOrdHdrs = SlsOrdHdrRepository::findAllByHdrIds($hdrIds);
		$slsOrdHdrs->load('siteFlow', 'division', 'deliveryPoint', 'slsOrdDtls', 'slsOrdDtls.item');
        //2) loop for each slsOrdHdr
        foreach($slsOrdHdrs as $slsOrdHdr)
        {
            //2.1) variables to store the outbOrd
			$hdrData = array();
			//hash by frDocDtlId
			$dtlDataHashByDtlId = array();
			//hash by frDocDtlId, match with dtlDataHashByDtlId
			$frDocDtlModelHashByDtlId = array();
            $docTxnFlowDataList = array();
            $division = $slsOrdHdr->division;
			$lineNo = 1;
			
			//query the pickingCriteria, and format it into hash by itemId
			$pickingCriteriasHashByItemId = array();
			$deliveryPoint = $slsOrdHdr->deliveryPoint;
			$siteFlow = $slsOrdHdr->siteFlow;
			
            //2.2) get the details of this slsOrd
			$slsOrdDtls = $slsOrdHdr->slsOrdDtls;
			$slsOrdDtls = $slsOrdDtls->sortBy('line_no');
            foreach($slsOrdDtls as $slsOrdDtl)
            {				
				$item = $slsOrdDtl->item;
				$pickingCriterias = PickingCriteriaService::processByDeliveryPointAndItem($siteFlow, $deliveryPoint, $item);
				$tmpPickingCriterias = array();
				if(array_key_exists($item->id, $pickingCriteriasHashByItemId))
				{
					$tmpPickingCriterias = $pickingCriteriasHashByItemId[$item->id];
				}
				$tmpPickingCriterias = array_merge($tmpPickingCriterias, $pickingCriterias);
				$pickingCriteriasHashByItemId[$item->id] = $tmpPickingCriterias;

				$dtlData = array();
				unset($slsOrdDtl->item);
				$dtlData = RepositoryUtils::modelToData($dtlData, $slsOrdDtl);
				unset($dtlData['outb_ord_hdr_id']);
				unset($dtlData['outb_ord_dtl_id']);

				$dtlDataHashByDtlId[$slsOrdDtl->id] = $dtlData;
				$frDocDtlModelHashByDtlId[$slsOrdDtl->id] = $slsOrdDtl;
    
				$lineNo++;
            }
			
			//2.7) build the hdrData
            $hdrData = array(
				'sls_ord_hdr_id' => $slsOrdHdr->id,
				'sls_ord_hdr_code' => $slsOrdHdr->doc_code,
				'del_ord_hdr_id' => 0,
				'del_ord_hdr_code' => '',
				'sls_inv_hdr_id' => 0,
				'sls_inv_hdr_code' => '',
				'sls_rtn_hdr_id' => 0,
				'sls_rtn_hdr_code' => '',
				'sls_cn_hdr_id' => 0,
				'sls_cn_hdr_code' => '',
				//1 = outbound delivery documents (delOrdHdr/slsInvHdr), -1 = outbound receiving documents (credit note), 0 = sales order/sales return
				'sign' => 0,
				'cur_res_type' => ResType::$MAP['SLS_ORD'],
			);
			unset($slsOrdHdr->siteFlow);
			unset($slsOrdHdr->division);
			unset($slsOrdHdr->deliveryPoint);
			unset($slsOrdHdr->slsOrdDtls);
			$hdrData = RepositoryUtils::modelToData($hdrData, $slsOrdHdr);
			unset($hdrData['outb_ord_hdr_id']);

            $tmpDocTxnFlowData = array(
                'fr_doc_hdr_type' => \App\SlsOrdHdr::class,
                'fr_doc_hdr_id' => $slsOrdHdr->id,
                'fr_doc_hdr_code' => $slsOrdHdr->doc_code,
                'is_closed' => 1
            );
            $docTxnFlowDataList[] = $tmpDocTxnFlowData;

            //2.8) get the outbOrdHdr doc no
            $divisionDocNo = DivisionDocNoRepository::findDivisionDocNo($division->id, \App\OutbOrdHdr::class);
            if(empty($divisionDocNo))
            {
                $exc = new ApiException(__('DivisionDocNo.doc_no_not_found', ['divisionId'=>$division->id, 'docType'=>\App\OutbOrdHdr::class]));
                //$exc->addData(\App\DivisionDocNo::class, $division->id);
                throw $exc;
            }

            //2.9) query the invTxnFlow, so know the next step
            $invTxnFlow = InvTxnFlowRepository::findByPk($division->id, $procType);

            //2.10) create the outbOrdHdr
            $outbOrdHdr = OutbOrdHdrRepository::createProcess($pickingCriteriasHashByItemId, $procType, $divisionDocNo->doc_no_id, $hdrData, $dtlDataHashByDtlId, $slsOrdHdr, $frDocDtlModelHashByDtlId, $docTxnFlowDataList);

            //2.11) commit outbOrdHdr to WIP/Complete according to invTxnFlow
            if($invTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
            {
                $outbOrdHdr = self::transitionToWip($outbOrdHdr->id);
            }
            elseif($invTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
            {
                $outbOrdHdr = self::transitionToComplete($outbOrdHdr->id);
            }
            
            $outbOrdHdrs[] = $outbOrdHdr;
        }

        $docCodeMsg = '';
        for ($a = 0; $a < count($outbOrdHdrs); $a++)
        {
            $outbOrdHdr = $outbOrdHdrs[$a];
            if($a == 0)
            {
                $docCodeMsg .= $outbOrdHdr->doc_code;
            }
            else
            {
                $docCodeMsg .= ', '.$outbOrdHdr->doc_code;
            }
        }
        $message = __('OutbOrd.document_successfully_created', ['docCode'=>$docCodeMsg]);

        return array(
            'data' => $outbOrdHdrs,
            'message' => $message
        );
	}

	static public function transitionToVoid($hdrId)
    {
		AuthService::authorize(
            array(
                'outb_ord_revert'
            )
		);

		//use transaction to make sure this is latest value
		$hdrModel = OutbOrdHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('OutbOrd.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\OutbOrdHdr::class, $hdrModel->id);
            throw $exc;
		}

		//revert the document
		$hdrModel = OutbOrdHdrRepository::revertDraftToVoid($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('OutbOrd.commit_to_void_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\OutbOrdHdr::class, $hdrModel->id);
        throw $exc;
	}

	static public function processOutgoingDetail($model)
    {
        $item = $model->item;
        $model->item_code = $item->code;
        //$model->item_desc_01 = $item->desc_01;
        //$model->item_desc_02 = $item->desc_02;
        //item select2
        $initItemOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($item))
        {
            $initItemOption = array(
                'value'=>$item->id, 
                'label'=>$item->code.' '.$item->desc_01,
            );
        }
        $model->item_select2 = $initItemOption;

        $uom = $model->uom;
		//uom select2
		$model->uom_code = '';
        $initUomOption = array(
            'value'=>0,
            'label'=>''
		);
        if(!empty($uom))
        {
			$model->uom_code = $uom->code;
            $initUomOption = array('value'=>$uom->id,'label'=>$uom->code);
        }        
		$model->uom_select2 = $initUomOption;
		
		$model = ItemService::processCaseLoose($model, $item);

		unset($model->item);
        unset($model->uom);

        return $model;
	}
	
	static public function processOutgoingHeader($model)
    {
		$docFlows = self::processDocFlows($model);
		$model->doc_flows = $docFlows;
		
        $division = $model->division;
        $model->division_code = $division->code;
        $company = $model->company;
        $model->company_code = $company->code;

        $model->str_doc_status = DocStatus::$MAP[$model->doc_status];

        //currency select2
        $initCurrencyOption = array('value'=>0,'label'=>'');
        $currency = CurrencyRepository::findByPk($model->currency_id);
        if(!empty($currency))
        {
            $initCurrencyOption = array('value'=>$currency->id, 'label'=>$currency->symbol);
        }
        $model->currency_select2 = $initCurrencyOption;

        //creditTerm select2
        $initCreditTermOption = array('value'=>0,'label'=>'');
        $creditTerm = CreditTermRepository::findByPk($model->credit_term_id);
        if(!empty($creditTerm))
        {
            $initCreditTermOption = array('value'=>$creditTerm->id, 'label'=>$creditTerm->code);
        }
        $model->credit_term_select2 = $initCreditTermOption;

        //salesman select2
        $salesman = $model->salesman;
        $initSalesmanOption = array(
            'value'=>$salesman->id,
            'label'=>$salesman->username
        );
        $model->salesman_select2 = $initSalesmanOption;

        //biz partner select2
        $deliveryPoint = $model->deliveryPoint;
        $model->delivery_point_unit_no = $deliveryPoint->unit_no;
        $model->delivery_point_building_name = $deliveryPoint->building_name;
        $model->delivery_point_street_name = $deliveryPoint->street_name;
        $model->delivery_point_district_01 = $deliveryPoint->district_01;
        $model->delivery_point_district_02 = $deliveryPoint->district_02;
        $model->delivery_point_postcode = $deliveryPoint->postcode;
        $model->delivery_point_state_name = $deliveryPoint->state_name;
        $model->delivery_point_country_name = $deliveryPoint->country_name;
        $initDeliveryPointOption = array(
            'value'=>$deliveryPoint->id,
            'label'=>$deliveryPoint->code.' '.$deliveryPoint->company_name_01
        );
        $model->delivery_point_select2 = $initDeliveryPointOption;

        $model->hdr_disc_val_01 = 0;
        $model->hdr_disc_perc_01 = 0;
        $model->hdr_disc_val_02 = 0;
        $model->hdr_disc_perc_02 = 0;
        $model->hdr_disc_val_03 = 0;
        $model->hdr_disc_perc_03 = 0;
        $model->hdr_disc_val_04 = 0;
        $model->hdr_disc_perc_04 = 0;
        $model->hdr_disc_val_05 = 0;
        $model->hdr_disc_perc_05 = 0;
        $firstDtlModel = OutbOrdDtlRepository::findTopByHdrId($model->id);
        if(!empty($firstDtlModel))
        {
            $model->hdr_disc_val_01 = $firstDtlModel->hdr_disc_val_01;
            $model->hdr_disc_perc_01 = $firstDtlModel->hdr_disc_perc_01;
            $model->hdr_disc_val_02 = $firstDtlModel->hdr_disc_val_02;
            $model->hdr_disc_perc_02 = $firstDtlModel->hdr_disc_perc_02;
            $model->hdr_disc_val_03 = $firstDtlModel->hdr_disc_val_03;
            $model->hdr_disc_perc_03 = $firstDtlModel->hdr_disc_perc_03;
            $model->hdr_disc_val_04 = $firstDtlModel->hdr_disc_val_04;
            $model->hdr_disc_perc_04 = $firstDtlModel->hdr_disc_perc_04;
            $model->hdr_disc_val_05 = $firstDtlModel->hdr_disc_val_05;
            $model->hdr_disc_perc_05 = $firstDtlModel->hdr_disc_perc_05;
        }

        return $model;
	}
	
	public function transitionToStatus($hdrId, $strDocStatus)
    {
        $hdrModel = null;
        if(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['WIP'])
        {
            $hdrModel = self::transitionToWip($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['COMPLETE'])
        {
            $hdrModel = self::transitionToComplete($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['DRAFT'])
        {
            $hdrModel = self::transitionToDraft($hdrId);
		}
		elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['VOID'])
        {
            $hdrModel = self::transitionToVoid($hdrId);
        }
        
        $documentHeader = self::processOutgoingHeader($hdrModel);

        $message = __('OutbOrd.document_successfully_transition', ['docCode'=>$documentHeader->doc_code,'strDocStatus'=>$strDocStatus]);
        
        return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>array(),
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}
	
	static public function transitionToDraft($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = OutbOrdHdrRepository::txnFindByPk($hdrId);

		//only WIP or COMPLETE can transition to DRAFT
		if($hdrModel->doc_status == DocStatus::$MAP['WIP'])
		{
            $hdrModel = OutbOrdHdrRepository::revertWipToDraft($hdrModel->id);
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		elseif($hdrModel->doc_status == DocStatus::$MAP['COMPLETE'])
		{
			AuthService::authorize(
				array(
					'outb_ord_revert'
				)
			);

            $hdrModel = OutbOrdHdrRepository::revertCompleteToDraft($hdrModel->id);
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		elseif($hdrModel->doc_status == DocStatus::$MAP['VOID'])
		{
			AuthService::authorize(
				array(
					'outb_ord_confirm'
				)
			);

			$hdrModel = OutbOrdHdrRepository::commitVoidToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

		$exc = new ApiException(__('OutbOrd.doc_status_is_not_wip_or_complete', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\OutbOrdHdr::class, $hdrModel->id);
		throw $exc;
	}
	
	public function createDetail($hdrId, $data)
	{
		AuthService::authorize(
			array(
				'outb_ord_update'
			)
		);

        $data = self::processIncomingDetail($data);

        $outbOrdHdr = OutbOrdHdrRepository::txnFindByPk($hdrId);
        if($outbOrdHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('OutbOrd.doc_status_is_wip_or_complete', ['docCode'=>$outbOrdHdr->doc_code]));
            $exc->addData(\App\OutbOrdHdr::class, $outbOrdHdr->id);
            throw $exc;
        }
		$outbOrdHdrData = $outbOrdHdr->toArray();

		$outbOrdDtlArray = array();
		$outbOrdDtlModels = OutbOrdDtlRepository::findAllByHdrId($hdrId);
		foreach($outbOrdDtlModels as $outbOrdDtlModel)
		{
			$outbOrdDtlData = $outbOrdDtlModel->toArray();
			$outbOrdDtlData['is_modified'] = 0;
			$outbOrdDtlArray[] = $outbOrdDtlData;
		}
		$lastLineNo = count($outbOrdDtlModels);

		//assign temp id
		$tmpUuid = uniqid();
		//append NEW here to make sure it will be insert in repository later
		$data['id'] = 'NEW'.$tmpUuid;
		$data['line_no'] = $lastLineNo + 1;
        $data['is_modified'] = 1;
		$data = $this->updateSaleItemUom($data, $outbOrdHdr['doc_date'], 0, $outbOrdHdr['currency_id'], 0, 0);
		$outbOrdDtlData = $this->initOutbOrdDtlData($data);
		$outbOrdDtlArray[] = $outbOrdDtlData;

		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($outbOrdHdrData, $outbOrdDtlArray);
		$outbOrdHdrData = $result['hdrData'];
        $outbOrdDtlArray = $result['dtlArray'];

		$result = OutbOrdHdrRepository::updateDetails($outbOrdHdrData, $outbOrdDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('OutbOrd.detail_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function deleteDetails($hdrId, $dtlArray)
	{
		AuthService::authorize(
			array(
				'outb_ord_update'
			)
		);

        $outbOrdHdr = OutbOrdHdrRepository::txnFindByPk($hdrId);
        if($outbOrdHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('OutbOrd.doc_status_is_wip_or_complete', ['docCode'=>$outbOrdHdr->doc_code]));
            $exc->addData(\App\OutbOrdHdr::class, $outbOrdHdr->id);
            throw $exc;
        }
		$outbOrdHdrData = $outbOrdHdr->toArray();

		//query the outbOrdDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$delOutbOrdDtlArray = array();
		$outbOrdDtlArray = array();
        $outbOrdDtlModels = OutbOrdDtlRepository::findAllByHdrId($hdrId);
        $lineNo = 1;
		foreach($outbOrdDtlModels as $outbOrdDtlModel)
		{
			$outbOrdDtlData = $outbOrdDtlModel->toArray();
			$outbOrdDtlData['is_modified'] = 0;
			$outbOrdDtlData['is_deleted'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{
				if($dtlData['id'] == $outbOrdDtlData['id'])
				{
					//this is deleted dtl
					$outbOrdDtlData['is_deleted'] = 1;
					break;
				}
			}
			if($outbOrdDtlData['is_deleted'] > 0)
			{
				$delOutbOrdDtlArray[] = $outbOrdDtlData;
			}
			else
			{
                $outbOrdDtlData['line_no'] = $lineNo;
                $outbOrdDtlData['is_modified'] = 1;
                $outbOrdDtlArray[] = $outbOrdDtlData;
                $lineNo++;
			}
        }
        
		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($outbOrdHdrData, $outbOrdDtlArray, $delOutbOrdDtlArray);
		$outbOrdHdrData = $result['hdrData'];
		$modifiedDtlArray = $result['dtlArray'];
		foreach($outbOrdDtlArray as $outbOrdDtlSeq => $outbOrdDtlData)
		{
			foreach($modifiedDtlArray as $modifiedDtlData)
			{
				if($modifiedDtlData['id'] == $outbOrdDtlData['id'])
				{
					foreach($modifiedDtlData as $field => $value)
					{
						$outbOrdDtlData[$field] = $value;
					}
					$outbOrdDtlData['is_modified'] = 1;
					$outbOrdDtlArray[$outbOrdDtlSeq] = $outbOrdDtlData;
					break;
				}
			}
		}
		
		$result = OutbOrdHdrRepository::updateDetails($outbOrdHdrData, $outbOrdDtlArray, $delOutbOrdDtlArray);
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('OutbOrd.details_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>$delOutbOrdDtlArray
            ),
			'message' => $message
		);
    }
    
    public function updateDetails($hdrId, $dtlArray)
	{
		AuthService::authorize(
			array(
				'outb_ord_update'
			)
		);

		//get the associated inbOrdHdr
        $outbOrdHdr = OutbOrdHdrRepository::txnFindByPk($hdrId);
        if($outbOrdHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('OutbOrd.doc_status_is_wip_or_complete', ['docCode'=>$outbOrdHdr->doc_code]));
            $exc->addData(\App\OutbOrdHdr::class, $outbOrdHdr->id);
            throw $exc;
        }
		$outbOrdHdrData = $outbOrdHdr->toArray();
		$outbOrdHdr->load('deliveryPoint');
		$deliveryPoint = $outbOrdHdr->deliveryPoint;

		//query the outbOrdDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$promoUuidArray = array();
		$outbOrdDtlArray = array();
        $outbOrdDtlModels = OutbOrdDtlRepository::findAllByHdrId($hdrId);
		foreach($outbOrdDtlModels as $outbOrdDtlModel)
		{
			$outbOrdDtlData = $outbOrdDtlModel->toArray();
			$outbOrdDtlData['is_modified'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{                
				if($dtlData['id'] == $outbOrdDtlData['id'])
				{
					$item = ItemRepository::findByPk($dtlData['item_id']);
					$dtlData = self::processIncomingDetail($dtlData, $item);

					$uuid = $outbOrdDtlData['promo_uuid'];
					$dtlData = $this->updateSaleItemUom($dtlData, $outbOrdHdr->doc_date, 0, $outbOrdHdr->currency_id, 0, 0);
					foreach($dtlData as $fieldName => $value)
					{
						$outbOrdDtlData[$fieldName] = $value;
					}
					//$outbOrdDtlData = $this->clearPromotion($outbOrdDtlData);
					$outbOrdDtlData['is_modified'] = 1;

					if(!empty($uuid))
					{
						$promoUuidArray[] = $uuid;
					}
					break;
				}
			}
			$outbOrdDtlArray[] = $outbOrdDtlData;
		}

		//another loop to reset the promotion if this record is updated
		foreach($outbOrdDtlArray as $outbOrdDtlSeq => $outbOrdDtlData)
		{
			foreach($promoUuidArray as $promoUuid)
			{
				if($promoUuid == $outbOrdDtlData['promo_uuid'])
				{
					//$outbOrdDtlData = $this->clearPromotion($outbOrdDtlData);
					$outbOrdDtlData['is_modified'] = 1;
					break;
				}
			}
			$outbOrdDtlArray[$outbOrdDtlSeq] = $outbOrdDtlData;
		}
		
		//promotion
		$modifiedDtlArray = $this->processPromotion($outbOrdDtlArray, $outbOrdHdr->doc_date, $outbOrdHdr->division_id, 
		$deliveryPoint->debtor_id, $deliveryPoint->debtor_group_01_id, $deliveryPoint->debtor_group_02_id, $deliveryPoint->debtor_group_03_id, $deliveryPoint->debtor_group_04_id, $deliveryPoint->debtor_group_05_id);
		foreach($outbOrdDtlArray as $key => $outbOrdDtlData)
		{
			foreach($modifiedDtlArray as $modifiedDtlData)
			{
				if($modifiedDtlData['id'] == $outbOrdDtlData['id'])
				{
					foreach($modifiedDtlData as $field => $value)
					{
						$outbOrdDtlData[$field] = $value;
					}
					$outbOrdDtlData['is_modified'] = 1;
					$outbOrdDtlArray[$key] = $outbOrdDtlData;
				}
			}
		}
		
		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($outbOrdHdrData, $outbOrdDtlArray);
		$outbOrdHdrData = $result['hdrData'];
		$outbOrdDtlArray = $result['dtlArray'];
		
		$result = OutbOrdHdrRepository::updateDetails($outbOrdHdrData, $outbOrdDtlArray, array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('OutbOrd.details_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }
    
    public function updateHeader($hdrData)
	{
		AuthService::authorize(
			array(
				'outb_ord_update'
			)
		);

        $hdrData = self::processIncomingHeader($hdrData);

		//get the associated inbOrdHdr
        $outbOrdHdr = OutbOrdHdrRepository::txnFindByPk($hdrData['id']);
        if($outbOrdHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('OutbOrd.doc_status_is_wip_or_complete', ['docCode'=>$outbOrdHdr->doc_code]));
            $exc->addData(\App\OutbOrdHdr::class, $outbOrdHdr->id);
            throw $exc;
        }
        $outbOrdHdrData = $outbOrdHdr->toArray();
        //assign hdrData to model
        foreach($hdrData as $field => $value) 
        {
            if(strpos($field, 'hdr_disc_val') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_disc_perc') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_tax_val') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_tax_perc') !== false)
            {
                continue;
            }
            $outbOrdHdrData[$field] = $value;
        }

		//$outbOrdHdr->load('inbOrdHdr', 'bizPartner');
        //$bizPartner = $outbOrdHdr->bizPartner;
        
        $outbOrdDtlArray = array();

        //check if hdr disc or hdr tax is set and more than 0
        $needToProcessDetails = false;
        $firstDtlModel = OutbOrdDtlRepository::findTopByHdrId($hdrData['id']);
        if(!empty($firstDtlModel))
        {
            $firstDtlData = $firstDtlModel->toArray();
            for($a = 1; $a <= 5; $a++)
            {
                if(array_key_exists('hdr_disc_val_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_disc_val_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_disc_val_0'.$a], $firstDtlData['hdr_disc_val_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
                if(array_key_exists('hdr_disc_perc_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_disc_perc_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_disc_perc_0'.$a], $firstDtlData['hdr_disc_perc_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }

                if(array_key_exists('hdr_tax_val_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_tax_val_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_tax_val_0'.$a], $firstDtlData['hdr_tax_val_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
                if(array_key_exists('hdr_tax_perc_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_tax_perc_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_tax_perc_0'.$a], $firstDtlData['hdr_tax_perc_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
            }
        }

        if($needToProcessDetails)
        {
            //query the outbOrdDtlModels from DB
            //format to array, with is_modified field to indicate it is changed
            $outbOrdDtlModels = OutbOrdDtlRepository::findAllByHdrId($hdrData['id']);
            foreach($outbOrdDtlModels as $outbOrdDtlModel)
            {
                $outbOrdDtlData = $outbOrdDtlModel->toArray();
                for($a = 1; $a <= 5; $a++)
                {
                    if(array_key_exists('hdr_disc_val_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_disc_val_0'.$a], 0, 5) > 0)
                    {
                        $outbOrdDtlData['hdr_disc_val_0'.$a] = $hdrData['hdr_disc_val_0'.$a];
                    }
                    if(array_key_exists('hdr_disc_perc_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_disc_perc_0'.$a], 0, 5) > 0)
                    {
                        $outbOrdDtlData['hdr_disc_perc_0'.$a] = $hdrData['hdr_disc_perc_0'.$a];
                    }

                    if(array_key_exists('hdr_tax_val_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_tax_val_0'.$a], 0, 5) > 0)
                    {
                        $outbOrdDtlData['hdr_tax_val_0'.$a] = $hdrData['hdr_tax_val_0'.$a];
                    }
                    if(array_key_exists('hdr_tax_perc_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_tax_perc_0'.$a], 0, 5) > 0)
                    {
                        $outbOrdDtlData['hdr_tax_perc_0'.$a] = $hdrData['hdr_tax_perc_0'.$a];
                    }
                }
                $outbOrdDtlData['is_modified'] = 1;
                $outbOrdDtlArray[] = $outbOrdDtlData;
            }
            
            //calculate details amount, use adaptive rounding tax
            $result = $this->calculateAmount($outbOrdHdrData, $outbOrdDtlArray);
            $outbOrdHdrData = $result['hdrData'];
            $outbOrdDtlArray = $result['dtlArray'];
        }
            
        $result = OutbOrdHdrRepository::updateDetails($outbOrdHdrData, $outbOrdDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];

        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('OutbOrd.document_successfully_updated', ['docCode'=>$documentHeader->doc_code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}
	
	static public function transitionToWip($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = OutbOrdHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('OutbOrd.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\OutbOrdHdr::class, $hdrModel->id);
            throw $exc;
        }
        
		//commit the document
		$hdrModel = OutbOrdHdrRepository::commitToWip($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

		$exc = new ApiException(__('OutbOrd.commit_to_wip_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\OutbOrdHdr::class, $hdrModel->id);
        throw $exc;
	}

	static public function processIncomingDetail($data, $item)
    {
        $data['qty'] = round($data['qty'], $item->qty_scale);

        //preprocess the select2 and dropDown
        if(array_key_exists('item_select2', $data))
        {
            $data['item_id'] = $data['item_select2']['value'];
            unset($data['item_select2']);
        }
        if(array_key_exists('uom_select2', $data))
        {
            $data['uom_id'] = $data['uom_select2']['value'];
            unset($data['uom_select2']);
        }
        if(array_key_exists('item_code', $data))
        {
            unset($data['item_code']);
        }
        if(array_key_exists('uom_code', $data))
        {
            unset($data['uom_code']);
        }
        if(array_key_exists('item', $data))
        {
            unset($data['item']);
        }
        if(array_key_exists('uom', $data))
        {
            unset($data['uom']);
		}
		
		if(array_key_exists('item_code', $data))
        {
            unset($data['item_code']);
		}
		if(array_key_exists('item_ref_code_01', $data))
        {
            unset($data['item_ref_code_01']);
		}
		if(array_key_exists('item_desc_01', $data))
        {
            unset($data['item_desc_01']);
		}
		if(array_key_exists('item_desc_02', $data))
        {
            unset($data['item_desc_02']);
		}
		if(array_key_exists('storage_class', $data))
        {
            unset($data['storage_class']);
		}
		if(array_key_exists('item_group_01_code', $data))
        {
            unset($data['item_group_01_code']);
		}
		if(array_key_exists('item_group_02_code', $data))
        {
            unset($data['item_group_02_code']);
		}
		if(array_key_exists('item_group_03_code', $data))
        {
            unset($data['item_group_03_code']);
		}
		if(array_key_exists('item_group_04_code', $data))
        {
            unset($data['item_group_04_code']);
		}
		if(array_key_exists('item_group_05_code', $data))
        {
            unset($data['item_group_05_code']);
		}
		if(array_key_exists('item_cases_per_pallet_length', $data))
        {
            unset($data['item_cases_per_pallet_length']);
		}
		if(array_key_exists('item_cases_per_pallet_width', $data))
        {
            unset($data['item_cases_per_pallet_width']);
		}
		if(array_key_exists('item_no_of_layers', $data))
        {
            unset($data['item_no_of_layers']);
		}

		if(array_key_exists('unit_qty', $data))
        {
            unset($data['unit_qty']);
		}
		if(array_key_exists('item_unit_uom_code', $data))
        {
            unset($data['item_unit_uom_code']);
		}
		if(array_key_exists('loose_qty', $data))
        {
            unset($data['loose_qty']);
		}
		if(array_key_exists('item_loose_uom_code', $data))
        {
            unset($data['item_loose_uom_code']);
        }
        if(array_key_exists('loose_uom_id', $data))
        {
            unset($data['loose_uom_id']);
		}
		if(array_key_exists('loose_uom_rate', $data))
        {
            unset($data['loose_uom_rate']);
		}
		if(array_key_exists('case_qty', $data))
        {
            unset($data['case_qty']);
		}
		if(array_key_exists('case_uom_rate', $data))
        {
            unset($data['case_uom_rate']);
		}
        if(array_key_exists('item_case_uom_code', $data))
        {
            unset($data['item_case_uom_code']);
        }
        if(array_key_exists('uom_code', $data))
        {
            unset($data['uom_code']);
		}
		if(array_key_exists('item_unit_barcode', $data))
        {
            unset($data['item_unit_barcode']);
		}
		if(array_key_exists('item_case_barcode', $data))
        {
            unset($data['item_case_barcode']);
		}
		if(array_key_exists('gross_weight', $data))
        {
            unset($data['gross_weight']);
		}
		if(array_key_exists('cubic_meter', $data))
        {
            unset($data['cubic_meter']);
		}
		
		if(array_key_exists('location_code', $data))
        {
            unset($data['location_code']);
		}
		if(array_key_exists('location', $data))
        {
            unset($data['location']);
		}		
        return $data;
    }
	
	static public function processIncomingHeader($data, $isClearHdrDiscTax=false)
    {
        if(array_key_exists('doc_flows', $data))
        {
            unset($data['doc_flows']);
        }
        if(array_key_exists('submit_action', $data))
        {
            unset($data['submit_action']);
        }
        //preprocess the select2 and dropDown
        if(array_key_exists('credit_term_select2', $data))
        {
            $data['credit_term_id'] = $data['credit_term_select2']['value'];
            unset($data['credit_term_select2']);
        }
        if(array_key_exists('currency_select2', $data))
        {
            $data['currency_id'] = $data['currency_select2']['value'];
            unset($data['currency_select2']);
        }   
        if(array_key_exists('salesman_select2', $data))
        {
            $data['salesman_id'] = $data['salesman_select2']['value'];
            unset($data['salesman_select2']);
        }
        if(array_key_exists('delivery_point_select2', $data))
        {
            $data['delivery_point_id'] = $data['delivery_point_select2']['value'];
            unset($data['delivery_point_select2']);
        }
        if(array_key_exists('str_doc_status', $data))
        {
            unset($data['str_doc_status']);
        }
        if(array_key_exists('division_code', $data))
        {
            unset($data['division_code']);
        }
        if(array_key_exists('company_code', $data))
        {
            unset($data['company_code']);
        }
        if(array_key_exists('delivery_point_unit_no', $data))
        {
            unset($data['delivery_point_unit_no']);
        }
        if(array_key_exists('delivery_point_building_name', $data))
        {
            unset($data['delivery_point_building_name']);
        }
        if(array_key_exists('delivery_point_street_name', $data))
        {
            unset($data['delivery_point_street_name']);
        }
        if(array_key_exists('delivery_point_district_01', $data))
        {
            unset($data['delivery_point_district_01']);
        }
        if(array_key_exists('delivery_point_district_02', $data))
        {
            unset($data['delivery_point_district_02']);
        }
        if(array_key_exists('delivery_point_postcode', $data))
        {
            unset($data['delivery_point_postcode']);
        }
        if(array_key_exists('delivery_point_state_name', $data))
        {
            unset($data['delivery_point_state_name']);
        }
        if(array_key_exists('delivery_point_country_name', $data))
        {
            unset($data['delivery_point_country_name']);
        }
        if(array_key_exists('division', $data))
        {
            unset($data['division']);
        }
        if(array_key_exists('company', $data))
        {
            unset($data['company']);
        }
        if(array_key_exists('salesman', $data))
        {
            unset($data['salesman']);
        }
        if(array_key_exists('delivery_point', $data))
        {
            unset($data['delivery_point']);
        }

        if($isClearHdrDiscTax)
        {  
            for($a = 1; $a <= 5; $a++)
            {
                if(array_key_exists('hdr_disc_val_0'.$a, $data))
                {
                    unset($data['hdr_disc_val_0'.$a]);
                }
                if(array_key_exists('hdr_disc_perc_0'.$a, $data))
                {
                    unset($data['hdr_disc_perc_0'.$a]);
                }
                if(array_key_exists('hdr_tax_val_0'.$a, $data))
                {
                    unset($data['hdr_tax_val_0'.$a]);
                }
                if(array_key_exists('hdr_tax_perc_0'.$a, $data))
                {
                    unset($data['hdr_tax_perc_0'.$a]);
                }
            }
        }
        return $data;
    }
	
	public function changeDeliveryPoint($deliveryPointId)
    {
		AuthService::authorize(
			array(
				'outb_ord_update'
			)
		);

        $results = array();
        $deliveryPoint = DeliveryPointRepository::findByPk($deliveryPointId);
        if(!empty($deliveryPoint))
        {
            $results['delivery_point_unit_no'] = $deliveryPoint->unit_no;
            $results['delivery_point_building_name'] = $deliveryPoint->building_name;
            $results['delivery_point_street_name'] = $deliveryPoint->street_name;
            $results['delivery_point_district_01'] = $deliveryPoint->district_01;
            $results['delivery_point_district_02'] = $deliveryPoint->district_02;
            $results['delivery_point_postcode'] = $deliveryPoint->postcode;
            $results['delivery_point_state_name'] = $deliveryPoint->state_name;
            $results['delivery_point_country_name'] = $deliveryPoint->country_name;
        }
        return $results;
    }

    public function changeItem($hdrId, $itemId)
    {
		AuthService::authorize(
			array(
				'outb_ord_update'
			)
		);

        $outbOrdHdr = OutbOrdHdrRepository::findByPk($hdrId);

        $results = array();
        $item = ItemRepository::findByPk($itemId);
        if(!empty($item)
        && !empty($outbOrdHdr))
        {
            $results['desc_01'] = $item->desc_01;
            $results['desc_02'] = $item->desc_02;
        }
        return $results;
    }

    public function changeItemUom($hdrId, $itemId, $uomId)
    {
		AuthService::authorize(
			array(
				'outb_ord_update'
			)
		);

        $outbOrdHdr = OutbOrdHdrRepository::findByPk($hdrId);

        $results = array();
        $itemUom = ItemUomRepository::findByItemIdAndUomId($itemId, $uomId);
        if(!empty($itemUom)
        && !empty($outbOrdHdr))
        {
            $results['item_id'] = $itemUom->item_id;
            $results['uom_id'] = $itemUom->uom_id;
            $results['uom_rate'] = $itemUom->uom_rate;

            $results = $this->updateSaleItemUom($results, $outbOrdHdr->doc_date, 0, $outbOrdHdr->currency_id, 0, 0);
        }
        return $results;
	}
	
	public function changeCurrency($currencyId)
    {
		AuthService::authorize(
			array(
				'outb_ord_update'
			)
		);
		
        $results = array();
        $currency = CurrencyRepository::findByPk($currencyId);
        if(!empty($currency))
        {
            $results['currency_rate'] = $currency->currency_rate;
        }
        return $results;
    }
}
