<?php

namespace App\Services;

use App\Uom;
use App\Item;
use App\CycleCountHdr;
use App\QuantBal;
use App\Services\Env\ResStatus;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Services\Env\WhseJobType;
use App\Services\Env\HandlingType;
use App\Services\Env\PhysicalCountStatus;
use App\Imports\CycleCountExcel01Import;
use App\Services\Utils\ApiException;
use App\Repositories\SiteDocNoRepository;
use App\Repositories\WhseTxnFlowRepository;
use App\Repositories\CycleCountHdrRepository;
use App\Repositories\CycleCountDtlRepository;
use App\Repositories\StorageBinRepository;
use App\Repositories\QuantBalRepository;
use App\Repositories\SiteFlowRepository;
use App\Repositories\StorageTypeRepository;
use App\Repositories\BatchJobStatusRepository;
use App\Repositories\ItemBatchRepository;
use App\Repositories\ItemUomRepository;
use App\Repositories\ItemRepository;
use App\Repositories\UomRepository;
use App\Repositories\DocNoRepository;
use App\Repositories\QuantBalAdvTxnRepository;
use App\Repositories\QuantBalRsvdTxnRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CycleCountService extends InventoryService
{
    public function __construct() 
	{
    }
    
    public function indexProcess($strProcType, $siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['CYCLE_COUNT_01']) 
            {
                //STORAGE_BINS -> CYCLE_COUNT
                $storageBins = $this->indexCycleCount01($siteFlowId, $sorts, $filters, $pageSize = 20);
                return $storageBins;
            }
            elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['CYCLE_COUNT_02']) 
            {
                //CYCLE_COUNT RECOUNT
                $storageBins = $this->indexCycleCount02($siteFlowId, $sorts, $filters, $pageSize = 20);
                return $storageBins;
            }
            elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['CYCLE_COUNT_03']) 
            {
                //CYCLE_COUNT RECOUNT
                $storageBins = $this->indexCycleCount03($siteFlowId, $sorts, $filters, $pageSize = 20);
                return $storageBins;
            }
		}

        return array();
    }

    protected function indexCycleCount01($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
            array(
                'storage_bin_read'
            )
        );

        $storageBins = StorageBinRepository::findAll($sorts, $filters, $pageSize);
        
        $storageBins->load('site', 'location', 'storageBay', 'storageRow', 'storageType');
		foreach($storageBins as $storageBin)
		{
            $storageBin->str_bin_status = ResStatus::$MAP[$storageBin->bin_status];
            $storageRow = $storageBin->storageRow;

            $storageBin->site_code = !empty($storageBin->site) ? $storageBin->site->code : '';            
            $storageBin->location_code = !empty($storageBin->location) ? $storageBin->location->code : '';                 
            $storageBin->storage_bay_code = !empty($storageBin->storageBay) ? $storageBin->storageBay->code : '';              
            $storageBin->storage_row_code = $storageRow->code;
            $storageBin->aisle_code = $storageRow->aisle_code;
            $storageBin->storage_type_code = !empty($storageBin->storageType) ? $storageBin->storageType->code : '';
            
            $jobCycleCountHdr = array();
            $cycleCountHdrs = CycleCountHdrRepository::findLatestByStorageBinId($storageBin->id);
            $cycleCountHdrs->load('user');
            foreach($cycleCountHdrs as $cycleCountHdr)
			{
                $cycleCountHdr->username = !empty($cycleCountHdr->user) ? $cycleCountHdr->user->username : '';
				$jobCycleCountHdr[] = $cycleCountHdr;
            }
            
            $storageBin->details = $jobCycleCountHdr;

            unset($storageBin->site);
            unset($storageBin->location);
            unset($storageBin->storageBay);
            unset($storageBin->storageRow);
            unset($storageBin->storageType);
		}
        return $storageBins;
    }

    protected function indexCycleCount02($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
            array(
                'cycle_count_read'
            )
        );

        $qStatuses = array(DocStatus::$MAP['WIP'], DocStatus::$MAP['COMPLETE']);
        $cycleCountHdrs = CycleCountHdrRepository::findAllNotExistCountAdj01Txn($siteFlowId, $qStatuses, $sorts, $filters, $pageSize);
        $cycleCountHdrs->load(
			'cycleCountDtls',
			'cycleCountDtls.item', 'cycleCountDtls.storageBin', 
			'cycleCountDtls.storageBin.storageBay', 'cycleCountDtls.storageBin.storageRow'
		);
        foreach($cycleCountHdrs as $cycleCountHdr)
		{
            $cycleCountHdr->str_doc_status = DocStatus::$MAP[$cycleCountHdr->doc_status];

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$cycleCountDtls = $cycleCountHdr->cycleCountDtls;

			//sort by job_no, group_no, line_no
			$cycleCountDtls = $cycleCountDtls->sort(function ($a, $b) {
				if($a->job_no == $b->job_no)
				{
					if($a->group_no == $b->group_no)
					{
						return $a->line_no > $b->line_no;
					}
					return $a->group_no > $b->group_no;
				}
				return $a->job_no > $b->job_no;
			});

			foreach($cycleCountDtls as $cycleCountDtl)
			{
				$item = $cycleCountDtl->item;
				if(empty($item))
				{
					$item = new Item;
					$item->code = '';
					$item->ref_code_01 = '';
					$item->desc_01 = '';
					$item->desc_02 = '';
					$item->storage_class = 0;
					$item->item_group_01_code = '';
					$item->item_group_02_code = '';
					$item->item_group_03_code = '';
					$item->item_group_04_code = '';
					$item->item_group_05_code = '';
					$item->case_ext_length = 0;
					$item->case_ext_width = 0;
					$item->case_ext_height = 0;
					$item->uom_rate = 1;
					$item->case_uom_rate = 35000;
					$item->case_gross_weight = 0;
				}
				$storageBin = $cycleCountDtl->storageBin;

				$whse_job_key = $cycleCountDtl->whse_job_type.':'.$cycleCountDtl->whse_job_code;
				$cycleCountDtl->whse_job_key = $whse_job_key;
				$cycleCountDtl->str_whse_job_type = WhseJobType::$MAP[$cycleCountDtl->whse_job_type];
				$cycleCountDtl->str_whse_job_type = __('WhseJob.'.strtolower($cycleCountDtl->str_whse_job_type));

				$cycleCountDtl->storage_bin_code = '';
				$cycleCountDtl->storage_row_code = '';
				$cycleCountDtl->storage_bay_code = '';
				$cycleCountDtl->item_bay_sequence = 0;
				if(!empty($storageBin))
				{
					$cycleCountDtl->storage_bin_code = $storageBin->code;
					if(!empty($storageBin->storageRow))
					{
						$cycleCountDtl->storage_row_code = $storageBin->storageRow->code;
					}				
					if(!empty($storageBin->storageBay))
					{
						$cycleCountDtl->storage_bay_code = $storageBin->storageBay->code;
						$cycleCountDtl->item_bay_sequence = $storageBin->storageBay->bay_sequence;
					}
				}

				$cycleCountDtl = ItemService::processCaseLoose($cycleCountDtl, $item);

				$cycleCountDtl->pallet_qty = 0;
				if($cycleCountDtl->handling_unit_id > 0)
				{
					//then tis is full pallet picking
					$cycleCountDtl->pallet_qty = 1;
					$cycleCountDtl->case_qty = 0;
				}

				$caseQty = bcadd($caseQty, $cycleCountDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $cycleCountDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $cycleCountDtl->cubic_meter, 8);

				if($cycleCountDtl->handling_unit_id > 0)
				{
					$palletHashByHandingUnitId[$cycleCountDtl->handling_unit_id] = $cycleCountDtl->handling_unit_id;
				}

				unset($cycleCountDtl->storageBin);
				unset($cycleCountDtl->item);
			}
			
			$cycleCountHdr->pallet_qty = count($palletHashByHandingUnitId);
			$cycleCountHdr->case_qty = $caseQty;
			$cycleCountHdr->gross_weight = $grossWeight;
			$cycleCountHdr->cubic_meter = $cubicMeter;

			$cycleCountHdr->details = $cycleCountDtls->values()->all();

			unset($cycleCountHdr->cycleCountDtls);
		}
        return $cycleCountHdrs;
    }

    protected function indexCycleCount03($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
            array(
                'cycle_count_read'
            )
        );

        $cycleCountHdrs = CycleCountHdrRepository::findAllDraftAndWIP($siteFlowId, $sorts, $filters, $pageSize);
        $cycleCountHdrs->load(
            'cycleCountDtls',
            'cycleCountDtls.item', 'cycleCountDtls.company'
        );
        foreach($cycleCountHdrs as $cycleCountHdr)
		{
            $cycleCountHdr->str_doc_status = DocStatus::$MAP[$cycleCountHdr->doc_status];
            
            $jobCycleCountDtls = array();
            $cycleCountDtls = $cycleCountHdr->cycleCountDtls;
            
			//query quantBal, and calculate variance
			foreach($cycleCountDtls as $cycleCountDtl)
			{
                $item = $cycleCountDtl->item;

                $cycleCountDtl->count_unit_qty = 0;
                if(!empty($item))
                {
                    $cycleCountDtl->count_unit_qty = bcmul($cycleCountDtl->uom_rate, $cycleCountDtl->qty, $item->qty_scale);
                }

                $company = $cycleCountDtl->company;
                $cycleCountDtl->company_code = $company->code;

                $cycleCountDtl->balance_unit_qty = 0;
				$quantBal = QuantBalRepository::queryByAttributes(
                    $cycleCountDtl->storage_bin_id, $cycleCountDtl->handling_unit_id,
                    $cycleCountDtl->company_id, $cycleCountDtl->item_id, $cycleCountDtl->item_batch_id,
                    $cycleCountDtl->batch_serial_no, $cycleCountDtl->expiry_date, $cycleCountDtl->receipt_date);
                if(!empty($quantBal))
                {
                    $cycleCountDtl->balance_unit_qty = $quantBal->balance_unit_qty;
                }

                $cycleCountDtl->variance_qty = 0;
                if(!empty($item)) 
                {
                    $cycleCountDtl->variance_qty = bcsub($cycleCountDtl->count_unit_qty, $cycleCountDtl->balance_unit_qty, $item->qty_scale);
                }
                $cycleCountDtl->abs_variance_qty = abs($cycleCountDtl->variance_qty);
                $cycleCountDtl->variance_perc = 100;
                $cycleCountDtl->accuracy = 0;
                if(bccomp($cycleCountDtl->balance_unit_qty, 0, 5) > 0)
                {
                    $cycleCountDtl->variance_perc = bcmul(bcdiv($cycleCountDtl->abs_variance_qty, $cycleCountDtl->balance_unit_qty, 10), 100, 10);
                    $cycleCountDtl->accuracy = bcmul(bcdiv($cycleCountDtl->count_unit_qty, $cycleCountDtl->balance_unit_qty, 10), 100, 10);
                }

                unset($cycleCountDtl->item);
                unset($cycleCountDtl->company);

                $jobCycleCountDtls[] = $cycleCountDtl;
            }

            $cycleCountHdr->details = $cycleCountDtls;

			//unset the item, so it wont send in json
		}
        return $cycleCountHdrs;
    }
    
    public function createProcess($strProcType, $siteFlowId, $ids, $data)
    {
        $user = Auth::user();

        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['CYCLE_COUNT_01']) 
            {
                //STORAGE_BIN -> CYCLE_COUNT
                $result = $this->createCycleCount01(ProcType::$MAP[$strProcType], $user, $siteFlowId, $ids);
                return $result;
            }
            elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['CYCLE_COUNT_02']) 
            {
                //CYCLE_COUNT -> RECOUNT
                $result = $this->createCycleCount02(ProcType::$MAP[$strProcType], $user, $siteFlowId, $ids, $data);
                return $result;
            }
            elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['CYCLE_COUNT_01_01']) 
            {
                //CYCLE_COUNT, ADD NEW WITH CRITERIA
                $result = $this->createCycleCount0101(ProcType::$MAP[$strProcType], $user, $siteFlowId, $ids, $data);
                return $result;
            }
		}
    }

    protected function createCycleCount01($procType, $user, $siteFlowId, $ids)
    {        
        AuthService::authorize(
            array(
                'cycle_count_create'
            )
        );

        $siteFlow = SiteFlowRepository::findByPk($siteFlowId);

        $defaultCompanyId = 0;
        //get the default company id
        $userDivisions = $user->userDivisions;
        $userDivisions->load('division');
        foreach($userDivisions as $userDivision)
        {
            $division = $userDivision->division;
            $defaultCompanyId = $division->company_id;
            break;
        }

        $hdrData = array();
        $dtlDataList = array();

		$lineNo = 1;

        $storageHash = array();
        $sorts = array(
            array('field'=>'code','order'=>'ASC'),
        );
        $storageTypes = StorageTypeRepository::findAll($sorts, array(), PHP_INT_MAX);
        //format storageTypes to storageTypesHash
        foreach($storageTypes as $storageType)
        {
            $storageHash[$storageType->id] = $storageType;
        }

        $storageBins = StorageBinRepository::findAllByIds($ids);
        foreach($storageBins as $storageBin)
        {
            $storageType = $storageHash[$storageBin->storage_type_id];
            $whseJobType = WhseJobType::$MAP['CYCLE_COUNT_BIN_LOOSE'];
            if($storageType->handling_type == HandlingType::$MAP['PALLET']
            || $storageType->handling_type == HandlingType::$MAP['TOTE_BOX']
            || $storageType->handling_type == HandlingType::$MAP['CASE_BOX'])
            {
                $whseJobType = WhseJobType::$MAP['CYCLE_COUNT_BIN_PALLET'];
            }

            $quantBals = QuantBalRepository::findAllActiveByStorageBinId($storageBin->id);
            $quantBals->load('item', 'itemBatch');
            foreach($quantBals as $quantBal)
            {
                $item = $quantBal->item;
                $itemBatch = $quantBal->itemBatch;

                $dtlData = array(
                    'line_no' => $lineNo,
                    'storage_bin_id' => $quantBal->storage_bin_id,
                    'company_id' => $quantBal->company_id,
                    'item_id' => $quantBal->item_id,
                    'item_batch_id' => $quantBal->item_batch_id,
                    'batch_serial_no' => $itemBatch->batch_serial_no,
                    'expiry_date' => $itemBatch->expiry_date,
                    'receipt_date' => $itemBatch->receipt_date,
                    'handling_unit_id' => $quantBal->handling_unit_id,
                    'desc_01' => $item->desc_01,
                    'desc_02' => $item->desc_02,
                    'uom_id' => $item->unit_uom_id,
                    'uom_rate' => 1,
                    'qty' => $quantBal->balance_unit_qty,
                    'whse_job_type' => $whseJobType,
                    'whse_job_code' => '',
                    'count_seq' => 1,
                    'to_cycle_count_hdr_id' => 0,
                    'to_cycle_count_dtl_id' => 0
                );
                $dtlDataList[] = $dtlData;

                $lineNo++;
            }

            if(count($quantBals) == 0)
            {
                $dtlData = array(
                    'line_no' => $lineNo,
                    'storage_bin_id' => $storageBin->id,
                    'company_id' => $defaultCompanyId,
                    'item_id' => 0,
                    'item_batch_id' => 0,
                    'batch_serial_no' => '',
                    'expiry_date' => '',
                    'receipt_date' => '',
                    'handling_unit_id' => 0,
                    'desc_01' => '',
                    'desc_02' => '',
                    'uom_id' => 0,
                    'uom_rate' => 0,
                    'qty' => 0,
                    'whse_job_type' => $whseJobType,
                    'whse_job_code' => '',
                    'count_seq' => 1,
                    'to_cycle_count_hdr_id' => 0,
                    'to_cycle_count_dtl_id' => 0
                );
                $dtlDataList[] = $dtlData;

                $lineNo++;
            }
        }
        
        //build the hdrData
        $hdrData = array(
            'site_flow_id' => $siteFlow->id,
            'ref_code_01' => '',
            'ref_code_02' => '',
            'doc_date' => date('Y-m-d'),
            'desc_01' => '',
            'desc_02' => '',
            'group_count' => 1,
            'cur_job_no' => 0,
            'cur_count_seq' => 1,
        );

        $siteDocNo = SiteDocNoRepository::findSiteDocNo($siteFlow->site_id, \App\CycleCountHdr::class);
        if(empty($siteDocNo))
        {
            $exc = new ApiException(__('SiteDocNo.doc_no_not_found', ['siteId'=>$siteFlow->site_id, 'docType'=>\App\CycleCountHdr::class]));
            //$exc->addData(\App\CycleCountHdr::class, $siteFlow->site_id);
            throw $exc;
        }

        $whseTxnFlow = WhseTxnFlowRepository::findByPk($siteFlow->id, $procType);

        //DRAFT
        $cycleCountHdr = CycleCountHdrRepository::createProcess($procType, $siteDocNo->doc_no_id, $hdrData, $dtlDataList);

        if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
        {
            $cycleCountHdr = self::transitionToWip($cycleCountHdr->id);
        }
        elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
        {
            $cycleCountHdr = self::transitionToComplete($cycleCountHdr->id);
        }

        $message = __('CycleCount.document_successfully_created', ['docCode'=>$cycleCountHdr->doc_code]);

		return array(
			'data' => array($cycleCountHdr),
			'message' => $message
		);
    }

    protected function createCycleCount02($procType, $user, $siteFlowId, $ids, $data)
    {
        AuthService::authorize(
            array(
                'cycle_count_create'
            )
        );

        $siteFlow = SiteFlowRepository::findByPk($siteFlowId);

        $hdrData = array();
        $dtlDataList = array();
        $oriCycleCountDtlDataList = array();

        $groupCount = 1;
        if(array_key_exists('groupCount', $data))
        {
            $groupCount = $data['groupCount'];
        }
        //$jobDesc01 = $data['jobDesc01'];
        $jobDesc01 = '';

		$lineNo = 1;

        //here will have job1 and job2 details
        $cycleCountDtls = CycleCountDtlRepository::findAllByIds($ids);
        $cycleCountDtls->load('cycleCountHdr');

        $cycleCountHdr = null;
        //group the details by similar characteristic
        $cycleCountDtlsHashByKey = array();
        foreach($cycleCountDtls as $cycleCountDtl)
        {
            $cycleCountHdr = $cycleCountDtl->cycleCountHdr;

            //key by $handlingUnitId, $storageBin, $companyId, $itemId, $itemBatchId
            $key = $cycleCountDtl->handling_unit_id.'/'.$cycleCountDtl->storage_bin_id.'/'.
                $cycleCountDtl->company_id.'/'.
                $cycleCountDtl->item_id.'/'.$cycleCountDtl->item_batch_id;
            $tmpCycleCountDtls = array();
            if(array_key_exists($key, $cycleCountDtlsHashByKey))
            {
                $tmpCycleCountDtls = $cycleCountDtlsHashByKey[$key];
            }
            $tmpCycleCountDtls[] = $cycleCountDtl;
            $cycleCountDtlsHashByKey[$key] = $tmpCycleCountDtls;

            //add to array, will use to change the physical_count_status later
            $oriCycleCountDtlDataList[] = array(
                'id'=>$cycleCountDtl->id
            );
        }

        foreach($cycleCountDtlsHashByKey as $key => $kCycleCountDtls)
        {
            $firstCycleCountDtl = null;
            foreach($kCycleCountDtls as $kCycleCountDtl)
            {
                $firstCycleCountDtl = $kCycleCountDtl;
            }
            for($a = 1; $a <= $groupCount; $a++)
            {
                $dtlData = array();
                $dtlData['group_no'] = $a;
                $dtlData['job_no'] = 1;
                $dtlData['physical_count_status'] = PhysicalCountStatus::$MAP['COUNTING'];
                $dtlData['job_desc_01'] = $jobDesc01;
                $dtlData['job_desc_02'] = '';
                $dtlData['line_no'] = $lineNo++;
                $dtlData['count_seq'] = bcadd($cycleCountHdr->cur_count_seq, 1);
                $dtlData['storage_bin_id'] = $firstCycleCountDtl->storage_bin_id;
                $dtlData['company_id'] = $firstCycleCountDtl->company_id;
                $dtlData['item_id'] = $firstCycleCountDtl->item_id;
                $dtlData['item_batch_id'] = $firstCycleCountDtl->item_batch_id;
                $dtlData['batch_serial_no'] = $firstCycleCountDtl->batch_serial_no;
                $dtlData['expiry_date'] = $firstCycleCountDtl->expiry_date;
                $dtlData['receipt_date'] = $firstCycleCountDtl->receipt_date;
                $dtlData['handling_unit_id'] = $firstCycleCountDtl->handling_unit_id;
                $dtlData['desc_01'] = $firstCycleCountDtl->desc_01;
                $dtlData['desc_02'] =  $firstCycleCountDtl->desc_02;
                $dtlData['uom_id'] = $firstCycleCountDtl->uom_id;
                $dtlData['uom_rate'] = $firstCycleCountDtl->uom_rate;
                $dtlData['qty'] = 0;
                $dtlData['whse_job_type'] = $firstCycleCountDtl->whse_job_type;
                $dtlData['whse_job_code'] = $firstCycleCountDtl->whse_job_code;
                $dtlData['to_cycle_count_hdr_id'] = 0;
                $dtlData['to_cycle_count_dtl_id'] = 0;
                $dtlDataList[] = $dtlData;
            }
        }
        
        //build the hdrData
        $hdrData = array(
            'site_flow_id' => $siteFlow->id,
            'ref_code_01' => '',
            'ref_code_02' => '',
            'doc_date' => date('Y-m-d'),
            'desc_01' => '',
            'desc_02' => '',
            'group_count' => $groupCount,
            'cur_job_no' => 1,
            'cur_count_seq' => bcadd($cycleCountHdr->cur_count_seq, 1),
            'site_flow_id' => $siteFlow->id
        );

        $siteDocNo = SiteDocNoRepository::findSiteDocNo($siteFlow->site_id, \App\CycleCountHdr::class);
        if(empty($siteDocNo))
        {
            $exc = new ApiException(__('SiteDocNo.doc_no_not_found', ['siteId'=>$siteFlow->site_id, 'docType'=>\App\CycleCountHdr::class]));
            //$exc->addData(\App\CycleCountHdr::class, $siteFlow->site_id);
            throw $exc;
        }

        $whseTxnFlow = WhseTxnFlowRepository::findByPk($siteFlow->id, $procType);

        //DRAFT
        $cycleCountHdr = CycleCountHdrRepository::createProcess($procType, $siteDocNo->doc_no_id, $hdrData, $dtlDataList, $oriCycleCountDtlDataList);

        if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
        {
            $cycleCountHdr = self::transitionToWip($cycleCountHdr->id);
        }
        elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
        {
            $cycleCountHdr = self::transitionToComplete($cycleCountHdr->id);
        }

        $message = __('CycleCount.document_successfully_created', ['docCode'=>$cycleCountHdr->doc_code]);

		return array(
			'data' => array($cycleCountHdr),
			'message' => $message
		);
    }

    protected function createCycleCount0101($procType, $user, $siteFlowId, $ids, $data)
    {        
        AuthService::authorize(
            array(
                'cycle_count_create'
            )
        );

        $siteFlow = SiteFlowRepository::findByPk($siteFlowId);

        $defaultCompanyId = 0;
        //get the default company id
        $userDivisions = $user->userDivisions;
        $userDivisions->load('division');
        foreach($userDivisions as $userDivision)
        {
            $division = $userDivision->division;
            $defaultCompanyId = $division->company_id;
            break;
        }

        $hdrData = array();
        $dtlDataList = array();

        $storageHash = array();
        $sorts = array(
            array('field'=>'code','order'=>'ASC'),
        );
        $storageTypes = StorageTypeRepository::findAll($sorts, array(), PHP_INT_MAX);
        //format storageTypes to storageTypesHash
        foreach($storageTypes as $storageType)
        {
            $storageHash[$storageType->id] = $storageType;
        }

        $ref_code_01 = '';
        if(array_key_exists('ref_code_01', $data))
        {
            $ref_code_01 = $data['ref_code_01'];
        }
        $storageRowIds = array();
        if(array_key_exists('storage_row_ids', $data))
        {
            $storageRowIds = $data['storage_row_ids'];
        }
        $storageBayIds = array();
        if(array_key_exists('storage_bay_ids', $data))
        {
            $storageBayIds = $data['storage_bay_ids'];
        }
        $levels = array();
        if(array_key_exists('levels', $data))
        {
            $levels = $data['levels'];
        }

        $lineNo = 1;
        //DB::connection()->enableQueryLog();
        $storageBins = StorageBinRepository::findAllByAttributes($siteFlow->site_id, $storageRowIds, $storageBayIds, $levels);
        //dd(DB::getQueryLog());
        foreach($storageBins as $storageBin)
        {
            if($storageBin->bin_status == ResStatus::$MAP['INACTIVE'])
            {
                continue;
            }
            $storageType = $storageHash[$storageBin->storage_type_id];
            $whseJobType = WhseJobType::$MAP['CYCLE_COUNT_BIN_LOOSE'];
            if($storageType->handling_type == HandlingType::$MAP['PALLET']
            || $storageType->handling_type == HandlingType::$MAP['TOTE_BOX']
            || $storageType->handling_type == HandlingType::$MAP['CASE_BOX'])
            {
                $whseJobType = WhseJobType::$MAP['CYCLE_COUNT_BIN_PALLET'];
            }

            $quantBals = QuantBalRepository::findAllActiveByStorageBinId($storageBin->id);
            $quantBals->load('item', 'itemBatch');
            foreach($quantBals as $quantBal)
            {
                $item = $quantBal->item;
                $itemBatch = $quantBal->itemBatch;

                $dtlData = array(
                    'line_no' => $lineNo,
                    'storage_bin_id' => $quantBal->storage_bin_id,
                    'company_id' => $quantBal->company_id,
                    'item_id' => $quantBal->item_id,
                    'item_batch_id' => $quantBal->item_batch_id,
                    'batch_serial_no' => $itemBatch->batch_serial_no,
                    'expiry_date' => $itemBatch->expiry_date,
                    'receipt_date' => $itemBatch->receipt_date,
                    'handling_unit_id' => $quantBal->handling_unit_id,
                    'desc_01' => $item->desc_01,
                    'desc_02' => $item->desc_02,
                    'uom_id' => $item->unit_uom_id,
                    'uom_rate' => 1,
                    'qty' => $quantBal->balance_unit_qty,
                    'whse_job_type' => $whseJobType,
                    'whse_job_code' => '',
                    'count_seq' => 1,
                    'to_cycle_count_hdr_id' => 0,
                    'to_cycle_count_dtl_id' => 0
                );
                $dtlDataList[] = $dtlData;

                $lineNo++;
            }

            if(count($quantBals) == 0)
            {
                $dtlData = array(
                    'line_no' => $lineNo,
                    'storage_bin_id' => $storageBin->id,
                    'company_id' => $defaultCompanyId,
                    'item_id' => 0,
                    'item_batch_id' => 0,
                    'batch_serial_no' => '',
                    'expiry_date' => '',
                    'receipt_date' => '',
                    'handling_unit_id' => 0,
                    'desc_01' => '',
                    'desc_02' => '',
                    'uom_id' => 0,
                    'uom_rate' => 0,
                    'qty' => 0,
                    'whse_job_type' => $whseJobType,
                    'whse_job_code' => '',
                    'count_seq' => 1,
                    'to_cycle_count_hdr_id' => 0,
                    'to_cycle_count_dtl_id' => 0
                );
                $dtlDataList[] = $dtlData;

                $lineNo++;
            }
        }
        
        //build the hdrData
        $hdrData = array(
            'site_flow_id' => $siteFlow->id,
            'ref_code_01' =>  $ref_code_01,
            'ref_code_02' => '',
            'doc_date' => date('Y-m-d'),
            'desc_01' => '',
            'desc_02' => '',
            'group_count' => 1,
            'cur_job_no' => 0,
            'cur_count_seq' => 1,
        );

        $siteDocNo = SiteDocNoRepository::findSiteDocNo($siteFlow->site_id, \App\CycleCountHdr::class);
        if(empty($siteDocNo))
        {
            $exc = new ApiException(__('SiteDocNo.doc_no_not_found', ['siteId'=>$siteFlow->site_id, 'docType'=>\App\CycleCountHdr::class]));
            //$exc->addData(\App\CycleCountHdr::class, $siteFlow->site_id);
            throw $exc;
        }

        $whseTxnFlow = WhseTxnFlowRepository::findByPk($siteFlow->id, ProcType::$MAP['CYCLE_COUNT_01']);

        //DRAFT
        $cycleCountHdr = CycleCountHdrRepository::createProcess($procType, $siteDocNo->doc_no_id, $hdrData, $dtlDataList);

        if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
        {
            $cycleCountHdr = self::transitionToWip($cycleCountHdr->id);
        }
        elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
        {
            $cycleCountHdr = self::transitionToComplete($cycleCountHdr->id);
        }

        $message = __('CycleCount.document_successfully_created', ['docCode'=>$cycleCountHdr->doc_code]);

		return array(
			'data' => array($cycleCountHdr),
			'message' => $message
		);
    }

    public function transitionToStatus($hdrId, $strDocStatus)
    {
        $hdrModel = null;
        if(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['WIP'])
        {
            $hdrModel = self::transitionToWip($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['COMPLETE'])
        {
            $hdrModel = self::transitionToComplete($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['DRAFT'])
        {
            $hdrModel = self::transitionToDraft($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['VOID'])
        {
            $hdrModel = self::transitionToVoid($hdrId);
        }
        
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $message = __('CycleCount.document_successfully_transition', ['docCode'=>$documentHeader->doc_code,'strDocStatus'=>$strDocStatus]);
        
        return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>array(),
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }

    static public function transitionToWip($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = CycleCountHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status >= DocStatus::$MAP['WIP'])
		{
			$exc = new ApiException(__('CycleCount.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\CycleCountHdr::class, $hdrModel->id);
            throw $exc;
        }
        
        //check any pending pickList or putAway
        $storageBinIdHash = array();
        $cycleCountDtls = $hdrModel->cycleCountDtls;
        foreach($cycleCountDtls as $cycleCountDtl)
        {
            $storageBinIdHash[$cycleCountDtl->storage_bin_id] = $cycleCountDtl->storage_bin_id;
        }

        $docCodes = array();
        $putAwayDocDatas = QuantBalAdvTxnRepository::queryDocsByStorageBinIds(array_keys($storageBinIdHash));
        foreach($putAwayDocDatas as $putAwayDocData)
        {
            $docCodes[] = $putAwayDocData['doc_code'];
        }
        $pickListDocDatas = QuantBalRsvdTxnRepository::queryDocsByStorageBinIds(array_keys($storageBinIdHash));
        foreach($pickListDocDatas as $pickListDocData)
        {
            $docCodes[] = $pickListDocData['doc_code'];
        }
        if(count($docCodes) > 0)
        {
            $docCodeMsg = '';
            for ($a = 0; $a < count($docCodes); $a++)
            {
                $docCode = $docCodes[$a];
                if($a == 0)
                {
                    $docCodeMsg .= $docCode;
                }
                else
                {
                    $docCodeMsg .= ', '.$docCode;
                }
            }
            
            $exc = new ApiException(__('CycleCount.these_documents_are_wip', ['docCode'=>$docCodeMsg]));
            $exc->addData(\App\CycleCountHdr::class, $hdrModel->id);
            throw $exc;
        }

		//commit the document
		$hdrModel = CycleCountHdrRepository::commitToWip($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('CycleCount.commit_to_wip_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\CycleCountHdr::class, $hdrModel->id);
        throw $exc;
	}

	static public function transitionToComplete($hdrId)
    {
        AuthService::authorize(
            array(
                'cycle_count_confirm'
            )
        );

		$isSuccess = false;
		//use transaction to make sure this is latest value
		$hdrModel = CycleCountHdrRepository::txnFindByPk($hdrId);

		//only DRAFT or above can transition to COMPLETE
		if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE']
		|| $hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('CycleCount.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\CycleCountHdr::class, $hdrModel->id);
            throw $exc;
		}

		$dtlModels = CycleCountDtlRepository::findAllByHdrId($hdrModel->id);
		//preliminary checking
		//check the detail stock make sure the detail quant_bal_id > 0
		foreach($dtlModels as $dtlModel)
		{
            if($dtlModel->company_id == 0)
			{
				$exc = new ApiException(__('CycleCount.company_is_required', ['lineNo'=>$dtlModel->line_no]));
				$exc->addData(\App\CycleCountDtl::class, $dtlModel->id);
				throw $exc;
            }
            if($dtlModel->storage_bin_id == 0)
			{
				$exc = new ApiException(__('CycleCount.storage_bin_is_required', ['lineNo'=>$dtlModel->line_no]));
				$exc->addData(\App\CycleCountDtl::class, $dtlModel->id);
				throw $exc;
            }
            /*
			if($dtlModel->item_id == 0)
			{
				$exc = new ApiException(__('CycleCount.item_is_required', ['lineNo'=>$dtlModel->line_no]));
				$exc->addData(\App\CycleCountDtl::class, $dtlModel->id);
				throw $exc;
            }
            */
		}

		//commit the document
		$hdrModel = CycleCountHdrRepository::commitToComplete($hdrModel->id, true);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
        }
        
		$exc = new ApiException(__('CycleCount.commit_to_complete_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\CycleCountHdr::class, $hdrModel->id);
        throw $exc;
    }
    
    static public function transitionToDraft($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = CycleCountHdrRepository::txnFindByPk($hdrId);

		//only WIP or COMPLETE can transition to DRAFT
		if($hdrModel->doc_status == DocStatus::$MAP['WIP'])
		{
            $hdrModel = CycleCountHdrRepository::revertWipToDraft($hdrModel->id);
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		elseif($hdrModel->doc_status == DocStatus::$MAP['COMPLETE'])
		{
            AuthService::authorize(
                array(
                    'cycle_count_revert'
                )
            );

            $hdrModel = CycleCountHdrRepository::revertCompleteToDraft($hdrModel->id);
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
        }
        elseif($hdrModel->doc_status == DocStatus::$MAP['VOID'])
		{
            AuthService::authorize(
                array(
                    'cycle_count_confirm'
                )
            );

			$hdrModel = CycleCountHdrRepository::commitVoidToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
        
        $exc = new ApiException(__('CycleCount.doc_status_is_not_wip_or_complete', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\CycleCountHdr::class, $hdrModel->id);
		throw $exc;
    }
    
    public function uploadProcess($strProcType, $siteFlowId, $file)
    {
        $user = Auth::user();

        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['CYCLE_COUNT_EXCEL_01']) 
			{
                //process excel and create 1 cycle count
                $path = Storage::putFileAs('upload/', $file, 'CYCLE_COUNT_EXCEL_01.XLSX');
				return $this->uploadCycleCountExcel01($siteFlowId, $user->id, $path);
			}
		}
    }

    public function uploadCycleCountExcel01($siteFlowId, $userId, $path)
    {
        AuthService::authorize(
            array(
                'cycle_count_import'
            )
        );

        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['CYCLE_COUNT_EXCEL_01'], $userId);

        $siteFlow = SiteFlowRepository::findByPk($siteFlowId);

        $cycleCountExcel01Import = new CycleCountExcel01Import($siteFlow->site_id, $siteFlow->id, $userId, $batchJobStatusModel);
        $excel = Excel::import($cycleCountExcel01Import, $path);

        /*
        //need to process those storageBin which empty in excel,
        //when the storageBin is empty, there will be no detail in excel parsing
        //so here need to find out and set those quantBal to 0
        //using storageBay as base to check
        $notExistsStorageBinIdsHash = array();
        $storageBinIdHashByStorageBayId = $cycleCountExcel01Import->getStorageBinIdHashByStorageBayId();
        foreach($storageBinIdHashByStorageBayId as $storageBayId => $storageBinIdHash)
        {
            $storageBinModels = StorageBinRepository::findAllByStorageBayId($storageBayId);
            foreach($storageBinModels as $storageBinModel)
            {
                if(!array_key_exists($storageBinModel->id, $storageBinIdHash))
                {
                    $notExistsStorageBinIdsHash[$storageBinModel->id] = $storageBinModel->id;
                }
            }
        }
        */

        //need to process those storageBin which empty in excel,
        //when the storageBin is empty, there will be no detail in excel parsing
        //so here need to find out and set those quantBal to 0
        //using storageRow as base to check
        $notExistsStorageBinIdsHash = array();
        $storageBinIdHashByStorageRowId = $cycleCountExcel01Import->getStorageBinIdHashByStorageRowId();
        foreach($storageBinIdHashByStorageRowId as $storageRowId => $storageBinIdHash)
        {
            $storageBinModels = StorageBinRepository::findAllByStorageRowId($storageRowId);
            foreach($storageBinModels as $storageBinModel)
            {
                if(!array_key_exists($storageBinModel->id, $storageBinIdHash))
                {
                    $notExistsStorageBinIdsHash[$storageBinModel->id] = $storageBinModel->id;
                }
            }
        }

        $dtlDataListHashByCompanyId = $cycleCountExcel01Import->getDtlDataListHashByCompanyId();
        $docModels = array();
        foreach($dtlDataListHashByCompanyId as $companyId => $dtlDataList)
        {
            $lineNo = 1;

            for($a = 0; $a < count($dtlDataList); $a++)
            {
                $dtlData = $dtlDataList[$a];
                $dtlData['line_no'] = $lineNo;
                $dtlDataList[$a] = $dtlData;
                $lineNo++;
            }

            //query the not exists storage bin quantBals by companyId
            foreach($notExistsStorageBinIdsHash as $storageBinId)
            {
                $quantBals = QuantBalRepository::findAllActiveByStorageBinIdAndCompanyId($storageBinId, $companyId);
                $quantBals->load('item', 'itemBatch', 'storageType');

                foreach($quantBals as $quantBal)
                {                    
                    $item = $quantBal->item;
                    $itemBatch = $quantBal->itemBatch;
                    $storageType = $quantBal->storageType;

                    $whseJobType = WhseJobType::$MAP['CYCLE_COUNT_BIN_LOOSE'];
                    if($storageType->handling_type == HandlingType::$MAP['PALLET']
                    || $storageType->handling_type == HandlingType::$MAP['TOTE_BOX']
                    || $storageType->handling_type == HandlingType::$MAP['CASE_BOX'])
                    {
                        $whseJobType = WhseJobType::$MAP['CYCLE_COUNT_BIN_PALLET'];
                    }

                    $dtlData = array(
                        'line_no' => $lineNo,
                        'storage_bin_id' => $storageBinId,
                        'item_id' => $quantBal->item_id,
                        'item_batch_id' => $quantBal->item_batch_id,
                        'batch_serial_no' => $itemBatch->batch_serial_no,
                        'expiry_date' => $itemBatch->expiry_date,
                        'receipt_date' => $itemBatch->receipt_date,
                        'handling_unit_id' => $quantBal->handling_unit_id,
                        'company_id' => $companyId,
                        'desc_01' => $item->desc_01,
                        'desc_02' => $item->desc_02,
                        'uom_id' => $item->unit_uom_id,
                        'uom_rate' => 1,
                        'qty' => 0,
                        'whse_job_type' => $whseJobType,
                        'whse_job_code' => '',
                        'count_seq' => 1,
                        'to_cycle_count_hdr_id' => 0,
                        'to_cycle_count_dtl_id' => 0
                    );

                    $dtlDataList[] = $dtlData;
                    $lineNo++;
                }
            }
            
            //build the hdrData
            $hdrData = array(
                'ref_code_01' => '',
                'ref_code_02' => '',
                'doc_date' => date('Y-m-d'),
                'desc_01' => '',
                'desc_02' => '',
                'site_flow_id' => $siteFlowId,
                'group_count' => 1,
                'cur_job_no' => 0,
                'cur_count_seq' => 1,
                'user_id' => $userId,
            );

            $siteDocNo = SiteDocNoRepository::findSiteDocNo($siteFlow->site_id, \App\CycleCountHdr::class);
            if(empty($siteDocNo))
            {
                $exc = new ApiException(__('SiteDocNo.doc_no_not_found', ['siteId'=>$siteFlow->site_id, 'docType'=>\App\CycleCountHdr::class]));
                //$exc->addData(\App\CycleCountHdr::class, $this->siteId);
                throw $exc;
            }

            $whseTxnFlow = WhseTxnFlowRepository::findByPk($siteFlowId, ProcType::$MAP['CYCLE_COUNT_EXCEL_01']);

            //DRAFT
            $cycleCountHdr = CycleCountHdrRepository::createProcess(ProcType::$MAP['CYCLE_COUNT_EXCEL_01'], $siteDocNo->doc_no_id, $hdrData, $dtlDataList);

            if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
            {
                $cycleCountHdr = CycleCountService::transitionToWip($cycleCountHdr->id);
            }
            elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
            {
                $cycleCountHdr = CycleCountService::transitionToComplete($cycleCountHdr->id);
            }

            $docModels[] = $cycleCountHdr;
        }

        $docCodeMsg = '';
        for ($a = 0; $a < count($docModels); $a++)
        {
            $docModel = $docModels[$a];
            if($a == 0)
            {
                $docCodeMsg .= $docModel->doc_code;
            }
            else
            {
                $docCodeMsg .= ', '.$docModel->doc_code;
            }
        }
        
        if(count($docModels) > 0)
        {
            $message = __('CycleCount.document_successfully_created', ['docCode'=>$docCodeMsg]);
        }
        else
        {
            $message = __('CycleCount.nothing_is_created_from_the_uploaded_file', []);
        }

		return array(
			'data' => $docModels,
			'message' => $message
		);
    }

    static public function processOutgoingDetail($model)
  	{
        //relationships:
        $company = $model->company;
        $storageBin = $model->storageBin;
		$handlingUnit = $model->handlingUnit;
		$item = $model->item;
		$uom = $model->uom;
		
		$model->str_whse_job_type = WhseJobType::$MAP[$model->whse_job_type];
		$model->str_whse_job_type = __('WhseJob.'.strtolower($model->str_whse_job_type));

        $model->company_code = '';
		$model->storage_bin_code = '';
		$model->storage_row_code = '';
		$model->storage_bay_code = '';
		$model->handling_unit_barcode = '';

        $itemBatch = null;
        if(!empty($item))
        {
            $itemBatch = ItemBatchRepository::findByAttributes($item, $model->batch_serial_no, $model->expiry_date, $model->receipt_date);
        }
        $storageRow = null;
        $storageBay = null;
		if(!empty($storageBin))
		{
            $storageRow = $storageBin->storageRow;
            $storageBay = $storageBin->storageBay;
			$model->storage_bin_code = $storageBin->code;
		}
		if(!empty($handlingUnit))
		{
			$model->handling_unit_barcode = $handlingUnit->getBarcode();
		}
		if(!empty($storageRow))
		{
			$model->storage_row_code = $storageRow->code;
		}
		$model->item_bay_sequence = 0;
		if(!empty($storageBay))
		{
			$model->storage_bay_code = $storageBay->code;
			$model->item_bay_sequence = $storageBay->bay_sequence;
        }

		if(!empty($itemBatch))
		{
			$model->batch_serial_no = $itemBatch->batch_serial_no;
			$model->expiry_date = $itemBatch->expiry_date;
			$model->receipt_date = $itemBatch->receipt_date;
		}

		//calculate the pallet qty, case qty, gross weight, and m3
        $model = ItemService::processCaseLoose($model, $item);

        if(!empty($company))
        {
            $model->company_code = $company->code;
        }
        //storageBin select2
        $initCompanyOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($company))
        {
            $initCompanyOption = array('value'=>$company->id,'label'=>$company->code);
        } 
        $model->company_select2 = $initCompanyOption;

        $initStorageBinOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($storageBin))
        {
            $initStorageBinOption = array('value'=>$storageBin->id,'label'=>$storageBin->code);
        }        
		$model->storage_bin_select2 = $initStorageBinOption;
		
		$initHandlingUnitOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($handlingUnit))
        {
            $initHandlingUnitOption = array('value'=>$handlingUnit->id,'label'=>$handlingUnit->getBarcode());
        }        
		$model->handling_unit_select2 = $initHandlingUnitOption;

        $initItemOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($item))
        {
            $initItemOption = array('value'=>$item->id,'label'=>$item->code);
        } 
        $model->item_select2 = $initItemOption;

        $initItemBatchOption = array(
            'value'=>0,
            'label'=>'',
        );
        if(!empty($itemBatch))
        {
            $initItemBatchOption = array(
                'value'=>$itemBatch->id,
                'label'=>$itemBatch->expiry_date,
                'batch_serial_no'=>$itemBatch->batch_serial_no,
                'expiry_date'=>$itemBatch->expiry_date,
                'receipt_date'=>$itemBatch->receipt_date,
            );
        }
        $model->item_batch_select2 = $initItemBatchOption;

        $initUomOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($uom))
        {
            $initUomOption = array('value'=>$uom->id,'label'=>$uom->code);
        }        
		$model->uom_select2 = $initUomOption;
        
        unset($model->company);
        unset($model->storageBin);
		unset($model->handlingUnit);
		unset($model->item);
        unset($model->uom);

		return $model;
    }
    
    static public function processIncomingDetail($data)
    {
        //preprocess the select2 and dropDown
        if(array_key_exists('item_select2', $data))
        {
			$select2Id = $data['item_select2']['value'];
			if($select2Id > 0)
			{
				$data['item_id'] = $select2Id;
			}
            unset($data['item_select2']);
        }
        $item = ItemRepository::findByPk($data['item_id']);
        if(empty($item))
        {
            $exc = new ApiException(__('CycleCount.item_is_required', ['lineNo'=>$data['line_no']]));
            $exc->addData(\App\CycleCountDtl::class, $data['id']);
            throw $exc;
        }
        
		if(array_key_exists('qty', $data)
		&& !empty($item))
		{
			$data['qty'] = round($data['qty'], $item->qty_scale);
        }

        if(array_key_exists('company_select2', $data))
        {
			$select2Id = $data['company_select2']['value'];
			if($select2Id > 0)
			{
				$data['company_id'] = $select2Id;
			}
            unset($data['company_select2']);
        }        
        if(array_key_exists('item_batch_select2', $data))
        {
            //just unset itemBatch select2, because it will fill correct value in other fields, 
            //expiry_date, receipt_date, when the itemBatch is selected
            unset($data['item_batch_select2']);
        }
        if(array_key_exists('uom_select2', $data))
        {
			$select2Id = $data['uom_select2']['value'];
			if($select2Id > 0)
			{
				$data['uom_id'] = $select2Id;
			}
            unset($data['uom_select2']);
        }
        
        if(array_key_exists('storage_bin_select2', $data))
        {
			$select2Id = $data['storage_bin_select2']['value'];
			if($select2Id > 0)
			{
				$data['storage_bin_id'] = $select2Id;
			}
            unset($data['storage_bin_select2']);
		}
		if(array_key_exists('handling_unit_select2', $data))
        {
			$select2Id = $data['handling_unit_select2']['value'];
			if($select2Id > 0)
			{
				$data['handling_unit_id'] = $select2Id;
			}
            unset($data['handling_unit_select2']);
        }

        if(array_key_exists('company_code', $data))
        {
            unset($data['company_code']);
        }
        if(array_key_exists('storage_bin_code', $data))
        {
            unset($data['storage_bin_code']);
		}
		if(array_key_exists('storage_row_code', $data))
        {
            unset($data['storage_row_code']);
		}
		if(array_key_exists('storage_bay_code', $data))
        {
            unset($data['storage_bay_code']);
		}
		if(array_key_exists('item_bay_sequence', $data))
        {
            unset($data['item_bay_sequence']);
		}
		if(array_key_exists('item_bay_sequence', $data))
        {
            unset($data['item_bay_sequence']);
		}
		if(array_key_exists('to_storage_bin_code', $data))
        {
            unset($data['to_storage_bin_code']);
		}

		if(array_key_exists('str_whse_job_type', $data))
        {
            unset($data['str_whse_job_type']);
		}

        if(array_key_exists('handling_unit_barcode', $data))
        {
            unset($data['handling_unit_barcode']);
		}
		if(array_key_exists('handling_unit_ref_code_01', $data))
        {
            unset($data['handling_unit_ref_code_01']);
        }
        
        if(array_key_exists('item_code', $data))
        {
            unset($data['item_code']);
		}
		if(array_key_exists('item_ref_code_01', $data))
        {
            unset($data['item_ref_code_01']);
		}
		if(array_key_exists('item_desc_01', $data))
        {
            unset($data['item_desc_01']);
		}
		if(array_key_exists('item_desc_02', $data))
        {
            unset($data['item_desc_02']);
		}
		if(array_key_exists('storage_class', $data))
        {
            unset($data['storage_class']);
		}
		if(array_key_exists('item_group_01_code', $data))
        {
            unset($data['item_group_01_code']);
		}
		if(array_key_exists('item_group_02_code', $data))
        {
            unset($data['item_group_02_code']);
		}
		if(array_key_exists('item_group_03_code', $data))
        {
            unset($data['item_group_03_code']);
		}
		if(array_key_exists('item_group_04_code', $data))
        {
            unset($data['item_group_04_code']);
		}
		if(array_key_exists('item_group_05_code', $data))
        {
            unset($data['item_group_05_code']);
		}
		if(array_key_exists('item_cases_per_pallet_length', $data))
        {
            unset($data['item_cases_per_pallet_length']);
		}
		if(array_key_exists('item_cases_per_pallet_width', $data))
        {
            unset($data['item_cases_per_pallet_width']);
		}
		if(array_key_exists('item_no_of_layers', $data))
        {
            unset($data['item_no_of_layers']);
		}

		if(array_key_exists('unit_qty', $data))
        {
            unset($data['unit_qty']);
		}
		if(array_key_exists('item_unit_uom_code', $data))
        {
            unset($data['item_unit_uom_code']);
		}
		if(array_key_exists('loose_qty', $data))
        {
            unset($data['loose_qty']);
		}
		if(array_key_exists('item_loose_uom_code', $data))
        {
            unset($data['item_loose_uom_code']);
        }
        if(array_key_exists('loose_uom_id', $data))
        {
            unset($data['loose_uom_id']);
		}
		if(array_key_exists('loose_uom_rate', $data))
        {
            unset($data['loose_uom_rate']);
		}
		if(array_key_exists('case_qty', $data))
        {
            unset($data['case_qty']);
		}
		if(array_key_exists('case_uom_rate', $data))
        {
            unset($data['case_uom_rate']);
		}
        if(array_key_exists('item_case_uom_code', $data))
        {
            unset($data['item_case_uom_code']);
        }
        if(array_key_exists('uom_code', $data))
        {
            unset($data['uom_code']);
		}
		if(array_key_exists('item_unit_barcode', $data))
        {
            unset($data['item_unit_barcode']);
		}
		if(array_key_exists('item_case_barcode', $data))
        {
            unset($data['item_case_barcode']);
		}
		if(array_key_exists('gross_weight', $data))
        {
            unset($data['gross_weight']);
		}
		if(array_key_exists('cubic_meter', $data))
        {
            unset($data['cubic_meter']);
        }
        return $data;
    }

    static public function transitionToVoid($hdrId)
    {
        AuthService::authorize(
            array(
                'cycle_count_revert'
            )
        );

		//use transaction to make sure this is latest value
		$hdrModel = CycleCountHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('CycleCount.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\CycleCountHdr::class, $hdrModel->id);
            throw $exc;
		}

		//revert the document
		$hdrModel = CycleCountHdrRepository::revertDraftToVoid($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('CycleCount.commit_to_void_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\CycleCountHdr::class, $hdrModel->id);
        throw $exc;
    }

    public function showHeader($hdrId)
    {
        AuthService::authorize(
            array(
                'cycle_count_read',
                'cycle_count_update'
            )
        );

        $model = CycleCountHdrRepository::findByPk($hdrId);
        if(!empty($model))
        {
            $model = self::processOutgoingHeader($model);
        }

        return $model;
    }

    public function showDetails($hdrId) 
	{
        AuthService::authorize(
            array(
                'cycle_count_read',
                'cycle_count_update'
            )
        );

		$cycleCountDtls = CycleCountDtlRepository::findAllByHdrId($hdrId);
        $cycleCountDtls->load(
            'item', 
            'item.itemUoms', 
            'item.itemUoms.uom'
        );
        foreach($cycleCountDtls as $cycleCountDtl)
        {
			$cycleCountDtl = self::processOutgoingDetail($cycleCountDtl);
        }
        return $cycleCountDtls;
    }
    
    static public function processOutgoingHeader($model, $isShowPrint = false)
	{
        $docFlows = self::processDocFlows($model);
        $model->doc_flows = $docFlows;
        
		if($isShowPrint)
		{
			$printDocTxn = PrintDocTxnRepository::queryPrintDocTxn(CycleCountHdr::class, $model->id);
			$model->print_count = $printDocTxn->print_count;
			$model->first_printed_at = $printDocTxn->first_printed_at;
			$model->last_printed_at = $printDocTxn->last_printed_at;
		}

        $model->str_doc_status = DocStatus::$MAP[$model->doc_status];
        
        $initGroupCountOption = array(
            'value'=>$model->group_count,
            'label'=>$model->group_count
        );
        $model->group_count_select2 = $initGroupCountOption;

		return $model;
    }
    
    public function updateDetails($hdrId, $dtlArray)
	{
        AuthService::authorize(
            array(
                'cycle_count_update'
            )
        );

		$cycleCountHdr = CycleCountHdrRepository::txnFindByPk($hdrId);
        if($cycleCountHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('CycleCount.doc_status_is_wip_or_complete', ['docCode'=>$whseJobHdr->doc_code]));
            $exc->addData(\App\CycleCountHdr::class, $cycleCountHdr->id);
            throw $exc;
        }
		$cycleCountHdrData = $cycleCountHdr->toArray();

		//query the cycleCountDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$cycleCountDtlArray = array();
        $cycleCountDtlModels = CycleCountDtlRepository::findAllByHdrId($hdrId);
		foreach($cycleCountDtlModels as $cycleCountDtlModel)
		{
			$cycleCountDtlData = $cycleCountDtlModel->toArray();
			$cycleCountDtlData['is_modified'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{                
				if($dtlData['id'] == $cycleCountDtlData['id'])
				{
					$dtlData = CycleCountService::processIncomingDetail($dtlData);
					
					if($dtlData['uom_id'] == Uom::$PALLET
                    && bccomp($dtlData['uom_rate'], 0, 5) == 0)
                    {
					}
					else
					{
						$dtlData = $this->updateWarehouseItemUom($dtlData, 0, 0);
					}

					foreach($dtlData as $fieldName => $value)
					{
						$cycleCountDtlData[$fieldName] = $value;
					}

					$cycleCountDtlData['is_modified'] = 1;

					break;
				}
			}
			$cycleCountDtlArray[] = $cycleCountDtlData;
		}
		
		$result = CycleCountHdrRepository::updateDetails($cycleCountHdrData, $cycleCountDtlArray, array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = self::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('CycleCount.details_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }
    
    public function changeItem($hdrId, $itemId)
    {
        AuthService::authorize(
            array(
                'cycle_count_update'
            )
        );

        $cycleCountHdr = CycleCountHdrRepository::findByPk($hdrId);

        $results = array();
        $item = ItemRepository::findByPk($itemId);
        if(!empty($item)
        && !empty($cycleCountHdr))
        {
            $results['desc_01'] = $item->desc_01;
            $results['desc_02'] = $item->desc_02;
        }
        return $results;
    }
    
    public function changeItemUom($hdrId, $itemId, $uomId)
    {
        AuthService::authorize(
            array(
                'cycle_count_update'
            )
        );

        $cycleCountHdr = CycleCountHdrRepository::findByPk($hdrId);

        $results = array();
        $itemUom = ItemUomRepository::findByItemIdAndUomId($itemId, $uomId);
        if(!empty($itemUom)
        && !empty($cycleCountHdr))
        {
            $results['item_id'] = $itemUom->item_id;
            $results['uom_id'] = $itemUom->uom_id;
            $results['uom_rate'] = $itemUom->uom_rate;

			$results = $this->updateWarehouseItemUom($results, 0, 0);
        }
        return $results;
    }
    
    public function changeQuantBal($hdrId, $quantBalId)
    {
        AuthService::authorize(
            array(
                'cycle_count_update'
            )
        );
        
		$cycleCountHdr = CycleCountHdrRepository::findByPk($hdrId);

		$results = array();
		$quantBal = QuantBalRepository::findByPk($quantBalId);
        if(!empty($quantBal))
        {
			$item = $quantBal->item;
			$itemBatch = $quantBal->itemBatch;
			$handlingUnit = $quantBal->handlingUnit;
			
			$quantBal = ItemService::processCaseLoose($quantBal, $item, 1);

			$results['item_id'] = $quantBal->item_id;
			$results['item_code'] = $quantBal->item_code;
			$results['item_desc_01'] = $quantBal->item_desc_01;
			$results['item_desc_02'] = $quantBal->item_desc_02;
			$results['desc_01'] = $quantBal->item_desc_01;
			$results['desc_02'] = $quantBal->item_desc_02;
			$results['case_qty'] = $quantBal->case_qty;
			$results['item_case_uom_code'] = $quantBal->item_case_uom_code;
            $results['batch_serial_no'] = empty($itemBatch) ? '' : $itemBatch->batch_serial_no;
            $results['expiry_date'] = empty($itemBatch) ? '' : $itemBatch->expiry_date;
            $results['receipt_date'] = empty($itemBatch) ? '' : $itemBatch->receipt_date;
            $results['handling_unit_barcode'] = empty($handlingUnit) ? '' : $handlingUnit->getBarcode();
			
		}

        return $results;
    }
    
    public function changeItemBatch($hdrId, $itemBatchId)
    {
        AuthService::authorize(
            array(
                'cycle_count_update'
            )
        );

        $cycleCountHdr = CycleCountHdrRepository::findByPk($hdrId);

        $results = array();
        $itemBatch = ItemBatchRepository::findByPk($itemBatchId);
        if(!empty($itemBatch)
        && !empty($cycleCountHdr))
        {
            $results['batch_serial_no'] = $itemBatch->batch_serial_no;
            $results['expiry_date'] = $itemBatch->expiry_date;
            $results['receipt_date'] = $itemBatch->receipt_date;
        }
        return $results;
    }
    
    public function updateHeader($hdrData)
	{
        AuthService::authorize(
            array(
                'cycle_count_update'
            )
        );

        $hdrData = self::processIncomingHeader($hdrData);

        $cycleCountHdr = CycleCountHdrRepository::txnFindByPk($hdrData['id']);
        if($cycleCountHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('CycleCount.doc_status_is_wip_or_complete', ['docCode'=>$cycleCountHdr->doc_code]));
            $exc->addData(\App\CycleCountHdr::class, $cycleCountHdr->id);
            throw $exc;
        }
        $cycleCountHdrData = $cycleCountHdr->toArray();
        //assign hdrData to model
        foreach($hdrData as $field => $value) 
        {
            $cycleCountHdrData[$field] = $value;
        }

		//$cycleCountHdr->load('worker01');
        //$worker01 = $cycleCountHdr->worker01;
		
		//no detail to update
        $cycleCountDtlArray = array();

        $result = CycleCountHdrRepository::updateDetails($cycleCountHdrData, $cycleCountDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];

        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = self::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('CycleCount.document_successfully_updated', ['docCode'=>$documentHeader->doc_code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }
    
    static public function processIncomingHeader($data, $isClearHdrDiscTax=false)
    {
        if(array_key_exists('doc_flows', $data))
        {
            unset($data['doc_flows']);
        }
		if(array_key_exists('submit_action', $data))
        {
            unset($data['submit_action']);
        }
        //preprocess the select2 and dropDown
        
        if(array_key_exists('str_doc_status', $data))
        {
            unset($data['str_doc_status']);
        }
        
        if(array_key_exists('group_count_select2', $data))
        {
			$select2Id = $data['group_count_select2']['value'];
			if($select2Id > 0)
			{
				$data['group_count'] = $select2Id;
			}
            unset($data['group_count_select2']);
        }
        
        return $data;
    }
    
    public function createDetail($hdrId, $data)
	{
        AuthService::authorize(
            array(
                'cycle_count_update'
            )
        );

        $data = self::processIncomingDetail($data);
		$data = $this->updateWarehouseItemUom($data, 0, 0);

        $cycleCountHdr = CycleCountHdrRepository::txnFindByPk($hdrId);
        if($cycleCountHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('CycleCount.doc_status_is_wip_or_complete', ['docCode'=>$cycleCountHdr->doc_code]));
            $exc->addData(\App\CycleCountHdr::class, $cycleCountHdr->id);
            throw $exc;
        }
		$cycleCountHdrData = $cycleCountHdr->toArray();

		$cycleCountDtlArray = array();
		$cycleCountDtlModels = CycleCountDtlRepository::findAllByHdrId($hdrId);
		foreach($cycleCountDtlModels as $cycleCountDtlModel)
		{
			$cycleCountDtlData = $cycleCountDtlModel->toArray();
			$cycleCountDtlData['is_modified'] = 0;
			$cycleCountDtlArray[] = $cycleCountDtlData;
		}
		$lastLineNo = count($cycleCountDtlModels);

		//assign temp id
		$tmpUuid = uniqid();
		//append NEW here to make sure it will be insert in repository later
		$data['id'] = 'NEW'.$tmpUuid;
		$data['line_no'] = $lastLineNo + 1;
		$data['is_modified'] = 1;
		
		$cycleCountDtlData = $this->initCycleCountDtlData($data);

		$cycleCountDtlArray[] = $cycleCountDtlData;

		$result = CycleCountHdrRepository::updateDetails($cycleCountHdrData, $cycleCountDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = CycleCountService::processOutgoingDetail($dtlModel);
        }

        $message = __('CycleCount.detail_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }
    
    public function deleteDetails($hdrId, $dtlArray)
	{
        AuthService::authorize(
            array(
                'cycle_count_update'
            )
        );

        $cycleCountHdr = CycleCountHdrRepository::txnFindByPk($hdrId);
        if($cycleCountHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('CycleCount.doc_status_is_wip_or_complete', ['docCode'=>$cycleCountHdr->doc_code]));
            $exc->addData(\App\CycleCountHdr::class, $cycleCountHdr->id);
            throw $exc;
        }
		$cycleCountHdrData = $cycleCountHdr->toArray();

		//query the cycleCountDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$delCycleCountDtlArray = array();
		$cycleCountDtlArray = array();
        $cycleCountDtlModels = CycleCountDtlRepository::findAllByHdrId($hdrId);
        $lineNo = 1;
		foreach($cycleCountDtlModels as $cycleCountDtlModel)
		{
			$cycleCountDtlData = $cycleCountDtlModel->toArray();
			$cycleCountDtlData['is_modified'] = 0;
			$cycleCountDtlData['is_deleted'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{
				if($dtlData['id'] == $cycleCountDtlData['id'])
				{
					//this is deleted dtl
					$cycleCountDtlData['is_deleted'] = 1;
					break;
				}
			}
			if($cycleCountDtlData['is_deleted'] > 0)
			{
				$delCycleCountDtlArray[] = $cycleCountDtlData;
			}
			else
			{
                $cycleCountDtlData['line_no'] = $lineNo;
                $cycleCountDtlData['is_modified'] = 1;
                $cycleCountDtlArray[] = $cycleCountDtlData;
                $lineNo++;
			}
        }
		
		$result = CycleCountHdrRepository::updateDetails($cycleCountHdrData, $cycleCountDtlArray, $delCycleCountDtlArray);
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = self::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('CycleCount.details_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>$delCycleCountDtlArray
            ),
			'message' => $message
		);
    }
    
    public function createHeader($data)
    {		
        AuthService::authorize(
            array(
                'cycle_count_create'
            )
        );

        $data = self::processIncomingHeader($data, true);
        $docNoId = $data['doc_no_id'];
        unset($data['doc_no_id']);
        $procType = ProcType::$MAP['CYCLE_COUNT_03'];
        if(array_key_exists('proc_type', $data))
        {
            $procType = $data['proc_type'];
        }
        
        $hdrModel = CycleCountHdrRepository::createHeader($procType, $docNoId, $data);

        $message = __('CycleCount.document_successfully_created', ['docCode'=>$hdrModel->doc_code]);

		return array(
			'data' => $hdrModel->id,
			'message' => $message
		);
    }
    
    public function initHeader($siteFlowId)
    {
        AuthService::authorize(
            array(
                'cycle_count_create'
            )
        );

        $user = Auth::user();

        $siteFlow = SiteFlowRepository::findByPk($siteFlowId);
        $model = new CycleCountHdr();
        $model->doc_flows = array();
        $model->proc_type = ProcType::$MAP['CYCLE_COUNT_03'];
        $model->doc_status = DocStatus::$MAP['DRAFT'];
        $model->str_doc_status = DocStatus::$MAP[$model->doc_status];
        $model->doc_code = '';
        $model->ref_code_01 = '';
        $model->ref_code_02 = '';
        $model->doc_date = date('Y-m-d');
        $model->desc_01 = '';
        $model->desc_02 = '';

        //group count select2
        $initGroupCountOption = array(
            'value'=>1,
            'label'=>1
        );
        $model->group_count_select2 = $initGroupCountOption;

        $model->group_count = 1;
        $model->cur_count_seq = 1;
        $model->site_flow_id = $siteFlowId;

        $model->doc_no_id = 0;
        $docNoIdOptions = array();
        $docNos = DocNoRepository::findAllSiteDocNo($siteFlow->site_id, \App\CycleCountHdr::class, $model->doc_date);
        for($a = 0; $a < count($docNos); $a++)
        {
            $docNo = $docNos[$a];
            if($a == 0)
            {
                $model->doc_no_id = $docNo->id;
            }
            $docNoIdOptions[] = array('value'=>$docNo->id, 'label'=>$docNo->latest_code);
        }
        $model->doc_no_id_options = $docNoIdOptions;

        //company select2
        $initCompanyOption = array(
            'value'=>0,
            'label'=>''
        );
        $model->company_select2 = $initCompanyOption;

        return $model;
    }
    
    public function createJobDetail($hdrId, $data)
    {       
        AuthService::authorize(
            array(
                'cycle_count_update'
            )
        );

        $user = Auth::user();

        $cycleCountHdr = CycleCountHdrRepository::txnFindByPk($hdrId);
        if($cycleCountHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('CycleCount.doc_status_is_wip_or_complete', ['docCode'=>$cycleCountHdr->doc_code]));
            $exc->addData(\App\CycleCountHdr::class, $cycleCountHdr->id);
            throw $exc;
        }
        $siteFlow = $cycleCountHdr->siteFlow;
        unset($cycleCountHdr->siteFlow);
        $cycleCountHdrData = $cycleCountHdr->toArray();

        $defaultCompanyId = 0;
        //get the default company id
        $userDivisions = $user->userDivisions;
        $userDivisions->load('division');
        foreach($userDivisions as $userDivision)
        {
            $division = $userDivision->division;
            $defaultCompanyId = $division->company_id;
            break;
        }
        
        //add the cur_job_no at header
        $cycleCountHdrData['cur_job_no'] = bcadd($cycleCountHdrData['cur_job_no'], 1);

		$cycleCountDtlArray = array();
		$cycleCountDtlModels = CycleCountDtlRepository::findAllByHdrId($hdrId);
		foreach($cycleCountDtlModels as $cycleCountDtlModel)
		{
			$cycleCountDtlData = $cycleCountDtlModel->toArray();
			$cycleCountDtlData['is_modified'] = 0;
			$cycleCountDtlArray[] = $cycleCountDtlData;
		}
        $lastLineNo = count($cycleCountDtlModels);

        $storageHash = array();
        $sorts = array(
            array('field'=>'code','order'=>'ASC'),
        );
        $storageTypes = StorageTypeRepository::findAll($sorts, array(), PHP_INT_MAX);
        //format storageTypes to storageTypesHash
        foreach($storageTypes as $storageType)
        {
            $storageHash[$storageType->id] = $storageType;
        }

        $job_desc_01 = '';
        if(array_key_exists('job_desc_01', $data))
        {
            $job_desc_01 = $data['job_desc_01'];
        }
        $storageRowIds = array();
        if(array_key_exists('storage_row_ids', $data))
        {
            $storageRowIds = $data['storage_row_ids'];
        }
        $storageBayIds = array();
        if(array_key_exists('storage_bay_ids', $data))
        {
            $storageBayIds = $data['storage_bay_ids'];
        }
        $levels = array();
        if(array_key_exists('levels', $data))
        {
            $levels = $data['levels'];
        }

        //DB::connection()->enableQueryLog();
        $storageBins = StorageBinRepository::findAllByAttributes($siteFlow->site_id, $storageRowIds, $storageBayIds, $levels);
        //dd(DB::getQueryLog());
        foreach($storageBins as $storageBin)
        {
            if($storageBin->bin_status == ResStatus::$MAP['INACTIVE'])
            {
                continue;
            }
            $storageType = $storageHash[$storageBin->storage_type_id];
            $whseJobType = WhseJobType::$MAP['CYCLE_COUNT_BIN_LOOSE'];
            if($storageType->handling_type == HandlingType::$MAP['PALLET']
            || $storageType->handling_type == HandlingType::$MAP['TOTE_BOX']
            || $storageType->handling_type == HandlingType::$MAP['CASE_BOX'])
            {
                $whseJobType = WhseJobType::$MAP['CYCLE_COUNT_BIN_PALLET'];
            }

            $quantBals = QuantBalRepository::findAllActiveByStorageBinId($storageBin->id);
            $quantBals->load('item', 'itemBatch');

            for($a = 1; $a <= $cycleCountHdr->group_count; $a++)
            {
                //for each job group
                foreach($quantBals as $quantBal)
                {
                    $item = $quantBal->item;
                    $itemBatch = $quantBal->itemBatch;

                    //assign temp id
                    $tmpUuid = uniqid();
                    //append NEW here to make sure it will be insert in repository later
                    $dtlData = array();
                    $dtlData['id'] = 'NEW'.$tmpUuid;
                    $dtlData['group_no'] = $a;
                    $dtlData['job_no'] = $cycleCountHdrData['cur_job_no'];
                    $dtlData['physical_count_status'] = PhysicalCountStatus::$MAP['COUNTING'];
                    $dtlData['job_desc_01'] = $job_desc_01;
                    $dtlData['job_desc_02'] = '';
                    $dtlData['line_no'] = ++$lastLineNo;
                    $dtlData['is_modified'] = 1;
                    $dtlData['count_seq'] = 1;
                    $dtlData['storage_bin_id'] = $quantBal->storage_bin_id;
                    $dtlData['company_id'] = $quantBal->company_id;
                    $dtlData['item_id'] = $quantBal->item_id;
                    $dtlData['item_batch_id'] = $quantBal->item_batch_id;
                    $dtlData['batch_serial_no'] = $itemBatch->batch_serial_no;
                    $dtlData['expiry_date'] = $itemBatch->expiry_date;
                    $dtlData['receipt_date'] = $itemBatch->receipt_date;
                    $dtlData['handling_unit_id'] = $quantBal->handling_unit_id;
                    $dtlData['desc_01'] = $item->desc_01;
                    $dtlData['desc_02'] =  $item->desc_02;
                    $dtlData['uom_id'] = $item->unit_uom_id;
                    $dtlData['uom_rate'] = 1;
                    //$dtlData['qty'] = $quantBal->balance_unit_qty;
                    $dtlData['qty'] = 0;
                    $dtlData['whse_job_type'] = $whseJobType;
                    $dtlData['whse_job_code'] = '';
                    $dtlData['to_cycle_count_hdr_id'] = 0;
                    $dtlData['to_cycle_count_dtl_id'] = 0;
                    
                    $cycleCountDtlData = $this->initCycleCountDtlData($dtlData);

                    $cycleCountDtlArray[] = $cycleCountDtlData;
                }

                if(count($quantBals) == 0)
                {
                    //assign temp id
                    $tmpUuid = uniqid();
                    //append NEW here to make sure it will be insert in repository later
                    $dtlData = array();
                    $dtlData['id'] = 'NEW'.$tmpUuid;
                    $dtlData['group_no'] = $a;
                    $dtlData['job_no'] = $cycleCountHdrData['cur_job_no'];
                    $dtlData['physical_count_status'] = PhysicalCountStatus::$MAP['COUNTING'];
                    $dtlData['job_desc_01'] = $job_desc_01;
                    $dtlData['job_desc_02'] = '';
                    $dtlData['line_no'] = ++$lastLineNo;
                    $dtlData['is_modified'] = 1;
                    $dtlData['storage_bin_id'] = $storageBin->id;
                    $dtlData['company_id'] = $defaultCompanyId;
                    $dtlData['item_id'] = 0;
                    $dtlData['item_batch_id'] = 0;
                    $dtlData['batch_serial_no'] = '';
                    $dtlData['expiry_date'] = '';
                    $dtlData['receipt_date'] = '';
                    $dtlData['handling_unit_id'] = 0;
                    $dtlData['desc_01'] = '';
                    $dtlData['desc_02'] = '';
                    $dtlData['uom_id'] = 0;
                    $dtlData['uom_rate'] = 0;
                    $dtlData['qty'] = 0;
                    $dtlData['whse_job_type'] = $whseJobType;
                    $dtlData['whse_job_code'] = '';
                    $dtlData['count_seq'] = 1;
                    $dtlData['to_cycle_count_hdr_id'] = 0;
                    $dtlData['to_cycle_count_dtl_id'] = 0;

                    $cycleCountDtlData = $this->initCycleCountDtlData($dtlData);

                    $cycleCountDtlArray[] = $cycleCountDtlData;
                }
            }
        }
        
        $result = CycleCountHdrRepository::updateDetails($cycleCountHdrData, $cycleCountDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = CycleCountService::processOutgoingDetail($dtlModel);
        }

        $message = __('CycleCount.detail_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }

    public function deleteJobDetails($hdrId, $jobNoArray)
	{
        AuthService::authorize(
            array(
                'cycle_count_update'
            )
        );

        $cycleCountHdr = CycleCountHdrRepository::txnFindByPk($hdrId);
        if($cycleCountHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('CycleCount.doc_status_is_wip_or_complete', ['docCode'=>$cycleCountHdr->doc_code]));
            $exc->addData(\App\CycleCountHdr::class, $cycleCountHdr->id);
            throw $exc;
        }
		$cycleCountHdrData = $cycleCountHdr->toArray();

		//query the cycleCountDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$delCycleCountDtlArray = array();
		$cycleCountDtlArray = array();
        $cycleCountDtlModels = CycleCountDtlRepository::findAllByHdrId($hdrId);
        $lineNo = 1;
		foreach($cycleCountDtlModels as $cycleCountDtlModel)
		{
			$cycleCountDtlData = $cycleCountDtlModel->toArray();
			$cycleCountDtlData['is_modified'] = 0;
			$cycleCountDtlData['is_deleted'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($jobNoArray as $jobNoData)
			{
				if($jobNoData['job_no'] == $cycleCountDtlData['job_no'])
				{
					//this is deleted dtl
					$cycleCountDtlData['is_deleted'] = 1;
					break;
				}
			}
			if($cycleCountDtlData['is_deleted'] > 0)
			{
				$delCycleCountDtlArray[] = $cycleCountDtlData;
			}
			else
			{
                $cycleCountDtlData['line_no'] = $lineNo;
                $cycleCountDtlData['is_modified'] = 1;
                $cycleCountDtlArray[] = $cycleCountDtlData;
                $lineNo++;
			}
        }
		
		$result = CycleCountHdrRepository::updateDetails($cycleCountHdrData, $cycleCountDtlArray, $delCycleCountDtlArray);
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = self::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('CycleCount.details_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>$delCycleCountDtlArray
            ),
			'message' => $message
		);
    }

    public function showRecountDetails($hdrId) 
	{
        AuthService::authorize(
            array(
                'cycle_count_read'
            )
        );

        $cycleCountHdr = CycleCountHdrRepository::txnFindByPk($hdrId);

        $storageBinsHash = array();
        //key by $handlingUnitId, $storageBin, $companyId, $itemId, $itemBatchId
        $cycleCountDtlsHashByKey = array();
        $quantBalHashByKey = array();
        $processedCycleCountDtls = array();

        //load all details of this cycleCount
		$cycleCountDtls = CycleCountDtlRepository::findAllByHdrId($hdrId);
        $cycleCountDtls->load(
            'storageBin',
            'item'
        );
        //find out the storageBins of this details
        foreach($cycleCountDtls as $cycleCountDtl)
        {
            $storageBin = $cycleCountDtl->storageBin;
            $storageBinsHash[$storageBin->id] = $storageBin;
            
            //key by $handlingUnitId, $storageBin, $companyId, $itemId, $itemBatchId
            $key = $cycleCountDtl->handling_unit_id.'/'.$cycleCountDtl->storage_bin_id.'/'.
                $cycleCountDtl->company_id.'/'.
                $cycleCountDtl->item_id.'/'.$cycleCountDtl->item_batch_id;
            
            $tmpCycleCountDtls = array();
            if(array_key_exists($key, $cycleCountDtlsHashByKey))
            {
                $tmpCycleCountDtls = $cycleCountDtlsHashByKey[$key];
            }
            $tmpCycleCountDtls[] = $cycleCountDtl;
            $cycleCountDtlsHashByKey[$key] = $tmpCycleCountDtls;
        }

        foreach($storageBinsHash as $storageBinId => $storageBin)
        {
            //query out all quantBals of this storageBins
            //DB::connection()->enableQueryLog();
            $dbQuantBals = QuantBalRepository::findAllByStorageBinId($storageBinId, $cycleCountHdr->doc_date->format('Y-m-d'));
            //Log::error(DB::getQueryLog());
            $dbQuantBals->load('itemBatch');

            foreach($dbQuantBals as $dbQuantBal)
            {
                //key by $handlingUnitId, $storageBin, $companyId, $itemId, $itemBatchId
                $key = $dbQuantBal->handling_unit_id.'/'.$dbQuantBal->storage_bin_id.'/'.
                    $dbQuantBal->company_id.'/'.
                    $dbQuantBal->item_id.'/'.$dbQuantBal->item_batch_id;

                $quantBalHashByKey[$key] = $dbQuantBal;
            }
        }

        //first loop quantBalHashByKey
        //every loop will remove the cycleCountDtlsHashByKey, if the key is existed
        foreach($quantBalHashByKey as $key => $quantBal)
        {
            $item = null;
            $jobNo = 0;

            $quantBal->is_updated = true;
            $quantBal->physical_count_status = 0;
            //process by group_no
            //calculate the max_variance_perc stat
            $quantBal->ttl_count_unit_qty = 0;
            $quantBal->max_variance_qty = 0;
            $quantBal->max_abs_variance_qty = 0;
            $quantBal->max_variance_perc = 0;

            $storageBin = $quantBal->storageBin;
            $quantBal->storage_bin_code = $storageBin->code;

            $itemBatch = $quantBal->itemBatch;
            if(!empty($itemBatch))
            {
                $quantBal->batch_serial_no = $itemBatch->batch_serial_no;
                $quantBal->expiry_date = $itemBatch->expiry_date;
                $quantBal->receipt_date = $itemBatch->receipt_date;
            }

            $qCycleCountDtls = array();
            if(array_key_exists($key, $cycleCountDtlsHashByKey))
            {
                $qCycleCountDtls = $cycleCountDtlsHashByKey[$key];
                unset($cycleCountDtlsHashByKey[$key]);
            }

            //group by group_no
            $qCycleCountDtlsHashByGroupNo = array();
            foreach($qCycleCountDtls as $qCycleCountDtl)
            {                
                $tmpCycleCountDtls = array();
                if(array_key_exists($qCycleCountDtl->group_no, $qCycleCountDtlsHashByGroupNo))
                {
                    $tmpCycleCountDtls = $qCycleCountDtlsHashByGroupNo[$qCycleCountDtl->group_no];
                }
                $tmpCycleCountDtls[] = $qCycleCountDtl;
                $qCycleCountDtlsHashByGroupNo[$qCycleCountDtl->group_no] = $tmpCycleCountDtls;
            }

            //calculate the variance_qty, variance_perc stat
            foreach($qCycleCountDtlsHashByGroupNo as $groupNo => $gCycleCountDtls)
            {
                $ttlCountUnitQty = 0;
                foreach($gCycleCountDtls as $gCycleCountDtl)
                {
                    if($gCycleCountDtl->physical_count_status > $quantBal->physical_count_status)
                    {
                        $quantBal->physical_count_status = $gCycleCountDtl->physical_count_status;
                    }
                    
                    $item = $gCycleCountDtl->item;
                    $jobNo = $gCycleCountDtl->job_no;
                    if(!empty($item))
                    {
                        $countUnitQty = bcmul($gCycleCountDtl->uom_rate, $gCycleCountDtl->qty, $item->qty_scale);
                        $ttlCountUnitQty = bcadd($ttlCountUnitQty, $countUnitQty, $item->qty_scale);
                    }
                }
                
                $varianceQty = bcsub($ttlCountUnitQty, $quantBal->balance_unit_qty, 5);
                $absVarianceQty = abs($varianceQty);
                $variancePerc = 0;
                if($quantBal->balance_unit_qty > 0)
                {
                    $variancePerc = bcmul(bcdiv($absVarianceQty, $quantBal->balance_unit_qty, 10), 100, 10);
                }
                else
                {
                    if($absVarianceQty > 0)
                    {
                        $variancePerc = 100;
                    }
                    else
                    {
                        $variancePerc = 0;
                    }
                }
                if(bccomp($variancePerc, $quantBal->max_variance_perc, 5) >= 0)
                {
                    $quantBal->ttl_count_unit_qty = $ttlCountUnitQty;
                    $quantBal->max_variance_qty = $varianceQty;
                    $quantBal->max_abs_variance_qty = $absVarianceQty;
                    $quantBal->max_variance_perc = $variancePerc;
                }

                //update the variance_qty, variance_perc to every cycleCountDtl
                foreach($gCycleCountDtls as $gCycleCountDtl)
                {
                    $gCycleCountDtl = ItemService::processCaseLoose($gCycleCountDtl, $item, 0, false);
                    $countUnitQty = bcmul($gCycleCountDtl->uom_rate, $gCycleCountDtl->qty, $item->qty_scale);
                    $gCycleCountDtl->ttl_count_unit_qty = $countUnitQty;
                    $gCycleCountDtl->variance_qty = $varianceQty;
                    $gCycleCountDtl->abs_variance_qty = $absVarianceQty;
                    $gCycleCountDtl->variance_perc = $variancePerc;

                    $gCycleCountDtl->str_physical_count_status = PhysicalCountStatus::$MAP[$gCycleCountDtl->physical_count_status];
                    
                    $gCycleCountDtl->balance_unit_qty = $quantBal->balance_unit_qty;
                    $gCycleCountDtl->storage_bin_code = $quantBal->storage_bin_code;
                    $gCycleCountDtl->batch_serial_no = $quantBal->batch_serial_no;
                    $gCycleCountDtl->expiry_date = $quantBal->expiry_date;
                    $gCycleCountDtl->receipt_date = $quantBal->receipt_date;

                    unset($gCycleCountDtl->item);
                    unset($gCycleCountDtl->storageBin);
                    $processedCycleCountDtls[] = $gCycleCountDtl;
                }
            }
        }

        //second loop the remaining cycleCountDtlsHashByKey
        foreach($cycleCountDtlsHashByKey as $key => $qCycleCountDtls)
        {
            $keyParts = explode('/',$key);

            $handlingUnitId = $keyParts[0];
            $storageBinId = $keyParts[1];
            $companyId = $keyParts[2];
            $itemId = $keyParts[3];
            $itemBatchId = $keyParts[4];

            $item = null;
            $jobNo = 0;

            //process by group_no
            //calculate the max_variance_perc stat
            $quantBal = new QuantBal();
            $quantBal->id = 0;
            $quantBal->handling_unitId_id = $handlingUnitId;
            $quantBal->storage_bin_id = $storageBinId;
            $quantBal->company_id = $companyId;
            $quantBal->item_id = $itemId;
            $quantBal->item_batch_id = $itemBatchId;

            $quantBal->is_updated = true;
            $quantBal->physical_count_status = 0;

            $quantBal->balance_unit_qty = 0;

            $quantBal->ttl_count_unit_qty = 0;
            $quantBal->max_variance_qty = 0;
            $quantBal->max_abs_variance_qty = 0;
            $quantBal->max_variance_perc = 0;

            $quantBal->job_no = $jobNo;
            $storageBin = $quantBal->storageBin;
            $quantBal->storage_bin_code = $storageBin->code;

            $itemBatch = $quantBal->itemBatch;
            if(!empty($itemBatch))
            {
                $quantBal->batch_serial_no = $itemBatch->batch_serial_no;
                $quantBal->expiry_date = $itemBatch->expiry_date;
                $quantBal->receipt_date = $itemBatch->receipt_date;
            }

            //group by group_no
            $qCycleCountDtlsHashByGroupNo = array();
            foreach($qCycleCountDtls as $qCycleCountDtl)
            {
                $tmpCycleCountDtls = array();
                if(array_key_exists($qCycleCountDtl->group_no, $qCycleCountDtlsHashByGroupNo))
                {
                    $tmpCycleCountDtls = $qCycleCountDtlsHashByGroupNo[$qCycleCountDtl->group_no];
                }
                $tmpCycleCountDtls[] = $qCycleCountDtl;
                $qCycleCountDtlsHashByGroupNo[$qCycleCountDtl->group_no] = $tmpCycleCountDtls;
            }

            //calculate the variance_qty, variance_perc stat
            foreach($qCycleCountDtlsHashByGroupNo as $groupNo => $gCycleCountDtls)
            {
                $ttlCountUnitQty = 0;
                foreach($gCycleCountDtls as $gCycleCountDtl)
                {
                    if($gCycleCountDtl->physical_count_status > $quantBal->physical_count_status)
                    {
                        $quantBal->physical_count_status = $gCycleCountDtl->physical_count_status;
                    }

                    $item = $gCycleCountDtl->item;
                    $jobNo = $gCycleCountDtl->job_no;
                    if(!empty($item))
                    {
                        $countUnitQty = bcmul($gCycleCountDtl->uom_rate, $gCycleCountDtl->qty, $item->qty_scale);
                        $ttlCountUnitQty = bcadd($ttlCountUnitQty, $countUnitQty, $item->qty_scale);
                    }
                }
                
                $varianceQty = bcsub($ttlCountUnitQty, $quantBal->balance_unit_qty, 5);
                $absVarianceQty = abs($varianceQty);
                $variancePerc = 0;
                if($quantBal->balance_unit_qty > 0)
                {
                    $variancePerc = bcmul(bcdiv($absVarianceQty, $quantBal->balance_unit_qty, 10), 100, 10);
                }
                else
                {
                    if($absVarianceQty > 0)
                    {
                        $variancePerc = 100;
                    }
                    else
                    {
                        $variancePerc = 0;
                    }
                }
                if(bccomp($variancePerc, $quantBal->max_variance_perc, 5) >= 0)
                {
                    $quantBal->ttl_count_unit_qty = $ttlCountUnitQty;
                    $quantBal->max_variance_qty = $varianceQty;
                    $quantBal->max_abs_variance_qty = $absVarianceQty;
                    $quantBal->max_variance_perc = $variancePerc;
                }

                //update the variance_qty, variance_perc to every cycleCountDtl
                foreach($gCycleCountDtls as $gCycleCountDtl)
                {
                    $gCycleCountDtl = ItemService::processCaseLoose($gCycleCountDtl, $item, 0, false);
                    $countUnitQty = 0;
                    if(!empty($item))
                    {
                        $countUnitQty = bcmul($gCycleCountDtl->uom_rate, $gCycleCountDtl->qty, $item->qty_scale);
                    }
                    $gCycleCountDtl->ttl_count_unit_qty = $countUnitQty;
                    $gCycleCountDtl->variance_qty = $varianceQty;
                    $gCycleCountDtl->abs_variance_qty = $absVarianceQty;
                    $gCycleCountDtl->variance_perc = $variancePerc;

                    $gCycleCountDtl->str_physical_count_status = PhysicalCountStatus::$MAP[$gCycleCountDtl->physical_count_status];
                    
                    $gCycleCountDtl->balance_unit_qty = $quantBal->balance_unit_qty;
                    $gCycleCountDtl->storage_bin_code = $quantBal->storage_bin_code;
                    $gCycleCountDtl->batch_serial_no = $quantBal->batch_serial_no;
                    $gCycleCountDtl->expiry_date = $quantBal->expiry_date;
                    $gCycleCountDtl->receipt_date = $quantBal->receipt_date;

                    unset($gCycleCountDtl->item);
                    unset($gCycleCountDtl->storageBin);
                    $processedCycleCountDtls[] = $gCycleCountDtl;
                }
            }
        }

        return $processedCycleCountDtls;
    }

    public function updateRecountDetails($id, $strPhysicalCountStatus)
	{
        AuthService::authorize(
            array(
                'cycle_count_update'
            )
        );

        $physicalCountStatus = PhysicalCountStatus::$MAP[$strPhysicalCountStatus];

        $thisCycleCountDtl = CycleCountDtlRepository::findByPk($id);

        $cycleCountHdr = CycleCountHdrRepository::findByPk($thisCycleCountDtl->hdr_id);
        $allCycleCountDtls = $cycleCountHdr->cycleCountDtls;

        $data = array();
        //check physicalCountStatus
        if($physicalCountStatus == PhysicalCountStatus::$MAP['MARK_RECOUNT'])
        {
            //if mark as recount, need to recount all details of the same bin
            foreach($allCycleCountDtls as $tmpCycleCountDtl)
            {
                if($thisCycleCountDtl->storage_bin_id == $tmpCycleCountDtl->storage_bin_id)
                {
                    if($tmpCycleCountDtl->physical_count_status <= 10)
                    {
                        //physical_count_status is already confirmed, can not update
                        $exc = new ApiException(__('CycleCount.physical_count_status_update_failed', [
                            'lineNo'=>$tmpCycleCountDtl->line_no,
                            'physical_count_status'=>PhysicalCountStatus::$MAP[$tmpCycleCountDtl->physical_count_status]
                            ]));
                        //$exc->addData(\App\CycleCountDtl::class, $tmpCycleCountDtl->id);
                        throw $exc;
                    }
                    $data[$tmpCycleCountDtl->id] = $physicalCountStatus;
                }
            }
        }
        elseif($physicalCountStatus == PhysicalCountStatus::$MAP['MARK_CONFIRMED'])
        {
            //if mark as confirmed, need to find other jobs and marked as dropped
            foreach($allCycleCountDtls as $tmpCycleCountDtl)
            {
                if($tmpCycleCountDtl->job_no == $thisCycleCountDtl->job_no
                && $tmpCycleCountDtl->handling_unit_id == $thisCycleCountDtl->handling_unit_id
                && $tmpCycleCountDtl->storage_bin_id == $thisCycleCountDtl->storage_bin_id
                && $tmpCycleCountDtl->company_id == $thisCycleCountDtl->company_id
                && $tmpCycleCountDtl->item_id == $thisCycleCountDtl->item_id
                && $tmpCycleCountDtl->item_batch_id == $thisCycleCountDtl->item_batch_id)
                {
                    if($tmpCycleCountDtl->physical_count_status <= 10)
                    {
                        //physical_count_status is already confirmed, can not update
                        $exc = new ApiException(__('CycleCount.physical_count_status_update_failed', [
                            'lineNo'=>$tmpCycleCountDtl->line_no,
                            'physical_count_status'=>PhysicalCountStatus::$MAP[$tmpCycleCountDtl->physical_count_status]
                            ]));
                        //$exc->addData(\App\CycleCountDtl::class, $tmpCycleCountDtl->id);
                        throw $exc;
                    }
                    if($tmpCycleCountDtl->id == $thisCycleCountDtl->id
                    || ($tmpCycleCountDtl->group_no == $thisCycleCountDtl->group_no)) 
                    {
                        $data[$tmpCycleCountDtl->id] = PhysicalCountStatus::$MAP['MARK_CONFIRMED'];
                    }
                    else
                    {
                        $data[$tmpCycleCountDtl->id] = PhysicalCountStatus::$MAP['MARK_DROPPED'];
                    }
                }                
            }
        }
        elseif($physicalCountStatus == PhysicalCountStatus::$MAP['MARK_DROPPED'])
        {
            //if mark as confirmed, need to find other jobs and marked as dropped
            foreach($allCycleCountDtls as $tmpCycleCountDtl)
            {
                if($tmpCycleCountDtl->job_no == $thisCycleCountDtl->job_no
                && $tmpCycleCountDtl->handling_unit_id == $thisCycleCountDtl->handling_unit_id
                && $tmpCycleCountDtl->storage_bin_id == $thisCycleCountDtl->storage_bin_id
                && $tmpCycleCountDtl->company_id == $thisCycleCountDtl->company_id
                && $tmpCycleCountDtl->item_id == $thisCycleCountDtl->item_id
                && $tmpCycleCountDtl->item_batch_id == $thisCycleCountDtl->item_batch_id)
                {
                    if($tmpCycleCountDtl->physical_count_status <= 10)
                    {
                        //physical_count_status is already confirmed, can not update
                        $exc = new ApiException(__('CycleCount.physical_count_status_update_failed', [
                            'lineNo'=>$tmpCycleCountDtl->line_no,
                            'physical_count_status'=>PhysicalCountStatus::$MAP[$tmpCycleCountDtl->physical_count_status]
                            ]));
                        //$exc->addData(\App\CycleCountDtl::class, $tmpCycleCountDtl->id);
                        throw $exc;
                    }
                    if($tmpCycleCountDtl->id == $thisCycleCountDtl->id
                    || ($tmpCycleCountDtl->group_no == $thisCycleCountDtl->group_no)) 
                    {
                        $data[$tmpCycleCountDtl->id] = PhysicalCountStatus::$MAP['MARK_DROPPED'];
                    }
                    else
                    {
                        $data[$tmpCycleCountDtl->id] = PhysicalCountStatus::$MAP['MARK_CONFIRMED'];
                    }
                }                
            }
        }
        elseif($physicalCountStatus == PhysicalCountStatus::$MAP['RESET'])
        {
            //reset the mark
            if($thisCycleCountDtl->physical_count_status <= 10)
            {
                //physical_count_status is already confirmed, can not update
                $exc = new ApiException(__('CycleCount.physical_count_status_update_failed', [
                    'lineNo'=>$thisCycleCountDtl->line_no,
                    'physical_count_status'=>PhysicalCountStatus::$MAP[$thisCycleCountDtl->physical_count_status]
                    ]));
                //$exc->addData(\App\CycleCountDtl::class, $thisCycleCountDtl->id);
                throw $exc;
            }

            //check thisCycleCountDtl physical_count_status
            if($thisCycleCountDtl->physical_count_status == PhysicalCountStatus::$MAP['MARK_CONFIRMED']
            || $thisCycleCountDtl->physical_count_status == PhysicalCountStatus::$MAP['MARK_DROPPED'])
            {
                //just need to reset the similar characteristic details to COUNTING
                foreach($allCycleCountDtls as $tmpCycleCountDtl)
                {
                    if($tmpCycleCountDtl->job_no == $thisCycleCountDtl->job_no
                    && $tmpCycleCountDtl->handling_unit_id == $thisCycleCountDtl->handling_unit_id
                    && $tmpCycleCountDtl->storage_bin_id == $thisCycleCountDtl->storage_bin_id
                    && $tmpCycleCountDtl->company_id == $thisCycleCountDtl->company_id
                    && $tmpCycleCountDtl->item_id == $thisCycleCountDtl->item_id
                    && $tmpCycleCountDtl->item_batch_id == $thisCycleCountDtl->item_batch_id)
                    {
                        if($tmpCycleCountDtl->physical_count_status <= 10)
                        {
                            //physical_count_status is already confirmed, can not update
                            $exc = new ApiException(__('CycleCount.physical_count_status_update_failed', [
                                'lineNo'=>$tmpCycleCountDtl->line_no,
                                'physical_count_status'=>PhysicalCountStatus::$MAP[$tmpCycleCountDtl->physical_count_status]
                                ]));
                            //$exc->addData(\App\CycleCountDtl::class, $tmpCycleCountDtl->id);
                            throw $exc;
                        }
                        $data[$tmpCycleCountDtl->id] = PhysicalCountStatus::$MAP['COUNTING'];
                    }                
                }
            }
            elseif($thisCycleCountDtl->physical_count_status == PhysicalCountStatus::$MAP['MARK_RECOUNT'])
            {
                //need to reset the details of this bin to COUNTING
                foreach($allCycleCountDtls as $tmpCycleCountDtl)
                {
                    if($thisCycleCountDtl->storage_bin_id == $tmpCycleCountDtl->storage_bin_id)
                    {
                        if($tmpCycleCountDtl->physical_count_status <= 10)
                        {
                            //physical_count_status is already confirmed, can not update
                            $exc = new ApiException(__('CycleCount.physical_count_status_update_failed', [
                                'lineNo'=>$tmpCycleCountDtl->line_no,
                                'physical_count_status'=>PhysicalCountStatus::$MAP[$tmpCycleCountDtl->physical_count_status]
                                ]));
                            //$exc->addData(\App\CycleCountDtl::class, $tmpCycleCountDtl->id);
                            throw $exc;
                        }
                        $data[$tmpCycleCountDtl->id] = PhysicalCountStatus::$MAP['COUNTING'];
                    }
                }
            }
        }
		
		$result = CycleCountHdrRepository::updateRecountDetails($data);
        $dtlModels = $result['dtlModels'];
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $dtlModel->str_physical_count_status = PhysicalCountStatus::$MAP[$dtlModel->physical_count_status];                    
            $documentDetails[] = $dtlModel;
        }

        $message = __('CycleCount.recount_details_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_details'=>$documentDetails
            ),
			'message' => $message
		);
    }

    public function index($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
            array(
                'cycle_count_read'
            )
        );

        //DB::connection()->enableQueryLog();
        $cycleCountHdrs = CycleCountHdrRepository::findAll($siteFlowId, $sorts, $filters, $pageSize);
        $cycleCountHdrs->load(
            'cycleCountDtls'
            //'cycleCountDtls.item', 'cycleCountDtls.item.itemUoms', 
            //'cycleCountDtls.item.itemUoms.uom'
        );
		foreach($cycleCountHdrs as $cycleCountHdr)
		{
			$cycleCountHdr->str_doc_status = DocStatus::$MAP[$cycleCountHdr->doc_status];

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
            //query the docDtls
            /*
			$cycleCountDtls = $cycleCountHdr->cycleCountDtls;
			foreach($cycleCountDtls as $cycleCountDtl)
			{
				$item = $cycleCountDtl->item;

				//calculate the pallet qty, case qty, gross weight, and m3
				$cycleCountDtl = ItemService::processCaseLoose($cycleCountDtl, $item);

                $caseQty = bcadd($caseQty, $cycleCountDtl->case_qty, 8);
                $grossWeight = bcadd($grossWeight, $cycleCountDtl->gross_weight, 8);
                $cubicMeter = bcadd($cubicMeter, $cycleCountDtl->cubic_meter, 8);

				unset($cycleCountDtl->item);
            }
            $cycleCountHdr->details = $cycleCountDtls;
            */

			$cycleCountHdr->case_qty = $caseQty;
			$cycleCountHdr->gross_weight = $grossWeight;
			$cycleCountHdr->cubic_meter = $cubicMeter;
        }
        //Log::error(DB::getQueryLog());
    	return $cycleCountHdrs;
    }

    public function autoConfirmDetails($hdrId)
	{
        AuthService::authorize(
            array(
                'cycle_count_update'
            )
        );

        $cycleCountHdr = CycleCountHdrRepository::findByPk($hdrId);
        $allCycleCountDtls = $cycleCountHdr->cycleCountDtls;

        //group the details by jobNo, palletId, bin, companyId, item, itemBatch
        $allCycleCountDtlsHashByKey = array();
        foreach($allCycleCountDtls as $tmpCycleCountDtl)
        {
            /*
            if($tmpCycleCountDtl->line_no == 548)
            {
                echo '';
            }
            */
            
            //only process those NULL/0 detail
            if($tmpCycleCountDtl->physical_count_status != PhysicalCountStatus::$MAP['COUNTING'])
            {
                continue;
            }

            //skip if the details not yet updated
            if($tmpCycleCountDtl->created_at == $tmpCycleCountDtl->updated_at)
            {
                continue;
            }

            $key = $tmpCycleCountDtl->job_no.'/'.
                $tmpCycleCountDtl->handling_unit_id.'/'.
                $tmpCycleCountDtl->storage_bin_id.'/'.
                $tmpCycleCountDtl->company_id.'/'.
                $tmpCycleCountDtl->item_id.'/'.
                $tmpCycleCountDtl->item_batch_id;
            
            $tmpCycleCountDtls = array();
            if(array_key_exists($key, $allCycleCountDtlsHashByKey))
            {
                $tmpCycleCountDtls = $allCycleCountDtlsHashByKey[$key];
            }
            $tmpCycleCountDtls[] = $tmpCycleCountDtl;

            $allCycleCountDtlsHashByKey[$key] = $tmpCycleCountDtls;
        }

        //loop the job, and identify is it same, if same then mark the 1st as confirmed, 
        //other jobs will be marked as dropped, store the result in data array
        $data = array();
        foreach($allCycleCountDtlsHashByKey as $key => $sameCycleCountDtls)
        {
            /*
            //code for checking auto confirm
            foreach($sameCycleCountDtls as $sameCycleCountDtl)
            {
                if($sameCycleCountDtl->line_no == 548)
                {
                    echo '';
                }
            }
            */

            $keyParts = explode('/',$key);

            $jobNo = $keyParts[0];
            $handlingUnitId = $keyParts[1];
            $storageBinId = $keyParts[2];
            $companyId = $keyParts[3];
            $itemId = $keyParts[4];
            $itemBatchId = $keyParts[5];

            $isVerifySame = true;
            //check if match group_no, group_count
            if(count($sameCycleCountDtls) != $cycleCountHdr->group_count)
            {
                continue;
            }

            //sort the sameCycleCountDtls by group_no ASC
            usort($sameCycleCountDtls, function ($a, $b) {
                return ($a->group_no < $b->group_no) ? -1 : 1;
            });
            for($a = 0; $a < count($sameCycleCountDtls); $a++)
            {
                $sameCycleCountDtl = $sameCycleCountDtls[$a];
                if(($a+1) != $sameCycleCountDtl->group_no)
                {
                    $isVerifySame = false;
                }
            }
            if($isVerifySame == false)
            {
                continue;
            }

            //check if the count qty is same
            $isVerifySame = true;
            $firstCountUnitQty = -35;
            for($a = 0; $a < count($sameCycleCountDtls); $a++)
            {
                $sameCycleCountDtl = $sameCycleCountDtls[$a];
                if($a == 0)
                {
                    $firstCountUnitQty = bcmul($sameCycleCountDtl->qty, $sameCycleCountDtl->uom_rate);
                }
                else
                {
                    $thisCountUnitQty = bcmul($sameCycleCountDtl->qty, $sameCycleCountDtl->uom_rate);
                    if(bccomp($firstCountUnitQty, $thisCountUnitQty, 10) != 0)
                    {
                        $isVerifySame = false;
                    }
                }
            }
            if($isVerifySame == false)
            {
                continue;
            }

            //check if the stock balance is same with count balance
            $quantBal = QuantBalRepository::txnFindByKey($companyId, $handlingUnitId, $itemId, $itemBatchId, $storageBinId, $cycleCountHdr->doc_date->format('Y-m-d'));
            if(bccomp($firstCountUnitQty, $quantBal->balance_unit_qty, 10) != 0)
            {
                $isVerifySame = false;
                continue;
            }
            
            //pass all checking, mark the 1st MARK_CONFIRMED
            //other group will MARK_DROPPED
            for($a = 0; $a < count($sameCycleCountDtls); $a++)
            {
                $sameCycleCountDtl = $sameCycleCountDtls[$a];
                if($a == 0) 
                {
                    $data[$sameCycleCountDtl->id] = PhysicalCountStatus::$MAP['MARK_CONFIRMED'];
                }
                else
                {
                    $data[$sameCycleCountDtl->id] = PhysicalCountStatus::$MAP['MARK_DROPPED'];
                }
            }                
        }
        
        //update the result
		$result = CycleCountHdrRepository::updateRecountDetails($data);
        $dtlModels = $result['dtlModels'];
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $dtlModel->str_physical_count_status = PhysicalCountStatus::$MAP[$dtlModel->physical_count_status];                    
            $documentDetails[] = $dtlModel;
        }

        $message = __('CycleCount.recount_details_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_details'=>$documentDetails
            ),
			'message' => $message
		);
    }
}
