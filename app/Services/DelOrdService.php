<?php

namespace App\Services;

use App\DelOrdHdr;
use App\Services\Env\ResType;
use App\Services\Env\DocStatus;
use App\Repositories\ItemRepository;
use App\Repositories\DelOrdHdrRepository;
use App\Repositories\DelOrdDtlRepository;
use App\Repositories\DocTxnFlowRepository;
use App\Services\Utils\ApiException;
use App\Services\ItemService;

class DelOrdService extends InventoryService
{
    public static $HDR_ESCAPE = array(
		'id',
		'doc_code',
		'proc_type',
		'sls_ord_hdr_id',
		'sls_ord_hdr_code',
		'del_ord_hdr_id',
		'del_ord_hdr_code',
		'sls_inv_hdr_id',
		'sls_inv_hdr_code',
		'sls_rtn_hdr_id',
		'sls_rtn_hdr_code',
		'sls_cn_hdr_id',
		'sls_cn_hdr_code',
		'sign',
		'cur_res_type',
		'doc_date',
		'doc_status',
		'created_at',
		'updated_at'
	);
	
	public static $DTL_ESCAPE = array(
		'id',
		'hdr_id',
		'created_at',
		'updated_at',
		'outb_ord_hdr',
		'item'
  	);

  	public function __construct() 
	{

	}

	static public function transitionToComplete($hdrId)
  	{
		AuthService::authorize(
            array(
                'del_ord_confirm'
            )
		);
		
		//use transaction to make sure this is latest value
		$hdrModel = DelOrdHdrRepository::txnFindByPk($hdrId);

		//only DRAFT or above can transition to COMPLETE
		if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE']
		|| $hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('DelOrd.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
			$exc->addData(\App\DelOrdHdr::class, $hdrModel->id);
			throw $exc;
		}

		//preliminary checking

		//commit the document
		$hdrModel = DelOrdHdrRepository::commitToComplete($hdrModel->id);
		if(!empty($hdrModel))
		{
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		
		$exc = new ApiException(__('DelOrd.commit_to_complete_error', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\DelOrdHdr::class, $hdrModel->id);
		throw $exc;
	}

	static public function transitionToWip($hdrId)
  	{
		//use transaction to make sure this is latest value
		$hdrModel = DelOrdHdrRepository::txnFindByPk($hdrId);
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('DelOrd.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
			$exc->addData(\App\DelOrdHdr::class, $hdrModel->id);
			throw $exc;
		}

		//preliminary checking

		//commit the document
		$hdrModel = DelOrdHdrRepository::commitToWip($hdrModel->id);
		if(!empty($hdrModel))
		{
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			$isSuccess = true;
		}

		$exc = new ApiException(__('DelOrd.commit_to_wip_error', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\DelOrdHdr::class, $hdrModel->id);
		throw $exc;
	}

	static public function transitionToDraft($hdrId)
  	{
		//use transaction to make sure this is latest value
		$hdrModel = DelOrdHdrRepository::txnFindByPk($hdrId);

		//only WIP or COMPLETE can transition to DRAFT
		if($hdrModel->doc_status == DocStatus::$MAP['WIP'])
		{
			$hdrModel = DelOrdHdrRepository::revertWipToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		elseif($hdrModel->doc_status == DocStatus::$MAP['COMPLETE'])
		{
			AuthService::authorize(
				array(
					'del_ord_revert'
				)
			);

			$hdrModel = DelOrdHdrRepository::revertCompleteToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		
		$exc = new ApiException(__('DelOrd.doc_status_is_not_wip_or_complete', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\DelOrdHdr::class, $hdrModel->id);
		throw $exc;
	}

	public function showDetails($hdrId) 
	{
		AuthService::authorize(
			array(
				'del_ord_read',
                'del_ord_update'
			)
		);

		$delOrdDtls = DelOrdDtlRepository::findAllByHdrId($hdrId);
		$delOrdDtls->load(
            'item', 'item.itemUoms', 'item.itemUoms.uom'
        );
        foreach($delOrdDtls as $delOrdDtl)
        {
            $delOrdDtl = self::processOutgoingDetail($delOrdDtl);
        }
        return $delOrdDtls;
    }

	public function updateDetails($hdrId, $dtlArray)
	{
		AuthService::authorize(
			array(
				'del_ord_update'
			)
		);

		//get the associated outbOrdHdr
		$delOrdHdr = DelOrdHdrRepository::txnFindByPk($hdrId);
		if($delOrdHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('DelOrd.doc_status_is_wip_or_complete', ['docCode'=>$delOrdHdr->doc_code]));
            $exc->addData(\App\DelOrdHdr::class, $delOrdHdr->id);
            throw $exc;
        }
		$delOrdHdrData = $delOrdHdr->toArray();
		$delOrdHdr->load('outbOrdHdr', 'deliveryPoint');
		$deliveryPoint = $delOrdHdr->deliveryPoint;

		//query the delOrdDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$promoUuidArray = array();
		$delOrdDtlArray = array();
		$delOrdDtlModels = DelOrdDtlRepository::findAllByHdrId($hdrId);
		foreach($delOrdDtlModels as $delOrdDtlModel)
		{
			$delOrdDtlData = $delOrdDtlModel->toArray();
			$delOrdDtlData['is_modified'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{
				if($dtlData['id'] == $delOrdDtlData['id'])
				{
					$item = ItemRepository::findByPk($dtlData['item_id']);
					$dtlData = self::processIncomingDetail($dtlData, $item);
					
					$uuid = $delOrdDtlData['promo_uuid'];
					$dtlData = $this->updateSaleItemUom($dtlData, $delOrdHdr->doc_date, 0, $delOrdHdr->currency_id, 0, 0);
					foreach($dtlData as $fieldName => $value)
					{
						$delOrdDtlData[$fieldName] = $value;
					}
					//$delOrdDtlData = $this->clearPromotion($delOrdDtlData);
					$delOrdDtlData['is_modified'] = 1;

					if(!empty($uuid))
					{
						$promoUuidArray[] = $uuid;
					}
					break;
				}
			}
			$delOrdDtlArray[] = $delOrdDtlData;
		}

		//another loop to reset the promotion if this record is updated
		foreach($delOrdDtlArray as $delOrdDtlSeq => $delOrdDtlData)
		{
			foreach($promoUuidArray as $promoUuid)
			{
				if($promoUuid == $delOrdDtlData['promo_uuid'])
				{
					//$delOrdDtlData = $this->clearPromotion($delOrdDtlData);
					$delOrdDtlData['is_modified'] = 1;
					break;
				}
			}
			$delOrdDtlArray[$delOrdDtlSeq] = $delOrdDtlData;
		}
		
		//promotion
		$modifiedDtlArray = $this->processPromotion($delOrdDtlArray, $delOrdHdr->doc_date, $delOrdHdr->division_id, 
		$deliveryPoint->debtor_id, $deliveryPoint->debtor_group_01_id, $deliveryPoint->debtor_group_02_id, $deliveryPoint->debtor_group_03_id, $deliveryPoint->debtor_group_04_id, $deliveryPoint->debtor_group_05_id);
		foreach($delOrdDtlArray as $key => $delOrdDtlData)
		{
			foreach($modifiedDtlArray as $modifiedDtlData)
			{
				if($modifiedDtlData['id'] == $delOrdDtlData['id'])
				{
					foreach($modifiedDtlData as $field => $value)
					{
						$delOrdDtlData[$field] = $value;
					}
					$delOrdDtlData['is_modified'] = 1;
					$delOrdDtlArray[$key] = $delOrdDtlData;
				}
			}
		}

		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($delOrdHdrData, $delOrdDtlArray);
		$delOrdHdrData = $result['hdrData'];
		$delOrdDtlArray = $result['dtlArray'];

		//need to update the associated outbOrdHdr
		$outbOrdHdrData = array();
		$outbOrdDtlArray = array();
		$outbOrdHdr = $delOrdHdr->outbOrdHdr;
		if(!empty($outbOrdHdr))
		{
			if($outbOrdHdr->cur_res_type == ResType::$MAP['SLS_INV'])
			{
				$result = $this->tallyOutbOrd($delOrdHdrData, $delOrdDtlArray);
				$outbOrdHdrData = $result['outbOrdHdrData'];
				$outbOrdDtlArray = $result['outbOrdDtlArray'];
			}
		}
		
		$result = DelOrdHdrRepository::updateDetails($delOrdHdrData, $delOrdDtlArray, array(), $outbOrdHdrData, $outbOrdDtlArray, array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('DelOrd.details_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function createDetail($hdrId, $data)
	{
		AuthService::authorize(
			array(
				'del_ord_update'
			)
		);

		$delOrdHdr = DelOrdHdrRepository::txnFindByPk($hdrId);
		$delOrdHdrData = $delOrdHdr->toArray();
		$delOrdHdr->load('outbOrdHdr', 'deliveryPoint');
		$deliveryPoint = $delOrdHdr->deliveryPoint;

		$lastLineNo = 0;
		$delOrdDtlArray = array();
		$delOrdDtlModels = DelOrdDtlRepository::findAllByHdrId($hdrId);
		foreach($delOrdDtlModels as $delOrdDtlModel)
		{
			$lastLineNo = $delOrdDtlModel->line_no;
			$delOrdDtlData = $delOrdDtlModel->toArray();
			$delOrdDtlData['is_modified'] = 0;
			$delOrdDtlArray[] = $delOrdDtlData;
		}

		//assign temp id
		$tmpUuid = uniqid();
		//append NEW here to make sure it will be insert in repository later
		$data['id'] = 'NEW'.$tmpUuid;
		$data['line_no'] = $lastLineNo + 1;
		$data['is_modified'] = 1;
		$data = $this->updateSaleItemUom($data, $delOrdHdr['doc_date'], 0, $delOrdHdr['currency_id'], 0, 0);
		$delOrdDtlData = $this->initDelOrdDtlData($data);
		$delOrdDtlArray[] = $delOrdDtlData;

		//promotion
		$modifiedDtlArray = $this->processPromotion($delOrdDtlArray, $delOrdHdr->doc_date, $delOrdHdr->division_id, 
		$deliveryPoint->debtor_id, $deliveryPoint->debtor_group_01_id, $deliveryPoint->debtor_group_02_id, $deliveryPoint->debtor_group_03_id, $deliveryPoint->debtor_group_04_id, $deliveryPoint->debtor_group_05_id);
		foreach($delOrdDtlArray as $key => $delOrdDtlData)
		{
			foreach($modifiedDtlArray as $modifiedDtlData)
			{
				if($modifiedDtlData['id'] == $delOrdDtlData['id'])
				{
					foreach($modifiedDtlData as $field => $value)
					{
						$delOrdDtlData[$field] = $value;
					}
					$delOrdDtlData['is_modified'] = 1;
					$delOrdDtlArray[$key] = $delOrdDtlData;
				}
			}
		}

		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($delOrdHdrData, $delOrdDtlArray);
		$delOrdHdrData = $result['hdrData'];
		$delOrdDtlArray = $result['dtlArray'];		

		//need to update the associated outbOrdHdr
		$outbOrdHdrData = array();
		$outbOrdDtlArray = array();
		$outbOrdHdr = $delOrdHdr->outbOrdHdr;
		if(!empty($outbOrdHdr))
		{
			if($outbOrdHdr->cur_res_type == ResType::$MAP['DEL_ORD'])
			{
				$result = $this->tallyOutbOrd($delOrdHdrData, $delOrdDtlArray);
				$outbOrdHdrData = $result['outbOrdHdrData'];
				$outbOrdDtlArray = $result['outbOrdDtlArray'];
			}
		}

		$result = DelOrdHdrRepository::updateDetails($delOrdHdrData, $delOrdDtlArray, array(), $outbOrdHdrData, $outbOrdDtlArray, array());
		return $result;
	}

	public function deleteDetails($hdrId, $dtlArray)
	{
		AuthService::authorize(
			array(
				'del_ord_update'
			)
		);

		//get the associated outbOrdHdr
		$delOrdHdr = DelOrdHdrRepository::txnFindByPk($hdrId);
		$delOrdHdrData = $delOrdHdr->toArray();
		$delOrdHdr->load('outbOrdHdr', 'deliveryPoint');
		$deliveryPoint = $delOrdHdr->deliveryPoint;

		//query the delOrdDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$promoUuidArray = array();
		$delDelOrdDtlArray = array();
		$delOrdDtlArray = array();
		$delOrdDtlModels = DelOrdDtlRepository::findAllByHdrId($hdrId);
		foreach($delOrdDtlModels as $delOrdDtlModel)
		{
			$delOrdDtlData = $delOrdDtlModel->toArray();
			$delOrdDtlData['is_modified'] = 0;
			$delOrdDtlData['is_deleted'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{
				if($dtlData['id'] == $delOrdDtlData['id'])
				{
					//this is deleted dtl
					$uuid = $delOrdDtlData['promo_uuid'];
					$delOrdDtlData = $this->clearPromotion($delOrdDtlData);
					$delOrdDtlData['is_deleted'] = 1;

					if(!empty($uuid))
					{
						$promoUuidArray[] = $uuid;
					}
					break;
				}
			}
			if($delOrdDtlData['is_deleted'] > 0)
			{
				$delDelOrdDtlArray[] = $delOrdDtlData;
			}
			else
			{
				$delOrdDtlArray[] = $delOrdDtlData;
			}
		}

		//another loop to reset the promotion if this record is updated
		//and update the line no also
		$lineNo = 1;
		foreach($delOrdDtlArray as $delOrdDtlSeq => $delOrdDtlData)
		{
			foreach($promoUuidArray as $promoUuid)
			{
				if($promoUuid == $delOrdDtlData['promo_uuid'])
				{
					$delOrdDtlData = $this->clearPromotion($delOrdDtlData);
					$delOrdDtlData['is_modified'] = 1;
					break;
				}
			}
			$delOrdDtlData['line_no'] = $lineNo++;
			$delOrdDtlArray[$delOrdDtlSeq] = $delOrdDtlData;
		}
		
		//promotion
		$modifiedDtlArray = $this->processPromotion($delOrdDtlArray, $delOrdHdr->doc_date, $delOrdHdr->division_id, 
		$deliveryPoint->debtor_id, $deliveryPoint->debtor_group_01_id, $deliveryPoint->debtor_group_02_id, $deliveryPoint->debtor_group_03_id, $deliveryPoint->debtor_group_04_id, $deliveryPoint->debtor_group_05_id);
		foreach($delOrdDtlArray as $delOrdDtlSeq => $delOrdDtlData)
		{
			foreach($modifiedDtlArray as $modifiedDtlData)
			{
				if($modifiedDtlData['id'] == $delOrdDtlData['id'])
				{
					foreach($modifiedDtlData as $field => $value)
					{
						$delOrdDtlData[$field] = $value;
					}
					$delOrdDtlData['is_modified'] = 1;
					$delOrdDtlArray[$delOrdDtlSeq] = $delOrdDtlData;
					break;
				}
			}
		}

		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($delOrdHdrData, $delOrdDtlArray, $delDelOrdDtlArray);
		$delOrdHdrData = $result['hdrData'];
		$modifiedDtlArray = $result['dtlArray'];
		foreach($delOrdDtlArray as $delOrdDtlSeq => $delOrdDtlData)
		{
			foreach($modifiedDtlArray as $modifiedDtlData)
			{
				if($modifiedDtlData['id'] == $delOrdDtlData['id'])
				{
					foreach($modifiedDtlData as $field => $value)
					{
						$delOrdDtlData[$field] = $value;
					}
					$delOrdDtlData['is_modified'] = 1;
					$delOrdDtlArray[$delOrdDtlSeq] = $delOrdDtlData;
					break;
				}
			}
		}

		//need to update the associated outbOrdHdr
		$outbOrdHdrData = array();
		$outbOrdDtlArray = array();
		$delOutbOrdDtlArray = array();
		$outbOrdHdr = $delOrdHdr->outbOrdHdr;
		if(!empty($outbOrdHdr))
		{
			if($outbOrdHdr->cur_res_type == ResType::$MAP['DEL_ORD'])
			{
				$result = $this->tallyOutbOrd($delOrdHdrData, $delOrdDtlArray, $delDelOrdDtlArray);
				$outbOrdHdrData = $result['outbOrdHdrData'];
				$outbOrdDtlArray = $result['outbOrdDtlArray'];
				$delOutbOrdDtlArray = $result['delOutbOrdDtlArray'];
			}
		}
		
		$result = DelOrdHdrRepository::updateDetails($delOrdHdrData, $delOrdDtlArray, $delDelOrdDtlArray, $outbOrdHdrData, $outbOrdDtlArray, $delOutbOrdDtlArray);
		return $result;
	}

	public function showHeader($hdrId)
	{
		AuthService::authorize(
			array(
				'del_ord_read',
                'del_ord_update'
			)
		);

		$model = DelOrdHdrRepository::findByPk($hdrId);
		if(!empty($model))
        {
            $model = self::processOutgoingHeader($model);
		}
		
		return $model;
	}

	static public function processOutgoingHeader($model, $isShowPrint = false)
	{
		$docFlows = self::processDocFlows($model);
		$model->doc_flows = $docFlows;
		
		if($isShowPrint)
		{
			$printDocTxn = PrintDocTxnRepository::queryPrintDocTxn(DelOrdHdr::class, $model->id);
			$model->print_count = $printDocTxn->print_count;
			$model->first_printed_at = $printDocTxn->first_printed_at;
			$model->last_printed_at = $printDocTxn->last_printed_at;
		}

		$division = $model->division;
        $model->division_code = $division->code;
        $company = $model->company;
        $model->company_code = $company->code;

        $model->str_doc_status = DocStatus::$MAP[$model->doc_status];

        //currency select2
		$initCurrencyOption = array('value'=>0,'label'=>'');
		$model->currency_symbol = '';
        $currency = $model->currency;
        if(!empty($currency))
        {
			$model->currency_symbol = $currency->symbol;
            $initCurrencyOption = array('value'=>$currency->id, 'label'=>$currency->symbol);
        }
        $model->currency_select2 = $initCurrencyOption;

        //creditTerm select2
		$initCreditTermOption = array('value'=>0,'label'=>'');
		$model->credit_term_code = '';
		$model->credit_term_type = 0;
        $creditTerm = $model->creditTerm;
        if(!empty($creditTerm))
        {
			$model->credit_term_code = $creditTerm->code;
			$model->credit_term_type = $creditTerm->credit_term_type;
            $initCreditTermOption = array('value'=>$creditTerm->id, 'label'=>$creditTerm->code);
        }
        $model->credit_term_select2 = $initCreditTermOption;

		//salesman select2
		$initSalesmanOption = array('value'=>0,'label'=>'');
		$model->salesman_code = '';
		$salesman = $model->salesman;
		if(!empty($salesman))
		{
			$model->salesman_code = $salesman->username;
			$initSalesmanOption = array(
				'value'=>$salesman->id,
				'label'=>$salesman->username
			);
		}
		$model->salesman_select2 = $initSalesmanOption;

        //delivery point select2
		$deliveryPoint = $model->deliveryPoint;
		$model->delivery_point_code = $deliveryPoint->code;
		$model->delivery_point_area_code = $deliveryPoint->area_code;
		$model->delivery_point_code = $deliveryPoint->code;
		$model->delivery_point_company_name_01 = $deliveryPoint->company_name_01;
		$model->delivery_point_company_name_02 = $deliveryPoint->company_name_02;
        $model->delivery_point_unit_no = $deliveryPoint->unit_no;
        $model->delivery_point_building_name = $deliveryPoint->building_name;
        $model->delivery_point_street_name = $deliveryPoint->street_name;
        $model->delivery_point_district_01 = $deliveryPoint->district_01;
        $model->delivery_point_district_02 = $deliveryPoint->district_02;
        $model->delivery_point_postcode = $deliveryPoint->postcode;
        $model->delivery_point_state_name = $deliveryPoint->state_name;
		$model->delivery_point_country_name = $deliveryPoint->country_name;
		$model->delivery_point_attention = $deliveryPoint->attention;
		$model->delivery_point_phone_01 = $deliveryPoint->phone_01;
		$model->delivery_point_phone_02 = $deliveryPoint->phone_02;
		$model->delivery_point_fax_01 = $deliveryPoint->fax_01;
		$model->delivery_point_fax_02 = $deliveryPoint->fax_02;
        $initDeliveryPointOption = array(
            'value'=>$deliveryPoint->id,
            'label'=>$deliveryPoint->code.' '.$deliveryPoint->company_name_01
        );
		$model->delivery_point_select2 = $initDeliveryPointOption;
		
		//debtor
		$debtor = $deliveryPoint->debtor;
		$model->debtor_code = $debtor->code;
		$model->debtor_area_code = $debtor->area_code;
		$model->debtor_company_name_01 = $debtor->company_name_01;
		$model->debtor_company_name_02 = $debtor->company_name_02;
        $model->debtor_unit_no = $debtor->unit_no;
        $model->debtor_building_name = $debtor->building_name;
        $model->debtor_street_name = $debtor->street_name;
        $model->debtor_district_01 = $debtor->district_01;
        $model->debtor_district_02 = $debtor->district_02;
        $model->debtor_postcode = $debtor->postcode;
        $model->debtor_state_name = $debtor->state_name;
		$model->debtor_country_name = $debtor->country_name;
		$model->debtor_attention = $debtor->attention;
		$model->debtor_phone_01 = $debtor->phone_01;
		$model->debtor_phone_02 = $debtor->phone_02;
		$model->debtor_fax_01 = $debtor->fax_01;
		$model->debtor_fax_02 = $debtor->fax_02;

        $model->hdr_disc_val_01 = 0;
        $model->hdr_disc_perc_01 = 0;
        $model->hdr_disc_val_02 = 0;
        $model->hdr_disc_perc_02 = 0;
        $model->hdr_disc_val_03 = 0;
        $model->hdr_disc_perc_03 = 0;
        $model->hdr_disc_val_04 = 0;
        $model->hdr_disc_perc_04 = 0;
        $model->hdr_disc_val_05 = 0;
        $model->hdr_disc_perc_05 = 0;
        $firstDtlModel = DelOrdDtlRepository::findTopByHdrId($model->id);
        if(!empty($firstDtlModel))
        {
            $model->hdr_disc_val_01 = $firstDtlModel->hdr_disc_val_01;
            $model->hdr_disc_perc_01 = $firstDtlModel->hdr_disc_perc_01;
            $model->hdr_disc_val_02 = $firstDtlModel->hdr_disc_val_02;
            $model->hdr_disc_perc_02 = $firstDtlModel->hdr_disc_perc_02;
            $model->hdr_disc_val_03 = $firstDtlModel->hdr_disc_val_03;
            $model->hdr_disc_perc_03 = $firstDtlModel->hdr_disc_perc_03;
            $model->hdr_disc_val_04 = $firstDtlModel->hdr_disc_val_04;
            $model->hdr_disc_perc_04 = $firstDtlModel->hdr_disc_perc_04;
            $model->hdr_disc_val_05 = $firstDtlModel->hdr_disc_val_05;
            $model->hdr_disc_perc_05 = $firstDtlModel->hdr_disc_perc_05;
		}
		
		//pickListOutbOrds
		$model->sls_ord_hdr_doc_code = '';
		$model->pick_list_hdr_doc_code = '';
		$model->pick_list_hdr_doc_date = '1970-01-01';
		$pickListOutbOrds = PickListOutbOrdRepository::findAllCompletePickListByOutbOrdHdrId($model->outb_ord_hdr_id);
		foreach($pickListOutbOrds as $pickListOutbOrd)
		{
			$model->sls_ord_hdr_doc_code = $pickListOutbOrd->sls_ord_hdr_doc_code;
			$model->pick_list_hdr_doc_code = $pickListOutbOrd->pick_list_hdr_doc_code;
			$model->pick_list_hdr_doc_date = $pickListOutbOrd->pick_list_hdr_doc_date;
			break;
		}

		$model->net_amt_english = InventoryService::numberToWords($model->net_amt);

        return $model;
	}
    
	static public function formatDocuments($delOrdHdrs, $isShowPrint = false)
	{
		AuthService::authorize(
            array(
                'del_ord_print'
            )
		);

		foreach($delOrdHdrs as $delOrdHdr)
		{
			if($isShowPrint)
			{
				$printDocTxn = PrintDocTxnRepository::queryPrintDocTxn(DelOrdHdr::class, $delOrdHdr->id);
				$delOrdHdr->print_count = $printDocTxn->print_count;
				$delOrdHdr->first_printed_at = $printDocTxn->first_printed_at;
				$delOrdHdr->last_printed_at = $printDocTxn->last_printed_at;
			}

			$delOrdHdr->str_doc_status = DocStatus::$MAP[$delOrdHdr->doc_status];

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletQty = 0;
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			$dtlDiscAmt = 0;
			//query the docDtls
			$delOrdDtls = DelOrdDtlRepository::findAllByHdrId($delOrdHdr->id);
			$delOrdDtls->load('item');

			$formatDelOrdDtls = array();
			foreach($delOrdDtls as $delOrdDtl)
			{
				$item = $delOrdDtl->item;

				$delOrdDtl = ItemService::processCaseLoose($delOrdDtl, $item);

				$caseQty = bcadd($caseQty, $delOrdDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $delOrdDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $delOrdDtl->cubic_meter, 8);
				$dtlDiscAmt = bcadd($dtlDiscAmt, $delOrdDtl->dtl_disc_amt, 8);

				unset($delOrdDtl->item);
				$formatDelOrdDtls[] = $delOrdDtl;
			}

			$delOrdHdr->case_qty = $caseQty;
			$delOrdHdr->gross_weight = $grossWeight;
			$delOrdHdr->cubic_meter = $cubicMeter;
			$delOrdHdr->dtl_disc_amt = $dtlDiscAmt;

			$delOrdHdr->details = $formatDelOrdDtls;
		}
		return $delOrdHdrs;
	}

	static public function processOutgoingDetail($model)
    {
        $item = $model->item;
        $model->item_code = $item->code;
        //$model->item_desc_01 = $item->desc_01;
        //$model->item_desc_02 = $item->desc_02;
        //item select2
        $initItemOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($item))
        {
            $initItemOption = array(
                'value'=>$item->id, 
                'label'=>$item->code.' '.$item->desc_01,
            );
        }
		$model->item_select2 = $initItemOption;
		
		//calculate the pallet qty, case qty, gross weight, and m3
        $model = ItemService::processCaseLoose($model, $item);

		$model->uom_code = '';
        $uom = $model->uom;
        //uom select2
        $initUomOption = array('value'=>0, 'label'=>'');
        if(!empty($uom))
        {
			$model->uom_code = $uom->code;
            $initUomOption = array('value'=>$uom->id,'label'=>$uom->code);
        }        
		$model->uom_select2 = $initUomOption;
		
		//location
		$location = $model->location;
		$model->location_code = '';
		$initLocationOption = array('value'=>0, 'label'=>'');
        if(!empty($location))
        {
			$model->location_code = $location->code;
            $initLocationOption = array('value'=>$location->id,'label'=>$location->code);
        }        
		$model->location_select2 = $initLocationOption;

        return $model;
	}
	
	public function transitionToStatus($hdrId, $strDocStatus)
    {
        $hdrModel = null;
        if(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['WIP'])
        {
            $hdrModel = self::transitionToWip($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['COMPLETE'])
        {
            $hdrModel = self::transitionToComplete($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['DRAFT'])
        {
            $hdrModel = self::transitionToDraft($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['VOID'])
        {
            $hdrModel = self::transitionToVoid($hdrId);
        }
        
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $message = __('DelOrd.document_successfully_transition', ['docCode'=>$documentHeader->doc_code,'strDocStatus'=>$strDocStatus]);
        
        return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>array(),
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}
	
	static public function transitionToVoid($hdrId)
    {
		AuthService::authorize(
			array(
				'del_ord_revert'
			)
		);

		//use transaction to make sure this is latest value
		$hdrModel = DelOrdHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('DelOrd.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\DelOrdHdr::class, $hdrModel->id);
            throw $exc;
		}

		//revert the document
		$hdrModel = DelOrdHdrRepository::revertDraftToVoid($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('DelOrd.commit_to_void_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\DelOrdHdr::class, $hdrModel->id);
        throw $exc;
	}
	
	public function initHeader($divisionId)
    {
		AuthService::authorize(
			array(
				'del_ord_create'
			)
		);

        $user = Auth::user();

        $model = new DelOrdHdr();
        $model->doc_flows = array();
        $model->doc_status = DocStatus::$MAP['DRAFT'];
        $model->str_doc_status = DocStatus::$MAP[$model->doc_status];
        $model->doc_code = '';
        $model->ref_code_01 = '';
        $model->ref_code_02 = '';
        $model->doc_date = date('Y-m-d');
        $model->est_del_date = date('Y-m-d');
        $model->desc_01 = '';
        $model->desc_02 = '';

        $division = DivisionRepository::findByPk($divisionId);
        $model->division_id = $divisionId;
        $model->division_code = '';
        $model->site_flow_id = 0;
        $model->company_id = 0;
        $model->company_code = '';
        if(!empty($division))
        {
            $model->division_code = $division->code;
            $model->site_flow_id = $division->site_flow_id;
            $company = $division->company;
            $model->company_id = $company->id;
            $model->company_code = $company->code;
        }

        $model->doc_no_id = 0;
        $docNoIdOptions = array();
        $docNos = DocNoRepository::findAllDivisionDocNo($divisionId, \App\DelOrdHdr::class, $model->doc_date);
        for($a = 0; $a < count($docNos); $a++)
        {
            $docNo = $docNos[$a];
            if($a == 0)
            {
                $model->doc_no_id = $docNo->id;
            }
            $docNoIdOptions[] = array('value'=>$docNo->id, 'label'=>$docNo->latest_code);
        }
        $model->doc_no_id_options = $docNoIdOptions;

        //currency dropdown
        $model->currency_id = 0;
        $model->currency_rate = 1;
        $initCurrencyOption = array('value'=>0,'label'=>'');
        $model->currency_select2 = $initCurrencyOption;

        //creditTerm select2
        $initCreditTermOption = array('value'=>0,'label'=>'');
        $model->credit_term_select2 = $initCreditTermOption;

        //salesman select2
        $initSalesmanOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($user))
        {
            $initSalesmanOption = array(
                'value'=>$user->id, 
                'label'=>$user->username
            );
        }
        $model->salesman_select2 = $initSalesmanOption;

        //delivery point select2
        $model->delivery_point_unit_no = '';
        $model->delivery_point_building_name = '';
        $model->delivery_point_street_name = '';
        $model->delivery_point_district_01 = '';
        $model->delivery_point_district_02 = '';
        $model->delivery_point_postcode = '';
        $model->delivery_point_state_name = '';
        $model->delivery_point_country_name = '';
        $initDeliveryPointOption = array(
            'value'=>0,
            'label'=>''
        );
        $deliveryPoint = DeliveryPointRepository::findTop();
        if(!empty($deliveryPoint))
        {
            $initDeliveryPointOption = array(
                'value'=>$deliveryPoint->id, 
                'label'=>$deliveryPoint->code.' '.$deliveryPoint->company_name_01
            );

            $model->delivery_point_unit_no = $deliveryPoint->unit_no;
            $model->delivery_point_building_name = $deliveryPoint->building_name;
            $model->delivery_point_street_name = $deliveryPoint->street_name;
            $model->delivery_point_district_01 = $deliveryPoint->district_01;
            $model->delivery_point_district_02 = $deliveryPoint->district_02;
            $model->delivery_point_postcode = $deliveryPoint->postcode;
            $model->delivery_point_state_name = $deliveryPoint->state_name;
            $model->delivery_point_country_name = $deliveryPoint->country_name;
        }
        $model->delivery_point_select2 = $initDeliveryPointOption;

        $model->hdr_disc_val_01 = 0;
        $model->hdr_disc_perc_01 = 0;
        $model->hdr_disc_val_02 = 0;
        $model->hdr_disc_perc_02 = 0;
        $model->hdr_disc_val_03 = 0;
        $model->hdr_disc_perc_03 = 0;
        $model->hdr_disc_val_04 = 0;
        $model->hdr_disc_perc_04 = 0;
        $model->hdr_disc_val_05 = 0;
        $model->hdr_disc_perc_05 = 0;

        $model->disc_amt = 0;
        $model->tax_amt = 0;
        $model->round_adj_amt = 0;
        $model->net_amt = 0;

        return $model;
	}

	static public function processIncomingHeader($data, $isClearHdrDiscTax=false)
    {
        if(array_key_exists('doc_flows', $data))
        {
            unset($data['doc_flows']);
        }
        if(array_key_exists('submit_action', $data))
        {
            unset($data['submit_action']);
        }
        //preprocess the select2 and dropDown
        if(array_key_exists('credit_term_select2', $data))
        {
            $data['credit_term_id'] = $data['credit_term_select2']['value'];
            unset($data['credit_term_select2']);
        }
        if(array_key_exists('currency_select2', $data))
        {
            $data['currency_id'] = $data['currency_select2']['value'];
            unset($data['currency_select2']);
        }   
        if(array_key_exists('salesman_select2', $data))
        {
            $data['salesman_id'] = $data['salesman_select2']['value'];
            unset($data['salesman_select2']);
        }
        if(array_key_exists('delivery_point_select2', $data))
        {
            $data['delivery_point_id'] = $data['delivery_point_select2']['value'];
            unset($data['delivery_point_select2']);
        }
        if(array_key_exists('str_doc_status', $data))
        {
            unset($data['str_doc_status']);
        }
        if(array_key_exists('division_code', $data))
        {
            unset($data['division_code']);
        }
        if(array_key_exists('company_code', $data))
        {
            unset($data['company_code']);
        }
        if(array_key_exists('delivery_point_unit_no', $data))
        {
            unset($data['delivery_point_unit_no']);
        }
        if(array_key_exists('delivery_point_building_name', $data))
        {
            unset($data['delivery_point_building_name']);
        }
        if(array_key_exists('delivery_point_street_name', $data))
        {
            unset($data['delivery_point_street_name']);
        }
        if(array_key_exists('delivery_point_district_01', $data))
        {
            unset($data['delivery_point_district_01']);
        }
        if(array_key_exists('delivery_point_district_02', $data))
        {
            unset($data['delivery_point_district_02']);
        }
        if(array_key_exists('delivery_point_postcode', $data))
        {
            unset($data['delivery_point_postcode']);
        }
        if(array_key_exists('delivery_point_state_name', $data))
        {
            unset($data['delivery_point_state_name']);
        }
        if(array_key_exists('delivery_point_country_name', $data))
        {
            unset($data['delivery_point_country_name']);
        }
        if(array_key_exists('division', $data))
        {
            unset($data['division']);
        }
        if(array_key_exists('company', $data))
        {
            unset($data['company']);
        }
        if(array_key_exists('salesman', $data))
        {
            unset($data['salesman']);
        }
        if(array_key_exists('delivery_point', $data))
        {
            unset($data['delivery_point']);
        }

        if($isClearHdrDiscTax)
        {  
            for($a = 1; $a <= 5; $a++)
            {
                if(array_key_exists('hdr_disc_val_0'.$a, $data))
                {
                    unset($data['hdr_disc_val_0'.$a]);
                }
                if(array_key_exists('hdr_disc_perc_0'.$a, $data))
                {
                    unset($data['hdr_disc_perc_0'.$a]);
                }
                if(array_key_exists('hdr_tax_val_0'.$a, $data))
                {
                    unset($data['hdr_tax_val_0'.$a]);
                }
                if(array_key_exists('hdr_tax_perc_0'.$a, $data))
                {
                    unset($data['hdr_tax_perc_0'.$a]);
                }
            }
        }
        return $data;
    }
	
	public function updateHeader($hdrData)
	{
		AuthService::authorize(
			array(
				'del_ord_update'
			)
		);

        $hdrData = self::processIncomingHeader($hdrData);

		//get the associated inbOrdHdr
        $delOrdHdr = DelOrdHdrRepository::txnFindByPk($hdrData['id']);
        if($delOrdHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('DelOrd.doc_status_is_wip_or_complete', ['docCode'=>$delOrdHdr->doc_code]));
            $exc->addData(\App\DelOrdHdr::class, $delOrdHdr->id);
            throw $exc;
        }
        $delOrdHdrData = $delOrdHdr->toArray();
        //assign hdrData to model
        foreach($hdrData as $field => $value) 
        {
            if(strpos($field, 'hdr_disc_val') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_disc_perc') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_tax_val') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_tax_perc') !== false)
            {
                continue;
            }
            $delOrdHdrData[$field] = $value;
        }

		//$delOrdHdr->load('outbOrdHdr', 'deliveryPoint');
        //$deliveryPoint = $delOrdHdr->deliveryPoint;
        
        $delOrdDtlArray = array();

        $outbOrdHdrData = array();
        $outbOrdDtlArray = array();

        //check if hdr disc or hdr tax is set and more than 0
        $needToProcessDetails = false;
        $firstDtlModel = DelOrdDtlRepository::findTopByHdrId($hdrData['id']);
        if(!empty($firstDtlModel))
        {
            $firstDtlData = $firstDtlModel->toArray();
            for($a = 1; $a <= 5; $a++)
            {
                if(array_key_exists('hdr_disc_val_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_disc_val_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_disc_val_0'.$a], $firstDtlData['hdr_disc_val_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
                if(array_key_exists('hdr_disc_perc_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_disc_perc_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_disc_perc_0'.$a], $firstDtlData['hdr_disc_perc_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }

                if(array_key_exists('hdr_tax_val_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_tax_val_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_tax_val_0'.$a], $firstDtlData['hdr_tax_val_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
                if(array_key_exists('hdr_tax_perc_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_tax_perc_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_tax_perc_0'.$a], $firstDtlData['hdr_tax_perc_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
            }
        }

        if($needToProcessDetails)
        {
            //query the delOrdDtlModels from DB
            //format to array, with is_modified field to indicate it is changed
            $delOrdDtlModels = DelOrdDtlRepository::findAllByHdrId($hdrData['id']);
            foreach($delOrdDtlModels as $delOrdDtlModel)
            {
                $delOrdDtlData = $delOrdDtlModel->toArray();
                for($a = 1; $a <= 5; $a++)
                {
                    if(array_key_exists('hdr_disc_val_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_disc_val_0'.$a], 0, 5) > 0)
                    {
                        $delOrdDtlData['hdr_disc_val_0'.$a] = $hdrData['hdr_disc_val_0'.$a];
                    }
                    if(array_key_exists('hdr_disc_perc_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_disc_perc_0'.$a], 0, 5) > 0)
                    {
                        $delOrdDtlData['hdr_disc_perc_0'.$a] = $hdrData['hdr_disc_perc_0'.$a];
                    }

                    if(array_key_exists('hdr_tax_val_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_tax_val_0'.$a], 0, 5) > 0)
                    {
                        $delOrdDtlData['hdr_tax_val_0'.$a] = $hdrData['hdr_tax_val_0'.$a];
                    }
                    if(array_key_exists('hdr_tax_perc_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_tax_perc_0'.$a], 0, 5) > 0)
                    {
                        $delOrdDtlData['hdr_tax_perc_0'.$a] = $hdrData['hdr_tax_perc_0'.$a];
                    }
                }
                $delOrdDtlData['is_modified'] = 1;
                $delOrdDtlArray[] = $delOrdDtlData;
            }
            
            //calculate details amount, use adaptive rounding tax
            $result = $this->calculateAmount($delOrdHdrData, $delOrdDtlArray);
            $delOrdHdrData = $result['hdrData'];
            $delOrdDtlArray = $result['dtlArray'];

            //need to update the associated outbOrdHdr
            $outbOrdHdr = $delOrdHdr->outbOrdHdr;
            if(!empty($outbOrdHdr))
            {
                if($outbOrdHdr->cur_res_type == ResType::$MAP['SLS_INV'])
                {
                    $result = $this->tallyOutbOrd($delOrdHdrData, $delOrdDtlArray);
                    $outbOrdHdrData = $result['outbOrdHdrData'];
                    $outbOrdDtlArray = $result['outbOrdDtlArray'];
                }
            }
        }
            
        $result = DelOrdHdrRepository::updateDetails($delOrdHdrData, $delOrdDtlArray, array(), $outbOrdHdrData, $outbOrdDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];

        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('DelOrd.document_successfully_updated', ['docCode'=>$documentHeader->doc_code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}
	
	public function createHeader($data)
    {		
		AuthService::authorize(
			array(
				'del_ord_create'
			)
		);

        $data = self::processIncomingHeader($data, true);
        $docNoId = $data['doc_no_id'];
        unset($data['doc_no_id']);
        $ordHdrModel = DelOrdHdrRepository::createHeader(ProcType::$MAP['NULL'], $docNoId, $data);

        $message = __('DelOrd.document_successfully_created', ['docCode'=>$ordHdrModel->doc_code]);

		return array(
			'data' => $ordHdrModel->id,
			'message' => $message
		);
	}
	
	public function index($divisionId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
			array(
				'del_ord_read'
			)
		);

        //DB::connection()->enableQueryLog();
        $delOrdHdrs = DelOrdHdrRepository::findAll($divisionId, $sorts, $filters, $pageSize);
        $delOrdHdrs->load(
            'delOrdDtls'
            //'delOrdDtls.item', 'delOrdDtls.item.itemUoms', 'delOrdDtls.item.itemUoms.uom', 
        );
		foreach($delOrdHdrs as $delOrdHdr)
		{
			$delOrdHdr->str_doc_status = DocStatus::$MAP[$delOrdHdr->doc_status];

			$deliveryPoint = $delOrdHdr->deliveryPoint;
			$delOrdHdr->delivery_point_code = $deliveryPoint->code;
			$delOrdHdr->delivery_point_company_name_01 = $deliveryPoint->company_name_01;
			$delOrdHdr->delivery_point_area_code = $deliveryPoint->area_code;
			$area = $deliveryPoint->area;
			if(!empty($area))
			{
				$delOrdHdr->delivery_point_area_desc_01 = $area->desc_01;
			}

			$salesman = $delOrdHdr->salesman;
			$delOrdHdr->salesman_username = $salesman->username;

			$delOrdDtls = $delOrdHdr->delOrdDtls;
			$delOrdHdr->details = $delOrdDtls;
        }
        //Log::error(DB::getQueryLog());
    	return $delOrdHdrs;
	}
	
	static public function processIncomingDetail($data, $item)
    {
        $data['qty'] = round($data['qty'], $item->qty_scale);

        //preprocess the select2 and dropDown
        if(array_key_exists('item_select2', $data))
        {
            $data['item_id'] = $data['item_select2']['value'];
            unset($data['item_select2']);
        }
        if(array_key_exists('uom_select2', $data))
        {
            $data['uom_id'] = $data['uom_select2']['value'];
            unset($data['uom_select2']);
        }
        if(array_key_exists('item_code', $data))
        {
            unset($data['item_code']);
        }
        if(array_key_exists('uom_code', $data))
        {
            unset($data['uom_code']);
        }
        if(array_key_exists('item', $data))
        {
            unset($data['item']);
        }
        if(array_key_exists('uom', $data))
        {
            unset($data['uom']);
		}
		
		if(array_key_exists('item_code', $data))
        {
            unset($data['item_code']);
		}
		if(array_key_exists('item_ref_code_01', $data))
        {
            unset($data['item_ref_code_01']);
		}
		if(array_key_exists('item_desc_01', $data))
        {
            unset($data['item_desc_01']);
		}
		if(array_key_exists('item_desc_02', $data))
        {
            unset($data['item_desc_02']);
		}
		if(array_key_exists('storage_class', $data))
        {
            unset($data['storage_class']);
		}
		if(array_key_exists('item_group_01_code', $data))
        {
            unset($data['item_group_01_code']);
		}
		if(array_key_exists('item_group_02_code', $data))
        {
            unset($data['item_group_02_code']);
		}
		if(array_key_exists('item_group_03_code', $data))
        {
            unset($data['item_group_03_code']);
		}
		if(array_key_exists('item_group_04_code', $data))
        {
            unset($data['item_group_04_code']);
		}
		if(array_key_exists('item_group_05_code', $data))
        {
            unset($data['item_group_05_code']);
		}
		if(array_key_exists('item_cases_per_pallet_length', $data))
        {
            unset($data['item_cases_per_pallet_length']);
		}
		if(array_key_exists('item_cases_per_pallet_width', $data))
        {
            unset($data['item_cases_per_pallet_width']);
		}
		if(array_key_exists('item_no_of_layers', $data))
        {
            unset($data['item_no_of_layers']);
		}

		if(array_key_exists('unit_qty', $data))
        {
            unset($data['unit_qty']);
		}
		if(array_key_exists('item_unit_uom_code', $data))
        {
            unset($data['item_unit_uom_code']);
		}
		if(array_key_exists('loose_qty', $data))
        {
            unset($data['loose_qty']);
		}
		if(array_key_exists('item_loose_uom_code', $data))
        {
            unset($data['item_loose_uom_code']);
        }
        if(array_key_exists('loose_uom_id', $data))
        {
            unset($data['loose_uom_id']);
		}
		if(array_key_exists('loose_uom_rate', $data))
        {
            unset($data['loose_uom_rate']);
		}
		if(array_key_exists('case_qty', $data))
        {
            unset($data['case_qty']);
		}
		if(array_key_exists('case_uom_rate', $data))
        {
            unset($data['case_uom_rate']);
		}
        if(array_key_exists('item_case_uom_code', $data))
        {
            unset($data['item_case_uom_code']);
        }
        if(array_key_exists('uom_code', $data))
        {
            unset($data['uom_code']);
		}
		if(array_key_exists('item_unit_barcode', $data))
        {
            unset($data['item_unit_barcode']);
		}
		if(array_key_exists('item_case_barcode', $data))
        {
            unset($data['item_case_barcode']);
		}
		if(array_key_exists('gross_weight', $data))
        {
            unset($data['gross_weight']);
		}
		if(array_key_exists('cubic_meter', $data))
        {
            unset($data['cubic_meter']);
		}
		
		if(array_key_exists('location_code', $data))
        {
            unset($data['location_code']);
		}
		if(array_key_exists('location', $data))
        {
            unset($data['location']);
		}		
        return $data;
    }
}
