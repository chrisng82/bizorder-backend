<?php

namespace App\Services;

use App\InbOrdDtl;
use App\InbOrdHdr;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Services\Env\ResType;
use App\Services\Env\ItemType;
use App\Services\Env\BizPartnerType;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;
use App\Repositories\InbOrdHdrRepository;
use App\Repositories\InbOrdDtlRepository;
use App\Repositories\AdvShipHdrRepository;
use App\Repositories\AdvShipDtlRepository;
use App\Repositories\UserRepository;
use App\Repositories\BizPartnerRepository;
use App\Repositories\CurrencyRepository;
use App\Repositories\ItemRepository;
use App\Repositories\ItemUomRepository;
use App\Repositories\DivisionDocNoRepository;
use App\Repositories\InvTxnFlowRepository;
use App\Repositories\SlsRtnHdrRepository;
use App\Repositories\CreditTermRepository;
use App\Services\Utils\RepositoryUtils;
use Illuminate\Support\Facades\Auth;

class InbOrdService extends InventoryService
{
	public function __construct()
	{
	}

  	public function createDetail($hdrId, $data)
	{
        AuthService::authorize(
            array(
                'inb_ord_update'
            )
        );
        
		$inbOrdHdr = InbOrdHdrRepository::txnFindByPk($hdrId);
		$inbOrdHdrData = $inbOrdHdr->toArray();
		$inbOrdHdr->load('bizPartner');
		$bizPartner = $inbOrdHdr->bizPartner;

		$inbOrdDtlArray = array();
		$inbOrdDtlModels = InbOrdDtlRepository::findAllByHdrId($hdrId);
		foreach($inbOrdDtlModels as $inbOrdDtlModel)
		{
			$lastLineNo = $inbOrdDtlModel->line_no;
			$inbOrdDtlData = $inbOrdDtlModel->toArray();
			$inbOrdDtlData['is_modified'] = 0;
			$inbOrdDtlArray[] = $inbOrdDtlData;
		}
		$lastLineNo = count($inbOrdDtlModels);

		//assign temp id
		$tmpUuid = uniqid();
		//append NEW here to make sure it will be insert in repository later
		$data['id'] = 'NEW'.$tmpUuid;
		$data['line_no'] = $lastLineNo + 1;
		$data['is_modified'] = 1;
		$data = $this->updatePurchaseItemUom($data, $inbOrdHdr['doc_date'], $bizPartner->price_group_id, $inbOrdHdr['currency_id'], 0, 0);
		$inbOrdDtlData = $this->initInbOrdDtlData($data);
		$inbOrdDtlArray[] = $inbOrdDtlData;

		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($inbOrdHdrData, $inbOrdDtlArray);
		$inbOrdHdrData = $result['hdrData'];
		$inbOrdDtlArray = $result['dtlArray'];		

		$result = InbOrdHdrRepository::updateDetails($inbOrdHdrData, $inbOrdDtlArray, array());
		return $result;
  	}
    
  	public function createHeader($procType, $divisionId, $data)
	{
        AuthService::authorize(
            array(
                'inb_ord_create'
            )
        );

		$divisionDocNo = DivisionDocNoRepository::findSiteDocNo($divisionId, \App\InbOrdHdr::class);
		if(empty($divisionDocNo))
		{
			$exc = new ApiException(__('DivisionDocNo.doc_no_not_found', ['divisionId'=>$divisionId, 'docType'=>\App\InbOrdHdr::class]));
			//$exc->addData(\App\DivisionDocNo::class, $divisionId);
			throw $exc;
		}

		$inbOrdHdr = InbOrdHdrRepository::createHeader($procType, $divisionDocNo->doc_no_id, $inbOrdHdrData);
		return $inbOrdHdr;
  	}
    
  	public function deleteDetails($hdrId, $dtlArray)
	{
        AuthService::authorize(
            array(
                'inb_ord_update'
            )
        );

		//get the associated inbOrdHdr
		$inbOrdHdr = InbOrdHdrRepository::txnFindByPk($hdrId);
		$inbOrdHdrData = $inbOrdHdr->toArray();
		$inbOrdHdr->load('inbOrdHdr', 'bizPartner');
		$bizPartner = $inbOrdHdr->bizPartner;

		//query the inbOrdDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$delInbOrdDtlArray = array();
		$inbOrdDtlArray = array();
		$inbOrdDtlModels = InbOrdDtlRepository::findAllByHdrId($hdrId);
		foreach($inbOrdDtlModels as $inbOrdDtlModel)
		{
			$inbOrdDtlData = $inbOrdDtlModel->toArray();
			$inbOrdDtlData['is_modified'] = 0;
			$inbOrdDtlData['is_deleted'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{
				if($dtlData['id'] == $inbOrdDtlData['id'])
				{
					//this is deleted dtl
					$inbOrdDtlData['is_deleted'] = 1;
					break;
				}
			}
			if($inbOrdDtlData['is_deleted'] > 0)
			{
				$delInbOrdDtlArray[] = $inbOrdDtlData;
			}
			else
			{
				$inbOrdDtlArray[] = $inbOrdDtlData;
			}
		}
		
		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($inbOrdHdrData, $inbOrdDtlArray, $delInbOrdDtlArray);
		$inbOrdHdrData = $result['hdrData'];
		$modifiedDtlArray = $result['dtlArray'];
		foreach($inbOrdDtlArray as $inbOrdDtlSeq => $inbOrdDtlData)
		{
			foreach($modifiedDtlArray as $modifiedDtlData)
			{
				if($modifiedDtlData['id'] == $inbOrdDtlData['id'])
				{
					foreach($modifiedDtlData as $field => $value)
					{
						$inbOrdDtlData[$field] = $value;
					}
					$inbOrdDtlData['is_modified'] = 1;
					$inbOrdDtlArray[$inbOrdDtlSeq] = $inbOrdDtlData;
					break;
				}
			}
		}
		
		$result = InbOrdHdrRepository::updateDetails($inbOrdHdrData, $inbOrdDtlArray, $delInbOrdDtlArray);
		return $result;
  	}
    
	public function updateDetails($hdrId, $dtlArray)
	{
        AuthService::authorize(
            array(
                'inb_ord_update'
            )
        );

		//get the associated inbOrdHdr
		$inbOrdHdr = InbOrdHdrRepository::txnFindByPk($hdrId);
		$inbOrdHdrData = $inbOrdHdr->toArray();
		$inbOrdHdr->load('bizPartner');
		$bizPartner = $inbOrdHdr->bizPartner;

		//query the inbOrdDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$inbOrdDtlArray = array();
		$inbOrdDtlModels = InbOrdDtlRepository::findAllByHdrId($hdrId);
		foreach($inbOrdDtlModels as $inbOrdDtlModel)
		{
			$inbOrdDtlData = $inbOrdDtlModel->toArray();
			$inbOrdDtlData['is_modified'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{
				if($dtlData['id'] == $inbOrdDtlData['id'])
				{
					$dtlData = $this->updatePurchaseItemUom($dtlData, $inbOrdHdr->doc_date, 0, $inbOrdHdr->currency_id, 0, 0);
					foreach($dtlData as $fieldName => $value)
					{
						$inbOrdDtlData[$fieldName] = $value;
					}
					$inbOrdDtlData['is_modified'] = 1;

					break;
				}
			}
			$inbOrdDtlArray[] = $inbOrdDtlData;
		}
		
		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($inbOrdHdrData, $inbOrdDtlArray);
		$inbOrdHdrData = $result['hdrData'];
		$inbOrdDtlArray = $result['dtlArray'];

		$result = InbOrdHdrRepository::updateDetails($inbOrdHdrData, $inbOrdDtlArray, array());
		return $result;
	}

	public function testCreateDocuments($siteFlowId, $companyId, $divisionId)
	{
		$numOfDocs = mt_rand(1, 3);

		$ordHdrModels = array();

		$divisionDocNo = DivisionDocNoRepository::findDivisionDocNo($divisionId, \App\InbOrdHdr::class);
		if(empty($divisionDocNo))
		{
			$exc = new ApiException(__('DivisionDocNo.doc_no_not_found', ['divisionId'=>$divisionId, 'docType'=>\App\InbOrdHdr::class]));
			$exc->addData(\App\DivisionDocNo::class, $divisionId);
			throw $exc;
		}
		
		for($a = 0; $a < $numOfDocs; $a++)
		{
			//Start point of our date range.
			$start = strtotime("-5 day");
			//End point of our date range.
			$end = strtotime("+5 day");
			$timestamp = mt_rand($start, $end);
			$docDate = date('Y-m-d', $timestamp);
			
			$purchaser = UserRepository::findRandom();
			$bizPartner = BizPartnerRepository::findRandomByType(BizPartnerType::$MAP['CREDITOR']);
			$currency = CurrencyRepository::findRandom();

			$ordHdr = array();
			$ordHdr['sign'] = 1;
			$ordHdr['cur_res_type'] = ResType::$MAP['PUR_ORD'];
			$ordHdr['ref_code_01'] = '';
			$ordHdr['ref_code_02'] = '';
			$ordHdr['doc_date'] = $docDate;
			$ordHdr['desc_01'] = '';
			$ordHdr['desc_02'] = '';
			$ordHdr['site_flow_id'] = $siteFlowId;
			$ordHdr['company_id'] = $companyId;
			$ordHdr['division_id'] = $divisionId;
			$ordHdr['purchaser_id'] = $purchaser->id;
			$ordHdr['biz_partner_id'] = $bizPartner->id;
			$ordHdr['est_del_date'] = $docDate;
			$ordHdr['currency_id'] = $currency->id;
			$ordHdr['currency_rate'] = $currency->currency_rate;
			$ordHdrModel = InbOrdHdrRepository::createHeader(ProcType::$MAP['INB_ORD_01'], $divisionDocNo->doc_no_id, $ordHdr);

			$numOfDtls = mt_rand(2, 5);

			for($b = 0; $b < $numOfDtls; $b++)
			{
				$item = ItemRepository::findRandomByType(ItemType::$MAP['STOCK_ITEM']);
				$itemUom = ItemUomRepository::findRandomByItemId($item->id);

				$ordDtl['line_no'] = $b + 1;
				$ordDtl['item_id'] = $item->id;
				$ordDtl['desc_01'] = $item->desc_01;
				$ordDtl['desc_02'] = $item->desc_02;
				$ordDtl['uom_id'] = $itemUom->uom_id;
				$ordDtl['uom_rate'] = $itemUom->uom_rate;
				$ordDtl['qty'] = mt_rand(100, 500);

				$result = $this->createDetail($ordHdrModel->id, $ordDtl);
				$ordHdrModel = $result['hdrModel'];
			}
			$ordHdrModel = self::transitionToComplete($ordHdrModel->id);

			$ordHdrModels[] = $ordHdrModel;
		}
        
		return $ordHdrModels;
	}
	  
	static public function transitionToComplete($hdrId)
	{
        AuthService::authorize(
            array(
                'inb_ord_confirm'
            )
        );

		$isSuccess = false;
		//use transaction to make sure this is latest value
		$inbOrdHdr = InbOrdHdrRepository::txnFindByPk($hdrId);

		//only DRAFT or above can transition to COMPLETE
		if($inbOrdHdr->doc_status >= DocStatus::$MAP['COMPLETE']
		|| $inbOrdHdr->doc_status < DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('InbOrd.doc_status_is_not_draft_or_wip', ['docCode'=>$inbOrdHdr->doc_code]));
			$exc->addData(\App\InbOrdHdr::class, $inbOrdHdr->id);
			throw $exc;
		}
  
		//commit the document
		$hdrModel = InbOrdHdrRepository::commitToComplete($inbOrdHdr->id, true);
		if(!empty($hdrModel))
		{
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		
		$exc = new ApiException(__('InbOrd.commit_to_complete_error', ['docCode'=>$inbOrdHdr->doc_code]));
		$exc->addData(\App\InbOrdHdr::class, $inbOrdHdr->id);
		throw $exc;
	}

	public function indexProcess($strProcType, $divisionId, $sorts, $filters = array(), $pageSize = 20) 
	{       
		if(isset(ProcType::$MAP[$strProcType]))
		{ 
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['INB_ORD_01']) 
			{
				//ADV_SHIP -> INB_ORD
				$advShipHdrs = $this->indexInbOrd01($divisionId, $sorts, $filters, $pageSize);
				return $advShipHdrs;
			}
			elseif (ProcType::$MAP[$strProcType] == ProcType::$MAP['INB_ORD_02']) {
				//sales return -> inbound order, can issue partially
				$slsRtnHdrs = $this->indexInbOrd02($divisionId, $sorts, $filters, $pageSize);
				return $slsRtnHdrs;
			}
		}
    }

    protected function indexInbOrd01($divisionId, $sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
            array(
                'adv_ship_read'
            )
        );

		//ADV_SHIP -> INB_ORD
		$advShipHdrs = AdvShipHdrRepository::findAllNotExistInbOrd01Txn($divisionId, $sorts, $filters, $pageSize);
        $advShipHdrs->load('division', 'bizPartner', 'purchaser');
		//query the docDtls, and format into group details
		foreach($advShipHdrs as $advShipHdr)
		{
			$advShipHdr->str_doc_status = DocStatus::$MAP[$advShipHdr->doc_status];

			$division = $advShipHdr->division;
			$advShipHdr->division_code = $division->code;

			$bizPartner = $advShipHdr->bizPartner;
			$advShipHdr->biz_partner_code = $bizPartner->code;
			$advShipHdr->biz_partner_company_name_01 = $bizPartner->company_name_01;

			$purchaser = $advShipHdr->purchaser;
			$advShipHdr->purchaser_username = $purchaser->username;

			$advShipDtls = AdvShipDtlRepository::queryShipmentDetailsGroupByItemId($advShipHdr->id);
			//calculate the case_qty, gross_weight and cubic_meter
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			foreach($advShipDtls as $advShipDtl)
			{
				$advShipDtl->case_qty = round($advShipDtl->case_qty, 8);
				$advShipDtl->gross_weight = round($advShipDtl->gross_weight, 8);
				$advShipDtl->cubic_meter = round($advShipDtl->cubic_meter, 8);
				$caseQty = bcadd($caseQty, $advShipDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $advShipDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $advShipDtl->cubic_meter, 8);
			}

			$advShipHdr->case_qty = $caseQty;
			$advShipHdr->gross_weight = $grossWeight;
			$advShipHdr->cubic_meter = $cubicMeter;
			$advShipHdr->details = $advShipDtls;

			//unset the item, so it wont send in json
			unset($advShipHdr->division);
			unset($advShipHdr->creditor);
			unset($advShipHdr->purchaser);
		}
        return $advShipHdrs;
	}
	
	public function createProcess($strProcType, $hdrIds) 
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['INB_ORD_01'])
			{
				$result = $this->createInbOrd01(ProcType::$MAP[$strProcType], $hdrIds);
				return $result;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['INB_ORD_02'])
			{
				$result = $this->createInbOrd02(ProcType::$MAP[$strProcType], $hdrIds);
				return $result;
			}
		}
	}

	protected function createInbOrd01($procType, $hdrIds)
    {
        AuthService::authorize(
            array(
                'inb_ord_create'
            )
        );

		$inbOrdHdrs = array();
        //1) 1 AdvShip -> 1 InbOrd
		$advShipHdrs = AdvShipHdrRepository::findAllByHdrIds($hdrIds);
		$advShipHdrs->load('division');
        //2) loop for each advShip
        foreach($advShipHdrs as $advShipHdr)
        {
            //2.1) variables to store the inbOrd
			$hdrData = array();
			//hash by frDocDtlId
			$dtlDataHashByDtlId = array();
			//hash by frDocDtlId, match with dtlDataHashByDtlId
			$frDocDtlModelHashByDtlId = array();
            $docTxnFlowDataList = array();
            $division = $advShipHdr->division;
            $lineNo = 1;

            //2.2) get the details of this advShip
            $advShipDtls = AdvShipDtlRepository::findAllByHdrId($advShipHdr->id);
            foreach($advShipDtls as $advShipDtl)
            {
				$dtlData = array();
				$dtlData = RepositoryUtils::modelToData($dtlData, $advShipDtl);
				unset($dtlData['inb_ord_hdr_id']);
				unset($dtlData['inb_ord_dtl_id']);

				$dtlDataHashByDtlId[$advShipDtl->id] = $dtlData;
				$frDocDtlModelHashByDtlId[$advShipDtl->id] = $advShipDtl;
    
                $lineNo++;
            }
            
			//2.7) build the hdrData
            $hdrData = array(			
				'pur_ord_hdr_id' => 0,
				'pur_ord_hdr_code' => '',
				'adv_ship_hdr_id' => $advShipHdr->id,
				'adv_ship_hdr_code' => $advShipHdr->doc_code,
				'pur_inv_hdr_id' => 0,
				'pur_inv_hdr_code' => '',
				//1 = inbound receipt documents (purInvHdr), -1 = inbound delivery documents (pur debit note), 0 = advShipHdr/purchase order/purchase return
				'sign' => 0,
				'cur_res_type' => ResType::$MAP['ADV_SHIP'],
			);
			$hdrData = RepositoryUtils::modelToData($hdrData, $advShipHdr);
			unset($hdrData['inb_ord_hdr_id']);
			unset($hdrData['division']);

            $tmpDocTxnFlowData = array(
                'fr_doc_hdr_type' => \App\AdvShipHdr::class,
                'fr_doc_hdr_id' => $advShipHdr->id,
                'fr_doc_hdr_code' => $advShipHdr->doc_code,
                'is_closed' => 1
            );
            $docTxnFlowDataList[] = $tmpDocTxnFlowData;

            //2.8) get the inbOrdHdr doc no
            $divisionDocNo = DivisionDocNoRepository::findDivisionDocNo($division->id, \App\InbOrdHdr::class);
            if(empty($divisionDocNo))
            {
                $exc = new ApiException(__('DivisionDocNo.doc_no_not_found', ['divisionId'=>$division->id, 'docType'=>\App\InbOrdHdr::class]));
                //$exc->addData(\App\DivisionDocNo::class, $division->id);
                throw $exc;
            }

            //2.9) query the invTxnFlow, so know the next step
            $invTxnFlow = InvTxnFlowRepository::findByPk($division->id, $procType);
            
            //2.10) create the inbOrdHdr
            $inbOrdHdr = InbOrdHdrRepository::createProcess($procType, $divisionDocNo->doc_no_id, $hdrData, $dtlDataHashByDtlId, $advShipHdr, $frDocDtlModelHashByDtlId, $docTxnFlowDataList);
            
            //2.11) commit inbOrdHdr to WIP/Complete according to invTxnFlow
            if($invTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
            {
                $inbOrdHdr = self::transitionToWip($inbOrdHdr->id);
            }
            elseif($invTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
            {
                $inbOrdHdr = self::transitionToComplete($inbOrdHdr->id);
            }
            
            $inbOrdHdrs[] = $inbOrdHdr;
        }

        $docCodeMsg = '';
        for ($a = 0; $a < count($inbOrdHdrs); $a++)
        {
            $inbOrdHdr = $inbOrdHdrs[$a];
            if($a == 0)
            {
                $docCodeMsg .= $inbOrdHdr->doc_code;
            }
            else
            {
                $docCodeMsg .= ', '.$inbOrdHdr->doc_code;
            }
        }
        $message = __('InbOrd.document_successfully_created', ['docCode'=>$docCodeMsg]);

        return array(
            'data' => $inbOrdHdrs,
            'message' => $message
        );
	}

	protected function indexInbOrd02($divisionId, $sorts, $filters = array(), $pageSize = 20)
    {
        AuthService::authorize(
            array(
                'sls_rtn_read'
            )
        );

		//SLS_RTN -> INB_ORD
		//DB::connection()->enableQueryLog();
		$slsRtnHdrs = SlsRtnHdrRepository::findAllNotExistInbOrd02Txn($divisionId, $sorts, $filters, $pageSize);
		//Log::error(DB::getQueryLog());
		$slsRtnHdrs->load(
			'siteFlow',
			'slsRtnDtls', 'slsRtnDtls.item',
			'division', 'deliveryPoint', 'deliveryPoint.area', 'salesman'
		);
		//query the docDtls, and format into group details
		foreach($slsRtnHdrs as $slsRtnHdr)
		{
			$siteFlow = $slsRtnHdr->siteFlow;
			$slsRtnHdr->str_doc_status = DocStatus::$MAP[$slsRtnHdr->doc_status];

			$division = $slsRtnHdr->division;
			$slsRtnHdr->division_code = $division->code;

			$deliveryPoint = $slsRtnHdr->deliveryPoint;
			$slsRtnHdr->delivery_point_code = $deliveryPoint->code;
			$slsRtnHdr->delivery_point_company_name_01 = $deliveryPoint->company_name_01;
			$slsRtnHdr->delivery_point_area_code = $deliveryPoint->area_code;
			$area = $deliveryPoint->area;
			if(!empty($area))
			{
				$slsRtnHdr->delivery_point_area_desc_01 = $area->desc_01;
			}

			$salesman = $slsRtnHdr->salesman;
			$slsRtnHdr->salesman_username = $salesman->username;

			$slsRtnDtls = $slsRtnHdr->slsRtnDtls;
			//calculate the case_qty, gross_weight and cubic_meter
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			foreach($slsRtnDtls as $slsRtnDtl)
			{
				$slsRtnDtl = SlsRtnService::processOutgoingDetail($slsRtnDtl);

				$slsRtnDtl->case_qty = round($slsRtnDtl->case_qty, 8);
				$slsRtnDtl->gross_weight = round($slsRtnDtl->gross_weight, 8);
				$slsRtnDtl->cubic_meter = round($slsRtnDtl->cubic_meter, 8);
				$caseQty = bcadd($caseQty, $slsRtnDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $slsRtnDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $slsRtnDtl->cubic_meter, 8);
			}

			$slsRtnHdr->case_qty = $caseQty;
			$slsRtnHdr->gross_weight = $grossWeight;
			$slsRtnHdr->cubic_meter = $cubicMeter;
			$slsRtnHdr->details = $slsRtnDtls;

			//unset the item, so it wont send in json
			unset($slsRtnHdr->division);
			unset($slsRtnHdr->debtor);
			unset($slsRtnHdr->salesman);
		}
        return $slsRtnHdrs;
	}
	
	protected function createInbOrd02($procType, $hdrIds)
    {
        AuthService::authorize(
            array(
                'inb_ord_create'
            )
        );

		$inbOrdHdrs = array();
        //1) 1 SlsRtn -> 1 InbOrd
		$slsRtnHdrs = SlsRtnHdrRepository::findAllByHdrIds($hdrIds);
		$slsRtnHdrs->load('siteFlow', 'division', 'deliveryPoint', 'slsRtnDtls', 'slsRtnDtls.item');
        //2) loop for each slsRtnHdr
        foreach($slsRtnHdrs as $slsRtnHdr)
        {
            //2.1) variables to store the inbOrd
			$hdrData = array();
			//hash by frDocDtlId
			$dtlDataHashByDtlId = array();
			//hash by frDocDtlId, match with dtlDataHashByDtlId
			$frDocDtlModelHashByDtlId = array();
            $docTxnFlowDataList = array();
            $division = $slsRtnHdr->division;
			$lineNo = 1;
			
			$deliveryPoint = $slsRtnHdr->deliveryPoint;
			$siteFlow = $slsRtnHdr->siteFlow;
			
            //2.2) get the details of this slsRtn
			$slsRtnDtls = $slsRtnHdr->slsRtnDtls;
			$slsRtnDtls = $slsRtnDtls->sortBy('line_no');
            foreach($slsRtnDtls as $slsRtnDtl)
            {				
				$item = $slsRtnDtl->item;
				
				$dtlData = array();
				unset($slsRtnDtl->item);
				$dtlData = RepositoryUtils::modelToData($dtlData, $slsRtnDtl);
				unset($dtlData['inb_ord_hdr_id']);
				unset($dtlData['inb_ord_dtl_id']);

				$dtlDataHashByDtlId[$slsRtnDtl->id] = $dtlData;
				$frDocDtlModelHashByDtlId[$slsRtnDtl->id] = $slsRtnDtl;
    
				$lineNo++;
            }
			
			//2.7) build the hdrData
            $hdrData = array(
				'sls_rtn_hdr_id' => $slsRtnHdr->id,
				'sls_rtn_hdr_code' => $slsRtnHdr->doc_code,
				'rtn_rcpt_hdr_id' => 0,
				'rtn_rcpt_hdr_code' => '',
				'sls_cn_hdr_id' => 0,
				'sls_cn_hdr_code' => '',
				'sign' => 0,
				'cur_res_type' => ResType::$MAP['SLS_RTN'],
			);
			unset($slsRtnHdr->siteFlow);
			unset($slsRtnHdr->division);
			unset($slsRtnHdr->deliveryPoint);
			unset($slsRtnHdr->slsRtnDtls);
			$hdrData = RepositoryUtils::modelToData($hdrData, $slsRtnHdr);
			unset($hdrData['inb_ord_hdr_id']);
			unset($hdrData['debtor_id']);

            $tmpDocTxnFlowData = array(
                'fr_doc_hdr_type' => \App\SlsRtnHdr::class,
                'fr_doc_hdr_id' => $slsRtnHdr->id,
                'fr_doc_hdr_code' => $slsRtnHdr->doc_code,
                'is_closed' => 1
            );
            $docTxnFlowDataList[] = $tmpDocTxnFlowData;

            //2.8) get the inbOrdHdr doc no
            $divisionDocNo = DivisionDocNoRepository::findDivisionDocNo($division->id, \App\InbOrdHdr::class);
            if(empty($divisionDocNo))
            {
                $exc = new ApiException(__('DivisionDocNo.doc_no_not_found', ['divisionId'=>$division->id, 'docType'=>\App\InbOrdHdr::class]));
                //$exc->addData(\App\DivisionDocNo::class, $division->id);
                throw $exc;
            }

            //2.9) query the invTxnFlow, so know the next step
            $invTxnFlow = InvTxnFlowRepository::findByPk($division->id, $procType);

			//2.10) create the inbOrdHdr
            $inbOrdHdr = InbOrdHdrRepository::createProcess($procType, $divisionDocNo->doc_no_id, $hdrData, $dtlDataHashByDtlId, $slsRtnHdr, $frDocDtlModelHashByDtlId, $docTxnFlowDataList);

            //2.11) commit inbOrdHdr to WIP/Complete according to invTxnFlow
            if($invTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
            {
                $inbOrdHdr = self::transitionToWip($inbOrdHdr->id);
            }
            elseif($invTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
            {
                $inbOrdHdr = self::transitionToComplete($inbOrdHdr->id);
            }
            
            $inbOrdHdrs[] = $inbOrdHdr;
        }

        $docCodeMsg = '';
        for ($a = 0; $a < count($inbOrdHdrs); $a++)
        {
            $inbOrdHdr = $inbOrdHdrs[$a];
            if($a == 0)
            {
                $docCodeMsg .= $inbOrdHdr->doc_code;
            }
            else
            {
                $docCodeMsg .= ', '.$inbOrdHdr->doc_code;
            }
        }
        $message = __('InbOrd.document_successfully_created', ['docCode'=>$docCodeMsg]);

        return array(
            'data' => $inbOrdHdrs,
            'message' => $message
        );
	}

	public function transitionToStatus($hdrId, $strDocStatus)
    {
        $hdrModel = null;
        if(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['WIP'])
        {
            $hdrModel = self::transitionToWip($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['COMPLETE'])
        {
            $hdrModel = self::transitionToComplete($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['DRAFT'])
        {
            $hdrModel = self::transitionToDraft($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['VOID'])
        {
            $hdrModel = self::transitionToVoid($hdrId);
        }
        
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $message = __('InbOrd.document_successfully_transition', ['docCode'=>$documentHeader->doc_code,'strDocStatus'=>$strDocStatus]);
        
        return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>array(),
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }

    static public function processOutgoingHeader($model, $isShowPrint = false)
	{
        $docFlows = self::processDocFlows($model);
        $model->doc_flows = $docFlows;

        $division = $model->division;
        $model->division_code = $division->code;
        $company = $model->company;
        $model->company_code = $company->code;

		if($isShowPrint)
		{
			$printDocTxn = PrintDocTxnRepository::queryPrintDocTxn(InbOrdHdr::class, $model->id);
			$model->print_count = $printDocTxn->print_count;
			$model->first_printed_at = $printDocTxn->first_printed_at;
			$model->last_printed_at = $printDocTxn->last_printed_at;
		}

        $model->str_doc_status = DocStatus::$MAP[$model->doc_status];
        
        //currency select2
        $initCurrencyOption = array('value'=>0,'label'=>'');
        $currency = CurrencyRepository::findByPk($model->currency_id);
        if(!empty($currency))
        {
            $initCurrencyOption = array('value'=>$currency->id, 'label'=>$currency->symbol);
        }
        $model->currency_select2 = $initCurrencyOption;

        //creditTerm select2
        $initCreditTermOption = array('value'=>0,'label'=>'');
        $creditTerm = CreditTermRepository::findByPk($model->credit_term_id);
        if(!empty($creditTerm))
        {
            $initCreditTermOption = array('value'=>$creditTerm->id, 'label'=>$creditTerm->code);
        }
        $model->credit_term_select2 = $initCreditTermOption;

        //salesman select2
        $salesman = $model->salesman;
        $initSalesmanOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($salesman))
        {
            $initSalesmanOption = array(
                'value'=>$salesman->id,
                'label'=>$salesman->username
            );
        }
        $model->salesman_select2 = $initSalesmanOption;

        //biz partner select2
        $deliveryPoint = $model->deliveryPoint;
        $initDeliveryPointOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($deliveryPoint))
        {
            $model->delivery_point_unit_no = $deliveryPoint->unit_no;
            $model->delivery_point_building_name = $deliveryPoint->building_name;
            $model->delivery_point_street_name = $deliveryPoint->street_name;
            $model->delivery_point_district_01 = $deliveryPoint->district_01;
            $model->delivery_point_district_02 = $deliveryPoint->district_02;
            $model->delivery_point_postcode = $deliveryPoint->postcode;
            $model->delivery_point_state_name = $deliveryPoint->state_name;
            $model->delivery_point_country_name = $deliveryPoint->country_name;
            $initDeliveryPointOption = array(
                'value'=>$deliveryPoint->id,
                'label'=>$deliveryPoint->code.' '.$deliveryPoint->company_name_01
            );
        }
        $model->delivery_point_select2 = $initDeliveryPointOption;

        $model->hdr_disc_val_01 = 0;
        $model->hdr_disc_perc_01 = 0;
        $model->hdr_disc_val_02 = 0;
        $model->hdr_disc_perc_02 = 0;
        $model->hdr_disc_val_03 = 0;
        $model->hdr_disc_perc_03 = 0;
        $model->hdr_disc_val_04 = 0;
        $model->hdr_disc_perc_04 = 0;
        $model->hdr_disc_val_05 = 0;
        $model->hdr_disc_perc_05 = 0;
        $firstDtlModel = InbOrdDtlRepository::findTopByHdrId($model->id);
        if(!empty($firstDtlModel))
        {
            $model->hdr_disc_val_01 = $firstDtlModel->hdr_disc_val_01;
            $model->hdr_disc_perc_01 = $firstDtlModel->hdr_disc_perc_01;
            $model->hdr_disc_val_02 = $firstDtlModel->hdr_disc_val_02;
            $model->hdr_disc_perc_02 = $firstDtlModel->hdr_disc_perc_02;
            $model->hdr_disc_val_03 = $firstDtlModel->hdr_disc_val_03;
            $model->hdr_disc_perc_03 = $firstDtlModel->hdr_disc_perc_03;
            $model->hdr_disc_val_04 = $firstDtlModel->hdr_disc_val_04;
            $model->hdr_disc_perc_04 = $firstDtlModel->hdr_disc_perc_04;
            $model->hdr_disc_val_05 = $firstDtlModel->hdr_disc_val_05;
            $model->hdr_disc_perc_05 = $firstDtlModel->hdr_disc_perc_05;
        }

		return $model;
    }
    
    static public function processOutgoingDetail($model)
    {
        $item = $model->item;
        $model->item_code = $item->code;
        //$model->item_desc_01 = $item->desc_01;
        //$model->item_desc_02 = $item->desc_02;
        //item select2
        $initItemOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($item))
        {
            $initItemOption = array(
                'value'=>$item->id, 
                'label'=>$item->code.' '.$item->desc_01,
            );
        }
        $model->item_select2 = $initItemOption;

        $uom = $model->uom;
		//uom select2
		$model->uom_code = '';
        $initUomOption = array(
            'value'=>0,
            'label'=>''
		);
        if(!empty($uom))
        {
			$model->uom_code = $uom->code;
            $initUomOption = array('value'=>$uom->id,'label'=>$uom->code);
        }        
        $model->uom_select2 = $initUomOption;

        $model = ItemService::processCaseLoose($model, $item);

        return $model;
	}

    static public function transitionToWip($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = InbOrdHdrRepository::txnFindByPk($hdrId);
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status >= DocStatus::$MAP['WIP'])
		{
			$exc = new ApiException(__('InbOrd.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\InbOrdHdr::class, $hdrModel->id);
            throw $exc;
		}

		//commit the document
		$hdrModel = InbOrdHdrRepository::commitToWip($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('InbOrd.commit_to_wip_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\InbOrdHdr::class, $hdrModel->id);
        throw $exc;
	}

    static public function transitionToDraft($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = InbOrdHdrRepository::txnFindByPk($hdrId);

		//only WIP or COMPLETE can transition to DRAFT
		if($hdrModel->doc_status == DocStatus::$MAP['WIP'])
		{
            $hdrModel = InbOrdHdrRepository::revertWipToDraft($hdrModel->id);
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		elseif($hdrModel->doc_status == DocStatus::$MAP['COMPLETE'])
		{
            AuthService::authorize(
                array(
                    'inb_ord_revert'
                )
            );

            $hdrModel = InbOrdHdrRepository::revertCompleteToDraft($hdrModel->id);
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
        }
        elseif($hdrModel->doc_status == DocStatus::$MAP['VOID'])
		{
            AuthService::authorize(
                array(
                    'inb_ord_confirm'
                )
            );

			$hdrModel = InbOrdHdrRepository::commitVoidToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
        
        $exc = new ApiException(__('InbOrd.doc_status_is_not_wip_or_complete', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\InbOrdHdr::class, $hdrModel->id);
		throw $exc;
    }

    static public function transitionToVoid($hdrId)
    {
        AuthService::authorize(
            array(
                'inb_ord_revert'
            )
        );

		//use transaction to make sure this is latest value
		$hdrModel = InbOrdHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('InbOrd.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\InbOrdHdr::class, $hdrModel->id);
            throw $exc;
		}

		//revert the document
		$hdrModel = InbOrdHdrRepository::revertDraftToVoid($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('InbOrd.commit_to_void_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\InbOrdHdr::class, $hdrModel->id);
        throw $exc;
    }

    public function initHeader($divisionId)
    {
        AuthService::authorize(
            array(
                'inb_ord_create'
            )
        );

        $user = Auth::user();

        $model = new InbOrdHdr();
        $model->doc_flows = array();
        $model->doc_status = DocStatus::$MAP['DRAFT'];
        $model->str_doc_status = DocStatus::$MAP[$model->doc_status];
        $model->doc_code = '';
        $model->ref_code_01 = '';
        $model->ref_code_02 = '';
        $model->ref_code_03 = '';
        $model->ref_code_04 = '';
        $model->ref_code_05 = '';
        $model->ref_code_06 = '';
        $model->doc_date = date('Y-m-d');
        $model->est_del_date = date('Y-m-d');
        $model->desc_01 = '';
        $model->desc_02 = '';

        $division = DivisionRepository::findByPk($divisionId);
        $model->division_id = $divisionId;
        $model->division_code = '';
        $model->site_flow_id = 0;
        $model->company_id = 0;
        $model->company_code = '';
        if(!empty($division))
        {
            $model->division_code = $division->code;
            $model->site_flow_id = $division->site_flow_id;
            $company = $division->company;
            $model->company_id = $company->id;
            $model->company_code = $company->code;
        }

        $model->doc_no_id = 0;
        $docNoIdOptions = array();
        $docNos = DocNoRepository::findAllDivisionDocNo($divisionId, \App\InbOrdHdr::class, $model->doc_date);
        for($a = 0; $a < count($docNos); $a++)
        {
            $docNo = $docNos[$a];
            if($a == 0)
            {
                $model->doc_no_id = $docNo->id;
            }
            $docNoIdOptions[] = array('value'=>$docNo->id, 'label'=>$docNo->latest_code);
        }
        $model->doc_no_id_options = $docNoIdOptions;

        //currency dropdown
        $model->currency_id = 0;
        $model->currency_rate = 1;
        $initCurrencyOption = array('value'=>0,'label'=>'');
        $model->currency_select2 = $initCurrencyOption;

        //creditTerm select2
        $initCreditTermOption = array('value'=>0,'label'=>'');
        $model->credit_term_select2 = $initCreditTermOption;

        //salesman select2
        $initSalesmanOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($user))
        {
            $initSalesmanOption = array(
                'value'=>$user->id, 
                'label'=>$user->username
            );
        }
        $model->salesman_select2 = $initSalesmanOption;

        //delivery_point select2
        
        $model->delivery_point_unit_no = '';
        $model->delivery_point_building_name = '';
        $model->delivery_point_street_name = '';
        $model->delivery_point_district_01 = '';
        $model->delivery_point_district_02 = '';
        $model->delivery_point_postcode = '';
        $model->delivery_point_state_name = '';
        $model->delivery_point_country_name = '';
        $initDeliveryPointOption = array(
            'value'=>0,
            'label'=>''
        );
        $deliveryPoint = DeliveryPointRepository::findTop();
        if(!empty($deliveryPoint))
        {
            $initDeliveryPointOption = array(
                'value'=>$deliveryPoint->id, 
                'label'=>$deliveryPoint->code.' '.$deliveryPoint->company_name_01
            );

            $model->delivery_point_unit_no = $deliveryPoint->unit_no;
            $model->delivery_point_building_name = $deliveryPoint->building_name;
            $model->delivery_point_street_name = $deliveryPoint->street_name;
            $model->delivery_point_district_01 = $deliveryPoint->district_01;
            $model->delivery_point_district_02 = $deliveryPoint->district_02;
            $model->delivery_point_postcode = $deliveryPoint->postcode;
            $model->delivery_point_state_name = $deliveryPoint->state_name;
            $model->delivery_point_country_name = $deliveryPoint->country_name;
        }
        $model->delivery_point_select2 = $initDeliveryPointOption;

        $model->hdr_disc_val_01 = 0;
        $model->hdr_disc_perc_01 = 0;
        $model->hdr_disc_val_02 = 0;
        $model->hdr_disc_perc_02 = 0;
        $model->hdr_disc_val_03 = 0;
        $model->hdr_disc_perc_03 = 0;
        $model->hdr_disc_val_04 = 0;
        $model->hdr_disc_perc_04 = 0;
        $model->hdr_disc_val_05 = 0;
        $model->hdr_disc_perc_05 = 0;

        $model->disc_amt = 0;
        $model->tax_amt = 0;
        $model->round_adj_amt = 0;
        $model->net_amt = 0;

        return $model;
    }

    public function showHeader($hdrId)
    {
        AuthService::authorize(
            array(
                'inb_ord_read',
				'inb_ord_update'
            )
        );

		$model = InbOrdHdrRepository::findByPk($hdrId);
		if(!empty($model))
        {
            $model = self::processOutgoingHeader($model);
        }
		return $model;
    }

    public function updateHeader($hdrData)
	{
        AuthService::authorize(
            array(
                'inb_ord_update'
            )
        );

        $hdrData = self::processIncomingHeader($hdrData);

		//get the associated inbOrdHdr
        $inbOrdHdr = InbOrdHdrRepository::txnFindByPk($hdrData['id']);
        if($inbOrdHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('InbOrd.doc_status_is_wip_or_complete', ['docCode'=>$inbOrdHdr->doc_code]));
            $exc->addData(\App\InbOrdHdr::class, $inbOrdHdr->id);
            throw $exc;
        }
        $inbOrdHdrData = $inbOrdHdr->toArray();
        //assign hdrData to model
        foreach($hdrData as $field => $value) 
        {
            if(strpos($field, 'hdr_disc_val') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_disc_perc') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_tax_val') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_tax_perc') !== false)
            {
                continue;
            }
            $inbOrdHdrData[$field] = $value;
        }
        
        $inbOrdDtlArray = array();

        //check if hdr disc or hdr tax is set and more than 0
        $needToProcessDetails = false;
        $firstDtlModel = InbOrdDtlRepository::findTopByHdrId($hdrData['id']);
        if(!empty($firstDtlModel))
        {
            $firstDtlData = $firstDtlModel->toArray();
            for($a = 1; $a <= 5; $a++)
            {
                if(array_key_exists('hdr_disc_val_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_disc_val_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_disc_val_0'.$a], $firstDtlData['hdr_disc_val_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
                if(array_key_exists('hdr_disc_perc_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_disc_perc_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_disc_perc_0'.$a], $firstDtlData['hdr_disc_perc_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }

                if(array_key_exists('hdr_tax_val_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_tax_val_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_tax_val_0'.$a], $firstDtlData['hdr_tax_val_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
                if(array_key_exists('hdr_tax_perc_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_tax_perc_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_tax_perc_0'.$a], $firstDtlData['hdr_tax_perc_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
            }
        }

        if($needToProcessDetails)
        {
            //query the inbOrdDtlModels from DB
            //format to array, with is_modified field to indicate it is changed
            $inbOrdDtlModels = InbOrdDtlRepository::findAllByHdrId($hdrData['id']);
            foreach($inbOrdDtlModels as $inbOrdDtlModel)
            {
                $inbOrdDtlData = $inbOrdDtlModel->toArray();
                for($a = 1; $a <= 5; $a++)
                {
                    if(array_key_exists('hdr_disc_val_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_disc_val_0'.$a], 0, 5) > 0)
                    {
                        $inbOrdDtlData['hdr_disc_val_0'.$a] = $hdrData['hdr_disc_val_0'.$a];
                    }
                    if(array_key_exists('hdr_disc_perc_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_disc_perc_0'.$a], 0, 5) > 0)
                    {
                        $inbOrdDtlData['hdr_disc_perc_0'.$a] = $hdrData['hdr_disc_perc_0'.$a];
                    }

                    if(array_key_exists('hdr_tax_val_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_tax_val_0'.$a], 0, 5) > 0)
                    {
                        $inbOrdDtlData['hdr_tax_val_0'.$a] = $hdrData['hdr_tax_val_0'.$a];
                    }
                    if(array_key_exists('hdr_tax_perc_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_tax_perc_0'.$a], 0, 5) > 0)
                    {
                        $inbOrdDtlData['hdr_tax_perc_0'.$a] = $hdrData['hdr_tax_perc_0'.$a];
                    }
                }
                $inbOrdDtlData['is_modified'] = 1;
                $inbOrdDtlArray[] = $inbOrdDtlData;
            }
            
            //calculate details amount, use adaptive rounding tax
            $result = $this->calculateAmount($inbOrdHdrData, $inbOrdDtlArray);
            $inbOrdHdrData = $result['hdrData'];
            $inbOrdDtlArray = $result['dtlArray'];
        }
            
        $result = InbOrdHdrRepository::updateDetails($inbOrdHdrData, $inbOrdDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];

        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('InbOrd.document_successfully_updated', ['docCode'=>$documentHeader->doc_code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }

    public function changeDeliveryPoint($deliveryPointId)
    {
        AuthService::authorize(
            array(
                'inb_ord_update'
            )
        );

        $results = array();
        $deliveryPoint = DeliveryPointRepository::findByPk($deliveryPointId);
        if(!empty($deliveryPoint))
        {
            $results['delivery_point_unit_no'] = $deliveryPoint->unit_no;
            $results['delivery_point_building_name'] = $deliveryPoint->building_name;
            $results['delivery_point_street_name'] = $deliveryPoint->street_name;
            $results['delivery_point_district_01'] = $deliveryPoint->district_01;
            $results['delivery_point_district_02'] = $deliveryPoint->district_02;
            $results['delivery_point_postcode'] = $deliveryPoint->postcode;
            $results['delivery_point_state_name'] = $deliveryPoint->state_name;
            $results['delivery_point_country_name'] = $deliveryPoint->country_name;
        }
        return $results;
    }

    public function changeItem($hdrId, $itemId)
    {
        AuthService::authorize(
            array(
                'inb_ord_update'
            )
        );

        $inbOrdHdr = InbOrdHdrRepository::findByPk($hdrId);

        $results = array();
        $item = ItemRepository::findByPk($itemId);
        if(!empty($item)
        && !empty($inbOrdHdr))
        {
            $results['desc_01'] = $item->desc_01;
            $results['desc_02'] = $item->desc_02;
        }
        return $results;
    }

    public function changeItemUom($hdrId, $itemId, $uomId)
    {
        AuthService::authorize(
            array(
                'inb_ord_update'
            )
        );

        $inbOrdHdr = InbOrdHdrRepository::findByPk($hdrId);

        $results = array();
        $itemUom = ItemUomRepository::findByItemIdAndUomId($itemId, $uomId);
        if(!empty($itemUom)
        && !empty($inbOrdHdr))
        {
            $results['item_id'] = $itemUom->item_id;
            $results['uom_id'] = $itemUom->uom_id;
            $results['uom_rate'] = $itemUom->uom_rate;

            $results = $this->updateSaleItemUom($results, $inbOrdHdr->doc_date, 0, $inbOrdHdr->currency_id, 0, 0);
        }
        return $results;
	}
	
	public function changeCurrency($currencyId)
    {
        AuthService::authorize(
            array(
                'inb_ord_update'
            )
        );

        $results = array();
        $currency = CurrencyRepository::findByPk($currencyId);
        if(!empty($currency))
        {
            $results['currency_rate'] = $currency->currency_rate;
        }
        return $results;
    }

    public function showDetails($hdrId) 
	{
        AuthService::authorize(
            array(
                'inb_ord_read',
				'inb_ord_update'
            )
        );

		$inbOrdDtls = InbOrdDtlRepository::findAllByHdrId($hdrId);
		$inbOrdDtls->load(
            'item', 'item.itemUoms', 'item.itemUoms.uom'
        );
        foreach($inbOrdDtls as $inbOrdDtl)
        {
            $inbOrdDtl = self::processOutgoingDetail($inbOrdDtl);
        }
        return $inbOrdDtls;
    }
    
    public function index($divisionId, $sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
            array(
                'inb_ord_read'
            )
        );
        
        //DB::connection()->enableQueryLog();
        $inbOrdHdrs = InbOrdHdrRepository::findAll($divisionId, $sorts, $filters, $pageSize);
        $inbOrdHdrs->load(
            'inbOrdDtls'
            //'inbOrdDtls.item', 'inbOrdDtls.item.itemUoms', 'inbOrdDtls.item.itemUoms.uom', 
        );
		foreach($inbOrdHdrs as $inbOrdHdr)
		{
			$inbOrdHdr->str_doc_status = DocStatus::$MAP[$inbOrdHdr->doc_status];

            /*
			$bizPartner = $inbOrdHdr->bizPartner;
			$inbOrdHdr->biz_partner_code = $bizPartner->code;
			$inbOrdHdr->biz_partner_company_name_01 = $bizPartner->company_name_01;
			$inbOrdHdr->biz_partner_area_code = $bizPartner->area_code;
			$area = $bizPartner->area;
			if(!empty($area))
			{
				$inbOrdHdr->biz_partner_area_desc_01 = $area->desc_01;
			}

			$purchaser = $inbOrdHdr->purchaser;
            $inbOrdHdr->purchaser_username = $purchaser->username;
            */

			$inbOrdDtls = $inbOrdHdr->inbOrdDtls;
			$inbOrdHdr->details = $inbOrdDtls;
        }
        //Log::error(DB::getQueryLog());
    	return $inbOrdHdrs;
	}
}
