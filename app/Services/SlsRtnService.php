<?php

namespace App\Services;

use App\SlsRtnHdr;
use App\SlsRtnDtl;

use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Services\Env\ResStatus;
use App\Services\Env\ScanMode;
use App\Services\Env\RetrievalMethod;
use App\Services\Env\StorageClass;
use App\Repositories\SlsRtnHdrRepository;
use App\Repositories\SlsRtnDtlRepository;
use App\Repositories\DivisionRepository;
use App\Repositories\InvTxnFlowRepository;
use App\Repositories\QuantBalRepository;
use App\Repositories\OutbOrdHdrRepository;
use App\Repositories\BatchJobStatusRepository;
use App\Repositories\SyncSettingHdrRepository;
use App\Repositories\SyncSettingDtlRepository;
use App\Repositories\DeliveryPointRepository;
use App\Repositories\ItemRepository;
use App\Repositories\ItemUomRepository;
use App\Repositories\CurrencyRepository;
use App\Repositories\CreditTermRepository;
use App\BatchJobStatus;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;
use Milon\Barcode\DNS2D;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SlsRtnService extends InventoryService
{
    public function __construct()
    {
    }

    public function syncProcess($strProcType, $divisionId)
    {
        $user = Auth::user();

        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['SLS_RTN_SYNC_01']) 
			{
				//sync sales order from EfiChain
				return $this->syncSlsRtnSync01($divisionId, $user->id);
            }
		}
    }

    protected function syncSlsRtnSync01($divisionId, $userId)
    {
        AuthService::authorize(
            array(
                'sls_rtn_import'
            )
        );

        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['SLS_RTN_SYNC_01'], $userId);

        $syncSettingHdr = SyncSettingHdrRepository::findByProcTypeAndDivisionId(ProcType::$MAP['SYNC_EFICHAIN_01'], $divisionId);
        if(empty($syncSettingHdr))
        {
            $exc = new ApiException(__('SlsRtn.sync_setting_not_found', ['divisionId'=>$divisionId]));
            //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
            throw $exc;
        }

        $division = DivisionRepository::findByPk($divisionId);
        $company = $division->company;
        $divUrl = '&divisions[]='.$division->code;

        $client = new Client();
        $header = array();

        $page = 1;
        $lastPage = 1;
        $pageSize = 1000;
        $total = 0;

        $usernameSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'username');
        $passwordSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'password');
        $formParams = array(
            'UserLogin[username]' => $usernameSetting->value,
            'UserLogin[password]' => $passwordSetting->value
        );
        $pageSizeSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'page_size');
        if(!empty($pageSizeSetting))
        {
            $pageSize = $pageSizeSetting->value;
        }

        //query from efichain, and update the sales order accordingly
        $slsRtnHdrDataList = array();
        while($page <= $lastPage)
        {
            $url = $syncSettingHdr->url.'/index.php?r=luuWu/getSalesReturns&page_size='.$pageSize.'&page='.$page.$divUrl;
            
            $response = $client->request('POST', $url, array('headers' => $header, 'form_params' => $formParams));
            $result = $response->getBody()->getContents();
            $result = json_decode($result, true);
            if(empty($result))
            {
                $exc = new ApiException(__('SlsRtn.fail_to_sync', ['url'=>$url]));
                //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
                throw $exc;
            }

            $total = $result['total'];
            for ($a = 0; $a < count($result['data']); $a++)
            {
                $slsRtnHdrData = $result['data'][$a];

                $details = $slsRtnHdrData['details'];
                if(empty($details))
                {
                    //just skip the document if details is empty
                    continue;
                }
                //DRAFT
                $slsRtnHdrModel = SlsRtnHdrRepository::syncSlsRtnSync01($company, $division, $slsRtnHdrData);

                $inbOrdHdrId = 0;
                $toDocTxnFlows = $slsRtnHdrModel->toDocTxnFlows;
                foreach($toDocTxnFlows as $toDocTxnFlow)
                {
                    $inbOrdHdrId = $toDocTxnFlow->to_doc_hdr_id;
                }

                $slsRtnHdrDataList[] = array(
                    'id'=>$slsRtnHdrModel->id,
                    'doc_code'=>$slsRtnHdrModel->doc_code,
                    'doc_date'=>$slsRtnHdrModel->doc_date,
                    'doc_status'=>$slsRtnHdrModel->doc_status,
                    'inb_ord_hdr_id'=>$inbOrdHdrId,
                );
                
                $statusNumber = 100;
                if($total > 0)
                {
                    $statusNumber = bcmul(bcdiv(($a + ($page - 1) * $pageSize), $total, 8), 100, 2);
                }
                BatchJobStatusRepository::updateStatusNumber($batchJobStatusModel->id, $statusNumber);
            }

            $lastPage = $total % $pageSize == 0 ? ($total / $pageSize) : ($total / $pageSize) + 1;

            $page++;
        }

        $batchJobStatusModel = BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

        return $batchJobStatusModel;
    }

    public function transitionToStatus($hdrId, $strDocStatus)
    {
        $hdrModel = null;
        if(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['WIP'])
        {
            $hdrModel = self::transitionToWip($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['COMPLETE'])
        {
            $hdrModel = self::transitionToComplete($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['DRAFT'])
        {
            $hdrModel = self::transitionToDraft($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['VOID'])
        {
            $hdrModel = self::transitionToVoid($hdrId);
        }
        
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $message = __('SlsRtn.document_successfully_transition', ['docCode'=>$documentHeader->doc_code,'strDocStatus'=>$strDocStatus]);
        
        return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>array(),
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }

    static public function processOutgoingHeader($model, $isShowPrint = false)
	{
        $docFlows = self::processDocFlows($model);
        $model->doc_flows = $docFlows;

		if($isShowPrint)
		{
			$printDocTxn = PrintDocTxnRepository::queryPrintDocTxn(SlsRtnHdr::class, $model->id);
			$model->print_count = $printDocTxn->print_count;
			$model->first_printed_at = $printDocTxn->first_printed_at;
			$model->last_printed_at = $printDocTxn->last_printed_at;
		}

        $model->str_doc_status = DocStatus::$MAP[$model->doc_status];
        
        //currency select2
        $initCurrencyOption = array('value'=>0,'label'=>'');
        $currency = CurrencyRepository::findByPk($model->currency_id);
        if(!empty($currency))
        {
            $initCurrencyOption = array('value'=>$currency->id, 'label'=>$currency->symbol);
        }
        $model->currency_select2 = $initCurrencyOption;

        //creditTerm select2
        $initCreditTermOption = array('value'=>0,'label'=>'');
        $creditTerm = CreditTermRepository::findByPk($model->credit_term_id);
        if(!empty($creditTerm))
        {
            $initCreditTermOption = array('value'=>$creditTerm->id, 'label'=>$creditTerm->code);
        }
        $model->credit_term_select2 = $initCreditTermOption;

        //salesman select2
        $salesman = $model->salesman;
        $initSalesmanOption = array(
            'value'=>$salesman->id,
            'label'=>$salesman->username
        );
        $model->salesman_select2 = $initSalesmanOption;

        //biz partner select2
        $deliveryPoint = $model->deliveryPoint;
        $model->delivery_point_unit_no = $deliveryPoint->unit_no;
        $model->delivery_point_building_name = $deliveryPoint->building_name;
        $model->delivery_point_street_name = $deliveryPoint->street_name;
        $model->delivery_point_district_01 = $deliveryPoint->district_01;
        $model->delivery_point_district_02 = $deliveryPoint->district_02;
        $model->delivery_point_postcode = $deliveryPoint->postcode;
        $model->delivery_point_state_name = $deliveryPoint->state_name;
        $model->delivery_point_country_name = $deliveryPoint->country_name;
        $initDeliveryPointOption = array(
            'value'=>$deliveryPoint->id,
            'label'=>$deliveryPoint->code.' '.$deliveryPoint->company_name_01
        );
        $model->delivery_point_select2 = $initDeliveryPointOption;

        $model->hdr_disc_val_01 = 0;
        $model->hdr_disc_perc_01 = 0;
        $model->hdr_disc_val_02 = 0;
        $model->hdr_disc_perc_02 = 0;
        $model->hdr_disc_val_03 = 0;
        $model->hdr_disc_perc_03 = 0;
        $model->hdr_disc_val_04 = 0;
        $model->hdr_disc_perc_04 = 0;
        $model->hdr_disc_val_05 = 0;
        $model->hdr_disc_perc_05 = 0;
        $firstDtlModel = SlsRtnDtlRepository::findTopByHdrId($model->id);
        if(!empty($firstDtlModel))
        {
            $model->hdr_disc_val_01 = $firstDtlModel->hdr_disc_val_01;
            $model->hdr_disc_perc_01 = $firstDtlModel->hdr_disc_perc_01;
            $model->hdr_disc_val_02 = $firstDtlModel->hdr_disc_val_02;
            $model->hdr_disc_perc_02 = $firstDtlModel->hdr_disc_perc_02;
            $model->hdr_disc_val_03 = $firstDtlModel->hdr_disc_val_03;
            $model->hdr_disc_perc_03 = $firstDtlModel->hdr_disc_perc_03;
            $model->hdr_disc_val_04 = $firstDtlModel->hdr_disc_val_04;
            $model->hdr_disc_perc_04 = $firstDtlModel->hdr_disc_perc_04;
            $model->hdr_disc_val_05 = $firstDtlModel->hdr_disc_val_05;
            $model->hdr_disc_perc_05 = $firstDtlModel->hdr_disc_perc_05;
        }

		return $model;
    }
    
    static public function processOutgoingDetail($model)
    {
        $item = $model->item;
        $model->item_code = $item->code;
        //$model->item_desc_01 = $item->desc_01;
        //$model->item_desc_02 = $item->desc_02;
        //item select2
        $initItemOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($item))
        {
            $initItemOption = array(
                'value'=>$item->id, 
                'label'=>$item->code.' '.$item->desc_01,
            );
        }
        $model->item_select2 = $initItemOption;

        $uom = $model->uom;
		//uom select2
		$model->uom_code = '';
        $initUomOption = array(
            'value'=>0,
            'label'=>''
		);
        if(!empty($uom))
        {
			$model->uom_code = $uom->code;
            $initUomOption = array('value'=>$uom->id,'label'=>$uom->code);
        }        
        $model->uom_select2 = $initUomOption;

        $model = ItemService::processCaseLoose($model, $item);

        return $model;
	}

    static public function transitionToWip($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = SlsRtnHdrRepository::txnFindByPk($hdrId);
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status >= DocStatus::$MAP['WIP'])
		{
			$exc = new ApiException(__('SlsRtn.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\SlsRtnHdr::class, $hdrModel->id);
            throw $exc;
		}

		//commit the document
		$hdrModel = SlsRtnHdrRepository::commitToWip($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('SlsRtn.commit_to_wip_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\SlsRtnHdr::class, $hdrModel->id);
        throw $exc;
	}

	static public function transitionToComplete($hdrId)
    {
        AuthService::authorize(
            array(
                'sls_rtn_confirm'
            )
        );

		$isSuccess = false;
		//use transaction to make sure this is latest value
		$hdrModel = SlsRtnHdrRepository::txnFindByPk($hdrId);

		//only DRAFT or above can transition to COMPLETE
		if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE']
		|| $hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('SlsRtn.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\SlsRtnHdr::class, $hdrModel->id);
            throw $exc;
		}

		$dtlModels = SlsRtnDtlRepository::findAllByHdrId($hdrModel->id);
		//preliminary checking
		//check the detail stock make sure the detail quant_bal_id > 0
		foreach($dtlModels as $dtlModel)
		{
			if($dtlModel->item_id == 0)
			{
				$exc = new ApiException(__('SlsRtn.item_is_required', ['lineNo'=>$dtlModel->line_no]));
				$exc->addData(\App\SlsRtnDtl::class, $dtlModel->id);
				throw $exc;
			}
		}

		//commit the document
		$hdrModel = SlsRtnHdrRepository::commitToComplete($hdrModel->id, true);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
        }
        
		$exc = new ApiException(__('SlsRtn.commit_to_complete_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\SlsRtnHdr::class, $hdrModel->id);
        throw $exc;
    }
    
    static public function transitionToDraft($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = SlsRtnHdrRepository::txnFindByPk($hdrId);

		//only WIP or COMPLETE can transition to DRAFT
		if($hdrModel->doc_status == DocStatus::$MAP['WIP'])
		{
            $hdrModel = SlsRtnHdrRepository::revertWipToDraft($hdrModel->id);
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		elseif($hdrModel->doc_status == DocStatus::$MAP['COMPLETE'])
		{
            AuthService::authorize(
                array(
                    'sls_rtn_revert'
                )
            );

            $hdrModel = SlsRtnHdrRepository::revertCompleteToDraft($hdrModel->id);
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
        }
        elseif($hdrModel->doc_status == DocStatus::$MAP['VOID'])
		{
            AuthService::authorize(
                array(
                    'sls_rtn_confirm'
                )
            );

			$hdrModel = SlsRtnHdrRepository::commitVoidToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
        
        $exc = new ApiException(__('SlsRtn.doc_status_is_not_wip_or_complete', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\SlsRtnHdr::class, $hdrModel->id);
		throw $exc;
    }

    static public function transitionToVoid($hdrId)
    {
        AuthService::authorize(
            array(
                'sls_rtn_revert'
            )
        );

		//use transaction to make sure this is latest value
		$hdrModel = SlsRtnHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('SlsRtn.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\SlsRtnHdr::class, $hdrModel->id);
            throw $exc;
		}

		//revert the document
		$hdrModel = SlsRtnHdrRepository::revertDraftToVoid($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('SlsRtn.commit_to_void_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\SlsRtnHdr::class, $hdrModel->id);
        throw $exc;
    }

    public function initHeader($divisionId)
    {
        AuthService::authorize(
            array(
                'sls_rtn_create'
            )
        );

        $user = Auth::user();

        $model = new SlsRtnHdr();
        $model->doc_flows = array();
        $model->doc_status = DocStatus::$MAP['DRAFT'];
        $model->str_doc_status = DocStatus::$MAP[$model->doc_status];
        $model->doc_code = '';
        $model->ref_code_01 = '';
        $model->ref_code_02 = '';
        $model->ref_code_03 = '';
        $model->ref_code_04 = '';
        $model->ref_code_05 = '';
        $model->ref_code_06 = '';
        $model->doc_date = date('Y-m-d');
        $model->est_del_date = date('Y-m-d');
        $model->desc_01 = '';
        $model->desc_02 = '';

        $division = DivisionRepository::findByPk($divisionId);
        $model->division_id = $divisionId;
        $model->division_code = '';
        $model->site_flow_id = 0;
        $model->company_id = 0;
        $model->company_code = '';
        if(!empty($division))
        {
            $model->division_code = $division->code;
            $model->site_flow_id = $division->site_flow_id;
            $company = $division->company;
            $model->company_id = $company->id;
            $model->company_code = $company->code;
        }

        $model->doc_no_id = 0;
        $docNoIdOptions = array();
        $docNos = DocNoRepository::findAllDivisionDocNo($divisionId, \App\SlsRtnHdr::class, $model->doc_date);
        for($a = 0; $a < count($docNos); $a++)
        {
            $docNo = $docNos[$a];
            if($a == 0)
            {
                $model->doc_no_id = $docNo->id;
            }
            $docNoIdOptions[] = array('value'=>$docNo->id, 'label'=>$docNo->latest_code);
        }
        $model->doc_no_id_options = $docNoIdOptions;

        //currency dropdown
        $model->currency_id = 0;
        $model->currency_rate = 1;
        $initCurrencyOption = array('value'=>0,'label'=>'');
        $model->currency_select2 = $initCurrencyOption;

        //creditTerm select2
        $initCreditTermOption = array('value'=>0,'label'=>'');
        $model->credit_term_select2 = $initCreditTermOption;

        //salesman select2
        $initSalesmanOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($user))
        {
            $initSalesmanOption = array(
                'value'=>$user->id, 
                'label'=>$user->username
            );
        }
        $model->salesman_select2 = $initSalesmanOption;

        //delivery_point select2
        
        $model->delivery_point_unit_no = '';
        $model->delivery_point_building_name = '';
        $model->delivery_point_street_name = '';
        $model->delivery_point_district_01 = '';
        $model->delivery_point_district_02 = '';
        $model->delivery_point_postcode = '';
        $model->delivery_point_state_name = '';
        $model->delivery_point_country_name = '';
        $initDeliveryPointOption = array(
            'value'=>0,
            'label'=>''
        );
        $deliveryPoint = DeliveryPointRepository::findTop();
        if(!empty($deliveryPoint))
        {
            $initDeliveryPointOption = array(
                'value'=>$deliveryPoint->id, 
                'label'=>$deliveryPoint->code.' '.$deliveryPoint->company_name_01
            );

            $model->delivery_point_unit_no = $deliveryPoint->unit_no;
            $model->delivery_point_building_name = $deliveryPoint->building_name;
            $model->delivery_point_street_name = $deliveryPoint->street_name;
            $model->delivery_point_district_01 = $deliveryPoint->district_01;
            $model->delivery_point_district_02 = $deliveryPoint->district_02;
            $model->delivery_point_postcode = $deliveryPoint->postcode;
            $model->delivery_point_state_name = $deliveryPoint->state_name;
            $model->delivery_point_country_name = $deliveryPoint->country_name;
        }
        $model->delivery_point_select2 = $initDeliveryPointOption;

        $model->hdr_disc_val_01 = 0;
        $model->hdr_disc_perc_01 = 0;
        $model->hdr_disc_val_02 = 0;
        $model->hdr_disc_perc_02 = 0;
        $model->hdr_disc_val_03 = 0;
        $model->hdr_disc_perc_03 = 0;
        $model->hdr_disc_val_04 = 0;
        $model->hdr_disc_perc_04 = 0;
        $model->hdr_disc_val_05 = 0;
        $model->hdr_disc_perc_05 = 0;

        $model->disc_amt = 0;
        $model->tax_amt = 0;
        $model->round_adj_amt = 0;
        $model->net_amt = 0;

        return $model;
    }

    public function showHeader($hdrId)
    {
        AuthService::authorize(
            array(
                'sls_rtn_read',
				'sls_rtn_update'
            )
        );

		$model = SlsRtnHdrRepository::findByPk($hdrId);
		if(!empty($model))
        {
            $model = self::processOutgoingHeader($model);
        }
		return $model;
    }

    public function updateHeader($hdrData)
	{
        AuthService::authorize(
            array(
                'sls_rtn_update'
            )
        );

        $hdrData = self::processIncomingHeader($hdrData);

		//get the associated inbOrdHdr
        $slsRtnHdr = SlsRtnHdrRepository::txnFindByPk($hdrData['id']);
        if($slsRtnHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('SlsRtn.doc_status_is_wip_or_complete', ['docCode'=>$slsRtnHdr->doc_code]));
            $exc->addData(\App\SlsRtnHdr::class, $slsRtnHdr->id);
            throw $exc;
        }
        $slsRtnHdrData = $slsRtnHdr->toArray();
        //assign hdrData to model
        foreach($hdrData as $field => $value) 
        {
            if(strpos($field, 'hdr_disc_val') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_disc_perc') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_tax_val') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_tax_perc') !== false)
            {
                continue;
            }
            $slsRtnHdrData[$field] = $value;
        }
        
        $slsRtnDtlArray = array();

        //check if hdr disc or hdr tax is set and more than 0
        $needToProcessDetails = false;
        $firstDtlModel = SlsRtnDtlRepository::findTopByHdrId($hdrData['id']);
        if(!empty($firstDtlModel))
        {
            $firstDtlData = $firstDtlModel->toArray();
            for($a = 1; $a <= 5; $a++)
            {
                if(array_key_exists('hdr_disc_val_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_disc_val_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_disc_val_0'.$a], $firstDtlData['hdr_disc_val_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
                if(array_key_exists('hdr_disc_perc_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_disc_perc_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_disc_perc_0'.$a], $firstDtlData['hdr_disc_perc_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }

                if(array_key_exists('hdr_tax_val_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_tax_val_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_tax_val_0'.$a], $firstDtlData['hdr_tax_val_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
                if(array_key_exists('hdr_tax_perc_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_tax_perc_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_tax_perc_0'.$a], $firstDtlData['hdr_tax_perc_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
            }
        }

        if($needToProcessDetails)
        {
            //query the slsRtnDtlModels from DB
            //format to array, with is_modified field to indicate it is changed
            $slsRtnDtlModels = SlsRtnDtlRepository::findAllByHdrId($hdrData['id']);
            foreach($slsRtnDtlModels as $slsRtnDtlModel)
            {
                $slsRtnDtlData = $slsRtnDtlModel->toArray();
                for($a = 1; $a <= 5; $a++)
                {
                    if(array_key_exists('hdr_disc_val_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_disc_val_0'.$a], 0, 5) > 0)
                    {
                        $slsRtnDtlData['hdr_disc_val_0'.$a] = $hdrData['hdr_disc_val_0'.$a];
                    }
                    if(array_key_exists('hdr_disc_perc_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_disc_perc_0'.$a], 0, 5) > 0)
                    {
                        $slsRtnDtlData['hdr_disc_perc_0'.$a] = $hdrData['hdr_disc_perc_0'.$a];
                    }

                    if(array_key_exists('hdr_tax_val_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_tax_val_0'.$a], 0, 5) > 0)
                    {
                        $slsRtnDtlData['hdr_tax_val_0'.$a] = $hdrData['hdr_tax_val_0'.$a];
                    }
                    if(array_key_exists('hdr_tax_perc_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_tax_perc_0'.$a], 0, 5) > 0)
                    {
                        $slsRtnDtlData['hdr_tax_perc_0'.$a] = $hdrData['hdr_tax_perc_0'.$a];
                    }
                }
                $slsRtnDtlData['is_modified'] = 1;
                $slsRtnDtlArray[] = $slsRtnDtlData;
            }
            
            //calculate details amount, use adaptive rounding tax
            $result = $this->calculateAmount($slsRtnHdrData, $slsRtnDtlArray);
            $slsRtnHdrData = $result['hdrData'];
            $slsRtnDtlArray = $result['dtlArray'];
        }
            
        $result = SlsRtnHdrRepository::updateDetails($slsRtnHdrData, $slsRtnDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];

        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('SlsRtn.document_successfully_updated', ['docCode'=>$documentHeader->doc_code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }
    
    public function createHeader($data)
    {		
        AuthService::authorize(
            array(
                'sls_rtn_create'
            )
        );

        $data = self::processIncomingHeader($data, true);
        $docNoId = $data['doc_no_id'];
        unset($data['doc_no_id']);
        $ordHdrModel = SlsRtnHdrRepository::createHeader(ProcType::$MAP['NULL'], $docNoId, $data);

        $message = __('SlsRtn.document_successfully_created', ['docCode'=>$ordHdrModel->doc_code]);

		return array(
			'data' => $ordHdrModel->id,
			'message' => $message
		);
    }

    public function changeDeliveryPoint($deliveryPointId)
    {
        AuthService::authorize(
            array(
                'sls_rtn_update'
            )
        );

        $results = array();
        $deliveryPoint = DeliveryPointRepository::findByPk($deliveryPointId);
        if(!empty($deliveryPoint))
        {
            $results['delivery_point_unit_no'] = $deliveryPoint->unit_no;
            $results['delivery_point_building_name'] = $deliveryPoint->building_name;
            $results['delivery_point_street_name'] = $deliveryPoint->street_name;
            $results['delivery_point_district_01'] = $deliveryPoint->district_01;
            $results['delivery_point_district_02'] = $deliveryPoint->district_02;
            $results['delivery_point_postcode'] = $deliveryPoint->postcode;
            $results['delivery_point_state_name'] = $deliveryPoint->state_name;
            $results['delivery_point_country_name'] = $deliveryPoint->country_name;
        }
        return $results;
    }

    public function changeItem($hdrId, $itemId)
    {
        AuthService::authorize(
            array(
                'sls_rtn_update'
            )
        );

        $slsRtnHdr = SlsRtnHdrRepository::findByPk($hdrId);

        $results = array();
        $item = ItemRepository::findByPk($itemId);
        if(!empty($item)
        && !empty($slsRtnHdr))
        {
            $results['desc_01'] = $item->desc_01;
            $results['desc_02'] = $item->desc_02;
        }
        return $results;
    }

    public function changeItemUom($hdrId, $itemId, $uomId)
    {
        AuthService::authorize(
            array(
                'sls_rtn_update'
            )
        );

        $slsRtnHdr = SlsRtnHdrRepository::findByPk($hdrId);

        $results = array();
        $itemUom = ItemUomRepository::findByItemIdAndUomId($itemId, $uomId);
        if(!empty($itemUom)
        && !empty($slsRtnHdr))
        {
            $results['item_id'] = $itemUom->item_id;
            $results['uom_id'] = $itemUom->uom_id;
            $results['uom_rate'] = $itemUom->uom_rate;

            $results = $this->updateSaleItemUom($results, $slsRtnHdr->doc_date, 0, $slsRtnHdr->currency_id, 0, 0);
        }
        return $results;
	}
	
	public function changeCurrency($currencyId)
    {
        AuthService::authorize(
            array(
                'sls_rtn_update'
            )
        );

        $results = array();
        $currency = CurrencyRepository::findByPk($currencyId);
        if(!empty($currency))
        {
            $results['currency_rate'] = $currency->currency_rate;
        }
        return $results;
    }

    public function createDetail($hdrId, $data)
	{
        AuthService::authorize(
            array(
                'sls_rtn_update'
            )
        );

        $data = self::processIncomingDetail($data);

        $slsRtnHdr = SlsRtnHdrRepository::txnFindByPk($hdrId);
        if($slsRtnHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('SlsRtn.doc_status_is_wip_or_complete', ['docCode'=>$slsRtnHdr->doc_code]));
            $exc->addData(\App\SlsRtnHdr::class, $slsRtnHdr->id);
            throw $exc;
        }
		$slsRtnHdrData = $slsRtnHdr->toArray();

		$slsRtnDtlArray = array();
		$slsRtnDtlModels = SlsRtnDtlRepository::findAllByHdrId($hdrId);
		foreach($slsRtnDtlModels as $slsRtnDtlModel)
		{
			$slsRtnDtlData = $slsRtnDtlModel->toArray();
			$slsRtnDtlData['is_modified'] = 0;
			$slsRtnDtlArray[] = $slsRtnDtlData;
		}
		$lastLineNo = count($slsRtnDtlModels);

		//assign temp id
		$tmpUuid = uniqid();
		//append NEW here to make sure it will be insert in repository later
		$data['id'] = 'NEW'.$tmpUuid;
		$data['line_no'] = $lastLineNo + 1;
        $data['is_modified'] = 1;
		$data = $this->updateSaleItemUom($data, $slsRtnHdr['doc_date'], 0, $slsRtnHdr['currency_id'], 0, 0);
		$slsRtnDtlData = $this->initSlsRtnDtlData($data);
		$slsRtnDtlArray[] = $slsRtnDtlData;

		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($slsRtnHdrData, $slsRtnDtlArray);
		$slsRtnHdrData = $result['hdrData'];
        $slsRtnDtlArray = $result['dtlArray'];

		$result = SlsRtnHdrRepository::updateDetails($slsRtnHdrData, $slsRtnDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('SlsRtn.detail_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function deleteDetails($hdrId, $dtlArray)
	{
        AuthService::authorize(
            array(
                'sls_rtn_update'
            )
        );

        $slsRtnHdr = SlsRtnHdrRepository::txnFindByPk($hdrId);
        if($slsRtnHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('SlsRtn.doc_status_is_wip_or_complete', ['docCode'=>$slsRtnHdr->doc_code]));
            $exc->addData(\App\SlsRtnHdr::class, $slsRtnHdr->id);
            throw $exc;
        }
		$slsRtnHdrData = $slsRtnHdr->toArray();

		//query the slsRtnDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$delSlsRtnDtlArray = array();
		$slsRtnDtlArray = array();
        $slsRtnDtlModels = SlsRtnDtlRepository::findAllByHdrId($hdrId);
        $lineNo = 1;
		foreach($slsRtnDtlModels as $slsRtnDtlModel)
		{
			$slsRtnDtlData = $slsRtnDtlModel->toArray();
			$slsRtnDtlData['is_modified'] = 0;
			$slsRtnDtlData['is_deleted'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{
				if($dtlData['id'] == $slsRtnDtlData['id'])
				{
					//this is deleted dtl
					$slsRtnDtlData['is_deleted'] = 1;
					break;
				}
			}
			if($slsRtnDtlData['is_deleted'] > 0)
			{
				$delSlsRtnDtlArray[] = $slsRtnDtlData;
			}
			else
			{
                $slsRtnDtlData['line_no'] = $lineNo;
                $slsRtnDtlData['is_modified'] = 1;
                $slsRtnDtlArray[] = $slsRtnDtlData;
                $lineNo++;
			}
        }
        
		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($slsRtnHdrData, $slsRtnDtlArray, $delSlsRtnDtlArray);
		$slsRtnHdrData = $result['hdrData'];
		$modifiedDtlArray = $result['dtlArray'];
		foreach($slsRtnDtlArray as $slsRtnDtlSeq => $slsRtnDtlData)
		{
			foreach($modifiedDtlArray as $modifiedDtlData)
			{
				if($modifiedDtlData['id'] == $slsRtnDtlData['id'])
				{
					foreach($modifiedDtlData as $field => $value)
					{
						$slsRtnDtlData[$field] = $value;
					}
					$slsRtnDtlData['is_modified'] = 1;
					$slsRtnDtlArray[$slsRtnDtlSeq] = $slsRtnDtlData;
					break;
				}
			}
		}
		
		$result = SlsRtnHdrRepository::updateDetails($slsRtnHdrData, $slsRtnDtlArray, $delSlsRtnDtlArray);
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('SlsRtn.details_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>$delSlsRtnDtlArray
            ),
			'message' => $message
		);
    }

    public function updateDetails($hdrId, $dtlArray)
	{
        AuthService::authorize(
            array(
                'sls_rtn_update'
            )
        );

        $slsRtnHdr = SlsRtnHdrRepository::txnFindByPk($hdrId);
        if($slsRtnHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('SlsRtn.doc_status_is_wip_or_complete', ['docCode'=>$slsRtnHdr->doc_code]));
            $exc->addData(\App\SlsRtnHdr::class, $slsRtnHdr->id);
            throw $exc;
        }
		$slsRtnHdrData = $slsRtnHdr->toArray();

		//query the slsRtnDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$slsRtnDtlArray = array();
        $slsRtnDtlModels = SlsRtnDtlRepository::findAllByHdrId($hdrId);
		foreach($slsRtnDtlModels as $slsRtnDtlModel)
		{
			$slsRtnDtlData = $slsRtnDtlModel->toArray();
			$slsRtnDtlData['is_modified'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{                
				if($dtlData['id'] == $slsRtnDtlData['id'])
				{
                    $dtlData = self::processIncomingDetail($dtlData);
					$dtlData = $this->updateSaleItemUom($dtlData, $slsRtnHdr->doc_date, 0, $slsRtnHdr->currency_id, 0, 0);
					foreach($dtlData as $fieldName => $value)
					{
						$slsRtnDtlData[$fieldName] = $value;
					}
					$slsRtnDtlData['is_modified'] = 1;

					break;
				}
			}
			$slsRtnDtlArray[] = $slsRtnDtlData;
		}
		
		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($slsRtnHdrData, $slsRtnDtlArray);
		$slsRtnHdrData = $result['hdrData'];
		$slsRtnDtlArray = $result['dtlArray'];
		
		$result = SlsRtnHdrRepository::updateDetails($slsRtnHdrData, $slsRtnDtlArray, array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('SlsRtn.details_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }

    public function showDetails($hdrId) 
	{
        AuthService::authorize(
            array(
                'sls_rtn_read',
				'sls_rtn_update'
            )
        );

		$slsRtnDtls = SlsRtnDtlRepository::findAllByHdrId($hdrId);
		$slsRtnDtls->load(
            'item', 'item.itemUoms', 'item.itemUoms.uom'
        );
        foreach($slsRtnDtls as $slsRtnDtl)
        {
            $slsRtnDtl = self::processOutgoingDetail($slsRtnDtl);
        }
        return $slsRtnDtls;
    }
    
    public function index($divisionId, $sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
            array(
                'sls_rtn_read'
            )
        );
        
        //DB::connection()->enableQueryLog();
        $slsRtnHdrs = SlsRtnHdrRepository::findAll($divisionId, $sorts, $filters, $pageSize);
        $slsRtnHdrs->load(
            'slsRtnDtls'
            //'slsRtnDtls.item', 'slsRtnDtls.item.itemUoms', 'slsRtnDtls.item.itemUoms.uom', 
        );
		foreach($slsRtnHdrs as $slsRtnHdr)
		{
			$slsRtnHdr->str_doc_status = DocStatus::$MAP[$slsRtnHdr->doc_status];

			$deliveryPoint = $slsRtnHdr->deliveryPoint;
			$slsRtnHdr->delivery_point_code = $deliveryPoint->code;
			$slsRtnHdr->delivery_point_company_name_01 = $deliveryPoint->company_name_01;
			$slsRtnHdr->delivery_point_area_code = $deliveryPoint->area_code;
			$area = $deliveryPoint->area;
			if(!empty($area))
			{
				$slsRtnHdr->delivery_point_area_desc_01 = $area->desc_01;
			}

			$salesman = $slsRtnHdr->salesman;
			$slsRtnHdr->salesman_username = $salesman->username;

			$slsRtnDtls = $slsRtnHdr->slsRtnDtls;
			$slsRtnHdr->details = $slsRtnDtls;
        }
        //Log::error(DB::getQueryLog());
    	return $slsRtnHdrs;
    }
}
