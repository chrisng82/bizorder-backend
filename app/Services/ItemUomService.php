<?php

namespace App\Services;

use App\Repositories\ItemUomRepository;
use App\Repositories\ItemSalePriceRepository;


class ItemUomService 
{
    public function __construct() 
	{
    }
    
    public function select2($search, $filters)
    {
        $itemUoms = ItemUomRepository::select2($search, $filters);
        return $itemUoms;
    }

    public function deleteItemUom($item_sale_prices_id)
    {
        $itemSalePrice = ItemSalePriceRepository::deleteByPK($item_sale_prices_id);
        $Uom = ItemUomRepository::findByItemIdAndUomId($itemSalePrice->item_id, $itemSalePrice->uom_id);
        $itemUoms=ItemUomRepository::deleteByPK($Uom->id);
        return $itemUoms;
    }
}
