<?php

namespace App\Services;

use App\CartHdr;
use App\CartDtl;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Services\Env\ResStatus;
use App\Services\Env\ScanMode;
use App\Services\Env\RetrievalMethod;
use App\Services\Env\StorageClass;
use App\Repositories\SlsOrdHdrRepository;
use App\Repositories\BatchJobStatusRepository;
use App\Repositories\SyncSettingHdrRepository;
use App\Repositories\SyncSettingDtlRepository;
use App\Repositories\SiteFlowRepository;
use App\Repositories\CartHdrRepository;
use App\Repositories\CartDtlRepository;
use App\Repositories\DivisionRepository;
use App\Repositories\InvTxnFlowRepository;
use App\Repositories\QuantBalRepository;
use App\Repositories\OutbOrdHdrRepository;
use App\Repositories\DeliveryPointRepository;
use App\Repositories\ItemRepository;
use App\Repositories\ItemUomRepository;
use App\Repositories\CurrencyRepository;
use App\Repositories\CreditTermRepository;
use App\Repositories\DocTxnFlowRepository;
use App\Repositories\PromotionRepository;
use App\Repositories\PromotionVariantRepository;
use App\Repositories\CartDtlPromotionRepository;
use App\BatchJobStatus;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;
use App\Services\Env\PromotionActionType;
use App\Services\Env\PromotionRuleType;
use App\Libs\Promotion\DebtorChecker;
use App\Libs\Promotion\DebtorGroupChecker;
use App\Libs\Promotion\ItemPercentDiscountAction;
use App\Libs\Promotion\ItemFixedPriceAction;
use App\Libs\Promotion\ItemFocAction;
use Milon\Barcode\DNS2D;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Log;

class CartService extends InventoryService
{
    public function __construct()
    {
    }

    // public function syncProcess($strProcType, $divisionId)
    // {
    //     $user = Auth::user();

    //     if(isset(ProcType::$MAP[$strProcType]))
	// 	{
	// 		if(ProcType::$MAP[$strProcType] == ProcType::$MAP['SLS_ORD_SYNC_01']) 
	// 		{
	// 			//sync sales order from EfiChain
    //             return $this->syncSlsOrdSync01($divisionId, $user->id);
    //             //return $this->checkSlsOrdWithInverze($divisionId, $user->id);
    //         }
	// 	}
    // }

    // protected function syncSlsOrdSync01($divisionId, $userId)
    // {
    //     AuthService::authorize(
	// 		array(
	// 			'sls_ord_import'
	// 		)
    //     );
        
    //     $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['SLS_ORD_SYNC_01'], $userId);

    //     $syncSettingHdr = SyncSettingHdrRepository::findByProcTypeAndDivisionId(ProcType::$MAP['SYNC_EFICHAIN_01'], $divisionId);
    //     if(empty($syncSettingHdr))
    //     {
    //         $exc = new ApiException(__('SlsOrd.sync_setting_not_found', ['divisionId'=>$divisionId]));
    //         //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
    //         throw $exc;
    //     }

    //     $division = DivisionRepository::findByPk($divisionId);
    //     $company = $division->company;
    //     $divUrl = '&divisions[]='.$division->code;

    //     $client = new Client();
    //     $header = array();

    //     $page = 1;
    //     $lastPage = 1;
    //     $pageSize = 1000;
    //     $total = 0;

    //     $usernameSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'username');
    //     $passwordSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'password');
    //     $formParams = array(
    //         'UserLogin[username]' => $usernameSetting->value,
    //         'UserLogin[password]' => $passwordSetting->value
    //     );
    //     $pageSizeSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'page_size');
    //     if(!empty($pageSizeSetting))
    //     {
    //         $pageSize = $pageSizeSetting->value;
    //     }

    //     //query from efichain, and update the sales order accordingly
    //     $cartHdrDataList = array();
    //     while($page <= $lastPage)
    //     {
    //         $url = $syncSettingHdr->url.'/index.php?r=luuWu/getSalesOrders&page_size='.$pageSize.'&page='.$page.$divUrl;
            
    //         $response = $client->request('POST', $url, array('headers' => $header, 'form_params' => $formParams));
    //         $result = $response->getBody()->getContents();
    //         $result = json_decode($result, true);
    //         if(empty($result))
    //         {
    //             $exc = new ApiException(__('SlsOrd.fail_to_sync', ['url'=>$url]));
    //             //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
    //             throw $exc;
    //         }

    //         $total = $result['total'];
    //         for ($a = 0; $a < count($result['data']); $a++)
    //         {
    //             $cartHdrData = $result['data'][$a];
    //             //DRAFT
    //             $cartHdrModel = CartHdrRepository::syncSlsOrdSync01($company, $division, $cartHdrData);

    //             $outbOrdHdrId = 0;
    //             $toDocTxnFlows = $cartHdrModel->toDocTxnFlows;
    //             foreach($toDocTxnFlows as $toDocTxnFlow)
    //             {
    //                 $outbOrdHdrId = $toDocTxnFlow->to_doc_hdr_id;
    //             }

    //             $cartHdrDataList[] = array(
    //                 'id'=>$cartHdrModel->id,
    //                 'doc_code'=>$cartHdrModel->doc_code,
    //                 'doc_date'=>$cartHdrModel->doc_date,
    //                 'doc_status'=>$cartHdrModel->doc_status,
    //                 'outb_ord_hdr_id'=>$outbOrdHdrId,
    //             );
                
    //             $statusNumber = 100;
    //             if($total > 0)
    //             {
    //                 $statusNumber = bcmul(bcdiv(($a + ($page - 1) * $pageSize), $total, 8), 100, 2);
    //             }
    //             BatchJobStatusRepository::updateStatusNumber($batchJobStatusModel->id, $statusNumber);
    //         }

    //         $lastPage = $total % $pageSize == 0 ? ($total / $pageSize) : ($total / $pageSize) + 1;

    //         $page++;
    //     }

    //     //post back to efichain, to set the efichain sales order to PICKING
    //     $formParams = array(
    //         'UserLogin[username]' => $usernameSetting->value,
    //         'UserLogin[password]' => $passwordSetting->value
	// 	);		
    //     $formParams['CartHdrs'] = $cartHdrDataList;
        
    //     $url = $syncSettingHdr->url.'/index.php?r=luuWu/putSalesOrderStatus';
            
	// 	$response = $client->request('POST', $url, array('headers' => $header, 'form_params' => $formParams));
		
	// 	$result = $response->getBody()->getContents();
	// 	$result = json_decode($result, true);
	// 	if(empty($result))
	// 	{
	// 		BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

	// 		$exc = new ApiException(__('SlsOrd.fail_to_sync', ['url'=>$url]));
	// 		//$exc->addData(\App\SlsInvDtl::class, $delDtlData['id']);
	// 		throw $exc;
    //     }
    //     else
	// 	{
	// 		if($result['success'] === false)
	// 		{
	// 			BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

	// 			$exc = new ApiException(__('SlsOrd.fail_to_sync', ['url'=>$url,'message'=>$result['message']]));
	// 			//$exc->addData(\App\SlsInvDtl::class, $delDtlData['id']);
	// 			throw $exc;
    //         }
    //         else
    //         {
    //             $batchJobStatusModel = BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);
    //         }
	// 	}

    //     return $batchJobStatusModel;
    // }

    public function transitionToStatus($hdrId, $strDocStatus)
    {
        $hdrModel = null;
        if(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['WIP'])
        {
            $hdrModel = self::transitionToWip($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['COMPLETE'])
        {
            $hdrModel = self::transitionToComplete($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['DRAFT'])
        {
            $hdrModel = self::transitionToDraft($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['VOID'])
        {
            $hdrModel = self::transitionToVoid($hdrId);
        }
        
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $message = __('SlsOrd.document_successfully_transition', ['docCode'=>$documentHeader->doc_code,'strDocStatus'=>$strDocStatus]);
        
        return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>array(),
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }

    static public function processOutgoingHeader($model, $isShowPrint = false)
	{
        $docFlows = self::processDocFlows($model);
        $model->doc_flows = $docFlows;
        
		// if($isShowPrint)
		// {
		// 	$printDocTxn = PrintDocTxnRepository::queryPrintDocTxn(CartHdr::class, $model->id);
		// 	$model->print_count = $printDocTxn->print_count;
		// 	$model->first_printed_at = $printDocTxn->first_printed_at;
		// 	$model->last_printed_at = $printDocTxn->last_printed_at;
        // }
        
        $division = $model->division;
        $model->division_code = $division->code;
        $company = $model->company;
        $model->company_code = $company->code;

        $model->str_doc_status = DocStatus::$MAP[$model->doc_status];
        
        //currency select2
        $initCurrencyOption = array('value'=>0,'label'=>'');
        $currency = CurrencyRepository::findByPk($model->currency_id);
        if(!empty($currency))
        {
            $initCurrencyOption = array('value'=>$currency->id, 'label'=>$currency->symbol);
        }
        $model->currency_select2 = $initCurrencyOption;

        //creditTerm select2
        $initCreditTermOption = array('value'=>0,'label'=>'');
        $creditTerm = CreditTermRepository::findByPk($model->credit_term_id);
        if(!empty($creditTerm))
        {
            $initCreditTermOption = array('value'=>$creditTerm->id, 'label'=>$creditTerm->code);
        }
        $model->credit_term_select2 = $initCreditTermOption;

        //salesman select2
        $salesman = $model->salesman;
        $initSalesmanOption = array(
            'value'=>$salesman->id,
            'label'=>$salesman->username
        );
        $model->salesman_select2 = $initSalesmanOption;

        //biz partner select2
        // $deliveryPoint = $model->deliveryPoint;
		// if (!empty($deliveryPoint)) {
		// 	$model->delivery_point_unit_no = $deliveryPoint->unit_no;
		// 	$model->delivery_point_building_name = $deliveryPoint->building_name;
		// 	$model->delivery_point_street_name = $deliveryPoint->street_name;
		// 	$model->delivery_point_district_01 = $deliveryPoint->district_01;
		// 	$model->delivery_point_district_02 = $deliveryPoint->district_02;
		// 	$model->delivery_point_postcode = $deliveryPoint->postcode;
		// 	$model->delivery_point_state_name = $deliveryPoint->state_name;
		// 	$model->delivery_point_country_name = $deliveryPoint->country_name;
		// 	$initDeliveryPointOption = array(
		// 		'value'=>$deliveryPoint->id,
		// 		'label'=>$deliveryPoint->code.' '.$deliveryPoint->company_name_01
		// 	);
		// 	$model->delivery_point_select2 = $initDeliveryPointOption;
		// }

        $model->hdr_disc_val_01 = 0;
        $model->hdr_disc_perc_01 = 0;
        $model->hdr_disc_val_02 = 0;
        $model->hdr_disc_perc_02 = 0;
        $model->hdr_disc_val_03 = 0;
        $model->hdr_disc_perc_03 = 0;
        $model->hdr_disc_val_04 = 0;
        $model->hdr_disc_perc_04 = 0;
        $model->hdr_disc_val_05 = 0;
        $model->hdr_disc_perc_05 = 0;
        $firstDtlModel = CartDtlRepository::findTopByHdrId($model->id);
        if(!empty($firstDtlModel))
        {
            $model->hdr_disc_val_01 = $firstDtlModel->hdr_disc_val_01;
            $model->hdr_disc_perc_01 = $firstDtlModel->hdr_disc_perc_01;
            $model->hdr_disc_val_02 = $firstDtlModel->hdr_disc_val_02;
            $model->hdr_disc_perc_02 = $firstDtlModel->hdr_disc_perc_02;
            $model->hdr_disc_val_03 = $firstDtlModel->hdr_disc_val_03;
            $model->hdr_disc_perc_03 = $firstDtlModel->hdr_disc_perc_03;
            $model->hdr_disc_val_04 = $firstDtlModel->hdr_disc_val_04;
            $model->hdr_disc_perc_04 = $firstDtlModel->hdr_disc_perc_04;
            $model->hdr_disc_val_05 = $firstDtlModel->hdr_disc_val_05;
            $model->hdr_disc_perc_05 = $firstDtlModel->hdr_disc_perc_05;
        }

		return $model;
    }
    
    static public function processOutgoingDetail($model)
    {
        $item = $model->item;
        $model->item_code = $item->code;
        $model->item_desc_01 = $item->desc_01;
        $model->item_desc_02 = $item->desc_02;
        // item select2
        $initItemOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($item))
        {
            $initItemOption = array(
                'value'=>$item->id, 
                'label'=>$item->code.' '.$item->desc_01,
            );
        }
        $model->item_select2 = $initItemOption;

        $uom = $model->uom;
		//uom select2
		$model->uom_code = '';
        $initUomOption = array(
            'value'=>0,
            'label'=>''
		);
        if(!empty($uom))
        {
			$model->uom_code = $uom->code;
            $initUomOption = array('value'=>$uom->id,'label'=>$uom->code);
        }        
        $model->uom_select2 = $initUomOption;

        $model = ItemService::processCaseLoose($model, $item);

        return $model;
	}

    static public function transitionToWip($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = CartHdrRepository::txnFindByPk($hdrId);
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status >= DocStatus::$MAP['WIP'])
		{
			$exc = new ApiException(__('SlsOrd.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\CartHdr::class, $hdrModel->id);
            throw $exc;
		}

		//commit the document
		$hdrModel = CartHdrRepository::commitToWip($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('SlsOrd.commit_to_wip_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\CartHdr::class, $hdrModel->id);
        throw $exc;
	}

	static public function transitionToComplete($hdrId)
    {
        AuthService::authorize(
			array(
				'sls_ord_confirm'
			)
        );

		$isSuccess = false;
		//use transaction to make sure this is latest value
		$hdrModel = CartHdrRepository::txnFindByPk($hdrId);

		//only DRAFT or above can transition to COMPLETE
		if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE']
		|| $hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('SlsOrd.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\CartHdr::class, $hdrModel->id);
            throw $exc;
		}

		$dtlModels = CartDtlRepository::findAllByHdrId($hdrModel->id);
		//preliminary checking
		//check the detail stock make sure the detail quant_bal_id > 0
		foreach($dtlModels as $dtlModel)
		{
			if($dtlModel->item_id == 0)
			{
				$exc = new ApiException(__('SlsOrd.item_is_required', ['lineNo'=>$dtlModel->line_no]));
				$exc->addData(\App\CartDtl::class, $dtlModel->id);
				throw $exc;
			}
		}

		//commit the document
		$hdrModel = CartHdrRepository::commitToComplete($hdrModel->id, true);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
        }
        
		$exc = new ApiException(__('SlsOrd.commit_to_complete_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\CartHdr::class, $hdrModel->id);
        throw $exc;
    }
    
    static public function transitionToDraft($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = CartHdrRepository::txnFindByPk($hdrId);

		//only WIP or COMPLETE can transition to DRAFT
		if($hdrModel->doc_status == DocStatus::$MAP['WIP'])
		{
            $hdrModel = CartHdrRepository::revertWipToDraft($hdrModel->id);
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		elseif($hdrModel->doc_status == DocStatus::$MAP['COMPLETE'])
		{
            AuthService::authorize(
                array(
                    'sls_ord_revert'
                )
            );

            $hdrModel = CartHdrRepository::revertCompleteToDraft($hdrModel->id);
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
        }
        elseif($hdrModel->doc_status == DocStatus::$MAP['VOID'])
		{
            AuthService::authorize(
                array(
                    'sls_ord_confirm'
                )
            );

			$hdrModel = CartHdrRepository::commitVoidToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
        
        $exc = new ApiException(__('SlsOrd.doc_status_is_not_wip_or_complete', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\CartHdr::class, $hdrModel->id);
		throw $exc;
    }

    public static function checkStock($siteFlow, $cartHdr, $cartDtls)
    {
        $deliveryPoint = $cartHdr->deliveryPoint;
        //check the inventory
        foreach($cartDtls as $cartDtl)
        {
            $item = $cartDtl->item;

            $pickingCriterias = PickingCriteriaService::processByDeliveryPointAndItem($siteFlow, $deliveryPoint, $item);

            //DB::connection()->enableQueryLog();
            $availUnitQty = QuantBalRepository::queryAvailUnitQtyBySiteIdAndLocationId(
                $cartHdr->company_id,
                $cartDtl->item_id,
                $siteFlow->site_id,
                $siteFlow->id,
                $cartDtl->location_id,
                $pickingCriterias
            );
            //dd(DB::getQueryLog());

            //DB::connection()->enableQueryLog();
            $rsvdUnitQty = OutbOrdHdrRepository::queryRsvdUnitQtyBySiteFlowId(
                $cartHdr->company_id,
                $cartDtl->item_id,
                $siteFlow->id,
                $cartDtl->location_id
            );
            //dd(DB::getQueryLog());

            $availUnitQty = bcsub($availUnitQty, $rsvdUnitQty);

            $cartDtl->request_qty = bcmul($cartDtl->qty, $cartDtl->uom_rate, $item->qty_scale);
            $cartDtl->available_qty = $availUnitQty;
        }

        return $cartDtls;
    }

    static public function transitionToVoid($hdrId)
    {
        AuthService::authorize(
            array(
                'sls_ord_revert'
            )
        );

		//use transaction to make sure this is latest value
		$hdrModel = CartHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('SlsOrd.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\CartHdr::class, $hdrModel->id);
            throw $exc;
		}

		//revert the document
		$hdrModel = CartHdrRepository::revertDraftToVoid($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('SlsOrd.commit_to_void_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\CartHdr::class, $hdrModel->id);
        throw $exc;
    }
    
    public function initHeader($divisionId)
    {
        AuthService::authorize(
            array(
                'sls_ord_create'
            )
        );

        $user = Auth::user();

        $model = new CartHdr();
        $model->doc_flows = array();
        $model->doc_status = DocStatus::$MAP['DRAFT'];
        $model->str_doc_status = DocStatus::$MAP[$model->doc_status];
        $model->doc_code = '';
        $model->ref_code_01 = '';
        $model->ref_code_02 = '';
        $model->ref_code_03 = '';
        $model->ref_code_04 = '';
        $model->ref_code_05 = '';
        $model->ref_code_06 = '';
        $model->doc_date = date('Y-m-d');
        $model->est_del_date = date('Y-m-d');
        $model->desc_01 = '';
        $model->desc_02 = '';

        $division = DivisionRepository::findByPk($divisionId);
        $model->division_id = $divisionId;
        $model->division_code = '';
        $model->site_flow_id = 0;
        $model->company_id = 0;
        $model->company_code = '';
        if(!empty($division))
        {
            $model->division_code = $division->code;
            $model->site_flow_id = $division->site_flow_id;
            $company = $division->company;
            $model->company_id = $company->id;
            $model->company_code = $company->code;
        }

        $model->doc_no_id = 0;
        $docNoIdOptions = array();
        $docNos = DocNoRepository::findAllDivisionDocNo($divisionId, \App\CartHdr::class, $model->doc_date);
        for($a = 0; $a < count($docNos); $a++)
        {
            $docNo = $docNos[$a];
            if($a == 0)
            {
                $model->doc_no_id = $docNo->id;
            }
            $docNoIdOptions[] = array('value'=>$docNo->id, 'label'=>$docNo->latest_code);
        }
        $model->doc_no_id_options = $docNoIdOptions;

        //currency dropdown
        $model->currency_id = 0;
        $model->currency_rate = 1;
        $initCurrencyOption = array('value'=>0,'label'=>'');
        $model->currency_select2 = $initCurrencyOption;

        //creditTerm select2
        $initCreditTermOption = array('value'=>0,'label'=>'');
        $model->credit_term_select2 = $initCreditTermOption;

        //salesman select2
        $initSalesmanOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($user))
        {
            $initSalesmanOption = array(
                'value'=>$user->id, 
                'label'=>$user->username
            );
        }
        $model->salesman_select2 = $initSalesmanOption;

        //delivery_point select2
        
        $model->delivery_point_unit_no = '';
        $model->delivery_point_building_name = '';
        $model->delivery_point_street_name = '';
        $model->delivery_point_district_01 = '';
        $model->delivery_point_district_02 = '';
        $model->delivery_point_postcode = '';
        $model->delivery_point_state_name = '';
        $model->delivery_point_country_name = '';
        $initDeliveryPointOption = array(
            'value'=>0,
            'label'=>''
        );
        $deliveryPoint = DeliveryPointRepository::findTop();
        if(!empty($deliveryPoint))
        {
            $initDeliveryPointOption = array(
                'value'=>$deliveryPoint->id, 
                'label'=>$deliveryPoint->code.' '.$deliveryPoint->company_name_01
            );

            $model->delivery_point_unit_no = $deliveryPoint->unit_no;
            $model->delivery_point_building_name = $deliveryPoint->building_name;
            $model->delivery_point_street_name = $deliveryPoint->street_name;
            $model->delivery_point_district_01 = $deliveryPoint->district_01;
            $model->delivery_point_district_02 = $deliveryPoint->district_02;
            $model->delivery_point_postcode = $deliveryPoint->postcode;
            $model->delivery_point_state_name = $deliveryPoint->state_name;
            $model->delivery_point_country_name = $deliveryPoint->country_name;
        }
        $model->delivery_point_select2 = $initDeliveryPointOption;

        $model->hdr_disc_val_01 = 0;
        $model->hdr_disc_perc_01 = 0;
        $model->hdr_disc_val_02 = 0;
        $model->hdr_disc_perc_02 = 0;
        $model->hdr_disc_val_03 = 0;
        $model->hdr_disc_perc_03 = 0;
        $model->hdr_disc_val_04 = 0;
        $model->hdr_disc_perc_04 = 0;
        $model->hdr_disc_val_05 = 0;
        $model->hdr_disc_perc_05 = 0;

        $model->disc_amt = 0;
        $model->tax_amt = 0;
        $model->round_adj_amt = 0;
        $model->net_amt = 0;

        return $model;
    }

    public function showHeader($hdrId)
    {
        AuthService::authorize(
            array(
                'sls_ord_read',
				'sls_ord_update'
            )
        );

		$model = CartHdrRepository::findByPk($hdrId);
		if(!empty($model))
        {
            $model = self::processOutgoingHeader($model);
        }
		return $model;
    }

    public function updateHeader($hdrData)
	{
        AuthService::authorize(
            array(
                'sls_ord_update'
            )
        );

        $hdrData = self::processIncomingHeader($hdrData);

		//get the associated inbOrdHdr
        $cartHdr = CartHdrRepository::txnFindByPk($hdrData['id']);
        if($cartHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('SlsOrd.doc_status_is_wip_or_complete', ['docCode'=>$cartHdr->doc_code]));
            $exc->addData(\App\CartHdr::class, $cartHdr->id);
            throw $exc;
        }

        $cartHdrData = $cartHdr->toArray();
        //assign hdrData to model
        foreach($hdrData as $field => $value) 
        {
            if(strpos($field, 'hdr_disc_val') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_disc_perc') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_tax_val') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_tax_perc') !== false)
            {
                continue;
            }
            $cartHdrData[$field] = $value;
        }

		//$cartHdr->load('inbOrdHdr', 'bizPartner');
        //$bizPartner = $cartHdr->bizPartner;
        
        $cartDtlArray = array();

        //check if hdr disc or hdr tax is set and more than 0
        $needToProcessDetails = false;
        $firstDtlModel = CartDtlRepository::findTopByHdrId($hdrData['id']);
        if(!empty($firstDtlModel))
        {
            $firstDtlData = $firstDtlModel->toArray();
            for($a = 1; $a <= 5; $a++)
            {
                if(array_key_exists('hdr_disc_val_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_disc_val_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_disc_val_0'.$a], $firstDtlData['hdr_disc_val_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
                if(array_key_exists('hdr_disc_perc_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_disc_perc_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_disc_perc_0'.$a], $firstDtlData['hdr_disc_perc_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }

                if(array_key_exists('hdr_tax_val_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_tax_val_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_tax_val_0'.$a], $firstDtlData['hdr_tax_val_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
                if(array_key_exists('hdr_tax_perc_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_tax_perc_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_tax_perc_0'.$a], $firstDtlData['hdr_tax_perc_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
            }
        }

        if($needToProcessDetails)
        {
            //query the cartDtlModels from DB
            //format to array, with is_modified field to indicate it is changed
            $cartDtlModels = CartDtlRepository::findAllByHdrId($hdrData['id']);
            foreach($cartDtlModels as $cartDtlModel)
            {
                $cartDtlData = $cartDtlModel->toArray();
                for($a = 1; $a <= 5; $a++)
                {
                    if(array_key_exists('hdr_disc_val_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_disc_val_0'.$a], 0, 5) > 0)
                    {
                        $cartDtlData['hdr_disc_val_0'.$a] = $hdrData['hdr_disc_val_0'.$a];
                    }
                    if(array_key_exists('hdr_disc_perc_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_disc_perc_0'.$a], 0, 5) > 0)
                    {
                        $cartDtlData['hdr_disc_perc_0'.$a] = $hdrData['hdr_disc_perc_0'.$a];
                    }

                    if(array_key_exists('hdr_tax_val_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_tax_val_0'.$a], 0, 5) > 0)
                    {
                        $cartDtlData['hdr_tax_val_0'.$a] = $hdrData['hdr_tax_val_0'.$a];
                    }
                    if(array_key_exists('hdr_tax_perc_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_tax_perc_0'.$a], 0, 5) > 0)
                    {
                        $cartDtlData['hdr_tax_perc_0'.$a] = $hdrData['hdr_tax_perc_0'.$a];
                    }
                }
                $cartDtlData['is_modified'] = 1;
                $cartDtlArray[] = $cartDtlData;
            }
            
            //calculate details amount, use adaptive rounding tax
            $result = $this->calculateAmount($cartHdrData, $cartDtlArray);
            $cartHdrData = $result['hdrData'];
            $cartDtlArray = $result['dtlArray'];
        }
            
        $result = CartHdrRepository::updateDetails($cartHdrData, $cartDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];

        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('SlsOrd.document_successfully_updated', ['docCode'=>$documentHeader->doc_code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }
    
    public function createHeader($data)
    {		
        // AuthService::authorize(
        //     array(
        //         'sls_ord_create'
        //     )
        // );

        //$data = self::processIncomingHeader($data, true);
        $docNoId = $data['doc_no_id'];
        unset($data['doc_no_id']);
        $ordHdrModel = CartHdrRepository::createHeader(ProcType::$MAP['NULL'], $docNoId, $data);

        $message = __('SlsOrd.document_successfully_created', ['docCode'=>$ordHdrModel->doc_code]);

		return array(
			'data' => $ordHdrModel->id,
			'message' => $message
		);
    }

    public function changeDeliveryPoint($deliveryPointId)
    {
        AuthService::authorize(
            array(
                'sls_ord_update'
            )
        );

        $results = array();
        $deliveryPoint = DeliveryPointRepository::findByPk($deliveryPointId);
        if(!empty($deliveryPoint))
        {
            $results['delivery_point_unit_no'] = $deliveryPoint->unit_no;
            $results['delivery_point_building_name'] = $deliveryPoint->building_name;
            $results['delivery_point_street_name'] = $deliveryPoint->street_name;
            $results['delivery_point_district_01'] = $deliveryPoint->district_01;
            $results['delivery_point_district_02'] = $deliveryPoint->district_02;
            $results['delivery_point_postcode'] = $deliveryPoint->postcode;
            $results['delivery_point_state_name'] = $deliveryPoint->state_name;
            $results['delivery_point_country_name'] = $deliveryPoint->country_name;
        }
        return $results;
    }

    public function changeItem($hdrId, $itemId)
    {
        AuthService::authorize(
            array(
                'sls_ord_update'
            )
        );

        $cartHdr = CartHdrRepository::findByPk($hdrId);

        $results = array();
        $item = ItemRepository::findByPk($itemId);
        if(!empty($item)
        && !empty($cartHdr))
        {
            $results['desc_01'] = $item->desc_01;
            $results['desc_02'] = $item->desc_02;
        }
        return $results;
    }

    public function changeItemUom($hdrId, $itemId, $uomId)
    {
        AuthService::authorize(
            array(
                'sls_ord_update'
            )
        );

        $cartHdr = CartHdrRepository::findByPk($hdrId);

        $results = array();
        $itemUom = ItemUomRepository::findByItemIdAndUomId($itemId, $uomId);
        if(!empty($itemUom)
        && !empty($cartHdr))
        {
            $results['item_id'] = $itemUom->item_id;
            $results['uom_id'] = $itemUom->uom_id;
            $results['uom_rate'] = $itemUom->uom_rate;

            $results = $this->updateSaleItemUom($results, $cartHdr->doc_date, 0, $cartHdr->currency_id, 0, 0);
        }
        return $results;
	}
	
	public function changeCurrency($currencyId)
    {
        AuthService::authorize(
            array(
                'sls_ord_update'
            )
        );

        $results = array();
        $currency = CurrencyRepository::findByPk($currencyId);
        if(!empty($currency))
        {
            $results['currency_rate'] = $currency->currency_rate;
        }
        return $results;
    }

    public function createDetail($hdrId, $data)
	{
        AuthService::authorize(
            array(
                'sls_ord_update'
            )
        );

        $item = ItemRepository::findByPk($data['item_id']);
        $data = self::processIncomingDetail($data, $item);

        $cartHdr = CartHdrRepository::txnFindByPk($hdrId);
        if($cartHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('SlsOrd.doc_status_is_wip_or_complete', ['docCode'=>$cartHdr->doc_code]));
            $exc->addData(\App\CartHdr::class, $cartHdr->id);
            throw $exc;
        }
		$cartHdrData = $cartHdr->toArray();

		$cartDtlArray = array();
		$cartDtlModels = CartDtlRepository::findAllByHdrId($hdrId);
		foreach($cartDtlModels as $cartDtlModel)
		{
			$cartDtlData = $cartDtlModel->toArray();
			$cartDtlData['is_modified'] = 0;
			$cartDtlArray[] = $cartDtlData;
		}
		$lastLineNo = count($cartDtlModels);

		//assign temp id
		$tmpUuid = uniqid();
		//append NEW here to make sure it will be insert in repository later
		$data['id'] = 'NEW'.$tmpUuid;
		$data['line_no'] = $lastLineNo + 1;
        $data['is_modified'] = 1;

        unset($data['sale_price']); //always get latest price

		//$data = $this->updateSaleItemUom($data, $cartHdr['doc_date'], 0, $cartHdr['currency_id'], 0, 0);
		$data = $this->updateSaleItemUom($data, now(), 0, $cartHdr['currency_id'], 0, 0);
		$cartDtlData = $this->initCartDtlData($data);
		$cartDtlArray[] = $cartDtlData;

		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($cartHdrData, $cartDtlArray);
		$cartHdrData = $result['hdrData'];
        $cartDtlArray = $result['dtlArray'];

		$result = CartHdrRepository::updateDetails($cartHdrData, $cartDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('SlsOrd.detail_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function deleteDetails($hdrId, $dtlArray)
	{
        AuthService::authorize(
            array(
                'sls_ord_update'
            )
        );

        $cartHdr = CartHdrRepository::txnFindByPk($hdrId);
        if($cartHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('SlsOrd.doc_status_is_wip_or_complete', ['docCode'=>$cartHdr->doc_code]));
            $exc->addData(\App\CartHdr::class, $cartHdr->id);
            throw $exc;
        }
		$cartHdrData = $cartHdr->toArray();

		//query the cartDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$delCartDtlArray = array();
		$cartDtlArray = array();
        $cartDtlModels = CartDtlRepository::findAllByHdrId($hdrId);
        $lineNo = 1;
		foreach($cartDtlModels as $cartDtlModel)
		{
			$cartDtlData = $cartDtlModel->toArray();
			$cartDtlData['is_modified'] = 0;
			$cartDtlData['is_deleted'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{
				if($dtlData['id'] == $cartDtlData['id'])
				{
					//this is deleted dtl
					$cartDtlData['is_deleted'] = 1;
					break;
				}
			}
			if($cartDtlData['is_deleted'] > 0)
			{
				$delCartDtlArray[] = $cartDtlData;
			}
			else
			{
                $cartDtlData['line_no'] = $lineNo;
                $cartDtlData['is_modified'] = 1;
                $cartDtlArray[] = $cartDtlData;
                $lineNo++;
			}
        }
        
		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($cartHdrData, $cartDtlArray, $delCartDtlArray);
		$cartHdrData = $result['hdrData'];
		$modifiedDtlArray = $result['dtlArray'];
		foreach($cartDtlArray as $cartDtlSeq => $cartDtlData)
		{
			foreach($modifiedDtlArray as $modifiedDtlData)
			{
				if($modifiedDtlData['id'] == $cartDtlData['id'])
				{
					foreach($modifiedDtlData as $field => $value)
					{
						$cartDtlData[$field] = $value;
					}
					$cartDtlData['is_modified'] = 1;
					$cartDtlArray[$cartDtlSeq] = $cartDtlData;
					break;
				}
			}
		}
		
		$result = CartHdrRepository::updateDetails($cartHdrData, $cartDtlArray, $delCartDtlArray);
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('SlsOrd.details_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>$delCartDtlArray
            ),
			'message' => $message
		);
    }

    public function updateDetails($hdrId, $dtlArray)
	{
        AuthService::authorize(
            array(
                'sls_ord_update'
            )
        );

        $cartHdr = CartHdrRepository::txnFindByPk($hdrId);
        if($cartHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('SlsOrd.doc_status_is_wip_or_complete', ['docCode'=>$cartHdr->doc_code]));
            $exc->addData(\App\CartHdr::class, $cartHdr->id);
            throw $exc;
        }
		$cartHdrData = $cartHdr->toArray();

		//query the cartDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$cartDtlArray = array();
        $cartDtlModels = CartDtlRepository::findAllByHdrId($hdrId);
		foreach($cartDtlModels as $cartDtlModel)
		{
			$cartDtlData = $cartDtlModel->toArray();
			$cartDtlData['is_modified'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{                
				if($dtlData['id'] == $cartDtlData['id'])
				{
                    $item = ItemRepository::findByPk($dtlData['item_id']);
                    $dtlData = self::processIncomingDetail($dtlData, $item);
                    
                    unset($dtlData['sale_price']); //always get latest price
					// $dtlData = $this->updateSaleItemUom($dtlData, $cartHdr->doc_date, 0, $cartHdr->currency_id, 0, 0);
					$dtlData = $this->updateSaleItemUom($dtlData, now(), 0, $cartHdr->currency_id, 0, 0);
					foreach($dtlData as $fieldName => $value)
					{
						$cartDtlData[$fieldName] = $value;
					}
					$cartDtlData['is_modified'] = 1;

					break;
				}
			}
			$cartDtlArray[] = $cartDtlData;
		}
		
		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($cartHdrData, $cartDtlArray);
		$cartHdrData = $result['hdrData'];
		$cartDtlArray = $result['dtlArray'];
		
		$result = CartHdrRepository::updateDetails($cartHdrData, $cartDtlArray, array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('SlsOrd.details_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }

    public function showDetails($hdrId) 
	{
        AuthService::authorize(
            array(
                'sls_ord_read',
				'sls_ord_update'
            )
        );

		$cartDtls = CartDtlRepository::findAllByHdrId($hdrId);
		$cartDtls->load(
            'item', 'item.itemUoms', 'item.itemUoms.uom'
        );
        foreach($cartDtls as $cartDtl)
        {
            $cartDtl = self::processOutgoingDetail($cartDtl);
        }
        return $cartDtls;
    }
    
    // protected function checkSlsOrdWithInverze($divisionId, $userId)
    // {
    //     $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['SLS_ORD_SYNC_01'], $userId);

    //     $syncSettingHdr = SyncSettingHdrRepository::findByProcTypeAndDivisionId(ProcType::$MAP['SYNC_EFICHAIN_01'], $divisionId);
    //     if(empty($syncSettingHdr))
    //     {
    //         $exc = new ApiException(__('SlsOrd.sync_setting_not_found', ['divisionId'=>$divisionId]));
    //         //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
    //         throw $exc;
    //     }

    //     $division = DivisionRepository::findByPk($divisionId);
    //     $company = $division->company;
    //     $divUrl = '&divisions[]='.$division->code;

    //     $client = new Client();
    //     $header = array();

    //     $page = 1;
    //     $lastPage = 1;
    //     $pageSize = 1000;
    //     $total = 0;

    //     $usernameSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'username');
    //     $passwordSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'password');
    //     $formParams = array(
    //         'UserLogin[username]' => $usernameSetting->value,
    //         'UserLogin[password]' => $passwordSetting->value
    //     );
    //     $pageSizeSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'page_size');
    //     if(!empty($pageSizeSetting))
    //     {
    //         $pageSize = $pageSizeSetting->value;
    //     }

    //     //query from efichain, and update the sales order accordingly
    //     $cartHdrDataList = array();
    //     while($page <= $lastPage)
    //     {
    //         $url = $syncSettingHdr->url.'/index.php?r=luuWu/getAllSalesOrders&start_created_date=2019-07-01&end_created_date=2019-08-31&page_size='.$pageSize.'&page='.$page.$divUrl;
            
    //         $response = $client->request('POST', $url, array('headers' => $header, 'form_params' => $formParams));
    //         $result = $response->getBody()->getContents();
    //         $result = json_decode($result, true);
    //         if(empty($result))
    //         {
    //             $exc = new ApiException(__('SlsOrd.fail_to_sync', ['url'=>$url]));
    //             //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
    //             throw $exc;
    //         }

    //         $total = $result['total'];
    //         for ($a = 0; $a < count($result['data']); $a++)
    //         {
    //             $cartHdrData = $result['data'][$a];
    //             //DRAFT
    //             $cartHdrModel = CartHdrRepository::checkSlsOrdWithInverze($company, $division, $cartHdrData);

    //             $statusNumber = 100;
    //             if($total > 0)
    //             {
    //                 $statusNumber = bcmul(bcdiv(($a + ($page - 1) * $pageSize), $total, 8), 100, 2);
    //             }
    //             BatchJobStatusRepository::updateStatusNumber($batchJobStatusModel->id, $statusNumber);
    //         }

    //         $lastPage = $total % $pageSize == 0 ? ($total / $pageSize) : ($total / $pageSize) + 1;

    //         $page++;
    //     }

    //     return $batchJobStatusModel;
    // }

    public function index($divisionId, $sorts, $filters = array(), $pageSize = 20)
	{
        // AuthService::authorize(
        //     array(
        //         'sls_ord_read'
        //     )
        // );
        
        //DB::connection()->enableQueryLog();
        $cartHdrs = CartHdrRepository::findAll($divisionId, $sorts, $filters, $pageSize);
        $cartHdrs->load(
            'cartDtls'
            //'cartDtls.item', 'cartDtls.item.itemUoms', 'cartDtls.item.itemUoms.uom', 
        );
		foreach($cartHdrs as $cartHdr)
		{
			$cartHdr->str_doc_status = DocStatus::$MAP[$cartHdr->doc_status];

            $deliveryPoint = $cartHdr->deliveryPoint;
            if (!empty($deliveryPoint))
            {
                $cartHdr->delivery_point_code = $deliveryPoint->code;
                $cartHdr->delivery_point_company_name_01 = $deliveryPoint->company_name_01;
                $cartHdr->delivery_point_area_code = $deliveryPoint->area_code;
                $area = $deliveryPoint->area;
                if(!empty($area))
                {
                    $cartHdr->delivery_point_area_desc_01 = $area->desc_01;
                }
            }

			// $salesman = $cartHdr->salesman;
            // $cartHdr->salesman_username = $salesman->username;
            
            $user = $cartHdr->user;
            $cartHdr->created_username = $user->username; 
            $cartHdr->created_fullname = $user->first_name . ' ' . $user->last_name; 
            
            $debtor = $cartHdr->debtor;
            $cartHdr->debtor_code = $debtor->code; 
            $cartHdr->debtor_companyname = $debtor->company_name_01; 

			$cartDtls = $cartHdr->cartDtls;
			$cartHdr->details = $cartDtls;
        }
        //Log::error(DB::getQueryLog());
    	return $cartHdrs;
    }
    
    static public function processIncomingDetail($data, $item)
    {
        $data['qty'] = round($data['qty'], $item->qty_scale);

        //preprocess the select2 and dropDown
        if(array_key_exists('item_select2', $data))
        {
            $data['item_id'] = $data['item_select2']['value'];
            unset($data['item_select2']);
        }
        if(array_key_exists('uom_select2', $data))
        {
            $data['uom_id'] = $data['uom_select2']['value'];
            unset($data['uom_select2']);
        }
        if(array_key_exists('item_code', $data))
        {
            unset($data['item_code']);
        }
        if(array_key_exists('uom_code', $data))
        {
            unset($data['uom_code']);
        }
        if(array_key_exists('item', $data))
        {
            unset($data['item']);
        }
        if(array_key_exists('uom', $data))
        {
            unset($data['uom']);
		}
		
		if(array_key_exists('item_code', $data))
        {
            unset($data['item_code']);
		}
		if(array_key_exists('item_ref_code_01', $data))
        {
            unset($data['item_ref_code_01']);
		}
		if(array_key_exists('item_desc_01', $data))
        {
            unset($data['item_desc_01']);
		}
		if(array_key_exists('item_desc_02', $data))
        {
            unset($data['item_desc_02']);
		}
		if(array_key_exists('storage_class', $data))
        {
            unset($data['storage_class']);
		}
		if(array_key_exists('item_group_01_code', $data))
        {
            unset($data['item_group_01_code']);
		}
		if(array_key_exists('item_group_02_code', $data))
        {
            unset($data['item_group_02_code']);
		}
		if(array_key_exists('item_group_03_code', $data))
        {
            unset($data['item_group_03_code']);
		}
		if(array_key_exists('item_group_04_code', $data))
        {
            unset($data['item_group_04_code']);
		}
		if(array_key_exists('item_group_05_code', $data))
        {
            unset($data['item_group_05_code']);
		}
		if(array_key_exists('item_cases_per_pallet_length', $data))
        {
            unset($data['item_cases_per_pallet_length']);
		}
		if(array_key_exists('item_cases_per_pallet_width', $data))
        {
            unset($data['item_cases_per_pallet_width']);
		}
		if(array_key_exists('item_no_of_layers', $data))
        {
            unset($data['item_no_of_layers']);
		}

		if(array_key_exists('unit_qty', $data))
        {
            unset($data['unit_qty']);
		}
		if(array_key_exists('item_unit_uom_code', $data))
        {
            unset($data['item_unit_uom_code']);
		}
		if(array_key_exists('loose_qty', $data))
        {
            unset($data['loose_qty']);
		}
		if(array_key_exists('item_loose_uom_code', $data))
        {
            unset($data['item_loose_uom_code']);
        }
        if(array_key_exists('loose_uom_id', $data))
        {
            unset($data['loose_uom_id']);
		}
		if(array_key_exists('loose_uom_rate', $data))
        {
            unset($data['loose_uom_rate']);
		}
		if(array_key_exists('case_qty', $data))
        {
            unset($data['case_qty']);
		}
		if(array_key_exists('case_uom_rate', $data))
        {
            unset($data['case_uom_rate']);
		}
        if(array_key_exists('item_case_uom_code', $data))
        {
            unset($data['item_case_uom_code']);
        }
        if(array_key_exists('uom_code', $data))
        {
            unset($data['uom_code']);
		}
		if(array_key_exists('item_unit_barcode', $data))
        {
            unset($data['item_unit_barcode']);
		}
		if(array_key_exists('item_case_barcode', $data))
        {
            unset($data['item_case_barcode']);
		}
		if(array_key_exists('gross_weight', $data))
        {
            unset($data['gross_weight']);
		}
		if(array_key_exists('cubic_meter', $data))
        {
            unset($data['cubic_meter']);
		}
		
		if(array_key_exists('location_code', $data))
        {
            unset($data['location_code']);
		}
		if(array_key_exists('location', $data))
        {
            unset($data['location']);
		}		
        return $data;
    }

    static public function processIncomingHeader($data, $isClearHdrDiscTax=false)
    {
        if(array_key_exists('doc_flows', $data))
        {
            unset($data['doc_flows']);
        }
        if(array_key_exists('submit_action', $data))
        {
            unset($data['submit_action']);
        }
        //preprocess the select2 and dropDown
        if(array_key_exists('credit_term_select2', $data))
        {
            $data['credit_term_id'] = $data['credit_term_select2']['value'];
            unset($data['credit_term_select2']);
        }
        if(array_key_exists('currency_select2', $data))
        {
            $data['currency_id'] = $data['currency_select2']['value'];
            unset($data['currency_select2']);
        }   
        if(array_key_exists('salesman_select2', $data))
        {
            $data['salesman_id'] = $data['salesman_select2']['value'];
            unset($data['salesman_select2']);
        }
        if(array_key_exists('delivery_point_select2', $data))
        {
            $data['delivery_point_id'] = $data['delivery_point_select2']['value'];
            unset($data['delivery_point_select2']);
        }
        if(array_key_exists('str_doc_status', $data))
        {
            unset($data['str_doc_status']);
        }
        if(array_key_exists('division_code', $data))
        {
            unset($data['division_code']);
        }
        if(array_key_exists('company_code', $data))
        {
            unset($data['company_code']);
        }
        // if(array_key_exists('delivery_point_unit_no', $data))
        // {
        //     unset($data['delivery_point_unit_no']);
        // }
        // if(array_key_exists('delivery_point_building_name', $data))
        // {
        //     unset($data['delivery_point_building_name']);
        // }
        // if(array_key_exists('delivery_point_street_name', $data))
        // {
        //     unset($data['delivery_point_street_name']);
        // }
        // if(array_key_exists('delivery_point_district_01', $data))
        // {
        //     unset($data['delivery_point_district_01']);
        // }
        // if(array_key_exists('delivery_point_district_02', $data))
        // {
        //     unset($data['delivery_point_district_02']);
        // }
        // if(array_key_exists('delivery_point_postcode', $data))
        // {
        //     unset($data['delivery_point_postcode']);
        // }
        // if(array_key_exists('delivery_point_state_name', $data))
        // {
        //     unset($data['delivery_point_state_name']);
        // }
        // if(array_key_exists('delivery_point_country_name', $data))
        // {
        //     unset($data['delivery_point_country_name']);
        // }
        if(array_key_exists('division', $data))
        {
            unset($data['division']);
        }
        if(array_key_exists('company', $data))
        {
            unset($data['company']);
        }
        if(array_key_exists('salesman', $data))
        {
            unset($data['salesman']);
        }
        if(array_key_exists('delivery_point', $data))
        {
            unset($data['delivery_point']);
        }

        if($isClearHdrDiscTax)
        {  
            for($a = 1; $a <= 5; $a++)
            {
                if(array_key_exists('hdr_disc_val_0'.$a, $data))
                {
                    unset($data['hdr_disc_val_0'.$a]);
                }
                if(array_key_exists('hdr_disc_perc_0'.$a, $data))
                {
                    unset($data['hdr_disc_perc_0'.$a]);
                }
                if(array_key_exists('hdr_tax_val_0'.$a, $data))
                {
                    unset($data['hdr_tax_val_0'.$a]);
                }
                if(array_key_exists('hdr_tax_perc_0'.$a, $data))
                {
                    unset($data['hdr_tax_perc_0'.$a]);
                }
            }
        }
        return $data;
    }

    public function findOrCreateDraftHeader($data)
    {
        $user = Auth::user();
        //$data = self::processIncomingHeader($data, true);
        $divisionId = $data['division_id'];
        $userId = $user->id;
        $debtorId = $user->debtor_id;
        $cartHdr = CartHdrRepository::findOrCreateDraftHeader($divisionId, $userId, $debtorId); //returns an key value pair of 'cartHdr' and 'conflicts'

        $this->processOutgoingSuggestions($cartHdr['cartHdr']->cartDtls);

        $message = __('SlsOrd.document_successfully_created', ['docCode'=>$cartHdr['cartHdr']->doc_code]);

		return array(
			'data' => array(
                'cartHdr'=>$cartHdr['cartHdr'],
                // 'conflicts' => $cartHdr['conflicts'],
                // 'variant_flag' => $cartHdr['variant_flag']
            ),
			'message' => $message
		);
    }
    
    public function processOutgoingSuggestions($dtlArray) {
        $item_id = 0;

        foreach ($dtlArray as $dtl) {
            $suggestions = array();

            if ($item_id !== $dtl->item_id) {
                $data = $this->checkPromotion($dtl->id);
                $promotions = $data['data']['promotions'];
	
				if (count($promotions) > 0) {
					$item = $dtl->item;
					$itemUoms = $item->itemUoms->reverse();

					foreach ($promotions as $promotion) {
						if (!$promotion['entitled']) {
							$qty = $promotion['validation']['qty'];

							$bal = $qty;
							
							$uomQty = []; 
							foreach ($itemUoms as $itemUom) {
								if ($bal > 0) {
									$quotient = (int) ($bal / $itemUom->uom_rate);
									if ($quotient > 0) {
										$bal -= $quotient * $itemUom->uom_rate;
										$uomQty[] = $quotient . ' ' . $itemUom->uom->code;
									}
								}
							}
							
							// TODO Remove req_qty once app is updated
							array_push($suggestions, array(
								'promo_code'=>$promotion['promotion']['code'],
								'promo_desc_01'=>$promotion['promotion']['desc_01'],
								'promo_desc_02'=>$promotion['promotion']['desc_02'],
								'type'=>$promotion['validation']['type'],
								'qty'=>$qty,
								'uom_qty'=>implode(', ', $uomQty),
								'req_qty'=>$qty,
							));
						}
					}
				}

                $item_id = $dtl->item_id;
            }

            $dtl['suggestions'] = $suggestions;
        }
    }

    public function updateCartItem($hdrId, $dtlArray)
    {
        //$data = self::processIncomingHeader($data, true);
        $cartHdr = CartHdrRepository::txnFindByPk($hdrId);
        if($cartHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('SlsOrd.doc_status_is_wip_or_complete', ['docCode'=>$cartHdr->doc_code]));
            $exc->addData(\App\CartHdr::class, $cartHdr->id);
            throw $exc;
        }
        $cartHdrData = $cartHdr->toArray();

        $updateMode = '';

		//query the cartDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
        $cartDtlModels = CartDtlRepository::findAllByHdrId($hdrId);
        $dtlToCreateArray = array();
        $dtlToUpdateArray = array();
        $dtlToDeleteArray = array();
        foreach($dtlArray as $dtlData)
        {               
            $mode = 'create'; 
            foreach($cartDtlModels as $cartDtlModel)
            {
                $cartDtlData = $cartDtlModel->toArray();
                //check with going to updated records, to assign value to data, and set is_modified = true
                
                if($dtlData['item_id'] == $cartDtlData['item_id'] 
                    && $dtlData['uom_id'] == $cartDtlData['uom_id']
                    && $cartDtlData['net_amt'] > 0.00)
                {
                    $item = ItemRepository::findByPk($dtlData['item_id']);
                    $dtlData = self::processIncomingDetail($dtlData, $item);
                    //update item description
                    $dtlData['desc_01'] = $item->desc_01;
                    $dtlData['desc_02'] = $item->desc_02;
                    
                    if ($dtlData['qty'] > 0) 
                    {
                        $dtlData['id'] = $cartDtlData['id'];
                        $dtlToUpdateArray[] = $dtlData;
                        $mode = 'update'; 
                    }
                    else if ($dtlData['qty'] == 0)
                    {
                        //delete item
                        $dtlData['id'] = $cartDtlData['id'];
                        $dtlToDeleteArray[] = $dtlData;
                        $mode = 'delete'; 
                    }
                }
            }
            if ($mode == 'create') {
                if ($dtlData['qty'] > 0) 
                {
                    $item = ItemRepository::findByPk($dtlData['item_id']);
                    $dtlData = self::processIncomingDetail($dtlData, $item);
                    //update item description
                    $dtlData['desc_01'] = $item->desc_01;
                    $dtlData['desc_02'] = $item->desc_02;
                    $dtlToCreateArray[] = $dtlData;
                }
            }
        }
        
        $result = array();
        foreach($dtlToCreateArray as $dtlData)
        {       
            $result[] = $this->createDetail($hdrId, $dtlData);   
        }
        if (!empty($dtlToUpdateArray)) {
            $result[] = $this->updateDetails($hdrId, $dtlToUpdateArray);
        }
        if (!empty($dtlToDeleteArray)) {
            $result[] = $this->deleteDetails($hdrId, $dtlToDeleteArray);
        }
        
        //apply promotion at the end
        $result[] = $this->applyPromotion($hdrId);

        if (count($result) > 0) {
            return $result[0];
        }
        else {
            return null;
        }
    }

    public function reapplyCartPromotions($hdrId, $dtlArray)
    {
        //$data = self::processIncomingHeader($data, true);
        $cartHdr = CartHdrRepository::txnFindByPk($hdrId);
        if($cartHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('SlsOrd.doc_status_is_wip_or_complete', ['docCode'=>$cartHdr->doc_code]));
            $exc->addData(\App\CartHdr::class, $cartHdr->id);
            throw $exc;
        }
        $cartHdrData = $cartHdr->toArray();

        $result[] = $this->applyPromotion($hdrId);

        // $cartDtlModels = CartDtlRepository::findAllByHdrId($hdrId);
        // $cartDtlArray = array();
        // foreach($dtlArray as $dtlData) {
        //     foreach($cartDtlModels as $cartDtlModel)
        //     {
        //         $cartDtlModel->promotions;
        //         $cartDtlData = $cartDtlModel->toArray();

        //         if($cartDtlData['item_id'] == $dtlData['item_id'])
        //         {
        //             $promo_exists = false;

        //             foreach($dtlData['promotions'] as $promotion1) {
        //                 foreach($cartDtlData['promotions'] as $promotion2) {
        //                     if($promotion1 == $promotion2['promotion_id']) {
        //                         $promo_exists = true;
        //                         break 2;
        //                     }
        //                 }
        //             }

        //             if(!$promo_exists) {
        //                 $cartDtlModel = CartDtlRepository::disableCartDtlByPromotionId($cartDtlModel->id);
        //             }
                    
        //             $cartDtlArray[] = $cartDtlModel;
        //         }
        //     }
        // }
        
        return $result[0];
    }

    public function applyConflict($data)
    {
        $result = array();

        foreach($data as $id) {
            $result[] = CartDtlRepository::disableCartDtlByPromotionId($id);
        }

        return $result;
    }

    public function checkPromotion($dtlId)
    {
        $cartDtlModel = CartDtlRepository::findByPk($dtlId);
        $cartDtlData = $cartDtlModel->toArray();
        $cartHdr = CartHdrRepository::findByPk($cartDtlData['hdr_id']);
        $cartHdrData = $cartHdr->toArray();
        
        $promotions = PromotionRepository::findAllActiveByItem($cartDtlData['item_id']);

        $promoDataArray = array();

        //check promotion rule eligible
        $promotions = $promotions->filter(function ($promotion) use ($cartHdrData) {
            $rules = $promotion->rules;
            if ($rules->isEmpty()) {
                //empty rules means all are entitled for promotion
                return true;
            }
            foreach ($rules as $rule) {
                // failing any one of rules will return false
                // DebtorChecker
                if ($rule->type == PromotionRuleType::$MAP['debtor']) {
                    return (new DebtorChecker($cartHdrData))->isEligible($rule->config);
                }
                if ($rule->type == PromotionRuleType::$MAP['debtor_group']) {
                    return (new DebtorGroupChecker($cartHdrData))->isEligible($rule->config);
                }
            }
        });
        
        // execute action for each promotion to order
        foreach ($promotions as $promotion) {
            $actions = $promotion->actions;
            foreach ($actions as $action) {
                if ($action->type == PromotionActionType::$MAP['item_percent_disc'] ||
                    $action->type == PromotionActionType::$MAP['item_fixed_disc']) {
                        
                    $qtyValidation = true;
                    //check promo action has buy_qty (min assorted qty)
                    if ($action->type == PromotionActionType::$MAP['item_percent_disc'] && 
                        isset($action->config['buy_qty']) && $action->config['buy_qty'] > 0) {
                        $buy_qty = $action->config['buy_qty'];
                        $allVariants = PromotionVariantRepository::findAllEnabled($promotion->id);
                        $itemIds = array();
                        foreach ($allVariants as $variant) {
                            $itemIds[] = $variant->item_id;
                        }
                        $itemTotalQty = CartDtlRepository::getTotalItemArrayQty($cartDtlData['hdr_id'], $itemIds);
                        if ($itemTotalQty < $buy_qty) {
                            $qtyValidation = false;
                            $requiredQty = $buy_qty - $itemTotalQty;
                            $data = [
                                'promotion'=>$promotion,
                                'entitled'=>false,
                                'validation'=>['type'=>'min_qty', 'qty'=>$requiredQty],
                                'action'=>['type'=>PromotionActionType::$MAP[$action->type], 'config'=>$action->config]
                            ];
                            $promoDataArray[] = $data;
                        }
                        else
                        {
                            $data = [
                                'promotion'=>$promotion,
                                'entitled'=>true,
                                'validation'=>[],
                                'action'=>['type'=>PromotionActionType::$MAP[$action->type], 'config'=>$action->config]
                            ];
                            $promoDataArray[] = $data;
                        }
                    }
                    else {
                        //get promotion variants matching item id
                        $promotionVariant = PromotionVariantRepository::findEnabledByItemAndGroup($promotion->id, $cartDtlData['item_id']);
    
                        if (!empty($promotionVariant))
                        {
                            if ($promotionVariant->min_qty > 0 || $promotionVariant->max_qty > 0) {
                                //get the total item qty
                                $itemTotalQty = CartDtlRepository::getTotalItemQty($cartDtlData['hdr_id'], $cartDtlData['item_id']);
                                if ($promotionVariant->min_qty > 0 && $itemTotalQty < $promotionVariant->min_qty) {
                                    $qtyValidation = false;
                                    $requiredQty = $promotionVariant->min_qty - $itemTotalQty;
                                    $data = [
                                        'promotion'=>$promotion,
                                        'entitled'=>false,
                                        'validation'=>['type'=>'min_qty', 'qty'=>$requiredQty],
                                        'action'=>['type'=>PromotionActionType::$MAP[$action->type], 'config'=>$action->config]
                                    ];
                                    $promoDataArray[] = $data;
                                }
                                else if ($promotionVariant->max_qty > 0 && $itemTotalQty > $promotionVariant->max_qty) {
                                    $qtyValidation = false;
                                    $exceededQty = $itemTotalQty - $promotionVariant->max_qty;
                                    $data = [
                                        'promotion'=>$promotion,
                                        'entitled'=>false,
                                        'validation'=>['type'=>'max_qty', 'qty'=>$exceededQty],
                                        'action'=>['type'=>PromotionActionType::$MAP[$action->type], 'config'=>$action->config]
                                    ];
                                    $promoDataArray[] = $data;
                                }
                            }
                            if ($qtyValidation) {
                                $data = [
                                    'promotion'=>$promotion,
                                    'entitled'=>true,
                                    'validation'=>[],
                                    'action'=>['type'=>PromotionActionType::$MAP[$action->type], 'config'=>$action->config]
                                ];
                                $promoDataArray[] = $data;
                            }
                        }
                    }
                }
                else if ($action->type == PromotionActionType::$MAP['item_foc']) {
                    $promotionVariant = PromotionVariantRepository::findEnabledMainByItem($promotion->id, $cartDtlData['item_id']);
                    if (!empty($promotionVariant))
                    {
                        $buy_qty = $action->config['buy_qty'];
                        $buy_qty = floatval($buy_qty);
                        $foc_qty = $action->config['foc_qty'];
                        $foc_qty = floatval($foc_qty);

                        $allVariants = PromotionVariantRepository::findAllEnabledMain($promotion->id);
                        $itemIds = array();
                        foreach ($allVariants as $variant) {
                            $itemIds[] = $variant->item_id;
                        }
                        //
                        $itemTotalQty = CartDtlRepository::getTotalItemArrayQty($cartDtlData['hdr_id'], $itemIds);
                        if ($itemTotalQty >= $buy_qty) {
                            $data = [
                                'promotion'=>$promotion,
                                'entitled'=>true,
                                'validation'=>[],
                                'action'=>['type'=>PromotionActionType::$MAP[$action->type], 'config'=>$action->config]
                            ];
                            $promoDataArray[] = $data;
                        }
                        else {
                            $requiredQty = $buy_qty - $itemTotalQty;
                            $data = [
                                'promotion'=>$promotion,
                                'entitled'=>false,
                                'validation'=>['type'=>'buy_qty', 'qty'=>$requiredQty],
                                'action'=>['type'=>PromotionActionType::$MAP[$action->type], 'config'=>$action->config]
                            ];
                            $promoDataArray[] = $data;
                        }
                    }
                }
            }
        }

		return array(
			'data' => array(
                'timestamp'=>time(),
                'promotions'=>$promoDataArray
            )
		);
    }

    private function applyPromotion($hdrId)
    {
        $cartHdr = CartHdrRepository::txnFindByPk($hdrId);
        if($cartHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('SlsOrd.doc_status_is_wip_or_complete', ['docCode'=>$cartHdr->doc_code]));
            $exc->addData(\App\CartHdr::class, $cartHdr->id);
            throw $exc;
        }
        $cartHdrData = $cartHdr->toArray();
        $cartDtlModels = CartDtlRepository::findAllByHdrId($hdrId);

        $dtlToCreateArray = array(); //to hold new foc dtl 
        $dtlToUpdateArray = array();
        $dtlToDeleteArray = array(); //to hold old foc dtl 
        $result = array();
        
        $processedDtlModels = array();
        $cartItemIds = array();
        foreach ($cartDtlModels as $cartDtlModel) {
            $cartDtlData = $cartDtlModel->toArray();
            //clear foc line
            if ($cartDtlModel->net_amt == 0.00 && !empty($cartDtlModel->promotions)) {
                $dtlToDeleteArray[] = $cartDtlData;
            }
            else {
                $processedDtlModels[] = $cartDtlModel;
            }
            if (!in_array($cartDtlData['item_id'], $cartItemIds)) {
                $cartItemIds[] = $cartDtlData['item_id'];
            }
        }

        $processedIds = array();
        $processedDtlPromotions = array();
        //item level discount
        foreach ($processedDtlModels as $cartDtlModel) {
            $cartDtlData = $cartDtlModel->toArray();

            // if (in_array($cartDtlData['id'], $processedIds)) {
            //     continue;
            // }

            //remove promotion and run through promotion checking again
            $cartDtlData = $this->clearPromotion($cartDtlData);

            $promotions = PromotionRepository::findAllActiveByItem($cartDtlData['item_id']);

            //check promotion rule eligible
            $promotions = $promotions->filter(function ($promotion) use ($cartHdrData) {
                $rules = $promotion->rules;
                if ($rules->isEmpty()) {
                    //empty rules means all are entitled for promotion
                    return true;
                }
                foreach ($rules as $rule) {
                    //failing any one of rules will return false
                    // DebtorChecker
                    if ($rule->type == PromotionRuleType::$MAP['debtor']) {
                        return (new DebtorChecker($cartHdrData))->isEligible($rule->config);
                    }
                    if ($rule->type == PromotionRuleType::$MAP['debtor_group']) {
                        return (new DebtorGroupChecker($cartHdrData))->isEligible($rule->config);
                    }
                }
            });

            $dtlRequiredUpdate = false;
            
            // execute action for each promotion to order
            foreach ($promotions as $promotion) {
                //check if previous promotion already assigned to cart item
                // if (in_array($cartDtlData['id'], $processedIds)) {
                //     continue;
                // }

                $actions = $promotion->actions;
                $promoUuid = uniqid();
                
                foreach ($actions as $action) {
                    if ($action->type == PromotionActionType::$MAP['item_percent_disc']) {
                        $qtyValidation = true;

                        //check promo action has buy_qty (min assorted qty)
                        if (isset($action->config['buy_qty']) && $action->config['buy_qty'] > 0) {
                            $buy_qty = $action->config['buy_qty'];
                            $allVariants = PromotionVariantRepository::findAllEnabled($promotion->id);
                            $itemIds = array();
                            foreach ($allVariants as $variant) {
                                $itemIds[] = $variant->item_id;
                            }
                            $itemTotalQty = CartDtlRepository::getTotalItemArrayQty($hdrId, $itemIds);
                            if ($itemTotalQty < $buy_qty) {
                                $qtyValidation = false;
                            }
                        }

                        if ($qtyValidation) {
                            //get promotion variants matching item id
                            $promotionVariant = PromotionVariantRepository::findEnabledByItemAndGroup($promotion->id, $cartDtlData['item_id']);

                            if (!empty($promotionVariant))
                            {
                                if ($promotionVariant->min_qty > 0 || $promotionVariant->max_qty > 0) {
                                    //get the total item qty
                                    $itemTotalQty = CartDtlRepository::getTotalItemQty($hdrId, $cartDtlData['item_id']);
                                    if (($promotionVariant->min_qty > 0 && $itemTotalQty < $promotionVariant->min_qty) || 
                                        ($promotionVariant->max_qty > 0 && $itemTotalQty > $promotionVariant->max_qty)) {
                                        $qtyValidation = false;
                                    }
                                }
                                //passed qty validation
                                if ($qtyValidation) {
                                    //check if dtl already has discount
                                    for ($i = 1; $i <= 4; $i++) {
                                        if ($cartDtlData['dtl_disc_perc_0'.$i] > 0.00 && $promotionVariant->{'disc_perc_0'.$i} > 0.00)
                                        {
                                            if ($cartDtlData['dtl_disc_perc_0'.$i] >= $promotionVariant->{'disc_perc_0'.$i}) 
                                            {
                                                //compare if discount applied is higher or equal, skip this promotion
                                                continue;
                                            }
                                            else
                                            {
                                                //else remove the previous and apply this promotion

                                                //need to retain other discount value
                                                // $prevPriceDisc = $cartDtlData['price_disc'];
                                                // $prevDtlDiscPerc01 = $cartDtlData['dtl_disc_perc_01'];
                                                // $prevDtlDiscPerc02 = $cartDtlData['dtl_disc_perc_02'];
                                                // $prevDtlDiscPerc03 = $cartDtlData['dtl_disc_perc_03'];
                                                // $prevDtlDiscPerc04 = $cartDtlData['dtl_disc_perc_04'];

                                                // clear previous value
                                                $cartDtlData['dtl_disc_perc_0'.$i] = 0.00;

                                                $newProcessedDtlPromotions = array();
                                                foreach ($processedDtlPromotions as $processedDtlPromotion) {
                                                    if ($processedDtlPromotion['cart_dtl_id'] != $cartDtlData['id']) {
                                                        $newProcessedDtlPromotions[] = $processedDtlPromotion;
                                                    }
                                                    else {
                                                        if ($processedDtlPromotion['disc_perc_0'.$i] == 0.00) {
                                                            $newProcessedDtlPromotions[] = $processedDtlPromotion;
                                                        }
                                                    }
                                                }
                                                $processedDtlPromotions = $newProcessedDtlPromotions;

                                                //check if promotion was applied previously in loop
                                                //remove it from array, will add it back after apply discount
                                                if (in_array($cartDtlData['id'], $processedIds)) {
                                                    $newDtlToUpdateArray = array();
                                                    foreach ($dtlToUpdateArray as $dtlToUpdate) {
                                                        if ($dtlToUpdate['id'] != $cartDtlData['id']) {
                                                            $newDtlToUpdateArray[] = $dtlToUpdate;
                                                        }
                                                    }
                                                    $dtlToUpdateArray = $newDtlToUpdateArray;
                                                }
                                            }
                                        }
                                    }
                                    //start apply discount to dtl
                                    $discountConfig = array();
                                    $discountConfig['discount01'] = $promotionVariant->disc_perc_01;
                                    $discountConfig['discount02'] = $promotionVariant->disc_perc_02;
                                    $discountConfig['discount03'] = $promotionVariant->disc_perc_03;
                                    $discountConfig['discount04'] = $promotionVariant->disc_perc_04;
                                    $cartDtlData = (new ItemPercentDiscountAction($cartDtlData))->execute($discountConfig);
                                    // $cartDtlData['promo_hdr_id'] = $promotion->id;
                                    $cartDtlData['promo_uuid'] = $promoUuid;

                                    //create dtl promo data
                                    $dtlPromoData = array();
                                    $dtlPromoData['cart_dtl_id'] = $cartDtlData['id'];
                                    $dtlPromoData['promotion_id'] = $promotion->id;
                                    $dtlPromoData['promo_uuid'] = $promoUuid;
                                    $dtlPromoData['sale_price'] = $cartDtlData['sale_price'];
                                    $dtlPromoData['price_disc'] = 0.00;
                                    $dtlPromoData['disc_perc_01'] = $cartDtlData['dtl_disc_perc_01'];
                                    $dtlPromoData['disc_perc_02'] = $cartDtlData['dtl_disc_perc_02'];
                                    $dtlPromoData['disc_perc_03'] = $cartDtlData['dtl_disc_perc_03'];
                                    $dtlPromoData['disc_perc_04'] = $cartDtlData['dtl_disc_perc_04'];
                                    $processedDtlPromotions[] = $dtlPromoData;
                                }
                            }
                        }

                        //$dtlToUpdateArray[] = $cartDtlData;
                        $dtlRequiredUpdate = true;
                        $processedIds[] = $cartDtlData['id'];
                    }
                    else if ($action->type == PromotionActionType::$MAP['item_fixed_disc']) {

                        //get promotion variants matching item id
                        $promotionVariant = PromotionVariantRepository::findEnabledByItem($promotion->id, $cartDtlData['item_id']);

                        if (!empty($promotionVariant))
                        {
                            $qtyValidation = true;
                            if ($promotionVariant->min_qty > 0 || $promotionVariant->max_qty > 0) {
                                //get the total item qty
                                $itemTotalQty = CartDtlRepository::getTotalItemQty($hdrId, $cartDtlData['item_id']);
                                if (($promotionVariant->min_qty > 0 && $itemTotalQty < $promotionVariant->min_qty) || 
                                    ($promotionVariant->max_qty > 0 && $itemTotalQty > $promotionVariant->max_qty)) {
                                    $qtyValidation = false;
                                }
                            }
                            if ($qtyValidation)
                            {
                                if ($cartDtlData['price_disc'] > 0.00 && $promotionVariant->disc_fixed_price > 0.00)
                                {
                                    $unitPrice = bcdiv($cartDtlData['sale_price'], $cartDtlData['uom_rate'], 10);
                                    $unitPriceDisc = bcsub($unitPrice, $promotionVariant->disc_fixed_price, 10);
                                    $priceDisc = bcmul($unitPriceDisc, $cartDtlData['uom_rate'], config('scm.decimal_scale'));

                                    if ($cartDtlData['price_disc'] >= $priceDisc) 
                                    {
                                        //compare if discount applied is higher or equal, skip this promotion
                                        continue;
                                    }
                                    else
                                    {
                                        //else remove the previous and apply this promotion
                                        // $cartDtlData = $this->clearPromotion($cartDtlData);
                                        $cartDtlData['price_disc'] = 0.00;

                                        $newProcessedDtlPromotions = array();
                                        foreach ($processedDtlPromotions as $processedDtlPromotion) {
                                            if ($processedDtlPromotion['cart_dtl_id'] != $cartDtlData['id']) {
                                                $newProcessedDtlPromotions[] = $processedDtlPromotion;
                                            }
                                            else {
                                                if ($processedDtlPromotion['price_disc'] == 0.00) {
                                                    $newProcessedDtlPromotions[] = $processedDtlPromotion;
                                                }
                                            }
                                        }
                                        $processedDtlPromotions = $newProcessedDtlPromotions;
                                        
                                        //check if promotion was applied previously in loop
                                        //remove it from array, will add it back after apply discount
                                        if (in_array($cartDtlData['id'], $processedIds)) {
                                            $newDtlToUpdateArray = array();
                                            foreach ($dtlToUpdateArray as $dtlToUpdate) {
                                                if ($dtlToUpdate['id'] != $cartDtlData['id']) {
                                                    $newDtlToUpdateArray[] = $dtlToUpdate;
                                                }
                                            }
                                            $dtlToUpdateArray = $newDtlToUpdateArray;
                                        }
                                    }
                                }

                                $discountConfig = array();
                                $discountConfig['discountPrice'] = $promotionVariant->disc_fixed_price;
                                $cartDtlData = (new ItemFixedPriceAction($cartDtlData))->execute($discountConfig);
                                // $cartDtlData['promo_hdr_id'] = $promotion->id;
                                $cartDtlData['promo_uuid'] = $promoUuid;
                                
                                //create dtl promo data
                                $dtlPromoData = array();
                                $dtlPromoData['cart_dtl_id'] = $cartDtlData['id'];
                                $dtlPromoData['promotion_id'] = $promotion->id;
                                $dtlPromoData['promo_uuid'] = $promoUuid;
                                $dtlPromoData['sale_price'] = $cartDtlData['sale_price'];
                                $dtlPromoData['price_disc'] = $cartDtlData['price_disc'];
                                $dtlPromoData['disc_perc_01'] = 0.00;
                                $dtlPromoData['disc_perc_02'] = 0.00;
                                $dtlPromoData['disc_perc_03'] = 0.00;
                                $dtlPromoData['disc_perc_04'] = 0.00;
                                $processedDtlPromotions[] = $dtlPromoData;
                            }
                        }
                        
                        //$dtlToUpdateArray[] = $cartDtlData;
                        $dtlRequiredUpdate = true;
                        $processedIds[] = $cartDtlData['id'];
                    }
                    else if ($action->type == PromotionActionType::$MAP['item_foc']) {
                        
                        if (in_array($cartDtlData['id'], $processedIds)) {
                            continue;
                        }
                        
                        $promotionVariant = PromotionVariantRepository::findEnabledMainByItem($promotion->id, $cartDtlData['item_id']);
                        
                        if (!empty($promotionVariant))
                        {
                            $buy_qty = $action->config['buy_qty'];
                            $buy_qty = floatval($buy_qty);
                            $foc_qty = $action->config['foc_qty'];
                            $foc_qty = floatval($foc_qty);

                            $allVariants = PromotionVariantRepository::findAllEnabledMain($promotion->id);
                            $itemIds = array();
                            foreach ($allVariants as $variant) {
                                $itemIds[] = $variant->item_id;
                            }
                            //
                            $itemTotalQty = CartDtlRepository::getTotalMainItemArrayQty($hdrId, $itemIds);
                            if ($itemTotalQty >= $buy_qty) {
                                $focConfig = array();
                                $focConfig['total_qty'] = $itemTotalQty;
                                $focConfig['buy_qty'] = $buy_qty;
                                $focConfig['foc_qty'] = $foc_qty;
                                $focConfig['promotion_id'] = $promotion->id;
                                $focConfig['promo_uuid'] = $promoUuid;
                                $result = (new ItemFocAction($cartDtlData))->execute($focConfig);

                                if (!empty($result)) {
                                    //create dtl promo data
                                    $processedResult = array();
                                    foreach ($result as $focDtl) 
                                    {
                                        $dtlPromoData = array();
                                        // $dtlPromoData['cart_dtl_id'] = $result['id'];
                                        $dtlPromoData['promotion_id'] = $promotion->id;
                                        $dtlPromoData['promo_uuid'] = $promoUuid;
                                        $dtlPromoData['sale_price'] = $focDtl['sale_price'];
                                        $dtlPromoData['price_disc'] = 0.00;
                                        $dtlPromoData['disc_perc_01'] = 100.00; //100% discount = foc
                                        $dtlPromoData['disc_perc_02'] = 0.00;
                                        $dtlPromoData['disc_perc_03'] = 0.00;
                                        $dtlPromoData['disc_perc_04'] = 0.00;
                                        // $processedDtlPromotions[] = $dtlPromoData;
                                        $focDtl['dtlPromoData'] = $dtlPromoData;
                                        $processedResult[] = $focDtl;
                                    }
                                    $result = $processedResult;
                                }

                                $dtlToCreateArray = array_merge($dtlToCreateArray, $result);

                                //mark other involved lines so won't get processed again
                                foreach ($processedDtlModels as $otherCartDtlModel) {
                                    if ($otherCartDtlModel->id == $cartDtlData['id']) {
                                        continue;
                                    }
                                    if (in_array($otherCartDtlModel->id, $processedIds)) {
                                        continue;
                                    }
                                    if (in_array($otherCartDtlModel->item_id, $itemIds)) {
                                        $otherCartDtlData = $otherCartDtlModel->toArray();
                                        //remove promotion and run through promotion checking again
                                        $otherCartDtlData = $this->clearPromotion($otherCartDtlData);
                                        // $otherCartDtlData['promo_hdr_id'] = $promotion->id;
                                        $otherCartDtlData['promo_uuid'] = $promoUuid;
                                        
                                        //create dtl promo data
                                        $dtlPromoData = array();
                                        $dtlPromoData['cart_dtl_id'] = $otherCartDtlData['id'];
                                        $dtlPromoData['promotion_id'] = $promotion->id;
                                        $dtlPromoData['promo_uuid'] = $promoUuid;
                                        $dtlPromoData['sale_price'] = $otherCartDtlData['sale_price'];
                                        $dtlPromoData['price_disc'] = $otherCartDtlData['price_disc'];
                                        $dtlPromoData['disc_perc_01'] = 0.00;
                                        $dtlPromoData['disc_perc_02'] = 0.00;
                                        $dtlPromoData['disc_perc_03'] = 0.00;
                                        $dtlPromoData['disc_perc_04'] = 0.00;
                                        $processedDtlPromotions[] = $dtlPromoData;

                                        $dtlToUpdateArray[] = $otherCartDtlData;
                                        $processedIds[] = $otherCartDtlData['id'];
                                    }
                                }

                                // $cartDtlData['promo_hdr_id'] = $promotion->id;
                                $cartDtlData['promo_uuid'] = $promoUuid;
                                
                                //create dtl promo data
                                $dtlPromoData = array();
                                $dtlPromoData['cart_dtl_id'] = $cartDtlData['id'];
                                $dtlPromoData['promotion_id'] = $promotion->id;
                                $dtlPromoData['promo_uuid'] = $promoUuid;
                                $dtlPromoData['sale_price'] = $cartDtlData['sale_price'];
                                $dtlPromoData['price_disc'] = $cartDtlData['price_disc'];
                                $dtlPromoData['disc_perc_01'] = 0.00;
                                $dtlPromoData['disc_perc_02'] = 0.00;
                                $dtlPromoData['disc_perc_03'] = 0.00;
                                $dtlPromoData['disc_perc_04'] = 0.00;
                                $processedDtlPromotions[] = $dtlPromoData;

                                //$dtlToUpdateArray[] = $cartDtlData;
                                $dtlRequiredUpdate = true;
                                $processedIds[] = $cartDtlData['id'];
                            }
                        }
                    }
                }
            }
            if ($dtlRequiredUpdate) {
                $dtlToUpdateArray[] = $cartDtlData;
            }
        }

        foreach($dtlToCreateArray as $dtlData)
        {       
            $dtlPromoData = array();
            if (isset($dtlData['dtlPromoData']))
            {
                $dtlPromoData = $dtlData['dtlPromoData'];
                unset($dtlData['dtlPromoData']);
            }

            $createdResult = $this->createDetail($hdrId, $dtlData);   
            $result[] = $createdResult;

            if (!empty($dtlPromoData))
            {
                $details = $createdResult['data']['document_details'];
                foreach ($details as $detail)
                {
                    if ($dtlPromoData['promo_uuid'] == $detail['promo_uuid'] && $detail['dtl_disc_perc_01'] == 100)
                    {
                        $dtlPromoData['cart_dtl_id'] = $detail['id'];
                        $processedDtlPromotions[] = $dtlPromoData;
                    }
                }
            }

        }
        if (!empty($dtlToUpdateArray)) {
            $result[] = $this->updateDetails($hdrId, $dtlToUpdateArray);
        }
        if (!empty($dtlToDeleteArray)) {
            $result[] = $this->deleteDetails($hdrId, $dtlToDeleteArray);
        }

        //clear previous dtl promotion
        CartDtlPromotionRepository::deleteAllByDtlIds($processedIds);
        //create new dtl promotion
        foreach($processedDtlPromotions as $processedDtlPromotion)
        {       
            CartDtlPromotionRepository::createModel($processedDtlPromotion);
        }
        
        return $result;
    }

    public function updateDeliveryPoint($hdrId, $deliveryPointId)
    {
        AuthService::authorize(
            array(
                'sls_ord_update'
            )
        );

        $result = array();
        $model = null;
        $message = '';

        $deliveryPoint = DeliveryPointRepository::findByPk($deliveryPointId);
        if(!empty($deliveryPoint))
        {
            $cartHdrData = array();
            $cartHdrData['id'] = $hdrId;
            //$cartHdrData['delivery_point_id'] = $deliveryPoint->id;

            //generate delivery related values by delivery point
            $cartHdrData['delivery_point_unit_no'] = $deliveryPoint->unit_no;
            $cartHdrData['delivery_point_building_name'] = $deliveryPoint->building_name;
            $cartHdrData['delivery_point_street_name'] = $deliveryPoint->street_name;
            $cartHdrData['delivery_point_district_01'] = $deliveryPoint->district_01;
            $cartHdrData['delivery_point_district_02'] = $deliveryPoint->district_02;
            $cartHdrData['delivery_point_postcode'] = $deliveryPoint->postcode;
            
            if(!empty($deliveryPoint->state)) {
                $cartHdrData['delivery_point_state_name'] = $deliveryPoint->state->desc_01;
            }

            if(!empty($deliveryPoint->area)) {
                $cartHdrData['delivery_point_area_name'] = $deliveryPoint->area->desc_01;
            }

            $cartHdrData['delivery_point_country_name'] = $deliveryPoint->country_name;
            $cartHdrData['delivery_point_attention'] = $deliveryPoint->attention;
            $cartHdrData['delivery_point_phone_01'] = $deliveryPoint->phone_01;
            $cartHdrData['delivery_point_phone_02'] = $deliveryPoint->phone_02;
            $cartHdrData['delivery_point_fax_01'] = $deliveryPoint->phone_01;
            $cartHdrData['delivery_point_fax_02'] = $deliveryPoint->fax_02;
            
            $result = $this->updateHeader($cartHdrData);

            $model = $result['data']['document_header'];
            $message = $result['message'];

            // return array(
            //     'data' => array(
            //         'timestamp'=>time(),
            //         'document_header'=>$documentHeader,
            //         'document_details'=>$documentDetails,
            //         'deleted_details'=>array()
            //     ),
            //     'message' => $message
            // );

            $initDeliveryPointOption = array(
                'value'=>$deliveryPoint->id, 
                'label'=>$deliveryPoint->code.' '.$deliveryPoint->company_name_01
            );

            $model->delivery_point_unit_no = $deliveryPoint->unit_no;
            $model->delivery_point_building_name = $deliveryPoint->building_name;
            $model->delivery_point_street_name = $deliveryPoint->street_name;
            $model->delivery_point_district_01 = $deliveryPoint->district_01;
            $model->delivery_point_district_02 = $deliveryPoint->district_02;
            $model->delivery_point_postcode = $deliveryPoint->postcode;
            $model->delivery_point_state_name = $deliveryPoint->state_name;
            $model->delivery_point_country_name = $deliveryPoint->country_name;
            $model->delivery_point_select2 = $initDeliveryPointOption;

        }
		return array(
			'data' => array(
                'cartHdr'=>$model
            ),
			'message' => $message
        );
        
        if(!empty($deliveryPoint))
        {
            
            $results['delivery_point_unit_no'] = $deliveryPoint->unit_no;
            $results['delivery_point_building_name'] = $deliveryPoint->building_name;
            $results['delivery_point_street_name'] = $deliveryPoint->street_name;
            $results['delivery_point_district_01'] = $deliveryPoint->district_01;
            $results['delivery_point_district_02'] = $deliveryPoint->district_02;
            $results['delivery_point_postcode'] = $deliveryPoint->postcode;
            $results['delivery_point_state_name'] = $deliveryPoint->state_name;
            $results['delivery_point_country_name'] = $deliveryPoint->country_name;
        }
        return $results;
    }

    public function placeOrder($hdrId, $data) {
        $user = Auth::user();
        //$data = self::processIncomingHeader($data, true);
        $cartHdr = CartHdrRepository::txnFindByPk($hdrId);
        if($cartHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('SlsOrd.doc_status_is_wip_or_complete', ['docCode'=>$cartHdr->doc_code]));
            $exc->addData(\App\CartHdr::class, $cartHdr->id);
            throw $exc;
        }
        $cartHdrData = $cartHdr->toArray();

        $cartDtlDataArray = array();
        $cartDtlModels = CartDtlRepository::findAllByHdrId($hdrId);
        foreach($cartDtlModels as $cartDtlModel)
        {
            $cartDtlData = $cartDtlModel->toArray();
            $cartDtlDataArray[] = $cartDtlData;
        }

        $slsOrdHdr = CartHdrRepository::transferToSlsOdrHdr($cartHdrData);
        $slsOrdHdrData = $slsOrdHdr->toArray();
        $slsOrdDtlArray = array();
        foreach($cartDtlDataArray as $cartDtlData)
        {
            //copy promotion data
            $cartDtlPromotions = CartDtlPromotionRepository::findAllByDtlId($cartDtlData['id']);
            if (!empty($cartDtlPromotions))
            {
                $dtlPromoDataArray = array();
                foreach ($cartDtlPromotions as $cartDtlPromotion) 
                {
                    $dtlPromoData = array();
                    $dtlPromoData['sls_ord_dtl_id'] = '';
                    $dtlPromoData['promotion_id'] = $cartDtlPromotion->promotion_id;
                    $dtlPromoData['promo_uuid'] = $cartDtlPromotion->promo_uuid;
                    $dtlPromoData['sale_price'] = $cartDtlPromotion->sale_price;
                    $dtlPromoData['price_disc'] = $cartDtlPromotion->price_disc;
                    $dtlPromoData['disc_perc_01'] = $cartDtlPromotion->disc_perc_01;
                    $dtlPromoData['disc_perc_02'] = $cartDtlPromotion->disc_perc_02;
                    $dtlPromoData['disc_perc_03'] = $cartDtlPromotion->disc_perc_03;
                    $dtlPromoData['disc_perc_04'] = $cartDtlPromotion->disc_perc_04;
                    $dtlPromoDataArray[] = $dtlPromoData;
                }
                $cartDtlData['dtl_promotions'] = $dtlPromoDataArray;
            }

            $cartDtlData['id'] = 'NEW';
            $cartDtlData['is_modified'] = 1;
            unset($cartDtlData['status']);
            
            $slsOrdDtlArray[] = $cartDtlData;
        }
		$result = $this->calculateAmount($slsOrdHdrData, $slsOrdDtlArray);
		$result = SlsOrdHdrRepository::updateDetails($result['hdrData'], $result['dtlArray']);
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }
        $cartHdrModel = self::transitionToComplete($hdrId);

		$docTxnFlowData = array(
			'fr_doc_hdr_type' => \App\CartHdr::class,
			'fr_doc_hdr_id' => $cartHdrModel->id,
			'fr_doc_hdr_code' => $cartHdrModel->doc_code,
            'to_doc_hdr_type' => \App\SlsOrdHdr::class,
            'to_doc_hdr_id' => $hdrModel->id,
			'is_closed' => 1
        );

        $docTxnFlow = CartHdrRepository::createProcess(ProcType::$MAP['CART_ORD_01'], $docTxnFlowData);

        $message = __('SlsOrd.details_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'cart_header'=>$cartHdrModel,
                'sls_ord_header'=>$documentHeader,
                'sls_ord_details'=>$documentDetails
            ),
			'message' => $message
		);

    }

}
