<?php

namespace App\Services;

use App\Division;
use App\Models\Promotion;
use App\Models\PromotionRule;
use App\Services\Env\ProcType;
use App\Services\Env\PromotionType;
use App\Services\Env\PromotionActionType;
use App\Services\Env\PromotionRuleType;
use App\Services\Env\ResStatus;
use App\Services\Utils\ApiException;
use App\Repositories\BatchJobStatusRepository;
use App\Repositories\CartDtlPromotionRepository;
use App\Repositories\CartDtlRepository;
use App\Repositories\PromotionRepository;
use App\Repositories\PromotionVariantRepository;
use App\Repositories\PromotionActionRepository;
use App\Repositories\PromotionRuleRepository;
use App\Repositories\ItemSalePriceRepository;
use App\Repositories\ItemUomRepository;
use App\Repositories\DebtorRepository;
use App\Repositories\DebtorGroup01Repository;
use App\Repositories\ItemRepository;
use App\Repositories\ItemGroup01Repository;
use App\Repositories\ItemGroup02Repository;
use App\Repositories\MessageRepository;
use App\Repositories\SiteFlowRepository;
use App\Repositories\SyncSettingHdrRepository;
use App\Repositories\SyncSettingDtlRepository;
use App\Repositories\PromotionPhotoRepository;
use App\Repositories\UserRepository;
use App\Libs\Promotion\DebtorChecker;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;

class PromotionService 
{
    public function __construct() 
	{
    }

    //Broadcast promotion message to listed users
    public function broadcastPromo($data, $divisionId)
    {   
        //Init message structure
        $dataArray = [];
        $dataArray['user_id'] = 0;
        $dataArray['division_id'] = $divisionId;
        $dataArray['company_id'] = $divisionId;
        $dataArray['title'] = $data['title'];
        $dataArray['content'] = $data['content'];
        $dataArray['payload'] = json_encode(['id' => $data['id'], 'navigation' => 'PromotionsScreen']);
        $dataArray['priority'] = $data['priority'];

        //Init debtor/group containers
        $result = [];
        $debtor_ids = [];

        //Init
        $debtor_sorts = [['field' => 'code', 'order' => 'asc']];
        $debtor_group_filters = [];

        if(count($data['debtor_group_ids']) > 0 || count($data['debtor_ids']) > 0) {
            if(count($data['debtor_group_ids']) > 0) {
                $debtor_group_filters = [['field' => 'debtor_category_01_id', 'value' => $data['debtor_group_ids']]];
                $debtor_groups = DebtorRepository::findAll($debtor_sorts, $debtor_group_filters, 0, $divisionId);

                foreach($debtor_groups as $debtor) {
                    $user = UserRepository::findWithDevicesByDebtorId($debtor->id);
                    if($user) {
                        $debtor_ids[] = $debtor->id;
                    }
                }
            } 
            
            if(count($data['debtor_ids']) > 0) {
                foreach($data['debtor_ids'] as $id) {
                    if(!in_array($id, $debtor_ids)) {
                        $user = UserRepository::findWithDevicesByDebtorId($id);
                        if($user) {
                            $debtor_ids[] = $id;
                        }
                    }
                }
            }
        } else {
            $debtors = DebtorRepository::findAll($debtor_sorts, $debtor_group_filters, 0, $divisionId);
            foreach($debtors as $debtor) {
                $user = UserRepository::findWithDevicesByDebtorId($debtor->id);
                if($user) {
                    $debtor_ids[] = $debtor->id;
                }
            }
        }

        foreach($debtor_ids as $id) {
            $dataArray['user_id'] = $id;
            $dataArray['created_at'] = Carbon::now();
            $dataArray['updated_at'] = $dataArray['created_at'];
            $result[] = MessageRepository::createModel($dataArray);
        }
        
        $message = __('Promotion.promotions_successfully_broadcasted.', []);

        return array(
            'result' => $result,
			'message' => $message
        );
    }

    public function changeItemGroup($promotionId)
    {
        // AuthService::authorize(
        //     array(
        //         'sls_ord_update'
        //     )
        // );
        $results = array();

        $promotion = Promotion::find($promotionId);

        if($promotion->type == PromotionType::$MAP['disc_percent']) {
            $action = PromotionActionRepository::findByPromotionIdAndType($promotionId, PromotionActionType::$MAP['item_percent_disc']);
            if(!empty($action))
            {
                $actionData = $action->toArray();
                $config = $actionData['config'];
                if(isset($config['disc_perc_01']))
                {
                    $results['disc_perc_01'] = $config['disc_perc_01'];
                }
            }
        }

        return $results;
    }

    public function changeItem($itemId, $promotionId)
    {
        // AuthService::authorize(
        //     array(
        //         'sls_ord_update'
        //     )
        // );
        $results = array();
        $itemUom = ItemUomRepository::findSmallestByItemId($itemId);
        if(!empty($itemUom))
        {
			$itemSalePrice = ItemSalePriceRepository::findByItemIdAndUomId(now(), 0, 1, $itemId, $itemUom->uom_id);
			if(!empty($itemSalePrice))
			{
                $results['sale_price'] = round($itemSalePrice->sale_price, config('scm.decimal_scale'));
			}
        }

        $promotion = Promotion::find($promotionId);
        if($promotion->type == PromotionType::$MAP['disc_fixed_price']) {
            $action = PromotionActionRepository::findByPromotionIdAndType($promotionId, PromotionActionType::$MAP['item_fixed_disc']);
            if(!empty($action))
            {
                $actionData = $action->toArray();
                $config = $actionData['config'];
                if(isset($config['disc_fixed_price']))
                {
                    $results['disc_fixed_price'] = $results['sale_price'] - $config['disc_fixed_price'];
                }
            }
        } else if($promotion->type == PromotionType::$MAP['disc_percent']) {
            $action = PromotionActionRepository::findByPromotionIdAndType($promotionId, PromotionActionType::$MAP['item_percent_disc']);
            if(!empty($action))
            {
                $actionData = $action->toArray();
                $config = $actionData['config'];
                if(isset($config['disc_perc_01']))
                {
                    $results['disc_perc_01'] = $config['disc_perc_01'];
                }
            }
        }
        
        
        return $results;
    }

    public function createModel($divisionId, $strPromoType, $data)
    {		
        // AuthService::authorize(
        //     array(
        //         'item_create'
        //     )
        // );

        if (!isset(PromotionType::$MAP[$strPromoType])) {
            $exc = new ApiException(__('Promotion.invalid_type', ['type'=>$strPromoType]));
            throw $exc;
        }

        $data['type'] = $strPromoType;
        $data['division_id'] = $divisionId;
        $data['status'] = ResStatus::$MAP['ACTIVE'];

        $buy_qty = 0;
        $foc_qty = 0;
        $disc_fixed_price = 0;
        if($strPromoType == PromotionType::$MAP['disc_percent']) 
        {
            //for min assorted qty
            if (isset($data['buy_qty'])) {
                $buy_qty = $data['buy_qty'];
            }
        }
        if($strPromoType == PromotionType::$MAP['foc']) 
        {
            $buy_qty = $data['buy_qty'];
            $foc_qty = $data['foc_qty'];
        }
        if($strPromoType == PromotionType::$MAP['disc_fixed_price'])
        {
            if(isset($data['disc_fixed_price'])) {
                $disc_price = $data['disc_fixed_price'];
            }
        }
        
        $data = self::processIncomingModel($data);
        $model = PromotionRepository::createModel($data);

		if(isset(PromotionType::$MAP[$strPromoType]))
		{
			if($strPromoType == PromotionType::$MAP['disc_percent']) 
			{
                //create action
                $actionData = array();
                $actionData['promotion_id'] = $model->id;
                $actionData['type'] = PromotionActionType::$MAP['item_percent_disc'];
                $actionData['config'] = ['buy_qty'=>$buy_qty];
                PromotionActionRepository::createModel($actionData);
            }
			if($strPromoType == PromotionType::$MAP['disc_fixed_price']) 
			{
                //create action
                $actionData = array();
                $actionData['promotion_id'] = $model->id;
                $actionData['type'] = PromotionActionType::$MAP['item_fixed_disc'];
                PromotionActionRepository::createModel($actionData);
            }
			if($strPromoType == PromotionType::$MAP['foc']) 
			{
                //create action
                $actionData = array();
                $actionData['promotion_id'] = $model->id;
                $actionData['type'] = PromotionActionType::$MAP['item_foc'];
                $actionData['config'] = ['buy_qty'=>$buy_qty, 'foc_qty'=>$foc_qty];
                PromotionActionRepository::createModel($actionData);
            }
        }

        $message = __('Promotion.record_successfully_created', ['docCode'=>$model->code]);

		return array(
            'data' => $model->id,
			'message' => $message
		);
    }
    
    public function createRule($promotionId, $data)
	{
        // AuthService::authorize(
        //     array(
        //         'sls_ord_update'
        //     )
        // );
        $promotion = PromotionRepository::findByPk($promotionId);
        $promotionData = $promotion->toArray();
        $rules = array();
        
        if ($data['type'] == 'debtor') {
            //json
            $config = [];

            //find rule with same type before create
            $rule = PromotionRule::where('promotion_id', $promotionId)
                    ->where('type', PromotionRuleType::$MAP['debtor'])->first();

            if ($rule) {
                //check debtor id already existed
                $value = $rule->config['debtor_id'];
                $valueArray = explode(',', $value);

                foreach ($data['debtor_ids'] as $debtorId) {
                    if (!in_array($debtorId, $valueArray)) {
                        //append value
                        $valueArray[] = $debtorId;
                    }
                }

                $config = ['debtor_id'=>implode(',', $valueArray)];

                $data['id'] = $rule->id;
                $data['type'] = PromotionRuleType::$MAP['debtor'];
                $data['config'] = $config; 
                
                $data = self::processIncomingRule($data);
                
                $data['promotion_id'] = $promotionId;
                $result = PromotionRuleRepository::updateModel($data);
                $ruleModel = $result['model'];
                $rules[] = self::processOutgoingRule($ruleModel);
            }
            else {
                $config = ['debtor_id'=>implode(',', $data['debtor_ids'])];
                $data['config'] = $config; 
                $data['type'] = PromotionRuleType::$MAP['debtor'];

                $data = self::processIncomingRule($data);
                
                $data['promotion_id'] = $promotionId;
                $ruleModel = PromotionRuleRepository::createModel($data);
                $rules[] = self::processOutgoingRule($ruleModel);
            }
        }
        else if ($data['type'] == 'debtor_group01') {
            // $data['type'] = PromotionRuleType::$MAP['debtor_group01'];
            // $data['config'] = ['debtor_group_01_id'=>$data['debtor_group_01_id']]; 
            
            //json
            $config = [];

            //find rule with same type before create
            $rule = PromotionRule::where('promotion_id', $promotionId)
                    ->where('type', PromotionRuleType::$MAP['debtor_group'])->first();

            if ($rule) {
                //check debtor id already existed
                $value = $rule->config['debtor_group_01_id'];
                $valueArray = explode(',', $value);

                foreach ($data['debtor_group_01_ids'] as $debtorGroupId) {
                    if (!in_array($debtorGroupId, $valueArray)) {
                        //append value
                        $valueArray[] = $debtorGroupId;
                    }
                }

                $config = ['debtor_group_01_id'=>implode(',', $valueArray)];

                $data['id'] = $rule->id;
                $data['type'] = PromotionRuleType::$MAP['debtor_group'];
                $data['config'] = $config; 
                
                $data = self::processIncomingRule($data);
                
                $data['promotion_id'] = $promotionId;
                $result = PromotionRuleRepository::updateModel($data);
                $ruleModel = $result['model'];
                $rules[] = self::processOutgoingRule($ruleModel);
            }
            else {
                $config = ['debtor_group_01_id'=>implode(',', $data['debtor_group_01_ids'])];
                $data['config'] = $config; 
                $data['type'] = PromotionRuleType::$MAP['debtor_group'];

                $data = self::processIncomingRule($data);
                
                $data['promotion_id'] = $promotionId;
                $ruleModel = PromotionRuleRepository::createModel($data);
                $rules[] = self::processOutgoingRule($ruleModel);
            }
        }

        $message = __('Promotion.rule_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'promotion'=>$promotionData,
                'promotion_rules'=>$rules,
            ),
			'message' => $message
		);
    }
    
    public function createVariant($promotionId, $data)
	{
        // AuthService::authorize(
        //     array(
        //         'sls_ord_update'
        //     )
        // );

        $data = self::processIncomingDetail($data);
        if ($data['variant_type'] == 'item') {
        }
        else if ($data['variant_type'] == 'item_group01') {
        }

        $promotion = PromotionRepository::findByPk($promotionId);
        $promotionData = $promotion->toArray();
        
        $data['promotion_id'] = $promotionId;
        $variantModel = PromotionRepository::createVariantModel($data);
        $variants = array();
        $variants[] = self::processOutgoingVariant($variantModel);

        $message = __('Promotion.variant_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'promotion'=>$promotionData,
                'promotion_variants'=>$variants,
            ),
			'message' => $message
		);
    }

    public function deleteModel($data)
    {  
        //Delete promotion record and its child models 
        PromotionRepository::deleteModel($data['id']);
        
        //Update existing cart details to remove promotions
        $cart_dtls_id_array = CartDtlPromotionRepository::deleteByPromotionId($data['id']);
        
        // $cart_dtls_array = []; //For use later to notify users
        // foreach($cart_dtls_id_array as $dtl) {
        //     $cart_dtls_array[] = CartDtlRepository::disableCartDtlByPromotionId($dtl);
        // }

        $message = __('Promotion.record_successfully_deleted');

        return array(
            'data' => array(
                'promotion_id' => $data['id']
            ),
            'message' => $message
        );
    }
    
    public function deletePhotos($promotionId)
	{
        $deletedPromotionPhotoModelArray = PromotionPhotoRepository::deletePromotionPhotos($promotionId);
        $message = __('Promotion.photos_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'photos'=>array(),
                'deleted_photos'=>$deletedPromotionPhotoModelArray,
            ),
			'message' => $message
		);
    }
    
    public function deleteRule($promotionId, $data)
    {
        // AuthService::authorize(
        //     array(
        //         'sls_ord_update'
        //     )
        // );

        $promotion = PromotionRepository::findByPk($promotionId);
        $promotionData = $promotion->toArray();
        
        $rule = PromotionRuleRepository::deleteModel($data['id']);

        //Checks if cart user applies debtor rule

        // //Update existing cart details to remove promotions
        // $cart_dtls_id_array = CartDtlPromotionRepository::deleteByPromotionIdAndRule($promotionId, $rule);
                
        // $cart_dtls_array = []; //For use later to notify users
        // foreach($cart_dtls_id_array as $dtl) {
        //     $cart_dtls_array[] = CartDtlRepository::removeCartItemById($dtl);
        // }

        $message = __('Promotion.rule_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'promotion'=>$promotionData,
                'deleted_promotion_rule'=>$data,
            ),
			'message' => $message
		);
    }
    
    public function deleteVariant($promotionId, $data)
	{
        // AuthService::authorize(
        //     array(
        //         'sls_ord_update'
        //     )
        // );

        $promotion = PromotionRepository::findByPk($promotionId);
        $promotionData = $promotion->toArray();
        
        PromotionRepository::deleteVariantModel($data['id']);
        
        

        $message = __('Promotion.variant_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'promotion'=>$promotionData,
                'deleted_promotion_variant'=>$data,
            ),
			'message' => $message
		);
    }

    public function findAllActiveByItem($itemId)
    {
        $user = Auth::user();
        $userId = $user->id;
        $debtorId = $user->debtor_id;

        $promotions = PromotionRepository::findAllActiveByItem($itemId);

        if (!empty($debtorId)) {
            //check promotion rule eligible
            $promotions = $promotions->filter(function ($promotion) use ($debtorId) {
                $rules = $promotion->rules;
                if ($rules->isEmpty()) {
                    //empty rules means all are entitled for promotion
                    return true;
                }
                $data = ['debtor_id'=>$debtorId];
                foreach ($rules as $rule) {
                    // failing any one of rules will return false
                    // DebtorChecker
                    if ($rule->type == PromotionRuleType::$MAP['debtor']) {
                        return (new DebtorChecker($data))->isEligible($rule->config);
                    }
                }
            });
        }

    	return $promotions;
    }

    public function findAllRelatedItems($sorts = array(), $filters = array(), $promotionIds = array())
    {
        $related = array();

        foreach($promotionIds as $promotion) {
            $items = array();
            $item_group_01s = array();
            $item_group_02s = array();
            
            $promotion_variant = PromotionVariantRepository::findAllByPromotionId($promotion);
            
            foreach($promotion_variant as $variant) {
                if($variant->variant_type === 'item'){
                    $item = ItemRepository::findByPkWithPhoto($variant->item_id);
                    array_push($items, $item);
                } else if($variant->variant_type === 'item_group01') {
                    $item_group_01 = ItemGroup01Repository::findByPk($variant->item_group_01_id);
                    array_push($item_group_01s, $item_group_01);
                } else if($variant->variant_type === 'item_group02') {
                    $item_group_02 = ItemGroup02Repository::findByPk($variant->item_group_02_id);
                    array_push($item_group_02s, $item_group_02);
                }
            }

            array_push($related, array(
                'id' => $promotion,
                'items' => $items,
                'item_group_01s' => $item_group_01s,
                'item_group_02s' => $item_group_02s
            ));
        }

        return $related;
    }

    public function findPromotion($promotion_id)
    {
        $promotion = PromotionRepository::findByPk($promotion_id);
        return $promotion;
    }

    public function findPromotionItems($promotion_id)
    {
        $item_list_array = PromotionRepository::findItemsByPromotion($promotion_id);
        return $item_list_array;
    }
    
    public function index($divisionId, $sorts, $filters = array(), $pageSize = 20)
	{
        // AuthService::authorize(
        //     array(
        //         'user_read'
        //     )
        // );

        $promotions = PromotionRepository::findAll($divisionId, $sorts, $filters, $pageSize);
		foreach($promotions as $promotion)
		{
            $promotion->str_status = ResStatus::$MAP[$promotion->status];
        }
    
        return $promotions;
    }
    
    public function indexDisc($divisionId, $sorts, $filters = array(), $pageSize = 20)
	{
        // AuthService::authorize(
        //     array(
        //         'user_read'
        //     )
        // );
        $promoTypes = [PromotionType::$MAP['disc_percent'], PromotionType::$MAP['disc_fixed_price']];
        $promotions = PromotionRepository::findAll($divisionId, $sorts, $filters, $pageSize, $promoTypes);
		foreach($promotions as $promotion)
		{
            $promotion->str_status = ResStatus::$MAP[$promotion->status];
        }

    	return $promotions;
    }
    
    public function indexFoc($divisionId, $sorts, $filters = array(), $pageSize = 20)
	{
        // AuthService::authorize(
        //     array(
        //         'user_read'
        //     )
        // );

        $promoType = PromotionType::$MAP['foc'];
        $promotions = PromotionRepository::findAll($divisionId, $sorts, $filters, $pageSize, $promoType);
		foreach($promotions as $promotion)
		{
            $promotion->str_status = ResStatus::$MAP[$promotion->status];
        }

    	return $promotions;
    }

    public function initModel($divisionId, $strPromoType)
    {
        // AuthService::authorize(
        //     array(
        //         'item_create'
        //     )
        // );

        if (!isset(PromotionType::$MAP[$strPromoType])) {
            $exc = new ApiException(__('Promotion.invalid_type', ['type'=>$strPromoType]));
            throw $exc;
        }

        $model = new Promotion();
        $model->code = '';
        $model->name = '';
        $model->desc_01 = '';
        $model->desc_02 = '';
        $model->position = 0;
        $model->type = $strPromoType;
        $model->division_id = $divisionId;
        $model->valid_from = '2010-01-01 00:00:00';
        $model->valid_to = '2038-01-19 00:00:00';
        $model->status = ResStatus::$MAP['ACTIVE'];

        if ($strPromoType == PromotionType::$MAP['foc']) {
            //foc type
            $model->buy_qty = 0;
            $model->foc_qty = 0;
        }

        return $model;
    }
    
    public function select2($search, $filters)
    {
        $promotions = PromotionRepository::select2($search, $filters);
        return $promotions;
    }

    public function select2Init($id)
    {
        $promotion = null;
        if($id > 0) 
        {
            $promotion = PromotionRepository::findByPk($id);
        }
        else
        {
            $promotion = PromotionRepository::findTop();
        }
        $option = array('value'=>0, 'label'=>'');
        if(!empty($promotion))
        {
            $option = array(
                'value'=>$promotion->id, 
                'label'=>$promotion->code.' '.$promotion->name_01
            );
        }
        return $option;
    }
    
    public function showModel($promotionId)
    {
        // AuthService::authorize(
        //     array(
        //         'item_read',
		// 		'item_update'
        //     )
        // );

        $model = PromotionRepository::findByPk($promotionId);
		if(!empty($model))
        {
            $model->load('variants', 'rules', 'actions');
            $model = self::processOutgoingModel($model);
        }
        
        return $model;
    }
	
    public function showRules($promotionId)
    {
        $rules = PromotionRuleRepository::findAllByPromotionId($promotionId);

		foreach($rules as $rule)
		{
			$rule = self::processOutgoingRule($rule);
		}
        return $rules;
    }
	
    public function showVariants($promotionId, $mode='all')
    {
        if ($mode == 'all') {
            $variants = PromotionVariantRepository::findAllByPromotionId($promotionId);
        }
        else if ($mode == 'main') {
            $variants = PromotionVariantRepository::findAllMainByPromotionId($promotionId);
        }
        else if ($mode == 'add_on') {
            $variants = PromotionVariantRepository::findAllAddOnByPromotionId($promotionId);
        }

		foreach($variants as $variant)
		{
			$variant = self::processOutgoingVariant($variant);
		}
        return $variants;
    }

    public function updateModel($data)
	{
        // AuthService::authorize(
        //     array(
        //         'item_update'
        //     )
        // );

        $promotion = PromotionRepository::findByPk($data['id']);
        $promotionData = $promotion->toArray();

        $buy_qty = 0;
        $foc_qty = 0;
        $disc_price = 0;
        $disc_perc_01 = 0;
        if($promotion->type == PromotionType::$MAP['disc_percent']) 
        {
            if (isset($data['buy_qty'])) {
                $buy_qty = $data['buy_qty'];
            }
            if (isset($data['disc_perc_01'])) {
                $disc_perc_01 = $data['disc_perc_01'];
            }
        }
        if($promotion->type == PromotionType::$MAP['foc']) 
        {
            $buy_qty = $data['buy_qty'];
            $foc_qty = $data['foc_qty'];
        }
        if($promotion->type == PromotionType::$MAP['disc_fixed_price'])
        {
            if(isset($data['disc_fixed_price'])) {
                $disc_price = $data['disc_fixed_price'];
            }
        }

        $data = self::processIncomingModel($data);

        //assign data to model
        foreach($data as $field => $value) 
        {
            $promotionData[$field] = $value;
        }

        $result = PromotionRepository::updateModel($promotionData);
        $model = $result['model'];
        
        if($promotion->type == PromotionType::$MAP['disc_percent']) 
        {
            $action = PromotionActionRepository::findByPromotionIdAndType($model->id, PromotionActionType::$MAP['item_percent_disc']);
            if ($action) {
                $actionData = $action->toArray();
                $actionData['config'] = ['buy_qty'=>$buy_qty, 'disc_perc_01'=>$disc_perc_01];
                PromotionActionRepository::updateModel($actionData);
            }
            else {
                $actionData = array();
                $actionData['promotion_id'] = $model->id;
                $actionData['type'] = PromotionActionType::$MAP['item_percent_disc'];
                $actionData['config'] = ['buy_qty'=>$buy_qty, 'disc_perc_01'=>$disc_perc_01];
                PromotionActionRepository::createModel($actionData);
            }

            //Updates all the new price in item variants by promotion id after changing the discount fixed price in headerform section
            PromotionVariantRepository::updateAllVariantByPromotionId($model->id, $disc_perc_01);
        }
        if($promotion->type == PromotionType::$MAP['foc']) 
        {
            $action = PromotionActionRepository::findByPromotionIdAndType($model->id, PromotionActionType::$MAP['item_foc']);
            if ($action) {
                $actionData = $action->toArray();
                $actionData['config'] = ['buy_qty'=>$buy_qty, 'foc_qty'=>$foc_qty];
                PromotionActionRepository::updateModel($actionData);
            }
            else {
                $actionData = array();
                $actionData['promotion_id'] = $model->id;
                $actionData['type'] = PromotionActionType::$MAP['item_foc'];
                $actionData['config'] = ['buy_qty'=>$buy_qty, 'foc_qty'=>$foc_qty];
                PromotionActionRepository::createModel($actionData);
            }
        }
        if($promotion->type == PromotionType::$MAP['disc_fixed_price'])
        {
            $action = PromotionActionRepository::findByPromotionIdAndType($model->id, PromotionActionType::$MAP['item_fixed_disc']);
            $actionData = $action->toArray();
            if($action) {
                $actionData['config'] = ['disc_fixed_price' => $disc_price];
                PromotionActionRepository::updateModel($actionData);
            } else {
                $actionData['promotion_id'] = $model->id;
                $actionData['type'] = PromotionActionType::$MAP['item_fixed_disc'];
                $actionData['config'] = ['disc_fixed_price' => $disc_price];
                PromotionActionRepository::createModel($actionData);
            }

            //Updates all the new price in item variants by promotion id after changing the discount fixed price in headerform section
            PromotionVariantRepository::updateAllVariantByPromotionId($model->id, $disc_price);
        }

        $model = self::processOutgoingModel($model);

        $message = __('Promotion.record_successfully_updated', ['code'=>$model->code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'model'=>$model
            ),
			'message' => $message
		);
    }
    
    public function uploadPhotos($promotionId, $fileArray)
	{
        $promotionPhotoArray = array();

        foreach($fileArray as $files)
        {
            foreach ($files as $file)
            {
                // Log::info(print_r($file, true));
                $promotionPhotoData = array();
                $promotionPhotoData['promotionId'] = $promotionId;
                $promotionPhotoData['photo_desc_01'] = $file->getClientOriginalName();
                $promotionPhotoData['photo_desc_02'] = '';
                $promotionPhotoData['old_filename'] = $file->getRealPath();
                $promotionPhotoData['blob'] = $file;
                $promotionPhotoModel = PromotionPhotoRepository::savePromotionPhoto($promotionPhotoData);
    
                $promotionPhotoArray[] = $promotionPhotoModel;
            }
        }
        if (count($promotionPhotoArray) > 0) {
            $message = __('Promotion.photo_successfully_uploaded', []);
        }
        else {
            $message = __('Promotion.no_photo_uploaded', []);
        }

		return array(
			'data' => array(
                'timestamp'=>time(),
                'photos'=>$promotionPhotoArray,
                'deleted_photos'=>array(),
            ),
			'message' => $message
		);
    }
    
    public function updateRule($promotionId, $data)
    {
        // AuthService::authorize(
        //     array(
        //         'sls_ord_update'
        //     )
        // );

        $promotion = PromotionRepository::findByPk($promotionId);
        $promotionData = $promotion->toArray();
        $rules = array();
        
        if ($data['type'] == 'debtor') {
            //json
            $config = [];

            $rule = PromotionRule::where('promotion_id', $promotionId)
                    ->where('type', PromotionRuleType::$MAP['debtor'])->first();

            if ($rule && $rule->id == $data['id']) {
                $config = ['debtor_id'=>implode(',', $data['debtor_ids'])];
                $data['type'] = PromotionRuleType::$MAP['debtor'];
                $data['config'] = $config; 
                
                $data = self::processIncomingRule($data);
                
                // $data['promotion_id'] = $promotionId;
                $result = PromotionRuleRepository::updateModel($data);
                $ruleModel = $result['model'];
                $rules[] = self::processOutgoingRule($ruleModel);
            }
        }
        else if ($data['type'] == 'debtor_group01') {
            // $data['type'] = PromotionRuleType::$MAP['debtor_group01'];
            // $data['config'] = ['debtor_group_01_id'=>$data['debtor_group_01_id']]; 
            
            //json
            $config = [];

            $rule = PromotionRule::where('promotion_id', $promotionId)
                    ->where('type', PromotionRuleType::$MAP['debtor_group'])->first();

            if ($rule && $rule->id == $data['id']) {
                $config = ['debtor_group_01_id'=>implode(',', $data['debtor_group_01_ids'])];
                $data['type'] = PromotionRuleType::$MAP['debtor_group'];
                $data['config'] = $config; 
                
                $data = self::processIncomingRule($data);
                
                // $data['promotion_id'] = $promotionId;
                $result = PromotionRuleRepository::updateModel($data);
                $ruleModel = $result['model'];
                $rules[] = self::processOutgoingRule($ruleModel);
            }
        }

        $message = __('Promotion.rule_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'promotion'=>$promotionData,
                'promotion_rules'=>$rules,
            ),
			'message' => $message
		);
    }
    
    public function updateVariant($promotionId, $data)
	{
        // AuthService::authorize(
        //     array(
        //         'sls_ord_update'
        //     )
        // );

        $data = self::processIncomingDetail($data);
        if ($data['variant_type'] == 'item') {
        }
        else if ($data['variant_type'] == 'item_group01') {
        }

        $promotion = PromotionRepository::findByPk($promotionId);
        $promotionData = $promotion->toArray();
        
        $data['promotion_id'] = $promotionId;
        $variantModel = PromotionRepository::updateVariantModel($data);
        $variants = array();
        $variants[] = self::processOutgoingVariant($variantModel);

        $message = __('Promotion.variant_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'promotion'=>$promotionData,
                'promotion_variants'=>$variants,
            ),
			'message' => $message
		);
    }
    
    public function reorderPromo($curPromoId, $prevPromoId, $curDivisionId)
    {
        $data = PromotionRepository::reorderPromo($curPromoId, $prevPromoId, $curDivisionId);
        $message = __('ItemGroup01.promotions_successfully_reordered', []);

        return array(
            'data' => $data,
            'message' => $message
        );
    }

    public function syncProcess($strProcType, $siteFlowId, $divisionId)
    {
        $user = Auth::user();

        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['PROMO_SYNC_01']) 
			{
				//sync price group from EfiChain as promotion
				return $this->syncPromotionSync01($user->id, $divisionId);
            }
		}
    }

    protected function syncPromotionSync01($userId, $divisionId)
    {
        // AuthService::authorize(
        //     array(
        //         'promo_import'
        //     )
        // );

        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['PROMO_SYNC_01'], $userId);

        $syncSettingHdr = SyncSettingHdrRepository::findByProcTypeAndDivisionId(ProcType::$MAP['SYNC_EFICHAIN_01'], $divisionId);
        if(empty($syncSettingHdr))
        {
            $exc = new ApiException(__('Promotion.sync_setting_not_found', ['divisionId'=>$divisionId]));
            //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
            throw $exc;
        }

        $client = new Client();
        $header = array();

        $page = 1;
        $lastPage = 1;
        $pageSize = 1000;
        $total = 0;

        $usernameSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'username');
        $passwordSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'password');
        $formParams = array(
            'UserLogin[username]' => $usernameSetting->value,
            'UserLogin[password]' => $passwordSetting->value
        );
        $pageSizeSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'page_size');
        if(!empty($pageSizeSetting))
        {
            $pageSize = $pageSizeSetting->value;
        }

        //use division code 
        $divisionDB = Division::where('id', $divisionId)->first();

        while($page <= $lastPage)
        {
            $url = $syncSettingHdr->url.'/index.php?r=bizOrder/getPriceGroups&division='.$divisionDB->code.'&page_size='.$pageSize.'&page='.$page;
            
            $response = $client->request('POST', $url, array('headers' => $header, 'form_params' => $formParams));
            $result = $response->getBody()->getContents();
            $result = json_decode($result, true);
            if(empty($result))
            {
                $exc = new ApiException(__('Promotion.fail_to_sync', ['url'=>$url]));
                //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
                throw $exc;
            }

            // $total = $result['total'];
            $promotionArray = array();
            for ($a = 0; $a < count($result['data']); $a++)
            {
                $priceGrpData = $result['data'][$a];

                if (!array_key_exists($priceGrpData['pg_code'], $promotionArray))
                {
                    $promotionArray[$priceGrpData['pg_code']] = array();
                }

                $promotionArray[$priceGrpData['pg_code']][] = $priceGrpData;

                // $statusNumber = 100;
                // if($total > 0)
                // {
                //     $statusNumber = bcmul(bcdiv(($a + ($page - 1) * $pageSize), $total, 8), 100, 2);
                // }
                // BatchJobStatusRepository::updateStatusNumber($batchJobStatusModel->id, $statusNumber);
            }

            $total = count($promotionArray);
            $a = 0;
            foreach ($promotionArray as $code=>$promotionDataArray)
            {
                $promotionModel = PromotionRepository::syncPromoSync01($code, $promotionDataArray, $divisionId, $syncSettingHdr->id);
    
                $statusNumber = 100;
                if($total > 0)
                {
                    $statusNumber = bcmul(bcdiv(($a + ($page - 1) * $pageSize), $total, 8), 100, 2);
                }
                BatchJobStatusRepository::updateStatusNumber($batchJobStatusModel->id, $statusNumber);

                $a++;
            }
                

            $lastPage = $total % $pageSize == 0 ? ($total / $pageSize) : ($total / $pageSize) + 1;

            $page++;
        }

        $batchJobStatusModel = BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);
        
        $message = __('Promotion.sync_success', ['total'=>$total]);
		return array(
			'data' => array(
                'timestamp'=>time(),
                'batchJobStatusModel'=>$batchJobStatusModel
            ),
			'message' => $message
		);
    }

    static public function processIncomingDetail($data)
    {
        //preprocess the select2 and dropDown
        if(array_key_exists('item_select2', $data))
        {
            $data['item_id'] = $data['item_select2']['value'];
            unset($data['item_select2']);
        }
        if(array_key_exists('item_code', $data))
        {
            unset($data['item_code']);
        }
        if(array_key_exists('item', $data))
        {
            unset($data['item']);
        }
        if(array_key_exists('uom', $data))
        {
            unset($data['uom']);
		}
		if(array_key_exists('item_code', $data))
        {
            unset($data['item_code']);
		}
		if(array_key_exists('item_ref_code_01', $data))
        {
            unset($data['item_ref_code_01']);
		}
		if(array_key_exists('item_desc_01', $data))
        {
            unset($data['item_desc_01']);
		}
		if(array_key_exists('item_desc_02', $data))
        {
            unset($data['item_desc_02']);
		}
		if(array_key_exists('item_group_01_code', $data))
        {
            unset($data['item_group_01_code']);
		}
		if(array_key_exists('item_group_02_code', $data))
        {
            unset($data['item_group_02_code']);
		}
		if(array_key_exists('item_group_03_code', $data))
        {
            unset($data['item_group_03_code']);
		}
		if(array_key_exists('item_group_04_code', $data))
        {
            unset($data['item_group_04_code']);
		}
		if(array_key_exists('item_group_05_code', $data))
        {
            unset($data['item_group_05_code']);
		}
        return $data;
    }

    static public function processIncomingModel($data)
    {
        if(array_key_exists('submit_action', $data))
        {
            unset($data['submit_action']);
        }
        if(array_key_exists('buy_qty', $data))
        {
            unset($data['buy_qty']);
        }
        if(array_key_exists('foc_qty', $data))
        {
            unset($data['foc_qty']);
        }
        if(array_key_exists('disc_fixed_price', $data))
        {
            unset($data['disc_fixed_price']);
        }
        if(array_key_exists('disc_perc_01', $data))
        {
            unset($data['disc_perc_01']);
        }
        return $data;
    }

    static public function processIncomingRule($data)
    {
        //preprocess the select2 and dropDown
        if(array_key_exists('debtor_ids', $data))
        {
            unset($data['debtor_ids']);
        }
        if(array_key_exists('debtor_ids_select2', $data))
        {
            unset($data['debtor_ids_select2']);
        }
        if(array_key_exists('debtor_group_01_ids', $data))
        {
            unset($data['debtor_group_01_ids']);
        }
        if(array_key_exists('debtor_group_01_ids_select2', $data))
        {
            unset($data['debtor_group_01_ids_select2']);
        }
        if(array_key_exists('item_select2', $data))
        {
            $data['item_id'] = $data['item_select2']['value'];
            unset($data['item_select2']);
        }
        if(array_key_exists('item_code', $data))
        {
            unset($data['item_code']);
        }
        if(array_key_exists('item', $data))
        {
            unset($data['item']);
        }
        if(array_key_exists('uom', $data))
        {
            unset($data['uom']);
		}
		if(array_key_exists('item_code', $data))
        {
            unset($data['item_code']);
		}
		if(array_key_exists('item_ref_code_01', $data))
        {
            unset($data['item_ref_code_01']);
		}
		if(array_key_exists('item_desc_01', $data))
        {
            unset($data['item_desc_01']);
		}
		if(array_key_exists('item_desc_02', $data))
        {
            unset($data['item_desc_02']);
		}
		if(array_key_exists('item_group_01_code', $data))
        {
            unset($data['item_group_01_code']);
		}
		if(array_key_exists('item_group_02_code', $data))
        {
            unset($data['item_group_02_code']);
		}
		if(array_key_exists('item_group_03_code', $data))
        {
            unset($data['item_group_03_code']);
		}
		if(array_key_exists('item_group_04_code', $data))
        {
            unset($data['item_group_04_code']);
		}
		if(array_key_exists('item_group_05_code', $data))
        {
            unset($data['item_group_05_code']);
		}
        return $data;
    }

    static public function processOutgoingModel($model)
	{
        $actions = $model->actions;
        foreach ($actions as $action) 
        {
            if ($action->type == PromotionActionType::$MAP['item_percent_disc']) 
            {
                if (isset($action->config['buy_qty'])) {
                    $model->buy_qty = $action->config['buy_qty'];
                }
            }
            if ($action->type == PromotionActionType::$MAP['item_foc']) 
            {
                if (isset($action->config['buy_qty'])) {
                    $model->buy_qty = $action->config['buy_qty'];
                }
                if (isset($action->config['foc_qty'])) {
                    $model->foc_qty = $action->config['foc_qty'];
                }
            }
            if ($action->type == PromotionActionType::$MAP['item_fixed_disc'])
            {
                if(isset($action->config['disc_fixed_price']))
                {
                    $model->disc_price = $action->config['disc_fixed_price'];
                }
            }
        }
		return $model;
    }
    
    static public function processOutgoingRule($model)
    {
        $model->type_str = PromotionRuleType::$MAP[$model->type];

        $description = '';
        $initDebtorOption = array();
        $initDebtorGroupOption = array();
        if ($model->type == PromotionRuleType::$MAP['debtor']) 
        {
            $value = $model->config['debtor_id'];
            $debtorIds = explode(',', $value);
            foreach ($debtorIds as $debtorId)
            {
                if (empty($debtorId))
                {
                    continue;
                }

                $debtor = DebtorRepository::findByPk($debtorId);
                if ($description != '') {
                    $description .= '<br/>';
                }
                $description .= $debtor->code . ' - ' . $debtor->company_name_01;
                
                $initDebtorOption[] = array(
                    'value'=>$debtor->id, 
                    'label'=>$debtor->code.' '.$debtor->company_name_01,
                );
            }
        }
        else if ($model->type == PromotionRuleType::$MAP['debtor_group']) 
        {
            $value = $model->config['debtor_group_01_id'];
            $debtorGroupIds = explode(',', $value);
            foreach ($debtorGroupIds as $debtorGroupId)
            {
                if (empty($debtorGroupId))
                {
                    continue;
                }

                $debtorGroup = DebtorGroup01Repository::findByPk($debtorGroupId);
                if ($description != '') {
                    $description .= '<br/>';
                }
                $description .= $debtorGroup->code . ' - ' . $debtorGroup->desc_01;
                
                $initDebtorGroupOption[] = array(
                    'value'=>$debtorGroup->id, 
                    'label'=>$debtorGroup->code.' '.$debtorGroup->desc_01,
                );
            }
        }
        $model->description = $description;

        $model->debtor_ids_select2 = $initDebtorOption;
        $model->debtor_group_ids_select2 = $initDebtorGroupOption;
        
        $ruleType = array(
            'value'=>0, 
            'label'=>'',
        );
		if($model->type == PromotionRuleType::$MAP['debtor']) 
		{
			$ruleType = array(
				'value'=>3, 
				'label'=>'Debtor',
			);
		} else if($model->type == PromotionRuleType::$MAP['debtor_group']) {
			$ruleType = array(
				'value'=>4, 
				'label'=>'Debtor Group',
			);
		}
		$model->rule_type_select2 = $ruleType;
        
        return $model;
    }

    static public function processOutgoingVariant($model)
    {
        $item = $model->item;
        // item select2
        $initItemOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($item))
        {
            $model->item_code = $item->code;
            $model->item_desc_01 = $item->desc_01;
            $model->item_desc_02 = $item->desc_02;
            
            $initItemOption = array(
                'value'=>$item->id, 
                'label'=>$item->code.' '.$item->desc_01,
            );
            
            $itemUom = ItemUomRepository::findSmallestByItemId($item->id);
            if(!empty($itemUom))
            {
                $itemSalePrice = ItemSalePriceRepository::findByItemIdAndUomId(now(), 0, 1, $item->id, $itemUom->uom_id);
                if(!empty($itemSalePrice))
                {
                    $model->sale_price = round($itemSalePrice->sale_price, config('scm.decimal_scale'));
                }
            }
        }
        $model->item_select2 = $initItemOption;

		$itemGroup01 = $model->itemGroup01;
		$initGroup01Option = array(
            'value'=>0,
            'label'=>''
		);
		
		if (!empty($itemGroup01)) {
			$model->item_group_01_code = $itemGroup01->code;
			$model->item_group_01_desc_01 = $itemGroup01->desc_01;
			$model->item_group_01_desc_02 = $itemGroup01->desc_02;
			
			$initGroup01Option = array(
                'value'=>$itemGroup01->id, 
                'label'=>$itemGroup01->code
            );
		}
		$model->item_group_01_select2 = $initGroup01Option;

		$itemGroup02 = $model->itemGroup02;
		$initGroup02Option = array(
            'value'=>0,
            'label'=>''
		);
		
		if (!empty($itemGroup02)) {
			$model->item_group_02_code = $itemGroup02->code;
			$model->item_group_02_desc_01 = $itemGroup02->desc_01;
			$model->item_group_02_desc_02 = $itemGroup02->desc_02;
			
			$initGroup02Option = array(
                'value'=>$itemGroup02->id, 
                'label'=>$itemGroup02->code
            );
		}
		$model->item_group_02_select2 = $initGroup02Option;

		$itemGroup03 = $model->itemGroup03;
		$initGroup03Option = array(
            'value'=>0,
            'label'=>''
		);
		
		if (!empty($itemGroup03)) {
			$model->item_group_03_code = $itemGroup03->code;
			$model->item_group_03_desc_01 = $itemGroup03->desc_01;
			$model->item_group_03_desc_02 = $itemGroup03->desc_02;
			
			$initGroup03Option = array(
                'value'=>$itemGroup03->id, 
                'label'=>$itemGroup03->code
            );
		}
		$model->item_group_03_select2 = $initGroup03Option;

		if($model->variant_type === 'item_group01') 
		{
			$variantType = array(
				'value'=>1, 
				'label'=>'Brand',
			);
		} else if($model->variant_type === 'item_group02') {
			$variantType = array(
				'value'=>2, 
				'label'=>'Category',
			);
		} else if($model->variant_type === 'item_group03') {
			$variantType = array(
				'value'=>3, 
				'label'=>'Manufacturer',
			);
		} else {
			$variantType = array(
				'value'=>0, 
				'label'=>'Item',
			);
		}

		$model->variant_type_select2 = $variantType;

        return $model;
	}
}
