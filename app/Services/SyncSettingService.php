<?php

namespace App\Services;

use App\SyncSettingHdr;
use App\Services\Env\ProcType;
use App\Repositories\SyncSettingHdrRepository;
use App\Repositories\SyncSettingDtlRepository;
class SyncSettingService 
{
    public function __construct() 
	{
    }

    public function showSiteFlowSetting($strProcType, $siteFlowId)
    {
        $syncSettingHdrModel = SyncSettingHdrRepository::findByProcTypeAndSiteFlowId(ProcType::$MAP[$strProcType], $siteFlowId);
        $syncSettingHdrModel->load('syncSettingDtls');

        $syncSettingDtlModels = $syncSettingHdrModel->syncSettingDtls;
        foreach($syncSettingDtlModels as $syncSettingDtlModel)
        {
            if(strcmp($syncSettingDtlModel->field, 'password') == 0)
            {
                $syncSettingHdrModel->{$syncSettingDtlModel->field} = '*****';
            }
            else
            {
                $syncSettingHdrModel->{$syncSettingDtlModel->field} = $syncSettingDtlModel->value;
            }
        }
        unset($syncSettingHdrModel->syncSettingDtls);

        return $syncSettingHdrModel;
    }

    public function showDivisionSetting($strProcType, $divisionId)
    {
        $syncSettingHdrModel = SyncSettingHdrRepository::findByProcTypeAndDivisionId(ProcType::$MAP[$strProcType], $divisionId);
        
        if ($syncSettingHdrModel) {
            $syncSettingHdrModel->load('syncSettingDtls');
        }
        else {
            $syncSettingHdrModel = $this->initModel($divisionId);
        }

        $syncSettingHdrModel = self::processOutgoingModel($syncSettingHdrModel);

        return $syncSettingHdrModel;
    }
    
    public function initModel($divisionId)
    {
        // AuthService::authorize(
        //     array(
        //         'item_create'
        //     )
        // );

        $model = new SyncSettingHdr();
        $model->proc_type = '20000';
        $model->site_flow_id = 1;
        $model->division_id = $divisionId;
        $model->url = '';
        $model->page_size = 1000;
        $model->last_total = 0;
        $model->last_synced_at = '2000-01-01 00:00:00';
        $model->last_user_id = 0;
        $model->item_brand_mapping = array('value'=>'', 'label'=>'N/A');
        $model->item_category_mapping = array('value'=>'', 'label'=>'N/A');
        $model->item_manufacturer_mapping = array('value'=>'', 'label'=>'N/A');
        $model->debtor_category_mapping = array('value'=>'', 'label'=>'N/A');

        return $model;
    }
    
    public function updateModel($data)
	{
        // AuthService::authorize(
        //     array(
        //         'item_update'
        //     )
        // );

        $data = self::processIncomingModel($data);
        if (isset($data['id'])) {
            $item = SyncSettingHdrRepository::findByPk($data['id']);
        }
        else {
            $hdrData = $data;
            unset($hdrData['details']);
            $item = SyncSettingHdrRepository::createHeader($hdrData);
        }
        $itemData = $item->toArray();
        $itemDetailData = array();
        //assign data to model
        foreach($data as $field => $value) 
        {
            if ($field == 'details')
            {
                foreach ($value as $detailField => $detailValue)
                {
                    $itemDetailData[$detailField] = $detailValue;
                }
            }
            else
            {
                $itemData[$field] = $value;
            }
        }

        if (!isset($itemData['proc_type']) || $itemData['proc_type'] == 0)
        {
            //default to 20000 sync efichain
            $itemData['proc_type'] = 20000;
        }

        $result = SyncSettingHdrRepository::updateModel($itemData, $itemDetailData);
        $model = $result['model'];

        $model = self::processOutgoingModel($model);

        $message = __('SyncSetting.record_successfully_updated', ['code'=>$model->code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'model'=>$model
            ),
			'message' => $message
		);
    }

    static public function processIncomingModel($data)
    {
        if(array_key_exists('submit_action', $data))
        {
            unset($data['submit_action']);
        }

        if(array_key_exists('password', $data))
        {
            $data['details']['password'] = $data['password'];
            unset($data['password']);
        }

        if(array_key_exists('username', $data))
        {
            $data['details']['username'] = $data['username'];
            unset($data['username']);
        }

        if(array_key_exists('item_brand_mapping_select2', $data))
        {
            $data['details']['item_brand_mapping'] = $data['item_brand_mapping_select2'] ?? '';
            unset($data['item_brand_mapping_select2']);
        }

        if(array_key_exists('item_brand_mapping', $data))
        {
            unset($data['item_brand_mapping']);
        }

        if(array_key_exists('item_category_mapping_select2', $data))
        {
            $data['details']['item_category_mapping'] = $data['item_category_mapping_select2'] ?? '';
            unset($data['item_category_mapping_select2']);
        }

        if(array_key_exists('item_category_mapping', $data))
        {
            unset($data['item_category_mapping']);
        }

        if(array_key_exists('item_manufacturer_mapping_select2', $data))
        {
            $data['details']['item_manufacturer_mapping'] = $data['item_manufacturer_mapping_select2'] ?? '';
            unset($data['item_manufacturer_mapping']);
        }

        if(array_key_exists('item_manufacturer_mapping', $data))
        {
            unset($data['item_manufacturer_mapping']);
        }

        if(array_key_exists('debtor_category_mapping_select2', $data))
        {
            $data['details']['debtor_category_mapping'] = $data['debtor_category_mapping_select2'] ?? '';
            unset($data['debtor_category_mapping']);
        }

        if(array_key_exists('debtor_category_mapping', $data))
        {
            unset($data['debtor_category_mapping']);
        }
        
        return $data;
    }

    static public function processOutgoingModel($model)
	{
        $syncSettingDtlModels = $model->syncSettingDtls;
        foreach($syncSettingDtlModels as $syncSettingDtlModel)
        {
            if(strcmp($syncSettingDtlModel->field, 'password') == 0)
            {
                $model->{$syncSettingDtlModel->field} = '*****';
            }
            else if(strcmp($syncSettingDtlModel->field, 'item_brand_mapping') == 0 || 
                    strcmp($syncSettingDtlModel->field, 'item_category_mapping') == 0 || 
                    strcmp($syncSettingDtlModel->field, 'item_manufacturer_mapping') == 0)
            {
                if ($syncSettingDtlModel->value == 'item_group')
                {
                    $model->{$syncSettingDtlModel->field} = 'item_group';
                    $model->{$syncSettingDtlModel->field.'_select2'} = array('value'=>0, 'label'=>'Item Group');
                }
                else if ($syncSettingDtlModel->value == 'item_group_01')
                {
                    $model->{$syncSettingDtlModel->field} = 'item_group_01';
                    $model->{$syncSettingDtlModel->field.'_select2'} = array('value'=>1, 'label'=>'Item Group 01');
                }
                else if ($syncSettingDtlModel->value == 'item_group_02')
                {
                    $model->{$syncSettingDtlModel->field} = 'item_group_02';
                    $model->{$syncSettingDtlModel->field.'_select2'} = array('value'=>2, 'label'=>'Item Group 02');
                }
                else
                {
                    $model->{$syncSettingDtlModel->field} = '';
                    $model->{$syncSettingDtlModel->field.'_select2'} = array('value'=>'', 'label'=>'N/A');
                }
            }
            else if(strcmp($syncSettingDtlModel->field, 'debtor_category_mapping') == 0)
            {
                if ($syncSettingDtlModel->value == 'customer_category')
                {
                    $model->{$syncSettingDtlModel->field} = 'customer_category';
                    $model->{$syncSettingDtlModel->field.'_select2'} = array('value'=>0, 'label'=>'Customer Category');
                }
                else if ($syncSettingDtlModel->value == 'customer_category_01')
                {
                    $model->{$syncSettingDtlModel->field} = 'customer_category_01';
                    $model->{$syncSettingDtlModel->field.'_select2'} = array('value'=>1, 'label'=>'Customer Category 01');
                }
                else if ($syncSettingDtlModel->value == 'customer_category_02')
                {
                    $model->{$syncSettingDtlModel->field} = 'customer_category_02';
                    $model->{$syncSettingDtlModel->field.'_select2'} = array('value'=>2, 'label'=>'Customer Category 02');
                }
                else
                {
                    $model->{$syncSettingDtlModel->field} = '';
                    $model->{$syncSettingDtlModel->field.'_select2'} = array('value'=>'', 'label'=>'N/A');
                }
            }
            else
            {
                $model->{$syncSettingDtlModel->field} = $syncSettingDtlModel->value;
            }
        }
        
        if(!isset($model->item_brand_mapping))
        {
            $model->item_brand_mapping = '';
            $model->item_brand_mapping_select2 = array('value'=>'', 'label'=>'N/A');
        }
        if(!isset($model->item_category_mapping))
        {
            $model->item_category_mapping = '';
            $model->item_category_mapping_select2 = array('value'=>'', 'label'=>'N/A');
        }
        if(!isset($model->item_manufacturer_mapping))
        {
            $model->item_manufacturer_mapping = '';
            $model->item_manufacturer_mapping_select2 = array('value'=>'', 'label'=>'N/A');
        }
        if(!isset($model->debtor_category_mapping))
        {
            $model->debtor_category_mapping = '';
            $model->debtor_category_mapping_select2 = array('value'=>'', 'label'=>'N/A');
        }

        unset($model->syncSettingDtls);

		return $model;
    }
}
