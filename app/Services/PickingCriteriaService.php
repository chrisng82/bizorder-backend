<?php

namespace App\Services;

use App\Repositories\PickingCriteriaRepository;
use App\Repositories\PickListOutbOrdRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PickingCriteriaService
{
    public function __construct()
    {
	}
	
	static public function processByPickListHdrId($siteFlow, $item, $pickListHdrId)
    {
        $storageClass = $item->storage_class;
        $itemGroup01Id = $item->item_group_01_id; //brand
        $itemGroup02Id = $item->item_group_02_id; //category
        $itemGroup03Id = $item->item_group_03_id; //principal
        $itemGroup04Id = $item->item_group_04_id; //market segment
        $itemGroup05Id = $item->item_group_05_id;
        $itemId = $item->id;

        $debtorGroup01Ids = array(); //category
        $debtorGroup02Ids = array(); //chain
        $debtorGroup03Ids = array(); //channel
        $debtorGroup04Ids = array();
        $debtorGroup05Ids = array();
        $deliveryPointIds = array();

        $pickListOutbOrds = PickListOutbOrdRepository::findAllByHdrId($pickListHdrId);
        $pickListOutbOrds->load('outbOrdHdr', 'outbOrdHdr.deliveryPoint');
        foreach($pickListOutbOrds as $pickListOutbOrd)
        {
            $outbOrdHdr = $pickListOutbOrd->outbOrdHdr;
            $deliveryPoint = $outbOrdHdr->deliveryPoint;

            $debtorGroup01Ids[] = $deliveryPoint->debtor_group_01_id;
            $debtorGroup02Ids[] = $deliveryPoint->debtor_group_02_id;
            $debtorGroup03Ids[] = $deliveryPoint->debtor_group_03_id;
            $debtorGroup04Ids[] = $deliveryPoint->debtor_group_04_id;
            $debtorGroup05Ids[] = $deliveryPoint->debtor_group_05_id;
            $deliveryPointIds[] = $deliveryPoint->id;
        }

        //find all the picking_criteria that match the ids, OR relationship
        $pickingCriterias = PickingCriteriaRepository::findAllByAttributes(
            $siteFlow->site_id, 
            $storageClass, $itemGroup01Id, $itemGroup02Id,
            $itemGroup03Id, $itemGroup04Id, $itemGroup05Id, $itemId,
            $debtorGroup01Ids, $debtorGroup02Ids, $debtorGroup03Ids, 
            $debtorGroup04Ids, $debtorGroup05Ids, $deliveryPointIds);
        
        //filter by the field, make sure only 1 field type will be return
        $pickingCriterias->sortByDesc('line_no');
        $pickingCriteriaHash = array();
        foreach($pickingCriterias as $pickingCriteria)
        {
            if(array_key_exists($pickingCriteria->field, $pickingCriteriaHash))
            {
                //skip if already have this field
                continue;
            }
            $pickingCriteriaHash[$pickingCriteria->field] = $pickingCriteria;
        }
        return array_values($pickingCriteriaHash);
    }

    static public function processByDeliveryPointAndItem($siteFlow, $deliveryPoint, $item)
    {
        $storageClass = $item->storage_class;
        $itemGroup01Id = $item->item_group_01_id; //brand
        $itemGroup02Id = $item->item_group_02_id; //category
        $itemGroup03Id = $item->item_group_03_id; //principal
        $itemGroup04Id = $item->item_group_04_id; //market segment
        $itemGroup05Id = $item->item_group_05_id;
        $itemId = $item->id;

        $debtorGroup01Ids = array(); //category
        $debtorGroup02Ids = array(); //chain
        $debtorGroup03Ids = array(); //channel
        $debtorGroup04Ids = array();
        $debtorGroup05Ids = array();
        $deliveryPointIds = array();

        $debtorGroup01Ids[] = $deliveryPoint->debtor_group_01_id;
        $debtorGroup02Ids[] = $deliveryPoint->debtor_group_02_id;
        $debtorGroup03Ids[] = $deliveryPoint->debtor_group_03_id;
        $debtorGroup04Ids[] = $deliveryPoint->debtor_group_04_id;
        $debtorGroup05Ids[] = $deliveryPoint->debtor_group_05_id;
        $deliveryPointIds[] = $deliveryPoint->id;

        //find all the picking_criteria that match the ids, OR relationship
        //DB::connection()->enableQueryLog();
        $pickingCriterias = PickingCriteriaRepository::findAllByAttributes(
            $siteFlow->site_id, 
            $storageClass, $itemGroup01Id, $itemGroup02Id,
            $itemGroup03Id, $itemGroup04Id, $itemGroup05Id, $itemId,
            $debtorGroup01Ids, $debtorGroup02Ids, $debtorGroup03Ids, 
            $debtorGroup04Ids, $debtorGroup05Ids, $deliveryPointIds);
        //Log::error(DB::getQueryLog());
        //filter by the field, make sure only 1 field type will be return
        $pickingCriterias->sortByDesc('line_no');
        $pickingCriteriaHash = array();
        foreach($pickingCriterias as $pickingCriteria)
        {
            if(array_key_exists($pickingCriteria->field, $pickingCriteriaHash))
            {
                //skip if already have this field
                continue;
            }
            $pickingCriteriaHash[$pickingCriteria->field] = $pickingCriteria;
        }
        return array_values($pickingCriteriaHash);
    }
}
