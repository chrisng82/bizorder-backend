<?php

namespace App\Services;

use App\Repositories\SyncSettingHdrRepository;
use App\Repositories\SyncSettingDtlRepository;
use App\Repositories\QuantBalRepository;
use App\Repositories\DivisionRepository;
use App\Repositories\BatchJobStatusRepository;
use App\Services\Env\ProcType;
use App\Services\Utils\ApiException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EfiChainService 
{
	public function __construct() 
	{
    }

    public function syncProcess($strProcType, $divisionId)
    {
		$user = Auth::user();

        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['SYNC_EFICHAIN_01']) 
			{
				//export stock balance
				return $this->syncEfiChain01($divisionId, $user->id);
            }
            elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['SYNC_EFICHAIN_02'])
            {
                //export sales order
                return $this->syncEfiChain02($divisionId, $user->id);
            }
		}
    }

    protected function syncEfiChain01($divisionId, $userId)
    {
        $division = DivisionRepository::findByPk($divisionId);

        //export stock balance
        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['SYNC_EFICHAIN_01'], $userId);

        $syncSettingHdr = SyncSettingHdrRepository::findByProcTypeAndDivisionId(ProcType::$MAP['SYNC_EFICHAIN_01'], $divisionId);
        if(empty($syncSettingHdr))
        {
            $exc = new ApiException(__('EfiChain.sync_setting_not_found', ['divisionId'=>$divisionId]));
            //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
            throw $exc;
        }

        $quantBals = QuantBalRepository::queryBalanceUnitQtyBySiteId($division->site_id);

        $client = new Client();
        $header = array();

        $usernameSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'username');
        $passwordSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'password');
        $formParams = array(
            'UserLogin[username]' => $usernameSetting->value,
            'UserLogin[password]' => $passwordSetting->value
        );		
        
        $formParams['Division'] = json_encode($division);
		$formParams['QuantBals'] = json_encode($quantBals);

        $url = $syncSettingHdr->url.'/index.php?r=luuWu/postStockBalance';
            
		$response = $client->post($url, array('headers' => $header, 'form_params' => $formParams));
		
		$result = $response->getBody()->getContents();
		$result = json_decode($result, true);
		if(empty($result))
		{
			BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

			$exc = new ApiException(__('EfiChain.fail_to_sync', ['url'=>$url]));
			//$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
			throw $exc;
		}
		else
		{
			if($result['success'] === false)
			{
				BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

				$exc = new ApiException(__('EfiChain.fail_to_sync', ['url'=>$url,'message'=>$result['message']]));
				//$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
				throw $exc;
			}
		}

		$batchJobStatusModel = BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

		return array(
			'data' => $batchJobStatusModel,
			'message' => $result['message']
		);
    }

    public function syncEfiChain02($divisionId, $userId)
    {
        //export transactions
        //export purchase receive
        //export return delivery (N/A)
        //export return receive (N/A)
        //export sales invoice (N/A)

        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['PICK_LIST_SYNC_01'], $userId);

		//query the completed pickList associated outbOrdHdrs from DB

		//DB::connection()->enableQueryLog();
		$pickListOutbOrds = PickListOutbOrdRepository::findAllByCompletePickList($divisionId, $startDate, $endDate);
		//dd(DB::getQueryLog());
		$postSlsOrds = array();
		foreach($pickListOutbOrds as $pickListOutbOrd)
		{
			$data = array(
				'sls_ord_doc_code' => $pickListOutbOrd->sls_ord_hdr_code,
				'pick_list_doc_code' => $pickListOutbOrd->pick_list_doc_code,
				'pick_list_doc_date' => $pickListOutbOrd->pick_list_doc_date,
			);
			$postSlsOrds[] = $data;
		}

        $syncSettingHdr = SyncSettingHdrRepository::findByProcTypeAndDivisionId(ProcType::$MAP['SYNC_EFICHAIN_01'], $divisionId);
        if(empty($syncSettingHdr))
        {
            $exc = new ApiException(__('PickList.sync_setting_not_found', ['divisionId'=>$divisionId]));
            //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
            throw $exc;
        }

        $client = new Client();
        $header = array();

        $usernameSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'username');
        $passwordSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'password');
        $formParams = array(
            'UserLogin[username]' => $usernameSetting->value,
            'UserLogin[password]' => $passwordSetting->value
		);		
		$formParams['SlsOrdHdrs'] = $postSlsOrds;

        $url = $syncSettingHdr->url.'/index.php?r=luuWu/postPickLists';
            
		$response = $client->post($url, array('headers' => $header, 'form_params' => $formParams));
		
		$result = $response->getBody()->getContents();
		$result = json_decode($result, true);
		if(empty($result))
		{
			BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

			$exc = new ApiException(__('PickList.fail_to_sync', ['url'=>$url]));
			//$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
			throw $exc;
		}
		else
		{
			if($result['success'] === false)
			{
				BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

				$exc = new ApiException(__('PickList.fail_to_sync', ['url'=>$url,'message'=>$result['message']]));
				//$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
				throw $exc;
			}
		}

		/*
		$successPickListDocCodes = array();
		for ($a = 0; $a < count($result['data']); $a++)
		{
			$successPickListDocCodes[] = $result['data'][$a];
		}
		*/
		$batchJobStatusModel = BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

		return array(
			'data' => $batchJobStatusModel,
			'message' => $result['message']
		);
    }
}