<?php

namespace App\Services\Utils;

class RepositoryUtils
{
    public static function dataToModel($model, $data)
    {
        foreach($data as $fieldName => $value)
        {
            if(strcmp($fieldName, 'is_modified') == 0
            || strcmp($fieldName, 'is_deleted') == 0
            || strcmp($fieldName, 'id') == 0
            //|| strcmp($fieldName, 'hdr_id') == 0
            || strcmp($fieldName, 'doc_status') == 0
            || strcmp($fieldName, 'created_at') == 0
            || strcmp($fieldName, 'updated_at') == 0
            || strpos($fieldName, '_options') !== false
            || strpos($fieldName, '_select2') !== false)
            {
                continue;
            }
            if(is_null($value))
            {
                $value = '';
            }
            $model->{$fieldName} = $value;
        }
        return $model;
    }

    public static function modelToData($data, $model)
    {
        $modelData = $model->toArray();
        foreach($modelData as $fieldName => $value)
        {
            if(strcmp($fieldName, 'is_modified') == 0
            || strcmp($fieldName, 'is_deleted') == 0
            || strcmp($fieldName, 'id') == 0
            //|| strcmp($fieldName, 'hdr_id') == 0
            || strcmp($fieldName, 'doc_status') == 0
            || strcmp($fieldName, 'created_at') == 0
            || strcmp($fieldName, 'updated_at') == 0
            || strpos($fieldName, '_options') !== false
            || strpos($fieldName, '_select2') !== false)
            {
                continue;
            }
            if(is_null($value))
            {
                $value = '';
            }
            $data[$fieldName] = $value;
        }
        return $data;
    }
}