<?php

namespace App\Services\Utils;

use App\Http\Requests\Auth\CustomerRules;
use App\User;
use App\UserProfiles;
use App\UserDivision;
use App\Customer;
use App\CustomerDivision;
use App\CustomerCategory;
use App\CustomerCategory1;
use App\CustomerCategory2;
Use App\Division;
Use App\Area;
Use App\Terms;
Use App\Currency;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\Auth\LoginRules;
use Illuminate\Support\Facades\Auth;
use Exception;

/**
 * Class LoginController
 * @package App\Http\Controllers\Auth
 * @resource App\Http\Controllers\Auth
 */
class ResponseServices
{
    private static $instance = null;
    private $success = null;
    private $type = null;
    private $data = null;
    private $message = null;
    private $title = null;
    private $errorCode = 0;

    private $response = [];

//    public function __call($method, $args)
//    {
//         $instance = self::getInstance();
//         return $instance;
////        if(function_exists($this->$method())){
//////            die();
////        }
//    }

    public static function success($message = null)
    {
        $instance = self::getInstance();

        $instance->success = true;
        $instance->type = 'success';

        if ($message !== null) {
            $instance->message = $message;
        }

        return $instance;
    }

    public static function error($message = null, $errorCode = 0)
    {
        $instance = self::getInstance();

        $instance->success = false;
        $instance->type = 'error';

        $instance->errorCode = $errorCode;
        if ($message !== null) {
            $instance->message = $message;
        }

        return $instance;
    }

    public function data($data, $key = 'data')
    {
        $this->data = $data;

        if ($this->data !== null) {
            $type = [$key => $this->data];
            $this->response = array_merge($this->response, $type);
        }
        return $this;
    }

    public function type($type)
    {
        $this->type = $type;
        if ($this->type !== null) {
            $type = ['type' => $this->type];
            $this->response = array_merge($this->response, $type);
        }
        return $this;
    }

    public function title($title)
    {
        $this->title = $title;
        if ($this->title !== null) {
            $title = ['title' => $this->title];
            $this->response = array_merge($this->response, $title);
        }
        return $this;
    }

    public function toJson()
    {
        $response = $this->prepareResponse();
        return response()->json($response);
    }

    public function toArray()
    {
        $response = $this->prepareResponse();
        return response($response);
    }

    private static function getInstance()
    {
        if (self::$instance == null) {
            return new self;
        } else {
            return self::$instance;
        }
    }

    private function prepareResponse()
    {
        $this->response = array_merge($this->response, [
            'success' => $this->success
        ]);

        if ($this->data !== null) {
            $data = ['data' => $this->data];
            $this->response = array_merge($this->response, $data);
        }

        if ($this->message !== null) {
            $message = ['message' => $this->message];
            $this->response = array_merge($this->response, $message);
        }

        if ($this->errorCode > 0) {
            $errorCode = ['error_code' => $this->errorCode];
            $this->response = array_merge($this->response, $errorCode);
        }

        return $this->response;
    }
}
