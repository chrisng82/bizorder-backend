<?php

namespace App\Services;

use App\DeliveryPoint;
use App\Services\Env\ProcType;
use App\Services\Env\ResStatus;
use App\Imports\DeliveryPointExcel01Import;
use App\Repositories\AreaRepository;
use App\Repositories\SiteFlowRepository;
use App\Repositories\DeliveryPointRepository;
use App\Repositories\BatchJobStatusRepository;
use App\Repositories\SyncSettingHdrRepository;
use App\Repositories\SyncSettingDtlRepository;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;

class DeliveryPointService 
{
    public static $DELIVERY_POINT_EXCEL_01 = array(
        'DEBTOR_CODE' => 'debtor_code', 
        'CODE' => 'code',
        'REF_CODE_01' => 'ref_code_01',
        'NAME_01' => 'company_name_01',
        'NAME_02' => 'company_name_02',
        'STATUS' => 'status',
        'CATEGORY' => 'item_group_01_code',
        'CHAIN' => 'item_group_02_code',
        'CHANNEL' => 'item_group_03_code',        
        'AREA' => 'area_code',
        'UNIT_NO' => 'unit_no',
        'BUILDING_NAME' => 'building_name',
        'STREET_NAME' => 'street_name',
        'DISTRICT_01' => 'district_01',
        'DISTRICT_02' => 'district_02',
        'POSTCODE' => 'postcode',
        'STATE_NAME' => 'state_name',
        'COUNTRY_NAME' => 'country_name',
        'ATTENTION' => 'attention',
        'PHONE_01' => 'phone_01',
        'PHONE_02' => 'phone_02',
        'FAX_01' => 'fax_01',
        'FAX_02' => 'fax_02',
        'EMAIL_01' => 'email_01',
        'EMAIL_02' => 'email_02',
    );

    public function __construct() 
	{
    }
    
    public function indexProcess($strProcType, $sorts, $filters = array(), $pageSize = 20)
    {
        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['DELIVERY_POINT_LIST_01'])
			{
				//delivery point listing
				$deliveryPoints = $this->indexDeliveryPointList01($sorts, $filters, $pageSize);
				return $deliveryPoints;
			}
		}	
    }

    protected function indexDeliveryPointList01($sorts, $filters = array(), $pageSize = 20)
	{
		$deliveryPoints = DeliveryPointRepository::findAll($sorts, $filters, $pageSize);
        
        $deliveryPoints->load('debtor', 'debtorGroup01', 'debtorGroup02', 'debtorGroup03', 'debtorGroup04', 'debtorGroup05');
		foreach($deliveryPoints as $deliveryPoint)
		{
            $deliveryPoint->debtor_code = !empty($deliveryPoint->debtor) ? $deliveryPoint->debtor->code : '';            
            $deliveryPoint->debtor_group_01_code = !empty($deliveryPoint->debtorGroup01) ? $deliveryPoint->debtorGroup01->code : '';            
            $deliveryPoint->debtor_group_02_code = !empty($deliveryPoint->debtorGroup02) ? $deliveryPoint->debtorGroup02->code : '';                 
            $deliveryPoint->debtor_group_03_code = !empty($deliveryPoint->debtorGroup03) ? $deliveryPoint->debtorGroup03->code : '';              
            $deliveryPoint->debtor_group_04_code = !empty($deliveryPoint->debtorGroup04) ? $deliveryPoint->debtorGroup04->code : '';     
            $deliveryPoint->debtor_group_05_code = !empty($deliveryPoint->debtorGroup05) ? $deliveryPoint->debtorGroup05->code : '';
            
            unset($deliveryPoint->debtor);
            unset($deliveryPoint->debtorGroup01);
            unset($deliveryPoint->debtorGroup02);
            unset($deliveryPoint->debtorGroup03);
            unset($deliveryPoint->debtorGroup04);
            unset($deliveryPoint->debtorGroup05);
        }
        return $deliveryPoints;
    }
    
    public function uploadProcess($strProcType, $siteFlowId, $divisionId, $file)
    {
        $user = Auth::user();
        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['DELIVERY_POINT_EXCEL_01']) 
			{
                //process item excel
                $path = Storage::putFileAs('upload/', $file, 'DELIVERY_POINT_EXCEL_01.XLSX');
				return $this->uploadDeliveryPointExcel01($siteFlowId, $divisionId, $user->id, $path);
			}
		}
    }

    public function uploadDeliveryPointExcel01($siteFlowId, $divisionId, $userId, $path)
    {
        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['STORAGE_BIN_EXCEL_01'], $userId);

        $siteFlow = SiteFlowRepository::findByPk($siteFlowId);
    }

    public function syncProcess($strProcType, $siteFlowId, $divisionId)
    {
        $user = Auth::user();

        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['DELIVERY_POINT_SYNC_01']) 
			{
				//sync items from EfiChain
				return $this->syncDeliveryPointSync01($siteFlowId, $user->id);
            }
		}
    }

    protected function syncDeliveryPointSync01($siteFlowId, $userId)
    {
        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['DELIVERY_POINT_SYNC_01'], $userId);

        $syncSettingHdr = SyncSettingHdrRepository::findByProcTypeAndSiteFlowId(ProcType::$MAP['SYNC_EFICHAIN_01'], $siteFlowId);
        if(empty($syncSettingHdr))
        {
            $exc = new ApiException(__('DeliveryPoint.sync_setting_not_found', ['siteFlowId'=>$siteFlowId]));
            //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
            throw $exc;
        }

        $client = new Client();
        $header = array();

        $page = 1;
        $lastPage = 1;
        $pageSize = 1000;
        $total = 0;

        $usernameSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'username');
        $passwordSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'password');
        $formParams = array(
            'UserLogin[username]' => $usernameSetting->value,
            'UserLogin[password]' => $passwordSetting->value
        );
        $pageSizeSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'page_size');
        if(!empty($pageSizeSetting))
        {
            $pageSize = $pageSizeSetting->value;
        }

        while($page <= $lastPage)
        {
            $url = $syncSettingHdr->url.'/index.php?r=luuWu/getCustomers&page_size='.$pageSize.'&page='.$page;
            
            $response = $client->post($url, array('headers' => $header, 'form_params' => $formParams));
            $result = $response->getBody()->getContents();
            $result = json_decode($result, true);
            if(empty($result))
            {
                $exc = new ApiException(__('DeliveryPoint.fail_to_sync', ['url'=>$url]));
                //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
                throw $exc;
            }

            $total = $result['total'];
            for ($a = 0; $a < count($result['data']); $a++)
            {
                $deliveryPointData = $result['data'][$a];
                $deliveryPointModel = DeliveryPointRepository::syncDeliveryPointSync01($deliveryPointData);

                $statusNumber = 100;
                if($total > 0)
                {
                    $statusNumber = bcmul(bcdiv(($a + ($page - 1) * $pageSize), $total, 8), 100, 2);
                }
                BatchJobStatusRepository::updateStatusNumber($batchJobStatusModel->id, $statusNumber);
            }

            $lastPage = $total % $pageSize == 0 ? ($total / $pageSize) : ($total / $pageSize) + 1;

            $page++;
        }

        $batchJobStatusModel = BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

        return $batchJobStatusModel;
    }

    public function select2($search, $filters)
    {
        //DB::connection()->enableQueryLog();
        $deliveryPoints = DeliveryPointRepository::select2($search, $filters);
        //dd(DB::getQueryLog());
        return $deliveryPoints;
    }
    
    public function initModel()
    {
        // AuthService::authorize(
        //     array(
        //         'item_create'
        //     )
        // );

        $model = new DeliveryPoint();
        $model->code = '';
        $model->site_id = 0;
        $model->site_flow_id = 0;
        $model->ref_code_01 = '';
        $model->name_01 = '';
        $model->name_02 = '';

        return $model;
    }
    
    public function createModel($data)
    {		
        // AuthService::authorize(
        //     array(
        //         'item_create'
        //     )
        // );

        $data = self::processIncomingModel($data, true);
        $model = DeliveryPointRepository::createModel($data);

        //create a sync hdr setting for new division

        $message = __('DeliveryPoint.record_successfully_created', ['docCode'=>$model->code]);

		return array(
            'data' => $model->id,
			'message' => $message
		);
    }
    
    public function showModel($itemId)
    {
        // AuthService::authorize(
        //     array(
        //         'item_read',
		// 		'item_update'
        //     )
        // );

        $model = DeliveryPointRepository::findByPk($itemId);
		if(!empty($model))
        {
            $model = self::processOutgoingModel($model);
        }
        
        return $model;
    }

    public function deleteModel($deliveryPointId)
    {
        // AuthService::authorize(
        //     array(
        //         'item_read',
		// 		'item_update'
        //     )
        // );

        $result = DeliveryPointRepository::deleteModel($deliveryPointId);
        $model = $result['model'];
        
		$message = __('DeliveryPoint.record_successfully_updated', ['id'=>$model->id]);
		
		return array(
			'data' => array(
                'timestamp'=>time(),
                'model'=>$model,
            ),
			'message' => $message
		);
    }

    public function updateModel($data)
	{
        // AuthService::authorize(
        //     array(
        //         'item_update'
        //     )
        // );

        $data = self::processIncomingModel($data);

        $item = DeliveryPointRepository::findByPk($data['id']);
        $itemData = $item->toArray();
        //assign data to model
        foreach($data as $field => $value) 
        {
            $itemData[$field] = $value;
        }

        $result = DeliveryPointRepository::updateModel($itemData);
        $model = $result['model'];

        $model = self::processOutgoingModel($model);

        $message = __('DeliveryPoint.record_successfully_updated', ['code'=>$model->code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'model'=>$model
            ),
			'message' => $message
		);
    }

    static public function processIncomingModel($data)
    {
        if(array_key_exists('submit_action', $data))
        {
            unset($data['submit_action']);
        }
        
        //preprocess the select2 and dropDown
        if(array_key_exists('area_id_select2', $data))
        {
            $data['area_id'] = $data['area_id_select2']['value'];
            unset($data['area_id_select2']);
        }
        if(array_key_exists('area', $data))
        {
            unset($data['area']);
        }

        return $data;
    }

    static public function processOutgoingModel($model)
	{
        $area = $model->area;

        //area id select2
        $initAreaOption = array('value'=>0,'label'=>'');
        $area = AreaRepository::findByPk($model->area_id);
        if(!empty($area))
        {
            $initAreaOption = array('value'=>$area->id, 'label'=>$area->code . ' ' . $area->desc_01);
        }
        $model->area_id_select2 = $initAreaOption;

		return $model;
    }
}
