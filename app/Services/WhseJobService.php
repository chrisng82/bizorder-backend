<?php

namespace App\Services;

use App\QuantBal;
use App\WhseJobHdr;
use App\Uom;
use App\Item;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Services\Env\ResType;
use App\Services\Env\WhseJobType;
use App\Services\ItemService;
use App\Repositories\PackListHdrRepository;
use App\Repositories\PackListDtlRepository;
use App\Repositories\PickListHdrRepository;
use App\Repositories\PickListDtlRepository;
use App\Repositories\PickListOutbOrdRepository;
use App\Repositories\DocTxnFlowRepository;
use App\Repositories\QuantBalRsvdTxnRepository;
use App\Repositories\QuantBalRepository;
use App\Repositories\WhseJobHdrRepository;
use App\Repositories\WhseJobDtlRepository;
use App\Repositories\SiteDocNoRepository;
use App\Repositories\WhseTxnFlowRepository;
use App\Repositories\StorageBinRepository;
use App\Repositories\PrintDocSettingRepository;
use App\Repositories\PrintDocTxnRepository;
use App\Repositories\GdsRcptHdrRepository;
use App\Repositories\GdsRcptDtlRepository;
use App\Repositories\CycleCountHdrRepository;
use App\Repositories\CycleCountDtlRepository;
use App\Repositories\PutAwayHdrRepository;
use App\Repositories\PutAwayDtlRepository;
use App\Repositories\GdsRcptItemRepository;
use App\Repositories\WhseJobItemRepository;
use App\Repositories\ItemUomRepository;
use App\Repositories\ItemBatchRepository;
use App\Repositories\GdsRcptInbOrdRepository;
use App\Repositories\PutAwayInbOrdRepository;
use App\Repositories\BinTrfHdrRepository;
use App\Repositories\BinTrfDtlRepository;
use App\Repositories\ItemRepository;
use App\Services\Utils\ApiException;
use App\Services\PickListService;
use App\Services\GdsRcptService;
use App\Services\PutAwayService;
use App\Services\CycleCountService;
use App\Services\BinTrfService;
use App\Services\PackListService;
use App\Services\LoadListService;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class WhseJobService extends InventoryService
{
	/**
	 */
	public function __construct() 
	{
	}

	public function indexProcess($strProcType, $siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
		$user = Auth::user();

		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_03_01']) 
			{
				//pick list -> WHSE_JOB
				$pickListHdrs = $this->indexWhseJob0301($user, $siteFlowId, $sorts, $filters, $pageSize);
				return $pickListHdrs;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_03_01_01'])
			{
				//whse job
				$whseJobHdrs = $this->indexWhseJob030101($siteFlowId, $sorts, $filters, $pageSize);
				return $whseJobHdrs;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_03_01_02'])
			{
				//whse job
				$whseJobHdrs = $this->indexWhseJob030102($user, $siteFlowId, $sorts, $filters, $pageSize);
				return $whseJobHdrs;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_05_01'])
			{
				//pack list -> WHSE_JOB
				$packListHdrs = $this->indexWhseJob0501($siteFlowId, $sorts, $filters, $pageSize);
				return $packListHdrs;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_14_01'])
			{
				//gds rcpt -> WHSE_JOB
				$gdsRcptHdrs = $this->indexWhseJob1401($siteFlowId, $sorts, $filters, $pageSize);
				return $gdsRcptHdrs;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_14_01_01'])
			{
				//gds rcpt whse job
				$whseJobHdrs = $this->indexWhseJob140101($siteFlowId, $sorts, $filters, $pageSize);
				return $whseJobHdrs;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_15_01'])
			{
				//put away -> WHSE_JOB
				$putAwayHdrs = $this->indexWhseJob1501($siteFlowId, $sorts, $filters, $pageSize);
				return $putAwayHdrs;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_15_01_01'])
			{
				//put away whse job
				$whseJobHdrs = $this->indexWhseJob150101($siteFlowId, $sorts, $filters, $pageSize);
				return $whseJobHdrs;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_16_01'])
			{
				//cycle count -> WHSE_JOB
				$cycleCountHdrs = $this->indexWhseJob1601($siteFlowId, $sorts, $filters, $pageSize);
				return $cycleCountHdrs;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_17_02'])
			{
				//bin transfer -> WHSE_JOB
				$cycleCountHdrs = $this->indexWhseJob1702($siteFlowId, $sorts, $filters, $pageSize);
				return $cycleCountHdrs;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_16_01_01'])
			{
				//gds rcpt whse job
				$whseJobHdrs = $this->indexWhseJob160101($siteFlowId, $sorts, $filters, $pageSize);
				return $whseJobHdrs;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_16_01_02'])
			{
				//whse job
				$whseJobHdrs = $this->indexWhseJob160102($user, $siteFlowId, $sorts, $filters, $pageSize);
				return $whseJobHdrs;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_17_02_01'])
			{
				//gds rcpt whse job
				$whseJobHdrs = $this->indexWhseJob170201($siteFlowId, $sorts, $filters, $pageSize);
				return $whseJobHdrs;
			}
		}
	}

	protected function indexWhseJob0301($user, $siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
            array(
                'pick_list_read'
            )
		);
		
		$divisionIds = array();
		$userDivisions = $user->userDivisions;
		foreach($userDivisions as $userDivision)
		{
			$divisionIds[] = $userDivision->division_id;
		}

		//DB::connection()->enableQueryLog();
		$pickListHdrs = PickListHdrRepository::findAllNotExistWhseJob0301Txn($divisionIds, $sorts, $filters, $pageSize);
		$pickListHdrs->load(
			'pickListDtls',
			'pickListDtls.item', 'pickListDtls.quantBal', 
			'pickListDtls.quantBal.itemBatch', 'pickListDtls.quantBal.storageBin', 
			'pickListDtls.quantBal.storageRow', 'pickListDtls.quantBal.storageBay'
		);
		//Log::error(DB::getQueryLog());
		foreach($pickListHdrs as $pickListHdr)
		{
			$pickListHdr->str_doc_status = DocStatus::$MAP[$pickListHdr->doc_status];

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$pickListDtls = $pickListHdr->pickListDtls;

			$jobPickListDtls = array();
			foreach($pickListDtls as $pickListDtl)
			{
				$item = $pickListDtl->item;
				$quantBal = $pickListDtl->quantBal;
				$itemBatch = null;
				$storageBin = null;
				$storageRow = null;
				$storageBay = null;
				if(!empty($quantBal))
				{
					$itemBatch = $quantBal->itemBatch;
					$storageBin = $quantBal->storageBin;
					$storageRow = $quantBal->storageRow;
					$storageBay = $quantBal->storageBay;
				}

				$whse_job_key = $pickListDtl->whse_job_type.':'.$pickListDtl->whse_job_code;
				$pickListDtl->whse_job_key = $whse_job_key;
				$pickListDtl->str_whse_job_type = WhseJobType::$MAP[$pickListDtl->whse_job_type];
				$pickListDtl->str_whse_job_type = __('WhseJob.'.strtolower($pickListDtl->str_whse_job_type));

				$pickListDtl->batch_serial_no = '';
				$pickListDtl->expiry_date = '';
				$pickListDtl->receipt_date = '';
				if(!empty($itemBatch))
				{
					$pickListDtl->batch_serial_no = $itemBatch->batch_serial_no;
					$pickListDtl->expiry_date = $itemBatch->expiry_date;
					$pickListDtl->receipt_date = $itemBatch->receipt_date;
				}

				$pickListDtl->storage_bin_code = '';
				if(!empty($storageBin))
				{
					$pickListDtl->storage_bin_code = $storageBin->code;
				}
				$pickListDtl->storage_row_code = '';
				if(!empty($storageRow))
				{
					$pickListDtl->storage_row_code = $storageRow->code;
				}
				$pickListDtl->storage_bay_code = '';
				$pickListDtl->item_bay_sequence = 0;
				if(!empty($storageBay))
				{
					$pickListDtl->storage_bay_code = $storageBay->code;
					$pickListDtl->item_bay_sequence = $storageBay->bay_sequence;
				}

				//calculate the pallet qty, case qty, gross weight, and m3
				if($pickListDtl->uom_id == Uom::$PALLET
        		&& bccomp($pickListDtl->uom_rate, 0, 5) == 0)
				{
					//this is pallet picking
					$pickListDtl->ttl_unit_qty = 0;
					$pickListDtl->case_qty = 0;
					$pickListDtl->gross_weight = 0;
					$pickListDtl->cubic_meter = 0;

					$handlingQuantBals = QuantBalRepository::findAllByHandlingUnitId($quantBal->handling_unit_id);
					$handlingQuantBals->load('item');
					foreach($handlingQuantBals as $handlingQuantBal)
					{
						$handlingItem = $handlingQuantBal->item;

						$handlingQuantBal = ItemService::processCaseLoose($handlingQuantBal, $handlingItem);

						$pickListDtl->unit_qty = bcadd($pickListDtl->unit_qty, $handlingQuantBal->balance_unit_qty, 10);
						$pickListDtl->case_qty = bcadd($pickListDtl->case_qty, $handlingQuantBal->case_qty, 10);
						$pickListDtl->gross_weight = bcadd($pickListDtl->gross_weight, $handlingQuantBal->gross_weight, 10);
						$pickListDtl->cubic_meter = bcadd($pickListDtl->cubic_meter, $handlingQuantBal->cubic_meter, 10);
					}

					$caseQty = bcadd($caseQty, $pickListDtl->case_qty, 8);
					$grossWeight = bcadd($grossWeight, $pickListDtl->gross_weight, 8);
					$cubicMeter = bcadd($cubicMeter, $pickListDtl->cubic_meter, 8);
				}
				else
				{
					$pickListDtl = ItemService::processCaseLoose($pickListDtl, $item);

					$caseQty = bcadd($caseQty, $pickListDtl->case_qty, 8);
					$grossWeight = bcadd($grossWeight, $pickListDtl->gross_weight, 8);
					$cubicMeter = bcadd($cubicMeter, $pickListDtl->cubic_meter, 8);
				}

				if($pickListDtl->handling_unit_id > 0)
				{
					$palletHashByHandingUnitId[$pickListDtl->handling_unit_id] = $pickListDtl->handling_unit_id;
				}

				unset($pickListDtl->quantBal);
				unset($pickListDtl->item);
				$jobPickListDtls[] = $pickListDtl;
			}

			$pickListHdr->pallet_qty = count($palletHashByHandingUnitId);
			$pickListHdr->case_qty = $caseQty;
			$pickListHdr->gross_weight = $grossWeight;
			$pickListHdr->cubic_meter = $cubicMeter;

			$pickListHdr->details = $jobPickListDtls;
		}
    	return $pickListHdrs;
	}

	static public function processOutgoingHeader($model, $isShowPrint = false)
	{
		$docFlows = self::processDocFlows($model);
		$model->doc_flows = $docFlows;
		
		if($isShowPrint)
		{
			$printDocTxn = PrintDocTxnRepository::queryPrintDocTxn(WhseJobHdr::class, $model->id);
			$model->print_count = $printDocTxn->print_count;
			$model->first_printed_at = $printDocTxn->first_printed_at;
			$model->last_printed_at = $printDocTxn->last_printed_at;
		}

		$model->str_doc_status = DocStatus::$MAP[$model->doc_status];

		//worker01 select2
        $worker01 = $model->worker01;
        $initWorker01Option = array(
            'value'=> empty($worker01) ? 0 : $worker01->id,
            'label'=> empty($worker01) ? '' : $worker01->username
        );
		$model->worker_01_select2 = $initWorker01Option;
		
		return $model;
	}

	protected function indexWhseJob030101($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
            array(
                'whse_job_read'
            )
		);

		$whseJobHdrs = WhseJobHdrRepository::findAllDraftAndWIP($siteFlowId, ProcType::$MAP['WHSE_JOB_03_01'], $sorts, $filters, $pageSize);
		$whseJobHdrs->load(
			'frDocTxnFlows'
		);
		foreach($whseJobHdrs as $whseJobHdr)
		{
			$whseJobHdr = self::processOutgoingHeader($whseJobHdr, true);
			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$whseJobDtls = WhseJobDtlRepository::findAllByHdrId($whseJobHdr->id);
			$whseJobDtls->load('item', 'storageBin', 'quantBal', 'quantBal.itemBatch', 'quantBal.storageBin', 'quantBal.storageRow', 'quantBal.storageBay');

			$formatWhseJobDtls = array();
			foreach($whseJobDtls as $whseJobDtl)
			{
				$whseJobDtl = PickListService::processOutgoingDetail($whseJobDtl, true);

				$caseQty = bcadd($caseQty, $whseJobDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $whseJobDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $whseJobDtl->cubic_meter, 8);

				if($whseJobDtl->handling_unit_id > 0)
				{
					$palletHashByHandingUnitId[$whseJobDtl->handling_unit_id] = $whseJobDtl->handling_unit_id;
				}
				unset($whseJobDtl->quantBal);
				unset($whseJobDtl->storageBin);
				unset($whseJobDtl->item);
				$formatWhseJobDtls[] = $whseJobDtl;
			}

			$whseJobHdr->pallet_qty = count($palletHashByHandingUnitId);
			$whseJobHdr->case_qty = $caseQty;
			$whseJobHdr->gross_weight = $grossWeight;
			$whseJobHdr->cubic_meter = $cubicMeter;

			$frDocHdrCodes = array();
			foreach($whseJobHdr->frDocTxnFlows as $frDocTxnFlow)
			{
				$frDocHdrCodes[] = $frDocTxnFlow->fr_doc_hdr_code;
			}

			$whseJobHdr->details = $formatWhseJobDtls;
			$whseJobHdr->fr_doc_hdr_codes = $frDocHdrCodes;
			unset($whseJobHdr->frDocTxnFlows);
		}
    	return $whseJobHdrs;
	}

	protected function indexWhseJob140101($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
            array(
                'whse_job_read'
            )
		);

		$whseJobHdrs = WhseJobHdrRepository::findAllDraftAndWIP($siteFlowId, ProcType::$MAP['WHSE_JOB_14_01'], $sorts, $filters, $pageSize);
		$whseJobHdrs->load('frDocTxnFlows');
		foreach($whseJobHdrs as $whseJobHdr)
		{
			$whseJobHdr = self::processOutgoingHeader($whseJobHdr, true);

			//query the inbOrds
			$frDocTxnFlows = $whseJobHdr->frDocTxnFlows;
			$gdsRcptHdrIds = array();
			foreach($frDocTxnFlows as $frDocTxnFlow)
			{
				$gdsRcptHdrIds[] = $frDocTxnFlow->fr_doc_hdr_id;
			}
			$gdsRcptInbOrds = GdsRcptInbOrdRepository::findAllByHdrIds($gdsRcptHdrIds);
			$gdsRcptInbOrds->load('inbOrdHdr');
			for($a = 0; $a < count($gdsRcptInbOrds); $a++)
			{
				$gdsRcptInbOrd = $gdsRcptInbOrds[$a];
				$inbOrdHdr = $gdsRcptInbOrd->inbOrdHdr;
				if($a == 0)
				{
					$whseJobHdr->adv_ship_hdr_code = $inbOrdHdr->adv_ship_hdr_code;
					$whseJobHdr->ref_code_01 = $inbOrdHdr->ref_code_01;
					$whseJobHdr->ref_code_02 = $inbOrdHdr->ref_code_02;
					$whseJobHdr->ref_code_03 = $inbOrdHdr->ref_code_03;
					$whseJobHdr->ref_code_04 = $inbOrdHdr->ref_code_04;
					$whseJobHdr->ref_code_05 = $inbOrdHdr->ref_code_05;
					$whseJobHdr->ref_code_06 = $inbOrdHdr->ref_code_06;
					$whseJobHdr->ref_code_07 = $inbOrdHdr->ref_code_07;
					$whseJobHdr->ref_code_08 = $inbOrdHdr->ref_code_08;
				}
				else
				{
					$whseJobHdr->adv_ship_hdr_code .= '/'.$inbOrdHdr->adv_ship_hdr_code;
					$whseJobHdr->ref_code_01 .= '/'.$inbOrdHdr->ref_code_01;
					$whseJobHdr->ref_code_02 .= '/'.$inbOrdHdr->ref_code_02;
					$whseJobHdr->ref_code_03 .= '/'.$inbOrdHdr->ref_code_03;
					$whseJobHdr->ref_code_04 .= '/'.$inbOrdHdr->ref_code_04;
					$whseJobHdr->ref_code_05 .= '/'.$inbOrdHdr->ref_code_05;
					$whseJobHdr->ref_code_06 .= '/'.$inbOrdHdr->ref_code_06;
					$whseJobHdr->ref_code_07 .= '/'.$inbOrdHdr->ref_code_07;
					$whseJobHdr->ref_code_08 .= '/'.$inbOrdHdr->ref_code_08;
				}
			}

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$whseJobDtls = WhseJobDtlRepository::findAllByHdrId($whseJobHdr->id);
			$whseJobDtls->load('item', 'storageBin', 'quantBal', 'quantBal.itemBatch', 'quantBal.storageBin', 'quantBal.storageRow', 'quantBal.storageBay');

			$formatWhseJobDtls = array();
			foreach($whseJobDtls as $whseJobDtl)
			{
				$whseJobDtl = GdsRcptService::processOutgoingDetail($whseJobDtl, false);

				$caseQty = bcadd($caseQty, $whseJobDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $whseJobDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $whseJobDtl->cubic_meter, 8);

				if($whseJobDtl->to_handling_unit_id > 0)
				{
					$palletHashByHandingUnitId[$whseJobDtl->to_handling_unit_id] = $whseJobDtl->to_handling_unit_id;
				}
				unset($whseJobDtl->quantBal);
				unset($whseJobDtl->storageBin);
				unset($whseJobDtl->item);
				$formatWhseJobDtls[] = $whseJobDtl;
			}

			$whseJobHdr->pallet_qty = count($palletHashByHandingUnitId);
			$whseJobHdr->case_qty = $caseQty;
			$whseJobHdr->gross_weight = $grossWeight;
			$whseJobHdr->cubic_meter = $cubicMeter;

			$whseJobHdr->details = $formatWhseJobDtls;
		}
    	return $whseJobHdrs;
	}

	protected function indexWhseJob0501($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
            array(
                'pack_list_read'
            )
		);

		$packListHdrs = PackListHdrRepository::findAllNotExistWhseJob0501Txn($siteFlowId, DocStatus::$MAP['WIP'], $sorts, $filters, $pageSize);
		//query the docDtls, and format into group details
		foreach($packListHdrs as $packListHdr)
		{
			$packListDtls = PackListDtlRepository::queryShipmentDetails($packListHdr->id);
			//calculate the case_qty, gross_weight and cubic_meter
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			foreach($packListDtls as $packListDtl)
			{
				$caseQty = bcadd($caseQty, $packListDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $packListDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $packListDtl->cubic_meter, 8);
			}

			$packListHdr->case_qty = $caseQty;
			$packListHdr->gross_weight = $grossWeight;
			$packListHdr->cubic_meter = $cubicMeter;

			$packListHdr->details = $packListDtls;
		}
		
		return $packListHdrs;
	}

	public function createProcess($strProcType, $hdrIds) 
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_03_01'])
			{
				//pick list -> WHSE_JOB				
				PickListService::checkReservation($strProcType, $hdrIds);
				$result = $this->createWhseJob0301(ProcType::$MAP[$strProcType], $hdrIds);
				return $result;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_05_01'])
			{
				//pack list -> WHSE_JOB
				$result = $this->createWhseJob0501(ProcType::$MAP[$strProcType], $hdrIds);
				return $result;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_14_01'])
			{
				//gds rcpt -> WHSE_JOB
				$result = $this->createWhseJob1401(ProcType::$MAP[$strProcType], $hdrIds);
				return $result;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_15_01'])
			{
				//put away -> WHSE_JOB
				$result = $this->createWhseJob1501(ProcType::$MAP[$strProcType], $hdrIds);
				return $result;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_16_01'])
			{
				//cycle count -> WHSE_JOB
				$result = $this->createWhseJob1601(ProcType::$MAP[$strProcType], $hdrIds);
				return $result;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_17_02'])
			{
				//bin trf -> WHSE_JOB
				$result = $this->createWhseJob1702(ProcType::$MAP[$strProcType], $hdrIds);
				return $result;
			}
		}
	}

	protected function convertWhseJobTypeToPickJobGroup($whseJobType)
	{
		$pickJobGroup = 'A';
		if($whseJobType == WhseJobType::$MAP['PICK_FULL_PALLET'])
		{
			$pickJobGroup = 'A';
		}
		elseif($whseJobType == WhseJobType::$MAP['PICK_BROKEN_PALLET'])
		{
			$pickJobGroup = 'B';
		}
		elseif($whseJobType == WhseJobType::$MAP['PICK_FACE_REPLENISHMENT'])
		{
			$pickJobGroup = 'C';
		}
		elseif($whseJobType == WhseJobType::$MAP['WAITING_REPLENISHMENT']
		|| $whseJobType == WhseJobType::$MAP['PICK_FULL_CASE']
		|| $whseJobType == WhseJobType::$MAP['PICK_BROKEN_CASE'])
		{
			$pickJobGroup = 'D';
		}
		elseif($whseJobType == WhseJobType::$MAP['PICK_NOT_FULFILLED'])
		{
			$pickJobGroup = 'E';
		}
		return $pickJobGroup;
	}

	protected function createWhseJob0301($procType, $hdrIds)
  	{		
		AuthService::authorize(
            array(
                'whse_job_create'
            )
		);

	  	//1 picklist to 1 whseJob
		$whseJobHdrDataList = array();
		$siteFlow = null;

		$pickListHdrs = PickListHdrRepository::findAllByHdrIds($hdrIds);
		$pickListHdrs->load('siteFlow');
		foreach($pickListHdrs as $pickListHdr) 
		{
			//check the frDocHdr to make sure it is not closed
			$tmpDocTxnFlow = DocTxnFlowRepository::findByProcTypeAndFrHdrId($procType, \App\PickListHdr::class, $pickListHdr->id, 1);
			if(!empty($tmpDocTxnFlow))
			{
				$exc = new ApiException(__('WhseJob.fr_doc_is_closed', ['docType'=>$tmpDocTxnFlow->fr_doc_hdr_type, 'docCode'=>$tmpDocTxnFlow->fr_doc_hdr_code]));
				$exc->addData($tmpDocTxnFlow->fr_doc_hdr_type, $tmpDocTxnFlow->fr_doc_hdr_code);
				throw $exc;
			}

			$siteFlow = $pickListHdr->siteFlow;

			//format whseJobHdr into hashtable by quantBalId
			$pickFaceJobHdrHashByQuantBalId = array();
			//format pick face pickListDtls into array
			$pickFaceReplnDtls = array();
			//format other (non pick face replenishment) pickListDtls into hashtable by hdrId, whseJobType, whseJobCode
			$pickListDtlsHashByKey1 = array();

			$pickListDtls = PickListDtlRepository::findAllByHdrId($pickListHdr->id);
			$pickListDtls->load('item', 'quantBal');
			foreach($pickListDtls as $pickListDtl) 
			{
				if($pickListDtl->whse_job_type == WhseJobType::$MAP['PICK_NOT_FULFILLED'])
				{
					$exc = new ApiException(__('WhseJob.document_out_of_stock', ['docCode'=>$pickListHdr->doc_code]));
					$exc->addData(\App\PickListHdr::class, $pickListHdr->doc_code);
					throw $exc;
				}

				//separate pick face replenishment from other pickListDtls
				if($pickListDtl->whse_job_type == WhseJobType::$MAP['PICK_FACE_REPLENISHMENT'])
				{
					$pickFaceReplnDtls[] = $pickListDtl;
				}
				else
				{
					$pickJobGroup = $this->convertWhseJobTypeToPickJobGroup($pickListDtl->whse_job_type);
					//format pickListDtls into hashtable by hdrId, whseJobType, whseJobCode
					$key1 = $pickListDtl->hdr_id.'/'.$pickJobGroup.'/'.$pickListDtl->whse_job_code;
					$tmpPickListDtls = array();
					if(array_key_exists($key1, $pickListDtlsHashByKey1))
					{
						$tmpPickListDtls = $pickListDtlsHashByKey1[$key1];
					}
					$tmpPickListDtls[] = $pickListDtl;
					$pickListDtlsHashByKey1[$key1] = $tmpPickListDtls;
				}			
			}

			$siteDocNo = SiteDocNoRepository::findSiteDocNo($siteFlow->site_id, \App\WhseJobHdr::class);
			if(empty($siteDocNo))
			{
				$exc = new ApiException(__('SiteDocNo.doc_no_not_found', ['siteId'=>$siteFlow->site_id, 'docType'=>\App\WhseJobHdr::class]));
				//$exc->addData(\App\SiteDocNo::class, $siteFlow->site_id);
				throw $exc;
			}

			$whseTxnFlow = WhseTxnFlowRepository::findByPk($siteFlow->id, $procType);

			//START: create pick face warehouse job
			//process the pick face details, check if the detail already have other open job,
			//if do not have, all will be create into a new whseJobHdr
			if(count($pickFaceReplnDtls) > 0)
			{
				$lineNo = 1;
				$dtlDataList = array();
				$docTxnFlowDataList = array();
				foreach($pickFaceReplnDtls as $pickListDtl)
				{
					//pick face replenishment, need to check if this quantBal already have other job, if got, then just skip
					//check is there any opened or recent pick face replenishment job
					$tmpWhseJobHdr = WhseJobHdrRepository::queryRecentPickFaceJob($pickListDtl->quant_bal_id);
					if(!empty($tmpWhseJobHdr))
					{
						//this pick face replenishment already exist in another job
						//skip this pickListDtl 
						//just need to link the waiting_replenishment to this whse_job_hdr_id
						$pickFaceJobHdrHashByQuantBalId[$pickListDtl->quant_bal_id] = $tmpWhseJobHdr;
					}
					else
					{
						$quantBal = $pickListDtl->quantBal;
						
						$dtlData = array(
							'is_split' => $pickListDtl->is_split,
							'whse_job_type' => $pickListDtl->whse_job_type,
							'req_whse_job_hdr_id' => 0,
							'company_id' => $pickListDtl->company_id,
							'doc_hdr_type' => \App\PickListHdr::class,
							'doc_hdr_id' => $pickListDtl->hdr_id,
							'doc_dtl_type' => \App\PickListDtl::class,
							'doc_dtl_id' => $pickListDtl->id,
							'line_no' => $lineNo,
							'desc_01' => $pickListDtl->desc_01,
							'desc_02' => $pickListDtl->desc_02,

							'storage_bin_id' => $pickListDtl->storage_bin_id,
							'handling_unit_id' => empty($quantBal)? 0 : $quantBal->handling_unit_id,
							'quant_bal_id' => empty($quantBal)? 0 : $quantBal->id,

							'item_id' => $pickListDtl->item_id,
							'uom_id' => $pickListDtl->uom_id,
							'uom_rate' => $pickListDtl->uom_rate,
							'qty' => $pickListDtl->qty,
							'doc_status' => DocStatus::$MAP['DRAFT'],

							'scan_mode' => 0,
							'to_storage_bin_id' => $pickListDtl->to_storage_bin_id,
							'to_handling_unit_id' => $pickListDtl->to_handling_unit_id,
						);
						$dtlDataList[] = $dtlData;

						$lineNo++;
					}
				}
					
				//build the hdrData
				$hdrData = array(
					'ref_code_01' => '',
					'ref_code_02' => '',
					'doc_date' => date('Y-m-d'),
					'desc_01' => '',
					'desc_02' => '',
					'site_flow_id' => $siteFlow->id,
					'mobile_profile_id' => 0,
					'worker_01_id' => 0,
					'worker_02_id' => 0,
					'worker_03_id' => 0,
					'worker_04_id' => 0,
					'worker_05_id' => 0,
					'equipment_01_id' => 0,
					'equipment_02_id' => 0,
					'equipment_03_id' => 0,
					'equipment_04_id' => 0,
					'equipment_05_id' => 0
				);
				
				$tmpDocTxnFlowData = array(
					'fr_doc_hdr_type' => \App\PickListHdr::class,
					'fr_doc_hdr_id' => $pickListHdr->id,
					'fr_doc_hdr_code' => $pickListHdr->doc_code,
					'is_closed' => 0
				);
				$docTxnFlowDataList[] = $tmpDocTxnFlowData;

				//DRAFT
				$whseJobHdr = WhseJobHdrRepository::createProcess($procType, $siteDocNo->doc_no_id, $hdrData, $dtlDataList, $docTxnFlowDataList);
				
				if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
				{
					$whseJobHdr = self::transitionToWip($whseJobHdr->id);
				}
				elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
				{
					$whseJobHdr = self::transitionToComplete($whseJobHdr->id);
				}

				$whseJobHdr->str_doc_status = DocStatus::$MAP[$whseJobHdr->doc_status];
				$whseJobHdrDataList[] = $whseJobHdr;

				//update to pickFaceJob hashtable
				foreach($dtlDataList as $dtlData)
				{
					$pickFaceJobHdrHashByQuantBalId[$dtlData['quant_bal_id']] = $whseJobHdr;
				}
			}
			//END: create pick face warehouse job

			//loop the pickListDtlsHashByKey1 to find the last key1 for hdrId
			$ttlKey1 = count($pickListDtlsHashByKey1);

			//START: create other warehouse jobs
			//PICK_FULL_PALLET, PICK_BROKEN_PALLET, WAITING_REPLENISHMENT, PICK_FULL_CASE, PICK_BROKEN_CASE, PICK_NOT_FULFILLED
			$indexKey1 = 0;
			foreach($pickListDtlsHashByKey1 as $key1 => $tmpPickListDtls)
			{
				$keyParts = explode('/',$key1);
				$hdrId = $keyParts[0];
				$pickJobGroup = $keyParts[1];
				$whseJobCode = $keyParts[2];

				$lineNo = 1;
				$pickListHdr = null;
				$dtlDataList = array();
				$docTxnFlowDataList = array();
				foreach($tmpPickListDtls as $pickListDtl)
				{
					$pickListHdr = $pickListDtl->pickListHdr;
					$quantBal = $pickListDtl->quantBal;
					
					$reqWhseJobHdrId = 0;
					if(!empty($quantBal))
					{
						if(array_key_exists($quantBal->id, $pickFaceJobHdrHashByQuantBalId))
						{
							$pickFaceJobHdr = $pickFaceJobHdrHashByQuantBalId[$quantBal->id];
							$reqWhseJobHdrId = $pickFaceJobHdr->id;
						}
					}

					$dtlData = array(
						'is_split' => $pickListDtl->is_split,
						'whse_job_type' => $pickListDtl->whse_job_type,
						'req_whse_job_hdr_id' => $reqWhseJobHdrId,
						'company_id' => $pickListDtl->company_id,
						'doc_hdr_type' => \App\PickListHdr::class,
						'doc_hdr_id' => $pickListDtl->hdr_id,
						'doc_dtl_type' => \App\PickListDtl::class,
						'doc_dtl_id' => $pickListDtl->id,
						'line_no' => $lineNo,

						'storage_bin_id' => $pickListDtl->storage_bin_id,
						'handling_unit_id' => empty($quantBal) ? 0 : $quantBal->handling_unit_id,
						'quant_bal_id' => empty($quantBal) ? 0 : $quantBal->id,

						'item_id' => $pickListDtl->item_id,
						'uom_id' => $pickListDtl->uom_id,
						'uom_rate' => $pickListDtl->uom_rate,
						'qty' => $pickListDtl->qty,
						'doc_status' => DocStatus::$MAP['DRAFT'],

						'scan_mode' => 0,
						'to_storage_bin_id' => $pickListDtl->to_storage_bin_id,
						'to_handling_unit_id' => $pickListDtl->to_handling_unit_id,
					);
					$dtlDataList[] = $dtlData;

					$lineNo++;
				}

				//build the hdrData
				$hdrData = array(
					'ref_code_01' => '',
					'ref_code_02' => '',
					'doc_date' => date('Y-m-d'),
					'desc_01' => '',
					'desc_02' => '',
					'site_flow_id' => $siteFlow->id,
					'mobile_profile_id' => 0,
					'worker_01_id' => 0,
					'worker_02_id' => 0,
					'worker_03_id' => 0,
					'worker_04_id' => 0,
					'worker_05_id' => 0,
					'equipment_01_id' => 0,
					'equipment_02_id' => 0,
					'equipment_03_id' => 0,
					'equipment_04_id' => 0,
					'equipment_05_id' => 0
				);

				$isClosed = 0;
				if($indexKey1 == ($ttlKey1 - 1))
				{
					$isClosed = 1;
				}
				$tmpDocTxnFlowData = array(
					'fr_doc_hdr_type' => \App\PickListHdr::class,
					'fr_doc_hdr_id' => $pickListHdr->id,
					'fr_doc_hdr_code' => $pickListHdr->doc_code,
					'is_closed' => $isClosed
				);
				$docTxnFlowDataList[] = $tmpDocTxnFlowData;

				//DRAFT
				$whseJobHdr = WhseJobHdrRepository::createProcess($procType, $siteDocNo->doc_no_id, $hdrData, $dtlDataList, $docTxnFlowDataList);

				if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
				{
					$whseJobHdr = self::transitionToWip($whseJobHdr->id);
				}
				elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
				{
					$whseJobHdr = self::transitionToComplete($whseJobHdr->id);
				}

				$whseJobHdr->str_doc_status = DocStatus::$MAP[$whseJobHdr->doc_status];
				$whseJobHdrDataList[] = $whseJobHdr;

				$indexKey1++;
			}
			//END: create other warehouse jobs
		}

		$docCodeMsg = '';
		for ($a = 0; $a < count($whseJobHdrDataList); $a++)
		{
			$whseJobHdrData = $whseJobHdrDataList[$a];
			if($a == 0)
			{
				$docCodeMsg .= $whseJobHdrData->doc_code;
			}
			else
			{
				$docCodeMsg .= ', '.$whseJobHdrData->doc_code;
			}
		}
		$message = __('WhseJob.document_successfully_created', ['docCode'=>$docCodeMsg]);

		return array(
			'data' => $whseJobHdrDataList,
			'message' => $message
		);
	}

	static public function transitionToWip($hdrId, $mobileProfileId = 0, 
		$worker01Id = 0, $worker02Id = 0, $worker03Id = 0, $worker04Id = 0, $worker05Id = 0,
		$equipment01Id = 0, $equipment02Id = 0, $equipment03Id = 0, $equipment04Id = 0, $equipment05Id = 0)
  	{
		//use transaction to make sure this is latest value
		$hdrModel = WhseJobHdrRepository::txnFindByPk($hdrId);
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status >= DocStatus::$MAP['WIP'])
		{
			$exc = new ApiException(__('WhseJob.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
			$exc->addData(\App\WhseJobHdr::class, $hdrModel->id);
			throw $exc;
		}

		//commit the document
		$hdrModel = WhseJobHdrRepository::commitToWip($hdrModel->id, $mobileProfileId, 
			$worker01Id, $worker02Id, $worker03Id, $worker04Id, $worker05Id,
			$equipment01Id, $equipment02Id, $equipment03Id, $equipment04Id, $equipment05Id);
		if(!empty($hdrModel))
		{
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

		$exc = new ApiException(__('WhseJob.commit_to_wip_error', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\WhseJobHdr::class, $hdrModel->id);
		throw $exc;
	}

	static public function transitionToComplete($hdrId, $isForced = true)
  	{
		AuthService::authorize(
            array(
                'whse_job_confirm'
            )
		);

		//use transaction to make sure this is latest value
		$hdrModel = WhseJobHdrRepository::txnFindByPk($hdrId);

		//only DRAFT or above can transition to COMPLETE
		if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE']
		|| $hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('WhseJob.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
			$exc->addData(\App\WhseJobHdr::class, $hdrModel->id);
			throw $exc;
		}

		//find the frDocDtls of this whseJobHdr
		//summarise whseJobDtls into frDocHdrType and frDocHdrId
		$frDocHdrHash = array();
		$dtlModels = WhseJobDtlRepository::findAllByHdrId($hdrModel->id);
		foreach($dtlModels as $dtlModel)
		{
			$key = $dtlModel->doc_hdr_type.'/'.$dtlModel->doc_hdr_id;
			$frDocHdrHash[$key] = $key;

			if($dtlModel->whse_job_type == WhseJobType::$MAP['GOODS_RECEIPT_PALLET'])
			{
				//validate the toHandlingUnitId > 100 if whseJobType is GOODS_RECEIPT_PALLET
				if($dtlModel->to_handling_unit_id <= 100)
				{
					$exc = new ApiException(__('WhseJob.to_handling_unit_is_empty', ['docCode'=>$hdrModel->doc_code, 'lineNo'=>$dtlModel->line_no]));
					$exc->addData(\App\WhseJobHdr::class, $hdrModel->id);
					throw $exc;
				}
			}
		}

		//commit the document
		$hdrModel = WhseJobHdrRepository::commitToComplete($hdrModel->id, $isForced);
		if(!empty($hdrModel))
		{
			//commit the from doc if all from doc details are COMPLETE
			foreach($frDocHdrHash as $key => $value)
			{
				$keyParts = explode('/',$key);
				$frDocHdrType = $keyParts[0];
				$frDocHdrId = $keyParts[1];

				$isComplete = WhseJobHdrRepository::isAllWhseJobsComplete($frDocHdrType, $frDocHdrId);
				if($isComplete === true)
				{
					try
        			{
						$frDocHdr = WhseJobHdrRepository::commitFrDocToComplete($frDocHdrType, $frDocHdrId);
					}
					catch(ApiException $exp)
					{
						//if fail to complete the frDocHdr, then set the whseJob to draft
						$hdrModel = WhseJobHdrRepository::revertCompleteToDraft($hdrModel->id, array());

						throw $exp;
					}					
				}
			}
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		
		$exc = new ApiException(__('WhseJob.commit_to_complete_error', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\WhseJobHdr::class, $hdrModel->id);
		throw $exc;
	}

	static public function transitionToDraft($hdrId, $isForced = true)
  	{
		//use transaction to make sure this is latest value
		$hdrModel = WhseJobHdrRepository::txnFindByPk($hdrId);

		//only WIP or COMPLETE can transition to DRAFT
		if($hdrModel->doc_status == DocStatus::$MAP['WIP'])
		{
			$hdrModel = WhseJobHdrRepository::revertWipToDraft($hdrModel->id, $isForced);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		elseif($hdrModel->doc_status == DocStatus::$MAP['COMPLETE'])
		{
			AuthService::authorize(
				array(
					'whse_job_revert'
				)
			);

			$frDocHdrHash = array();
			$dtlModels = WhseJobDtlRepository::findAllByHdrId($hdrModel->id);
			foreach($dtlModels as $dtlModel)
			{
				$key = $dtlModel->doc_hdr_type.'/'.$dtlModel->doc_hdr_id;
				$frDocHdrHash[$key] = $key;
			}
			$hdrModel = WhseJobHdrRepository::revertCompleteToDraft($hdrModel->id, $frDocHdrHash);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		elseif($hdrModel->doc_status == DocStatus::$MAP['VOID'])
		{
			AuthService::authorize(
				array(
					'whse_job_confirm'
				)
			);

			$hdrModel = WhseJobHdrRepository::commitVoidToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		
		$exc = new ApiException(__('WhseJob.doc_status_is_not_wip_or_complete', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\WhseJobHdr::class, $hdrModel->id);
		throw $exc;
	}

	public function revertProcess($hdrId)
  	{
		AuthService::authorize(
			array(
				'whse_job_revert'
			)
		);

		$hdrModel = WhseJobHdrRepository::findByPk($hdrId);

		$whseTxnFlow = WhseTxnFlowRepository::findByPk($hdrModel->site_flow_id, $hdrModel->proc_type);

		$hdrModel = self::transitionToDraft($hdrModel->id);

		return $hdrModel;
	}

	public function downloadProcess($hdrId, $mobileProfileId = 0, 
	$worker01Id = 0, $worker02Id = 0, $worker03Id = 0, $worker04Id = 0, $worker05Id = 0,
	$equipment01Id = 0, $equipment02Id = 0, $equipment03Id = 0, $equipment04Id = 0, $equipment05Id = 0)
  	{
		AuthService::authorize(
			array(
				'whse_job_download'
			)
		);

		$whseJobHdr = WhseJobHdrRepository::txnFindByPk($hdrId);
		if(empty($whseJobHdr))
		{
			$exc = new ApiException(__('WhseJob.job_not_found', ['id'=>$hdrId]));
			$exc->addData(\App\WhseJobHdr::class, $hdrId);
			throw $exc;
		}
		$whseJobDtls = WhseJobDtlRepository::findAllByHdrId($whseJobHdr->id);

		//verify this whseJobHdr req oledi complete
		foreach($whseJobDtls as $whseJobDtl)
		{
			if($whseJobDtl->req_whse_job_hdr_id > 0)
			{
				$reqWhseJobHdr = WhseJobHdrRepository::txnFindByPk($whseJobDtl->req_whse_job_hdr_id);
				if($reqWhseJobHdr->doc_status < DocStatus::$MAP['COMPLETE'])
				{
					$exc = new ApiException(__('WhseJob.required_job_not_complete', ['docCode'=>$reqWhseJobHdr->doc_code]));
					$exc->addData(\App\WhseJobHdr::class, $reqWhseJobHdr->id);
					throw $exc;
				}
			}
		}
		if($whseJobHdr->doc_status == DocStatus::$MAP['DRAFT'])
		{
			$whseJobHdr = self::transitionToWip($whseJobHdr->id, $mobileProfileId, 
				$worker01Id, $worker02Id, $worker03Id, $worker04Id, $worker05Id,
				$equipment01Id, $equipment02Id, $equipment03Id, $equipment04Id, $equipment05Id);
		}
		elseif($whseJobHdr->doc_status <= DocStatus::$MAP['COMPLETE']
		|| $whseJobHdr->doc_status > DocStatus::$MAP['DRAFT'])
		{
			if($whseJobHdr->mobile_profile_id != $mobileProfileId)
			{
				$exc = new ApiException(__('WhseJob.already_being_processed', ['mobileProfileId'=>$whseJobHdr->mobile_profile_id]));
				$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
				throw $exc;
			}
			else
			{
				if($whseJobHdr->doc_status == DocStatus::$MAP['COMPLETE'])
				{
					$exc = new ApiException(__('WhseJob.doc_status_is_complete', ['docCode'=>$whseJobHdr->doc_code]));
					$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
					throw $exc;
				}
			}
		}		

		$whseJobItems = WhseJobItemRepository::findAllByHdrId($whseJobHdr->id);
		$whseJobHdr->details = $whseJobDtls;
		$whseJobHdr->items = $whseJobItems;

		return $whseJobHdr;
	}

	public function completeProcess($hdrId, $dtlArray, $newItemArray)
  	{
		AuthService::authorize(
			array(
				'whse_job_confirm'
			)
		);

		if(is_null($dtlArray))
		{
			$exc = new ApiException(__('WhseJob.data_is_invalid', ['name'=>'details']));
			$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
			throw $exc;
		}
		if(is_null($newItemArray))
		{
			$exc = new ApiException(__('WhseJob.data_is_invalid', ['name'=>'new items']));
			$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
			throw $exc;
		}

		//temporarily item hashtable for later processing
		$newItemHash = array();
		$whseJobHdr = WhseJobHdrRepository::txnFindByPk($hdrId);

		$whseTxnFlow = WhseTxnFlowRepository::findByPk($whseJobHdr->site_flow_id, $whseJobHdr->proc_type);

		if($whseJobHdr->doc_status < DocStatus::$MAP['WIP'])
		{
			$exc = new ApiException(__('WhseJob.doc_status_is_not_wip', ['docCode'=>$whseJobHdr->doc_code]));
			$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
			throw $exc;
		}
		if($whseJobHdr->doc_status >= DocStatus::$MAP['COMPLETE'])
		{
			$exc = new ApiException(__('WhseJob.doc_status_is_complete', ['docCode'=>$whseJobHdr->doc_code]));
			$exc->addData('ERROR_CODE', 1);
			throw $exc;
		}

		//validate the gdsRcpt WIP Item
		foreach($newItemArray as $newItemData)
		{
			$newItemHash[$newItemData['item_id']] = $newItemData;
		}

		$whseJobDtlArray = array();
		$lineNo = 1;
		foreach($dtlArray as $dtlData)
		{
			//assume all details send from mobile is modified
			$dtlData['is_modified'] = 1;
			//verify compulsory fields
			//item_id, uom_id, qty, doc_hdr_id, doc_dtl_id, doc_hdr_type, company_id
			$this->processCompulsoryFields($dtlData);

			if(strcmp($dtlData['doc_hdr_type'], \App\PickListHdr::class) == 0)
			{
				$dtlQuantBal = QuantBalRepository::findByPk($dtlData['quant_bal_id']);
				if(empty($dtlQuantBal))
				{
					$exc = new ApiException(__('WhseJob.quant_bal_not_found', ['dtlId'=>$dtlData['id']]));
					$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
					throw $exc;
				}
				$toStorageBin = StorageBinRepository::findByPk($dtlData['to_storage_bin_id']);
				if(empty($toStorageBin))
				{
					$exc = new ApiException(__('WhseJob.to_storage_bin_not_found', ['dtlId'=>$dtlData['id']]));
					$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
					throw $exc;
				}                      
			}
			if(strcmp($dtlData['doc_hdr_type'], \App\PutAwayHdr::class) == 0)
			{
				$dtlQuantBal = QuantBalRepository::findByPk($dtlData['quant_bal_id']);
				if(empty($dtlQuantBal))
				{
					$exc = new ApiException(__('WhseJob.quant_bal_not_found', ['dtlId'=>$dtlData['id']]));
					$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
					throw $exc;
				}
				$toStorageBin = StorageBinRepository::findByPk($dtlData['to_storage_bin_id']);
				if(empty($toStorageBin))
				{
					$exc = new ApiException(__('WhseJob.to_storage_bin_not_found', ['dtlId'=>$dtlData['id']]));
					$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
					throw $exc;
				}
			}
			if(strcmp($dtlData['doc_hdr_type'], \App\GdsRcptHdr::class) == 0)
			{
				$toStorageBin = StorageBinRepository::findByPk($dtlData['to_storage_bin_id']);
				if(empty($toStorageBin))
				{
					$exc = new ApiException(__('WhseJob.to_storage_bin_not_found', ['dtlId'=>$dtlData['id']]));
					$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
					throw $exc;
				}
			}  
			if(strcmp($dtlData['doc_hdr_type'], \App\CycleCountHdr::class) == 0)
			{
				$storageBin = StorageBinRepository::findByPk($dtlData['storage_bin_id']);
				if(empty($storageBin))
				{
					$exc = new ApiException(__('WhseJob.storage_bin_not_found', ['dtlId'=>$dtlData['id']]));
					$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
					throw $exc;
				}
			}
			if(strcmp($dtlData['doc_hdr_type'], \App\BinTrfHdr::class) == 0)
			{
				$dtlQuantBal = QuantBalRepository::findByPk($dtlData['quant_bal_id']);
				if(empty($dtlQuantBal))
				{
					$exc = new ApiException(__('WhseJob.quant_bal_not_found', ['dtlId'=>$dtlData['id']]));
					$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
					throw $exc;
				}
				$toStorageBin = StorageBinRepository::findByPk($dtlData['to_storage_bin_id']);
				if(empty($toStorageBin))
				{
					$exc = new ApiException(__('WhseJob.to_storage_bin_not_found', ['dtlId'=>$dtlData['id']]));
					$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
					throw $exc;
				}
			}

			if(strcmp($dtlData['doc_hdr_type'], \App\GdsRcptHdr::class) == 0
			&& array_key_exists($dtlData['item_id'], $newItemHash))
			{
				$newItemData = $newItemHash[$dtlData['item_id']];

				//this detail line is new item, and this is GdsRcpt
				//unit_barcode and case_uom_rate is mandatory fields
				if(empty($newItemData['unit_barcode']))
				{
					$exc = new ApiException(__('WhseJob.new_item_unit_barcode_is_empty', ['lineNo'=>$lineNo]));
					$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
					throw $exc;
				}
				if(bccomp($newItemData['case_uom_rate'], 0) == 0)
				{
					$exc = new ApiException(__('WhseJob.new_item_case_uom_rate_is_empty', ['lineNo'=>$lineNo]));
					$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
					throw $exc;
				}

				//GdsRcpt detail, uom_id must be UNIT(3) or CASE(2)
				if($dtlData['uom_id'] != Uom::$UNIT
				&& $dtlData['uom_id'] != Uom::$CASE)
				{
					$exc = new ApiException(__('WhseJob.new_item_uom_must_be_unit_or_case', ['lineNo'=>$lineNo]));
					$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
					throw $exc;
				}
			}

			$lineNo++;
			$whseJobDtlArray[] = $dtlData;
		}

		$whseJobHdrData = $whseJobHdr->toArray();
		$result = WhseJobHdrRepository::updateDetails($whseJobHdrData, $whseJobDtlArray, array(), $newItemHash);
		$whseJobHdr = $result['hdrModel'];

		$whseJobHdr = self::transitionToComplete($whseJobHdr->id);

		return $whseJobHdr;
	}

	protected function processCompulsoryFields($dtlData)
  	{
		if(!isset($dtlData['company_id']))
		{
			$exc = new ApiException(__('WhseJob.field_is_compulsory', ['field'=>'company_id','dtlId'=>$dtlData['id']]));
			//$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
			throw $exc;
		}
		elseif(!isset($dtlData['whse_job_type']))
		{
			$exc = new ApiException(__('WhseJob.field_is_compulsory', ['field'=>'whse_job_type','dtlId'=>$dtlData['id']]));
			//$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
			throw $exc;
		}
		/*
		elseif(!isset($dtlData['doc_hdr_type']))
		{
			$exc = new ApiException(__('WhseJob.field_is_compulsory', ['field'=>'doc_hdr_type','dtlId'=>$dtlData['id']]));
			//$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
			throw $exc;
		}
		elseif(!isset($dtlData['doc_hdr_id']))
		{
			$exc = new ApiException(__('WhseJob.field_is_compulsory', ['field'=>'doc_hdr_id','dtlId'=>$dtlData['id']]));
			//$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
			throw $exc;
		}
		elseif(!isset($dtlData['doc_dtl_type']))
		{
			$exc = new ApiException(__('WhseJob.field_is_compulsory', ['field'=>'doc_dtl_type','dtlId'=>$dtlData['id']]));
			//$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
			throw $exc;
		}
		elseif(!isset($dtlData['doc_dtl_id']))
		{
			$exc = new ApiException(__('WhseJob.field_is_compulsory', ['field'=>'doc_dtl_id','dtlId'=>$dtlData['id']]));
			//$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
			throw $exc;
		}
		*/
		elseif(!isset($dtlData['uom_id']))
		{
			$exc = new ApiException(__('WhseJob.field_is_compulsory', ['field'=>'uom_id','dtlId'=>$dtlData['id']]));
			//$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
			throw $exc;
		}
		elseif(!isset($dtlData['qty']))
		{
			$exc = new ApiException(__('WhseJob.field_is_compulsory', ['field'=>'qty','dtlId'=>$dtlData['id']]));
			//$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
			throw $exc;
		}
		elseif(!isset($dtlData['item_id']))
		{
			$exc = new ApiException(__('WhseJob.field_is_compulsory', ['field'=>'item_id','dtlId'=>$dtlData['id']]));
			//$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
			throw $exc;
		}
	}

	protected function createWhseJob0501($procType, $hdrIds)
  	{
		AuthService::authorize(
			array(
				'whse_job_create'
			)
		);

		//1 packList to 1 whseJob
		$whseJobHdrDataList = array();
		
		$packListHdrs = PackListHdrRepository::findAllByHdrIds($hdrIds);
		$packListHdrs->load('siteFlow');
		foreach($packListHdrs as $packListHdr) 
		{
			$dtlDataList = array();
			$lineNo = 1;
			$docTxnFlowDataList = array();

			$siteFlow = $packListHdr->siteFlow;

			$packListDtls = PackListDtlRepository::findAllByHdrId($packListHdr->id);
			$packListDtls->load('quantBal');
			foreach($packListDtls as $packListDtl)
			{
				$quantBal = $packListDtl->quantBal;

				$dtlData = array(
					'is_split' => 0,
					'whse_job_type' => $packListDtl->whse_job_type,
					'req_whse_job_hdr_id' => 0,
					'company_id' => $packListDtl->company_id,
					'doc_hdr_type' => \App\PackListHdr::class,
					'doc_hdr_id' => $packListDtl->hdr_id,
					'doc_dtl_type' => \App\PackListDtl::class,
					'doc_dtl_id' => $packListDtl->id,
					'line_no' => $lineNo,
					'desc_01' => $packListDtl->desc_01,
					'desc_02' => $packListDtl->desc_02,

					'storage_bin_id' => $quantBal->storage_bin_id,
					'handling_unit_id' => $quantBal->handling_unit_id,
					'quant_bal_id' => $quantBal->id,
					'item_id' => $quantBal->item_id,

					'uom_id' => $packListDtl->uom_id,
					'uom_rate' => $packListDtl->uom_rate,
					'qty' => $packListDtl->qty,
					'doc_status' => DocStatus::$MAP['DRAFT'],

					'scan_mode' => 0,
					'to_storage_bin_id' => $packListDtl->to_storage_bin_id,
					'to_handling_unit_id' => $packListDtl->to_handling_unit_id,
				);
				$dtlDataList[] = $dtlData;

				$lineNo++;
			}

			//build the hdrData
			$hdrData = array(
				'ref_code_01' => '',
				'ref_code_02' => '',
				'doc_date' => date('Y-m-d'),
				'desc_01' => '',
				'desc_02' => '',
				'site_flow_id' => $siteFlow->id,
				'mobile_profile_id' => 0,
				'worker_01_id' => 0,
				'worker_02_id' => 0,
				'worker_03_id' => 0,
				'worker_04_id' => 0,
				'worker_05_id' => 0,
				'equipment_01_id' => 0,
				'equipment_02_id' => 0,
				'equipment_03_id' => 0,
				'equipment_04_id' => 0,
				'equipment_05_id' => 0
			);

			$tmpDocTxnFlowData = array(
				'fr_doc_hdr_type' => \App\PackListHdr::class,
				'fr_doc_hdr_id' => $packListHdr->id,
				'fr_doc_hdr_code' => $packListHdr->doc_code,
				'is_closed' => 1
			);
			$docTxnFlowDataList[] = $tmpDocTxnFlowData;

			$siteDocNo = SiteDocNoRepository::findSiteDocNo($siteFlow->site_id, \App\WhseJobHdr::class);
			if(empty($siteDocNo))
			{
				$exc = new ApiException(__('SiteDocNo.doc_no_not_found', ['siteId'=>$siteFlow->site_id, 'docType'=>\App\WhseJobHdr::class]));
				//$exc->addData(\App\SiteDocNo::class, $siteFlow->site_id);
				throw $exc;
			}

			$whseTxnFlow = WhseTxnFlowRepository::findByPk($siteFlow->id, $procType);
			
			//DRAFT
			$whseJobHdr = WhseJobHdrRepository::createProcess($procType, $siteDocNo->doc_no_id, $hdrData, $dtlDataList, $docTxnFlowDataList);

			if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
			{
				$whseJobHdr = self::transitionToWip($whseJobHdr->id);
			}
			elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
			{
				$whseJobHdr = self::transitionToComplete($whseJobHdr->id);
			}

			$whseJobHdr->str_doc_status = DocStatus::$MAP[$whseJobHdr->doc_status];
			$whseJobHdrDataList[] = $whseJobHdr;
		}

		$docCodeMsg = '';
		for ($a = 0; $a < count($whseJobHdrDataList); $a++)
		{
			$whseJobHdrData = $whseJobHdrDataList[$a];
			if($a == 0)
			{
				$docCodeMsg .= $whseJobHdrData->doc_code;
			}
			else
			{
				$docCodeMsg .= ', '.$whseJobHdrData->doc_code;
			}
		}
		$message = __('WhseJob.document_successfully_created', ['docCode'=>$docCodeMsg]);

		return array(
			'data' => $whseJobHdrDataList,
			'message' => $message
		);
	}

	public function printProcess($strProcType, $siteFlowId, $hdrIds)
  	{
		$user = Auth::user();

		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_03_01']) 
			{
				//print pick list WHSE_JOB
				return $this->printWhseJob030101($siteFlowId, $user, $hdrIds);
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_03_01_02']) 
			{
				//print pick list WHSE_JOB by pick list
				return $this->printWhseJob030102($siteFlowId, $user, $hdrIds);
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_14_01'])
			{
				//print gds rcpt WHSE_JOB
				return $this->printWhseJob140101($siteFlowId, $user, $hdrIds);
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_15_01'])
			{
				//print put away WHSE_JOB
				return $this->printWhseJob150101($siteFlowId, $user, $hdrIds);
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_16_01_02'])
			{
				//print cycle count WHSE_JOB
				return $this->printWhseJob160102($siteFlowId, $user, $hdrIds);
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['WHSE_JOB_17_02'])
			{
				//print bin trf WHSE_JOB
				return $this->printWhseJob170201($siteFlowId, $user, $hdrIds);
			}
		}
	}

	public function printWhseJob030101($siteFlowId, $user, $hdrIds)
	{
		AuthService::authorize(
			array(
				'pick_list_print'
			)
		);

		$docHdrType = \App\WhseJobHdr::class.'03';
		$printDocSetting = PrintDocSettingRepository::findByDocHdrTypeAndSiteFlowId($docHdrType, $siteFlowId);
		if(empty($printDocSetting))
		{
			$exc = new ApiException(__('PrintDoc.report_template_not_found', ['docType'=>\App\WhseJobHdr::class]));
			$exc->addData(\App\PrintDocSetting::class, \App\WhseJobHdr::class);
			throw $exc;
		}

		$whseJobHdrs = WhseJobHdrRepository::findAllByIds($hdrIds);
		$printDocTxnDataArray = array();
		foreach($whseJobHdrs as $whseJobHdr)
		{
			$whseJobHdr->view_name = $printDocSetting->view_name;

			$printDocTxnData = array();
			$printDocTxnData['doc_hdr_type'] = \App\WhseJobHdr::class;
			$printDocTxnData['doc_hdr_id'] = $whseJobHdr->id;
			$printDocTxnData['user_id'] = $user->id;
			$printDocTxnDataArray[] = $printDocTxnData;
		}
		
		foreach($whseJobHdrs as $whseJobHdr)
		{
			$whseJobHdr = self::processOutgoingHeader($whseJobHdr, false);
			$whseJobTypeHash = array();
			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			$netAmt = 0;
			//query the docDtls
			$whseJobDtls = WhseJobDtlRepository::findAllByHdrId($whseJobHdr->id);
			$whseJobDtls->load('item', 'storageBin', 'quantBal', 'quantBal.itemBatch', 'quantBal.storageBin', 'quantBal.storageRow', 'quantBal.storageBay');

			//check for the frDocHdrType, frDocHdrId
			$frDocHdrDataHash = array();
			$formatWhseJobDtls = array();
			foreach($whseJobDtls as $whseJobDtl)
			{
				$whseJobDtl = PickListService::processOutgoingDetail($whseJobDtl, true);

				//if job type is WAITING REPLENISHMENT, find the storageBinCode
				if($whseJobDtl->whse_job_type == WhseJobType::$MAP['WAITING_REPLENISHMENT'])
				{
					$whseJobCodeHash = array();
					$repWhseJobDtls = WhseJobDtlRepository::findAllByQuantBalIdAndWhseJobType($whseJobDtl->quant_bal_id, WhseJobType::$MAP['PICK_FACE_REPLENISHMENT']);
					foreach($repWhseJobDtls as $repWhseJobDtl)
					{
						$repWhseJobDtl = $repWhseJobDtls[$a];
						$whseJobCodeHash[$repWhseJobDtl->doc_code] = $repWhseJobDtl->doc_code;
					}
					$whseJobDtl->rep_whse_job_doc_codes = '';
					foreach($whseJobCodeHash as $docCode)
					{
						if(empty($whseJobDtl->whse_job_doc_codes))
						{
							$whseJobDtl->rep_whse_job_doc_codes .= $docCode;
						}
						else
						{
							$whseJobDtl->rep_whse_job_doc_codes .= ' '.$docCode;
						}
					}
				}

				$caseQty = bcadd($caseQty, $whseJobDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $whseJobDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $whseJobDtl->cubic_meter, 8);

				if($whseJobDtl->handling_unit_id > 0)
				{
					$palletHashByHandingUnitId[$whseJobDtl->handling_unit_id] = $whseJobDtl->handling_unit_id;
				}

				$whseJobDtl->str_whse_job_type = WhseJobType::$MAP[$whseJobDtl->whse_job_type];
				$whseJobDtl->whse_job_type_label = __('WhseJobType.'.strtolower($whseJobDtl->str_whse_job_type).'_label');
				$whseJobDtl->whse_job_type_desc = __('WhseJobType.'.strtolower($whseJobDtl->str_whse_job_type).'_desc');
				$whseJobTypeHash[$whseJobDtl->whse_job_type_desc] = $whseJobDtl->whse_job_type_desc;

				$key = $whseJobDtl->doc_hdr_type.';'.$whseJobDtl->doc_hdr_id;
				$frDocHdrDataHash[$key] = $key;

				unset($whseJobDtl->quantBal);
				unset($whseJobDtl->storageBin);
				unset($whseJobDtl->item);
				$formatWhseJobDtls[] = $whseJobDtl;
			}

			$pickListHdrIds = array();
			$frDocHdrs = array();
			foreach($frDocHdrDataHash as $key => $value)
			{
				$keyParts = explode(';', $key);
				$docHdrType = $keyParts[0];
				$docHdrId = $keyParts[1];
				if(strcmp($docHdrType, \App\PickListHdr::class) == 0)
				{
					$pickListHdrIds[] = $docHdrId;

					$pickListHdr = PickListHdrRepository::findByPk($docHdrId);
					$frDocHdrs[] = $pickListHdr;
				}	
			}
			//find all the pickListOutbOrds
			$pickListOutbOrds = PickListOutbOrdRepository::findAllByHdrIds($pickListHdrIds);
			$pickListOutbOrds->load('outbOrdHdr','outbOrdHdr.deliveryPoint','outbOrdHdr.deliveryPoint.area');
			$outbOrdHdrs = array();
			foreach($pickListOutbOrds as $pickListOutbOrd)
			{
				$outbOrdHdr = $pickListOutbOrd->outbOrdHdr;
				$deliveryPoint = $outbOrdHdr->deliveryPoint;
				$outbOrdHdr->delivery_point_code = $deliveryPoint->code;
				$outbOrdHdr->delivery_point_area_code = $deliveryPoint->area_code;
				$outbOrdHdr->delivery_point_area_desc_01 = $deliveryPoint->area_code;
				$area = $deliveryPoint->area;
				if(!empty($area))
				{
					$outbOrdHdr->delivery_point_area_desc_01 = $area->desc_01;
				}
				$outbOrdHdr->delivery_point_company_name_01 = $deliveryPoint->company_name_01;
				$outbOrdHdr->delivery_point_company_name_02 = $deliveryPoint->company_name_02;

				$netAmt = bcadd($netAmt, $outbOrdHdr->net_amt, 8);

				$outbOrdHdrs[] = $outbOrdHdr;
			}

			$whseJobHdr->pallet_qty = count($palletHashByHandingUnitId);
			$whseJobHdr->case_qty = $caseQty;
			$whseJobHdr->gross_weight = $grossWeight;
			$whseJobHdr->cubic_meter = $cubicMeter;
			$whseJobHdr->net_amt = $netAmt;

			//process the $whseJobTypeHash
			$whseJobHdr->whse_job_type_desc = '';
			$a = 0;
			foreach($whseJobTypeHash as $whseJobTypeDesc)
			{
				if($a == 0)
				{
					$whseJobHdr->whse_job_type_desc = $whseJobTypeDesc;
				}
				else
				{
					$whseJobHdr->whse_job_type_desc .= '/'.$whseJobTypeDesc;
				}
				$a++;
			}

			usort($formatWhseJobDtls, function ($a, $b) {
				//return ($a->item_bay_sequence < $b->item_bay_sequence) ? -1 : 1;
				return ($a->storage_bin_code < $b->storage_bin_code) ? -1 : 1;
			});
			$whseJobHdr->details = $formatWhseJobDtls;
			$whseJobHdr->outbOrdHdrs = $outbOrdHdrs;
			$whseJobHdr->frDocHdrs = $frDocHdrs;
			$whseJobHdr->barcode = '11'.str_pad($whseJobHdr->id, 5, '0', STR_PAD_LEFT);
		}

		$printedAt = PrintDocTxnRepository::createProcess($printDocTxnDataArray);

		$pdf = \Illuminate\Support\Facades\App::make('dompdf.wrapper');
		$pdf->getDomPDF()->set_option("enable_php", true);
		$pdf->setPaper(array(
			0,
			0,
			$printDocSetting->page_width, 
			$printDocSetting->page_height
		), $printDocSetting->page_orientation);

		//each data must have the view_name
		$pdf->loadHTML(
			view('batchPrintDocuments',
				array(
					'title' => 'Pick List Whse Job',
					'data' => $whseJobHdrs, 
					'printedAt' => $printedAt,
					'printedBy' => $user,
					'isPageBreakAfter' => true,
				)
			)
		);

		return $pdf->stream();
	}

	public function printWhseJob030102($siteFlowId, $user, $hdrIds)
	{
		AuthService::authorize(
			array(
				'pick_list_print'
			)
		);

		$docHdrType = \App\WhseJobHdr::class.'030102';
		$printDocSetting = PrintDocSettingRepository::findByDocHdrTypeAndSiteFlowId($docHdrType, $siteFlowId);
		if(empty($printDocSetting))
		{
			$exc = new ApiException(__('PrintDoc.report_template_not_found', ['docType'=>\App\WhseJobHdr::class]));
			$exc->addData(\App\PrintDocSetting::class, \App\WhseJobHdr::class);
			throw $exc;
		}

		$printDocTxnDataArray = array();
		$pickListHdrs = PickListHdrRepository::findAllByHdrIds($hdrIds);
		$pickListHdrs->load(
			'pickListOutbOrds', 'pickListOutbOrds.outbOrdHdr',
			'pickListOutbOrds.outbOrdHdr.deliveryPoint', 'pickListOutbOrds.outbOrdHdr.deliveryPoint.area',
			'toDocTxnFlows', 'toDocTxnFlows.toDocHdr'
		);		
		foreach($pickListHdrs as $pickListHdr)
		{
			$pickListHdr->view_name = $printDocSetting->view_name;
			$pickListHdr->pick_face_replenishments = array();
			$pickListHdr->details = array();
			$pickListHdr->outb_ord_hdrs = array();
			$pickListHdr->net_amt = 0;

			$pickListHdr->picking_whse_job_hdrs = array();
			$pickListHdr->replenish_whse_job_hdrs = array();

			$areaHashById = array();
			$deliveryPointHashById = array();
			$pickWhseJobHdrHashById = array();
			$replenishWhseJobHdrHashById = array();

			$fullPalletDetails = array();
			$loosePickingDetails = array();
			$pickFaceReplenishments = array();

			//find all the pickListOutbOrds
			$pickListOutbOrds = $pickListHdr->pickListOutbOrds;
			$outbOrdHdrs = array();
			
			foreach($pickListOutbOrds as $pickListOutbOrd)
			{
				$outbOrdHdr = $pickListOutbOrd->outbOrdHdr;
				$deliveryPoint = $outbOrdHdr->deliveryPoint;
				$deliveryPointHashById[$deliveryPoint->id] = $deliveryPoint;

				$outbOrdHdr->delivery_point_code = $deliveryPoint->code;
				$outbOrdHdr->delivery_point_area_code = $deliveryPoint->area_code;
				$outbOrdHdr->delivery_point_area_desc_01 = $deliveryPoint->area_code;
				$area = $deliveryPoint->area;
				if(!empty($area))
				{
					$areaHashById[$area->id] = $area;
					$outbOrdHdr->delivery_point_area_desc_01 = $area->desc_01;
				}
				$outbOrdHdr->delivery_point_company_name_01 = $deliveryPoint->company_name_01;
				$outbOrdHdr->delivery_point_company_name_02 = $deliveryPoint->company_name_02;

				$pickListHdr->net_amt = bcadd($pickListHdr->net_amt, $outbOrdHdr->net_amt, 8);

				$outbOrdHdrs[] = $outbOrdHdr;
			}
			$pickListHdr->outb_ord_hdrs = $outbOrdHdrs;
			$pickListHdr->areas = array_values($areaHashById);
			$pickListHdr->delivery_points = array_values($deliveryPointHashById);		

			$toDocTxnFlows = $pickListHdr->toDocTxnFlows;
			foreach($toDocTxnFlows as $toDocTxnFlow)
			{
				if(strcmp($toDocTxnFlow->to_doc_hdr_type, \App\WhseJobHdr::class) != 0)
				{
					//skip if this is not whseJobHdr
					continue;
				}

				$whseJobHdr = $toDocTxnFlow->toDocHdr;

				$printDocTxnData = array();
				$printDocTxnData['doc_hdr_type'] = \App\WhseJobHdr::class;
				$printDocTxnData['doc_hdr_id'] = $whseJobHdr->id;
				$printDocTxnData['user_id'] = $user->id;
				$printDocTxnDataArray[] = $printDocTxnData;

				$whseJobHdr = self::processOutgoingHeader($whseJobHdr, false);
				//query the docDtls
				$whseJobDtls = WhseJobDtlRepository::findAllByHdrId($whseJobHdr->id);
				$whseJobDtls->load('item', 'storageBin', 'quantBal', 'quantBal.itemBatch', 'quantBal.storageBin', 'quantBal.storageRow', 'quantBal.storageBay');

				//first loop to fill the basic info
				$formatWhseJobDtls = array();
				foreach($whseJobDtls as $whseJobDtl)
				{
					$whseJobDtl = PickListService::processOutgoingDetail($whseJobDtl, true);

					//if job type is WAITING REPLENISHMENT, find the storageBinCode
					if($whseJobDtl->whse_job_type == WhseJobType::$MAP['WAITING_REPLENISHMENT'])
					{
						$repPickListCodeHash = array();
						$repPickListDtls = PickListDtlRepository::findAllByQuantBalIdAndWhseJobType($whseJobDtl->quant_bal_id, WhseJobType::$MAP['PICK_FACE_REPLENISHMENT']);
						foreach($repPickListDtls as $repPickListDtl)
						{
							$repPickListCodeHash[$repPickListDtl->doc_code] = $repPickListDtl->doc_code;
						}
						$whseJobDtl->rep_pick_list_doc_codes = '';
						foreach($repPickListCodeHash as $repDocCode)
						{
							if(empty($whseJobDtl->rep_pick_list_doc_codes))
							{
								$whseJobDtl->rep_pick_list_doc_codes = $repDocCode;
							}
							else
							{
								$whseJobDtl->rep_pick_list_doc_codes .= ' '.$repDocCode;
							}
						}
					}
					if($whseJobDtl->whse_job_type == WhseJobType::$MAP['PICK_FACE_REPLENISHMENT'])
					{
						$pfPickListCodeHash = array();
						$pfPickListDtls = PickListDtlRepository::findAllByQuantBalIdAndWhseJobType($whseJobDtl->quant_bal_id, WhseJobType::$MAP['WAITING_REPLENISHMENT']);
						foreach($pfPickListDtls as $pfPickListDtl)
						{
							$pfPickListCodeHash[$pfPickListDtl->doc_code] = $pfPickListDtl->doc_code;
						}
						$whseJobDtl->pf_pick_list_doc_codes = '';
						foreach($pfPickListCodeHash as $pfDocCode)
						{
							if(empty($whseJobDtl->pf_pick_list_doc_codes))
							{
								$whseJobDtl->pf_pick_list_doc_codes = $pfDocCode;
							}
							else
							{
								$whseJobDtl->pf_pick_list_doc_codes .= ' '.$pfDocCode;
							}
						}
					}

					if($whseJobDtl->handling_unit_id > 0)
					{
						$palletHashByHandingUnitId[$whseJobDtl->handling_unit_id] = $whseJobDtl->handling_unit_id;
					}

					$whseJobDtl->str_whse_job_type = WhseJobType::$MAP[$whseJobDtl->whse_job_type];
					$whseJobDtl->whse_job_type_label = __('WhseJobType.'.strtolower($whseJobDtl->str_whse_job_type).'_label');
					$whseJobDtl->whse_job_type_desc = __('WhseJobType.'.strtolower($whseJobDtl->str_whse_job_type).'_desc');
					$whseJobTypeHash[$whseJobDtl->whse_job_type_desc] = $whseJobDtl->whse_job_type_desc;

					unset($whseJobDtl->quantBal);
					unset($whseJobDtl->storageBin);
					unset($whseJobDtl->item);
					$formatWhseJobDtls[] = $whseJobDtl;
				}

				usort($formatWhseJobDtls, function ($a, $b) {
					//return ($a->item_bay_sequence < $b->item_bay_sequence) ? -1 : 1;
					return ($a->storage_bin_code < $b->storage_bin_code) ? -1 : 1;
				});

				$whseJobHdr->barcode = '11'.str_pad($whseJobHdr->id, 5, '0', STR_PAD_LEFT);
				foreach($formatWhseJobDtls as $formatWhseJobDtl)
				{
					$formatWhseJobDtl->barcode = $whseJobHdr->barcode;

					if($formatWhseJobDtl->whse_job_type == WhseJobType::$MAP['PICK_FACE_REPLENISHMENT'])
					{
						$replenishWhseJobHdrHashById[$whseJobHdr->id] = $whseJobHdr;
						$pickFaceReplenishments[]  = $formatWhseJobDtl;
					}
					elseif($formatWhseJobDtl->whse_job_type == WhseJobType::$MAP['PICK_FULL_PALLET'])
					{
						$pickWhseJobHdrHashById[$whseJobHdr->id] = $whseJobHdr;
						$fullPalletDetails[] = $formatWhseJobDtl;
					}
					else
					{
						$pickWhseJobHdrHashById[$whseJobHdr->id] = $whseJobHdr;
						//all waiting replenish and loose picking is here
						$loosePickingDetails[] = $formatWhseJobDtl;
					}
				}				
			}

			//add full pallet to details first
			$details = array();
			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			foreach($fullPalletDetails as $fullPalletDetail)
			{
				$caseQty = bcadd($caseQty, $fullPalletDetail->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $fullPalletDetail->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $fullPalletDetail->cubic_meter, 8);
				$details[] = $fullPalletDetail;
			}	
			//follow by adding loose to details first
			foreach($loosePickingDetails as $loosePickingDetail)
			{
				$caseQty = bcadd($caseQty, $loosePickingDetail->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $loosePickingDetail->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $loosePickingDetail->cubic_meter, 8);
				$details[] = $loosePickingDetail;
			}
			
			$pickListHdr->details = $details;
			$pickListHdr->picking_whse_job_hdrs = array_values($pickWhseJobHdrHashById);
			$pickListHdr->replenish_whse_job_hdrs = array_values($replenishWhseJobHdrHashById);
			$pickListHdr->pick_face_replenishments = $pickFaceReplenishments;
			$pickListHdr->case_qty = $caseQty;
			$pickListHdr->gross_weight = $grossWeight;
			$pickListHdr->cubic_meter = $cubicMeter;
		}

		$printedAt = PrintDocTxnRepository::createProcess($printDocTxnDataArray);

		$pdf = \Illuminate\Support\Facades\App::make('dompdf.wrapper');
		$pdf->getDomPDF()->set_option("enable_php", true);
		$pdf->setPaper(array(
			0,
			0,
			$printDocSetting->page_width, 
			$printDocSetting->page_height
		), $printDocSetting->page_orientation);

		//each data must have the view_name
		$pdf->loadHTML(
			view('batchPrintDocuments',
				array(
					'title' => 'Pick List Whse Job',
					'data' => $pickListHdrs, 
					'printedAt' => $printedAt,
					'printedBy' => $user,
					'isPageBreakAfter' => true,
				)
			)
		);

		return $pdf->stream();
	}

	public function printWhseJob140101($siteFlowId, $user, $hdrIds)
	{
		AuthService::authorize(
			array(
				'gds_rcpt_print'
			)
		);

		$docHdrType = \App\WhseJobHdr::class.'14';
		$printDocSetting = PrintDocSettingRepository::findByDocHdrTypeAndSiteFlowId($docHdrType, $siteFlowId);
		if(empty($printDocSetting))
		{
			$exc = new ApiException(__('PrintDoc.report_template_not_found', ['docType'=>\App\WhseJobHdr::class]));
			$exc->addData(\App\PrintDocSetting::class, \App\WhseJobHdr::class);
			throw $exc;
		}

		$whseJobHdrs = WhseJobHdrRepository::findAllByIds($hdrIds);
		$whseJobHdrs->load(
			'frDocTxnFlows', 'frDocTxnFlows.frDocHdr'
		);
		$printDocTxnDataArray = array();
		foreach($whseJobHdrs as $whseJobHdr)
		{
			$whseJobHdr->view_name = $printDocSetting->view_name;

			$printDocTxnData = array();
			$printDocTxnData['doc_hdr_type'] = \App\WhseJobHdr::class;
			$printDocTxnData['doc_hdr_id'] = $whseJobHdr->id;
			$printDocTxnData['user_id'] = $user->id;
			$printDocTxnDataArray[] = $printDocTxnData;
		}
		
		foreach($whseJobHdrs as $whseJobHdr)
		{
			$whseJobHdr = self::processOutgoingHeader($whseJobHdr, false);
			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$whseJobDtls = WhseJobDtlRepository::findAllByHdrId($whseJobHdr->id);
			$whseJobDtls->load('item', 'storageBin', 'quantBal', 'quantBal.itemBatch', 'quantBal.storageBin', 'quantBal.storageRow', 'quantBal.storageBay');

			$formatWhseJobDtls = array();
			foreach($whseJobDtls as $whseJobDtl)
			{
				$whseJobDtl = GdsRcptService::processOutgoingDetail($whseJobDtl, false);

				$caseQty = bcadd($caseQty, $whseJobDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $whseJobDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $whseJobDtl->cubic_meter, 8);

				if($whseJobDtl->handling_unit_id > 0)
				{
					$palletHashByHandingUnitId[$whseJobDtl->handling_unit_id] = $whseJobDtl->handling_unit_id;
				}

				$whseJobDtl->str_whse_job_type = WhseJobType::$MAP[$whseJobDtl->whse_job_type];
				$whseJobDtl->whse_job_type_label = __('WhseJobType.'.strtolower($whseJobDtl->str_whse_job_type).'_label');

				unset($whseJobDtl->quantBal);
				unset($whseJobDtl->storageBin);
				unset($whseJobDtl->item);
				$formatWhseJobDtls[] = $whseJobDtl;
			}

			$whseJobHdr->pallet_qty = count($palletHashByHandingUnitId);
			$whseJobHdr->case_qty = $caseQty;
			$whseJobHdr->gross_weight = $grossWeight;
			$whseJobHdr->cubic_meter = $cubicMeter;

			usort($formatWhseJobDtls, function ($a, $b) {
				if($a->item_code == $b->item_code)
				{
					return ($a->case_qty < $b->case_qty) ? 1 : -1;
				}
				return ($a->item_code < $b->item_code) ? -1 : 1;
			});
			$whseJobHdr->details = $formatWhseJobDtls;

			$frDocHdrs = array();
			foreach($whseJobHdr->frDocTxnFlows as $frDocTxnFlow)
			{
				$frDocHdrs[] = $frDocTxnFlow->frDocHdr;
			}
			$whseJobHdr->frDocHdrs = $frDocHdrs;
			$whseJobHdr->barcode = '11'.str_pad($whseJobHdr->id, 5, '0', STR_PAD_LEFT);
		}

		$printedAt = PrintDocTxnRepository::createProcess($printDocTxnDataArray);

		$pdf = \Illuminate\Support\Facades\App::make('dompdf.wrapper');
		$pdf->getDomPDF()->set_option("enable_php", true);
		$pdf->setPaper(array(
			0,
			0,
			$printDocSetting->page_width, 
			$printDocSetting->page_height
		), $printDocSetting->page_orientation);

		//each data must have the view_name
		$pdf->loadHTML(
			view('batchPrintDocuments',
				array(
					'title' => 'Goods Receipt Whse Job',
					'data' => $whseJobHdrs, 
					'printedAt' => $printedAt,
					'printedBy' => $user,
					'isPageBreakAfter' => true,
				)
			)
		);

		return $pdf->stream();
	}

	public function printWhseJob150101($siteFlowId, $user, $hdrIds)
	{
		AuthService::authorize(
			array(
				'put_away_print'
			)
		);

		$docHdrType = \App\WhseJobHdr::class.'15';
		$printDocSetting = PrintDocSettingRepository::findByDocHdrTypeAndSiteFlowId($docHdrType, $siteFlowId);
		if(empty($printDocSetting))
		{
			$exc = new ApiException(__('PrintDoc.report_template_not_found', ['docType'=>\App\WhseJobHdr::class]));
			$exc->addData(\App\PrintDocSetting::class, \App\WhseJobHdr::class);
			throw $exc;
		}

		$whseJobHdrs = WhseJobHdrRepository::findAllByIds($hdrIds);
		$whseJobHdrs->load(
			'frDocTxnFlows', 'frDocTxnFlows.frDocHdr'
		);
		$printDocTxnDataArray = array();
		foreach($whseJobHdrs as $whseJobHdr)
		{
			$whseJobHdr->view_name = $printDocSetting->view_name;

			$printDocTxnData = array();
			$printDocTxnData['doc_hdr_type'] = \App\WhseJobHdr::class;
			$printDocTxnData['doc_hdr_id'] = $whseJobHdr->id;
			$printDocTxnData['user_id'] = $user->id;
			$printDocTxnDataArray[] = $printDocTxnData;
		}
		
		foreach($whseJobHdrs as $whseJobHdr)
		{
			$whseJobHdr = self::processOutgoingHeader($whseJobHdr, false);
			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$whseJobDtls = WhseJobDtlRepository::findAllByHdrId($whseJobHdr->id);
			$whseJobDtls->load('item', 'storageBin', 'toStorageBin', 'quantBal', 'quantBal.itemBatch', 'quantBal.storageBin', 'quantBal.storageRow', 'quantBal.storageBay');

			$formatWhseJobDtls = array();
			foreach($whseJobDtls as $whseJobDtl)
			{
				$whseJobDtl = PutAwayService::processOutgoingDetail($whseJobDtl, false);

				$caseQty = bcadd($caseQty, $whseJobDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $whseJobDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $whseJobDtl->cubic_meter, 8);

				if($whseJobDtl->handling_unit_id > 0)
				{
					$palletHashByHandingUnitId[$whseJobDtl->handling_unit_id] = $whseJobDtl->handling_unit_id;
				}

				$whseJobDtl->str_whse_job_type = WhseJobType::$MAP[$whseJobDtl->whse_job_type];
				$whseJobDtl->whse_job_type_label = __('WhseJobType.'.strtolower($whseJobDtl->str_whse_job_type).'_label');

				unset($whseJobDtl->quantBal);
				unset($whseJobDtl->storageBin);
				unset($whseJobDtl->item);
				$formatWhseJobDtls[] = $whseJobDtl;
			}

			$whseJobHdr->pallet_qty = count($palletHashByHandingUnitId);
			$whseJobHdr->case_qty = $caseQty;
			$whseJobHdr->gross_weight = $grossWeight;
			$whseJobHdr->cubic_meter = $cubicMeter;

			usort($formatWhseJobDtls, function ($a, $b) {
				if($a->item_code == $b->item_code)
				{
					return ($a->case_qty < $b->case_qty) ? 1 : -1;
				}
				return ($a->item_code < $b->item_code) ? -1 : 1;
			});
			$whseJobHdr->details = $formatWhseJobDtls;

			$frDocHdrs = array();
			foreach($whseJobHdr->frDocTxnFlows as $frDocTxnFlow)
			{
				$frDocHdrs[] = $frDocTxnFlow->frDocHdr;
			}
			$whseJobHdr->frDocHdrs = $frDocHdrs;
			$whseJobHdr->barcode = '11'.str_pad($whseJobHdr->id, 5, '0', STR_PAD_LEFT);
		}

		$printedAt = PrintDocTxnRepository::createProcess($printDocTxnDataArray);

		$pdf = \Illuminate\Support\Facades\App::make('dompdf.wrapper');
		$pdf->getDomPDF()->set_option("enable_php", true);
		$pdf->setPaper(array(
			0,
			0,
			$printDocSetting->page_width, 
			$printDocSetting->page_height
		), $printDocSetting->page_orientation);

		//each data must have the view_name
		$pdf->loadHTML(
			view('batchPrintDocuments',
				array(
					'title' => 'Put Away Whse Job',
					'data' => $whseJobHdrs, 
					'printedAt' => $printedAt,
					'printedBy' => $user,
					'isPageBreakAfter' => true,
				)
			)
		);

		return $pdf->stream();
	}

	protected function indexWhseJob1401($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
			array(
				'gds_rcpt_read'
			)
		);

		$gdsRcptHdrs = GdsRcptHdrRepository::findAllNotExistWhseJob1401Txn($siteFlowId, $sorts, $filters, $pageSize);
		$gdsRcptHdrs->load('gdsRcptInbOrds');
		foreach($gdsRcptHdrs as $gdsRcptHdr)
		{
			$gdsRcptHdr = GdsRcptService::processOutgoingHeader($gdsRcptHdr, false);
			
			//query the inbOrds
			$gdsRcptInbOrds = $gdsRcptHdr->gdsRcptInbOrds;
			$gdsRcptInbOrds->load('inbOrdHdr');
			for($a = 0; $a < count($gdsRcptInbOrds); $a++)
			{
				$gdsRcptInbOrd = $gdsRcptInbOrds[$a];
				$inbOrdHdr = $gdsRcptInbOrd->inbOrdHdr;
				if($a == 0)
				{
					$gdsRcptHdr->adv_ship_hdr_code = $inbOrdHdr->adv_ship_hdr_code;
					$gdsRcptHdr->ref_code_01 = $inbOrdHdr->ref_code_01;
					$gdsRcptHdr->ref_code_02 = $inbOrdHdr->ref_code_02;
					$gdsRcptHdr->ref_code_03 = $inbOrdHdr->ref_code_03;
					$gdsRcptHdr->ref_code_04 = $inbOrdHdr->ref_code_04;
					$gdsRcptHdr->ref_code_05 = $inbOrdHdr->ref_code_05;
					$gdsRcptHdr->ref_code_06 = $inbOrdHdr->ref_code_06;
					$gdsRcptHdr->ref_code_07 = $inbOrdHdr->ref_code_07;
					$gdsRcptHdr->ref_code_08 = $inbOrdHdr->ref_code_08;
				}
				else
				{
					$gdsRcptHdr->adv_ship_hdr_code .= '/'.$inbOrdHdr->adv_ship_hdr_code;
					$gdsRcptHdr->ref_code_01 .= '/'.$inbOrdHdr->ref_code_01;
					$gdsRcptHdr->ref_code_02 .= '/'.$inbOrdHdr->ref_code_02;
					$gdsRcptHdr->ref_code_03 .= '/'.$inbOrdHdr->ref_code_03;
					$gdsRcptHdr->ref_code_04 .= '/'.$inbOrdHdr->ref_code_04;
					$gdsRcptHdr->ref_code_05 .= '/'.$inbOrdHdr->ref_code_05;
					$gdsRcptHdr->ref_code_06 .= '/'.$inbOrdHdr->ref_code_06;
					$gdsRcptHdr->ref_code_07 .= '/'.$inbOrdHdr->ref_code_07;
					$gdsRcptHdr->ref_code_08 .= '/'.$inbOrdHdr->ref_code_08;
				}
			}

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$gdsRcptDtls = GdsRcptDtlRepository::findAllByHdrId($gdsRcptHdr->id);
			$gdsRcptDtls->load('item', 'toStorageBin', 'toStorageBin.storageBay', 'toStorageBin.storageRow');

			foreach($gdsRcptDtls as $gdsRcptDtl)
			{
				$gdsRcptDtl = GdsRcptService::processOutgoingDetail($gdsRcptDtl);

				$caseQty = bcadd($caseQty, $gdsRcptDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $gdsRcptDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $gdsRcptDtl->cubic_meter, 8);

				if($gdsRcptDtl->to_handling_unit_id > 0)
				{
					$palletHashByHandingUnitId[$gdsRcptDtl->to_handling_unit_id] = $gdsRcptDtl->to_handling_unit_id;
				}
				unset($gdsRcptDtl->toStorageBin);
				unset($gdsRcptDtl->item);
			}
			
			$gdsRcptHdr->pallet_qty = count($palletHashByHandingUnitId);
			$gdsRcptHdr->case_qty = $caseQty;
			$gdsRcptHdr->gross_weight = $grossWeight;
			$gdsRcptHdr->cubic_meter = $cubicMeter;

			$gdsRcptHdr->details = $gdsRcptDtls;
		}
    	return $gdsRcptHdrs;
	}

	protected function createWhseJob1401($procType, $hdrIds)
  	{
		AuthService::authorize(
			array(
				'whse_job_create'
			)
		);

		//1 gdsRcpt to 1 whseJob
		$whseJobHdrDataList = array();

		$gdsRcptHdrs = GdsRcptHdrRepository::findAllByHdrIds($hdrIds);
		$gdsRcptHdrs->load('siteFlow');
		foreach($gdsRcptHdrs as $gdsRcptHdr) 
		{
			//check the frDocHdr to make sure it is not closed
			$tmpDocTxnFlow = DocTxnFlowRepository::findByProcTypeAndFrHdrId($procType, \App\GdsRcptHdr::class, $gdsRcptHdr->id, 1);
			if(!empty($tmpDocTxnFlow))
			{
				$exc = new ApiException(__('WhseJob.fr_doc_is_closed', ['docType'=>$tmpDocTxnFlow->fr_doc_hdr_type, 'docCode'=>$tmpDocTxnFlow->fr_doc_hdr_code]));
				$exc->addData($tmpDocTxnFlow->fr_doc_hdr_type, $tmpDocTxnFlow->fr_doc_hdr_code);
				throw $exc;
			}

			$siteFlow = $gdsRcptHdr->siteFlow;

			$siteDocNo = SiteDocNoRepository::findSiteDocNo($siteFlow->site_id, \App\WhseJobHdr::class);
			if(empty($siteDocNo))
			{
				$exc = new ApiException(__('SiteDocNo.doc_no_not_found', ['siteId'=>$siteFlow->site_id, 'docType'=>\App\WhseJobHdr::class]));
				//$exc->addData(\App\SiteDocNo::class, $siteFlow->site_id);
				throw $exc;
			}

			$whseTxnFlow = WhseTxnFlowRepository::findByPk($siteFlow->id, $procType);

			$lineNo = 1;
			$dtlDataList = array();
			$docTxnFlowDataList = array();
			//whseJobItem
			$itemDataList = array();
			
			//START: create goods receipt warehouse jobs
			$gdsRcptDtls = GdsRcptDtlRepository::findAllByHdrId($gdsRcptHdr->id);
			$gdsRcptDtls->load('item', 'toStorageBin', 'toHandlingUnit');
			foreach($gdsRcptDtls as $gdsRcptDtl) 
			{
				$dtlData = array(
					'is_split' => 0,
					'whse_job_type' => $gdsRcptDtl->whse_job_type,
					'req_whse_job_hdr_id' => 0,
					'company_id' => $gdsRcptDtl->company_id,
					'doc_hdr_type' => \App\GdsRcptHdr::class,
					'doc_hdr_id' => $gdsRcptDtl->hdr_id,
					'doc_dtl_type' => \App\GdsRcptDtl::class,
					'doc_dtl_id' => $gdsRcptDtl->id,
					'line_no' => $lineNo,
					'desc_01' => $gdsRcptDtl->desc_01,
					'desc_02' => $gdsRcptDtl->desc_02,

					'storage_bin_id' => 0,
					'handling_unit_id' => 0,
					'quant_bal_id' => 0,

					'item_id' => $gdsRcptDtl->item_id,

					'batch_serial_no' => $gdsRcptDtl->batch_serial_no,
					'expiry_date' => $gdsRcptDtl->expiry_date,
					'receipt_date' => $gdsRcptDtl->receipt_date,					
					'item_cond_01_id' => $gdsRcptDtl->item_cond_01_id,		
					'item_cond_02_id' => $gdsRcptDtl->item_cond_02_id,		
					'item_cond_03_id' => $gdsRcptDtl->item_cond_03_id,		
					'item_cond_04_id' => $gdsRcptDtl->item_cond_04_id,		
					'item_cond_05_id' => $gdsRcptDtl->item_cond_05_id,

					'uom_id' => $gdsRcptDtl->uom_id,
					'uom_rate' => $gdsRcptDtl->uom_rate,
					'qty' => $gdsRcptDtl->qty,
					'doc_status' => DocStatus::$MAP['DRAFT'],

					'scan_mode' => 0,
					'to_storage_bin_id' => $gdsRcptDtl->to_storage_bin_id,
					'to_handling_unit_id' => $gdsRcptDtl->to_handling_unit_id,
				);
				$dtlDataList[] = $dtlData;

				$lineNo++;
			}

			//build the hdrData
			$hdrData = array(
				'ref_code_01' => '',
				'ref_code_02' => '',
				'doc_date' => date('Y-m-d'),
				'desc_01' => '',
				'desc_02' => '',
				'site_flow_id' => $siteFlow->id,
				'mobile_profile_id' => 0,
				'worker_01_id' => 0,
				'worker_02_id' => 0,
				'worker_03_id' => 0,
				'worker_04_id' => 0,
				'worker_05_id' => 0,
				'equipment_01_id' => 0,
				'equipment_02_id' => 0,
				'equipment_03_id' => 0,
				'equipment_04_id' => 0,
				'equipment_05_id' => 0
			);

			$tmpDocTxnFlowData = array(
				'fr_doc_hdr_type' => \App\GdsRcptHdr::class,
				'fr_doc_hdr_id' => $gdsRcptHdr->id,
				'fr_doc_hdr_code' => $gdsRcptHdr->doc_code,
				'is_closed' => 1
			);
			$docTxnFlowDataList[] = $tmpDocTxnFlowData;

			$gdsRcptItems = GdsRcptItemRepository::findAllByHdrIds($hdrIds);
			foreach($gdsRcptItems as $gdsRcptItem)
			{
				$itemData = array(
					'doc_hdr_type' => \App\GdsRcptHdr::class,
					'doc_hdr_id' => $gdsRcptItem->hdr_id,
					'doc_item_type' => \App\GdsRcptItem::class,
					'doc_item_id' => $gdsRcptItem->id,
					'item_id' => $gdsRcptItem->item_id,
					'code' => $gdsRcptItem->code,
					'ref_code_01' => $gdsRcptItem->ref_code_01,
					'desc_01' => $gdsRcptItem->desc_01,
					'desc_02' => $gdsRcptItem->desc_02,
					'item_status' => $gdsRcptItem->item_status,
					'unit_barcode' => $gdsRcptItem->unit_barcode,
					'case_barcode' => $gdsRcptItem->case_barcode,
					'case_uom_rate' => $gdsRcptItem->case_uom_rate
				);
				$itemDataList[] = $itemData;
			}

			//DRAFT
			$whseJobHdr = WhseJobHdrRepository::createProcess($procType, $siteDocNo->doc_no_id, $hdrData, $dtlDataList, $docTxnFlowDataList, $itemDataList);

			if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
			{
				$whseJobHdr = self::transitionToWip($whseJobHdr->id);
			}
			elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
			{
				$whseJobHdr = self::transitionToComplete($whseJobHdr->id);
			}

			$whseJobHdr->str_doc_status = DocStatus::$MAP[$whseJobHdr->doc_status];
			$whseJobHdrDataList[] = $whseJobHdr;
			//END: create goods receipt warehouse jobs
		}

		$docCodeMsg = '';
		for ($a = 0; $a < count($whseJobHdrDataList); $a++)
		{
			$whseJobHdrData = $whseJobHdrDataList[$a];
			if($a == 0)
			{
				$docCodeMsg .= $whseJobHdrData->doc_code;
			}
			else
			{
				$docCodeMsg .= ', '.$whseJobHdrData->doc_code;
			}
		}
		$message = __('WhseJob.document_successfully_created', ['docCode'=>$docCodeMsg]);

		return array(
			'data' => $whseJobHdrDataList,
			'message' => $message
		);
	}

	protected function createWhseJob1601($procType, $hdrIds)
  	{
		AuthService::authorize(
			array(
				'whse_job_create'
			)
		);

		//1 cycle count -> m warehouse job		
		$whseJobHdrDataList = array();

		$cycleCountHdrs = CycleCountHdrRepository::findAllByHdrIds($hdrIds);
		$cycleCountHdrs->load(
			'cycleCountDtls', 'siteFlow', 
			'cycleCountDtls.item', 'cycleCountDtls.storageBin', 
			'cycleCountDtls.handlingUnit'
		);

		//START: create cycle count warehouse jobs
		foreach($cycleCountHdrs as $cycleCountHdr)
		{
			$siteFlow = $cycleCountHdr->siteFlow;

			//check the frDocHdr to make sure it is not closed
			$tmpDocTxnFlow = DocTxnFlowRepository::findByProcTypeAndFrHdrId($procType, \App\CycleCountHdr::class, $cycleCountHdr->id, 1);
			if(!empty($tmpDocTxnFlow))
			{
				$exc = new ApiException(__('WhseJob.fr_doc_is_closed', ['docType'=>$tmpDocTxnFlow->fr_doc_hdr_type, 'docCode'=>$tmpDocTxnFlow->fr_doc_hdr_code]));
				$exc->addData($tmpDocTxnFlow->fr_doc_hdr_type, $tmpDocTxnFlow->fr_doc_hdr_code);
				throw $exc;
			}

			$siteDocNo = SiteDocNoRepository::findSiteDocNo($siteFlow->site_id, \App\WhseJobHdr::class);
			if(empty($siteDocNo))
			{
				$exc = new ApiException(__('SiteDocNo.doc_no_not_found', ['siteId'=>$siteFlow->site_id, 'docType'=>\App\WhseJobHdr::class]));
				//$exc->addData(\App\SiteDocNo::class, $siteFlow->site_id);
				throw $exc;
			}

			$whseTxnFlow = WhseTxnFlowRepository::findByPk($siteFlow->id, $procType);

			$cycleCountDtlsHashByGroupNoAndJobNo = array();
			$cycleCountDtls = $cycleCountHdr->cycleCountDtls;

			//sort by group_no, job_no, line_no
			$cycleCountDtls = $cycleCountDtls->sort(function ($a, $b) {
				if($a->group_no == $b->group_no)
				{
					if($a->job_no == $b->job_no)
					{
						return $a->line_no > $b->line_no;
					}
					return $a->job_no > $b->job_no;
				}
				return $a->group_no > $b->group_no;
			});

			foreach($cycleCountDtls as $cycleCountDtl)
			{
				$key = $cycleCountDtl->group_no.'/'.$cycleCountDtl->job_no;
				$tmpCycleCountDtls = array();
				if(array_key_exists($key, $cycleCountDtlsHashByGroupNoAndJobNo))
				{
					$tmpCycleCountDtls = $cycleCountDtlsHashByGroupNoAndJobNo[$key];
				}
				$tmpCycleCountDtls[] = $cycleCountDtl;
				$cycleCountDtlsHashByGroupNoAndJobNo[$key] = $tmpCycleCountDtls;
			}
			
			$a = 0;
			foreach($cycleCountDtlsHashByGroupNoAndJobNo as $key => $tmpCycleCountDtls)
			{
				$isFrDocClosed = false;
				if ($a == 0) {
					// first
				} else if ($a == count($cycleCountDtlsHashByGroupNoAndJobNo) - 1) {
					// last
					$isFrDocClosed = true;
				}

				//2) sort the cycleCountList array by line_no ASC
				/*
				usort($tmpCycleCountDtls, function ($a, $b) {
					return ($a->line_no < $b->line_no) ? -1 : 1;
				});
				*/

				$curGroupNo = 0;
				$curJobNo = 0;
				$curJobDesc01 = '';

				$lineNo = 1;
				$dtlDataList = array();
				$docTxnFlowDataList = array();
				foreach($tmpCycleCountDtls as $cycleCountDtl)
				{
					$curGroupNo = $cycleCountDtl->group_no;
					$curJobNo = $cycleCountDtl->job_no;
					$curJobDesc01 = $cycleCountDtl->job_desc_01;
					
					$dtlData = array(
						'is_split' => 0,
						'whse_job_type' => $cycleCountDtl->whse_job_type,
						'req_whse_job_hdr_id' => 0,
						'company_id' => $cycleCountDtl->company_id,
						'doc_hdr_type' => \App\CycleCountHdr::class,
						'doc_hdr_id' => $cycleCountDtl->hdr_id,
						'doc_dtl_type' => \App\CycleCountDtl::class,
						'doc_dtl_id' => $cycleCountDtl->id,
						'line_no' => $lineNo,
						'desc_01' => $cycleCountDtl->desc_01,
						'desc_02' => $cycleCountDtl->desc_02,

						'storage_bin_id' => $cycleCountDtl->storage_bin_id,
						'handling_unit_id' => $cycleCountDtl->handling_unit_id,
						'quant_bal_id' => 0,

						'item_id' => $cycleCountDtl->item_id,

						'batch_serial_no' => $cycleCountDtl->batch_serial_no,
						'expiry_date' => $cycleCountDtl->expiry_date,
						'receipt_date' => $cycleCountDtl->receipt_date,					
						'item_cond_01_id' => 0,		
						'item_cond_02_id' => 0,		
						'item_cond_03_id' => 0,		
						'item_cond_04_id' => 0,		
						'item_cond_05_id' => 0,

						'uom_id' => $cycleCountDtl->uom_id,
						'uom_rate' => $cycleCountDtl->uom_rate,
						'qty' => $cycleCountDtl->qty,
						'doc_status' => DocStatus::$MAP['DRAFT'],

						'scan_mode' => 0,
						'to_storage_bin_id' => 0,
						'to_handling_unit_id' => 0,
					);
					$dtlDataList[] = $dtlData;

					$lineNo++;
				}

				//build the hdrData
				$hdrData = array(
					'ref_code_01' => $cycleCountHdr->ref_code_01,
					'ref_code_02' => $cycleCountHdr->ref_code_02,
					'doc_date' => date('Y-m-d'),
					'desc_01' => 'GROUP: #'.$curGroupNo.' JOB: #'.$curJobNo,
					'desc_02' => $curJobDesc01,
					'site_flow_id' => $siteFlow->id,
					'mobile_profile_id' => 0,
					'worker_01_id' => 0,
					'worker_02_id' => 0,
					'worker_03_id' => 0,
					'worker_04_id' => 0,
					'worker_05_id' => 0,
					'equipment_01_id' => 0,
					'equipment_02_id' => 0,
					'equipment_03_id' => 0,
					'equipment_04_id' => 0,
					'equipment_05_id' => 0
				);

				$tmpDocTxnFlowData = array(
					'fr_doc_hdr_type' => \App\CycleCountHdr::class,
					'fr_doc_hdr_id' => $cycleCountHdr->id,
					'fr_doc_hdr_code' => $cycleCountHdr->doc_code,
					'is_closed' => $isFrDocClosed
				);
				$docTxnFlowDataList[] = $tmpDocTxnFlowData;

				//DRAFT
				$whseJobHdr = WhseJobHdrRepository::createProcess($procType, $siteDocNo->doc_no_id, $hdrData, $dtlDataList, $docTxnFlowDataList);

				if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
				{
					$whseJobHdr = self::transitionToWip($whseJobHdr->id);
				}
				elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
				{
					$whseJobHdr = self::transitionToComplete($whseJobHdr->id);
				}

				$whseJobHdr->str_doc_status = DocStatus::$MAP[$whseJobHdr->doc_status];
				$whseJobHdrDataList[] = $whseJobHdr;

				$a++;
			}
		}
		//END: create goods receipt warehouse jobs

		$docCodeMsg = '';
		for ($a = 0; $a < count($whseJobHdrDataList); $a++)
		{
			$whseJobHdrData = $whseJobHdrDataList[$a];
			if($a == 0)
			{
				$docCodeMsg .= $whseJobHdrData->doc_code;
			}
			else
			{
				$docCodeMsg .= ', '.$whseJobHdrData->doc_code;
			}
		}
		$message = __('WhseJob.document_successfully_created', ['docCode'=>$docCodeMsg]);

		return array(
			'data' => $whseJobHdrDataList,
			'message' => $message
		);
	}

	protected function indexWhseJob1601($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
			array(
				'cycle_count_read'
			)
		);

		$cycleCountHdrs = CycleCountHdrRepository::findAllNotExistWhseJob1601Txn($siteFlowId, $sorts, $filters, $pageSize);
		$cycleCountHdrs->load(
			'cycleCountDtls',
			'cycleCountDtls.item', 'cycleCountDtls.storageBin', 
			'cycleCountDtls.storageBin.storageBay', 'cycleCountDtls.storageBin.storageRow'
		);
		foreach($cycleCountHdrs as $cycleCountHdr)
		{
			$cycleCountHdr->str_doc_status = DocStatus::$MAP[$cycleCountHdr->doc_status];

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$cycleCountDtls = $cycleCountHdr->cycleCountDtls;

			//sort by job_no, group_no, line_no
			$cycleCountDtls = $cycleCountDtls->sort(function ($a, $b) {
				if($a->job_no == $b->job_no)
				{
					if($a->group_no == $b->group_no)
					{
						return $a->line_no > $b->line_no;
					}
					return $a->group_no > $b->group_no;
				}
				return $a->job_no > $b->job_no;
			});

			foreach($cycleCountDtls as $cycleCountDtl)
			{
				$item = $cycleCountDtl->item;
				if(empty($item))
				{
					$item = new Item;
					$item->code = '';
					$item->ref_code_01 = '';
					$item->desc_01 = '';
					$item->desc_02 = '';
					$item->storage_class = 0;
					$item->item_group_01_code = '';
					$item->item_group_02_code = '';
					$item->item_group_03_code = '';
					$item->item_group_04_code = '';
					$item->item_group_05_code = '';
					$item->case_ext_length = 0;
					$item->case_ext_width = 0;
					$item->case_ext_height = 0;
					$item->uom_rate = 1;
					$item->case_uom_rate = 35000;
					$item->case_gross_weight = 0;
				}
				$storageBin = $cycleCountDtl->storageBin;

				$whse_job_key = $cycleCountDtl->whse_job_type.':'.$cycleCountDtl->whse_job_code;
				$cycleCountDtl->whse_job_key = $whse_job_key;
				$cycleCountDtl->str_whse_job_type = WhseJobType::$MAP[$cycleCountDtl->whse_job_type];
				$cycleCountDtl->str_whse_job_type = __('WhseJob.'.strtolower($cycleCountDtl->str_whse_job_type));

				$cycleCountDtl->storage_bin_code = '';
				$cycleCountDtl->storage_row_code = '';
				$cycleCountDtl->storage_bay_code = '';
				$cycleCountDtl->item_bay_sequence = 0;
				if(!empty($storageBin))
				{
					$cycleCountDtl->storage_bin_code = $storageBin->code;
					if(!empty($storageBin->storageRow))
					{
						$cycleCountDtl->storage_row_code = $storageBin->storageRow->code;
					}				
					if(!empty($storageBin->storageBay))
					{
						$cycleCountDtl->storage_bay_code = $storageBin->storageBay->code;
						$cycleCountDtl->item_bay_sequence = $storageBin->storageBay->bay_sequence;
					}
				}

				$cycleCountDtl = ItemService::processCaseLoose($cycleCountDtl, $item);

				$cycleCountDtl->pallet_qty = 0;
				if($cycleCountDtl->handling_unit_id > 0)
				{
					//then tis is full pallet picking
					$cycleCountDtl->pallet_qty = 1;
					$cycleCountDtl->case_qty = 0;
				}

				$caseQty = bcadd($caseQty, $cycleCountDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $cycleCountDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $cycleCountDtl->cubic_meter, 8);

				if($cycleCountDtl->handling_unit_id > 0)
				{
					$palletHashByHandingUnitId[$cycleCountDtl->handling_unit_id] = $cycleCountDtl->handling_unit_id;
				}

				unset($cycleCountDtl->storageBin);
				unset($cycleCountDtl->item);
			}
			
			$cycleCountHdr->pallet_qty = count($palletHashByHandingUnitId);
			$cycleCountHdr->case_qty = $caseQty;
			$cycleCountHdr->gross_weight = $grossWeight;
			$cycleCountHdr->cubic_meter = $cubicMeter;

			$cycleCountHdr->details = $cycleCountDtls->values()->all();

			unset($cycleCountHdr->cycleCountDtls);
		}
    	return $cycleCountHdrs;
	}

	protected function indexWhseJob160101($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
			array(
				'cycle_count_read'
			)
		);

		$whseJobHdrs = WhseJobHdrRepository::findAllDraftAndWIP($siteFlowId, ProcType::$MAP['WHSE_JOB_16_01'], $sorts, $filters, $pageSize);
		foreach($whseJobHdrs as $whseJobHdr)
		{
			$whseJobHdr = self::processOutgoingHeader($whseJobHdr, true);
			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$whseJobDtls = WhseJobDtlRepository::findAllByHdrId($whseJobHdr->id);
			$whseJobDtls->load('item', 'storageBin', 'handlingUnit', 'quantBal');

			$formatWhseJobDtls = array();
			foreach($whseJobDtls as $whseJobDtl)
			{
				$whseJobDtl = CycleCountService::processOutgoingDetail($whseJobDtl);

				$caseQty = bcadd($caseQty, $whseJobDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $whseJobDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $whseJobDtl->cubic_meter, 8);

				if($whseJobDtl->handling_unit_id > 0)
				{
					$palletHashByHandingUnitId[$whseJobDtl->handling_unit_id] = $whseJobDtl->handling_unit_id;
				}

				$formatWhseJobDtls[] = $whseJobDtl;
			}

			$whseJobHdr->pallet_qty = count($palletHashByHandingUnitId);
			$whseJobHdr->case_qty = $caseQty;
			$whseJobHdr->gross_weight = $grossWeight;
			$whseJobHdr->cubic_meter = $cubicMeter;

			$whseJobHdr->details = $formatWhseJobDtls;
		}
    	return $whseJobHdrs;
	}

	protected function indexWhseJob170201($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
			array(
				'cycle_count_read'
			)
		);

		$whseJobHdrs = WhseJobHdrRepository::findAllDraftAndWIP($siteFlowId, ProcType::$MAP['WHSE_JOB_17_02'], $sorts, $filters, $pageSize);
		foreach($whseJobHdrs as $whseJobHdr)
		{
			$whseJobHdr = self::processOutgoingHeader($whseJobHdr, true);
			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$whseJobDtls = WhseJobDtlRepository::findAllByHdrId($whseJobHdr->id);
			$whseJobDtls->load('item', 'storageBin', 'handlingUnit', 'quantBal');

			$formatWhseJobDtls = array();
			foreach($whseJobDtls as $whseJobDtl)
			{
				$whseJobDtl = CycleCountService::processOutgoingDetail($whseJobDtl);

				$caseQty = bcadd($caseQty, $whseJobDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $whseJobDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $whseJobDtl->cubic_meter, 8);

				if($whseJobDtl->handling_unit_id > 0)
				{
					$palletHashByHandingUnitId[$whseJobDtl->handling_unit_id] = $whseJobDtl->handling_unit_id;
				}

				$formatWhseJobDtls[] = $whseJobDtl;
			}

			$whseJobHdr->pallet_qty = count($palletHashByHandingUnitId);
			$whseJobHdr->case_qty = $caseQty;
			$whseJobHdr->gross_weight = $grossWeight;
			$whseJobHdr->cubic_meter = $cubicMeter;

			$whseJobHdr->details = $formatWhseJobDtls;
		}
    	return $whseJobHdrs;
	}

	protected function createWhseJob1501($procType, $hdrIds)
  	{
		AuthService::authorize(
			array(
				'whse_job_create'
			)
		);

		//1 putAway to 1 whseJob
		$whseJobHdrDataList = array();
		
		$putAwayHdrs = PutAwayHdrRepository::findAllByHdrIds($hdrIds);
		$putAwayHdrs->load('siteFlow');
		foreach($putAwayHdrs as $putAwayHdr) 
		{
			$dtlDataList = array();
			$lineNo = 1;
			$docTxnFlowDataList = array();

			$siteFlow = $putAwayHdr->siteFlow;

			$putAwayDtls = PutAwayDtlRepository::findAllByHdrId($putAwayHdr->id);
			$putAwayDtls->load('quantBal');
			foreach($putAwayDtls as $putAwayDtl)
			{
				$quantBal = $putAwayDtl->quantBal;

				$dtlData = array(
					'is_split' => 0,
					'whse_job_type' => $putAwayDtl->whse_job_type,
					'req_whse_job_hdr_id' => 0,
					'company_id' => $putAwayDtl->company_id,
					'doc_hdr_type' => \App\PutAwayHdr::class,
					'doc_hdr_id' => $putAwayDtl->hdr_id,
					'doc_dtl_type' => \App\PutAwayDtl::class,
					'doc_dtl_id' => $putAwayDtl->id,
					'line_no' => $lineNo,
					'desc_01' => $putAwayDtl->desc_01,
					'desc_02' => $putAwayDtl->desc_02,

					'item_cond_01_id' => $putAwayDtl->item_cond_01_id,
                    'item_cond_02_id' => $putAwayDtl->item_cond_02_id,
                    'item_cond_03_id' => $putAwayDtl->item_cond_03_id,
                    'item_cond_04_id' => $putAwayDtl->item_cond_04_id,
                    'item_cond_05_id' => $putAwayDtl->item_cond_05_id,

					'storage_bin_id' => $quantBal->storage_bin_id,
					'handling_unit_id' => $quantBal->handling_unit_id,
					'quant_bal_id' => $quantBal->id,
					'item_id' => $quantBal->item_id,

					'uom_id' => $putAwayDtl->uom_id,
					'uom_rate' => $putAwayDtl->uom_rate,
					'qty' => $putAwayDtl->qty,
					'doc_status' => DocStatus::$MAP['DRAFT'],

					'scan_mode' => 0,
					'to_storage_bin_id' => $putAwayDtl->to_storage_bin_id,
					'to_handling_unit_id' => $putAwayDtl->to_handling_unit_id,
				);
				$dtlDataList[] = $dtlData;

				$lineNo++;
			}

			//build the hdrData
			$hdrData = array(
				'ref_code_01' => '',
				'ref_code_02' => '',
				'doc_date' => date('Y-m-d'),
				'desc_01' => '',
				'desc_02' => '',
				'site_flow_id' => $siteFlow->id,
				'mobile_profile_id' => 0,
				'worker_01_id' => 0,
				'worker_02_id' => 0,
				'worker_03_id' => 0,
				'worker_04_id' => 0,
				'worker_05_id' => 0,
				'equipment_01_id' => 0,
				'equipment_02_id' => 0,
				'equipment_03_id' => 0,
				'equipment_04_id' => 0,
				'equipment_05_id' => 0
			);

			$tmpDocTxnFlowData = array(
				'fr_doc_hdr_type' => \App\PutAwayHdr::class,
				'fr_doc_hdr_id' => $putAwayHdr->id,
				'fr_doc_hdr_code' => $putAwayHdr->doc_code,
				'is_closed' => 1
			);
			$docTxnFlowDataList[] = $tmpDocTxnFlowData;

			$siteDocNo = SiteDocNoRepository::findSiteDocNo($siteFlow->site_id, \App\WhseJobHdr::class);
			if(empty($siteDocNo))
			{
				$exc = new ApiException(__('SiteDocNo.doc_no_not_found', ['siteId'=>$siteFlow->site_id, 'docType'=>\App\WhseJobHdr::class]));
				//$exc->addData(\App\SiteDocNo::class, $siteFlow->site_id);
				throw $exc;
			}

			$whseTxnFlow = WhseTxnFlowRepository::findByPk($siteFlow->id, $procType);
			
			//DRAFT
			$whseJobHdr = WhseJobHdrRepository::createProcess($procType, $siteDocNo->doc_no_id, $hdrData, $dtlDataList, $docTxnFlowDataList);

			if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
			{
				$whseJobHdr = self::transitionToWip($whseJobHdr->id);
			}
			elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
			{
				$whseJobHdr = self::transitionToComplete($whseJobHdr->id);
			}

			$whseJobHdr->str_doc_status = DocStatus::$MAP[$whseJobHdr->doc_status];
			$whseJobHdrDataList[] = $whseJobHdr;
		}

		$docCodeMsg = '';
		for ($a = 0; $a < count($whseJobHdrDataList); $a++)
		{
			$whseJobHdrData = $whseJobHdrDataList[$a];
			if($a == 0)
			{
				$docCodeMsg .= $whseJobHdrData->doc_code;
			}
			else
			{
				$docCodeMsg .= ', '.$whseJobHdrData->doc_code;
			}
		}
		$message = __('WhseJob.document_successfully_created', ['docCode'=>$docCodeMsg]);

		return array(
			'data' => $whseJobHdrDataList,
			'message' => $message
		);
	}

	protected function indexWhseJob1501($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
			array(
				'put_away_read'
			)
		);

		$putAwayHdrs = PutAwayHdrRepository::findAllNotExistWhseJob1501Txn($siteFlowId, $sorts, $filters, $pageSize);
		$putAwayHdrs->load('putAwayInbOrds');
		foreach($putAwayHdrs as $putAwayHdr)
		{
			$putAwayHdr = PutAwayService::processOutgoingHeader($putAwayHdr);

			//query the inbOrds
			$putAwayInbOrds = $putAwayHdr->putAwayInbOrds;
			$putAwayInbOrds->load('inbOrdHdr');
			for($a = 0; $a < count($putAwayInbOrds); $a++)
			{
				$putAwayInbOrd = $putAwayInbOrds[$a];
				$inbOrdHdr = $putAwayInbOrd->inbOrdHdr;
				if($a == 0)
				{
					$putAwayHdr->adv_ship_hdr_code = $inbOrdHdr->adv_ship_hdr_code;
					$putAwayHdr->ref_code_01 = $inbOrdHdr->ref_code_01;
					$putAwayHdr->ref_code_02 = $inbOrdHdr->ref_code_02;
					$putAwayHdr->ref_code_03 = $inbOrdHdr->ref_code_03;
					$putAwayHdr->ref_code_04 = $inbOrdHdr->ref_code_04;
					$putAwayHdr->ref_code_05 = $inbOrdHdr->ref_code_05;
					$putAwayHdr->ref_code_06 = $inbOrdHdr->ref_code_06;
					$putAwayHdr->ref_code_07 = $inbOrdHdr->ref_code_07;
					$putAwayHdr->ref_code_08 = $inbOrdHdr->ref_code_08;
				}
				else
				{
					$putAwayHdr->adv_ship_hdr_code .= '/'.$inbOrdHdr->adv_ship_hdr_code;
					$putAwayHdr->ref_code_01 .= '/'.$inbOrdHdr->ref_code_01;
					$putAwayHdr->ref_code_02 .= '/'.$inbOrdHdr->ref_code_02;
					$putAwayHdr->ref_code_03 .= '/'.$inbOrdHdr->ref_code_03;
					$putAwayHdr->ref_code_04 .= '/'.$inbOrdHdr->ref_code_04;
					$putAwayHdr->ref_code_05 .= '/'.$inbOrdHdr->ref_code_05;
					$putAwayHdr->ref_code_06 .= '/'.$inbOrdHdr->ref_code_06;
					$putAwayHdr->ref_code_07 .= '/'.$inbOrdHdr->ref_code_07;
					$putAwayHdr->ref_code_08 .= '/'.$inbOrdHdr->ref_code_08;
				}
			}

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$putAwayDtls = PutAwayDtlRepository::findAllByHdrId($putAwayHdr->id);
			$putAwayDtls->load('item', 'quantBal', 'toStorageBin', 'toStorageBin.storageBay', 'toStorageBin.storageRow');

			foreach($putAwayDtls as $putAwayDtl)
			{
				$putAwayDtl = PutAwayService::processOutgoingDetail($putAwayDtl);

				$caseQty = bcadd($caseQty, $putAwayDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $putAwayDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $putAwayDtl->cubic_meter, 8);

				if($putAwayDtl->handling_unit_id > 0)
				{
					$palletHashByHandingUnitId[$putAwayDtl->handling_unit_id] = $putAwayDtl->handling_unit_id;
				}

				unset($putAwayDtl->toStorageBin);
				unset($putAwayDtl->item);
			}
			
			$putAwayHdr->pallet_qty = count($palletHashByHandingUnitId);
			$putAwayHdr->case_qty = $caseQty;
			$putAwayHdr->gross_weight = $grossWeight;
			$putAwayHdr->cubic_meter = $cubicMeter;

			$putAwayHdr->details = $putAwayDtls;
		}
    	return $putAwayHdrs;
	}

	protected function indexWhseJob150101($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
			array(
				'put_away_read'
			)
		);

		$whseJobHdrs = WhseJobHdrRepository::findAllDraftAndWIP($siteFlowId, ProcType::$MAP['WHSE_JOB_15_01'], $sorts, $filters, $pageSize);
		$whseJobHdrs->load('frDocTxnFlows');
		foreach($whseJobHdrs as $whseJobHdr)
		{
			$whseJobHdr = self::processOutgoingHeader($whseJobHdr, true);

			//query the inbOrds
			$frDocTxnFlows = $whseJobHdr->frDocTxnFlows;
			$putAwayHdrIds = array();
			foreach($frDocTxnFlows as $frDocTxnFlow)
			{
				$putAwayHdrIds[] = $frDocTxnFlow->fr_doc_hdr_id;
			}
			$putAwayInbOrds = PutAwayInbOrdRepository::findAllByHdrIds($putAwayHdrIds);
			$putAwayInbOrds->load('inbOrdHdr');
			for($a = 0; $a < count($putAwayInbOrds); $a++)
			{
				$putAwayInbOrd = $putAwayInbOrds[$a];
				$inbOrdHdr = $putAwayInbOrd->inbOrdHdr;
				if($a == 0)
				{
					$whseJobHdr->adv_ship_hdr_code = $inbOrdHdr->adv_ship_hdr_code;
					$whseJobHdr->ref_code_01 = $inbOrdHdr->ref_code_01;
					$whseJobHdr->ref_code_02 = $inbOrdHdr->ref_code_02;
					$whseJobHdr->ref_code_03 = $inbOrdHdr->ref_code_03;
					$whseJobHdr->ref_code_04 = $inbOrdHdr->ref_code_04;
					$whseJobHdr->ref_code_05 = $inbOrdHdr->ref_code_05;
					$whseJobHdr->ref_code_06 = $inbOrdHdr->ref_code_06;
					$whseJobHdr->ref_code_07 = $inbOrdHdr->ref_code_07;
					$whseJobHdr->ref_code_08 = $inbOrdHdr->ref_code_08;
				}
				else
				{
					$whseJobHdr->adv_ship_hdr_code .= '/'.$inbOrdHdr->adv_ship_hdr_code;
					$whseJobHdr->ref_code_01 .= '/'.$inbOrdHdr->ref_code_01;
					$whseJobHdr->ref_code_02 .= '/'.$inbOrdHdr->ref_code_02;
					$whseJobHdr->ref_code_03 .= '/'.$inbOrdHdr->ref_code_03;
					$whseJobHdr->ref_code_04 .= '/'.$inbOrdHdr->ref_code_04;
					$whseJobHdr->ref_code_05 .= '/'.$inbOrdHdr->ref_code_05;
					$whseJobHdr->ref_code_06 .= '/'.$inbOrdHdr->ref_code_06;
					$whseJobHdr->ref_code_07 .= '/'.$inbOrdHdr->ref_code_07;
					$whseJobHdr->ref_code_08 .= '/'.$inbOrdHdr->ref_code_08;
				}
			}

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$whseJobDtls = WhseJobDtlRepository::findAllByHdrId($whseJobHdr->id);
			$whseJobDtls->load('item', 'storageBin', 'quantBal', 'quantBal.itemBatch', 'quantBal.storageBin', 'quantBal.storageRow', 'quantBal.storageBay');

			$formatWhseJobDtls = array();
			foreach($whseJobDtls as $whseJobDtl)
			{
				$whseJobDtl = PutAwayService::processOutgoingDetail($whseJobDtl, false);

				$caseQty = bcadd($caseQty, $whseJobDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $whseJobDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $whseJobDtl->cubic_meter, 8);

				if($whseJobDtl->handling_unit_id > 0)
				{
					$palletHashByHandingUnitId[$whseJobDtl->handling_unit_id] = $whseJobDtl->handling_unit_id;
				}
				unset($whseJobDtl->quantBal);
				unset($whseJobDtl->storageBin);
				unset($whseJobDtl->item);
				$formatWhseJobDtls[] = $whseJobDtl;
			}

			$whseJobHdr->pallet_qty = count($palletHashByHandingUnitId);
			$whseJobHdr->case_qty = $caseQty;
			$whseJobHdr->gross_weight = $grossWeight;
			$whseJobHdr->cubic_meter = $cubicMeter;

			$whseJobHdr->details = $formatWhseJobDtls;
		}
    	return $whseJobHdrs;
	}

	public function showHeader($hdrId)
    {
		AuthService::authorize(
			array(
				'whse_job_read'
			)
		);

        $model = WhseJobHdrRepository::findByPk($hdrId);
		if(!empty($model))
        {
            $model = self::processOutgoingHeader($model);
			
			unset($model->worker01);
        }

        return $model;
    }

    public function showDetails($strResType, $hdrId) 
	{
		AuthService::authorize(
			array(
				'whse_job_read'
			)
		);

		if(isset(ResType::$MAP[$strResType]))
		{
			if(ResType::$MAP[$strResType] == ResType::$MAP['BIN_TRF']) 
			{
				//bin transfer 
				$result = $this->showBinTrfDetails($hdrId);
				return $result;
			}
			elseif(ResType::$MAP[$strResType] == ResType::$MAP['GDS_RCPT'])
			{
				//goods receipt
				$result = $this->showGdsRcptDetails($hdrId);
				return $result;
			}
			elseif(ResType::$MAP[$strResType] == ResType::$MAP['PUT_AWAY'])
			{
				//goods receipt
				$result = $this->showPutAwayDetails($hdrId);
				return $result;
			}
			elseif(ResType::$MAP[$strResType] == ResType::$MAP['PICK_LIST'])
			{
				//pick list
				$result = $this->showPickListDetails($hdrId);
				return $result;
			}
			elseif(ResType::$MAP[$strResType] == ResType::$MAP['CYCLE_COUNT'])
			{
				//cycle count
				$result = $this->showCycleCountDetails($hdrId);
				return $result;
			}
		}
	}

	private function showBinTrfDetails($hdrId) 
	{
		$whseJobDtls = WhseJobDtlRepository::findAllByHdrId($hdrId);
		$whseJobDtls->load('item', 'storageBin', 'handlingUnit', 'quantBal', 'uom', 'toStorageBin', 'toHandlingUnit');
        foreach($whseJobDtls as $whseJobDtl)
        {
			$whseJobDtl = BinTrfService::processOutgoingDetail($whseJobDtl);
        }
        return $whseJobDtls;
	}

	private function showGdsRcptDetails($hdrId) 
	{
		$whseJobDtls = WhseJobDtlRepository::findAllByHdrId($hdrId);
		$whseJobDtls->load('item');
        foreach($whseJobDtls as $whseJobDtl)
        {
			$whseJobDtl = GdsRcptService::processOutgoingDetail($whseJobDtl);
        }
        return $whseJobDtls;
	}

	private function showPutAwayDetails($hdrId) 
	{
		$whseJobDtls = WhseJobDtlRepository::findAllByHdrId($hdrId);
		$whseJobDtls->load('item');
        foreach($whseJobDtls as $whseJobDtl)
        {
			$whseJobDtl = PutAwayService::processOutgoingDetail($whseJobDtl);
        }
        return $whseJobDtls;
	}

	private function showPickListDetails($hdrId) 
	{
		$whseJobDtls = WhseJobDtlRepository::findAllByHdrId($hdrId);
		$whseJobDtls->load('item');
        foreach($whseJobDtls as $whseJobDtl)
        {
			$whseJobDtl = PickListService::processOutgoingDetail($whseJobDtl);
        }
        return $whseJobDtls;
	}

	private function showCycleCountDetails($hdrId) 
	{
		$whseJobDtls = WhseJobDtlRepository::findAllByHdrId($hdrId);
		$whseJobDtls->load('item');
        foreach($whseJobDtls as $whseJobDtl)
        {
			$whseJobDtl = CycleCountService::processOutgoingDetail($whseJobDtl);
        }
        return $whseJobDtls;
	}
	
	public function updateHeader($hdrData)
	{
		AuthService::authorize(
			array(
				'whse_job_update'
			)
		);

        $hdrData = self::processIncomingHeader($hdrData);

		//get the associated inbOrdHdr
        $whseJobHdr = WhseJobHdrRepository::txnFindByPk($hdrData['id']);
        if($whseJobHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('WhseJob.doc_status_is_wip_or_complete', ['docCode'=>$whseJobHdr->doc_code]));
            $exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
            throw $exc;
        }
        $whseJobHdrData = $whseJobHdr->toArray();
        //assign hdrData to model
        foreach($hdrData as $field => $value) 
        {
            $whseJobHdrData[$field] = $value;
        }

		//$whseJobHdr->load('worker01');
        //$worker01 = $whseJobHdr->worker01;
		
		//no detail to update
        $whseJobDtlArray = array();

        $result = WhseJobHdrRepository::updateDetails($whseJobHdrData, $whseJobDtlArray, array(), array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];

        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			if(strcmp($dtlModel->doc_hdr_type, \App\CycleCountHdr::class) == 0)
			{
				$dtlModel = CycleCountService::processOutgoingDetail($dtlModel);
			}
			elseif(strcmp($dtlModel->doc_hdr_type, \App\BinTrfHdr::class) == 0)
			{
				$dtlModel = BinTrfService::processOutgoingDetail($dtlModel);
			}
			elseif(strcmp($dtlModel->doc_hdr_type, \App\PickListHdr::class) == 0)
			{
				$dtlModel = PickListService::processOutgoingDetail($dtlModel);
			}
			elseif(strcmp($dtlModel->doc_hdr_type, \App\PackListHdr::class) == 0)
			{
				$dtlModel = PackListService::processOutgoingDetail($dtlModel);
			}
			elseif(strcmp($dtlModel->doc_hdr_type, \App\LoadListHdr::class) == 0)
			{
				$dtlModel = LoadListService::processOutgoingDetail($dtlModel);
			}
			elseif(strcmp($dtlModel->doc_hdr_type, \App\GdsRcptHdr::class) == 0)
			{
				$dtlModel = GdsRcptService::processOutgoingDetail($dtlModel);
			}
			elseif(strcmp($dtlModel->doc_hdr_type, \App\PutAwayHdr::class) == 0)
			{
				$dtlModel = PutAwayService::processOutgoingDetail($dtlModel);
			}
            $documentDetails[] = $dtlModel;
        }

        $message = __('WhseJob.document_successfully_updated', ['docCode'=>$documentHeader->doc_code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function updateDetails($strResType, $hdrId, $dtlArray)
	{
		AuthService::authorize(
			array(
				'whse_job_update'
			)
		);

		if(isset(ResType::$MAP[$strResType]))
		{
			if(ResType::$MAP[$strResType] == ResType::$MAP['BIN_TRF']) 
			{
				//bin transfer 
				$result = $this->updateBinTrfDetails($hdrId, $dtlArray);
				return $result;
			}
			elseif(ResType::$MAP[$strResType] == ResType::$MAP['GDS_RCPT'])
			{
				//goods receipt 
				$result = $this->updateGdsRcptDetails($hdrId, $dtlArray);
				return $result;
			}
			elseif(ResType::$MAP[$strResType] == ResType::$MAP['PUT_AWAY'])
			{
				//goods receipt 
				$result = $this->updatePutAwayDetails($hdrId, $dtlArray);
				return $result;
			}
			elseif(ResType::$MAP[$strResType] == ResType::$MAP['PICK_LIST'])
			{
				//pick list 
				$result = $this->updatePickListDetails($hdrId, $dtlArray);
				return $result;
			}
			elseif(ResType::$MAP[$strResType] == ResType::$MAP['CYCLE_COUNT'])
			{
				//cycle count 
				$result = $this->updateCycleCountDetails($hdrId, $dtlArray);
				return $result;
			}
        }
	}
	
	public function updateBinTrfDetails($hdrId, $dtlArray)
	{
		//get the associated inbOrdHdr
        $whseJobHdr = WhseJobHdrRepository::txnFindByPk($hdrId);
        if($whseJobHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('WhseJob.doc_status_is_wip_or_complete', ['docCode'=>$whseJobHdr->doc_code]));
            $exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
            throw $exc;
        }
		$whseJobHdrData = $whseJobHdr->toArray();

		//query the whseJobDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$whseJobDtlArray = array();
        $whseJobDtlModels = WhseJobDtlRepository::findAllByHdrId($hdrId);
		foreach($whseJobDtlModels as $whseJobDtlModel)
		{
			$whseJobDtlData = $whseJobDtlModel->toArray();
			$whseJobDtlData['is_modified'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{                
				if($dtlData['id'] == $whseJobDtlData['id'])
				{
					$dtlData = BinTrfService::processIncomingDetail($dtlData);
					
					if($dtlData['uom_id'] == Uom::$PALLET
                    && bccomp($dtlData['uom_rate'], 0, 5) == 0)
                    {
					}
					else
					{
						$dtlData = $this->updateWarehouseItemUom($dtlData, 0, 0);
						if($dtlData['quant_bal_id'] == 0)
						{
						}
					}

					foreach($dtlData as $fieldName => $value)
					{
						$whseJobDtlData[$fieldName] = $value;
					}

					$whseJobDtlData['is_modified'] = 1;

					break;
				}
			}
			$whseJobDtlArray[] = $whseJobDtlData;
		}
		
		$result = WhseJobHdrRepository::updateDetails($whseJobHdrData, $whseJobDtlArray, array(), array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = BinTrfService::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('WhseJob.details_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function updateGdsRcptDetails($hdrId, $dtlArray)
	{
		//get the associated inbOrdHdr
        $whseJobHdr = WhseJobHdrRepository::txnFindByPk($hdrId);
        if($whseJobHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('WhseJob.doc_status_is_wip_or_complete', ['docCode'=>$whseJobHdr->doc_code]));
            $exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
            throw $exc;
        }
		$whseJobHdrData = $whseJobHdr->toArray();

		//query the whseJobDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$whseJobDtlArray = array();
        $whseJobDtlModels = WhseJobDtlRepository::findAllByHdrId($hdrId);
		foreach($whseJobDtlModels as $whseJobDtlModel)
		{
			$whseJobDtlData = $whseJobDtlModel->toArray();
			$whseJobDtlData['is_modified'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{                
				if($dtlData['id'] == $whseJobDtlData['id'])
				{
					$dtlData = GdsRcptService::processIncomingDetail($dtlData);
					
					if($dtlData['uom_id'] == Uom::$PALLET
                    && bccomp($dtlData['uom_rate'], 0, 5) == 0)
                    {
					}
					else
					{
						$dtlData = $this->updateWarehouseItemUom($dtlData, 0, 0);
						if($dtlData['quant_bal_id'] == 0)
						{
						}
					}

					foreach($dtlData as $fieldName => $value)
					{
						$whseJobDtlData[$fieldName] = $value;
					}

					$whseJobDtlData['is_modified'] = 1;

					break;
				}
			}
			$whseJobDtlArray[] = $whseJobDtlData;
		}
		
		$result = WhseJobHdrRepository::updateDetails($whseJobHdrData, $whseJobDtlArray, array(), array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = GdsRcptService::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('WhseJob.details_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function updateCycleCountDetails($hdrId, $dtlArray)
	{
        $whseJobHdr = WhseJobHdrRepository::txnFindByPk($hdrId);
        if($whseJobHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('WhseJob.doc_status_is_wip_or_complete', ['docCode'=>$whseJobHdr->doc_code]));
            $exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
            throw $exc;
        }
		$whseJobHdrData = $whseJobHdr->toArray();

		//query the whseJobDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$whseJobDtlArray = array();
        $whseJobDtlModels = WhseJobDtlRepository::findAllByHdrId($hdrId);
		foreach($whseJobDtlModels as $whseJobDtlModel)
		{
			$whseJobDtlData = $whseJobDtlModel->toArray();
			$whseJobDtlData['is_modified'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{                
				if($dtlData['id'] == $whseJobDtlData['id'])
				{
					$dtlData = CycleCountService::processIncomingDetail($dtlData);
					
					if($dtlData['uom_id'] == Uom::$PALLET
                    && bccomp($dtlData['uom_rate'], 0, 5) == 0)
                    {
					}
					else
					{
						$dtlData = $this->updateWarehouseItemUom($dtlData, 0, 0);
						if($dtlData['quant_bal_id'] == 0)
						{
						}
					}

					foreach($dtlData as $fieldName => $value)
					{
						$whseJobDtlData[$fieldName] = $value;
					}

					$whseJobDtlData['is_modified'] = 1;

					break;
				}
			}
			$whseJobDtlArray[] = $whseJobDtlData;
		}
		
		$result = WhseJobHdrRepository::updateDetails($whseJobHdrData, $whseJobDtlArray, array(), array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = CycleCountService::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('WhseJob.details_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function updatePutAwayDetails($hdrId, $dtlArray)
	{
		//get the associated inbOrdHdr
        $whseJobHdr = WhseJobHdrRepository::txnFindByPk($hdrId);
        if($whseJobHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('WhseJob.doc_status_is_wip_or_complete', ['docCode'=>$whseJobHdr->doc_code]));
            $exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
            throw $exc;
        }
		$whseJobHdrData = $whseJobHdr->toArray();

		//query the whseJobDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$whseJobDtlArray = array();
        $whseJobDtlModels = WhseJobDtlRepository::findAllByHdrId($hdrId);
		foreach($whseJobDtlModels as $whseJobDtlModel)
		{
			$whseJobDtlData = $whseJobDtlModel->toArray();
			$whseJobDtlData['is_modified'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{                
				if($dtlData['id'] == $whseJobDtlData['id'])
				{
					$dtlData = PutAwayService::processIncomingDetail($dtlData);

					foreach($dtlData as $fieldName => $value)
					{
						$whseJobDtlData[$fieldName] = $value;
					}

					$whseJobDtlData['is_modified'] = 1;

					break;
				}
			}
			$whseJobDtlArray[] = $whseJobDtlData;
		}
		
		$result = WhseJobHdrRepository::updateDetails($whseJobHdrData, $whseJobDtlArray, array(), array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = PutAwayService::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('WhseJob.details_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}
	
	static public function processIncomingHeader($data, $isClearHdrDiscTax=false)
    {
		if(array_key_exists('doc_flows', $data))
        {
            unset($data['doc_flows']);
        }
		if(array_key_exists('submit_action', $data))
        {
            unset($data['submit_action']);
        }
        //preprocess the select2 and dropDown
        if(array_key_exists('worker_01_select2', $data))
        {
            $data['worker_01_id'] = $data['worker_01_select2']['value'];
            unset($data['worker_01_select2']);
		}
		if(array_key_exists('worker_02_select2', $data))
        {
            $data['worker_02_id'] = $data['worker_02_select2']['value'];
            unset($data['worker_02_select2']);
		}
		if(array_key_exists('worker_03_select2', $data))
        {
            $data['worker_03_id'] = $data['worker_03_select2']['value'];
            unset($data['worker_03_select2']);
		}
		if(array_key_exists('worker_04_select2', $data))
        {
            $data['worker_04_id'] = $data['worker_04_select2']['value'];
            unset($data['worker_04_select2']);
		}
		if(array_key_exists('worker_05_select2', $data))
        {
            $data['worker_05_id'] = $data['worker_05_select2']['value'];
            unset($data['worker_05_select2']);
        }
        if(array_key_exists('str_doc_status', $data))
        {
            unset($data['str_doc_status']);
		}
		
		if(array_key_exists('worker01', $data))
        {
            unset($data['worker01']);
		}
		if(array_key_exists('worker02', $data))
        {
            unset($data['worker02']);
		}
		if(array_key_exists('worker03', $data))
        {
            unset($data['worker03']);
		}
		if(array_key_exists('worker04', $data))
        {
            unset($data['worker04']);
		}
		if(array_key_exists('worker05', $data))
        {
            unset($data['worker05']);
        }
        
        return $data;
	}
	
	public function changeQuantBal($hdrId, $quantBalId)
    {
		AuthService::authorize(
			array(
				'whse_job_update'
			)
		);

		$whseJobHdr = WhseJobHdrRepository::findByPk($hdrId);

		$results = array();
		$quantBal = QuantBalRepository::findByPk($quantBalId);
        if(!empty($quantBal))
        {
			$item = $quantBal->item;
			$itemBatch = $quantBal->itemBatch;
			$handlingUnit = $quantBal->handlingUnit;
			
			$quantBal = ItemService::processCaseLoose($quantBal, $item, 1);

			$results['item_id'] = $quantBal->item_id;
			$results['item_code'] = $quantBal->item_code;
			$results['item_desc_01'] = $quantBal->item_desc_01;
			$results['item_desc_02'] = $quantBal->item_desc_02;
			$results['desc_01'] = $quantBal->item_desc_01;
			$results['desc_02'] = $quantBal->item_desc_02;
			$results['case_qty'] = $quantBal->case_qty;
			$results['item_case_uom_code'] = $quantBal->item_case_uom_code;
            $results['batch_serial_no'] = empty($itemBatch) ? '' : $itemBatch->batch_serial_no;
            $results['expiry_date'] = empty($itemBatch) ? '' : $itemBatch->expiry_date;
            $results['receipt_date'] = empty($itemBatch) ? '' : $itemBatch->receipt_date;
            $results['handling_unit_barcode'] = empty($handlingUnit) ? '' : $handlingUnit->getBarcode();
			
		}

        return $results;
	}
	
	public function createDetail($strResType, $hdrId, $data)
	{
		AuthService::authorize(
			array(
				'whse_job_update'
			)
		);

        if(isset(ResType::$MAP[$strResType]))
		{
			if(ResType::$MAP[$strResType] == ResType::$MAP['BIN_TRF']) 
			{
				//bin transfer 
				$result = $this->createBinTrfDetail($hdrId, $data);
				return $result;
			}
			elseif(ResType::$MAP[$strResType] == ResType::$MAP['GDS_RCPT'])
			{
				//goods receipt
				$result = $this->createGdsRcptDetail($hdrId, $data);
				return $result;
			}
			elseif(ResType::$MAP[$strResType] == ResType::$MAP['PUT_AWAY'])
			{
				//put away
				$result = $this->createPutAwayDetail($hdrId, $data);
				return $result;
			}
			elseif(ResType::$MAP[$strResType] == ResType::$MAP['PICK_LIST'])
			{
				//pick list
				$result = $this->createPickListDetail($hdrId, $data);
				return $result;
			}
			elseif(ResType::$MAP[$strResType] == ResType::$MAP['CYCLE_COUNT'])
			{
				//pick list
				$result = $this->createCycleCountDetail($hdrId, $data);
				return $result;
			}
        }
	}

    public function createBinTrfDetail($hdrId, $data)
	{
		$data = BinTrfService::processIncomingDetail($data);
		if($data['uom_id'] == Uom::$PALLET
		&& bccomp($data['uom_rate'], 0, 5) == 0)
		{
		}
		else
		{
			$data = $this->updateWarehouseItemUom($data, 0, 0);
		}

        $whseJobHdr = WhseJobHdrRepository::txnFindByPk($hdrId);
        if($whseJobHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('WhseJob.doc_status_is_wip_or_complete', ['docCode'=>$whseJobHdr->doc_code]));
            $exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
            throw $exc;
        }
		$whseJobHdrData = $whseJobHdr->toArray();

		$whseJobDtlArray = array();
		$whseJobDtlModels = WhseJobDtlRepository::findAllByHdrId($hdrId);
		foreach($whseJobDtlModels as $whseJobDtlModel)
		{
			$whseJobDtlData = $whseJobDtlModel->toArray();
			$whseJobDtlData['is_modified'] = 0;
			$whseJobDtlArray[] = $whseJobDtlData;
		}
		$lastLineNo = count($whseJobDtlModels);

		//assign temp id
		$tmpUuid = uniqid();
		//append NEW here to make sure it will be insert in repository later
		$data['id'] = 'NEW'.$tmpUuid;
		$data['line_no'] = $lastLineNo + 1;
		$data['is_modified'] = 1;
		
		$whseJobDtlData = $this->initWhseJobDtlData($data);

		$whseJobDtlArray[] = $whseJobDtlData;

		$result = WhseJobHdrRepository::updateDetails($whseJobHdrData, $whseJobDtlArray, array(), array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = BinTrfService::processOutgoingDetail($dtlModel);
        }

        $message = __('WhseJob.detail_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}
	
	public function changeItemUom($hdrId, $itemId, $uomId)
    {
		AuthService::authorize(
			array(
				'whse_job_update'
			)
		);

        $whseJobHdr = WhseJobHdrRepository::findByPk($hdrId);

        $results = array();
        $itemUom = ItemUomRepository::findByItemIdAndUomId($itemId, $uomId);
        if(!empty($itemUom)
        && !empty($whseJobHdr))
        {
            $results['item_id'] = $itemUom->item_id;
            $results['uom_id'] = $itemUom->uom_id;
            $results['uom_rate'] = $itemUom->uom_rate;

			$results = $this->updateWarehouseItemUom($results, 0, 0);
        }
        return $results;
	}

	public function changeItemBatch($hdrId, $itemBatchId)
    {
		AuthService::authorize(
			array(
				'whse_job_update'
			)
		);

        $whseJobHdr = WhseJobHdrRepository::findByPk($hdrId);

        $results = array();
        $itemBatch = ItemBatchRepository::findByPk($itemBatchId);
        if(!empty($itemBatch)
        && !empty($whseJobHdr))
        {
            $results['batch_serial_no'] = $itemBatch->batch_serial_no;
            $results['expiry_date'] = $itemBatch->expiry_date;
            $results['receipt_date'] = $itemBatch->receipt_date;
        }
        return $results;
	}
	
	public function transitionToStatus($hdrId, $strDocStatus, $hdrData = null)
    {
        $hdrModel = null;
        if(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['WIP'])
        {
			if(!empty($hdrData))
			{
				$this->updateHeader($hdrData);
			}
            $hdrModel = self::transitionToWip($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['COMPLETE'])
        {
			if(!empty($hdrData))
			{
				$this->updateHeader($hdrData);
			}
            $hdrModel = self::transitionToComplete($hdrId, false);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['DRAFT'])
        {
            $hdrModel = self::transitionToDraft($hdrId, false);
		}
		elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['VOID'])
        {
            $hdrModel = self::transitionToVoid($hdrId);
        }
        
        $documentHeader = self::processOutgoingHeader($hdrModel);

        $message = __('WhseJob.document_successfully_transition', ['docCode'=>$documentHeader->doc_code,'strDocStatus'=>$strDocStatus]);
        
        return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>array(),
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function changeItem($hdrId, $itemId)
    {
		AuthService::authorize(
			array(
				'whse_job_update'
			)
		);

        $whseJobHdr = WhseJobHdrRepository::findByPk($hdrId);

        $results = array();
        $item = ItemRepository::findByPk($itemId);
        if(!empty($item)
        && !empty($whseJobHdr))
        {
            $results['desc_01'] = $item->desc_01;
            $results['desc_02'] = $item->desc_02;
        }
        return $results;
	}
	
	public function createGdsRcptDetail($hdrId, $data)
	{
		$data = GdsRcptService::processIncomingDetail($data);		
		$data = $this->updateWarehouseItemUom($data, 0, 0);

        $whseJobHdr = WhseJobHdrRepository::txnFindByPk($hdrId);
        if($whseJobHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('WhseJob.doc_status_is_wip_or_complete', ['docCode'=>$whseJobHdr->doc_code]));
            $exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
            throw $exc;
        }
		$whseJobHdrData = $whseJobHdr->toArray();

		$whseJobDtlArray = array();
		$whseJobDtlModels = WhseJobDtlRepository::findAllByHdrId($hdrId);
		foreach($whseJobDtlModels as $whseJobDtlModel)
		{
			$whseJobDtlData = $whseJobDtlModel->toArray();
			$whseJobDtlData['is_modified'] = 0;
			$whseJobDtlArray[] = $whseJobDtlData;
		}
		$lastLineNo = count($whseJobDtlModels);

		//assign temp id
		$tmpUuid = uniqid();
		//append NEW here to make sure it will be insert in repository later
		$data['id'] = 'NEW'.$tmpUuid;
		$data['line_no'] = $lastLineNo + 1;
		$data['is_modified'] = 1;
		
		$whseJobDtlData = $this->initWhseJobDtlData($data);

		$whseJobDtlArray[] = $whseJobDtlData;

		$result = WhseJobHdrRepository::updateDetails($whseJobHdrData, $whseJobDtlArray, array(), array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = GdsRcptService::processOutgoingDetail($dtlModel);
        }

        $message = __('WhseJob.detail_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function createPutAwayDetail($hdrId, $data)
	{
		$data = PutAwayService::processIncomingDetail($data);
		//$data = $this->updateWarehouseItemUom($data, 0, 0);

        $whseJobHdr = WhseJobHdrRepository::txnFindByPk($hdrId);
        if($whseJobHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('WhseJob.doc_status_is_wip_or_complete', ['docCode'=>$whseJobHdr->doc_code]));
            $exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
            throw $exc;
        }
		$whseJobHdrData = $whseJobHdr->toArray();

		$whseJobDtlArray = array();
		$whseJobDtlModels = WhseJobDtlRepository::findAllByHdrId($hdrId);
		foreach($whseJobDtlModels as $whseJobDtlModel)
		{
			$whseJobDtlData = $whseJobDtlModel->toArray();
			$whseJobDtlData['is_modified'] = 0;
			$whseJobDtlArray[] = $whseJobDtlData;
		}
		$lastLineNo = count($whseJobDtlModels);

		//assign temp id
		$tmpUuid = uniqid();
		//append NEW here to make sure it will be insert in repository later
		$data['id'] = 'NEW'.$tmpUuid;
		$data['line_no'] = $lastLineNo + 1;
		$data['is_modified'] = 1;
		
		$whseJobDtlData = $this->initWhseJobDtlData($data);

		$whseJobDtlArray[] = $whseJobDtlData;

		$result = WhseJobHdrRepository::updateDetails($whseJobHdrData, $whseJobDtlArray, array(), array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = PutAwayService::processOutgoingDetail($dtlModel);
        }

        $message = __('WhseJob.detail_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function deleteDetails($hdrId, $dtlArray)
	{
		AuthService::authorize(
			array(
				'whse_job_update'
			)
		);

        $whseJobHdr = WhseJobHdrRepository::txnFindByPk($hdrId);
        if($whseJobHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('WhseJob.doc_status_is_wip_or_complete', ['docCode'=>$whseJobHdr->doc_code]));
            $exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
            throw $exc;
        }
		$whseJobHdrData = $whseJobHdr->toArray();

		//query the whseJobDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$delWhseJobDtlArray = array();
		$whseJobDtlArray = array();
        $whseJobDtlModels = WhseJobDtlRepository::findAllByHdrId($hdrId);
        $lineNo = 1;
		foreach($whseJobDtlModels as $whseJobDtlModel)
		{
			$whseJobDtlData = $whseJobDtlModel->toArray();
			$whseJobDtlData['is_modified'] = 0;
			$whseJobDtlData['is_deleted'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{
				if($dtlData['id'] == $whseJobDtlData['id'])
				{
					//this is deleted dtl
					$whseJobDtlData['is_deleted'] = 1;
					break;
				}
			}
			if($whseJobDtlData['is_deleted'] > 0)
			{
				$delWhseJobDtlArray[] = $whseJobDtlData;
			}
			else
			{
                $whseJobDtlData['line_no'] = $lineNo;
                $whseJobDtlData['is_modified'] = 1;
                $whseJobDtlArray[] = $whseJobDtlData;
                $lineNo++;
			}
        }
		
		$result = WhseJobHdrRepository::updateDetails($whseJobHdrData, $whseJobDtlArray, $delWhseJobDtlArray, array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			if(strcmp($dtlModel->doc_hdr_type, \App\CycleCountHdr::class) == 0)
			{
				$dtlModel = CycleCountService::processOutgoingDetail($dtlModel);
			}
			elseif(strcmp($dtlModel->doc_hdr_type, \App\BinTrfHdr::class) == 0)
			{
				$dtlModel = BinTrfService::processOutgoingDetail($dtlModel);
			}
			elseif(strcmp($dtlModel->doc_hdr_type, \App\PickListHdr::class) == 0)
			{
				$dtlModel = PickListService::processOutgoingDetail($dtlModel);
			}
			elseif(strcmp($dtlModel->doc_hdr_type, \App\PackListHdr::class) == 0)
			{
				$dtlModel = PackListService::processOutgoingDetail($dtlModel);
			}
			elseif(strcmp($dtlModel->doc_hdr_type, \App\LoadListHdr::class) == 0)
			{
				$dtlModel = LoadListService::processOutgoingDetail($dtlModel);
			}
			elseif(strcmp($dtlModel->doc_hdr_type, \App\GdsRcptHdr::class) == 0)
			{
				$dtlModel = GdsRcptService::processOutgoingDetail($dtlModel);
			}
			elseif(strcmp($dtlModel->doc_hdr_type, \App\PutAwayHdr::class) == 0)
			{
				$dtlModel = PutAwayService::processOutgoingDetail($dtlModel);
			}
            $documentDetails[] = $dtlModel;
        }

        $message = __('WhseJob.details_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>$delWhseJobDtlArray
            ),
			'message' => $message
		);
	}
	
	public function createPickListDetail($hdrId, $data)
	{
		$data = PickListService::processIncomingDetail($data);
		//$data = $this->updateWarehouseItemUom($data, 0, 0);

        $whseJobHdr = WhseJobHdrRepository::txnFindByPk($hdrId);
        if($whseJobHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('WhseJob.doc_status_is_wip_or_complete', ['docCode'=>$whseJobHdr->doc_code]));
            $exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
            throw $exc;
        }
		$whseJobHdrData = $whseJobHdr->toArray();

		$whseJobDtlArray = array();
		$whseJobDtlModels = WhseJobDtlRepository::findAllByHdrId($hdrId);
		foreach($whseJobDtlModels as $whseJobDtlModel)
		{
			$whseJobDtlData = $whseJobDtlModel->toArray();
			$whseJobDtlData['is_modified'] = 0;
			$whseJobDtlArray[] = $whseJobDtlData;
		}
		$lastLineNo = count($whseJobDtlModels);

		//assign temp id
		$tmpUuid = uniqid();
		//append NEW here to make sure it will be insert in repository later
		$data['id'] = 'NEW'.$tmpUuid;
		$data['line_no'] = $lastLineNo + 1;
		$data['is_modified'] = 1;
		
		$whseJobDtlData = $this->initWhseJobDtlData($data);

		$whseJobDtlArray[] = $whseJobDtlData;

		$result = WhseJobHdrRepository::updateDetails($whseJobHdrData, $whseJobDtlArray, array(), array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = PickListService::processOutgoingDetail($dtlModel);
        }

        $message = __('WhseJob.detail_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function createCycleCountDetail($hdrId, $data)
	{
		$data = CycleCountService::processIncomingDetail($data);
		$data = $this->updateWarehouseItemUom($data, 0, 0);

        $whseJobHdr = WhseJobHdrRepository::txnFindByPk($hdrId);
        if($whseJobHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('WhseJob.doc_status_is_wip_or_complete', ['docCode'=>$whseJobHdr->doc_code]));
            $exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
            throw $exc;
        }
		$whseJobHdrData = $whseJobHdr->toArray();

		$whseJobDtlArray = array();
		$whseJobDtlModels = WhseJobDtlRepository::findAllByHdrId($hdrId);
		foreach($whseJobDtlModels as $whseJobDtlModel)
		{
			$whseJobDtlData = $whseJobDtlModel->toArray();
			$whseJobDtlData['is_modified'] = 0;
			$whseJobDtlArray[] = $whseJobDtlData;
		}
		$lastLineNo = count($whseJobDtlModels);

		//assign temp id
		$tmpUuid = uniqid();
		//append NEW here to make sure it will be insert in repository later
		$data['id'] = 'NEW'.$tmpUuid;
		$data['line_no'] = $lastLineNo + 1;
		$data['is_modified'] = 1;
		
		$whseJobDtlData = $this->initWhseJobDtlData($data);

		$whseJobDtlArray[] = $whseJobDtlData;

		$result = WhseJobHdrRepository::updateDetails($whseJobHdrData, $whseJobDtlArray, array(), array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = CycleCountService::processOutgoingDetail($dtlModel);
        }

        $message = __('WhseJob.detail_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function updatePickListDetails($hdrId, $dtlArray)
	{
        $whseJobHdr = WhseJobHdrRepository::txnFindByPk($hdrId);
        if($whseJobHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('WhseJob.doc_status_is_wip_or_complete', ['docCode'=>$whseJobHdr->doc_code]));
            $exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
            throw $exc;
        }
		$whseJobHdrData = $whseJobHdr->toArray();

		//query the whseJobDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$whseJobDtlArray = array();
        $whseJobDtlModels = WhseJobDtlRepository::findAllByHdrId($hdrId);
		foreach($whseJobDtlModels as $whseJobDtlModel)
		{
			$whseJobDtlData = $whseJobDtlModel->toArray();
			$whseJobDtlData['is_modified'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{                
				if($dtlData['id'] == $whseJobDtlData['id'])
				{
					$dtlData = PickListService::processIncomingDetail($dtlData);

					foreach($dtlData as $fieldName => $value)
					{
						$whseJobDtlData[$fieldName] = $value;
					}

					$whseJobDtlData['is_modified'] = 1;

					break;
				}
			}
			$whseJobDtlArray[] = $whseJobDtlData;
		}
		
		$result = WhseJobHdrRepository::updateDetails($whseJobHdrData, $whseJobDtlArray, array(), array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = PutAwayService::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('WhseJob.details_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	static public function transitionToVoid($hdrId)
    {
		AuthService::authorize(
			array(
				'whse_job_revert'
			)
		);

		//use transaction to make sure this is latest value
		$hdrModel = WhseJobHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('WhseJob.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\WhseJobHdr::class, $hdrModel->id);
            throw $exc;
		}

		//revert the document
		$hdrModel = WhseJobHdrRepository::revertDraftToVoid($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('WhseJob.commit_to_void_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\WhseJobHdr::class, $hdrModel->id);
        throw $exc;
	}
	
	protected function indexWhseJob030102($user, $siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
			array(
				'pick_list_read'
			)
		);

		$divisionIds = array();
		$userDivisions = $user->userDivisions;
		foreach($userDivisions as $userDivision)
		{
			$divisionIds[] = $userDivision->division_id;
		}

		//DB::connection()->enableQueryLog();
		$pickListHdrs = PickListHdrRepository::findAllExistWhseJob0301TxnAndWIP($divisionIds, $sorts, $filters, $pageSize);
		$pickListHdrs->load(
			'pickListDtls',
			'pickListDtls.item', 'pickListDtls.quantBal', 
			'pickListDtls.quantBal.itemBatch', 'pickListDtls.quantBal.storageBin', 
			'pickListDtls.quantBal.storageRow', 'pickListDtls.quantBal.storageBay',
			'toDocTxnFlows', 'toDocTxnFlows.toDocHdr'
		);
		//Log::error(DB::getQueryLog());
		foreach($pickListHdrs as $pickListHdr)
		{
			$pickListHdr->str_doc_status = DocStatus::$MAP[$pickListHdr->doc_status];
			//$pickListHdr = PickListService::processOutgoingHeader($pickListHdr, true);

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;

			$jobPickListDtls = array();
			//query the docDtls
			/*
			$pickListDtls = $pickListHdr->pickListDtls;

			
			foreach($pickListDtls as $pickListDtl)
			{
				$item = $pickListDtl->item;
				$quantBal = $pickListDtl->quantBal;
				$itemBatch = null;
				$storageBin = null;
				$storageRow = null;
				$storageBay = null;
				if(!empty($quantBal))
				{
					$itemBatch = $quantBal->itemBatch;
					$storageBin = $quantBal->storageBin;
					$storageRow = $quantBal->storageRow;
					$storageBay = $quantBal->storageBay;
				}

				$whse_job_key = $pickListDtl->whse_job_type.':'.$pickListDtl->whse_job_code;
				$pickListDtl->whse_job_key = $whse_job_key;
				$pickListDtl->str_whse_job_type = WhseJobType::$MAP[$pickListDtl->whse_job_type];
				$pickListDtl->str_whse_job_type = __('WhseJob.'.strtolower($pickListDtl->str_whse_job_type));

				$pickListDtl->batch_serial_no = '';
				$pickListDtl->expiry_date = '';
				$pickListDtl->receipt_date = '';
				if(!empty($itemBatch))
				{
					$pickListDtl->batch_serial_no = $itemBatch->batch_serial_no;
					$pickListDtl->expiry_date = $itemBatch->expiry_date;
					$pickListDtl->receipt_date = $itemBatch->receipt_date;
				}

				$pickListDtl->storage_bin_code = '';
				if(!empty($storageBin))
				{
					$pickListDtl->storage_bin_code = $storageBin->code;
				}
				$pickListDtl->storage_row_code = '';
				if(!empty($storageRow))
				{
					$pickListDtl->storage_row_code = $storageRow->code;
				}
				$pickListDtl->storage_bay_code = '';
				$pickListDtl->item_bay_sequence = 0;
				if(!empty($storageBay))
				{
					$pickListDtl->storage_bay_code = $storageBay->code;
					$pickListDtl->item_bay_sequence = $storageBay->bay_sequence;
				}

				//calculate the pallet qty, case qty, gross weight, and m3
				if($pickListDtl->uom_id == Uom::$PALLET
        		&& bccomp($pickListDtl->uom_rate, 0, 5) == 0)
				{
					//this is pallet picking
					$pickListDtl->ttl_unit_qty = 0;
					$pickListDtl->case_qty = 0;
					$pickListDtl->gross_weight = 0;
					$pickListDtl->cubic_meter = 0;

					$handlingQuantBals = QuantBalRepository::findAllByHandlingUnitId($quantBal->handling_unit_id);
					$handlingQuantBals->load('item');
					foreach($handlingQuantBals as $handlingQuantBal)
					{
						$handlingItem = $handlingQuantBal->item;

						$handlingQuantBal = ItemService::processCaseLoose($handlingQuantBal, $handlingItem);

						$pickListDtl->unit_qty = bcadd($pickListDtl->unit_qty, $handlingQuantBal->balance_unit_qty, 10);
						$pickListDtl->case_qty = bcadd($pickListDtl->case_qty, $handlingQuantBal->case_qty, 10);
						$pickListDtl->gross_weight = bcadd($pickListDtl->gross_weight, $handlingQuantBal->gross_weight, 10);
						$pickListDtl->cubic_meter = bcadd($pickListDtl->cubic_meter, $handlingQuantBal->cubic_meter, 10);
					}

					$caseQty = bcadd($caseQty, $pickListDtl->case_qty, 8);
					$grossWeight = bcadd($grossWeight, $pickListDtl->gross_weight, 8);
					$cubicMeter = bcadd($cubicMeter, $pickListDtl->cubic_meter, 8);
				}
				else
				{
					$pickListDtl = ItemService::processCaseLoose($pickListDtl, $item);

					$caseQty = bcadd($caseQty, $pickListDtl->case_qty, 8);
					$grossWeight = bcadd($grossWeight, $pickListDtl->gross_weight, 8);
					$cubicMeter = bcadd($cubicMeter, $pickListDtl->cubic_meter, 8);
				}

				if($pickListDtl->handling_unit_id > 0)
				{
					$palletHashByHandingUnitId[$pickListDtl->handling_unit_id] = $pickListDtl->handling_unit_id;
				}

				unset($pickListDtl->quantBal);
				unset($pickListDtl->item);
				$jobPickListDtls[] = $pickListDtl;
			}
			*/

			$deliveryPointHash = array();
			$areaHash = array();
			$salesmanHash = array();
			$outbOrdHdrHash = array();

			$pickListOutbOrds = $pickListHdr->pickListOutbOrds;
			foreach($pickListOutbOrds as $pickListOutbOrd)
			{
				$outbOrdHdr = $pickListOutbOrd->outbOrdHdr;

				$deliveryPoint = $outbOrdHdr->deliveryPoint;
				$deliveryPointHash[$deliveryPoint->id] = $deliveryPoint;

				$area = $deliveryPoint->area;
				$areaHash[$area->id] = $area;

				$salesman = $outbOrdHdr->salesman;
				$salesmanHash[$salesman->id] = $salesman;

				$outbOrdHdrHash[$outbOrdHdr->id] = $outbOrdHdr;
			}

			$whseJobHdrs = array();
			$toDocTxnFlows = $pickListHdr->toDocTxnFlows;
			foreach($toDocTxnFlows as $toDocTxnFlow)
			{
				if(strcmp($toDocTxnFlow->to_doc_hdr_type, \App\WhseJobHdr::class) != 0)
				{
					//skip if this is not whseJobHdr
					continue;
				}


				$whseJobHdr = $toDocTxnFlow->toDocHdr;
				$whseJobHdr = self::processOutgoingHeader($whseJobHdr, true);
				$pickListHdr->print_count = $whseJobHdr->print_count;
				$pickListHdr->first_printed_at = $whseJobHdr->first_printed_at;
				$pickListHdr->last_printed_at = $whseJobHdr->last_printed_at;

				$whseJobDtl = WhseJobDtlRepository::findTopByHdrId($whseJobHdr->id);
				$whseJobHdr->whse_job_type_desc = '';
				if(!empty($whseJobDtl))
				{
					$whseJobDtl->str_whse_job_type = WhseJobType::$MAP[$whseJobDtl->whse_job_type];
					$whseJobHdr->whse_job_type_label = __('WhseJobType.'.strtolower($whseJobDtl->str_whse_job_type).'_label');
					$whseJobHdr->whse_job_type_desc = __('WhseJobType.'.strtolower($whseJobDtl->str_whse_job_type).'_desc');
				}

				$whseJobHdrs[] = $whseJobHdr;
			}

			$pickListHdr->delivery_points = array_values($deliveryPointHash);
			$pickListHdr->areas = array_values($areaHash);
			$pickListHdr->salesmans = array_values($salesmanHash);
			$pickListHdr->outb_ord_hdrs = array_values($outbOrdHdrHash);

			$pickListHdr->whse_job_hdrs = $whseJobHdrs;
			$pickListHdr->pallet_qty = count($palletHashByHandingUnitId);
			$pickListHdr->case_qty = $caseQty;
			$pickListHdr->gross_weight = $grossWeight;
			$pickListHdr->cubic_meter = $cubicMeter;

			$pickListHdr->details = $jobPickListDtls;
		}
    	return $pickListHdrs;
	}

	protected function indexWhseJob1702($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
			array(
				'bin_trf_read'
			)
		);

		$binTrfHdrs = BinTrfHdrRepository::findAllNotExistWhseJob1702Txn($siteFlowId, $sorts, $filters, $pageSize);
		$binTrfHdrs->load(
			'binTrfDtls',
			'binTrfDtls.item', 'binTrfDtls.storageBin', 
			'binTrfDtls.storageBin.storageBay', 
			'binTrfDtls.storageBin.storageRow'
		);
		foreach($binTrfHdrs as $binTrfHdr)
		{
			$binTrfHdr->str_doc_status = DocStatus::$MAP[$binTrfHdr->doc_status];

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$binTrfDtls = $binTrfHdr->binTrfDtls;

			foreach($binTrfDtls as $binTrfDtl)
			{
				$binTrfDtl = BinTrfService::processOutgoingDetail($binTrfDtl);

				$caseQty = bcadd($caseQty, $binTrfDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $binTrfDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $binTrfDtl->cubic_meter, 8);

				if($binTrfDtl->handling_unit_id > 0)
				{
					$palletHashByHandingUnitId[$binTrfDtl->handling_unit_id] = $binTrfDtl->handling_unit_id;
				}

				unset($binTrfDtl->toStorageBin);
				unset($binTrfDtl->item);
			}
			
			$binTrfHdr->pallet_qty = count($palletHashByHandingUnitId);
			$binTrfHdr->case_qty = $caseQty;
			$binTrfHdr->gross_weight = $grossWeight;
			$binTrfHdr->cubic_meter = $cubicMeter;

			$binTrfHdr->details = $binTrfDtls;
		}
    	return $binTrfHdrs;
	}

	protected function createWhseJob1702($procType, $hdrIds)
  	{		
		AuthService::authorize(
			array(
				'whse_job_create'
			)
		);

		//1 binTrf to 1 whseJob
		$whseJobHdrDataList = array();
		
		$binTrfHdrs = BinTrfHdrRepository::findAllByHdrIds($hdrIds);
		$binTrfHdrs->load(
			'siteFlow', 'binTrfDtls'
		);
		foreach($binTrfHdrs as $binTrfHdr) 
		{
			$dtlDataList = array();
			$lineNo = 1;
			$docTxnFlowDataList = array();

			$siteFlow = $binTrfHdr->siteFlow;

			$binTrfDtls = $binTrfHdr->binTrfDtls;
			foreach($binTrfDtls as $binTrfDtl)
			{
				$dtlData = array(
					'is_split' => 0,
					'whse_job_type' => $binTrfDtl->whse_job_type,
					'req_whse_job_hdr_id' => 0,
					'company_id' => $binTrfDtl->company_id,
					'doc_hdr_type' => \App\BinTrfHdr::class,
					'doc_hdr_id' => $binTrfDtl->hdr_id,
					'doc_dtl_type' => \App\BinTrfDtl::class,
					'doc_dtl_id' => $binTrfDtl->id,
					'line_no' => $lineNo,
					'desc_01' => $binTrfDtl->desc_01,
					'desc_02' => $binTrfDtl->desc_02,

					'item_cond_01_id' => 0,
                    'item_cond_02_id' => 0,
                    'item_cond_03_id' => 0,
                    'item_cond_04_id' => 0,
                    'item_cond_05_id' => 0,

					'storage_bin_id' => $binTrfDtl->storage_bin_id,
					'handling_unit_id' => $binTrfDtl->handling_unit_id,
					'quant_bal_id' => $binTrfDtl->id,
					'item_id' => $binTrfDtl->item_id,
					'batch_serial_no' => $binTrfDtl->batch_serial_no,
					'expiry_date' => $binTrfDtl->expiry_date,
					'receipt_date' => $binTrfDtl->receipt_date,

					'uom_id' => $binTrfDtl->uom_id,
					'uom_rate' => $binTrfDtl->uom_rate,
					'qty' => $binTrfDtl->qty,

					'scan_mode' => 0,
					'to_storage_bin_id' => $binTrfDtl->to_storage_bin_id,
					'to_handling_unit_id' => $binTrfDtl->to_handling_unit_id,
				);
				$dtlDataList[] = $dtlData;

				$lineNo++;
			}

			//build the hdrData
			$hdrData = array(
				'ref_code_01' => '',
				'ref_code_02' => '',
				'doc_date' => date('Y-m-d'),
				'desc_01' => '',
				'desc_02' => '',
				'site_flow_id' => $siteFlow->id,
				'mobile_profile_id' => 0,
				'worker_01_id' => 0,
				'worker_02_id' => 0,
				'worker_03_id' => 0,
				'worker_04_id' => 0,
				'worker_05_id' => 0,
				'equipment_01_id' => 0,
				'equipment_02_id' => 0,
				'equipment_03_id' => 0,
				'equipment_04_id' => 0,
				'equipment_05_id' => 0
			);

			$tmpDocTxnFlowData = array(
				'fr_doc_hdr_type' => \App\BinTrfHdr::class,
				'fr_doc_hdr_id' => $binTrfHdr->id,
				'fr_doc_hdr_code' => $binTrfHdr->doc_code,
				'is_closed' => 1
			);
			$docTxnFlowDataList[] = $tmpDocTxnFlowData;

			$siteDocNo = SiteDocNoRepository::findSiteDocNo($siteFlow->site_id, \App\WhseJobHdr::class);
			if(empty($siteDocNo))
			{
				$exc = new ApiException(__('SiteDocNo.doc_no_not_found', ['siteId'=>$siteFlow->site_id, 'docType'=>\App\WhseJobHdr::class]));
				//$exc->addData(\App\SiteDocNo::class, $siteFlow->site_id);
				throw $exc;
			}

			$whseTxnFlow = WhseTxnFlowRepository::findByPk($siteFlow->id, $procType);
			
			//DRAFT
			$whseJobHdr = WhseJobHdrRepository::createProcess($procType, $siteDocNo->doc_no_id, $hdrData, $dtlDataList, $docTxnFlowDataList);

			if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
			{
				$whseJobHdr = self::transitionToWip($whseJobHdr->id);
			}
			elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
			{
				$whseJobHdr = self::transitionToComplete($whseJobHdr->id);
			}

			$whseJobHdr->str_doc_status = DocStatus::$MAP[$whseJobHdr->doc_status];
			$whseJobHdrDataList[] = $whseJobHdr;
		}

		$docCodeMsg = '';
		for ($a = 0; $a < count($whseJobHdrDataList); $a++)
		{
			$whseJobHdrData = $whseJobHdrDataList[$a];
			if($a == 0)
			{
				$docCodeMsg .= $whseJobHdrData->doc_code;
			}
			else
			{
				$docCodeMsg .= ', '.$whseJobHdrData->doc_code;
			}
		}
		$message = __('WhseJob.document_successfully_created', ['docCode'=>$docCodeMsg]);

		return array(
			'data' => $whseJobHdrDataList,
			'message' => $message
		);
	}

	public function printWhseJob170201($siteFlowId, $user, $hdrIds)
	{
		AuthService::authorize(
			array(
				'bin_trf_print'
			)
		);

		$docHdrType = \App\WhseJobHdr::class.'17';
		$printDocSetting = PrintDocSettingRepository::findByDocHdrTypeAndSiteFlowId($docHdrType, $siteFlowId);
		if(empty($printDocSetting))
		{
			$exc = new ApiException(__('PrintDoc.report_template_not_found', ['docType'=>\App\WhseJobHdr::class]));
			$exc->addData(\App\PrintDocSetting::class, \App\WhseJobHdr::class);
			throw $exc;
		}

		$whseJobHdrs = WhseJobHdrRepository::findAllByIds($hdrIds);
		$printDocTxnDataArray = array();
		foreach($whseJobHdrs as $whseJobHdr)
		{
			$whseJobHdr->view_name = $printDocSetting->view_name;

			$printDocTxnData = array();
			$printDocTxnData['doc_hdr_type'] = \App\WhseJobHdr::class;
			$printDocTxnData['doc_hdr_id'] = $whseJobHdr->id;
			$printDocTxnData['user_id'] = $user->id;
			$printDocTxnDataArray[] = $printDocTxnData;
		}
		
		foreach($whseJobHdrs as $whseJobHdr)
		{
			$whseJobHdr = self::processOutgoingHeader($whseJobHdr, false);
			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$whseJobDtls = WhseJobDtlRepository::findAllByHdrId($whseJobHdr->id);
			$whseJobDtls->load('item', 'storageBin', 'toStorageBin', 'quantBal', 'quantBal.itemBatch', 'quantBal.storageBin', 'quantBal.storageRow', 'quantBal.storageBay');

			$formatWhseJobDtls = array();
			foreach($whseJobDtls as $whseJobDtl)
			{
				$whseJobDtl = BinTrfService::processOutgoingDetail($whseJobDtl, false);

				$caseQty = bcadd($caseQty, $whseJobDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $whseJobDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $whseJobDtl->cubic_meter, 8);

				if($whseJobDtl->handling_unit_id > 0)
				{
					$palletHashByHandingUnitId[$whseJobDtl->handling_unit_id] = $whseJobDtl->handling_unit_id;
				}

				$whseJobDtl->str_whse_job_type = WhseJobType::$MAP[$whseJobDtl->whse_job_type];
				$whseJobDtl->whse_job_type_label = __('WhseJobType.'.strtolower($whseJobDtl->str_whse_job_type).'_label');

				unset($whseJobDtl->quantBal);
				unset($whseJobDtl->storageBin);
				unset($whseJobDtl->item);
				$formatWhseJobDtls[] = $whseJobDtl;
			}

			$whseJobHdr->pallet_qty = count($palletHashByHandingUnitId);
			$whseJobHdr->case_qty = $caseQty;
			$whseJobHdr->gross_weight = $grossWeight;
			$whseJobHdr->cubic_meter = $cubicMeter;

			usort($formatWhseJobDtls, function ($a, $b) {
				if($a->item_code == $b->item_code)
				{
					return ($a->case_qty < $b->case_qty) ? 1 : -1;
				}
				return ($a->item_code < $b->item_code) ? -1 : 1;
			});
			$whseJobHdr->details = $formatWhseJobDtls;
			$whseJobHdr->barcode = '11'.str_pad($whseJobHdr->id, 5, '0', STR_PAD_LEFT);
		}

		$printedAt = PrintDocTxnRepository::createProcess($printDocTxnDataArray);

		$pdf = \Illuminate\Support\Facades\App::make('dompdf.wrapper');
		$pdf->getDomPDF()->set_option("enable_php", true);
		$pdf->setPaper(array(
			0,
			0,
			$printDocSetting->page_width, 
			$printDocSetting->page_height
		), $printDocSetting->page_orientation);

		//each data must have the view_name
		$pdf->loadHTML(
			view('batchPrintDocuments',
				array(
					'title' => 'Bin Trf Whse Job',
					'data' => $whseJobHdrs, 
					'printedAt' => $printedAt,
					'printedBy' => $user,
					'isPageBreakAfter' => true,
				)
			)
		);

		return $pdf->stream();
	}

	public function printWhseJob160102($siteFlowId, $user, $hdrIds)
	{
		AuthService::authorize(
			array(
				'cycle_count_print'
			)
		);

		$docHdrType = \App\WhseJobHdr::class.'160102';
		$printDocSetting = PrintDocSettingRepository::findByDocHdrTypeAndSiteFlowId($docHdrType, $siteFlowId);
		if(empty($printDocSetting))
		{
			$exc = new ApiException(__('PrintDoc.report_template_not_found', ['docType'=>\App\WhseJobHdr::class]));
			$exc->addData(\App\PrintDocSetting::class, \App\WhseJobHdr::class);
			throw $exc;
		}

		$printDocTxnDataArray = array();
		$cycleCountHdrs = CycleCountHdrRepository::findAllByHdrIds($hdrIds);
		$cycleCountHdrs->load(
			'toDocTxnFlows', 'toDocTxnFlows.toDocHdr'
		);		
		foreach($cycleCountHdrs as $cycleCountHdr)
		{
			$cycleCountHdr->view_name = $printDocSetting->view_name;
			
			//calculate the case_qty, gross_weight and cubic_meter
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;

			$whseJobHdrArray = array();

			$toDocTxnFlows = $cycleCountHdr->toDocTxnFlows;
			foreach($toDocTxnFlows as $toDocTxnFlow)
			{
				if(strcmp($toDocTxnFlow->to_doc_hdr_type, \App\WhseJobHdr::class) != 0)
				{
					//skip if this is not whseJobHdr
					continue;
				}

				$whseJobHdr = $toDocTxnFlow->toDocHdr;

				$printDocTxnData = array();
				$printDocTxnData['doc_hdr_type'] = \App\WhseJobHdr::class;
				$printDocTxnData['doc_hdr_id'] = $whseJobHdr->id;
				$printDocTxnData['user_id'] = $user->id;
				$printDocTxnDataArray[] = $printDocTxnData;

				$whseJobHdr = self::processOutgoingHeader($whseJobHdr, false);
				//query the docDtls
				$whseJobDtls = WhseJobDtlRepository::findAllByHdrId($whseJobHdr->id);
				$whseJobDtls->load(
					'item', 'storageBin', 
					'quantBal', 'quantBal.itemBatch', 
					'quantBal.storageBin', 'quantBal.storageRow', 
					'quantBal.storageBay'
				);

				foreach($whseJobDtls as $whseJobDtl)
				{
					$whseJobDtl = CycleCountService::processOutgoingDetail($whseJobDtl, true);

					$whseJobDtl->str_whse_job_type = WhseJobType::$MAP[$whseJobDtl->whse_job_type];
					$whseJobDtl->whse_job_type_label = __('WhseJobType.'.strtolower($whseJobDtl->str_whse_job_type).'_label');
					$whseJobDtl->whse_job_type_desc = __('WhseJobType.'.strtolower($whseJobDtl->str_whse_job_type).'_desc');
					$whseJobTypeHash[$whseJobDtl->whse_job_type_desc] = $whseJobDtl->whse_job_type_desc;

					$caseQty = bcadd($caseQty, $whseJobDtl->case_qty, 8);
					$grossWeight = bcadd($grossWeight, $whseJobDtl->gross_weight, 8);
					$cubicMeter = bcadd($cubicMeter, $whseJobDtl->cubic_meter, 8);
					
					unset($whseJobDtl->quantBal);
					unset($whseJobDtl->storageBin);
					unset($whseJobDtl->item);
					$formatWhseJobDtls[] = $whseJobDtl;
				}

				usort($formatWhseJobDtls, function ($a, $b) {
					//return ($a->item_bay_sequence < $b->item_bay_sequence) ? -1 : 1;
					return ($a->storage_bin_code < $b->storage_bin_code) ? -1 : 1;
				});

				$whseJobHdr->barcode = '11'.str_pad($whseJobHdr->id, 6, '0', STR_PAD_LEFT);	
				$whseJobHdr->details = $formatWhseJobDtls;

				$whseJobHdrArray[] = $whseJobHdr;
			}
			
			$cycleCountHdr->whse_job_hdrs = $whseJobHdrArray;
			$cycleCountHdr->case_qty = $caseQty;
			$cycleCountHdr->gross_weight = $grossWeight;
			$cycleCountHdr->cubic_meter = $cubicMeter;
		}

		$printedAt = PrintDocTxnRepository::createProcess($printDocTxnDataArray);

		$pdf = \Illuminate\Support\Facades\App::make('dompdf.wrapper');
		$pdf->getDomPDF()->set_option("enable_php", true);
		$pdf->setPaper(array(
			0,
			0,
			$printDocSetting->page_width, 
			$printDocSetting->page_height
		), $printDocSetting->page_orientation);

		//each data must have the view_name
		$pdf->loadHTML(
			view('batchPrintDocuments',
				array(
					'title' => 'Cycle Count Whse Job',
					'data' => $cycleCountHdrs, 
					'printedAt' => $printedAt,
					'printedBy' => $user,
					'isPageBreakAfter' => true,
				)
			)
		);

		return $pdf->stream();
	}

	protected function indexWhseJob160102($user, $siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
			array(
				'cycle_count_read'
			)
		);

		//DB::connection()->enableQueryLog();
		$cycleCountHdrs = CycleCountHdrRepository::findAllExistWhseJob1601TxnAndWIP($sorts, $filters, $pageSize);
		$cycleCountHdrs->load(
			'toDocTxnFlows', 'toDocTxnFlows.toDocHdr'
		);		
		foreach($cycleCountHdrs as $cycleCountHdr)
		{			
			//calculate the case_qty, gross_weight and cubic_meter
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;

			$whseJobHdrArray = array();

			$toDocTxnFlows = $cycleCountHdr->toDocTxnFlows;
			foreach($toDocTxnFlows as $toDocTxnFlow)
			{
				if(strcmp($toDocTxnFlow->to_doc_hdr_type, \App\WhseJobHdr::class) != 0)
				{
					//skip if this is not whseJobHdr
					continue;
				}

				$whseJobHdr = $toDocTxnFlow->toDocHdr;

				$whseJobHdr = self::processOutgoingHeader($whseJobHdr, false);
				$formatWhseJobDtls = array();
				/*
				//query the docDtls
				$whseJobDtls = WhseJobDtlRepository::findAllByHdrId($whseJobHdr->id);
				$whseJobDtls->load(
					'item', 'storageBin', 
					'quantBal', 'quantBal.itemBatch', 
					'quantBal.storageBin', 'quantBal.storageRow', 
					'quantBal.storageBay'
				);

				foreach($whseJobDtls as $whseJobDtl)
				{
					$whseJobDtl = CycleCountService::processOutgoingDetail($whseJobDtl, true);

					$whseJobDtl->str_whse_job_type = WhseJobType::$MAP[$whseJobDtl->whse_job_type];
					$whseJobDtl->whse_job_type_label = __('WhseJobType.'.strtolower($whseJobDtl->str_whse_job_type).'_label');
					$whseJobDtl->whse_job_type_desc = __('WhseJobType.'.strtolower($whseJobDtl->str_whse_job_type).'_desc');
					$whseJobTypeHash[$whseJobDtl->whse_job_type_desc] = $whseJobDtl->whse_job_type_desc;

					$caseQty = bcadd($caseQty, $whseJobDtl->case_qty, 8);
					$grossWeight = bcadd($grossWeight, $whseJobDtl->gross_weight, 8);
					$cubicMeter = bcadd($cubicMeter, $whseJobDtl->cubic_meter, 8);
					
					unset($whseJobDtl->quantBal);
					unset($whseJobDtl->storageBin);
					unset($whseJobDtl->item);
					$formatWhseJobDtls[] = $whseJobDtl;
				}

				usort($formatWhseJobDtls, function ($a, $b) {
					//return ($a->item_bay_sequence < $b->item_bay_sequence) ? -1 : 1;
					return ($a->storage_bin_code < $b->storage_bin_code) ? -1 : 1;
				});
				*/

				$whseJobHdr->barcode = '11'.str_pad($whseJobHdr->id, 6, '0', STR_PAD_LEFT);	
				$whseJobHdr->details = $formatWhseJobDtls;

				$whseJobHdrArray[] = $whseJobHdr;
			}
			
			$cycleCountHdr->whse_job_hdrs = $whseJobHdrArray;
			$cycleCountHdr->case_qty = $caseQty;
			$cycleCountHdr->gross_weight = $grossWeight;
			$cycleCountHdr->cubic_meter = $cubicMeter;
		}
    	return $cycleCountHdrs;
	}
}