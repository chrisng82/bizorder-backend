<?php

namespace App\Services;

use App\Services\Env\ProcType;
use App\Services\Env\DocStatus;
use App\Services\Env\WhseJobType;
use App\Repositories\OutbOrdHdrRepository;
use App\Repositories\OutbOrdDtlRepository;
use App\Repositories\LoadListDtlRepository;
use App\Repositories\QuantBalTxnRepository;
use App\Repositories\FailDelHdrRepository;

class FailDelService 
{
	/**
	 */
	public function __construct() 
	{
    }

    public function indexProcess($strProcType, $siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['FAIL_DEL_01']) 
			{
				//OutbOrd, LoadList -> FailDel
				$outbOrdHdrs = $this->indexFailDel01($siteFlowId, $sorts, $filters, $pageSize);
				return $outbOrdHdrs;
			}
		}
	}
	
	protected function indexFailDel01($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		$outbOrdHdrs = OutbOrdHdrRepository::findAllNotExistPrfOrFailDel01Txn($siteFlowId, DocStatus::$MAP['COMPLETE'], DocStatus::$MAP['COMPLETE'], $sorts, $filters, $pageSize);
        $outbOrdHdrs->load('division', 'deliveryPoint');
        //query the docDtls, and format into group details
        foreach($outbOrdHdrs as $outbOrdHdr)
        {
            $outbOrdDtls = OutbOrdDtlRepository::queryShipmentDetailsGroupByItemId($outbOrdHdr->id);
            //calculate the case_qty, gross_weight and cubic_meter
            $caseQty = 0;
            $grossWeight = 0;
            $cubicMeter = 0;
            foreach($outbOrdDtls as $outbOrdDtl)
            {
                $outbOrdDtl->case_qty = round($outbOrdDtl->case_qty, 8);
                $outbOrdDtl->gross_weight = round($outbOrdDtl->gross_weight, 8);
                $outbOrdDtl->cubic_meter = round($outbOrdDtl->cubic_meter, 8);
                $caseQty = bcadd($caseQty, $outbOrdDtl->case_qty, 8);
                $grossWeight = bcadd($grossWeight, $outbOrdDtl->gross_weight, 8);
                $cubicMeter = bcadd($cubicMeter, $outbOrdDtl->cubic_meter, 8);
            }

            $outbOrdHdr->case_qty = $caseQty;
            $outbOrdHdr->gross_weight = $grossWeight;
            $outbOrdHdr->cubic_meter = $cubicMeter;
            $outbOrdHdr->details = $outbOrdDtls;
        }
        return $outbOrdHdrs;
    }
    
    public function createProcess($strProcType, $hdrIds, $toStorageBinId) 
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['FAIL_DEL_01'])
			{
				$result = $this->createFailDel01(ProcType::$MAP[$strProcType], $hdrIds, $toStorageBinId);
				return $result;
			}
		}
    }
    
    protected function createFailDel01($procType, $hdrIds, $toStorageBinId)
	{
		//1) M OutbOrd -> 1 failDel
		//2) variables to store the pack list
		$hdrData = array();
		$dtlDataList = array();
        $docTxnFlowDataList = array();
        $outbOrdHdrList = array();
		$siteFlow = null;
		$lineNo = 1;

		$outbOrdDtls = OutbOrdDtlRepository::findAllByHdrIds($hdrIds);
		$outbOrdDtls->load('outbOrdHdr', 'item', 'outbOrdHdr.siteFlow');
		//3) loop for outbOrdDtls
		foreach($outbOrdDtls as $outbOrdDtl)
		{
            $outbOrdHdr = $outbOrdDtl->outbOrdHdr;
            $siteFlow = $outbOrdHdr->siteFlow;
            $outbOrdHdrList[] = $outbOrdHdr;

            $item = $outbOrdDtl->item;

            //3.1) calculate the outbOrdDtl total unit qty
            $ordTtlUnitQty = bcmul($outbOrdDtl->uom_rate, $outbOrdDtl->qty, $item->qty_scale);
            
            //3.2) get the associated loadListDtls
            //3.2) calculate the loadListDtl total unit qty
            $loadTtlUnitQty = 0;
            $loadListDtls = LoadListDtlRepository::findAllByOutbOrdDtlId($outbOrdDtl->hdr_id, $outbOrdDtl->id);
            foreach($loadListDtls as $loadListDtl)
            {
                //3.2.1) get the sign (1) toQuantBalTxns of this loadListDtl
                $toQuantBalTxns = QuantBalTxnRepository::findAllByDocDtlId(\App\LoadListHdr::class, $loadListDtl->hdr_id, $loadListDtl->id, 1);
                foreach($toQuantBalTxns as $toQuantBalTxn)
                {
                    //3.2.1.2) add to loadListDtl total unit qty
                    $loadTtlUnitQt = bcadd($loadTtlUnitQt, $toQuantBalTxn->unit_qty, 10);

                    //3.2.1.1) build the prfDelDtl
                    $dtlData = array(
                        'outb_ord_hdr_id' => $outbOrdHdr->id,
                        'outb_ord_dtl_id' => $outbOrdDtl->id,
                        'company_id' => $outbOrdHdr->company_id,
                        'item_id' => $outbOrdDtl->item_id,
                        'desc_01' => $outbOrdDtl->desc_01,
                        'desc_02' => $outbOrdDtl->desc_02,
                        'uom_id' => $item->unit_uom_id,
                        'uom_rate' => 1,
                        'qty' => $toQuantBalTxn->unit_qty,
                        'quant_bal_id' => $toQuantBalTxn->quant_bal_id,
                        'to_storage_bin_id' => $toStorageBinId,
                        'to_handling_unit_id' => 0,
                        'whse_job_type' => WhseJobType::$MAP['NULL'],
                        'whse_job_code' => ''
                    );
                    $dtlDataList[$lineNo] = $dtlData;
    
                    $lineNo++;
                }
            }

            //3.3) throw exception if ordTtlUnitQty and loadTtlUnitQty not tally
            if(bccomp($ordTtlUnitQty, $loadTtlUnitQty, 10) != 0)
            {
                $exc = new ApiException(__('LoadList.outb_ord_item_not_tally', ['itemCode'=>$item->code, 'loadUnitQty'=>$loadTtlUnitQty, 'ordUnitQty'=>$ordTtlUnitQty]));
				throw $exc;
            }
        }
        
        //4) build the docTxnFlow list
        foreach($outbOrdHdrList as $outbOrdHdr)
        {
			$tmpDocTxnFlowData = array(
				'fr_doc_hdr_type' => \App\OutbOrdHdr::class,
				'fr_doc_hdr_id' => $outbOrdHdr->id,
				'fr_doc_hdr_code' => $outbOrdHdr->doc_code,
				'is_closed' => 1
			);
			$docTxnFlowDataList[] = $tmpDocTxnFlowData;
        }
        
		//5) build the hdrData
		$hdrData = array(
			'ref_code_01' => '',
			'ref_code_02' => '',
			'doc_date' => date('Y-m-d'),
			'desc_01' => '',
			'desc_02' => '',
			'site_flow_id' => $siteFlow->id
		);

		//5) get the failDel doc no
		$siteDocNo = SiteDocNoRepository::findSiteDocNo($siteFlow->site_id, \App\FailDelHdr::class);
		if(empty($siteDocNo))
		{
			$exc = new ApiException(__('SiteDocNo.doc_no_not_found', ['siteId'=>$siteFlow->site_id, 'docType'=>\App\FailDelHdr::class]));
			//$exc->addData(\App\FailDelHdr::class, $siteFlow->site_id);
			throw $exc;
		}

		//6) query the whseTxnFlow, so know the next step
		$whseTxnFlow = WhseTxnFlowRepository::findByPk($siteFlow->id, $procType);

		//7) create the failDelHdr
		$failDelHdr = FailDelHdrRepository::createProcess($procType, $siteDocNo->doc_no_id, $hdrData, $dtlDataList, $docTxnFlowDataList);

		//8) commit packList to WIP/Complete according to whseTxnFlow
		if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
		{
			self::transitionToWip($failDelHdr->id);
		}
		elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
		{
			self::transitionToComplete($failDelHdr->id);
		}

		$message = __('FailDel.document_successfully_created', ['docCode'=>$failDelHdr->doc_code]);

		return array(
			'data' => $failDelHdr,
			'message' => $message
		);
    }

    public function revertProcess($hdrId)
    {
		$failDelHdr = FailDelHdrRepository::findByPk($hdrId);

		$whseTxnFlow = WhseTxnFlowRepository::findByPk($failDelHdr->site_flow_id, $failDelHdr->proc_type);

		self::transitionToDraft($failDelHdr->id);

		return $failDelHdr;
    }
    
    public function completeProcess($hdrId)
  	{
		//used by api engine for testing, should not be access by users
		$failDelHdr = FailDelHdrRepository::txnFindByPk($hdrId);

		$whseTxnFlow = WhseTxnFlowRepository::findByPk($failDelHdr->site_flow_id, $failDelHdr->proc_type);

		if($failDelHdr->doc_status < DocStatus::$MAP['WIP'])
		{
			$exc = new ApiException(__('FailDel.doc_status_is_not_wip', ['docCode'=>$failDelHdr->doc_code]));
			$exc->addData(\App\FailDelHdr::class, $failDelHdr->id);
			throw $exc;
		}
		if($failDelHdr->doc_status >= DocStatus::$MAP['COMPLETE'])
		{
			$exc = new ApiException(__('FailDel.doc_status_is_complete', ['docCode'=>$failDelHdr->doc_code]));
			$exc->addData(\App\FailDelHdr::class, $failDelHdr->id);
			throw $exc;
		}

		self::transitionToComplete($failDelHdr->id);

		return $failDelHdr;
	}

	public function submitProcess($hdrId)
  	{
		$failDelHdr = FailDelHdrRepository::txnFindByPk($hdrId);

		$whseTxnFlow = WhseTxnFlowRepository::findByPk($failDelHdr->site_flow_id, $failDelHdr->proc_type);

		if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
		{
			self::transitionToWip($failDelHdr->id);
		}
		elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
		{
			self::transitionToComplete($failDelHdr->id);
		}

		return $failDelHdr;
	}
}