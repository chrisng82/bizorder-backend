<?php

namespace App\Services;

use App\ItemUom;
use App\Item;
use App\HandlingUnit;
use App\Services\Env\ResType;
use App\Services\Env\ProcType;
use App\Services\Env\ResStatus;
use App\Services\Env\ScanMode;
use App\Services\Env\RetrievalMethod;
use App\Services\Env\ItemType;
use App\Services\Env\StorageClass;
use App\Repositories\BatchJobStatusRepository;
use App\Repositories\HandlingUnitRepository;
use App\Repositories\PrintDocSettingRepository;
use App\Repositories\QuantBalRepository;
use App\Repositories\SiteFlowRepository;
use App\Repositories\PrintResTxnRepository;
use App\Repositories\WhseJobDtlRepository;
use App\Repositories\GdsRcptInbOrdRepository;
use App\BatchJobStatus;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use App\Services\ItemService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class HandlingUnitService
{
    public function __construct()
    {
    }

    public function indexProcess($strProcType, $siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['HANDLING_UNIT_LIST_01']) 
			{
				//pallet label listing
				$items = $this->indexHandlingUnitList01($siteFlowId, $sorts, $filters, $pageSize);
				return $items;
			}
		}
	}
	
	protected function indexHandlingUnitList01($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
            array(
                'handling_unit_read'
            )
		);
		
		$handlingUnits = HandlingUnitRepository::findAllBySiteFlowId($siteFlowId, $sorts, $filters, $pageSize);
        //$handlingUnits->load('unitUom');
		//query the docDtls, and format into group details
		foreach($handlingUnits as $handlingUnit)
		{
			$handlingUnit->barcode = ResType::$MAP['HANDLING_UNIT'].str_pad($handlingUnit->id, 11, '0', STR_PAD_LEFT);
			
			$printResTxn = PrintResTxnRepository::queryPrintResTxn(HandlingUnit::class, $handlingUnit->id);
			$handlingUnit->print_count = $printResTxn->print_count;
			$handlingUnit->first_printed_at = $printResTxn->first_printed_at;
			$handlingUnit->last_printed_at = $printResTxn->last_printed_at;

			//need to check the quantBal items
			$processedQuantBals = array();
			$quantBals = QuantBalRepository::findAllActiveByHandlingUnitId($handlingUnit->id);
            $quantBals->load('storageBin', 'item', 'itemBatch');
            foreach($quantBals as $quantBal)
            {
				$storageBin = $quantBal->storageBin;
				$quantBal->storage_bin_code = $storageBin->code;

				$item = $quantBal->item;
				
				$quantBal = ItemService::processCaseLoose($quantBal, $item, 1);
				$processedQuantBals[] = $quantBal;
			}
			
			$handlingUnit->quant_bals = $processedQuantBals;
		}
        return $handlingUnits;
	}

	public function printProcess($strProcType, $siteFlowId, $ids)
  	{
		$user = Auth::user();

		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['HANDLING_UNIT_LIST_01']) 
			{
				//print pallet label
				return $this->printHandlingUnitList01($siteFlowId, $user, $ids);
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['HANDLING_UNIT_LIST_02'])
			{
				//print gdsRcpt whseJob pallet label
				return $this->printHandlingUnitList02($siteFlowId, $user, $ids);
			}
		}
	}

	public function printHandlingUnitList01($siteFlowId, $user, $ids)
  	{
		AuthService::authorize(
            array(
                'handling_unit_print'
            )
		);

		$printDocSetting = PrintDocSettingRepository::findByDocHdrTypeAndSiteFlowId(\App\HandlingUnit::class.'01', $siteFlowId);
		if(empty($printDocSetting))
		{
			$exc = new ApiException(__('PrintDoc.report_template_not_found', ['docType'=>\App\HandlingUnit::class]));
			$exc->addData(\App\PrintDocSetting::class, \App\HandlingUnit::class);
			throw $exc;
		}

		$handlingUnits = HandlingUnitRepository::findAllByIds($ids);
		$printResTxnDataArray = array();
		foreach($handlingUnits as $handlingUnit)
		{
			$handlingUnit->view_name = $printDocSetting->view_name;
			$handlingUnit->barcode = $handlingUnit->getBarcode();

			$printResTxnData = array();
			$printResTxnData['res_type'] = \App\HandlingUnit::class;
			$printResTxnData['res_id'] = $handlingUnit->id;
			$printResTxnData['user_id'] = $user->id;
			$printResTxnDataArray[] = $printResTxnData;
		}

		foreach($handlingUnits as $handlingUnit)
		{
			//need to check the quantBal items
			$processedQuantBals = array();
			$quantBals = QuantBalRepository::findAllActiveByHandlingUnitId($handlingUnit->id);
			$quantBals->load('storageBin', 'item', 'itemBatch');
			$handlingUnit->storage_bin_code = '';
			$handlingUnit->item_code = '';
			$handlingUnit->item_desc_01 = '';
			$handlingUnit->case_qty = '';
			$handlingUnit->expiry_date = '';
            for($a = 0; $a < count($quantBals); $a++)
            {
				$quantBal = $quantBals[$a];

				$storageBin = $quantBal->storageBin;
				$handlingUnit->storage_bin_code = $storageBin->code;

				$item = $quantBal->item;
				$itemBatch = $quantBal->itemBatch;
				
				$quantBal = ItemService::processCaseLoose($quantBal, $item, 1);

				if($a == 0)
				{
					$handlingUnit->item_code = $quantBal->item_code;
					$handlingUnit->item_desc_01 = $quantBal->item_desc_01;
					$handlingUnit->case_qty = $quantBal->case_qty.' '.$quantBal->item_case_uom_code;
					$handlingUnit->expiry_date = $itemBatch->expiry_date;
				}
				else
				{
					$handlingUnit->item_code .= '/'.$quantBal->item_code;
					$handlingUnit->item_desc_01 .= '/'.$quantBal->item_desc_01;
					$handlingUnit->case_qty .= '/'.$quantBal->case_qty.' '.$quantBal->item_case_uom_code;
					$handlingUnit->expiry_date .= '/'.$itemBatch->expiry_date;
				}
			}
		}

		$printedAt = PrintResTxnRepository::create($printResTxnDataArray);

		$pdf = \Illuminate\Support\Facades\App::make('dompdf.wrapper');
		$pdf->getDomPDF()->set_option("enable_php", true);
		$pdf->setPaper(array(
			0,
			0,
			$printDocSetting->page_width, 
			$printDocSetting->page_height
		), $printDocSetting->page_orientation);

		//each data must have the view_name
		$pdf->loadHTML(
			view('batchPrintDocuments',
				array(
					'data' => $handlingUnits, 
					'printedAt' => $printedAt,
					'printedBy' => $user,
					'isPageBreakAfter' => true,
				)
			)
		);

		return $pdf->stream();
	}

	public function printHandlingUnitList02($siteFlowId, $user, $ids)
  	{
		AuthService::authorize(
            array(
                'handling_unit_print'
            )
		);

		$printDocSetting = PrintDocSettingRepository::findByDocHdrTypeAndSiteFlowId(\App\HandlingUnit::class.'02', $siteFlowId);
		if(empty($printDocSetting))
		{
			$exc = new ApiException(__('PrintDoc.report_template_not_found', ['docType'=>\App\HandlingUnit::class]));
			$exc->addData(\App\PrintDocSetting::class, \App\HandlingUnit::class);
			throw $exc;
		}

		$printResTxnDataArray = array();
		$handlingUnitsHash = array();
		$InbOrdDataHashByWhseJobHdrId = array();
		$whseJobDtls = WhseJobDtlRepository::findAllByHdrIds($ids);
		$whseJobDtls->load('toHandlingUnit', 'item', 'whseJobHdr', 'whseJobHdr.frDocTxnFlows');
		foreach($whseJobDtls as $whseJobDtl)
		{
			$handlingUnit = $whseJobDtl->toHandlingUnit;
			if(empty($handlingUnit))
			{
				continue;
			}
			if(array_key_exists($handlingUnit->id, $handlingUnitsHash))
			{
				continue;
			}
			$item = $whseJobDtl->item;
			if(empty($item))
			{
				continue;
			}

			$inbOrdData = array();
			if(!array_key_exists($whseJobDtl->hdr_id, $InbOrdDataHashByWhseJobHdrId))
			{
				//query the gdsRcptInbOrd
				$whseJobHdr = $whseJobDtl->whseJobHdr;
				$frDocTxnFlows = $whseJobHdr->frDocTxnFlows;
				$gdsRcptHdrIds = array();
				foreach($frDocTxnFlows as $frDocTxnFlow)
				{
					$gdsRcptHdrIds[] = $frDocTxnFlow->fr_doc_hdr_id;
				}
				$gdsRcptInbOrds = GdsRcptInbOrdRepository::findAllByHdrIds($gdsRcptHdrIds);
				$gdsRcptInbOrds->load('inbOrdHdr');
				for($a = 0; $a < count($gdsRcptInbOrds); $a++)
				{
					$gdsRcptInbOrd = $gdsRcptInbOrds[$a];
					$inbOrdHdr = $gdsRcptInbOrd->inbOrdHdr;
					if($a == 0)
					{
						$inbOrdData['adv_ship_hdr_code'] = $inbOrdHdr->adv_ship_hdr_code;
						$inbOrdData['ref_code_01'] = $inbOrdHdr->ref_code_01;
						$inbOrdData['ref_code_02'] = $inbOrdHdr->ref_code_02;
						$inbOrdData['ref_code_03'] = $inbOrdHdr->ref_code_03;
						$inbOrdData['ref_code_04'] = $inbOrdHdr->ref_code_04;
						$inbOrdData['ref_code_05'] = $inbOrdHdr->ref_code_05;
						$inbOrdData['ref_code_06'] = $inbOrdHdr->ref_code_06;
						$inbOrdData['ref_code_07'] = $inbOrdHdr->ref_code_07;
						$inbOrdData['ref_code_08'] = $inbOrdHdr->ref_code_08;
					}
					else
					{
						$inbOrdData['adv_ship_hdr_code'] .= '/'.$inbOrdHdr->adv_ship_hdr_code;
						$inbOrdData['ref_code_01'] .= '/'.$inbOrdHdr->ref_code_01;
						$inbOrdData['ref_code_02'] .= '/'.$inbOrdHdr->ref_code_02;
						$inbOrdData['ref_code_03'] .= '/'.$inbOrdHdr->ref_code_03;
						$inbOrdData['ref_code_04'] .= '/'.$inbOrdHdr->ref_code_04;
						$inbOrdData['ref_code_05'] .= '/'.$inbOrdHdr->ref_code_05;
						$inbOrdData['ref_code_06'] .= '/'.$inbOrdHdr->ref_code_06;
						$inbOrdData['ref_code_07'] .= '/'.$inbOrdHdr->ref_code_07;
						$inbOrdData['ref_code_08'] .= '/'.$inbOrdHdr->ref_code_08;
					}
				}

				$InbOrdDataHashByWhseJobHdrId[$whseJobDtl->hdr_id] = $inbOrdData;
			}
			else
			{
				$inbOrdData = $InbOrdDataHashByWhseJobHdrId[$whseJobDtl->hdr_id];
			}

			$handlingUnit->view_name = $printDocSetting->view_name;
			$handlingUnit->barcode = $handlingUnit->getBarcode();

			$printResTxnData = array();
			$printResTxnData['res_type'] = \App\HandlingUnit::class;
			$printResTxnData['res_id'] = $handlingUnit->id;
			$printResTxnData['user_id'] = $user->id;
			$printResTxnDataArray[] = $printResTxnData;

			$handlingUnit->adv_ship_hdr_code = $inbOrdData['adv_ship_hdr_code'];
			$handlingUnit->ref_code_01 = $inbOrdData['ref_code_01'];
			$handlingUnit->ref_code_02 = $inbOrdData['ref_code_02'];
			$handlingUnit->ref_code_03 = $inbOrdData['ref_code_03'];
			$handlingUnit->ref_code_04 = $inbOrdData['ref_code_04'];
			$handlingUnit->ref_code_05 = $inbOrdData['ref_code_05'];
			$handlingUnit->ref_code_06 = $inbOrdData['ref_code_06'];
			$handlingUnit->ref_code_07 = $inbOrdData['ref_code_07'];
			$handlingUnit->ref_code_08 = $inbOrdData['ref_code_08'];

			$whseJobDtl = ItemService::processCaseLoose($whseJobDtl, $item);

			$handlingUnit->item_code = $whseJobDtl->item_code;
			$handlingUnit->item_desc_01 = $whseJobDtl->item_desc_01;
			$handlingUnit->case_qty = $whseJobDtl->case_qty.' '.$whseJobDtl->item_case_uom_code;
			$handlingUnit->loose_qty = $whseJobDtl->loose_qty.' '.$whseJobDtl->item_loose_uom_code;

			$handlingUnitsHash[$handlingUnit->id] = $handlingUnit;
		}

		$printedAt = PrintResTxnRepository::create($printResTxnDataArray);

		$pdf = \Illuminate\Support\Facades\App::make('dompdf.wrapper');
		$pdf->getDomPDF()->set_option("enable_php", true);
		$pdf->setPaper(array(
			0,
			0,
			$printDocSetting->page_width, 
			$printDocSetting->page_height
		), $printDocSetting->page_orientation);

		//each data must have the view_name
		$pdf->loadHTML(
			view('batchPrintDocuments',
				array(
					'data' => array_values($handlingUnitsHash), 
					'printedAt' => $printedAt,
					'printedBy' => $user,
					'isPageBreakAfter' => true,
				)
			)
		);

		return $pdf->stream();
	}

	public function createProcess($strProcType, $siteFlowId, $noOfPallets) 
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['HANDLING_UNIT_LIST_01'])
			{
				$result = $this->createHandlingUnitList01(ProcType::$MAP[$strProcType], $siteFlowId, $noOfPallets);
				return $result;
			}
		}
	}

	protected function createHandlingUnitList01($procType, $siteFlowId, $noOfPallets)
    {
		AuthService::authorize(
            array(
                'handling_unit_create'
            )
		);

		$handlingUnits = HandlingUnitRepository::createHandlingUnitList01($procType, $siteFlowId, $noOfPallets);
		foreach($handlingUnits as $handlingUnit)
		{
			$handlingUnit->barcode = ResType::$MAP['HANDLING_UNIT'].str_pad($handlingUnit->id, 11, '0', STR_PAD_LEFT);
			
			$printResTxn = PrintResTxnRepository::queryPrintResTxn(HandlingUnit::class, $handlingUnit->id);
			$handlingUnit->print_count = $printResTxn->print_count;
			$handlingUnit->first_printed_at = $printResTxn->first_printed_at;
			$handlingUnit->last_printed_at = $printResTxn->last_printed_at;
		}

		$message = __('HandlingUnit.pallets_successfully_created', ['noOfPallets'=>$noOfPallets]);

		return array(
			'data' => $handlingUnits,
			'message' => $message
		);
	}

	public function select2($search, $filters)
    {
		$search = str_replace('ZY', '', $search);
		//DB::connection()->enableQueryLog();
		$handlingUnits = HandlingUnitRepository::select2($search, $filters);
		//Log::error(DB::getQueryLog());
		//$handlingUnits->load('item', 'itemBatch', 'handlingUnit');
        foreach($handlingUnits as $handlingUnit)
        {
			$handlingUnit->barcode = $handlingUnit->getBarCode();
        }
        return $handlingUnits;
    }

    public function select2Init($id, $filters)
    {
        $quantBal = null;
        if($id > 0) 
        {
            $quantBal = QuantBalRepository::findByPk($id);
        }
        else
        {
            $quantBal = QuantBalRepository::findTopGroupByHandlingUnitId($filters);
        }
        $option = array('value'=>0, 'label'=>'');
        if(!empty($quantBal))
        {
            $handlingUnit = $quantBal->handlingUnit;
            $item = $quantBal->item;
            $itemBatch = $quantBal->itemBatch;
            //item code | expiry date | handling unit
            $option = array(
                'value'=>$quantBal->id, 
                'label'=>$item->code.' '.$quantBal->balance_unit_qty,
                'item_code'=>$item->code,
                'batch_serial_no'=> empty($itemBatch) ? $itemBatch->batch_serial_no : '',
                'expiry_date'=> empty($itemBatch) ? $itemBatch->expiry_date : '',
                'receipt_date'=> empty($itemBatch) ? $itemBatch->receipt_date : '',
                'handling_unit'=> empty($handlingUnit) ? $handlingUnit->getBarcode() : '',
            );
        }
        return $option;
    }
}
