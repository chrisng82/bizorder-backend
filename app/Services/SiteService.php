<?php

namespace App\Services;

use App\Services\Env\TxnFlowType;
use App\Services\Env\ProcType;
use App\Services\Env\ScanMode;
use App\Services\Env\ResType;
use App\Services\Env\DocStatus;
use App\Repositories\SiteFlowRepository;
use App\Repositories\WhseTxnFlowRepository;
use Illuminate\Support\Facades\Auth;

class SiteService 
{
	public function __construct() 
	{
    }

    public function indexSiteFlow() 
	{
        $user = Auth::user();

        $siteFlows = SiteFlowRepository::findAllByUserId($user->id);
        
        return $siteFlows;
    }
    
    public function indexGdsDelFlow($siteFlowId) 
	{
        $user = Auth::user();

        $whseTxnFlows = WhseTxnFlowRepository::findAllBySiteFlowIdAndTxnFlowType($siteFlowId, TxnFlowType::$MAP['OUTB_DEL']);
        foreach($whseTxnFlows as $whseTxnFlow)
        {
            $whseTxnFlow->txn_flow_type = TxnFlowType::$MAP[$whseTxnFlow->txn_flow_type];
            $whseTxnFlow->proc_type = ProcType::$MAP[$whseTxnFlow->proc_type];
            $whseTxnFlow->scan_mode = ScanMode::$MAP[$whseTxnFlow->scan_mode];
            $whseTxnFlow->fr_res_type = ResType::$MAP[$whseTxnFlow->fr_res_type];
            $whseTxnFlow->to_res_type = ResType::$MAP[$whseTxnFlow->to_res_type];
            $whseTxnFlow->to_doc_status = DocStatus::$MAP[$whseTxnFlow->to_doc_status];
        }
        return $whseTxnFlows;
    }
    
    public function indexGdsRcptFlow($siteFlowId) 
	{
        $user = Auth::user();

        $whseTxnFlows = WhseTxnFlowRepository::findAllBySiteFlowIdAndTxnFlowType($siteFlowId, TxnFlowType::$MAP['INB_RCPT']);
        foreach($whseTxnFlows as $whseTxnFlow)
        {
            $whseTxnFlow->txn_flow_type = TxnFlowType::$MAP[$whseTxnFlow->txn_flow_type];
            $whseTxnFlow->proc_type = ProcType::$MAP[$whseTxnFlow->proc_type];
            $whseTxnFlow->scan_mode = ScanMode::$MAP[$whseTxnFlow->scan_mode];
            $whseTxnFlow->fr_res_type = ResType::$MAP[$whseTxnFlow->fr_res_type];
            $whseTxnFlow->to_res_type = ResType::$MAP[$whseTxnFlow->to_res_type];
            $whseTxnFlow->to_doc_status = DocStatus::$MAP[$whseTxnFlow->to_doc_status];
        }
        return $whseTxnFlows;
    }
    
    public function indexInventoryAuditFlow($siteFlowId) 
	{
        $user = Auth::user();

        $whseTxnFlows = WhseTxnFlowRepository::findAllBySiteFlowIdAndTxnFlowType($siteFlowId, TxnFlowType::$MAP['INVENTORY_AUDIT']);
        foreach($whseTxnFlows as $whseTxnFlow)
        {
            $whseTxnFlow->txn_flow_type = TxnFlowType::$MAP[$whseTxnFlow->txn_flow_type];
            $whseTxnFlow->proc_type = ProcType::$MAP[$whseTxnFlow->proc_type];
            $whseTxnFlow->scan_mode = ScanMode::$MAP[$whseTxnFlow->scan_mode];
            $whseTxnFlow->fr_res_type = ResType::$MAP[$whseTxnFlow->fr_res_type];
            $whseTxnFlow->to_res_type = ResType::$MAP[$whseTxnFlow->to_res_type];
            $whseTxnFlow->to_doc_status = DocStatus::$MAP[$whseTxnFlow->to_doc_status];
        }
        return $whseTxnFlows;
    }

    public function indexBinTransferFlow($siteFlowId) 
	{
        $user = Auth::user();

        $whseTxnFlows = WhseTxnFlowRepository::findAllBySiteFlowIdAndTxnFlowType($siteFlowId, TxnFlowType::$MAP['BIN_TRANSFER']);
        foreach($whseTxnFlows as $whseTxnFlow)
        {
            $whseTxnFlow->txn_flow_type = TxnFlowType::$MAP[$whseTxnFlow->txn_flow_type];
            $whseTxnFlow->proc_type = ProcType::$MAP[$whseTxnFlow->proc_type];
            $whseTxnFlow->scan_mode = ScanMode::$MAP[$whseTxnFlow->scan_mode];
            $whseTxnFlow->fr_res_type = ResType::$MAP[$whseTxnFlow->fr_res_type];
            $whseTxnFlow->to_res_type = ResType::$MAP[$whseTxnFlow->to_res_type];
            $whseTxnFlow->to_doc_status = DocStatus::$MAP[$whseTxnFlow->to_doc_status];
        }
        return $whseTxnFlows;
    }

    public function indexRtnRcptFlow($siteFlowId) 
	{
        $user = Auth::user();

        $whseTxnFlows = WhseTxnFlowRepository::findAllBySiteFlowIdAndTxnFlowType($siteFlowId, TxnFlowType::$MAP['RTN_RCPT']);
        foreach($whseTxnFlows as $whseTxnFlow)
        {
            $whseTxnFlow->txn_flow_type = TxnFlowType::$MAP[$whseTxnFlow->txn_flow_type];
            $whseTxnFlow->proc_type = ProcType::$MAP[$whseTxnFlow->proc_type];
            $whseTxnFlow->scan_mode = ScanMode::$MAP[$whseTxnFlow->scan_mode];
            $whseTxnFlow->fr_res_type = ResType::$MAP[$whseTxnFlow->fr_res_type];
            $whseTxnFlow->to_res_type = ResType::$MAP[$whseTxnFlow->to_res_type];
            $whseTxnFlow->to_doc_status = DocStatus::$MAP[$whseTxnFlow->to_doc_status];
        }
        return $whseTxnFlows;
    }
}