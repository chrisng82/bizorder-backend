<?php

namespace App\Services;

use App\Services\Env\ResType;
use App\Services\Utils\ApiException;
use App\Repositories\AuditRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

class AuditService
{
    public function showAuditTypes($resType)
    {
        $auditTypes = array();
        if(array_key_exists($resType, ResType::$AUDIT))
        {
            $types = ResType::$AUDIT[$resType];
            foreach($types as $type)
            {
                $auditTypes[] = array(
                    'audit_type' => str_replace('App\\', '', $type),
                    'class_name' => $type
                );
            }
            
        }
        return $auditTypes;
    }

    public function auditResource($resType, $resId, $auditType, $sorts, $filters = array(), $pageSize = 20)
    {        
        AuthService::authorize(
			array(
				strtolower($resType).'_read'
			)
        );

        if(strpos($auditType, 'App\\') !== 0)
        {
            $auditType = 'App\\'.$auditType;
        }

        $auditTypes = array();
        if(array_key_exists($resType, ResType::$AUDIT))
        {
            $auditTypes = ResType::$AUDIT[$resType];
        }
        else
        {
            $exc = new ApiException(__('Audit.resource_type_not_defined', ['resType'=>$resType]));
			//$exc->addData(\App\WhseJobHdr::class, $whseJobHdr->id);
			throw $exc;
        }

        $audits = null;
        $primaryType = $auditTypes[0];
        if(strcmp($primaryType, $auditType) == 0)
        {
            //this is primary
            //DB::connection()->enableQueryLog();
            $audits = AuditRepository::findAllByAuditableTypeAndAuditableId($primaryType, $resId, $sorts, $filters, $pageSize);
            //Log::error(DB::getQueryLog());
        }
        else
        {
            //this is secondary
            $primaryModel = ($primaryType)::find($resId);
            //DB::connection()->enableQueryLog();
            $tags = $primaryModel->generateTags();
            $tag = '';
            foreach($tags as $tag)
            {
            }
            $audits = AuditRepository::findAllByAuditableTypeAndTags($auditType, $tag, $sorts, $filters, $pageSize);
            //Log::error(DB::getQueryLog());
        }        
        
        if(is_null($audits))
        {
            return array();
        }
        else
        {
            $audits->load('user');
            foreach($audits as $audit)
            {
                $user = $audit->user;
                $audit->username = $user->username;
                $audit->first_name = $user->first_name;
                $audit->last_name = $user->last_name;

                $audit = self::processOutgoingModel($audit, $user);

                unset($audit->user);
            }
            return $audits;
        }
    }

    public function auditUser($userId, $sorts, $filters = array(), $pageSize = 20)
    {        
        $user = Auth::user();

        AuthService::authorize(
			array(
				'user_read'
			)
        );

        //DB::connection()->enableQueryLog();
        $audits = AuditRepository::findAllByUserId($userId, $sorts, $filters, $pageSize);
        //$audits->load('auditable');
        foreach($audits as $audit)
        {
            //$auditable = $audit->auditable;
            $audit = self::processOutgoingModel($audit, $user);

            //unset($audit->auditable);
        }
        return $audits;
    }

    public function auditLog($auditable_id, $sorts, $filters = array(), $pageSize = 20)
    {        
        $user = Auth::user();

        AuthService::authorize(
			array(
				'user_read'
			)
        );

        //DB::connection()->enableQueryLog();
        $audits = AuditRepository::findAllByauditableId($auditable_id, $sorts, $filters, $pageSize);
        //$audits->load('auditable');
        foreach($audits as $audit)
        {
            //$auditable = $audit->auditable;
            $audit = self::processOutgoingModel($audit, $user);

            //unset($audit->auditable);
        }
        return $audits;
    }

    static public function processOutgoingModel($model, $user)
	{        
        $model->auditable_type = str_replace('App\\', '', $model->auditable_type);
        $model->created_at = $model->created_at->setTimezone($user->timezone);
        $model->updated_at = $model->updated_at->setTimezone($user->timezone);

        $oldValuesList = json_decode($model->old_values);
        $processValues = array();
        foreach($oldValuesList as $name => $value)
        {
            if(strpos($name, '_at') !== false
            || strpos($name, 'last_login') !== false
            || strpos($name, '_date') !== false)
            {
                if(!empty($value)) 
                {
                    if(is_object($value))
                    {
                        $value = new Carbon($value->date, $value->timezone);
                    }
                    else
                    {
                        $value = new Carbon($value, 'UTC');
                    }
                    
                    $value = $value->setTimezone($user->timezone)->format('Y-m-d H:i:s');
                }
            }

            $processValues[] = array(
                'name' => $name,
                'value' => $value
            );
        }
        $model->old_values = $processValues;

        $newValues = json_decode($model->new_values);
        $processValues = array();
        foreach($newValues as $name => $value)
        {
            if(strpos($name, '_at') !== false
            || strpos($name, 'last_login') !== false
            || strpos($name, '_date') !== false)
            {
                if(!empty($value)) 
                {
                    if(is_object($value))
                    {
                        $value = new Carbon($value->date, $value->timezone);
                    }
                    else
                    {
                        $value = new Carbon($value, 'UTC');
                    }
                    
                    $value = $value->setTimezone($user->timezone)->format('Y-m-d H:i:s');
                }
            }

            $processValues[] = array(
                'name' => $name,
                'value' => $value
            );
        }
        $model->new_values = $processValues;

		return $model;
	}
}
