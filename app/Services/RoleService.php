<?php

namespace App\Services;

use ErrorException;
use App\Services\Env\ProcType;
use App\Services\PermissionService;
use App\Repositories\RoleRepository;
use App\Repositories\PermissionRepository;
use App\Repositories\PermissionRoleRepository;
use App\Repositories\BatchJobStatusRepository;
use App\Repositories\SiteFlowRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use App\Imports\RoleExcel01Import;

class RoleService
{
    public function __construct() 
	{
    }

    public function indexProcess($strProcType, $sorts, $filters = array(), $pageSize = 20) 
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['ROLE_LIST_01']) 
			{
				//item listing
				$roles = $this->indexRoleList01($sorts, $filters, $pageSize);
				return $roles;
			}
		}
    }
    
    public function indexRoleList01($sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
            array(
                'role_read'
            )
        );

        //DB::connection()->enableQueryLog();
        $roles = RoleRepository::findAll($sorts, $filters, $pageSize);
        //Log::error(DB::getQueryLog());
        foreach($roles as $role)
        {
            $role = self::processOutgoingModel($role);
        }

    	return $roles;
    }

    public function indexPermissions($roleId, $sorts, $filters = array(), $pageSize = 20) 
	{
        AuthService::authorize(
            array(
                'role_read'
            )
        );

        $permissionIdHash = array();
        $permissionRoles = PermissionRoleRepository::findAllByRoleId($roleId);
        foreach($permissionRoles as $permissionRole)
        {
            $permissionIdHash[$permissionRole->permission_id] = $permissionRole->permission_id;
        }

        $allPermissions = PermissionService::index($sorts, $filters = array(), $pageSize = 20);
        foreach($allPermissions as $permission)
        {
            $permission->is_attached = false;
            if(array_key_exists($permission->id, $permissionIdHash))
            {
                $permission->is_attached = true;
            }
        }
        return $allPermissions;
    }
    
    public function uploadProcess($strProcType, $siteFlowId, $file)
    {
        $user = Auth::user();

        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['ROLE_EXCEL_01']) 
			{
                //process item excel
                $path = Storage::putFileAs('upload/', $file, 'ROLE_EXCEL_01.XLSX');
				return $this->uploadRoleExcel01($siteFlowId, $user->id, $path);
			}
		}
    }

    public function uploadRoleExcel01($siteFlowId, $userId, $path)
    {
        AuthService::authorize(
            array(
                'role_import'
            )
        );

        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['ROLE_EXCEL_01'], $userId);

        $siteFlow = SiteFlowRepository::findByPk($siteFlowId);

        $roleExcel01Import = new RoleExcel01Import($siteFlow->site_id, $userId, $batchJobStatusModel);
        $excel = Excel::import($roleExcel01Import, $path);

        $roleHashByName = array();
        //process the user role first
        $roleDataHash = $roleExcel01Import->getRoleDataHash();
        foreach($roleDataHash as $roleName => $roleData)
        {
            $role = RoleRepository::syncRoleExcel01($roleData);
            $roleHashByName[$role->name] = $role;
        }

        $permissionDataHash = $roleExcel01Import->getPermissionDataHash();
        foreach($permissionDataHash as $roleName => $permissionDatas)
        {
            $role = $roleHashByName[$roleName];

            $dbPermissions = $role->permissions;
            //struture the db permissions to hashtable
            $dbPermissionHash = array();
            foreach($dbPermissions as $dbPermission)
            {
                $dbPermissionHash[$dbPermission->name] = $dbPermission;
            }

            foreach($permissionDatas as $permissionData)
            {
                if(empty($permissionData['name']))
                {
                    continue;
                }
                $permission = PermissionRepository::syncRoleExcel01($permissionData);

                if(array_key_exists($permission->name, $dbPermissionHash))
                {
                    unset($dbPermissionHash[$permission->name]);
                }
                else
                {
                    $role->givePermissionTo($permission);
                }
            }
            
            foreach($dbPermissionHash as $permissionName => $dbPermission)
            {
                $role->revokePermissionTo($dbPermission);
            }
        }

        $message = __('Role.file_successfully_uploaded', ['total'=>$roleExcel01Import->getTotal()]);

		return array(
			'data' => $roleExcel01Import->getTotal(),
			'message' => $message
		);
    }

    public function select2($search, $filters)
    {
        //DB::connection()->enableQueryLog();
        $roles = RoleRepository::select2($search, $filters);
        //Log::error(DB::getQueryLog());
        foreach($roles as $role)
        {
            $role = self::processOutgoingModel($role);
        }
        return $roles;
    }

    static public function processOutgoingModel($model)
	{
        $model->desc_01 = __('Role.'.$model->name.'_desc_01');
		return $model;
    }
    
    public function showPermissions($id) 
	{
        AuthService::authorize(
            array(
                'role_read',
				'role_update'
            )
        );

        $role = RoleRepository::findByPk($id);
        $permissions = array();
        if(!empty($role))
        {
            $permissions = $role->permissions;
            foreach($permissions as $permission)
            {
                $permission = PermissionService::processOutgoingModel($permission);
            }
        }
        return $permissions;
    }

    public function deletePermissions($id, $permissionDataArray)
	{
        AuthService::authorize(
            array(
                'role_update'
            )
        );

        $role = RoleRepository::findByPk($id);
        
		$deletedPermissions = array();
        foreach($permissionDataArray as $permissionData)
        {
            $permission = PermissionRepository::findByPk($permissionData['permission_id']);
            if(!empty($permission))
            {
                $role->revokePermissionTo($permission);
                $permission = PermissionService::processOutgoingModel($permission);

                $deletedPermissions[] = $permission;
            }
        }

        $message = __('Role.permissions_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'permissions'=>array(),
                'deleted_permissions'=>$deletedPermissions
            ),
			'message' => $message
		);
    }
    
    public function updatePermissions($id, $permissionDataArray)
	{
        AuthService::authorize(
            array(
                'role_update'
            )
        );

        $role = RoleRepository::findByPk($id);
        
        $updatedPermissions = array();
        foreach($permissionDataArray as $permissionData)
        {
            $permission = PermissionRepository::findByPk($permissionData['permission_id']);
            if(!empty($permission))
            {
                $role->givePermissionTo($permission);
                $permission = PermissionService::processOutgoingModel($permission);

                $updatedPermissions[] = $permission;
            }
        }

        $message = __('Role.permissions_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'permissions'=>$updatedPermissions,
                'deleted_permissions'=>array()
            ),
			'message' => $message
		);
    }
}
