<?php

namespace App\Services;

use App\Services\Env\ResStatus;
use App\Services\Env\ResType;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Repositories\OutbOrdHdrRepository;
use App\Repositories\OutbOrdDtlRepository;
use App\Repositories\InbOrdHdrRepository;
use App\Repositories\InbOrdDtlRepository;
use App\Repositories\ItemRepository;
use App\Repositories\ItemUomRepository;
use App\Repositories\ItemSalePriceRepository;
use App\Repositories\ItemPurPriceRepository;
use App\Repositories\PromoDtlRepository;
use App\Repositories\DocTxnFlowRepository;
use App\Services\Utils\ApiException;

class InventoryService 
{

	protected function isDtlItemMatchWithPromoDtl($dtlItemModel, $promoDtlModel)
	{
		$isMatched = false;
		if(
			($promoDtlModel->item_id == $dtlItemModel->id && $promoDtlModel->item_id > 0)
		|| ($promoDtlModel->group_01_id == $dtlItemModel->item_group_01_id && $promoDtlModel->group_01_id > 0)
		|| ($promoDtlModel->group_02_id == $dtlItemModel->item_group_02_id && $promoDtlModel->group_02_id > 0)
		|| ($promoDtlModel->group_03_id == $dtlItemModel->item_group_03_id && $promoDtlModel->group_03_id > 0)
		|| ($promoDtlModel->group_04_id == $dtlItemModel->item_group_04_id && $promoDtlModel->group_04_id > 0)
		|| ($promoDtlModel->group_05_id == $dtlItemModel->item_group_05_id && $promoDtlModel->group_05_id > 0)
		)
		{
			$isMatched = true;
		}
		return $isMatched;
	}

	protected function processPromotion($dtlArray, $docDate, $divisionId, 
	$debtorId, $custGroup01Id, $custGroup02Id, $custGroup03Id, $custGroup04Id, $custGroup05Id)
    {
		//return the modifiedDtl array
		$modifiedDtlArray = array();
		//state:
		$prevDtlArray = array();
		$promoDtlsHashByCodeAndLevelNo = array();	

		//1) loop the document all details, sorted by line no
		//1) function like parser, start when the detail promoHdrId is 0, end when apply promotion
		//1) or loop until the document detail, promoHdrId is not 0 (already applied promotion)
		foreach($dtlArray as $dtlSeq => $dtlData)
		{
			//1.1) skip document detail, if already applied promotion
			if($dtlData['promo_hdr_id'] > 0)
			{
				//reset the state
				$prevDtlArray = array();
				$promoDtlsHashByCodeAndLevelNo = array();
				continue;
			}

			$dtlItem = ItemRepository::findByPk($dtlData['item_id']);

			//1.2) query the qualified promoHdr based on this detail attributes
			$hdrIdDtls = PromoDtlRepository::queryHdrIdByDocDtl($divisionId, $docDate,
			$debtorId, $custGroup01Id, $custGroup02Id, $custGroup03Id, $custGroup04Id, $custGroup05Id, 
			$dtlItem->id, $dtlItem->item_group_01_id, $dtlItem->item_group_02_id, $dtlItem->item_group_03_id, $dtlItem->item_group_04_id, $dtlItem->item_group_05_id);

			$hdrIds = array();
			foreach($hdrIdDtls as $hdrIdDtl)
			{
				$hdrIds[] = $hdrIdDtl->hdr_id;
			}		
			//1.3) find all the promoDtls of the qualified hdrIds
			$tmpPromoDtls = PromoDtlRepository::findAllByHdrIds($hdrIds);
			$tmpPromoDtls->load('promoHdr');
			
			//1.4) add the tmpPromoDtls to promoDtlsHashByCodeAndLevelNo
			//1.4) code is the promoHdr code, levelNo is for promotion multi level awards
			//1.4) promoDtls in the promoDtlsHashByCodeAndLevelNo is actually a hashtable by promoDtlId
			//1.4) to make sure the promoDtl is not duplicate, coz of the loop of dtlArray
			foreach($tmpPromoDtls as $tmpPromoDtl)
			{
				$tmpPromoHdr = $tmpPromoDtl->promoHdr;
				$key = $tmpPromoHdr->code.'/'.$tmpPromoDtl->level_no;

				$allPromoDtls = array();
				if(array_key_exists($key, $promoDtlsHashByCodeAndLevelNo))
				{
					$allPromoDtls = $promoDtlsHashByCodeAndLevelNo[$key];
				}
				$allPromoDtls[$tmpPromoDtl->id] = $tmpPromoDtl;
				$promoDtlsHashByCodeAndLevelNo[$key] = $allPromoDtls;
			}
			
			//1.5) sort the promoDtlsHashByCodeAndLevelNo by level_no DESC, code ASC
			//1.5) level DESC will make sure the promotion will try on higher level/fulfill first
			//1.5) for example, promotion A will have 3 level, lvl1 - buy 10 foc 1, lvl2 - buy 15 foc 2, lvl3 - buy 20 foc 3
			//1.5) so buy lvl3 - 20 foc 3 will be used to match with details first
			uksort($promoDtlsHashByCodeAndLevelNo, function ($a, $b) {    
				$aParts = explode('/',$a);
				$aCode = $aParts[0];
				$aLevelNo = $aParts[1];

				$bParts = explode('/',$b);
				$bCode = $bParts[0];
				$bLevelNo = $bParts[1];

				if($aLevelNo == $bLevelNo)
				{
					//code ASC
					return ($aCode < $bCode) ? -1 : 1;
				}
				//level_no DESC
				return ($aLevelNo < $bLevelNo) ? 1 : -1;
			});

			//1.6) add the current dtlData to prevDtlArray
			$prevDtlArray[] = $dtlData;

			//1.7) loop the promoDtlsHashByCodeAndLevelNo
			//1.7) verify the individual/group min_net_amt/min_unit_qty
			foreach($promoDtlsHashByCodeAndLevelNo as $key => $allPromoDtls)
			{
				$keyParts = explode('/',$key);
				$code = $keyParts[0];
				$levelNo = $keyParts[1];

				//1.7.1) format the promoDtl to hashtable by promoDtlId
				$promoDtlHashByPromoDtlId = array();
				//1.7.1) format the allPromoDtls to hashtable by groupNo
				$promoDtlsHashByGroupNo = array();
				foreach($allPromoDtls as $allPromoDtl)
				{
					$promoDtlHashByPromoDtlId[$allPromoDtl->id] = $allPromoDtl;

					$tmpPromoDtls = array();
					if(array_key_exists($allPromoDtl->group_no, $promoDtlsHashByGroupNo))
					{
						$tmpPromoDtls = $promoDtlsHashByGroupNo[$allPromoDtl->group_no];
					}
					$tmpPromoDtls[] = $allPromoDtl;

					$realGroupNo = $allPromoDtl->group_no;
					if(empty($realGroupNo))
					{
						$realGroupNo = $code.'/'.$allPromoDtl->id;
					}
					$promoDtlsHashByGroupNo[$realGroupNo] = $tmpPromoDtls;
				}

				//1.7.2) get the promotion group count, will be used to make sure all group criterias are fulfilled
				$promoGroupCount = count(array_keys($promoDtlsHashByGroupNo));
				$isFulfilled = true;
				$qualifiedDtlsHashByPromoDtlId = array();
				$qualifiedGroupNoHashByGroupNo = array();
				//1.7.2) used to mark the dtl that already matched with grpPromoDtl, to avoid it been double match
				$qualifiedDtlSeqHash = array();

				//1.7.3) loop the promotion groups, and verify the promotion group criterias, against the document details (prevDtlArray)
				//1.7.3) calculate the group ttlNetAmt and ttlUnitQty, by the matched prevDtlData
				foreach($promoDtlsHashByGroupNo as $groupNo => $grpPromoDtls)
				{
					$ttlNetAmt = 0;
					$ttlUnitQty = 0;
					$masterPromoDtl = null;
					//1.7.3.1) loop the group promoDtls to calculate the group ttlNetAmt and ttlUnitQty
					foreach($grpPromoDtls as $grpPromoDtl)
					{
						if(empty($masterPromoDtl))
						{
							$masterPromoDtl = $grpPromoDtl;
						}
						//1.7.3.1.1) loop the prevDtlArray
						foreach($prevDtlArray as $prevDtlSeq => $prevDtlData)
						{
							//1.7.3.1.1.1) calculate the ratio of total previous dtls unit qty, with current dtl unit qty
							//1.7.3.1.1.1) prevQtyRatio = prevUnitQty / ttlPrevUnitQty
							$prevUnitQty = bcmul($prevDtlData['qty'], $prevDtlData['uom_rate'], 10);
							$prevQtyRatio = 1;
							if(bccomp($ttlUnitQty, 0, 10) > 0)
							{
								$prevQtyRatio = bcdiv($prevUnitQty, $ttlUnitQty, 0);
							}
							//1.7.3.1.1.1) skip this detail if (prevQtyRatio < promotion foc ratio AND the current promoDtl is not foc promotion line)
							//1.7.3.1.1.1) this condition is to handle the case where criteria detail and foc promotion detail is same item/group
							if(bccomp($prevQtyRatio, $grpPromoDtl->foc_ratio, 10) < 0
							&& bccomp($grpPromoDtl->foc_qty, 0, 10) == 0)
							{
								continue;
							}
							//1.7.3.1.1.2) skip this detail if it already assigned to other promotion
							if($prevDtlData['promo_hdr_id'])
							{
								continue;
							}
							//1.7.3.1.1.3) skip this detail if it already assigned to previous grpPromoDtl, to avoid it been double match
							//1.7.3.1.1.3) for example, prevDtl A may be will assigned to promoDtl B and promoDtl C, where B and C is same promoHdrId and same levelNo, if this checking is not there
							if(array_key_exists($prevDtlSeq, $qualifiedDtlSeqHash))
							{
								continue;
							}
							$prevItem = ItemRepository::findByPk($prevDtlData['item_id']);
							if($this->isDtlItemMatchWithPromoDtl($prevItem, $grpPromoDtl) === true)
							{
								//1.7.3.1.1.4) check if this prevDtl item attributes match with this promoDtl
								$qualifiedDtls = array();
								if(array_key_exists($grpPromoDtl->id, $qualifiedDtlsHashByPromoDtlId))
								{
									$qualifiedDtls = $qualifiedDtlsHashByPromoDtlId[$grpPromoDtl->id];
								}
								$qualifiedDtls[] = $prevDtlData;
								//1.7.3.1.1.5) add to qualifiedDtlsHash for further processing
								$qualifiedDtlsHashByPromoDtlId[$grpPromoDtl->id] = $qualifiedDtls;
								$qualifiedGroupNoHashByGroupNo[$groupNo] = $groupNo;
								
								//1.7.3.1.1.6) mark this prevDtlData already assigned to grpPromoDtl, refer to 1.7.3.1.1.3
								$qualifiedDtlSeqHash[$prevDtlSeq] = $prevDtlSeq;

								//1.7.3.1.1.7) add this matched prevDtlData netAmt and unitQty to ttlNetAmt and ttlUnitQty, refer to 1.7.3
								$ttlNetAmt = bcadd($ttlNetAmt, $prevDtlData['net_amt'], 10);
								$ttlUnitQty = bcadd($ttlUnitQty, bcmul($prevDtlData['qty'], $prevDtlData['uom_rate'], 10), 10);
							}
						}
					}
					
					//1.7.3.2) after loop all the promoDtl within the group (hdrId/levelNo)
					//1.7.3.2) check wether this group criteria is fulfilled, mark it as false if fail
					if(bccomp($ttlNetAmt, $masterPromoDtl->min_net_amt, 10) < 0)
					{
						$isFulfilled = false;
					}
					if(bccomp($ttlUnitQty, $masterPromoDtl->min_unit_qty, 10) < 0)
					{
						$isFulfilled = false;
					}
					//1.7.3.3) if this promotion is package like, buy 2 @ RM5, buy 3 @ RM 10, then the ttlNetAmt/ttUnitQty need to be divide by minNetAmt/minUnitQty, with no remainder
					if($masterPromoDtl->is_package > 0)
					{
						if(bccomp($masterPromoDtl->min_net_amt, 0, 10) > 0)
						{
							$modulus = bcmod($ttlNetAmt, $masterPromoDtl->min_net_amt);
							if(bccomp($modulus, 0, 10) != 0)
							{
								$isFulfilled = false;
							}
						}
						if(bccomp($masterPromoDtl->min_unit_qty, 0, 10) > 0)
						{
							$modulus = bcmod($ttlUnitQty, $masterPromoDtl->min_unit_qty);
							if(bccomp($modulus, 0, 10) != 0)
							{
								$isFulfilled = false;
							}
						}
					}
				}

				//1.7.4) the matched dtlData group count must match with promotion group count
				//1.7.4) for example, the promotion may be have 3 group, group A requires at least 10 qty of red bull
				//1.7.4) group B requires at least 12 qty of attack, group c is foc 1 qty of pokka
				//1.7.4) the dtlData must match the group required by the promotion
				if(count($qualifiedGroupNoHashByGroupNo) != $promoGroupCount)
				{
					$isFulfilled = false;
				}
				
				if($isFulfilled === true)
				{
					//1.7.5) apply the promotion to the qualifiedDtls if all criterias fulfiled
					$ttlNetAmt = 0;
					$ttlUnitQty = 0;
					$maxNetAmt = 0;
					$maxUnitQty = 0;
					//1.7.6) generate the uuid for this promotion, later any update or delete, the promotion can be delete/update in group
					$promoUuid = uniqid();
					//1.7.7) loop the qualified dtlDatas, by promoDtlId, because some cases there will be multiple dtlData for 1 promoDtl
					//1.7.7) for example, promoDtl require 100 qty of red bull, and need 2 or 3 details to fulfill, like 1st detail have 50 qty, 2nd detail have 60 qty
					foreach($qualifiedDtlsHashByPromoDtlId as $promoDtlId => $qualifiedDtls)
					{
						$promoDtl = $promoDtlHashByPromoDtlId[$promoDtlId];
						foreach($qualifiedDtls as $qualifiedSeq => $qualifiedDtl)
						{
							if(bccomp($promoDtl->foc_qty, 0, 10) > 0)
							{
								//1.7.7.1) if this applied promotion is foc line, need to calculate the focQty
								$focUomId = $promoDtl->foc_uom_id;
								$focUomRate = $promoDtl->foc_uom_rate;
								$focQty = $promoDtl->foc_qty;
								if($promoDtl->is_foc_multiple > 0)
								{
									//1.7.7.2) we always assume foc line is the last line of the promotion
									//1.7.7.2) so ttlNetAmt/ttlUnitQty is accumulated from previous criteria detail lines
									$factor = 1;
									if(bccomp($maxNetAmt, 0, 10) > 0)
									{
										$factor = bcdiv($ttlNetAmt, $maxNetAmt, 0);
									}
									if(bccomp($maxUnitQty, 0, 10) > 0)
									{										
										$factor = bcdiv($ttlUnitQty, $maxUnitQty, 0);
									}
									$focQty = bcmul($focQty, $factor, 0);
								}
								
								$qualifiedDtl['uom_id'] = $focUomId;
								$qualifiedDtl['uom_rate'] = $focUomRate;
								$qualifiedDtl['qty'] = $focQty;
								$qualifiedDtl['dtl_disc_perc_01'] = 100;
							}
							else
							{
								//1.7.7.3) this is criteria (not foc) promotion line
								$ttlNetAmt = bcadd($ttlNetAmt, $qualifiedDtl['net_amt'], 10);
								$unitQty = bcmul($qualifiedDtl['qty'], $qualifiedDtl['uom_rate'], 10);
								$ttlUnitQty = bcadd($ttlUnitQty, $unitQty, 10);
								if(bccomp($promoDtl->min_net_amt, $maxNetAmt, 10) > 0)
								{
									$maxNetAmt = $promoDtl->min_net_amt;
								}
								if(bccomp($promoDtl->min_unit_qty, $maxUnitQty, 10) > 0)
								{
									$maxUnitQty = $promoDtl->min_unit_qty;
								}
							}
							//1.7.7.4) apply the price disc if it is not 0
							if(bccomp($promoDtl->price_disc, 0, 10) > 0)
							{
								$qualifiedDtl['price_disc'] = $promoDtl->price_disc;
							}
							//1.7.7.5) apply the disc val if it is not 0
							if(bccomp($promoDtl->min_disc_val_01, 0, 10) > 0)
							{
								$qualifiedDtl['dtl_disc_val_01'] = $promoDtl->min_disc_val_01;
							}
							if(bccomp($promoDtl->min_disc_val_02, 0, 10) > 0)
							{
								$qualifiedDtl['dtl_disc_val_02'] = $promoDtl->min_disc_val_02;
							}
							if(bccomp($promoDtl->min_disc_val_03, 0, 10) > 0)
							{
								$qualifiedDtl['dtl_disc_val_03'] = $promoDtl->min_disc_val_03;
							}
							if(bccomp($promoDtl->min_disc_val_04, 0, 10) > 0)
							{
								$qualifiedDtl['dtl_disc_val_04'] = $promoDtl->min_disc_val_04;
							}
							if(bccomp($promoDtl->min_disc_val_05, 0, 10) > 0)
							{
								$qualifiedDtl['dtl_disc_val_05'] = $promoDtl->min_disc_val_05;
							}
							//1.7.7.6) apply the disc perc if it is not 0
							if(bccomp($promoDtl->min_disc_perc_01, 0, 10) > 0)
							{
								$qualifiedDtl['dtl_disc_perc_01'] = $promoDtl->min_disc_perc_01;
							}
							if(bccomp($promoDtl->min_disc_perc_02, 0, 10) > 0)
							{
								$qualifiedDtl['dtl_disc_perc_02'] = $promoDtl->min_disc_perc_02;
							}
							if(bccomp($promoDtl->min_disc_perc_03, 0, 10) > 0)
							{
								$qualifiedDtl['dtl_disc_perc_03'] = $promoDtl->min_disc_perc_03;
							}
							if(bccomp($promoDtl->min_disc_perc_04, 0, 10) > 0)
							{
								$qualifiedDtl['dtl_disc_perc_04'] = $promoDtl->min_disc_perc_04;
							}
							if(bccomp($promoDtl->min_disc_perc_05, 0, 10) > 0)
							{
								$qualifiedDtl['dtl_disc_perc_05'] = $promoDtl->min_disc_perc_05;
							}				
							//1.7.7.7) assign the promotion id and uuid to this detail line
							$qualifiedDtl['promo_hdr_id'] = $promoDtl->hdr_id;	
							$qualifiedDtl['promo_uuid'] = $promoUuid;
							//1.7.7.8) set this detail line is_modified = 1
							$qualifiedDtl['is_modified'] = 1;
							$modifiedDtlArray[] = $qualifiedDtl;
						}
					}

					//reset the state if promotion is applied					
					if(!empty($prevDtlArray))
					{
						//promo_hdr_id is 0 and prevDtlArray is not null
						//then need to close the prevDtlArray
						$prevDtlArray = array();
					}

					if(!empty($promoDtlsHashByCodeAndLevelNo))
					{
						//promo_hdr_id is 0 and promoDtlsHashByCodeAndLevelNo is not null
						//then need to close the promoDtlsHashByCodeAndLevelNo
						$promoDtlsHashByCodeAndLevelNo = array();
					}
				}
			}
		}
		return $modifiedDtlArray;
	}

	protected function calculateAmount($hdrData, $dtlArray)
  {
		$modifiedDtlArray = array();
		
		//1) taxCalc array used to store the variables for tax adaptive rounding
		$taxCalc = array(
			'dtl_prev_unrounded_ttl_01' => 0,
			'dtl_prev_unrounded_ttl_02' => 0,
			'dtl_prev_unrounded_ttl_03' => 0,
			'dtl_prev_unrounded_ttl_04' => 0,
			'dtl_prev_unrounded_ttl_05' => 0,

			'dtl_prev_rounded_ttl_01' => 0,
			'dtl_prev_rounded_ttl_02' => 0,
			'dtl_prev_rounded_ttl_03' => 0,
			'dtl_prev_rounded_ttl_04' => 0,
			'dtl_prev_rounded_ttl_05' => 0,

			'hdr_prev_unrounded_ttl_01' => 0,
			'hdr_prev_unrounded_ttl_02' => 0,
			'hdr_prev_unrounded_ttl_03' => 0,
			'hdr_prev_unrounded_ttl_04' => 0,
			'hdr_prev_unrounded_ttl_05' => 0,

			'hdr_prev_rounded_ttl_01' => 0,
			'hdr_prev_rounded_ttl_02' => 0,
			'hdr_prev_rounded_ttl_03' => 0,
			'hdr_prev_rounded_ttl_04' => 0,
			'hdr_prev_rounded_ttl_05' => 0,
		);

		$ttlDtlGrossAmt = 0;
		$ttlDtlGrossLocalAmt = 0;
		$ttlDtlDiscAmt = 0;
		$ttlDtlDiscLocalAmt = 0;
		$ttlHdrDiscAmt = 0;
		$ttlHdrDiscLocalAmt = 0;
		$ttlDtlTaxAmt = 0;
		$ttlDtlTaxLocalAmt = 0;
		$ttlHdrTaxableAmt = 0;
		$ttlHdrTaxAmt = 0;
		$ttlHdrTaxLocalAmt = 0;
		$ttlDtlNetAmt = 0;
		$ttlDtlNetLocalAmt = 0;

		//2) loop the document details, here assume all the details of this document
		foreach($dtlArray as $dtlSeq => $dtlData)
		{
			//2.1) check if this detail is modified 
			if($dtlData['is_modified'] > 0)
			{
				//2.1.1) this details is modified, need to recalculate the detail amount
				//2.1.2) support priceDisc, netPrice = salePrice - priceDisc
				$netPrice = bcsub($dtlData['sale_price'], $dtlData['price_disc'], 2);
				
				//2.1.3) process detail disc
				//2.1.3) dtlDiscAmt1 -> dtlDiscPerc1 -> dtlDiscAmt2 -> dtlDiscPerc2
				$dtlDiscAmt = 0;
				$bfDiscAmt = $netPrice;
				for ($a = 1; $a <= 5; $a++) 
				{
					$afDiscValAmt = bcsub($bfDiscAmt, $dtlData['dtl_disc_val_0'.$a], 2);
					$discPercAmt = bcmul($afDiscValAmt, bcdiv($dtlData['dtl_disc_perc_0'.$a], 100, 2), 2);
					$afDiscPercAmt = bcsub($afDiscValAmt, $discPercAmt, 2);
					$bfDiscAmt = $afDiscPercAmt;

					$ttlDiscAmt = bcadd($dtlData['dtl_disc_val_0'.$a], $discPercAmt, 2);
					$dtlDiscAmt = bcadd($dtlDiscAmt, $ttlDiscAmt, 2);
				}			
				$dtlData['dtl_disc_amt'] = round($dtlDiscAmt, config('scm.decimal_scale'));
				
				//2.1.4) process header disc
				//2.1.4) hdrDiscAmt1 -> hdrDiscPerc1 -> hdrDiscAmt2 -> hdrDiscPerc2
				$hdrDiscAmt = 0;
				for ($a = 1; $a <= 5; $a++)
				{
					$afDiscValAmt = bcsub($bfDiscAmt, $dtlData['hdr_disc_val_0'.$a], 2);
					$discPercAmt = bcmul($afDiscValAmt, bcdiv($dtlData['hdr_disc_perc_0'.$a], 100, 2), 2);
					$afDiscPercAmt = bcsub($afDiscValAmt, $discPercAmt, 2);
					$bfDiscAmt = $afDiscPercAmt;

					$ttlDiscAmt = bcadd($dtlData['hdr_disc_val_0'.$a], $discPercAmt, 2);
					$hdrDiscAmt = bcadd($hdrDiscAmt, $ttlDiscAmt, 2);
				}
				$dtlData['hdr_disc_amt'] = round($hdrDiscAmt, config('scm.decimal_scale'));
				$dtlData['gross_amt'] = bcmul($dtlData['qty'], $bfDiscAmt, config('scm.decimal_scale'));
				
				//2.1.5) process detail tax, use adaptive rounding
				$bfTaxAmt = round($dtlData['gross_amt'], config('scm.decimal_scale'));
				for ($a = 1; $a <= 5; $a++)
				{			
					//2.1.5.1) update the last calculatedAmt to taxableAmt
					$dtlData['dtl_taxable_amt_0'.$a] = $bfTaxAmt;
					//2.1.5.1) assume tax val and tax perc will not be set at the same time, escpecially inclusive tax
					if($dtlData['dtl_tax_incl_0'.$a] > 0)
					{
						//2.1.5.1.1) if this is inclusive tax
						//2.1.5.1.2) process the taxVal first
						$afTaxValAmt = $bfTaxAmt;
						//2.1.5.1.3) process the taxPerc, taxableAmt = dtlAmt / (100 + taxPerc) / 100
						//2.1.5.1.3) GST 6% and dtlAmt 1, taxableAmt = 1 / 1.06 = 0.9433962264
						$tmpDivisor = bcadd($dtlData['dtl_tax_perc_0'.$a], 100, 2);
						$tmpDivisor = bcdiv($tmpDivisor, 100, 2);
						//2.1.5.1.3) afTaxValAmt here is the taxableAmt
						$afTaxValAmt = bcdiv($bfTaxAmt, $tmpDivisor, 2); //0.9433962264
					}
					else
					{
						//2.1.5.1.1) if this is non inclusive tax
						//2.1.5.1.2) process the taxVal first
						$afTaxValAmt = bcadd($bfTaxAmt, $dtlData['dtl_tax_val_0'.$a], 2);
					}
					//2.1.5.2) process the tax perc
					//2.1.5.3) calculate the unroundedTaxAmt by tax perc
					$dtlUnroundedTaxAmt = bcmul($afTaxValAmt, bcdiv($dtlData['dtl_tax_perc_0'.$a], 100, 2), 2); //0.056603773584
					//2.1.5.4) add the taxCalc prevUnroundedTtl with the current unroundedTaxAmt
					$taxCalc['dtl_prev_unrounded_ttl_0'.$a] = bcadd($taxCalc['dtl_prev_unrounded_ttl_0'.$a], $dtlUnroundedTaxAmt, 2);
					//2.1.5.5) round the taxCalc prevUnroundedTtl (total unrounded tax amt)
					$roundedTtlTaxAmt = round($taxCalc['dtl_prev_unrounded_ttl_0'.$a], config('scm.decimal_scale')); //0.06, 0.11
					//2.1.5.6) calculate adaptive rounding tax amt, by deduct rounded ttl tax with prev rounded ttl tax
					$dtlTaxAmt = bcsub($roundedTtlTaxAmt, $taxCalc['dtl_prev_rounded_ttl_0'.$a], config('scm.decimal_scale')); //0.06, 0.05
					//2.1.5.7) consider the detail tax adjustment amt also
					$dtlData['dtl_tax_amt_0'.$a] = bcadd($dtlTaxAmt, $dtlData['dtl_tax_adj_0'.$a], config('scm.decimal_scale')); //0.07, 0.05
					//2.1.5.8) update the taxCalc ttl rounded tax amt
					$taxCalc['dtl_prev_rounded_ttl_0'.$a] = $roundedTtlTaxAmt; //0.06, 0.11
					$dtlData['dtl_tax_amt_0'.$a] = bcadd($dtlData['dtl_tax_amt_0'.$a], $dtlData['dtl_tax_val_0'.$a], config('scm.decimal_scale'));

					if($dtlData['dtl_tax_incl_0'.$a] > 0)
					{
						//2.1.5.9) if this is inclusive tax, need to recalculate the taxableAmt
						//inclusive tax
						$dtlData['dtl_taxable_amt_0'.$a] = bcsub($bfTaxAmt, $dtlData['dtl_tax_amt_0'.$a], config('scm.decimal_scale'));
					}

					$afTaxPercAmt = bcadd($dtlData['dtl_taxable_amt_0'.$a], $dtlData['dtl_tax_amt_0'.$a], config('scm.decimal_scale'));
					$bfTaxAmt = $afTaxPercAmt;
				}

				//2.1.6) process header tax, adaptive rounding, refer to 2.1.5
				$bfTaxAmt = round($afTaxPercAmt, config('scm.decimal_scale'));
				for ($a = 1; $a <= 5; $a++)
				{			
					$dtlData['hdr_taxable_amt_0'.$a] = $bfTaxAmt;
					//assume tax val and tax perc will not be set at the same time, escpecially inclusive tax
					if($dtlData['hdr_tax_incl_0'.$a] > 0)
					{
						//inclusive tax
						//process the tax val first
						$bfTaxAmt = bcsub($bfTaxAmt, $dtlData['hdr_tax_val_0'.$a], config('scm.decimal_scale'));
						$afTaxValAmt = bcadd($bfTaxAmt, $dtlData['hdr_tax_val_0'.$a], config('scm.decimal_scale'));
						//process the tax perc
						$tmpDivisor = bcadd($dtlData['hdr_tax_perc_0'.$a], 100, 2);
						$tmpDivisor = bcdiv($tmpDivisor, 100, 2);
						$afTaxValAmt = bcdiv($bfTaxAmt, $tmpDivisor, 2); //0.9433962264
					}
					else
					{
						//non inclusive tax
						//process the tax val first
						$afTaxValAmt = bcadd($bfTaxAmt, $dtlData['hdr_tax_val_0'.$a], 2);
					}
					//process the tax perc
					//calculate the unrounded tax amt by tax perc
					$dtlUnroundedTaxAmt = bcmul($afTaxValAmt, bcdiv($dtlData['hdr_tax_perc_0'.$a], 100, 2), 2); //0.056603773584
					//get the prev unrounded ttl and add with the current unrounded tax amt
					$taxCalc['hdr_prev_unrounded_ttl_0'.$a] = bcadd($taxCalc['hdr_prev_unrounded_ttl_0'.$a], $dtlUnroundedTaxAmt, 2);
					//round the tax amt
					$roundedTtlTaxAmt = round($taxCalc['hdr_prev_unrounded_ttl_0'.$a], config('scm.decimal_scale')); //0.06, 0.11
					//calculate adaptive rounding tax amt, by deduct rounded ttl tax with prev rounded ttl tax
					$dtlTaxAmt = bcsub($roundedTtlTaxAmt, $taxCalc['hdr_prev_rounded_ttl_0'.$a], config('scm.decimal_scale')); //0.06, 0.05
					$dtlData['hdr_tax_amt_0'.$a] = bcadd($dtlTaxAmt, $dtlData['hdr_tax_adj_0'.$a], config('scm.decimal_scale')); //0.07, 0.05
					//update the the ttl rounded tax amt
					$taxCalc['hdr_prev_rounded_ttl_0'.$a] = $roundedTtlTaxAmt; //0.06, 0.11
					$dtlData['hdr_tax_amt_0'.$a] = bcadd($dtlData['hdr_tax_amt_0'.$a], $dtlData['hdr_tax_val_0'.$a], config('scm.decimal_scale'));

					if($dtlData['hdr_tax_incl_0'.$a] > 0)
					{
						//inclusive tax
						$dtlData['hdr_taxable_amt_0'.$a] = bcsub($bfTaxAmt, $dtlData['hdr_tax_amt_0'.$a], config('scm.decimal_scale'));
					}

					$afTaxPercAmt = bcadd($dtlData['hdr_taxable_amt_0'.$a], $dtlData['hdr_tax_amt_0'.$a], config('scm.decimal_scale'));
					$bfTaxAmt = $afTaxPercAmt;
				}
				//2.1.7) get the detail netAmt 
				$dtlData['net_amt'] = $afTaxPercAmt;

				//2.1.8) process currency rate
				$dtlData['gross_local_amt'] = bcmul($dtlData['gross_amt'], $hdrData['currency_rate'], config('scm.decimal_scale'));
				for ($a = 1; $a <= 5; $a++)
				{	
					$dtlData['dtl_tax_local_amt_0'.$a] = bcmul($dtlData['dtl_tax_amt_0'.$a], $hdrData['currency_rate'], config('scm.decimal_scale'));
				}
				for ($a = 1; $a <= 5; $a++)
				{	
					$dtlData['hdr_tax_local_amt_0'.$a] = bcmul($dtlData['hdr_tax_amt_0'.$a], $hdrData['currency_rate'], config('scm.decimal_scale'));
				}
				$dtlData['dtl_disc_local_amt'] = bcmul($dtlData['dtl_disc_amt'], $hdrData['currency_rate'], config('scm.decimal_scale'));
				$dtlData['hdr_disc_local_amt'] = bcmul($dtlData['hdr_disc_amt'], $hdrData['currency_rate'], config('scm.decimal_scale'));
				$dtlData['net_local_amt'] = bcmul($dtlData['net_amt'], $hdrData['currency_rate'], config('scm.decimal_scale'));

				$modifiedDtlArray[] = $dtlData;
			}
			
			//2.2) calculate the header amount, by every detail amount
			$ttlDtlGrossAmt = bcadd($ttlDtlGrossAmt, $dtlData['gross_amt'], config('scm.decimal_scale'));
			$ttlDtlGrossLocalAmt = bcadd($ttlDtlGrossLocalAmt, $dtlData['gross_local_amt'], config('scm.decimal_scale'));
			$ttlDtlDiscAmt = bcadd($ttlDtlDiscAmt, $dtlData['dtl_disc_amt'], config('scm.decimal_scale'));
			$ttlDtlDiscLocalAmt = bcadd($ttlDtlDiscLocalAmt, $dtlData['dtl_disc_local_amt'], config('scm.decimal_scale'));
			$ttlHdrDiscAmt = bcadd($ttlHdrDiscAmt, $dtlData['hdr_disc_amt'], config('scm.decimal_scale'));
			$ttlHdrDiscLocalAmt = bcadd($ttlHdrDiscLocalAmt, $dtlData['hdr_disc_local_amt'], config('scm.decimal_scale'));

			//2.3) calculate the header total tax amount
			$dtlTaxAmt = 0;
			$dtlTaxLocalAmt = 0;
			for ($a = 1; $a <= 5; $a++)
			{	
				$dtlTaxAmt = bcadd($dtlTaxAmt, $dtlData['dtl_tax_amt_0'.$a], 2);
				$dtlTaxLocalAmt = bcadd($dtlTaxLocalAmt, $dtlData['dtl_tax_local_amt_0'.$a], 2);
			}
			$ttlDtlTaxAmt = bcadd($ttlDtlTaxAmt, $dtlTaxAmt, config('scm.decimal_scale'));
			$ttlDtlTaxLocalAmt = bcadd($ttlDtlTaxLocalAmt, $dtlTaxLocalAmt, config('scm.decimal_scale'));

			$hdrTaxAmt = 0;
			$hdrTaxLocalAmt = 0;
			for ($a = 1; $a <= 5; $a++)
			{	
				$hdrTaxAmt = bcadd($hdrTaxAmt, $dtlData['hdr_tax_amt_0'.$a], 2);
				$hdrTaxLocalAmt = bcadd($hdrTaxLocalAmt, $dtlData['hdr_tax_local_amt_0'.$a], 2);
			}
			$ttlHdrTaxAmt = bcadd($ttlHdrTaxAmt, $hdrTaxAmt, config('scm.decimal_scale'));
			$ttlHdrTaxLocalAmt = bcadd($ttlHdrTaxLocalAmt, $hdrTaxLocalAmt, config('scm.decimal_scale'));
			//2.4) calculate the header net amount
			$ttlDtlNetAmt = bcadd($ttlDtlNetAmt, $dtlData['net_amt'], config('scm.decimal_scale'));
			$ttlDtlNetLocalAmt = bcadd($ttlDtlNetLocalAmt, $dtlData['net_local_amt'], config('scm.decimal_scale'));
		}

		//3) assign the header amounts
		$hdrData['gross_amt'] = bcsub($ttlDtlGrossAmt, $ttlDtlDiscAmt, config('scm.decimal_scale'));
		$hdrData['gross_local_amt'] = bcsub($ttlDtlGrossLocalAmt, $ttlDtlDiscLocalAmt, config('scm.decimal_scale'));
		$hdrData['disc_amt'] = $ttlHdrDiscAmt;
		$hdrData['disc_local_amt'] = $ttlHdrDiscLocalAmt;
		//$hdrData['tax_amt'] = bcadd($ttlDtlTaxAmt, $ttlHdrTaxAmt, config('scm.decimal_scale'));
		$hdrData['tax_amt'] = $ttlHdrTaxAmt; //standardise with hdr disc_amt
		//$hdrData['tax_local_amt'] = bcadd($ttlDtlTaxLocalAmt, $ttlHdrTaxLocalAmt, config('scm.decimal_scale'));
		$hdrData['tax_local_amt'] = $ttlHdrTaxLocalAmt; //standardise with hdr disc_amt
		$hdrData['net_amt'] = $ttlDtlNetAmt;
		$hdrData['net_local_amt'] = $ttlDtlNetLocalAmt;
		if($hdrData['is_round_adj'] == 1)
		{
			//4) this header need to do 5 sen rounding
			$amtToRound = 0.00;		
			$netAmtBeforeAdj = $hdrData['net_amt'];
			$netAmtInRound = bcmul($netAmtBeforeAdj, 100, 2);
			$lastDigit = round(fmod($netAmtInRound, 10), 0);
			if ($lastDigit == 1 || $lastDigit == 2)
			{
				$amtToRound = bcsub(0, $lastDigit, 10);
			}
			else if ($lastDigit == 3 || $lastDigit == 4)
			{
				$amtToRound = bcsub(5, $lastDigit, 10);
			}
			else if ($lastDigit == 6 || $lastDigit == 7)
			{
				$amtToRound = bcsub(5, $lastDigit, 10);
			}
			else if ($lastDigit == 8 || $lastDigit == 9)
			{
				$amtToRound = bcsub(10, $lastDigit, 10);
			}
			$hdrData['round_adj_amt'] = bcdiv($amtToRound, 100, config('scm.decimal_scale'));
			$hdrData['net_amt'] = bcadd($hdrData['net_amt'], $hdrData['round_adj_amt'], config('scm.decimal_scale'));
			
			$hdrData['round_adj_local_amt'] = bcmul($hdrData['round_adj_amt'], $hdrData['currency_rate'], config('scm.decimal_scale'));
			$hdrData['net_local_amt'] = bcadd($hdrData['net_local_amt'], $hdrData['round_adj_local_amt'], config('scm.decimal_scale'));
		}

		return array(
			'hdrData' => $hdrData,
			'dtlArray' => $modifiedDtlArray
		);
	}

	protected function initOutbOrdDtlData($data = array())
	{
		$newModelData = array();
		$newModelData['id'] = 0;
		$newModelData['hdr_id'] = 0; 

		$newModelData['line_no'] = 0;
		$newModelData['item_id'] = 0;
		$newModelData['desc_01'] = '';
		$newModelData['desc_02'] = '';

		$newModelData['location_id'] = 0;
				
		$newModelData['uom_id'] = 0;
		$newModelData['uom_rate'] = 0;
		$newModelData['sale_price'] = 0;
		$newModelData['price_disc'] = 0;
		$newModelData['qty'] = 0;

		$newModelData['gross_amt'] = 0;
		$newModelData['gross_local_amt'] = 0;

		$newModelData['promo_hdr_id'] = 0;
		$newModelData['promo_uuid'] = '';
		$newModelData['dtl_disc_val_01'] = 0;
		$newModelData['dtl_disc_perc_01'] = 0;
		$newModelData['dtl_disc_val_02'] = 0;
		$newModelData['dtl_disc_perc_02'] = 0;
		$newModelData['dtl_disc_val_03'] = 0;
		$newModelData['dtl_disc_perc_03'] = 0;
		$newModelData['dtl_disc_val_04'] = 0;
		$newModelData['dtl_disc_perc_04'] = 0;
		$newModelData['dtl_disc_val_05'] = 0;
		$newModelData['dtl_disc_perc_05'] = 0;
		$newModelData['dtl_disc_amt'] = 0;
		$newModelData['dtl_disc_local_amt'] = 0;

		$newModelData['hdr_disc_val_01'] = 0;
		$newModelData['hdr_disc_perc_01'] = 0;
		$newModelData['hdr_disc_val_02'] = 0;
		$newModelData['hdr_disc_perc_02'] = 0;
		$newModelData['hdr_disc_val_03'] = 0;
		$newModelData['hdr_disc_perc_03'] = 0;
		$newModelData['hdr_disc_val_04'] = 0;
		$newModelData['hdr_disc_perc_04'] = 0;
		$newModelData['hdr_disc_val_05'] = 0;
		$newModelData['hdr_disc_perc_05'] = 0;
		$newModelData['hdr_disc_amt'] = 0;
		$newModelData['hdr_disc_local_amt'] = 0;

		$newModelData['tax_hdr_id'] = 0;
		$newModelData['dtl_taxable_amt_01'] = 0;
		$newModelData['dtl_tax_incl_01'] = 0;
		$newModelData['dtl_tax_val_01'] = 0;
		$newModelData['dtl_tax_perc_01'] = 0;
		$newModelData['dtl_tax_adj_01'] = 0;
		$newModelData['dtl_tax_amt_01'] = 0;
		$newModelData['dtl_tax_local_amt_01'] = 0;

		$newModelData['dtl_taxable_amt_02'] = 0;
		$newModelData['dtl_tax_incl_02'] = 0;
		$newModelData['dtl_tax_val_02'] = 0;
		$newModelData['dtl_tax_perc_02'] = 0;
		$newModelData['dtl_tax_adj_02'] = 0;
		$newModelData['dtl_tax_amt_02'] = 0;
		$newModelData['dtl_tax_local_amt_02'] = 0;

		$newModelData['dtl_taxable_amt_03'] = 0;
		$newModelData['dtl_tax_incl_03'] = 0;
		$newModelData['dtl_tax_val_03'] = 0;
		$newModelData['dtl_tax_perc_03'] = 0;
		$newModelData['dtl_tax_adj_03'] = 0;
		$newModelData['dtl_tax_amt_03'] = 0;
		$newModelData['dtl_tax_local_amt_03'] = 0;

		$newModelData['dtl_taxable_amt_04'] = 0;
		$newModelData['dtl_tax_incl_04'] = 0;
		$newModelData['dtl_tax_val_04'] = 0;
		$newModelData['dtl_tax_perc_04'] = 0;
		$newModelData['dtl_tax_adj_04'] = 0;
		$newModelData['dtl_tax_amt_04'] = 0;
		$newModelData['dtl_tax_local_amt_04'] = 0;

		$newModelData['dtl_taxable_amt_05'] = 0;
		$newModelData['dtl_tax_incl_05'] = 0;
		$newModelData['dtl_tax_val_05'] = 0;
		$newModelData['dtl_tax_perc_05'] = 0;
		$newModelData['dtl_tax_adj_05'] = 0;
		$newModelData['dtl_tax_amt_05'] = 0;
		$newModelData['dtl_tax_local_amt_05'] = 0;

		$newModelData['hdr_taxable_amt_01'] = 0;
		$newModelData['hdr_tax_incl_01'] = 0;
		$newModelData['hdr_tax_val_01'] = 0;
		$newModelData['hdr_tax_perc_01'] = 0;
		$newModelData['hdr_tax_adj_01'] = 0;
		$newModelData['hdr_tax_amt_01'] = 0;
		$newModelData['hdr_tax_local_amt_01'] = 0;

		$newModelData['hdr_taxable_amt_02'] = 0;
		$newModelData['hdr_tax_incl_02'] = 0;
		$newModelData['hdr_tax_val_02'] = 0;
		$newModelData['hdr_tax_perc_02'] = 0;
		$newModelData['hdr_tax_adj_02'] = 0;
		$newModelData['hdr_tax_amt_02'] = 0;
		$newModelData['hdr_tax_local_amt_02'] = 0;

		$newModelData['hdr_taxable_amt_03'] = 0;
		$newModelData['hdr_tax_incl_03'] = 0;
		$newModelData['hdr_tax_val_03'] = 0;
		$newModelData['hdr_tax_perc_03'] = 0;
		$newModelData['hdr_tax_adj_03'] = 0;
		$newModelData['hdr_tax_amt_03'] = 0;
		$newModelData['hdr_tax_local_amt_03'] = 0;

		$newModelData['hdr_taxable_amt_04'] = 0;
		$newModelData['hdr_tax_incl_04'] = 0;
		$newModelData['hdr_tax_val_04'] = 0;
		$newModelData['hdr_tax_perc_04'] = 0;
		$newModelData['hdr_tax_adj_04'] = 0;
		$newModelData['hdr_tax_amt_04'] = 0;
		$newModelData['hdr_tax_local_amt_04'] = 0;

		$newModelData['hdr_taxable_amt_05'] = 0;
		$newModelData['hdr_tax_incl_05'] = 0;
		$newModelData['hdr_tax_val_05'] = 0;
		$newModelData['hdr_tax_perc_05'] = 0;
		$newModelData['hdr_tax_adj_05'] = 0;
		$newModelData['hdr_tax_amt_05'] = 0;
		$newModelData['hdr_tax_local_amt_05'] = 0;

		$newModelData['net_amt'] = 0;
		$newModelData['net_local_amt'] = 0;

		foreach($data as $fieldName => $value)
		{
			$newModelData[$fieldName] = $value;
		}
		return $newModelData;
	}

	protected function initSlsInvDtlData($data = array())
	{
		$newModelData = array();
		$newModelData['id'] = 0;
		$newModelData['hdr_id'] = 0; 
		$newModelData['outb_ord_hdr_id'] = 0;
		$newModelData['outb_ord_dtl_id'] = 0;

		$newModelData['line_no'] = 0;
		$newModelData['item_id'] = 0;
		$newModelData['desc_01'] = '';
		$newModelData['desc_02'] = '';

		$newModelData['location_id'] = 0;
				
		$newModelData['uom_id'] = 0;
		$newModelData['uom_rate'] = 0;
		$newModelData['sale_price'] = 0;
		$newModelData['price_disc'] = 0;
		$newModelData['qty'] = 0;

		$newModelData['gross_amt'] = 0;
		$newModelData['gross_local_amt'] = 0;

		$newModelData['promo_hdr_id'] = 0;
		$newModelData['promo_uuid'] = '';
		$newModelData['dtl_disc_val_01'] = 0;
		$newModelData['dtl_disc_perc_01'] = 0;
		$newModelData['dtl_disc_val_02'] = 0;
		$newModelData['dtl_disc_perc_02'] = 0;
		$newModelData['dtl_disc_val_03'] = 0;
		$newModelData['dtl_disc_perc_03'] = 0;
		$newModelData['dtl_disc_val_04'] = 0;
		$newModelData['dtl_disc_perc_04'] = 0;
		$newModelData['dtl_disc_val_05'] = 0;
		$newModelData['dtl_disc_perc_05'] = 0;
		$newModelData['dtl_disc_amt'] = 0;
		$newModelData['dtl_disc_local_amt'] = 0;

		$newModelData['hdr_disc_val_01'] = 0;
		$newModelData['hdr_disc_perc_01'] = 0;
		$newModelData['hdr_disc_val_02'] = 0;
		$newModelData['hdr_disc_perc_02'] = 0;
		$newModelData['hdr_disc_val_03'] = 0;
		$newModelData['hdr_disc_perc_03'] = 0;
		$newModelData['hdr_disc_val_04'] = 0;
		$newModelData['hdr_disc_perc_04'] = 0;
		$newModelData['hdr_disc_val_05'] = 0;
		$newModelData['hdr_disc_perc_05'] = 0;
		$newModelData['hdr_disc_amt'] = 0;
		$newModelData['hdr_disc_local_amt'] = 0;

		$newModelData['tax_hdr_id'] = 0;
		$newModelData['dtl_taxable_amt_01'] = 0;
		$newModelData['dtl_tax_incl_01'] = 0;
		$newModelData['dtl_tax_val_01'] = 0;
		$newModelData['dtl_tax_perc_01'] = 0;
		$newModelData['dtl_tax_adj_01'] = 0;
		$newModelData['dtl_tax_amt_01'] = 0;
		$newModelData['dtl_tax_local_amt_01'] = 0;

		$newModelData['dtl_taxable_amt_02'] = 0;
		$newModelData['dtl_tax_incl_02'] = 0;
		$newModelData['dtl_tax_val_02'] = 0;
		$newModelData['dtl_tax_perc_02'] = 0;
		$newModelData['dtl_tax_adj_02'] = 0;
		$newModelData['dtl_tax_amt_02'] = 0;
		$newModelData['dtl_tax_local_amt_02'] = 0;

		$newModelData['dtl_taxable_amt_03'] = 0;
		$newModelData['dtl_tax_incl_03'] = 0;
		$newModelData['dtl_tax_val_03'] = 0;
		$newModelData['dtl_tax_perc_03'] = 0;
		$newModelData['dtl_tax_adj_03'] = 0;
		$newModelData['dtl_tax_amt_03'] = 0;
		$newModelData['dtl_tax_local_amt_03'] = 0;

		$newModelData['dtl_taxable_amt_04'] = 0;
		$newModelData['dtl_tax_incl_04'] = 0;
		$newModelData['dtl_tax_val_04'] = 0;
		$newModelData['dtl_tax_perc_04'] = 0;
		$newModelData['dtl_tax_adj_04'] = 0;
		$newModelData['dtl_tax_amt_04'] = 0;
		$newModelData['dtl_tax_local_amt_04'] = 0;

		$newModelData['dtl_taxable_amt_05'] = 0;
		$newModelData['dtl_tax_incl_05'] = 0;
		$newModelData['dtl_tax_val_05'] = 0;
		$newModelData['dtl_tax_perc_05'] = 0;
		$newModelData['dtl_tax_adj_05'] = 0;
		$newModelData['dtl_tax_amt_05'] = 0;
		$newModelData['dtl_tax_local_amt_05'] = 0;

		$newModelData['hdr_taxable_amt_01'] = 0;
		$newModelData['hdr_tax_incl_01'] = 0;
		$newModelData['hdr_tax_val_01'] = 0;
		$newModelData['hdr_tax_perc_01'] = 0;
		$newModelData['hdr_tax_adj_01'] = 0;
		$newModelData['hdr_tax_amt_01'] = 0;
		$newModelData['hdr_tax_local_amt_01'] = 0;

		$newModelData['hdr_taxable_amt_02'] = 0;
		$newModelData['hdr_tax_incl_02'] = 0;
		$newModelData['hdr_tax_val_02'] = 0;
		$newModelData['hdr_tax_perc_02'] = 0;
		$newModelData['hdr_tax_adj_02'] = 0;
		$newModelData['hdr_tax_amt_02'] = 0;
		$newModelData['hdr_tax_local_amt_02'] = 0;

		$newModelData['hdr_taxable_amt_03'] = 0;
		$newModelData['hdr_tax_incl_03'] = 0;
		$newModelData['hdr_tax_val_03'] = 0;
		$newModelData['hdr_tax_perc_03'] = 0;
		$newModelData['hdr_tax_adj_03'] = 0;
		$newModelData['hdr_tax_amt_03'] = 0;
		$newModelData['hdr_tax_local_amt_03'] = 0;

		$newModelData['hdr_taxable_amt_04'] = 0;
		$newModelData['hdr_tax_incl_04'] = 0;
		$newModelData['hdr_tax_val_04'] = 0;
		$newModelData['hdr_tax_perc_04'] = 0;
		$newModelData['hdr_tax_adj_04'] = 0;
		$newModelData['hdr_tax_amt_04'] = 0;
		$newModelData['hdr_tax_local_amt_04'] = 0;

		$newModelData['hdr_taxable_amt_05'] = 0;
		$newModelData['hdr_tax_incl_05'] = 0;
		$newModelData['hdr_tax_val_05'] = 0;
		$newModelData['hdr_tax_perc_05'] = 0;
		$newModelData['hdr_tax_adj_05'] = 0;
		$newModelData['hdr_tax_amt_05'] = 0;
		$newModelData['hdr_tax_local_amt_05'] = 0;

		$newModelData['net_amt'] = 0;
		$newModelData['net_local_amt'] = 0;

		foreach($data as $fieldName => $value)
		{
			$newModelData[$fieldName] = $value;
		}
		return $newModelData;
	}

	protected function tallyInbOrd($hdrData, $dtlArray, $delDtlArray = array())
  	{
		$inbOrdHdrData = array();
		$inbOrdDtlArray = array();
		$delInbOrdDtlArray = array();

		/*
		$inbOrdHdrModel = InbOrdHdrRepository::findByPk($hdrData['inb_ord_hdr_id']);
		if(empty($inbOrdHdrModel))
		{
			return array(
				'inbOrdHdrData' => $inbOrdHdrData,
				'inbOrdDtlArray' => $inbOrdDtlArray,
				'delInbOrdDtlArray' => $delInbOrdDtlArray
			);
		}

		$inbOrdHdrData = $inbOrdHdrModel->toArray();
		foreach($hdrData as $fieldName => $value)
		{
			if(strcmp($fieldName, 'inb_ord_hdr_id') == 0)
			{
				continue;
			}
			$inbOrdHdrData[$fieldName] = $value;
		}

		foreach($dtlArray as $dtlData)
		{
			$inbOrdDtlData = array();
			if (strpos($dtlData['id'], 'NEW') !== false) 
      		{
				$inbOrdDtlData = $this->initInbOrdDtlData();
				//temporarily use dtl uuid as inbOrdDtlId
				$inbOrdDtlData['id'] = $dtlData['id'];
			}
			else
			{
				$inbOrdDtlModel = InbOrdDtlRepository::findByPk($dtlData['inb_ord_dtl_id']);
				$inbOrdDtlData = $inbOrdDtlModel->toArray();
			}
			foreach($dtlData as $fieldName => $value)
			{
				if(strcmp($fieldName, 'inb_ord_hdr_id') == 0
				|| strcmp($fieldName, 'inb_ord_dtl_id') == 0)
				{
					continue;
				}
				$inbOrdDtlData[$fieldName] = $value;
			}
			
			$inbOrdDtlArray[] = $inbOrdDtlData;
		}

		foreach($delDtlArray as $delDtlData)
		{
			if (strpos($delDtlData['id'], 'NEW') !== false) 
      {
				$exc = new ApiException(__('InbOrd.dtl_id_not_found', ['id'=>$delDtlData['id']]));
				$exc->addData(\App\InbOrdDtl::class, $delDtlData['id']);
				throw $exc;
			}
			
			$delInbOrdDtlModel = InbOrdDtlRepository::findByPk($delDtlData['inb_ord_dtl_id']);
			$delInbOrdDtlData = $delInbOrdDtlModel->toArray();
			
			$delInbOrdDtlArray[] = $delInbOrdDtlData;
		}
		*/

		return array(
			'inbOrdHdrData' => $inbOrdHdrData,
			'inbOrdDtlArray' => $inbOrdDtlArray,
			'delInbOrdDtlArray' => $delInbOrdDtlArray
		);
	}

	protected function tallyOutbOrd($hdrData, $dtlArray, $delDtlArray = array())
  	{
		$outbOrdHdrData = array();
		$outbOrdDtlArray = array();
		$delOutbOrdDtlArray = array();

		/*
		$outbOrdHdrModel = OutbOrdHdrRepository::findByPk($hdrData['outb_ord_hdr_id']);
		if(empty($outbOrdHdrModel))
		{
			return array(
				'outbOrdHdrData' => $outbOrdHdrData,
				'outbOrdDtlArray' => $outbOrdDtlArray,
				'delOutbOrdDtlArray' => $delOutbOrdDtlArray
			);
		}

		$outbOrdHdrData = $outbOrdHdrModel->toArray();
		foreach($hdrData as $fieldName => $value)
		{
			if(strcmp($fieldName, 'outb_ord_hdr_id') == 0)
			{
				continue;
			}
			$outbOrdHdrData[$fieldName] = $value;
		}

		foreach($dtlArray as $dtlData)
		{
			$outbOrdDtlData = array();
			if (strpos($dtlData['id'], 'NEW') !== false) 
            {
				$outbOrdDtlData = $this->initOutbOrdDtlData();
				//temporarily use dtl uuid as outbOrdDtlId
				$outbOrdDtlData['id'] = $dtlData['id'];
			}
			else
			{
				$outbOrdDtlModel = OutbOrdDtlRepository::findByPk($dtlData['outb_ord_dtl_id']);
				$outbOrdDtlData = $outbOrdDtlModel->toArray();
			}
			foreach($dtlData as $fieldName => $value)
			{
				if(strcmp($fieldName, 'outb_ord_hdr_id') == 0
				|| strcmp($fieldName, 'outb_ord_dtl_id') == 0)
				{
					continue;
				}
				$outbOrdDtlData[$fieldName] = $value;
			}
			
			$outbOrdDtlArray[] = $outbOrdDtlData;
		}

		foreach($delDtlArray as $delDtlData)
		{
			if (strpos($delDtlData['id'], 'NEW') !== false) 
            {
				$exc = new ApiException(__('OutbOrd.dtl_id_not_found', ['id'=>$delDtlData['id']]));
				$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
				throw $exc;
			}
			
			$delOutbOrdDtlModel = OutbOrdDtlRepository::findByPk($delDtlData['outb_ord_dtl_id']);
			$delOutbOrdDtlData = $delOutbOrdDtlModel->toArray();
			
			$delOutbOrdDtlArray[] = $delOutbOrdDtlData;
		}
		*/

		return array(
			'outbOrdHdrData' => $outbOrdHdrData,
			'outbOrdDtlArray' => $outbOrdDtlArray,
			'delOutbOrdDtlArray' => $delOutbOrdDtlArray
		);
	}

	protected function clearPromotion($dtlData)
    {
		$dtlData['promo_hdr_id'] = 0;
		$dtlData['promo_uuid'] = '';
		$dtlData['price_disc'] = 0;

		$dtlData['dtl_disc_val_01'] = 0;
		$dtlData['dtl_disc_perc_01'] = 0;
		$dtlData['dtl_disc_val_02'] = 0;
		$dtlData['dtl_disc_perc_02'] = 0;
		$dtlData['dtl_disc_val_03'] = 0;
		$dtlData['dtl_disc_perc_03'] = 0;
		$dtlData['dtl_disc_val_04'] = 0;
		$dtlData['dtl_disc_perc_04'] = 0;
		$dtlData['dtl_disc_val_05'] = 0;
		$dtlData['dtl_disc_perc_05'] = 0;
		return $dtlData;
	}

	protected function updateSaleItemUom($dtlData, $docDate, $priceGroupId, $currencyId, $oldItemId, $oldUomId)
  {
		$itemId = $oldItemId;
		if(isset($dtlData['item_id']))
		{
			$itemId = $dtlData['item_id'];
		}
		$uomId = $oldUomId;
		if(isset($dtlData['uom_id']))
		{
			$uomId = $dtlData['uom_id'];
		}

		if((isset($dtlData['item_id']) && $dtlData['item_id'] != $oldItemId)
		|| (isset($dtlData['uom_id']) && $dtlData['uom_id'] != $oldUomId))
		{
			$itemUom = ItemUomRepository::findByItemIdAndUomId($itemId, $uomId);
			if(!empty($itemUom))
			{
				$dtlData['uom_rate'] = $itemUom->uom_rate;
			}
			else
			{
				$exc = new ApiException(__('Item.uom_not_found', ['itemId'=>$itemId, 'uomId'=>$uomId]));
				throw $exc;
			}
		}
		if(!isset($dtlData['sale_price'])
		&& (isset($dtlData['item_id']) || isset($dtlData['uom_id']))
		)
		{
			$itemSalePrice = ItemSalePriceRepository::findByItemIdAndUomId($docDate, $priceGroupId, $currencyId, $itemId, $uomId);
			if(!empty($itemSalePrice))
			{
				$dtlData['sale_price'] = round($itemSalePrice->sale_price, config('scm.decimal_scale'));
			}
		}
		return $dtlData;
	}

	protected function updatePurchaseItemUom($dtlData, $docDate, $priceGroupId, $currencyId, $oldItemId, $oldUomId)
  	{
		$itemId = $oldItemId;
		if(isset($dtlData['item_id']))
		{
			$itemId = $dtlData['item_id'];
		}
		$uomId = $oldUomId;
		if(isset($dtlData['uom_id']))
		{
			$uomId = $dtlData['uom_id'];
		}

		if((isset($dtlData['item_id']) && $dtlData['item_id'] != $oldItemId)
		|| (isset($dtlData['uom_id']) && $dtlData['uom_id'] != $oldUomId))
		{
			$itemUom = ItemUomRepository::findByItemIdAndUomId($itemId, $uomId);
			if(!empty($itemUom))
			{
				$dtlData['uom_rate'] = $itemUom->uom_rate;
			}
			else
			{
				$exc = new ApiException(__('Item.uom_not_found', ['itemId'=>$itemId, 'uomId'=>$uomId]));
				throw $exc;
			}
		}
		if(!isset($dtlData['sale_price'])
		&& (isset($dtlData['item_id']) || isset($dtlData['uom_id']))
		)
		{
			$itemPurPrice = ItemPurPriceRepository::findByItemIdAndUomId($docDate, $priceGroupId, $currencyId, $itemId, $uomId);
			if(!empty($itemPurPrice))
			{
				$dtlData['sale_price'] = round($itemPurPrice->sale_price, config('scm.decimal_scale'));
			}
		}
		return $dtlData;
	}

	protected function initAdvShipDtlData($data = array())
	{
		$newModelData = array();
		$newModelData['id'] = 0;
		$newModelData['hdr_id'] = 0; 

		$newModelData['line_no'] = 0;
		$newModelData['item_id'] = 0;
		$newModelData['desc_01'] = '';
		$newModelData['desc_02'] = '';

		$newModelData['location_id'] = 0;
				
		$newModelData['uom_id'] = 0;
		$newModelData['uom_rate'] = 0;
		$newModelData['sale_price'] = 0;
		$newModelData['price_disc'] = 0;
		$newModelData['qty'] = 0;

		$newModelData['gross_amt'] = 0;
		$newModelData['gross_local_amt'] = 0;

		$newModelData['dtl_disc_val_01'] = 0;
		$newModelData['dtl_disc_perc_01'] = 0;
		$newModelData['dtl_disc_val_02'] = 0;
		$newModelData['dtl_disc_perc_02'] = 0;
		$newModelData['dtl_disc_val_03'] = 0;
		$newModelData['dtl_disc_perc_03'] = 0;
		$newModelData['dtl_disc_val_04'] = 0;
		$newModelData['dtl_disc_perc_04'] = 0;
		$newModelData['dtl_disc_val_05'] = 0;
		$newModelData['dtl_disc_perc_05'] = 0;
		$newModelData['dtl_disc_amt'] = 0;
		$newModelData['dtl_disc_local_amt'] = 0;

		$newModelData['hdr_disc_val_01'] = 0;
		$newModelData['hdr_disc_perc_01'] = 0;
		$newModelData['hdr_disc_val_02'] = 0;
		$newModelData['hdr_disc_perc_02'] = 0;
		$newModelData['hdr_disc_val_03'] = 0;
		$newModelData['hdr_disc_perc_03'] = 0;
		$newModelData['hdr_disc_val_04'] = 0;
		$newModelData['hdr_disc_perc_04'] = 0;
		$newModelData['hdr_disc_val_05'] = 0;
		$newModelData['hdr_disc_perc_05'] = 0;
		$newModelData['hdr_disc_amt'] = 0;
		$newModelData['hdr_disc_local_amt'] = 0;

		$newModelData['tax_hdr_id'] = 0;
		$newModelData['dtl_taxable_amt_01'] = 0;
		$newModelData['dtl_tax_incl_01'] = 0;
		$newModelData['dtl_tax_val_01'] = 0;
		$newModelData['dtl_tax_perc_01'] = 0;
		$newModelData['dtl_tax_adj_01'] = 0;
		$newModelData['dtl_tax_amt_01'] = 0;
		$newModelData['dtl_tax_local_amt_01'] = 0;

		$newModelData['dtl_taxable_amt_02'] = 0;
		$newModelData['dtl_tax_incl_02'] = 0;
		$newModelData['dtl_tax_val_02'] = 0;
		$newModelData['dtl_tax_perc_02'] = 0;
		$newModelData['dtl_tax_adj_02'] = 0;
		$newModelData['dtl_tax_amt_02'] = 0;
		$newModelData['dtl_tax_local_amt_02'] = 0;

		$newModelData['dtl_taxable_amt_03'] = 0;
		$newModelData['dtl_tax_incl_03'] = 0;
		$newModelData['dtl_tax_val_03'] = 0;
		$newModelData['dtl_tax_perc_03'] = 0;
		$newModelData['dtl_tax_adj_03'] = 0;
		$newModelData['dtl_tax_amt_03'] = 0;
		$newModelData['dtl_tax_local_amt_03'] = 0;

		$newModelData['dtl_taxable_amt_04'] = 0;
		$newModelData['dtl_tax_incl_04'] = 0;
		$newModelData['dtl_tax_val_04'] = 0;
		$newModelData['dtl_tax_perc_04'] = 0;
		$newModelData['dtl_tax_adj_04'] = 0;
		$newModelData['dtl_tax_amt_04'] = 0;
		$newModelData['dtl_tax_local_amt_04'] = 0;

		$newModelData['dtl_taxable_amt_05'] = 0;
		$newModelData['dtl_tax_incl_05'] = 0;
		$newModelData['dtl_tax_val_05'] = 0;
		$newModelData['dtl_tax_perc_05'] = 0;
		$newModelData['dtl_tax_adj_05'] = 0;
		$newModelData['dtl_tax_amt_05'] = 0;
		$newModelData['dtl_tax_local_amt_05'] = 0;

		$newModelData['hdr_taxable_amt_01'] = 0;
		$newModelData['hdr_tax_incl_01'] = 0;
		$newModelData['hdr_tax_val_01'] = 0;
		$newModelData['hdr_tax_perc_01'] = 0;
		$newModelData['hdr_tax_adj_01'] = 0;
		$newModelData['hdr_tax_amt_01'] = 0;
		$newModelData['hdr_tax_local_amt_01'] = 0;

		$newModelData['hdr_taxable_amt_02'] = 0;
		$newModelData['hdr_tax_incl_02'] = 0;
		$newModelData['hdr_tax_val_02'] = 0;
		$newModelData['hdr_tax_perc_02'] = 0;
		$newModelData['hdr_tax_adj_02'] = 0;
		$newModelData['hdr_tax_amt_02'] = 0;
		$newModelData['hdr_tax_local_amt_02'] = 0;

		$newModelData['hdr_taxable_amt_03'] = 0;
		$newModelData['hdr_tax_incl_03'] = 0;
		$newModelData['hdr_tax_val_03'] = 0;
		$newModelData['hdr_tax_perc_03'] = 0;
		$newModelData['hdr_tax_adj_03'] = 0;
		$newModelData['hdr_tax_amt_03'] = 0;
		$newModelData['hdr_tax_local_amt_03'] = 0;

		$newModelData['hdr_taxable_amt_04'] = 0;
		$newModelData['hdr_tax_incl_04'] = 0;
		$newModelData['hdr_tax_val_04'] = 0;
		$newModelData['hdr_tax_perc_04'] = 0;
		$newModelData['hdr_tax_adj_04'] = 0;
		$newModelData['hdr_tax_amt_04'] = 0;
		$newModelData['hdr_tax_local_amt_04'] = 0;

		$newModelData['hdr_taxable_amt_05'] = 0;
		$newModelData['hdr_tax_incl_05'] = 0;
		$newModelData['hdr_tax_val_05'] = 0;
		$newModelData['hdr_tax_perc_05'] = 0;
		$newModelData['hdr_tax_adj_05'] = 0;
		$newModelData['hdr_tax_amt_05'] = 0;
		$newModelData['hdr_tax_local_amt_05'] = 0;

		$newModelData['net_amt'] = 0;
		$newModelData['net_local_amt'] = 0;

		foreach($data as $fieldName => $value)
		{
			$newModelData[$fieldName] = $value;
		}
		return $newModelData;
	}

	protected function initInbOrdDtlData($data = array())
	{
		$newModelData = array();
		$newModelData['id'] = 0;
		$newModelData['hdr_id'] = 0; 

		$newModelData['line_no'] = 0;
		$newModelData['item_id'] = 0;
		$newModelData['desc_01'] = '';
		$newModelData['desc_02'] = '';

		$newModelData['location_id'] = 0;
				
		$newModelData['uom_id'] = 0;
		$newModelData['uom_rate'] = 0;
		$newModelData['sale_price'] = 0;
		$newModelData['price_disc'] = 0;
		$newModelData['qty'] = 0;

		$newModelData['gross_amt'] = 0;
		$newModelData['gross_local_amt'] = 0;

		$newModelData['dtl_disc_val_01'] = 0;
		$newModelData['dtl_disc_perc_01'] = 0;
		$newModelData['dtl_disc_val_02'] = 0;
		$newModelData['dtl_disc_perc_02'] = 0;
		$newModelData['dtl_disc_val_03'] = 0;
		$newModelData['dtl_disc_perc_03'] = 0;
		$newModelData['dtl_disc_val_04'] = 0;
		$newModelData['dtl_disc_perc_04'] = 0;
		$newModelData['dtl_disc_val_05'] = 0;
		$newModelData['dtl_disc_perc_05'] = 0;
		$newModelData['dtl_disc_amt'] = 0;
		$newModelData['dtl_disc_local_amt'] = 0;

		$newModelData['hdr_disc_val_01'] = 0;
		$newModelData['hdr_disc_perc_01'] = 0;
		$newModelData['hdr_disc_val_02'] = 0;
		$newModelData['hdr_disc_perc_02'] = 0;
		$newModelData['hdr_disc_val_03'] = 0;
		$newModelData['hdr_disc_perc_03'] = 0;
		$newModelData['hdr_disc_val_04'] = 0;
		$newModelData['hdr_disc_perc_04'] = 0;
		$newModelData['hdr_disc_val_05'] = 0;
		$newModelData['hdr_disc_perc_05'] = 0;
		$newModelData['hdr_disc_amt'] = 0;
		$newModelData['hdr_disc_local_amt'] = 0;

		$newModelData['tax_hdr_id'] = 0;
		$newModelData['dtl_taxable_amt_01'] = 0;
		$newModelData['dtl_tax_incl_01'] = 0;
		$newModelData['dtl_tax_val_01'] = 0;
		$newModelData['dtl_tax_perc_01'] = 0;
		$newModelData['dtl_tax_adj_01'] = 0;
		$newModelData['dtl_tax_amt_01'] = 0;
		$newModelData['dtl_tax_local_amt_01'] = 0;

		$newModelData['dtl_taxable_amt_02'] = 0;
		$newModelData['dtl_tax_incl_02'] = 0;
		$newModelData['dtl_tax_val_02'] = 0;
		$newModelData['dtl_tax_perc_02'] = 0;
		$newModelData['dtl_tax_adj_02'] = 0;
		$newModelData['dtl_tax_amt_02'] = 0;
		$newModelData['dtl_tax_local_amt_02'] = 0;

		$newModelData['dtl_taxable_amt_03'] = 0;
		$newModelData['dtl_tax_incl_03'] = 0;
		$newModelData['dtl_tax_val_03'] = 0;
		$newModelData['dtl_tax_perc_03'] = 0;
		$newModelData['dtl_tax_adj_03'] = 0;
		$newModelData['dtl_tax_amt_03'] = 0;
		$newModelData['dtl_tax_local_amt_03'] = 0;

		$newModelData['dtl_taxable_amt_04'] = 0;
		$newModelData['dtl_tax_incl_04'] = 0;
		$newModelData['dtl_tax_val_04'] = 0;
		$newModelData['dtl_tax_perc_04'] = 0;
		$newModelData['dtl_tax_adj_04'] = 0;
		$newModelData['dtl_tax_amt_04'] = 0;
		$newModelData['dtl_tax_local_amt_04'] = 0;

		$newModelData['dtl_taxable_amt_05'] = 0;
		$newModelData['dtl_tax_incl_05'] = 0;
		$newModelData['dtl_tax_val_05'] = 0;
		$newModelData['dtl_tax_perc_05'] = 0;
		$newModelData['dtl_tax_adj_05'] = 0;
		$newModelData['dtl_tax_amt_05'] = 0;
		$newModelData['dtl_tax_local_amt_05'] = 0;

		$newModelData['hdr_taxable_amt_01'] = 0;
		$newModelData['hdr_tax_incl_01'] = 0;
		$newModelData['hdr_tax_val_01'] = 0;
		$newModelData['hdr_tax_perc_01'] = 0;
		$newModelData['hdr_tax_adj_01'] = 0;
		$newModelData['hdr_tax_amt_01'] = 0;
		$newModelData['hdr_tax_local_amt_01'] = 0;

		$newModelData['hdr_taxable_amt_02'] = 0;
		$newModelData['hdr_tax_incl_02'] = 0;
		$newModelData['hdr_tax_val_02'] = 0;
		$newModelData['hdr_tax_perc_02'] = 0;
		$newModelData['hdr_tax_adj_02'] = 0;
		$newModelData['hdr_tax_amt_02'] = 0;
		$newModelData['hdr_tax_local_amt_02'] = 0;

		$newModelData['hdr_taxable_amt_03'] = 0;
		$newModelData['hdr_tax_incl_03'] = 0;
		$newModelData['hdr_tax_val_03'] = 0;
		$newModelData['hdr_tax_perc_03'] = 0;
		$newModelData['hdr_tax_adj_03'] = 0;
		$newModelData['hdr_tax_amt_03'] = 0;
		$newModelData['hdr_tax_local_amt_03'] = 0;

		$newModelData['hdr_taxable_amt_04'] = 0;
		$newModelData['hdr_tax_incl_04'] = 0;
		$newModelData['hdr_tax_val_04'] = 0;
		$newModelData['hdr_tax_perc_04'] = 0;
		$newModelData['hdr_tax_adj_04'] = 0;
		$newModelData['hdr_tax_amt_04'] = 0;
		$newModelData['hdr_tax_local_amt_04'] = 0;

		$newModelData['hdr_taxable_amt_05'] = 0;
		$newModelData['hdr_tax_incl_05'] = 0;
		$newModelData['hdr_tax_val_05'] = 0;
		$newModelData['hdr_tax_perc_05'] = 0;
		$newModelData['hdr_tax_adj_05'] = 0;
		$newModelData['hdr_tax_amt_05'] = 0;
		$newModelData['hdr_tax_local_amt_05'] = 0;

		$newModelData['net_amt'] = 0;
		$newModelData['net_local_amt'] = 0;

		foreach($data as $fieldName => $value)
		{
			$newModelData[$fieldName] = $value;
		}
		return $newModelData;
	}

	protected function updateWarehouseItemUom($dtlData, $oldItemId, $oldUomId)
  	{
		$itemId = $oldItemId;
		if(isset($dtlData['item_id']))
		{
			$itemId = $dtlData['item_id'];
		}
		$uomId = $oldUomId;
		if(isset($dtlData['uom_id']))
		{
			$uomId = $dtlData['uom_id'];
		}

		if((isset($dtlData['item_id']) && $dtlData['item_id'] != $oldItemId)
		|| (isset($dtlData['uom_id']) && $dtlData['uom_id'] != $oldUomId))
		{
			$itemUom = ItemUomRepository::findByItemIdAndUomId($itemId, $uomId);
			if(!empty($itemUom))
			{
				$dtlData['uom_rate'] = $itemUom->uom_rate;
			}
			else
			{
				$exc = new ApiException(__('Item.uom_not_found', ['itemId'=>$itemId, 'uomId'=>$uomId]));
				throw $exc;
			}
		}
		return $dtlData;
	}

	protected function initBinTrfDtlData($data = array())
	{
		$newModelData = array();
		$newModelData['id'] = 0;
		$newModelData['hdr_id'] = 0; 

		$newModelData['line_no'] = 0;
		$newModelData['quant_bal_id'] = 0;
		$newModelData['storage_bin_id'] = 0;
		$newModelData['handling_unit_id'] = 0;
		$newModelData['company_id'] = 0;
		$newModelData['item_id'] = 0;
		$newModelData['batch_serial_no'] = '';
		$newModelData['expiry_date'] = '1970-01-01';
		$newModelData['receipt_date'] = '1970-01-01';
		$newModelData['desc_01'] = '';
		$newModelData['desc_02'] = '';
				
		$newModelData['uom_id'] = 0;
		$newModelData['uom_rate'] = 0;
		$newModelData['qty'] = 0;
		$newModelData['to_storage_bin_id'] = 0;
		$newModelData['to_handling_unit_id'] = 0;
		$newModelData['whse_job_type'] = '';
		$newModelData['whse_job_code'] = '';

		foreach($data as $fieldName => $value)
		{
			$newModelData[$fieldName] = $value;
		}
		return $newModelData;
	}

	protected function initWhseJobDtlData($data = array())
	{
		$newModelData = array();
		$newModelData['id'] = 0;
		$newModelData['hdr_id'] = 0;
		$newModelData['is_split'] = 0;
		$newModelData['whse_job_type'] = '';
		$newModelData['req_whse_job_hdr_id'] = 0;

		$newModelData['company_id'] = 0;
		$newModelData['doc_hdr_type'] = '';
		$newModelData['doc_hdr_id'] = 0;
		$newModelData['doc_dtl_type'] = '';
		$newModelData['doc_dtl_id'] = 0;
		$newModelData['desc_01'] = '';
		$newModelData['desc_02'] = '';

		$newModelData['line_no'] = 0;
		$newModelData['storage_bin_id'] = 0;		
		$newModelData['handling_unit_id'] = 0;
		$newModelData['quant_bal_id'] = 0;
		
		$newModelData['item_id'] = 0;
		$newModelData['batch_serial_no'] = '';
		$newModelData['expiry_date'] = '1970-01-01';
		$newModelData['receipt_date'] = '1970-01-01';
		$newModelData['item_cond_01_id'] = 0;
		$newModelData['item_cond_02_id'] = 0;
		$newModelData['item_cond_03_id'] = 0;
		$newModelData['item_cond_04_id'] = 0;
		$newModelData['item_cond_05_id'] = 0;
		$newModelData['uom_id'] = 0;
		$newModelData['uom_rate'] = 0;
		$newModelData['qty'] = 0;
		$newModelData['doc_status'] = 0;
		$newModelData['scan_mode'] = 0;
		$newModelData['to_storage_bin_id'] = 0;
		$newModelData['to_handling_unit_id'] = 0;

		foreach($data as $fieldName => $value)
		{
			$newModelData[$fieldName] = $value;
		}
		return $newModelData;
	}

	protected function initGdsRcptDtlData($data = array())
	{
		$newModelData = array();
		$newModelData['id'] = 0;
		$newModelData['hdr_id'] = 0;
		$newModelData['line_no'] = 0;

		$newModelData['company_id'] = 0;
		$newModelData['item_id'] = 0;
		$newModelData['batch_serial_no'] = '';
		$newModelData['expiry_date'] = '1970-01-01';
		$newModelData['receipt_date'] = '1970-01-01';
		$newModelData['desc_01'] = '';
		$newModelData['desc_02'] = '';
		$newModelData['uom_id'] = 0;
		$newModelData['uom_rate'] = 0;
		$newModelData['qty'] = 0;
		$newModelData['to_storage_bin_id'] = 0;
		$newModelData['to_handling_unit_id'] = 0;
		$newModelData['item_cond_01_id'] = 0;
		$newModelData['item_cond_02_id'] = 0;
		$newModelData['item_cond_03_id'] = 0;
		$newModelData['item_cond_04_id'] = 0;
		$newModelData['item_cond_05_id'] = 0;
		$newModelData['cases_per_pallet_length'] = 0;
		$newModelData['cases_per_pallet_width'] = 0;
		$newModelData['no_of_layers'] = 0;
		$newModelData['layer_no'] = 0;
		$newModelData['whse_job_type'] = '';
		$newModelData['whse_job_code'] = 0;

		foreach($data as $fieldName => $value)
		{
			$newModelData[$fieldName] = $value;
		}
		return $newModelData;
	}

	protected function initPutAwayDtlData($data = array())
	{
		$newModelData = array();
		$newModelData['id'] = 0;
		$newModelData['hdr_id'] = 0;
		$newModelData['line_no'] = 0;

		$newModelData['company_id'] = 0;
		$newModelData['item_id'] = 0;
		$newModelData['desc_01'] = '';
		$newModelData['desc_02'] = '';
		$newModelData['item_cond_01_id'] = 0;
		$newModelData['item_cond_02_id'] = 0;
		$newModelData['item_cond_03_id'] = 0;
		$newModelData['item_cond_04_id'] = 0;
		$newModelData['item_cond_05_id'] = 0;
		
		$newModelData['uom_id'] = 0;
		$newModelData['uom_rate'] = 0;
		$newModelData['qty'] = 0;

		$newModelData['quant_bal_id'] = 0;
		$newModelData['to_storage_bin_id'] = 0;
		$newModelData['to_handling_unit_id'] = 0;
		
		$newModelData['whse_job_type'] = '';
		$newModelData['whse_job_code'] = '';

		foreach($data as $fieldName => $value)
		{
			$newModelData[$fieldName] = $value;
		}
		return $newModelData;
	}

	protected function initPickListDtlData($data = array())
	{
		$newModelData = array();
		$newModelData['id'] = 0;
		$newModelData['hdr_id'] = 0;
		$newModelData['parent_dtl_id'] = 0;
		$newModelData['is_split'] = 0;
		$newModelData['line_no'] = 0;

		$newModelData['company_id'] = 0;
		$newModelData['item_id'] = 0;
		$newModelData['desc_01'] = '';
		$newModelData['desc_02'] = '';
		
		$newModelData['uom_id'] = 0;
		$newModelData['uom_rate'] = 0;
		$newModelData['qty'] = 0;

		$newModelData['storage_bin_id'] = 0;
		$newModelData['quant_bal_id'] = 0;
		$newModelData['to_storage_bin_id'] = 0;
		$newModelData['to_handling_unit_id'] = 0;
		
		$newModelData['whse_job_type'] = '';
		$newModelData['whse_job_code'] = '';

		foreach($data as $fieldName => $value)
		{
			$newModelData[$fieldName] = $value;
		}
		return $newModelData;
	}

	protected function initCycleCountDtlData($data = array())
	{
		$newModelData = array();
		$newModelData['id'] = 0;
		$newModelData['hdr_id'] = 0;
		$newModelData['line_no'] = 0;
		$newModelData['count_seq'] = 0;
		$newModelData['storage_bin_id'] = 0;
		$newModelData['handling_unit_id'] = 0;

		$newModelData['company_id'] = 0;
		$newModelData['item_id'] = 0;
		$newModelData['item_batch_id'] = 0;
		$newModelData['batch_serial_no'] = '';
		$newModelData['expiry_date'] = '1970-01-01';
		$newModelData['receipt_date'] = '1970-01-01';
		$newModelData['desc_01'] = '';
		$newModelData['desc_02'] = '';
		
		$newModelData['uom_id'] = 0;
		$newModelData['uom_rate'] = 0;
		$newModelData['qty'] = 0;

		$newModelData['whse_job_type'] = '';
		$newModelData['whse_job_code'] = '';

		$newModelData['to_cycle_count_hdr_id'] = 0;
		$newModelData['to_cycle_count_dtl_id'] = 0;
		
		foreach($data as $fieldName => $value)
		{
			$newModelData[$fieldName] = $value;
		}
		return $newModelData;
	}

	static protected function numberToWords($num)
	{
		$ones = array( 
			0 => "",
			1 => "ONE", 
			2 => "TWO", 
			3 => "THREE", 
			4 => "FOUR", 
			5 => "FIVE", 
			6 => "SIX", 
			7 => "SEVEN", 
			8 => "EIGHT", 
			9 => "NINE", 
			10 => "TEN", 
			11 => "ELEVEN", 
			12 => "TWELVE", 
			13 => "THIRTEEN", 
			14 => "FOURTEEN", 
			15 => "FIFTEEN", 
			16 => "SIXTEEN", 
			17 => "SEVENTEEN", 
			18 => "EIGHTEEN", 
			19 => "NINETEEN" 
		); 
		$tens = array(
			0 => "",
			1 => "TEN",
			2 => "TWENTY", 
			3 => "THIRTY", 
			4 => "FORTY", 
			5 => "FIFTY", 
			6 => "SIXTY", 
			7 => "SEVENTY", 
			8 => "EIGHTY", 
			9 => "NINETY" 
		); 
		$hundreds = array( 
			"HUNDRED", 
			"THOUSAND", 
			"MILLION", 
			"BILLION", 
			"TRILLION", 
			"QUADRILLION" 
		); //limit t quadrillion 
		$num = number_format($num,2,".",","); 
		$num_arr = explode(".",$num); 
		$wholenum = $num_arr[0]; 
		$decnum = $num_arr[1]; 
		$whole_arr = array_reverse(explode(",",$wholenum)); 
		krsort($whole_arr); 
		$rettxt = ""; 
		foreach($whole_arr as $key => $i)
		{
			$i = intval($i);
			if($i < 20)
			{ 
				$rettxt .= $ones[$i]; 
			} 
			elseif($i < 100)
			{ 
				$rettxt .= $tens[substr($i,0,1)]; 
				$rettxt .= " ".$ones[substr($i,1,1)]; 
			}
			else
			{ 
				$rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
				$rettxt .= " ".$tens[substr($i,1,1)]; 
				$rettxt .= " ".$ones[substr($i,2,1)]; 
			} 
			if($key > 0)
			{ 
				$rettxt .= " ".$hundreds[$key]." "; 
			} 
		} 
		if($decnum > 0)
		{
			$decnum = intval($decnum);
			$rettxt .= " AND "; 
			if($decnum < 20)
			{ 
				$rettxt .= $ones[$decnum]; 
			}
			elseif($decnum < 100)
			{ 
				$rettxt .= $tens[substr($decnum,0,1)]; 
				$rettxt .= " ".$ones[substr($decnum,1,1)]; 
			} 
		}
		else
		{
			$rettxt .= " AND ZERO"; 
		}
		return $rettxt; 
	}

	protected function initCountAdjDtlData($data = array())
	{
		$newModelData = array();
		$newModelData['id'] = 0;
		$newModelData['hdr_id'] = 0;
		$newModelData['line_no'] = 0;
		$newModelData['storage_bin_id'] = 0;
		$newModelData['item_id'] = 0;
		$newModelData['quant_bal_id'] = 0;
		$newModelData['item_batch_id'] = 0;
		$newModelData['batch_serial_no'] = '';
		$newModelData['expiry_date'] = '1970-01-01';
		$newModelData['receipt_date'] = '1970-01-01';
		$newModelData['handling_unit_id'] = 0;		
		
		$newModelData['desc_01'] = '';
		$newModelData['desc_02'] = '';
		
		$newModelData['sign'] = 1;
		$newModelData['uom_id'] = 0;
		$newModelData['uom_rate'] = 0;
		$newModelData['qty'] = 0;

		$newModelData['fr_cycle_count_hdr_id'] = 0;
		$newModelData['fr_cycle_count_dtl_id'] = 0;
		
		foreach($data as $fieldName => $value)
		{
			$newModelData[$fieldName] = $value;
		}
		return $newModelData;
	}

	static protected function processDocFlows($docHdrModel)
	{
		//query the frDocFlows and toDocFlows            
		$docFlows1 = array();
		$docFlows2 = array();
		$frDocTxnFlows = DocTxnFlowRepository::findAllByToHdrId(get_class($docHdrModel), $docHdrModel->id);
		$frDocTxnFlows->load('frDocHdr');
		foreach($frDocTxnFlows as $frDocTxnFlow)
		{
			$frDocHdr = $frDocTxnFlow->frDocHdr;
			if(!empty($frDocHdr))
			{             
				$docFlowData = array(
					'proc_type' => $frDocTxnFlow->proc_type,
					'doc_type' => $frDocTxnFlow->fr_doc_hdr_type,
					'doc_id' => $frDocTxnFlow->fr_doc_hdr_id,
					'doc_code' => $frDocHdr->doc_code,
					'doc_date' => $frDocHdr->doc_date,
					'doc_status' => $frDocHdr->doc_status,
					'str_doc_status' => DocStatus::$MAP[$frDocHdr->doc_status],
					'is_current' => false
				);
				$docFlows1[] = $docFlowData;
			}
		}

		$docFlowData = array(
			'proc_type' => $docHdrModel->proc_type,
			'doc_type' => get_class($docHdrModel),
			'doc_id' => $docHdrModel->id,
			'doc_code' => $docHdrModel->doc_code,
			'doc_date' => $docHdrModel->doc_date,
			'doc_status' => $docHdrModel->doc_status,
			'str_doc_status' => DocStatus::$MAP[$docHdrModel->doc_status],
			'is_current' => true
		);
		$docFlows1[] = $docFlowData;

		$toDocTxnFlows = DocTxnFlowRepository::findAllByFrHdrId(get_class($docHdrModel), $docHdrModel->id);
		$toDocTxnFlows->load('toDocHdr');
		foreach($toDocTxnFlows as $toDocTxnFlow)
		{
			$toDocHdr = $toDocTxnFlow->toDocHdr;
			if(!empty($toDocHdr))
			{
				$docFlowData = array(
					'proc_type' => $toDocTxnFlow->proc_type,
					'doc_type' => $toDocTxnFlow->to_doc_hdr_type,
					'doc_id' => $toDocTxnFlow->to_doc_hdr_id,
					'doc_code' => $toDocHdr->doc_code,
					'doc_date' => $toDocHdr->doc_date,
					'doc_status' => $toDocHdr->doc_status,
					'str_doc_status' => DocStatus::$MAP[$toDocHdr->doc_status],
					'is_current' => false
				);
				$docFlows1[] = $docFlowData;
			}
		}
		foreach($docFlows1 as $docFlowData)
		{
			$docFlowData['controller'] = str_replace('App\\','',$docFlowData['doc_type']);
			$docFlowData['controller'] = str_replace('Hdr','',$docFlowData['controller']);
			$docFlowData['controller'] = lcfirst($docFlowData['controller']);

			if(strcmp($docFlowData['doc_type'], \App\WhseJobHdr::class) == 0)
			{		
				if($docFlowData['proc_type'] == ProcType::$MAP['WHSE_JOB_03_01'])
				{
					//pick list
					$docFlowData['action'] = 'whseJob03Details';
				}
				elseif($docFlowData['proc_type'] == ProcType::$MAP['WHSE_JOB_05_01']
				|| $docFlowData['proc_type'] == ProcType::$MAP['WHSE_JOB_05_02'])
				{
					//pack list
					$docFlowData['action'] = 'whseJob05Details';
				}
				elseif($docFlowData['proc_type'] == ProcType::$MAP['WHSE_JOB_06_01'])
				{
					//load list
					$docFlowData['action'] = 'whseJob06Details';
				}
				elseif($docFlowData['proc_type'] == ProcType::$MAP['WHSE_JOB_14_01']
				|| $docFlowData['proc_type'] == ProcType::$MAP['WHSE_JOB_14_02'])
				{
					//gds rcpt
					$docFlowData['action'] = 'whseJob14Details';
				}
				elseif($docFlowData['proc_type'] == ProcType::$MAP['WHSE_JOB_15_01'])
				{
					//put away
					$docFlowData['action'] = 'whseJob15Details';
				}
				elseif($docFlowData['proc_type'] == ProcType::$MAP['WHSE_JOB_16_01'])
				{
					//cycle count
					$docFlowData['action'] = 'whseJob16Details';
				}
				elseif($docFlowData['proc_type'] == ProcType::$MAP['WHSE_JOB_17_01']
				|| $docFlowData['proc_type'] == ProcType::$MAP['WHSE_JOB_17_02'])
				{
					//bin trf
					$docFlowData['action'] = 'whseJob17Details';
				}
				else
				{
					$docFlowData['action'] = '';
				}			
			}
			else
			{
				$docFlowData['action'] = 'details';
			}
			$docFlows2[] =	$docFlowData;	
		}

		return $docFlows2;
	}

	protected function initCartDtlData($data = array())
	{
		$newModelData = array();
		$newModelData['id'] = 0;
		$newModelData['hdr_id'] = 0; 

		$newModelData['line_no'] = 0;
		$newModelData['item_id'] = 0;
		$newModelData['desc_01'] = '';
		$newModelData['desc_02'] = '';

		$newModelData['location_id'] = 0;
				
		$newModelData['uom_id'] = 0;
		$newModelData['uom_rate'] = 0;
		$newModelData['sale_price'] = 0;
		$newModelData['price_disc'] = 0;
		$newModelData['qty'] = 0;

		$newModelData['gross_amt'] = 0;
		$newModelData['gross_local_amt'] = 0;

		$newModelData['promo_hdr_id'] = 0;
		$newModelData['promo_uuid'] = '';
		$newModelData['dtl_disc_val_01'] = 0;
		$newModelData['dtl_disc_perc_01'] = 0;
		$newModelData['dtl_disc_val_02'] = 0;
		$newModelData['dtl_disc_perc_02'] = 0;
		$newModelData['dtl_disc_val_03'] = 0;
		$newModelData['dtl_disc_perc_03'] = 0;
		$newModelData['dtl_disc_val_04'] = 0;
		$newModelData['dtl_disc_perc_04'] = 0;
		$newModelData['dtl_disc_val_05'] = 0;
		$newModelData['dtl_disc_perc_05'] = 0;
		$newModelData['dtl_disc_amt'] = 0;
		$newModelData['dtl_disc_local_amt'] = 0;

		$newModelData['hdr_disc_val_01'] = 0;
		$newModelData['hdr_disc_perc_01'] = 0;
		$newModelData['hdr_disc_val_02'] = 0;
		$newModelData['hdr_disc_perc_02'] = 0;
		$newModelData['hdr_disc_val_03'] = 0;
		$newModelData['hdr_disc_perc_03'] = 0;
		$newModelData['hdr_disc_val_04'] = 0;
		$newModelData['hdr_disc_perc_04'] = 0;
		$newModelData['hdr_disc_val_05'] = 0;
		$newModelData['hdr_disc_perc_05'] = 0;
		$newModelData['hdr_disc_amt'] = 0;
		$newModelData['hdr_disc_local_amt'] = 0;

		$newModelData['tax_hdr_id'] = 0;
		$newModelData['dtl_taxable_amt_01'] = 0;
		$newModelData['dtl_tax_incl_01'] = 0;
		$newModelData['dtl_tax_val_01'] = 0;
		$newModelData['dtl_tax_perc_01'] = 0;
		$newModelData['dtl_tax_adj_01'] = 0;
		$newModelData['dtl_tax_amt_01'] = 0;
		$newModelData['dtl_tax_local_amt_01'] = 0;

		$newModelData['dtl_taxable_amt_02'] = 0;
		$newModelData['dtl_tax_incl_02'] = 0;
		$newModelData['dtl_tax_val_02'] = 0;
		$newModelData['dtl_tax_perc_02'] = 0;
		$newModelData['dtl_tax_adj_02'] = 0;
		$newModelData['dtl_tax_amt_02'] = 0;
		$newModelData['dtl_tax_local_amt_02'] = 0;

		$newModelData['dtl_taxable_amt_03'] = 0;
		$newModelData['dtl_tax_incl_03'] = 0;
		$newModelData['dtl_tax_val_03'] = 0;
		$newModelData['dtl_tax_perc_03'] = 0;
		$newModelData['dtl_tax_adj_03'] = 0;
		$newModelData['dtl_tax_amt_03'] = 0;
		$newModelData['dtl_tax_local_amt_03'] = 0;

		$newModelData['dtl_taxable_amt_04'] = 0;
		$newModelData['dtl_tax_incl_04'] = 0;
		$newModelData['dtl_tax_val_04'] = 0;
		$newModelData['dtl_tax_perc_04'] = 0;
		$newModelData['dtl_tax_adj_04'] = 0;
		$newModelData['dtl_tax_amt_04'] = 0;
		$newModelData['dtl_tax_local_amt_04'] = 0;

		$newModelData['dtl_taxable_amt_05'] = 0;
		$newModelData['dtl_tax_incl_05'] = 0;
		$newModelData['dtl_tax_val_05'] = 0;
		$newModelData['dtl_tax_perc_05'] = 0;
		$newModelData['dtl_tax_adj_05'] = 0;
		$newModelData['dtl_tax_amt_05'] = 0;
		$newModelData['dtl_tax_local_amt_05'] = 0;

		$newModelData['hdr_taxable_amt_01'] = 0;
		$newModelData['hdr_tax_incl_01'] = 0;
		$newModelData['hdr_tax_val_01'] = 0;
		$newModelData['hdr_tax_perc_01'] = 0;
		$newModelData['hdr_tax_adj_01'] = 0;
		$newModelData['hdr_tax_amt_01'] = 0;
		$newModelData['hdr_tax_local_amt_01'] = 0;

		$newModelData['hdr_taxable_amt_02'] = 0;
		$newModelData['hdr_tax_incl_02'] = 0;
		$newModelData['hdr_tax_val_02'] = 0;
		$newModelData['hdr_tax_perc_02'] = 0;
		$newModelData['hdr_tax_adj_02'] = 0;
		$newModelData['hdr_tax_amt_02'] = 0;
		$newModelData['hdr_tax_local_amt_02'] = 0;

		$newModelData['hdr_taxable_amt_03'] = 0;
		$newModelData['hdr_tax_incl_03'] = 0;
		$newModelData['hdr_tax_val_03'] = 0;
		$newModelData['hdr_tax_perc_03'] = 0;
		$newModelData['hdr_tax_adj_03'] = 0;
		$newModelData['hdr_tax_amt_03'] = 0;
		$newModelData['hdr_tax_local_amt_03'] = 0;

		$newModelData['hdr_taxable_amt_04'] = 0;
		$newModelData['hdr_tax_incl_04'] = 0;
		$newModelData['hdr_tax_val_04'] = 0;
		$newModelData['hdr_tax_perc_04'] = 0;
		$newModelData['hdr_tax_adj_04'] = 0;
		$newModelData['hdr_tax_amt_04'] = 0;
		$newModelData['hdr_tax_local_amt_04'] = 0;

		$newModelData['hdr_taxable_amt_05'] = 0;
		$newModelData['hdr_tax_incl_05'] = 0;
		$newModelData['hdr_tax_val_05'] = 0;
		$newModelData['hdr_tax_perc_05'] = 0;
		$newModelData['hdr_tax_adj_05'] = 0;
		$newModelData['hdr_tax_amt_05'] = 0;
		$newModelData['hdr_tax_local_amt_05'] = 0;

		$newModelData['net_amt'] = 0;
		$newModelData['net_local_amt'] = 0;

		foreach($data as $fieldName => $value)
		{
			$newModelData[$fieldName] = $value;
		}
		return $newModelData;
	}

	protected function initSlsOrdDtlData($data = array())
	{
		$newModelData = array();
		$newModelData['id'] = 0;
		$newModelData['hdr_id'] = 0; 
		$newModelData['outb_ord_hdr_id'] = 0;
		$newModelData['outb_ord_dtl_id'] = 0;

		$newModelData['line_no'] = 0;
		$newModelData['item_id'] = 0;
		$newModelData['desc_01'] = '';
		$newModelData['desc_02'] = '';

		$newModelData['location_id'] = 0;
				
		$newModelData['uom_id'] = 0;
		$newModelData['uom_rate'] = 0;
		$newModelData['sale_price'] = 0;
		$newModelData['price_disc'] = 0;
		$newModelData['qty'] = 0;

		$newModelData['gross_amt'] = 0;
		$newModelData['gross_local_amt'] = 0;

		$newModelData['promo_hdr_id'] = 0;
		$newModelData['promo_uuid'] = '';
		$newModelData['dtl_disc_val_01'] = 0;
		$newModelData['dtl_disc_perc_01'] = 0;
		$newModelData['dtl_disc_val_02'] = 0;
		$newModelData['dtl_disc_perc_02'] = 0;
		$newModelData['dtl_disc_val_03'] = 0;
		$newModelData['dtl_disc_perc_03'] = 0;
		$newModelData['dtl_disc_val_04'] = 0;
		$newModelData['dtl_disc_perc_04'] = 0;
		$newModelData['dtl_disc_val_05'] = 0;
		$newModelData['dtl_disc_perc_05'] = 0;
		$newModelData['dtl_disc_amt'] = 0;
		$newModelData['dtl_disc_local_amt'] = 0;

		$newModelData['hdr_disc_val_01'] = 0;
		$newModelData['hdr_disc_perc_01'] = 0;
		$newModelData['hdr_disc_val_02'] = 0;
		$newModelData['hdr_disc_perc_02'] = 0;
		$newModelData['hdr_disc_val_03'] = 0;
		$newModelData['hdr_disc_perc_03'] = 0;
		$newModelData['hdr_disc_val_04'] = 0;
		$newModelData['hdr_disc_perc_04'] = 0;
		$newModelData['hdr_disc_val_05'] = 0;
		$newModelData['hdr_disc_perc_05'] = 0;
		$newModelData['hdr_disc_amt'] = 0;
		$newModelData['hdr_disc_local_amt'] = 0;

		$newModelData['tax_hdr_id'] = 0;
		$newModelData['dtl_taxable_amt_01'] = 0;
		$newModelData['dtl_tax_incl_01'] = 0;
		$newModelData['dtl_tax_val_01'] = 0;
		$newModelData['dtl_tax_perc_01'] = 0;
		$newModelData['dtl_tax_adj_01'] = 0;
		$newModelData['dtl_tax_amt_01'] = 0;
		$newModelData['dtl_tax_local_amt_01'] = 0;

		$newModelData['dtl_taxable_amt_02'] = 0;
		$newModelData['dtl_tax_incl_02'] = 0;
		$newModelData['dtl_tax_val_02'] = 0;
		$newModelData['dtl_tax_perc_02'] = 0;
		$newModelData['dtl_tax_adj_02'] = 0;
		$newModelData['dtl_tax_amt_02'] = 0;
		$newModelData['dtl_tax_local_amt_02'] = 0;

		$newModelData['dtl_taxable_amt_03'] = 0;
		$newModelData['dtl_tax_incl_03'] = 0;
		$newModelData['dtl_tax_val_03'] = 0;
		$newModelData['dtl_tax_perc_03'] = 0;
		$newModelData['dtl_tax_adj_03'] = 0;
		$newModelData['dtl_tax_amt_03'] = 0;
		$newModelData['dtl_tax_local_amt_03'] = 0;

		$newModelData['dtl_taxable_amt_04'] = 0;
		$newModelData['dtl_tax_incl_04'] = 0;
		$newModelData['dtl_tax_val_04'] = 0;
		$newModelData['dtl_tax_perc_04'] = 0;
		$newModelData['dtl_tax_adj_04'] = 0;
		$newModelData['dtl_tax_amt_04'] = 0;
		$newModelData['dtl_tax_local_amt_04'] = 0;

		$newModelData['dtl_taxable_amt_05'] = 0;
		$newModelData['dtl_tax_incl_05'] = 0;
		$newModelData['dtl_tax_val_05'] = 0;
		$newModelData['dtl_tax_perc_05'] = 0;
		$newModelData['dtl_tax_adj_05'] = 0;
		$newModelData['dtl_tax_amt_05'] = 0;
		$newModelData['dtl_tax_local_amt_05'] = 0;

		$newModelData['hdr_taxable_amt_01'] = 0;
		$newModelData['hdr_tax_incl_01'] = 0;
		$newModelData['hdr_tax_val_01'] = 0;
		$newModelData['hdr_tax_perc_01'] = 0;
		$newModelData['hdr_tax_adj_01'] = 0;
		$newModelData['hdr_tax_amt_01'] = 0;
		$newModelData['hdr_tax_local_amt_01'] = 0;

		$newModelData['hdr_taxable_amt_02'] = 0;
		$newModelData['hdr_tax_incl_02'] = 0;
		$newModelData['hdr_tax_val_02'] = 0;
		$newModelData['hdr_tax_perc_02'] = 0;
		$newModelData['hdr_tax_adj_02'] = 0;
		$newModelData['hdr_tax_amt_02'] = 0;
		$newModelData['hdr_tax_local_amt_02'] = 0;

		$newModelData['hdr_taxable_amt_03'] = 0;
		$newModelData['hdr_tax_incl_03'] = 0;
		$newModelData['hdr_tax_val_03'] = 0;
		$newModelData['hdr_tax_perc_03'] = 0;
		$newModelData['hdr_tax_adj_03'] = 0;
		$newModelData['hdr_tax_amt_03'] = 0;
		$newModelData['hdr_tax_local_amt_03'] = 0;

		$newModelData['hdr_taxable_amt_04'] = 0;
		$newModelData['hdr_tax_incl_04'] = 0;
		$newModelData['hdr_tax_val_04'] = 0;
		$newModelData['hdr_tax_perc_04'] = 0;
		$newModelData['hdr_tax_adj_04'] = 0;
		$newModelData['hdr_tax_amt_04'] = 0;
		$newModelData['hdr_tax_local_amt_04'] = 0;

		$newModelData['hdr_taxable_amt_05'] = 0;
		$newModelData['hdr_tax_incl_05'] = 0;
		$newModelData['hdr_tax_val_05'] = 0;
		$newModelData['hdr_tax_perc_05'] = 0;
		$newModelData['hdr_tax_adj_05'] = 0;
		$newModelData['hdr_tax_amt_05'] = 0;
		$newModelData['hdr_tax_local_amt_05'] = 0;

		$newModelData['net_amt'] = 0;
		$newModelData['net_local_amt'] = 0;

		foreach($data as $fieldName => $value)
		{
			$newModelData[$fieldName] = $value;
		}
		return $newModelData;
	}

	protected function initSlsRtnDtlData($data = array())
	{
		$newModelData = array();
		$newModelData['id'] = 0;
		$newModelData['hdr_id'] = 0; 
		$newModelData['inb_ord_hdr_id'] = 0;
		$newModelData['inb_ord_dtl_id'] = 0;

		$newModelData['line_no'] = 0;
		$newModelData['item_id'] = 0;
		$newModelData['desc_01'] = '';
		$newModelData['desc_02'] = '';

		$newModelData['location_id'] = 0;
				
		$newModelData['uom_id'] = 0;
		$newModelData['uom_rate'] = 0;
		$newModelData['sale_price'] = 0;
		$newModelData['price_disc'] = 0;
		$newModelData['qty'] = 0;

		$newModelData['gross_amt'] = 0;
		$newModelData['gross_local_amt'] = 0;

		$newModelData['promo_hdr_id'] = 0;
		$newModelData['promo_uuid'] = '';
		$newModelData['dtl_disc_val_01'] = 0;
		$newModelData['dtl_disc_perc_01'] = 0;
		$newModelData['dtl_disc_val_02'] = 0;
		$newModelData['dtl_disc_perc_02'] = 0;
		$newModelData['dtl_disc_val_03'] = 0;
		$newModelData['dtl_disc_perc_03'] = 0;
		$newModelData['dtl_disc_val_04'] = 0;
		$newModelData['dtl_disc_perc_04'] = 0;
		$newModelData['dtl_disc_val_05'] = 0;
		$newModelData['dtl_disc_perc_05'] = 0;
		$newModelData['dtl_disc_amt'] = 0;
		$newModelData['dtl_disc_local_amt'] = 0;

		$newModelData['hdr_disc_val_01'] = 0;
		$newModelData['hdr_disc_perc_01'] = 0;
		$newModelData['hdr_disc_val_02'] = 0;
		$newModelData['hdr_disc_perc_02'] = 0;
		$newModelData['hdr_disc_val_03'] = 0;
		$newModelData['hdr_disc_perc_03'] = 0;
		$newModelData['hdr_disc_val_04'] = 0;
		$newModelData['hdr_disc_perc_04'] = 0;
		$newModelData['hdr_disc_val_05'] = 0;
		$newModelData['hdr_disc_perc_05'] = 0;
		$newModelData['hdr_disc_amt'] = 0;
		$newModelData['hdr_disc_local_amt'] = 0;

		$newModelData['tax_hdr_id'] = 0;
		$newModelData['dtl_taxable_amt_01'] = 0;
		$newModelData['dtl_tax_incl_01'] = 0;
		$newModelData['dtl_tax_val_01'] = 0;
		$newModelData['dtl_tax_perc_01'] = 0;
		$newModelData['dtl_tax_adj_01'] = 0;
		$newModelData['dtl_tax_amt_01'] = 0;
		$newModelData['dtl_tax_local_amt_01'] = 0;

		$newModelData['dtl_taxable_amt_02'] = 0;
		$newModelData['dtl_tax_incl_02'] = 0;
		$newModelData['dtl_tax_val_02'] = 0;
		$newModelData['dtl_tax_perc_02'] = 0;
		$newModelData['dtl_tax_adj_02'] = 0;
		$newModelData['dtl_tax_amt_02'] = 0;
		$newModelData['dtl_tax_local_amt_02'] = 0;

		$newModelData['dtl_taxable_amt_03'] = 0;
		$newModelData['dtl_tax_incl_03'] = 0;
		$newModelData['dtl_tax_val_03'] = 0;
		$newModelData['dtl_tax_perc_03'] = 0;
		$newModelData['dtl_tax_adj_03'] = 0;
		$newModelData['dtl_tax_amt_03'] = 0;
		$newModelData['dtl_tax_local_amt_03'] = 0;

		$newModelData['dtl_taxable_amt_04'] = 0;
		$newModelData['dtl_tax_incl_04'] = 0;
		$newModelData['dtl_tax_val_04'] = 0;
		$newModelData['dtl_tax_perc_04'] = 0;
		$newModelData['dtl_tax_adj_04'] = 0;
		$newModelData['dtl_tax_amt_04'] = 0;
		$newModelData['dtl_tax_local_amt_04'] = 0;

		$newModelData['dtl_taxable_amt_05'] = 0;
		$newModelData['dtl_tax_incl_05'] = 0;
		$newModelData['dtl_tax_val_05'] = 0;
		$newModelData['dtl_tax_perc_05'] = 0;
		$newModelData['dtl_tax_adj_05'] = 0;
		$newModelData['dtl_tax_amt_05'] = 0;
		$newModelData['dtl_tax_local_amt_05'] = 0;

		$newModelData['hdr_taxable_amt_01'] = 0;
		$newModelData['hdr_tax_incl_01'] = 0;
		$newModelData['hdr_tax_val_01'] = 0;
		$newModelData['hdr_tax_perc_01'] = 0;
		$newModelData['hdr_tax_adj_01'] = 0;
		$newModelData['hdr_tax_amt_01'] = 0;
		$newModelData['hdr_tax_local_amt_01'] = 0;

		$newModelData['hdr_taxable_amt_02'] = 0;
		$newModelData['hdr_tax_incl_02'] = 0;
		$newModelData['hdr_tax_val_02'] = 0;
		$newModelData['hdr_tax_perc_02'] = 0;
		$newModelData['hdr_tax_adj_02'] = 0;
		$newModelData['hdr_tax_amt_02'] = 0;
		$newModelData['hdr_tax_local_amt_02'] = 0;

		$newModelData['hdr_taxable_amt_03'] = 0;
		$newModelData['hdr_tax_incl_03'] = 0;
		$newModelData['hdr_tax_val_03'] = 0;
		$newModelData['hdr_tax_perc_03'] = 0;
		$newModelData['hdr_tax_adj_03'] = 0;
		$newModelData['hdr_tax_amt_03'] = 0;
		$newModelData['hdr_tax_local_amt_03'] = 0;

		$newModelData['hdr_taxable_amt_04'] = 0;
		$newModelData['hdr_tax_incl_04'] = 0;
		$newModelData['hdr_tax_val_04'] = 0;
		$newModelData['hdr_tax_perc_04'] = 0;
		$newModelData['hdr_tax_adj_04'] = 0;
		$newModelData['hdr_tax_amt_04'] = 0;
		$newModelData['hdr_tax_local_amt_04'] = 0;

		$newModelData['hdr_taxable_amt_05'] = 0;
		$newModelData['hdr_tax_incl_05'] = 0;
		$newModelData['hdr_tax_val_05'] = 0;
		$newModelData['hdr_tax_perc_05'] = 0;
		$newModelData['hdr_tax_adj_05'] = 0;
		$newModelData['hdr_tax_amt_05'] = 0;
		$newModelData['hdr_tax_local_amt_05'] = 0;

		$newModelData['net_amt'] = 0;
		$newModelData['net_local_amt'] = 0;

		foreach($data as $fieldName => $value)
		{
			$newModelData[$fieldName] = $value;
		}
		return $newModelData;
	}
}
