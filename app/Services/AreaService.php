<?php

namespace App\Services;

use App\Repositories\AreaRepository;
class AreaService 
{
    public function __construct() 
	{
    }
    
    public function select2($search, $filters, $pageSize = 20)
    {
        $uoms = AreaRepository::select2($search, $filters, $pageSize);
        return $uoms;
    }
}
