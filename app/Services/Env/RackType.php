<?php

namespace App\Services\Env;

class RackType
{
    public static $MAP = array(
        'NULL' => 0,
        0 => 'NULL',

        'OPEN_RACK' => 1,
        1 => 'OPEN_RACK',

        'FILO' => 2,
        2 => 'FILO',

        'FIFO' => 3,
        3 => 'FIFO'
    );
}
