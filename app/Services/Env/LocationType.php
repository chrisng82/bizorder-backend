<?php

namespace App\Services\Env;

class LocationType
{
    public static $MAP = array(
        'NULL' => 0,
        0 => 'NULL',

        'GOOD_STOCK' => 1,
        1 => 'GOOD_STOCK',

        'BAD_STOCK' => 2,
        2 => 'BAD_STOCK',

        'TRANSITORY_STOCK' => 3,
        3 => 'TRANSITORY_STOCK',
    );
}
