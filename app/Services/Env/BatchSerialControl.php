<?php

namespace App\Services\Env;

class BatchSerialControl
{
    public static $MAP = array(
        'EXPIRY_CONTROL_ONLY' => 0,
        0 => 'EXPIRY_CONTROL_ONLY',

        'BATCH_CONTROL_ONLY' => 1,
        1 => 'BATCH_CONTROL_ONLY',

        'BATCH_EXPIRY_CONTROL' => 2,
        2 => 'BATCH_EXPIRY_CONTROL',

        'SERIAL_CONTROL_ONLY' => 3,
        3 => 'SERIAL_CONTROL_ONLY',
    );
}
