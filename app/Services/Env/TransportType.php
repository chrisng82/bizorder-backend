<?php

namespace App\Services\Env;

class TransportType
{
    public static $MAP = array(
        'NULL' => 0,
        0 => 'NULL',

        'OWN_TEAM' => 1,
        1 => 'OWN_TEAM',

        'THIRD_PARTY_INDIVIDUAL' => 2,
        2 => 'THIRD_PARTY_INDIVIDUAL',

        'THIRD_PARTY_COMPANY' => 3,
        3 => 'THIRD_PARTY_COMPANY'
    );
}
