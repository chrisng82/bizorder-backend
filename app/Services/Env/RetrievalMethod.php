<?php

namespace App\Services\Env;

class RetrievalMethod
{
    public static $MAP = array(
        'NULL' => 0,
        0 => 'NULL',

        'FIRST_EXPIRED_FIRST_OUT' => 1,
        1 => 'FIRST_EXPIRED_FIRST_OUT',

        'LAST_EXPIRED_FIRST_OUT' => 2,
        2 => 'LAST_EXPIRED_FIRST_OUT',

        'FIRST_IN_FIRST_OUT' => 3,
        3 => 'FIRST_IN_FIRST_OUT',

        'LAST_IN_FIRST_OUT' => 4,
        4 => 'LAST_IN_FIRST_OUT'
    );
}
