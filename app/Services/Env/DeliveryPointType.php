<?php

namespace App\Services\Env;

class DeliveryPointType
{
    public static $MAP = array(
        'NULL' => 0,
        0 => 'NULL',

        'DEBTOR' => 1,
        1 => 'DEBTOR',

        'CREDITOR' => 2,
        2 => 'CREDITOR',
    );
}
