<?php

namespace App\Services\Env;

class ResStatus
{
    public static $MAP = array(
        'NULL' => 0,
        0 => 'NULL',

        'DELETED' => 1,
        1 => 'DELETED',

        'INACTIVE' => 2,
        2 => 'INACTIVE',

        'BLOCKED' => 3,
        3 => 'BLOCKED',

        'CONFLICT' => 4,
        4 => 'CONFLICT',

        'NEW' => 5,
        5 => 'NEW',

        'WIP' => 50,
        50 => 'WIP',

        'ACTIVE' => 100,
        100 => 'ACTIVE',

        'DELETED' => 818,
        818 => 'DELETED',
    );
}
