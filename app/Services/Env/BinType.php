<?php

namespace App\Services\Env;

class BinType
{
    public static $MAP = array(
        'NULL' => 0,
        0 => 'NULL',

        //LOADING_BAY should be UNLOADING_AREA
        //maintain here is for old excel code
        //'LOADING_BAY' => 1,
        'UNLOADING_AREA' => 1,
        1 => 'UNLOADING_AREA',

        //STAGING_AREA should be LOADING_AREA
        //maintain here is for old excel code
        //'STAGING_AREA' => 2,
        'LOADING_AREA' => 2,
        2 => 'LOADING_AREA',

        'UNLOADING_LOADING_AREA' => 3,
        3 => 'UNLOADING_LOADING_AREA',

        //pallet
        'BULK_STORAGE' => 4,
        4 => 'BULK_STORAGE',
        //pallet
        'PALLET_STORAGE' => 5,
        5 => 'PALLET_STORAGE',
        //pallet
        'BROKEN_PALLET_STORAGE' => 6,
        6 => 'BROKEN_PALLET_STORAGE',
        //pickface
        'CASE_STORAGE' => 7,
        7 => 'CASE_STORAGE',
        //pickface
        'BROKEN_CASE_STORAGE' => 8,
        8 => 'BROKEN_CASE_STORAGE',

        'TRANSPORT' => 9,
        9 => 'TRANSPORT',

        'PACKING_AREA' => 10,
        10 => 'PACKING_AREA',

        'BAD_PALLET_STORAGE' => 11,
        11 => 'BAD_PALLET_STORAGE',

        'BAD_BROKEN_CASE_STORAGE' => 12,
        12 => 'BAD_BROKEN_CASE_STORAGE',
    );
}
