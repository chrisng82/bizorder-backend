<?php

namespace App\Services\Env;

class UserStatus
{
    public static $MAP = array(
        'NULL' => 0,
        0 => 'NULL',

        'DELETED' => 1,
        1 => 'DELETED',

        'INACTIVE' => 2,
        2 => 'INACTIVE',

        'PENDING' => 50,
        50 => 'PENDING',

        'ACTIVE' => 100,
        100 => 'ACTIVE',
    );
}
