<?php

namespace App\Services\Env;

class SysType
{
    public static $MAP = array(
        'NULL' => 0,
        0 => 'NULL',

        'WAREHOUSE' => 1,
        1 => 'WAREHOUSE',

        'INVENTORY' => 2,
        2 => 'INVENTORY'
    );
}
