<?php

namespace App\Services\Env;

class ResType
{
    public static $MAP = array(
        'NULL' => '00',
        '00' => 'NULL',
        //inventory
        'SLS_ORD' => '01',
        '01' => 'SLS_ORD',
        //inventory
        'OUTB_ORD' => '02',
        '02' => 'OUTB_ORD',
        //warehouse, FIXED
        'PICK_LIST' => '03',
        '03' => 'PICK_LIST',
        //inventory
        'SLS_INV' => '04',
        '04' => 'SLS_INV',
        //warehouse, FIXED
        'PACK_LIST' => '05',
        '05' => 'PACK_LIST',
        //warehouse, FIXED
        'LOAD_LIST' => '06',
        '06' => 'LOAD_LIST',
        //warehouse/inventory
        'GDS_ISS' => '07',
        '07' => 'GDS_ISS',
        //warehouse
        'PRF_DEL' => '08',
        '08' => 'PRF_DEL',
        //warehouse
        'FAIL_DEL' => '09',
        '09' => 'FAIL_DEL',
        //inventory
        'DEL_ORD' => '10',
        '10' => 'DEL_ORD',
        //warehouse
        'WHSE_JOB' => '11',
        '11' => 'WHSE_JOB',
        //inventory
        'PUR_ORD' => '12',
        '12' => 'PUR_ORD',
        //inventory
        'ADV_SHIP' => '13',
        '13' => 'ADV_SHIP',
        //warehouse/inventory, FIXED
        'GDS_RCPT' => '14',
        '14' => 'GDS_RCPT',
        //warehouse, FIXED
        'PUT_AWAY' => '15',
        '15' => 'PUT_AWAY',
        //warehouse, FIXED
        'CYCLE_COUNT' => '16',
        '16' => 'CYCLE_COUNT',
        //warehouse, FIXED
        'BIN_TRF' => '17',
        '17' => 'BIN_TRF',
        //warehouse, FIXED
        'COUNT_ADJ' => '18',
        '18' => 'COUNT_ADJ',
        //inventory
        'INB_ORD' => '19',
        '19' => 'INB_ORD',
        //inventory
        'PUR_INV' => '20',
        '20' => 'PUR_INV',
        //inventory
        'SLS_RTN' => '21',
        '21' => 'SLS_RTN',
        //inventory
        'RTN_RCPT' => '22',
        '22' => 'RTN_RCPT',
        
        'ITEM' => 'ZQ',
        'ZQ' => 'ITEM',
        'ITEM_BATCH' => 'ZR',
        'ZR' => 'ITEM_BATCH',
        'STORAGE_ROW' => 'ZS',
        'ZS' => 'STORAGE_ROW',
        'STORAGE_BAY' => 'ZT',
        'ZT' => 'STORAGE_BAY',
        'LOCATION' => 'ZU',
        'ZU' => 'LOCATION',
        'DOC_PHOTO' => 'ZV',
        'ZV' => 'DOC_PHOTO',
        'ITEM_PHOTO' => 'ZW',
        'ZW' => 'ITEM_PHOTO',
        'USER' => 'ZX',
        'ZX' => 'USER',
        'HANDLING_UNIT' => 'ZY',
        'ZY' => 'HANDLING_UNIT',
        'STORAGE_BIN' => 'ZZ',
        'ZZ' => 'STORAGE_BIN'
    );

    public static $AUDIT = array(
        'ADV_SHIP' => array(
            \App\AdvShipHdr::class,
            \App\AdvShipDtl::class
        ),
        'AREA' => array(
            \App\Area::class
        ),
        'BIN_TRF' => array(
            \App\BinTrfHdr::class,
            \App\BinTrfDtl::class
        ),
        'BIZ_PARTNER' => array(
            \App\BizPartner::class
        ),
        'COUNT_ADJ' => array(
            \App\CountAdjHdr::class,
            \App\CountAdjDtl::class
        ),
        'CREDITOR' => array(
            \App\Creditor::class
        ),
        'CYCLE_COUNT' => array(
            \App\CycleCountHdr::class,
            \App\CycleCountDtl::class
        ),
        'DEBTOR' => array(
            \App\Debtor::class
        ),
        'DELIVERY_POINT' => array(
            \App\DeliveryPoint::class
        ),
        'GDS_RCPT' => array(
            \App\GdsRcptHdr::class,
            \App\GdsRcptDtl::class
        ),
        'HANDLING_UNIT' => array(
            \App\HandlingUnit::class
        ),
        'INB_ORD' => array(
            \App\InbOrdHdr::class,
            \App\InbOrdDtl::class
        ),
        'ITEM' => array(
            \App\Item::class,
            \App\ItemUom::class,
            \App\ItemBatch::class,
            \App\ItemPhoto::class,
            \App\ItemSalePrice::class,
            \App\Promotion::class,
        ),
        'LOCATION' => array(
            \App\Location::class
        ),
        'OUTB_ORD' => array(
            \App\OutbOrdHdr::class,
            \App\OutbOrdDtl::class
        ),
        'PICK_FACE_STRATEGY' => array(
            \App\PickFaceStrategy::class
        ),
        'PICK_LIST' => array(
            \App\PickListHdr::class,
            \App\PickListDtl::class,
            \App\PickListOutbOrd::class
        ),
        'PUT_AWAY' => array(
            \App\PutAwayHdr::class,
            \App\PutAwayDtl::class,
            \App\PutAwayInbOrd::class
        ),
        'REASON01' => array(
            \App\Reason01::class
        ),
        'RTN_RCPT' => array(
            \App\RtnRcptHdr::class,
            \App\RtnRcptDtl::class,
        ),
        'SLS_INV' => array(
            \App\SlsInvHdr::class,
            \App\SlsInvDtl::class,
        ),
        'SLS_ORD' => array(
            \App\SlsOrdHdr::class,
            \App\SlsOrdDtl::class,
        ),
        'SLS_RTN' => array(
            \App\SlsRtnHdr::class,
            \App\SlsRtnDtl::class,
        ),
        'STORAGE_BIN' => array(
            \App\StorageBin::class
        ),
        'UOM' => array(
            \App\Uom::class
        ),
        'USER' => array(
            \App\User::class,
            \App\UserDivision::class,
        ),
        'WHSE_JOB' => array(
            \App\WhseJobHdr::class,
            \App\WhseJobDtl::class,
        ),
    );
}
