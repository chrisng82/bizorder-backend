<?php

namespace App\Services\Env;

class DocStatus
{
    public static $MAP = array(
        'NULL' => 0,
        0 => 'NULL',

        'DELETED' => 1,
        1 => 'DELETED',

        'VOID' => 2,
        2 => 'VOID',

        'DRAFT' => 3,
        3 => 'DRAFT',

        'WIP' => 50,
        50 => 'WIP',

        'COMPLETE' => 100,
        100 => 'COMPLETE',
    );
}
