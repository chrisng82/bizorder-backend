<?php

namespace App\Services\Env;

class InspectionControl
{
    public static $MAP = array(
        'NO_INSPECTION' => 0,
        0 => 'NO_INSPECTION',

        '1ST_LVL_INSPECTION' => 1,
        1 => '1ST_LVL_INSPECTION',

        '2ND_LVL_INSPECTION' => 2,
        2 => '2ND_LVL_INSPECTION',

        '3RD_LVL_INSPECTION' => 3,
        3 => '3RD_LVL_INSPECTION',

        '4TH_LVL_INSPECTION' => 4,
        4 => '4TH_LVL_INSPECTION',

        '5TH_LVL_INSPECTION' => 5,
        5 => '5TH_LVL_INSPECTION',
    );
}
