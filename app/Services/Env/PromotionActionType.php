<?php

namespace App\Services\Env;

class PromotionActionType
{
    public static $MAP = array(
        'item_percent_disc' => 0,
        0 => 'item_percent_disc',

        'item_fixed_disc' => 1,
        1 => 'item_fixed_disc',
        
        'order_percent_disc' => 2,
        2 => 'order_percent_disc',

        'order_fixed_disc' => 3,
        3 => 'order_fixed_disc',

        'item_foc' => 4,
        4 => 'item_foc',

        'add_on' => 5,
        5 => 'add_on',
    );
}
