<?php

namespace App\Services\Env;

class ItemType
{
    public static $MAP = array(
        'NULL' => 0,
        0 => 'NULL',

        'STOCK_ITEM' => 1,
        1 => 'STOCK_ITEM',

        'NON_STOCK_ITEM' => 2,
        2 => 'NON_STOCK_ITEM',

        'MATERIAL' => 3,
        3 => 'MATERIAL',

        'BILL_OF_MATERIAL' => 4,
        4 => 'BILL_OF_MATERIAL',
    );
}
