<?php

namespace App\Services\Env;

class ProcType
{
    //MAX 5 digits
    public static $MAP = array(
        'NULL' => 0,
        0 => 'NULL',
        
        //all debtor listing
        'DEBTOR_LIST_01' => 2000,
        2000 => 'DEBTOR_LIST_01',
        
        //sync debtor from efichain
        'DEBTOR_SYNC_01' => 2001,
        2001 => 'DEBTOR_SYNC_01',

        //cart -> sales order
        'CART_ORD_01' => 1001,
        1001 => 'CART_ORD_01',

        //sales order index
        'SLS_ORD_01' => 10001,
        10001 => 'SLS_ORD_01',

        //sales order -> outbound order
        'OUTB_ORD_01' => 2,
        2 => 'OUTB_ORD_01',

        //outbound order -> delivery order
        'DEL_ORD_01' => 3,
        3 => 'DEL_ORD_01',

        //outbound order -> sales invoice
        'SLS_INV_01' => 4,
        4 => 'SLS_INV_01',

        //DO, PRF -> sales invoice
        'SLS_INV_02' => 5,
        5 => 'SLS_INV_02',

        //outbound order -> picking list
        'PICK_LIST_01' => 6,
        6 => 'PICK_LIST_01',

        //Pick List -> PackList, generated from pick list
        'PACK_LIST_01' => 7,
        7 => 'PACK_LIST_01',

        //Pick List, DO/INV -> PackList, generated from pick list, separated by DO/INV
        'PACK_LIST_02' => 8,
        8 => 'PACK_LIST_02',

        //Pick List, DO/INV -> loading list (gds iss), DO
        'LOAD_LIST_01' => 9,
        9 => 'LOAD_LIST_01',

        //Pack List, DO/INV -> loading list (gds iss), DO
        'LOAD_LIST_02' => 10,
        10 => 'LOAD_LIST_02',

        //DO/INV -> proof of delivery
        'PRF_DEL_01' => 11,
        11 => 'PRF_DEL_01',

        //DO/INV -> failed delivery (gds rcpt)
        'FAIL_DEL_01' => 12,
        12 => 'FAIL_DEL_01',

        //pick list -> inventory docs (D/O, Invoice)
        'INV_DOC_01' => 13,
        13 => 'INV_DOC_01',

        //batch print inventory docs (D/O, Invoice)
        'INV_DOC_01_01' => 14,
        14 => 'INV_DOC_01_01',

        //batch reprint inventory docs (D/O, Invoice)
        'INV_DOC_01_02' => 18,
        18 => 'INV_DOC_01_02',

        //gds rcpt -> inventory docs (RTN_RCPT/CN)
        'INV_DOC_02' => 21,
        21 => 'INV_DOC_02',

        //import sales order from efichain, by division
        'SLS_ORD_SYNC_01' => 15,
        15 => 'SLS_ORD_SYNC_01',
        
        //export sales order to efichain, by division
        'SLS_ORD_SYNC_02' => 16,
        16 => 'SLS_ORD_SYNC_02',
        
        //update cancelled sales order from efichain, by division
        'SLS_ORD_SYNC_03' => 17,
        17 => 'SLS_ORD_SYNC_03',

        // //export pick list to efichain, by user's division
        // 'PICK_LIST_SYNC_01' => 17,
        // 17 => 'PICK_LIST_SYNC_01',

        //export sls inv to efichain, by user's division
        'SLS_INV_SYNC_01' => 18,
        18 => 'SLS_INV_SYNC_01',

        'SLS_INV_SYNC_02' => 19,
        19 => 'SLS_INV_SYNC_02',

        // 'SLS_RTN_SYNC_01' => 19,
        // 19 => 'SLS_RTN_SYNC_01',

        'RTN_RCPT_01' => 22,
        22 => 'RTN_RCPT_01',

        'RTN_RCPT_SYNC_01' => 20,
        20 => 'RTN_RCPT_SYNC_01',

        //PO
        'PUR_ORD_01' => 100,
        100 => 'PUR_ORD_01',

        //import sales order from efichain, by division
        'ADV_SHIP_SYNC_01' => 108,
        108 => 'ADV_SHIP_SYNC_01',

        //ADV_SHIP
        'ADV_SHIP_01' => 101,
        101 => 'ADV_SHIP_01',

        //ADV_SHIP -> INB_ORD
        'INB_ORD_01' => 102,
        102 => 'INB_ORD_01',

        //SLS_RTN -> INB_ORD
        'INB_ORD_02' => 103,
        103 => 'INB_ORD_02',

        //INB_ORD -> GDS_RCPT, pre assigned the receiving bay bin
        //ADV_SHIP
        'GDS_RCPT_01' => 104,
        104 => 'GDS_RCPT_01',

        //INB_ORD -> GDS_RCPT
        //SLS_RTN
        'GDS_RCPT_02' => 109,
        109 => 'GDS_RCPT_02',

        //INB_ORD -> GDS_RCPT, pre assigned the receiving bay bin
        //for cross docking purpose, will not have put away
        //ADV_SHIP
        'GDS_RCPT_03' => 111,
        111 => 'GDS_RCPT_03',

        //WIP GDS_RCPT
        //SLS_RTN
        'GDS_RCPT_02_01' => 110,
        110 => 'GDS_RCPT_02_01',

        //GDS_RCPT -> PUT AWAY
        'PUT_AWAY_01' => 105,
        105 => 'PUT_AWAY_01',

        //GDS_RCPT, ADV_SHIP -> PI
        'PUR_INV_01' => 106,
        106 => 'PUR_INV_01',

        //verify pur inv
        'PUR_INV_01_01' => 107,
        107 => 'PUR_INV_01_01',

        //CYCLE_COUNT, BIN BASED CYCLE COUNT
        'CYCLE_COUNT_01' => 200,
        200 => 'CYCLE_COUNT_01',

        //CYCLE_COUNT, BIN BASED CYCLE COUNT, ADD NEW WITH CRITERIA
        'CYCLE_COUNT_01_01' => 208,
        208 => 'CYCLE_COUNT_01_01',

        //CYCLE_RECOUNT, RECOUNT, BIN BASED CYCLE COUNT
        'CYCLE_COUNT_02' => 201,
        201 => 'CYCLE_COUNT_02',

        //CYCLE_COUNT, GROUP CYCLE COUNT
        'CYCLE_COUNT_03' => 203,
        203 => 'CYCLE_COUNT_03',

        //import cycle count details from excel
        'CYCLE_COUNT_EXCEL_01' => 205,
        205 => 'CYCLE_COUNT_EXCEL_01',

        //COUNT_ADJ, CYCLE COUNT -> COUNT ADJUSTMENT
        'COUNT_ADJ_01' => 204,
        204 => 'COUNT_ADJ_01',

        'COUNT_ADJ_DETAILS' => 209,
        209 => 'COUNT_ADJ_DETAILS',

        //BIN_TRF, PICK FACE REPLENISHMENT
        'BIN_TRF_01' => 206,
        206 => 'BIN_TRF_01',

        //BIN_TRF, MOBILE BIN TRANSFER
        'BIN_TRF_02' => 207,
        207 => 'BIN_TRF_02',

        //all item listing
        'ITEM_LIST_01' => 1000,
        1000 => 'ITEM_LIST_01',

        //all item listing with price
        'ITEM_LIST_02' => 1002,
        1002 => 'ITEM_LIST_02',

        //all item excel
        'ITEM_EXCEL_01' => 1001,
        1001 => 'ITEM_EXCEL_01',

        //sync all item EfiChain
        'ITEM_SYNC_01' => 1002,
        1002 => 'ITEM_SYNC_01',

        //sync all item photos
        'ITEM_SYNC_01_01' => 1003,
        1003 => 'ITEM_SYNC_01_01',

        //download all item photos from local folder
        'ITEM_SYNC_01_02' => 1009,
        1009 => 'ITEM_SYNC_01_02',

        'STORAGE_BIN_LIST_01' => 1004,
        1004 => 'STORAGE_BIN_LIST_01',

        //warehouse map
        'STORAGE_BIN_LIST_02' => 1010,
        1010 => 'STORAGE_BIN_LIST_02',

        'STORAGE_BIN_EXCEL_01' => 1005,        
        1005 => 'STORAGE_BIN_EXCEL_01',

        'DELIVERY_POINT_LIST_01' => 1006,
        1006 => 'DELIVERY_POINT_LIST_01',

        'DELIVERY_POINT_SYNC_01' => 1007,
        1007 => 'DELIVERY_POINT_SYNC_01',

        //list all pallet label
        'HANDLING_UNIT_LIST_01' => 1008,
        1008 => 'HANDLING_UNIT_LIST_01',

        //list pallet label by gdsRcpt whseJob
        'HANDLING_UNIT_LIST_02' => 1011,
        1011 => 'HANDLING_UNIT_LIST_02',

        'PICK_FACE_STRATEGY_LIST_01' => 1012,
        1012 => 'PICK_FACE_STRATEGY_LIST_01',

        'PICK_FACE_STRATEGY_EXCEL_01' => 1013,
        1013 => 'PICK_FACE_STRATEGY_EXCEL_01',

        //wms role listing
        'ROLE_LIST_01' => 1014,
        1014 => 'ROLE_LIST_01',

        //wms role excel
        'ROLE_EXCEL_01' => 1015,
        1015 => 'ROLE_EXCEL_01',

        //web (admin) user listing
        'USER_LIST_01' => 1016,
        1016 => 'USER_LIST_01',

        //wms user excel
        'USER_EXCEL_01' => 1017,
        1017 => 'USER_EXCEL_01',

        //vendor (admin) user listing
        'USER_LIST_02' => 1018,
        1018 => 'USER_LIST_02',
        
        //customer mobile user listing
        'USER_LIST_03' => 1019,
        1019 => 'USER_LIST_03',

        //vendor (admin) user excel
        'USER_EXCEL_02' => 1020,
        1020 => 'USER_EXCEL_02',

        //customer mobile user excel
        'USER_EXCEL_03' => 1021,
        1021 => 'USER_EXCEL_03',

        //promotion sync (price group to price discount promo)
        'PROMO_SYNC_01' => 1101,
        1101 => 'PROMO_SYNC_01',

        //create pick list whse job, 1 pick list = 1n whse job
        //03 is pick list ResType
        'WHSE_JOB_03_01' => 10000,
        10000 => 'WHSE_JOB_03_01',

        //print pick list whse job
        'WHSE_JOB_03_01_01' => 10001,
        10001 => 'WHSE_JOB_03_01_01',

        //print pick list whse job by pick list
        'WHSE_JOB_03_01_02' => 10016,
        10016 => 'WHSE_JOB_03_01_02',

        //create pack list whse job, count by picking list
        //05 is pack list ResType
        'WHSE_JOB_05_01' => 10002,
        10002 => 'WHSE_JOB_05_01',

        //execute pack list whse job
        'WHSE_JOB_05_01_01' => 10003,
        10003 => 'WHSE_JOB_05_01_01',

        //create pack list whse job, pack by outb ord
        //05 is pack list ResType
        'WHSE_JOB_05_02' => 10004,
        10004 => 'WHSE_JOB_05_02',

        //execute pack list whse job
        'WHSE_JOB_05_02_01' => 10005,
        10005 => 'WHSE_JOB_05_02_01',

        //create load list whse job, load by outb ord
        //06 is load list ResType
        'WHSE_JOB_06_01' => 10006,
        10006 => 'WHSE_JOB_06_01',

        //execute load list whse job
        'WHSE_JOB_06_01_01' => 10007,
        10007 => 'WHSE_JOB_06_01_01',

        //create gds rcpt whse job
        //14 is gds rcpt ResType
        'WHSE_JOB_14_01' => 10008,
        10008 => 'WHSE_JOB_14_01',

        //execute gds rcpt whse job
        'WHSE_JOB_14_01_01' => 10009,
        10009 => 'WHSE_JOB_14_01_01',

        //create gds rcpt whse job, sls rtn
        //14 is gds rcpt ResType
        'WHSE_JOB_14_02' => 10017,
        10017 => 'WHSE_JOB_14_02',

        //execute gds rcpt whse job, sls rtn
        'WHSE_JOB_14_02_01' => 10018,
        10018 => 'WHSE_JOB_14_02_01',

        //create put away whse job, 1 put away = 1 whse job
        //15 is put away ResType
        'WHSE_JOB_15_01' => 10010,
        10010 => 'WHSE_JOB_15_01',

        //execute put away whse job
        'WHSE_JOB_15_01_01' => 10011,
        10011 => 'WHSE_JOB_15_01_01',
        
        //create cycle count whse job
        //16 is cycle count ResType
        'WHSE_JOB_16_01' => 10012,
        10012 => 'WHSE_JOB_16_01',

        //print cycle count whse job
        'WHSE_JOB_16_01_01' => 10013,
        10013 => 'WHSE_JOB_16_01_01',

        //print cycle count whse job, by cycle count
        'WHSE_JOB_16_01_02' => 10021,
        10021 => 'WHSE_JOB_16_01_02',

        //create bin transfer whse job
        //17 is bin trf ResType
        'WHSE_JOB_17_01' => 10014,
        10014 => 'WHSE_JOB_17_01',

        //execute bin transfer whse job
        //17 is bin trf ResType
        'WHSE_JOB_17_01_01' => 10015,
        10015 => 'WHSE_JOB_17_01_01',

        //bin transfer whse job, mobile free modify
        'WHSE_JOB_17_02' => 10019,
        10019 => 'WHSE_JOB_17_02',

        //wip bin transfer whse job, mobile free modify
        'WHSE_JOB_17_02_01' => 10020,
        10020 => 'WHSE_JOB_17_02_01',

        //SYNC, INTEGRATION
        //export stock balance
        'SYNC_EFICHAIN_01' => 20000,
        20000 => 'SYNC_EFICHAIN_01',

        //export transaction
        'SYNC_EFICHAIN_02' => 20001,
        20001 => 'SYNC_EFICHAIN_02',

        //REPORTING
        //stock balance report
        'STOCK_BALANCE' => 30000,
        30000 => 'STOCK_BALANCE',
    );
}
