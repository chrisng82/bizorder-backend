<?php

namespace App\Services\Env;

class HandlingType
{
    public static $MAP = array(
        'NULL' => 0,
        0 => 'NULL',

        'PALLET' => 1,
        1 => 'PALLET',

        'CASE' => 2,
        2 => 'CASE',

        'TOTE_BOX' => 3,
        3 => 'TOTE_BOX',

        'CASE_BOX' => 4,
        4 => 'CASE_BOX',

        'LOOSE' => 5,
        5 => 'LOOSE',
    );
}
