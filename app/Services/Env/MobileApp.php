<?php

namespace App\Services\Env;

class MobileApp
{
    public static $MAP = array(
        'NULL' => 0,
        0 => 'NULL',

        'WHSE_WORKER' => 1,
        1 => 'WHSE_WORKER',
    );
}
