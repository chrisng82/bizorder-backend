<?php

namespace App\Services\Env;

class ScanMode
{
    public static $MAP = array(
        'NULL' => 0,
        0 => 'NULL',

        'FOLLOW_ITEM' => 1,
        1 => 'FOLLOW_ITEM',

        'ENTER_QTY' => 2,
        2 => 'ENTER_QTY',

        'SHOW_QTY' => 3,
        3 => 'SHOW_QTY',

        'MUST_SCAN_1_BY_1' => 4,
        4 => 'MUST_SCAN_1_BY_1'
    );
}
