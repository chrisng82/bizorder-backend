<?php

namespace App\Services\Env;

class UserLoginType
{
    public static $MAP = array(
        'WEB' => 'w',
        'w' => 'WEB',

        'CUSTOMER' => 'c',
        'c' => 'CUSTOMER',

        'VENDOR' => 'v',
        'v' => 'VENDOR',
    );
}
