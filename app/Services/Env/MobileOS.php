<?php

namespace App\Services\Env;

class MobileOS
{
    public static $MAP = array(
        'NULL' => 0,
        0 => 'NULL',

        'ANDROID' => 1,
        1 => 'ANDROID',

        'IOS' => 2,
        2 => 'IOS',
    );
}
