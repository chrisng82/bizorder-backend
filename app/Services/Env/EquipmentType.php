<?php

namespace App\Services\Env;

class EquipmentType
{
    public static $MAP = array(
        'NULL' => 0,
        0 => 'NULL',

        'FORKLIFT' => 1,
        1 => 'FORKLIFT',
    );
}
