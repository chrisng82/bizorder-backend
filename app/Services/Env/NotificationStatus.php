<?php

namespace App\Services\Env;

class NotificationStatus
{
    public static $MAP = array(
        'UNREAD' => 0,
        0 => 'UNREAD',

        'READ' => 1,
        1 => 'READ',
    );
}
