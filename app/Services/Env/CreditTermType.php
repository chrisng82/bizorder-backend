<?php

namespace App\Services\Env;

class CreditTermType
{
    public static $MAP = array(
        'NULL' => 0,
        0 => 'NULL',

        'CASH' => 1,
        1 => 'CASH',

        'COD' => 2,
        2 => 'COD',

        'DUE_IN_NO_OF_DAYS' => 3,
        3 => 'DUE_IN_NO_OF_DAYS',

        'DUE_ON_DAYS_OF_NEXT_MONTHS' => 4,
        4 => 'DUE_ON_DAYS_OF_NEXT_MONTHS',

        'DUE_ON_DAYS_BEFORE_END_OF_THE_MONTHS' => 5,
        5 => 'DUE_ON_DAYS_BEFORE_END_OF_THE_MONTHS',

        'DUE_ON_DAYS_BEFORE_END_OF_NEXT_MONTHS' => 6,
        6 => 'DUE_ON_DAYS_BEFORE_END_OF_NEXT_MONTHS',
    );
}
