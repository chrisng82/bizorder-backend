<?php

namespace App\Services\Env;

class StorageClass
{
    public static $MAP = array(
        'NULL' => 0,
        0 => 'NULL',

        'AMBIENT_01' => 1,
        1 => 'AMBIENT_01',

        'CHILLED_01' => 2,
        2 => 'CHILLED_01',

        'FROZEN_01' => 3,
        3 => 'FROZEN_01'
    );
}
