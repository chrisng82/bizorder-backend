<?php

namespace App\Services\Env;

class TxnFlowType
{
    public static $MAP = array(
        'NULL' => 0,
        0 => 'NULL',

        'OUTB_DEL' => 1,
        1 => 'OUTB_DEL',

        'OUTB_RCPT' => 2,
        2 => 'OUTB_RCPT',

        'INB_RCPT' => 3,
        3 => 'INB_RCPT',

        'RTN_RCPT' => 11,
        11 => 'RTN_RCPT',

        'INB_DEL' => 4,
        4 => 'INB_DEL',

        'SALES' => 5,
        5 => 'SALES',

        'SALES_RETURN' => 6,
        6 => 'SALES_RETURN',

        'PURCHASE' => 7,
        7 => 'PURCHASE',

        'PURCHASE_RETURN' => 8,
        8 => 'PURCHASE_RETURN',

        'INVENTORY_AUDIT' => 9,
        9 => 'INVENTORY_AUDIT',

        'BIN_TRANSFER' => 10,
        10 => 'BIN_TRANSFER',
    );
}
