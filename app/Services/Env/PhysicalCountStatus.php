<?php

namespace App\Services\Env;

class PhysicalCountStatus
{
    public static $MAP = array(
        //no effect, countAdj will assume it same like confirmed
        'NULL' => 0,
        0 => 'NULL',

        //this line record is dropped to next count
        'DROPPED' => 1,
        1 => 'DROPPED',

        //this line record is dropped to next count
        'RECOUNT' => 2,
        2 => 'RECOUNT',

        //this mean confirm, default
        'CONFIRMED' => 10,
        10 => 'CONFIRMED',

        'MARK_RECOUNT' => 50,
        50 => 'MARK_RECOUNT',

        'MARK_CONFIRMED' => 51,
        51 => 'MARK_CONFIRMED',

        'MARK_DROPPED' => 52,
        52 => 'MARK_DROPPED',

        //counting, doc_status can't be complate
        'COUNTING' => 100,
        100 => 'COUNTING',

        'RESET' => 1035,
        1035 => 'RESET',
    );
}
