<?php

namespace App\Services\Env;

class ItemCondStatus
{
    public static $MAP = array(
        'NULL' => 0,
        0 => 'NULL',
        //match with LocationType
        'GOOD' => 1,
        1 => 'GOOD',

        'BAD' => 2,
        2 => 'BAD',
    );
}
