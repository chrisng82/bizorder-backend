<?php

namespace App\Services\Env;

class WhseJobType
{
    public static $MAP = array(
        'NULL' => 0,
        0 => 'NULL',
        //A, need forklift
        'PICK_FULL_PALLET' => 1,
        1 => 'PICK_FULL_PALLET',
        //B, need forklift
        'PICK_BROKEN_PALLET' => 2,
        2 => 'PICK_BROKEN_PALLET',
        //C, need forklift
        'PICK_FACE_REPLENISHMENT' => 3,
        3 => 'PICK_FACE_REPLENISHMENT',
        //D, wait for pick face replenishment
        'WAITING_REPLENISHMENT' => 4,
        4 => 'WAITING_REPLENISHMENT',
        //D
        'PICK_FULL_CASE' => 5,
        5 => 'PICK_FULL_CASE',
        //D
        'PICK_BROKEN_CASE' => 6,
        6 => 'PICK_BROKEN_CASE',
        //E
        'PICK_NOT_FULFILLED' => 7,
        7 => 'PICK_NOT_FULFILLED',

        'PACK' => 8,
        8 => 'PACK',

        'LOAD' => 9,
        9 => 'LOAD',

        'GOODS_RECEIPT_PALLET' => 10,
        10 => 'GOODS_RECEIPT_PALLET',

        'GOODS_RECEIPT_LOOSE' => 21,
        21 => 'GOODS_RECEIPT_LOOSE',

        'PUT_AWAY_PALLET' => 11,
        11 => 'PUT_AWAY_PALLET',

        'PUT_AWAY_CASE' => 12,
        12 => 'PUT_AWAY_CASE',

        'PUT_AWAY_LOOSE' => 13,
        13 => 'PUT_AWAY_LOOSE',

        'CYCLE_COUNT_BIN_PALLET' => 14,
        14 => 'CYCLE_COUNT_BIN_PALLET',

        'CYCLE_COUNT_BIN_LOOSE' => 15,
        15 => 'CYCLE_COUNT_BIN_LOOSE',

        'BIN_TRF_PALLET_TO_BIN' => 16,
        16 => 'BIN_TRF_PALLET_TO_BIN',

        'BIN_TRF_BROKEN_PALLET_TO_PALLET' => 17,
        17 => 'BIN_TRF_BROKEN_PALLET_TO_PALLET',

        'BIN_TRF_BROKEN_PALLET_TO_LOOSE' => 18,
        18 => 'BIN_TRF_BROKEN_PALLET_TO_LOOSE',

        'BIN_TRF_LOOSE_TO_LOOSE' => 19,
        19 => 'BIN_TRF_LOOSE_TO_LOOSE',

        'BIN_TRF_PALLET_TO_LOOSE' => 20,
        20 => 'BIN_TRF_PALLET_TO_LOOSE',
    );
}
