<?php

namespace App\Services\Env;

class PromotionRuleType
{
    public static $MAP = array(
        'order_total' => 0,
        0 => 'order_total',

        'item_total' => 1,
        1 => 'item_total',
        
        'item_qty' => 2,
        2 => 'item_qty',

        'debtor' => 3,
        3 => 'debtor',

        'debtor_group' => 4,
        4 => 'debtor_group',
    );
}
