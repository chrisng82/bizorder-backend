<?php

namespace App\Services\Env;

class NotificationPriority
{
    public static $MAP = array(
        'NORMAL' => 0,
        0 => 'NORMAL',

        'URGENT' => 1,
        1 => 'URGENT',
    );
}
