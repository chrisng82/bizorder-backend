<?php

namespace App\Services\Env;

class PromotionType
{
    public static $MAP = array(
        'disc_percent' => 0,
        0 => 'disc_percent',

        'disc_fixed_price' => 1,
        1 => 'disc_fixed_price',

        'foc' => 2,
        2 => 'foc',
        
        'bundle' => 3,
        3 => 'bundle',
        
        'add_on' => 4,
        4 => 'add_on',
    );
}
