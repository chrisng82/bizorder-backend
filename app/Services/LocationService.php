<?php

namespace App\Services;

use App\Services\Env\ProcType;
use App\Services\Env\ResStatus;
use App\Repositories\LocationRepository;

class LocationService 
{
    public function __construct() 
	{
    }

    public function select2($search, $filters)
    {
        $locations = LocationRepository::select2($search, $filters);
        return $locations;
    }
}
