<?php

namespace App\Services;

use App\Repositories\UomRepository;
class UomService 
{
    public function __construct() 
	{
    }
    
    public function select2($search, $filters)
    {
        $uoms = UomRepository::select2($search, $filters);
        return $uoms;
    }
}
