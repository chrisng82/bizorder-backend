<?php

namespace App\Services;

use App\SlsOrdHdr;
use App\SlsOrdDtl;
use App\Promotion;

use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Services\Env\ResStatus;
use App\Services\Env\ScanMode;
use App\Services\Env\RetrievalMethod;
use App\Services\Env\StorageClass;
use App\Repositories\BatchJobStatusRepository;
use App\Repositories\SyncSettingHdrRepository;
use App\Repositories\SyncSettingDtlRepository;
use App\Repositories\SiteFlowRepository;
use App\Repositories\SlsOrdHdrRepository;
use App\Repositories\SlsOrdDtlRepository;
use App\Repositories\DivisionRepository;
use App\Repositories\InvTxnFlowRepository;
use App\Repositories\QuantBalRepository;
use App\Repositories\OutbOrdHdrRepository;
use App\Repositories\DeliveryPointRepository;
use App\Repositories\ItemRepository;
use App\Repositories\ItemUomRepository;
use App\Repositories\CurrencyRepository;
use App\Repositories\CreditTermRepository;
use App\Repositories\DebtorRepository;
use App\BatchJobStatus;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;
use Milon\Barcode\DNS2D;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Log;

class SlsOrdService extends InventoryService
{
    public function __construct()
    {
    }

    public function syncProcess($strProcType, $divisionId, $data=array())
    {
        $user = Auth::user();

        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['SLS_ORD_SYNC_01']) 
			{
				//sync sales order from EfiChain
                return $this->syncSlsOrdSync01($divisionId, $user->id);
                //return $this->checkSlsOrdWithInverze($divisionId, $user->id);
            }
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['SLS_ORD_SYNC_02']) 
			{
                $hdrIds = $data;
				//sync sales order from EfiChain
                return $this->syncSlsOrdSync02($divisionId, $user->id, $hdrIds);
                //return $this->checkSlsOrdWithInverze($divisionId, $user->id);
            }
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['SLS_ORD_SYNC_03']) 
			{
                $hdrIds = $data;
				//sync cancelled sales order from EfiChain
                return $this->syncSlsOrdSync03($divisionId, $user->id, $hdrIds);
            }
        }
        
        $exc = new ApiException(__('SlsOrd.sync_type_not_found', ['divisionId'=>$divisionId]));
        throw $exc;
    }

    protected function syncSlsOrdSync01($divisionId, $userId)
    {
        AuthService::authorize(
			array(
				'sls_ord_import'
			)
        );
        
        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['SLS_ORD_SYNC_01'], $userId);

        $syncSettingHdr = SyncSettingHdrRepository::findByProcTypeAndDivisionId(ProcType::$MAP['SYNC_EFICHAIN_01'], $divisionId);
        if(empty($syncSettingHdr))
        {
            $exc = new ApiException(__('SlsOrd.sync_setting_not_found', ['divisionId'=>$divisionId]));
            //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
            throw $exc;
        }

        $division = DivisionRepository::findByPk($divisionId);
        $company = $division->company;
        $divUrl = '&divisions[]='.$division->code;

        $client = new Client();
        $header = array();

        $page = 1;
        $lastPage = 1;
        $pageSize = 1000;
        $total = 0;

        $usernameSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'username');
        $passwordSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'password');
        $formParams = array(
            'UserLogin[username]' => $usernameSetting->value,
            'UserLogin[password]' => $passwordSetting->value
        );
        $pageSizeSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'page_size');
        if(!empty($pageSizeSetting))
        {
            $pageSize = $pageSizeSetting->value;
        }

        //query from efichain, and update the sales order accordingly
        $slsOrdHdrDataList = array();
        while($page <= $lastPage)
        {
            $url = $syncSettingHdr->url.'/index.php?r=luuWu/getSalesOrders&page_size='.$pageSize.'&page='.$page.$divUrl;
            
            $response = $client->request('POST', $url, array('headers' => $header, 'form_params' => $formParams));
            $result = $response->getBody()->getContents();
            $result = json_decode($result, true);
            if(empty($result))
            {
                $exc = new ApiException(__('SlsOrd.fail_to_sync', ['url'=>$url]));
                //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
                throw $exc;
            }

            $total = $result['total'];
            for ($a = 0; $a < count($result['data']); $a++)
            {
                $slsOrdHdrData = $result['data'][$a];
                //DRAFT
                $slsOrdHdrModel = SlsOrdHdrRepository::syncSlsOrdSync01($company, $division, $slsOrdHdrData);

                $outbOrdHdrId = 0;
                $toDocTxnFlows = $slsOrdHdrModel->toDocTxnFlows;
                foreach($toDocTxnFlows as $toDocTxnFlow)
                {
                    $outbOrdHdrId = $toDocTxnFlow->to_doc_hdr_id;
                }

                $slsOrdHdrDataList[] = array(
                    'id'=>$slsOrdHdrModel->id,
                    'doc_code'=>$slsOrdHdrModel->doc_code,
                    'doc_date'=>$slsOrdHdrModel->doc_date,
                    'doc_status'=>$slsOrdHdrModel->doc_status,
                    'outb_ord_hdr_id'=>$outbOrdHdrId,
                );
                
                $statusNumber = 100;
                if($total > 0)
                {
                    $statusNumber = bcmul(bcdiv(($a + ($page - 1) * $pageSize), $total, 8), 100, 2);
                }
                BatchJobStatusRepository::updateStatusNumber($batchJobStatusModel->id, $statusNumber);
            }

            $lastPage = $total % $pageSize == 0 ? ($total / $pageSize) : ($total / $pageSize) + 1;

            $page++;
        }

        //post back to efichain, to set the efichain sales order to PICKING
        $formParams = array(
            'UserLogin[username]' => $usernameSetting->value,
            'UserLogin[password]' => $passwordSetting->value
		);		
        $formParams['SlsOrdHdrs'] = $slsOrdHdrDataList;
        
        $url = $syncSettingHdr->url.'/index.php?r=luuWu/putSalesOrderStatus';
            
		$response = $client->request('POST', $url, array('headers' => $header, 'form_params' => $formParams));
		
		$result = $response->getBody()->getContents();
		$result = json_decode($result, true);
		if(empty($result))
		{
			BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

			$exc = new ApiException(__('SlsOrd.fail_to_sync', ['url'=>$url]));
			//$exc->addData(\App\SlsInvDtl::class, $delDtlData['id']);
			throw $exc;
        }
        else
		{
			if($result['success'] === false)
			{
				BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

				$exc = new ApiException(__('SlsOrd.fail_to_sync', ['url'=>$url,'message'=>$result['message']]));
				//$exc->addData(\App\SlsInvDtl::class, $delDtlData['id']);
				throw $exc;
            }
            else
            {
                $batchJobStatusModel = BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);
            }
		}

        return $batchJobStatusModel;
    }
    
    protected function syncSlsOrdSync02($divisionId, $userId, $hdrIds)
    {
		// AuthService::authorize(
		// 	array(
		// 		'sls_inv_export'
		// 	)
		// );

        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['SLS_ORD_SYNC_02'], $userId);

        $syncSettingHdr = SyncSettingHdrRepository::findByProcTypeAndDivisionId(ProcType::$MAP['SYNC_EFICHAIN_01'], $divisionId);
        if(empty($syncSettingHdr))
        {
            $exc = new ApiException(__('SlsOrd.sync_setting_not_found', ['divisionId'=>$divisionId]));
            //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
            throw $exc;
		}
		
		//query the completed pickList associated outbOrdHdrs from DB

		//DB::connection()->enableQueryLog();
		$slsOrdHdrs = SlsOrdHdrRepository::findAllByHdrIds($hdrIds);
		//dd(DB::getQueryLog());
		$postSlsOrds = array();
		foreach($slsOrdHdrs as $slsOrdHdr)
		{
			//this code will use transaction to make sure this document is not duplicate for this process
			// $tmpSlsInvSync01DocTxn = DocTxnFlowRepository::txnFindByProcTypeAndFrHdrId(
			// 	ProcType::$MAP['SLS_INV_SYNC_01'], 
			// 	\App\SlsInvHdr::class, 
			// 	$slsInvHdr->id
			// );
			// if(!empty($tmpSlsInvSync01DocTxn))
			// {
			// 	continue;
			// }

            $slsOrdHdr = self::processOutgoingHeader($slsOrdHdr);

			//query the docDtls
			$slsOrdDtls = $slsOrdHdr->slsOrdDtls;
			$slsOrdDtls = $slsOrdDtls->sortBy('line_no');

			$formatSlsOrdDtls = array();
			foreach($slsOrdDtls as $slsOrdDtl)
			{
				$item = $slsOrdDtl->item;
				
				$slsOrdDtl = self::processOutgoingDetail($slsOrdDtl);

				unset($slsOrdDtl->item);
				unset($slsOrdDtl->promotion);
                unset($slsOrdDtl->uom);
                unset($slsOrdDtl->item_select2);
                unset($slsOrdDtl->uom_select2);
				$formatSlsOrdDtls[] = $slsOrdDtl->toArray();
            }
            
			$slsOrdHdr->details = $formatSlsOrdDtls;
            unset($slsOrdHdr->doc_flows);
            unset($slsOrdHdr->division);
            unset($slsOrdHdr->company);
            unset($slsOrdHdr->debtor);
            unset($slsOrdHdr->salesman);
            // unset($slsOrdHdr->deliveryPoint);
            unset($slsOrdHdr->slsOrdDtls);
            unset($slsOrdHdr->debtor_select2);
            unset($slsOrdHdr->currency_select2);
            unset($slsOrdHdr->credit_term_select2);
            unset($slsOrdHdr->salesman_select2);
            unset($slsOrdHdr->delivery_point_select2);
			$hdrData = $slsOrdHdr->toArray();
			$postSlsOrdHdrs[] = $hdrData;
		}

        $client = new Client();
        $header = array();

        $usernameSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'username');
        $passwordSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'password');
        $formParams = array(
            'UserLogin[username]' => $usernameSetting->value,
            'UserLogin[password]' => $passwordSetting->value
		);		
		$formParams['SlsOrdHdrs'] = $postSlsOrdHdrs;

        $url = $syncSettingHdr->url.'/index.php?r=bizOrder/postSalesOrders';
		Log::error($formParams);
            
		$response = $client->request('POST', $url, array('headers' => $header, 'form_params' => $formParams, 'timeout' =>  10));
		
		$result = $response->getBody()->getContents();
		$result = json_decode($result, true);
		if(empty($result))
		{
			BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

			$exc = new ApiException(__('SlsOrd.fail_to_sync', ['url'=>$url]));
			//$exc->addData(\App\SlsInvDtl::class, $delDtlData['id']);
			throw $exc;
		}
		else
		{
			if($result['success'] === false)
			{
				BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

				$exc = new ApiException(__('SlsOrd.fail_to_sync', ['url'=>$url,'message'=>$result['message']]));
				//$exc->addData(\App\SlsInvDtl::class, $delDtlData['id']);
				throw $exc;
			}
			else
			{
				//mark the slsInv as success
				// $docTxnFlowDataList = array();
				// foreach($result['data'] as $successSlsOrd)
				// {
				// 	$tmpDocTxnFlowData = array(
				// 		'fr_doc_hdr_type' => \App\SlsInvHdr::class,
				// 		'fr_doc_hdr_id' => $successSlsOrd['id'],
				// 		'fr_doc_hdr_code' => $successSlsOrd['doc_code'],
				// 		'is_closed' => 1
				// 	);
				// 	$docTxnFlowDataList[] = $tmpDocTxnFlowData;
				// }
                // DocTxnFlowRepository::createProcess(ProcType::$MAP['SLS_ORD_SYNC_02'], $docTxnFlowDataList);
                
                foreach($result['data'] as $successSlsOrd)
				{
                    $hdrModel = self::transitionToWip($successSlsOrd['id']);
				}
			}
		}

		/*
		$successPickListDocCodes = array();
		for ($a = 0; $a < count($result['data']); $a++)
		{
			$successPickListDocCodes[] = $result['data'][$a];
		}
		*/
		$batchJobStatusModel = BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

		return array(
			'data' => $batchJobStatusModel,
			'message' => $result['message']
		);
    }
    
    //sync cancelled sales order
    protected function syncSlsOrdSync03($divisionId, $userId)
    {
        AuthService::authorize(
			array(
				'sls_ord_import'
			)
        );
        
        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['SLS_ORD_SYNC_03'], $userId);

        $syncSettingHdr = SyncSettingHdrRepository::findByProcTypeAndDivisionId(ProcType::$MAP['SYNC_EFICHAIN_01'], $divisionId);
        if(empty($syncSettingHdr))
        {
            $exc = new ApiException(__('SlsOrd.sync_setting_not_found', ['divisionId'=>$divisionId]));
            //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
            throw $exc;
        }

        $division = DivisionRepository::findByPk($divisionId);
        $company = $division->company;
        $divUrl = '&divisions[]='.$division->code;

        $client = new Client();
        $header = array();

        $page = 1;
        $lastPage = 1;
        $pageSize = 1000;
        $total = 0;

        $usernameSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'username');
        $passwordSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'password');
        $formParams = array(
            'UserLogin[username]' => $usernameSetting->value,
            'UserLogin[password]' => $passwordSetting->value
        );
        $pageSizeSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'page_size');
        if(!empty($pageSizeSetting))
        {
            $pageSize = $pageSizeSetting->value;
        }

        //query from efichain, and update the sales order accordingly
        $slsOrdHdrDataList = array();
        while($page <= $lastPage)
        {
            $url = $syncSettingHdr->url.'/index.php?r=bizOrder/getCancelledSalesOrders&page_size='.$pageSize.'&page='.$page.$divUrl;
            
            $response = $client->request('POST', $url, array('headers' => $header, 'form_params' => $formParams));
            $result = $response->getBody()->getContents();
            $result = json_decode($result, true);
            if(empty($result))
            {
                $exc = new ApiException(__('SlsOrd.fail_to_sync', ['url'=>$url]));
                //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
                throw $exc;
            }

            $total = $result['total'];
            for ($a = 0; $a < count($result['data']); $a++)
            {
                $slsOrdHdrData = $result['data'][$a];

                //find associated sales order
                $slsOrdHdrModel = SlsOrdHdr::where('doc_code', $slsOrdHdrData['doc_code'])
                    ->lockForUpdate()
                    ->first();
                
                //sales order not found, return null
                if (empty($slsOrdHdrModel)) {
                    continue;
                }

                if($slsOrdHdrModel->doc_status != DocStatus::$MAP['VOID'])
                {
                    $slsOrdHdrModel->desc_01 = $slsOrdHdrData['void_remark'];
                    $slsOrdHdrModel->doc_status = DocStatus::$MAP['VOID'];
                    $slsOrdHdrModel->save();
                }

                //DRAFT
                //$slsOrdHdrModel = SlsOrdHdrRepository::syncSlsOrdSync01($company, $division, $slsOrdHdrData);

                $statusNumber = 100;
                if($total > 0)
                {
                    $statusNumber = bcmul(bcdiv(($a + ($page - 1) * $pageSize), $total, 8), 100, 2);
                }
                BatchJobStatusRepository::updateStatusNumber($batchJobStatusModel->id, $statusNumber);
            }

            $lastPage = $total % $pageSize == 0 ? ($total / $pageSize) : ($total / $pageSize) + 1;

            $page++;
        }

        $batchJobStatusModel = BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

        return $batchJobStatusModel;
    }

    public function transitionToStatus($hdrId, $strDocStatus)
    {
        $hdrModel = null;
        if(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['WIP'])
        {
            $hdrModel = self::transitionToWip($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['COMPLETE'])
        {
            $hdrModel = self::transitionToComplete($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['DRAFT'])
        {
            $hdrModel = self::transitionToDraft($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['VOID'])
        {
            $hdrModel = self::transitionToVoid($hdrId);
        }
        
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $message = __('SlsOrd.document_successfully_transition', ['docCode'=>$documentHeader->doc_code,'strDocStatus'=>$strDocStatus]);
        
        return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>array(),
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }

    static public function processOutgoingHeader($model, $isShowPrint = false)
	{
        $docFlows = self::processDocFlows($model);
        $model->doc_flows = $docFlows;
        
		if($isShowPrint)
		{
			$printDocTxn = PrintDocTxnRepository::queryPrintDocTxn(SlsOrdHdr::class, $model->id);
			$model->print_count = $printDocTxn->print_count;
			$model->first_printed_at = $printDocTxn->first_printed_at;
			$model->last_printed_at = $printDocTxn->last_printed_at;
        }
        
        $division = $model->division;
        $model->division_code = $division->code;
        $company = $model->company;
        $model->company_code = $company->code;

        $debtor = $model->debtor;
        $model->debtor_code = $debtor->code;
        $model->debtor_ref_code = $debtor->ref_code;
        $model->debtor_company_name_01 = $debtor->company_name_01;
        $model->debtor_company_name_02 = $debtor->company_name_02;
        
        $salesman = $model->salesman;
        $model->salesman_code = $salesman->username;

        $model->str_doc_status = DocStatus::$MAP[$model->doc_status];

        //debtor select2
        $initDebtorOption = array('value'=>0,'label'=>'');
        $debtor = DebtorRepository::findByPk($model->debtor_id);
        if(!empty($debtor))
        {
            $initDebtorOption = array('value'=>$debtor->id, 'label'=>$debtor->code);
        }
        $model->debtor_select2 = $initDebtorOption;
        
        //currency select2
        $initCurrencyOption = array('value'=>0,'label'=>'');
        $currency = CurrencyRepository::findByPk($model->currency_id);
        if(!empty($currency))
        {
            $initCurrencyOption = array('value'=>$currency->id, 'label'=>$currency->symbol);
        }
        $model->currency_select2 = $initCurrencyOption;

        //creditTerm select2
        $initCreditTermOption = array('value'=>0,'label'=>'');
        $creditTerm = CreditTermRepository::findByPk($model->credit_term_id);
        if(!empty($creditTerm))
        {
            $initCreditTermOption = array('value'=>$creditTerm->id, 'label'=>$creditTerm->code);
        }
        $model->credit_term_select2 = $initCreditTermOption;

        //salesman select2
        $salesman = $model->salesman;
        $initSalesmanOption = array(
            'value'=>$salesman->id,
            'label'=>$salesman->username
        );
        $model->salesman_select2 = $initSalesmanOption;

        //biz partner select2
        // $deliveryPoint = $model->deliveryPoint;
        // $model->delivery_point_code = $deliveryPoint->code;
        // $model->delivery_point_unit_no = $deliveryPoint->unit_no;
        // $model->delivery_point_building_name = $deliveryPoint->building_name;
        // $model->delivery_point_street_name = $deliveryPoint->street_name;
        // $model->delivery_point_district_01 = $deliveryPoint->district_01;
        // $model->delivery_point_district_02 = $deliveryPoint->district_02;
        // $model->delivery_point_postcode = $deliveryPoint->postcode;
        // $model->delivery_point_state_name = $deliveryPoint->state_name;
        // $model->delivery_point_country_name = $deliveryPoint->country_name;
        // $initDeliveryPointOption = array(
        //     'value'=>$deliveryPoint->id,
        //     'label'=>$deliveryPoint->code.' '.$deliveryPoint->company_name_01
        // );
        // $model->delivery_point_select2 = $initDeliveryPointOption;

        $model->hdr_disc_val_01 = 0;
        $model->hdr_disc_perc_01 = 0;
        $model->hdr_disc_val_02 = 0;
        $model->hdr_disc_perc_02 = 0;
        $model->hdr_disc_val_03 = 0;
        $model->hdr_disc_perc_03 = 0;
        $model->hdr_disc_val_04 = 0;
        $model->hdr_disc_perc_04 = 0;
        $model->hdr_disc_val_05 = 0;
        $model->hdr_disc_perc_05 = 0;
        $firstDtlModel = SlsOrdDtlRepository::findTopByHdrId($model->id);
        if(!empty($firstDtlModel))
        {
            $model->hdr_disc_val_01 = $firstDtlModel->hdr_disc_val_01;
            $model->hdr_disc_perc_01 = $firstDtlModel->hdr_disc_perc_01;
            $model->hdr_disc_val_02 = $firstDtlModel->hdr_disc_val_02;
            $model->hdr_disc_perc_02 = $firstDtlModel->hdr_disc_perc_02;
            $model->hdr_disc_val_03 = $firstDtlModel->hdr_disc_val_03;
            $model->hdr_disc_perc_03 = $firstDtlModel->hdr_disc_perc_03;
            $model->hdr_disc_val_04 = $firstDtlModel->hdr_disc_val_04;
            $model->hdr_disc_perc_04 = $firstDtlModel->hdr_disc_perc_04;
            $model->hdr_disc_val_05 = $firstDtlModel->hdr_disc_val_05;
            $model->hdr_disc_perc_05 = $firstDtlModel->hdr_disc_perc_05;
        }

		return $model;
    }
    
    static public function processOutgoingDetail($model)
    {
        $item = $model->item;
        $model->item_code = $item->code;
        //$model->item_desc_01 = $item->desc_01;
        //$model->item_desc_02 = $item->desc_02;
        
        if ($model->promo_hdr_id > 0)
        {
            $promotion = $model->promotion;
            $model->promo_code = $promotion->code;
            $model->promo_desc = $promotion->desc_01;
        }

        //item select2
        $initItemOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($item))
        {
            $initItemOption = array(
                'value'=>$item->id, 
                'label'=>$item->code.' '.$item->desc_01,
            );
        }
        $model->item_select2 = $initItemOption;

        $uom = $model->uom;
		//uom select2
		$model->uom_code = '';
        $initUomOption = array(
            'value'=>0,
            'label'=>''
		);
        if(!empty($uom))
        {
			$model->uom_code = $uom->code;
            $initUomOption = array('value'=>$uom->id,'label'=>$uom->code);
        }        
        $model->uom_select2 = $initUomOption;

        $model = ItemService::processCaseLoose($model, $item);

        return $model;
	}

    static public function transitionToWip($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = SlsOrdHdrRepository::txnFindByPk($hdrId);
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status >= DocStatus::$MAP['WIP'])
		{
			$exc = new ApiException(__('SlsOrd.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\SlsOrdHdr::class, $hdrModel->id);
            throw $exc;
		}

		//commit the document
		$hdrModel = SlsOrdHdrRepository::commitToWip($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('SlsOrd.commit_to_wip_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\SlsOrdHdr::class, $hdrModel->id);
        throw $exc;
	}

	static public function transitionToComplete($hdrId)
    {
        AuthService::authorize(
			array(
				'sls_ord_confirm'
			)
        );

		$isSuccess = false;
		//use transaction to make sure this is latest value
		$hdrModel = SlsOrdHdrRepository::txnFindByPk($hdrId);

		//only DRAFT or above can transition to COMPLETE
		if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE']
		|| $hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('SlsOrd.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\SlsOrdHdr::class, $hdrModel->id);
            throw $exc;
		}

		$dtlModels = SlsOrdDtlRepository::findAllByHdrId($hdrModel->id);
		//preliminary checking
		//check the detail stock make sure the detail quant_bal_id > 0
		foreach($dtlModels as $dtlModel)
		{
			if($dtlModel->item_id == 0)
			{
				$exc = new ApiException(__('SlsOrd.item_is_required', ['lineNo'=>$dtlModel->line_no]));
				$exc->addData(\App\SlsOrdDtl::class, $dtlModel->id);
				throw $exc;
			}
		}

		//commit the document
		$hdrModel = SlsOrdHdrRepository::commitToComplete($hdrModel->id, true);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
        }
        
		$exc = new ApiException(__('SlsOrd.commit_to_complete_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\SlsOrdHdr::class, $hdrModel->id);
        throw $exc;
    }
    
    static public function transitionToDraft($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = SlsOrdHdrRepository::txnFindByPk($hdrId);

		//only WIP or COMPLETE can transition to DRAFT
		if($hdrModel->doc_status == DocStatus::$MAP['WIP'])
		{
            $hdrModel = SlsOrdHdrRepository::revertWipToDraft($hdrModel->id);
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		elseif($hdrModel->doc_status == DocStatus::$MAP['COMPLETE'])
		{
            AuthService::authorize(
                array(
                    'sls_ord_revert'
                )
            );

            $hdrModel = SlsOrdHdrRepository::revertCompleteToDraft($hdrModel->id);
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
        }
        elseif($hdrModel->doc_status == DocStatus::$MAP['VOID'])
		{
            AuthService::authorize(
                array(
                    'sls_ord_confirm'
                )
            );

			$hdrModel = SlsOrdHdrRepository::commitVoidToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
        
        $exc = new ApiException(__('SlsOrd.doc_status_is_not_wip_or_complete', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\SlsOrdHdr::class, $hdrModel->id);
		throw $exc;
    }

    public static function checkStock($siteFlow, $slsOrdHdr, $slsOrdDtls)
    {
        $deliveryPoint = $slsOrdHdr->deliveryPoint;
        //check the inventory
        foreach($slsOrdDtls as $slsOrdDtl)
        {
            $item = $slsOrdDtl->item;

            $pickingCriterias = PickingCriteriaService::processByDeliveryPointAndItem($siteFlow, $deliveryPoint, $item);

            //DB::connection()->enableQueryLog();
            $availUnitQty = QuantBalRepository::queryAvailUnitQtyBySiteIdAndLocationId(
                $slsOrdHdr->company_id,
                $slsOrdDtl->item_id,
                $siteFlow->site_id,
                $siteFlow->id,
                $slsOrdDtl->location_id,
                $pickingCriterias
            );
            //dd(DB::getQueryLog());

            //DB::connection()->enableQueryLog();
            $rsvdUnitQty = OutbOrdHdrRepository::queryRsvdUnitQtyBySiteFlowId(
                $slsOrdHdr->company_id,
                $slsOrdDtl->item_id,
                $siteFlow->id,
                $slsOrdDtl->location_id
            );
            //dd(DB::getQueryLog());

            $availUnitQty = bcsub($availUnitQty, $rsvdUnitQty);

            $slsOrdDtl->request_qty = bcmul($slsOrdDtl->qty, $slsOrdDtl->uom_rate, $item->qty_scale);
            $slsOrdDtl->available_qty = $availUnitQty;
        }

        return $slsOrdDtls;
    }

    static public function transitionToVoid($hdrId)
    {
        AuthService::authorize(
            array(
                'sls_ord_revert'
            )
        );

		//use transaction to make sure this is latest value
		$hdrModel = SlsOrdHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('SlsOrd.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\SlsOrdHdr::class, $hdrModel->id);
            throw $exc;
		}

		//revert the document
		$hdrModel = SlsOrdHdrRepository::revertDraftToVoid($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('SlsOrd.commit_to_void_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\SlsOrdHdr::class, $hdrModel->id);
        throw $exc;
    }
    
    public function initHeader($divisionId)
    {
        AuthService::authorize(
            array(
                'sls_ord_create'
            )
        );

        $user = Auth::user();

        $model = new SlsOrdHdr();
        $model->doc_flows = array();
        $model->doc_status = DocStatus::$MAP['DRAFT'];
        $model->str_doc_status = DocStatus::$MAP[$model->doc_status];
        $model->doc_code = '';
        $model->ref_code_01 = '';
        $model->ref_code_02 = '';
        $model->ref_code_03 = '';
        $model->ref_code_04 = '';
        $model->ref_code_05 = '';
        $model->ref_code_06 = '';
        $model->doc_date = date('Y-m-d');
        $model->est_del_date = date('Y-m-d');
        $model->desc_01 = '';
        $model->desc_02 = '';

        $division = DivisionRepository::findByPk($divisionId);
        $model->division_id = $divisionId;
        $model->division_code = '';
        $model->site_flow_id = 0;
        $model->company_id = 0;
        $model->company_code = '';
        if(!empty($division))
        {
            $model->division_code = $division->code;
            $model->site_flow_id = $division->site_flow_id;
            $company = $division->company;
            $model->company_id = $company->id;
            $model->company_code = $company->code;
        }

        $model->doc_no_id = 0;
        $docNoIdOptions = array();
        $docNos = DocNoRepository::findAllDivisionDocNo($divisionId, \App\SlsOrdHdr::class, $model->doc_date);
        for($a = 0; $a < count($docNos); $a++)
        {
            $docNo = $docNos[$a];
            if($a == 0)
            {
                $model->doc_no_id = $docNo->id;
            }
            $docNoIdOptions[] = array('value'=>$docNo->id, 'label'=>$docNo->latest_code);
        }
        $model->doc_no_id_options = $docNoIdOptions;

        //currency dropdown
        $model->currency_id = 0;
        $model->currency_rate = 1;
        $initCurrencyOption = array('value'=>0,'label'=>'');
        $model->currency_select2 = $initCurrencyOption;

        //creditTerm select2
        $initCreditTermOption = array('value'=>0,'label'=>'');
        $model->credit_term_select2 = $initCreditTermOption;

        //salesman select2
        $initSalesmanOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($user))
        {
            $initSalesmanOption = array(
                'value'=>$user->id, 
                'label'=>$user->username
            );
        }
        $model->salesman_select2 = $initSalesmanOption;

        //delivery_point select2
        
        $model->delivery_point_unit_no = '';
        $model->delivery_point_building_name = '';
        $model->delivery_point_street_name = '';
        $model->delivery_point_district_01 = '';
        $model->delivery_point_district_02 = '';
        $model->delivery_point_postcode = '';
        $model->delivery_point_state_name = '';
        $model->delivery_point_country_name = '';
        $initDeliveryPointOption = array(
            'value'=>0,
            'label'=>''
        );
        $deliveryPoint = DeliveryPointRepository::findTop();
        if(!empty($deliveryPoint))
        {
            $initDeliveryPointOption = array(
                'value'=>$deliveryPoint->id, 
                'label'=>$deliveryPoint->code.' '.$deliveryPoint->company_name_01
            );

            $model->delivery_point_unit_no = $deliveryPoint->unit_no;
            $model->delivery_point_building_name = $deliveryPoint->building_name;
            $model->delivery_point_street_name = $deliveryPoint->street_name;
            $model->delivery_point_district_01 = $deliveryPoint->district_01;
            $model->delivery_point_district_02 = $deliveryPoint->district_02;
            $model->delivery_point_postcode = $deliveryPoint->postcode;
            $model->delivery_point_state_name = $deliveryPoint->state_name;
            $model->delivery_point_country_name = $deliveryPoint->country_name;
        }
        $model->delivery_point_select2 = $initDeliveryPointOption;

        $model->hdr_disc_val_01 = 0;
        $model->hdr_disc_perc_01 = 0;
        $model->hdr_disc_val_02 = 0;
        $model->hdr_disc_perc_02 = 0;
        $model->hdr_disc_val_03 = 0;
        $model->hdr_disc_perc_03 = 0;
        $model->hdr_disc_val_04 = 0;
        $model->hdr_disc_perc_04 = 0;
        $model->hdr_disc_val_05 = 0;
        $model->hdr_disc_perc_05 = 0;

        $model->disc_amt = 0;
        $model->tax_amt = 0;
        $model->round_adj_amt = 0;
        $model->net_amt = 0;

        return $model;
    }

    public function showHeader($hdrId)
    {
        AuthService::authorize(
            array(
                'sls_ord_read',
				'sls_ord_update'
            )
        );

		$model = SlsOrdHdrRepository::findByPk($hdrId);
		if(!empty($model))
        {
            $model = self::processOutgoingHeader($model);
        }
		return $model;
    }

    public function updateHeader($hdrData)
	{
        AuthService::authorize(
            array(
                'sls_ord_update'
            )
        );

        $hdrData = self::processIncomingHeader($hdrData);

		//get the associated inbOrdHdr
        $slsOrdHdr = SlsOrdHdrRepository::txnFindByPk($hdrData['id']);
        if($slsOrdHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('SlsOrd.doc_status_is_wip_or_complete', ['docCode'=>$slsOrdHdr->doc_code]));
            $exc->addData(\App\SlsOrdHdr::class, $slsOrdHdr->id);
            throw $exc;
        }
        $slsOrdHdrData = $slsOrdHdr->toArray();
        //assign hdrData to model
        foreach($hdrData as $field => $value) 
        {
            if(strpos($field, 'hdr_disc_val') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_disc_perc') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_tax_val') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_tax_perc') !== false)
            {
                continue;
            }
            $slsOrdHdrData[$field] = $value;
        }

		//$slsOrdHdr->load('inbOrdHdr', 'bizPartner');
        //$bizPartner = $slsOrdHdr->bizPartner;
        
        $slsOrdDtlArray = array();

        //check if hdr disc or hdr tax is set and more than 0
        $needToProcessDetails = false;
        $firstDtlModel = SlsOrdDtlRepository::findTopByHdrId($hdrData['id']);
        if(!empty($firstDtlModel))
        {
            $firstDtlData = $firstDtlModel->toArray();
            for($a = 1; $a <= 5; $a++)
            {
                if(array_key_exists('hdr_disc_val_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_disc_val_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_disc_val_0'.$a], $firstDtlData['hdr_disc_val_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
                if(array_key_exists('hdr_disc_perc_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_disc_perc_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_disc_perc_0'.$a], $firstDtlData['hdr_disc_perc_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }

                if(array_key_exists('hdr_tax_val_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_tax_val_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_tax_val_0'.$a], $firstDtlData['hdr_tax_val_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
                if(array_key_exists('hdr_tax_perc_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_tax_perc_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_tax_perc_0'.$a], $firstDtlData['hdr_tax_perc_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
            }
        }

        if($needToProcessDetails)
        {
            //query the slsOrdDtlModels from DB
            //format to array, with is_modified field to indicate it is changed
            $slsOrdDtlModels = SlsOrdDtlRepository::findAllByHdrId($hdrData['id']);
            foreach($slsOrdDtlModels as $slsOrdDtlModel)
            {
                $slsOrdDtlData = $slsOrdDtlModel->toArray();
                for($a = 1; $a <= 5; $a++)
                {
                    if(array_key_exists('hdr_disc_val_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_disc_val_0'.$a], 0, 5) > 0)
                    {
                        $slsOrdDtlData['hdr_disc_val_0'.$a] = $hdrData['hdr_disc_val_0'.$a];
                    }
                    if(array_key_exists('hdr_disc_perc_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_disc_perc_0'.$a], 0, 5) > 0)
                    {
                        $slsOrdDtlData['hdr_disc_perc_0'.$a] = $hdrData['hdr_disc_perc_0'.$a];
                    }

                    if(array_key_exists('hdr_tax_val_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_tax_val_0'.$a], 0, 5) > 0)
                    {
                        $slsOrdDtlData['hdr_tax_val_0'.$a] = $hdrData['hdr_tax_val_0'.$a];
                    }
                    if(array_key_exists('hdr_tax_perc_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_tax_perc_0'.$a], 0, 5) > 0)
                    {
                        $slsOrdDtlData['hdr_tax_perc_0'.$a] = $hdrData['hdr_tax_perc_0'.$a];
                    }
                }
                $slsOrdDtlData['is_modified'] = 1;
                $slsOrdDtlArray[] = $slsOrdDtlData;
            }
            
            //calculate details amount, use adaptive rounding tax
            $result = $this->calculateAmount($slsOrdHdrData, $slsOrdDtlArray);
            $slsOrdHdrData = $result['hdrData'];
            $slsOrdDtlArray = $result['dtlArray'];
        }
            
        $result = SlsOrdHdrRepository::updateDetails($slsOrdHdrData, $slsOrdDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];

        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('SlsOrd.document_successfully_updated', ['docCode'=>$documentHeader->doc_code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }
    
    public function createHeader($data)
    {		
        AuthService::authorize(
            array(
                'sls_ord_create'
            )
        );

        $data = self::processIncomingHeader($data, true);
        $docNoId = $data['doc_no_id'];
        unset($data['doc_no_id']);
        $ordHdrModel = SlsOrdHdrRepository::createHeader(ProcType::$MAP['NULL'], $docNoId, $data);

        $message = __('SlsOrd.document_successfully_created', ['docCode'=>$ordHdrModel->doc_code]);

		return array(
			'data' => $ordHdrModel->id,
			'message' => $message
		);
    }

    public function changeDeliveryPoint($deliveryPointId)
    {
        AuthService::authorize(
            array(
                'sls_ord_update'
            )
        );

        $results = array();
        $deliveryPoint = DeliveryPointRepository::findByPk($deliveryPointId);
        if(!empty($deliveryPoint))
        {
            $results['delivery_point_unit_no'] = $deliveryPoint->unit_no;
            $results['delivery_point_building_name'] = $deliveryPoint->building_name;
            $results['delivery_point_street_name'] = $deliveryPoint->street_name;
            $results['delivery_point_district_01'] = $deliveryPoint->district_01;
            $results['delivery_point_district_02'] = $deliveryPoint->district_02;
            $results['delivery_point_postcode'] = $deliveryPoint->postcode;
            $results['delivery_point_state_name'] = $deliveryPoint->state_name;
            $results['delivery_point_country_name'] = $deliveryPoint->country_name;
        }
        return $results;
    }

    public function changeItem($hdrId, $itemId)
    {
        AuthService::authorize(
            array(
                'sls_ord_update'
            )
        );

        $slsOrdHdr = SlsOrdHdrRepository::findByPk($hdrId);

        $results = array();
        $item = ItemRepository::findByPk($itemId);
        if(!empty($item)
        && !empty($slsOrdHdr))
        {
            $results['desc_01'] = $item->desc_01;
            $results['desc_02'] = $item->desc_02;
        }
        return $results;
    }

    public function changeItemUom($hdrId, $itemId, $uomId)
    {
        AuthService::authorize(
            array(
                'sls_ord_update'
            )
        );

        $slsOrdHdr = SlsOrdHdrRepository::findByPk($hdrId);

        $results = array();
        $itemUom = ItemUomRepository::findByItemIdAndUomId($itemId, $uomId);
        if(!empty($itemUom)
        && !empty($slsOrdHdr))
        {
            $results['item_id'] = $itemUom->item_id;
            $results['uom_id'] = $itemUom->uom_id;
            $results['uom_rate'] = $itemUom->uom_rate;

            $results = $this->updateSaleItemUom($results, $slsOrdHdr->doc_date, 0, $slsOrdHdr->currency_id, 0, 0);
        }
        return $results;
	}
	
	public function changeCurrency($currencyId)
    {
        AuthService::authorize(
            array(
                'sls_ord_update'
            )
        );

        $results = array();
        $currency = CurrencyRepository::findByPk($currencyId);
        if(!empty($currency))
        {
            $results['currency_rate'] = $currency->currency_rate;
        }
        return $results;
    }

    public function createDetail($hdrId, $data)
	{
        AuthService::authorize(
            array(
                'sls_ord_update'
            )
        );

        $data = self::processIncomingDetail($data);

        $slsOrdHdr = SlsOrdHdrRepository::txnFindByPk($hdrId);
        if($slsOrdHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('SlsOrd.doc_status_is_wip_or_complete', ['docCode'=>$slsOrdHdr->doc_code]));
            $exc->addData(\App\SlsOrdHdr::class, $slsOrdHdr->id);
            throw $exc;
        }
		$slsOrdHdrData = $slsOrdHdr->toArray();

		$slsOrdDtlArray = array();
		$slsOrdDtlModels = SlsOrdDtlRepository::findAllByHdrId($hdrId);
		foreach($slsOrdDtlModels as $slsOrdDtlModel)
		{
			$slsOrdDtlData = $slsOrdDtlModel->toArray();
			$slsOrdDtlData['is_modified'] = 0;
			$slsOrdDtlArray[] = $slsOrdDtlData;
		}
		$lastLineNo = count($slsOrdDtlModels);

		//assign temp id
		$tmpUuid = uniqid();
		//append NEW here to make sure it will be insert in repository later
		$data['id'] = 'NEW'.$tmpUuid;
		$data['line_no'] = $lastLineNo + 1;
        $data['is_modified'] = 1;
		$data = $this->updateSaleItemUom($data, $slsOrdHdr['doc_date'], 0, $slsOrdHdr['currency_id'], 0, 0);
		$slsOrdDtlData = $this->initSlsOrdDtlData($data);
		$slsOrdDtlArray[] = $slsOrdDtlData;

		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($slsOrdHdrData, $slsOrdDtlArray);
		$slsOrdHdrData = $result['hdrData'];
        $slsOrdDtlArray = $result['dtlArray'];

		$result = SlsOrdHdrRepository::updateDetails($slsOrdHdrData, $slsOrdDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('SlsOrd.detail_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function deleteDetails($hdrId, $dtlArray)
	{
        AuthService::authorize(
            array(
                'sls_ord_update'
            )
        );

        $slsOrdHdr = SlsOrdHdrRepository::txnFindByPk($hdrId);
        if($slsOrdHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('SlsOrd.doc_status_is_wip_or_complete', ['docCode'=>$slsOrdHdr->doc_code]));
            $exc->addData(\App\SlsOrdHdr::class, $slsOrdHdr->id);
            throw $exc;
        }
		$slsOrdHdrData = $slsOrdHdr->toArray();

		//query the slsOrdDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$delSlsOrdDtlArray = array();
		$slsOrdDtlArray = array();
        $slsOrdDtlModels = SlsOrdDtlRepository::findAllByHdrId($hdrId);
        $lineNo = 1;
		foreach($slsOrdDtlModels as $slsOrdDtlModel)
		{
			$slsOrdDtlData = $slsOrdDtlModel->toArray();
			$slsOrdDtlData['is_modified'] = 0;
			$slsOrdDtlData['is_deleted'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{
				if($dtlData['id'] == $slsOrdDtlData['id'])
				{
					//this is deleted dtl
					$slsOrdDtlData['is_deleted'] = 1;
					break;
				}
			}
			if($slsOrdDtlData['is_deleted'] > 0)
			{
				$delSlsOrdDtlArray[] = $slsOrdDtlData;
			}
			else
			{
                $slsOrdDtlData['line_no'] = $lineNo;
                $slsOrdDtlData['is_modified'] = 1;
                $slsOrdDtlArray[] = $slsOrdDtlData;
                $lineNo++;
			}
        }
        
		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($slsOrdHdrData, $slsOrdDtlArray, $delSlsOrdDtlArray);
		$slsOrdHdrData = $result['hdrData'];
		$modifiedDtlArray = $result['dtlArray'];
		foreach($slsOrdDtlArray as $slsOrdDtlSeq => $slsOrdDtlData)
		{
			foreach($modifiedDtlArray as $modifiedDtlData)
			{
				if($modifiedDtlData['id'] == $slsOrdDtlData['id'])
				{
					foreach($modifiedDtlData as $field => $value)
					{
						$slsOrdDtlData[$field] = $value;
					}
					$slsOrdDtlData['is_modified'] = 1;
					$slsOrdDtlArray[$slsOrdDtlSeq] = $slsOrdDtlData;
					break;
				}
			}
		}
		
		$result = SlsOrdHdrRepository::updateDetails($slsOrdHdrData, $slsOrdDtlArray, $delSlsOrdDtlArray);
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('SlsOrd.details_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>$delSlsOrdDtlArray
            ),
			'message' => $message
		);
    }

    public function updateDetails($hdrId, $dtlArray)
	{
        AuthService::authorize(
            array(
                'sls_ord_update'
            )
        );

        $slsOrdHdr = SlsOrdHdrRepository::txnFindByPk($hdrId);
        if($slsOrdHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('SlsOrd.doc_status_is_wip_or_complete', ['docCode'=>$slsOrdHdr->doc_code]));
            $exc->addData(\App\SlsOrdHdr::class, $slsOrdHdr->id);
            throw $exc;
        }
		$slsOrdHdrData = $slsOrdHdr->toArray();

		//query the slsOrdDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$slsOrdDtlArray = array();
        $slsOrdDtlModels = SlsOrdDtlRepository::findAllByHdrId($hdrId);
		foreach($slsOrdDtlModels as $slsOrdDtlModel)
		{
			$slsOrdDtlData = $slsOrdDtlModel->toArray();
			$slsOrdDtlData['is_modified'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{                
				if($dtlData['id'] == $slsOrdDtlData['id'])
				{
                    $item = ItemRepository::findByPk($dtlData['item_id']);
                    $dtlData = self::processIncomingDetail($dtlData, $item);
                    
					$dtlData = $this->updateSaleItemUom($dtlData, $slsOrdHdr->doc_date, 0, $slsOrdHdr->currency_id, 0, 0);
					foreach($dtlData as $fieldName => $value)
					{
						$slsOrdDtlData[$fieldName] = $value;
					}
					$slsOrdDtlData['is_modified'] = 1;

					break;
				}
			}
			$slsOrdDtlArray[] = $slsOrdDtlData;
		}
		
		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($slsOrdHdrData, $slsOrdDtlArray);
		$slsOrdHdrData = $result['hdrData'];
		$slsOrdDtlArray = $result['dtlArray'];
		
		$result = SlsOrdHdrRepository::updateDetails($slsOrdHdrData, $slsOrdDtlArray, array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('SlsOrd.details_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }

    public function showDetails($hdrId) 
	{
        AuthService::authorize(
            array(
                'sls_ord_read',
				'sls_ord_update'
            )
        );

		$slsOrdDtls = SlsOrdDtlRepository::findAllByHdrId($hdrId);
		$slsOrdDtls->load(
            'item', 'item.itemUoms', 'item.itemUoms.uom', 'item.itemPhotos', 'promotions.promotion'
        );

        foreach($slsOrdDtls as $slsOrdDtl)
        {
            $slsOrdDtl = self::processOutgoingDetail($slsOrdDtl);
        }
        return $slsOrdDtls;
    }
    
    protected function checkSlsOrdWithInverze($divisionId, $userId)
    {
        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['SLS_ORD_SYNC_01'], $userId);

        $syncSettingHdr = SyncSettingHdrRepository::findByProcTypeAndDivisionId(ProcType::$MAP['SYNC_EFICHAIN_01'], $divisionId);
        if(empty($syncSettingHdr))
        {
            $exc = new ApiException(__('SlsOrd.sync_setting_not_found', ['divisionId'=>$divisionId]));
            //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
            throw $exc;
        }

        $division = DivisionRepository::findByPk($divisionId);
        $company = $division->company;
        $divUrl = '&divisions[]='.$division->code;

        $client = new Client();
        $header = array();

        $page = 1;
        $lastPage = 1;
        $pageSize = 1000;
        $total = 0;

        $usernameSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'username');
        $passwordSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'password');
        $formParams = array(
            'UserLogin[username]' => $usernameSetting->value,
            'UserLogin[password]' => $passwordSetting->value
        );
        $pageSizeSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'page_size');
        if(!empty($pageSizeSetting))
        {
            $pageSize = $pageSizeSetting->value;
        }

        //query from efichain, and update the sales order accordingly
        $slsOrdHdrDataList = array();
        while($page <= $lastPage)
        {
            $url = $syncSettingHdr->url.'/index.php?r=luuWu/getAllSalesOrders&start_created_date=2019-07-01&end_created_date=2019-08-31&page_size='.$pageSize.'&page='.$page.$divUrl;
            
            $response = $client->request('POST', $url, array('headers' => $header, 'form_params' => $formParams));
            $result = $response->getBody()->getContents();
            $result = json_decode($result, true);
            if(empty($result))
            {
                $exc = new ApiException(__('SlsOrd.fail_to_sync', ['url'=>$url]));
                //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
                throw $exc;
            }

            $total = $result['total'];
            for ($a = 0; $a < count($result['data']); $a++)
            {
                $slsOrdHdrData = $result['data'][$a];
                //DRAFT
                $slsOrdHdrModel = SlsOrdHdrRepository::checkSlsOrdWithInverze($company, $division, $slsOrdHdrData);

                $statusNumber = 100;
                if($total > 0)
                {
                    $statusNumber = bcmul(bcdiv(($a + ($page - 1) * $pageSize), $total, 8), 100, 2);
                }
                BatchJobStatusRepository::updateStatusNumber($batchJobStatusModel->id, $statusNumber);
            }

            $lastPage = $total % $pageSize == 0 ? ($total / $pageSize) : ($total / $pageSize) + 1;

            $page++;
        }

        return $batchJobStatusModel;
    }

    public function indexProcess($strProcType, $divisionId, $sorts, $filters = array(), $pageSize = 20) 
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['SLS_ORD_01']) 
			{
                return $this->indexSlsOrdList01($divisionId, $sorts, $filters, $pageSize);
			}
        }
        else
        {
            return $this->index($divisionId, $sorts, $filters, $pageSize);
        }
    }

    public function index($divisionId, $sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
            array(
                'sls_ord_read'
            )
        );
        
        //DB::connection()->enableQueryLog();
        $slsOrdHdrs = SlsOrdHdrRepository::findAll($divisionId, $sorts, $filters, $pageSize);
        $slsOrdHdrs->load(
            'slsOrdDtls'
            //'slsOrdDtls.item', 'slsOrdDtls.item.itemUoms', 'slsOrdDtls.item.itemUoms.uom', 
        );
		foreach($slsOrdHdrs as $slsOrdHdr)
		{
			$slsOrdHdr->str_doc_status = DocStatus::$MAP[$slsOrdHdr->doc_status];

			// $deliveryPoint = $slsOrdHdr->deliveryPoint;
			// $slsOrdHdr->delivery_point_code = $deliveryPoint->code;
			// $slsOrdHdr->delivery_point_company_name_01 = $deliveryPoint->company_name_01;
			// $slsOrdHdr->delivery_point_area_code = $deliveryPoint->area_code;
			// $area = $deliveryPoint->area;
			// if(!empty($area))
			// {
			// 	$slsOrdHdr->delivery_point_area_desc_01 = $area->desc_01;
            // }
            
			$slsOrdHdr->delivery_point_code = '';
			$slsOrdHdr->delivery_point_company_name_01 = '';
			$slsOrdHdr->delivery_point_area_code = '';
            $slsOrdHdr->delivery_point_area_desc_01 = '';

			$debtor = $slsOrdHdr->debtor;
			$slsOrdHdr->debtor_code = $debtor->code;
			$slsOrdHdr->debtor_ref_code = $debtor->ref_code;
			$slsOrdHdr->debtor_company_name_01 = $debtor->company_name_01;
			$slsOrdHdr->debtor_company_name_02 = $debtor->company_name_02;

			$salesman = $slsOrdHdr->salesman;
			$slsOrdHdr->salesman_username = $salesman->username;

			$slsOrdDtls = $slsOrdHdr->slsOrdDtls;
			$slsOrdHdr->details = $slsOrdDtls;
        }
        //Log::error(DB::getQueryLog());
    	return $slsOrdHdrs;
    }
    
    public function indexSlsOrdList01($divisionId, $sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
            array(
                'sls_ord_read'
            )
        );
        
        //DB::connection()->enableQueryLog();
        $slsOrdHdrs = SlsOrdHdrRepository::findAll($divisionId, $sorts, $filters, $pageSize);
        $slsOrdHdrs->load(
            'slsOrdDtls'
        );
		foreach($slsOrdHdrs as $slsOrdHdr)
		{
			$slsOrdHdr->str_doc_status = DocStatus::$MAP[$slsOrdHdr->doc_status];

			// $deliveryPoint = $slsOrdHdr->deliveryPoint;
			// $slsOrdHdr->delivery_point_code = $deliveryPoint->code;
			// $slsOrdHdr->delivery_point_company_name_01 = $deliveryPoint->company_name_01;
			// $slsOrdHdr->delivery_point_area_code = $deliveryPoint->area_code;
			// $area = $deliveryPoint->area;
			// if(!empty($area))
			// {
			// 	$slsOrdHdr->delivery_point_area_desc_01 = $area->desc_01;
			// }

			$debtor = $slsOrdHdr->debtor;
			$slsOrdHdr->debtor_code = $debtor->code;
			$slsOrdHdr->debtor_ref_code = $debtor->ref_code;
			$slsOrdHdr->debtor_company_name_01 = $debtor->company_name_01;
			$slsOrdHdr->debtor_company_name_02 = $debtor->company_name_02;

			$salesman = $slsOrdHdr->salesman;
			$slsOrdHdr->salesman_username = $salesman->username;

			$slsOrdDtls = $slsOrdHdr->slsOrdDtls;
            $slsOrdHdr->details = $slsOrdDtls;
            
            //query sales order -> invoice
            if (!empty($slsOrdHdr->toDocTxnFlows)) {
                foreach ($slsOrdHdr->toDocTxnFlows as $docTxnFlowData) {
                    $toDocHdrModel = $docTxnFlowData['to_doc_hdr_type']::where('id', $docTxnFlowData['to_doc_hdr_id'])->first();
                    $slsOrdHdr->toDocHdrModel = $toDocHdrModel;
                }
            }
        }
        //Log::error(DB::getQueryLog());
    	return $slsOrdHdrs;
    }
    
    
    static public function processIncomingDetail($data, $item)
    {
        $data['qty'] = round($data['qty'], $item->qty_scale);

        //preprocess the select2 and dropDown
        if(array_key_exists('item_select2', $data))
        {
            $data['item_id'] = $data['item_select2']['value'];
            unset($data['item_select2']);
        }
        if(array_key_exists('uom_select2', $data))
        {
            $data['uom_id'] = $data['uom_select2']['value'];
            unset($data['uom_select2']);
        }
        if(array_key_exists('item_code', $data))
        {
            unset($data['item_code']);
        }
        if(array_key_exists('uom_code', $data))
        {
            unset($data['uom_code']);
        }
        if(array_key_exists('item', $data))
        {
            unset($data['item']);
        }
        if(array_key_exists('uom', $data))
        {
            unset($data['uom']);
		}
		if(array_key_exists('promo_code', $data))
        {
            unset($data['promo_code']);
		}
		if(array_key_exists('promo_desc', $data))
        {
            unset($data['promo_desc']);
		}
		if(array_key_exists('item_ref_code_01', $data))
        {
            unset($data['item_ref_code_01']);
		}
		if(array_key_exists('item_desc_01', $data))
        {
            unset($data['item_desc_01']);
		}
		if(array_key_exists('item_desc_02', $data))
        {
            unset($data['item_desc_02']);
		}
		if(array_key_exists('storage_class', $data))
        {
            unset($data['storage_class']);
		}
		if(array_key_exists('item_group_01_code', $data))
        {
            unset($data['item_group_01_code']);
		}
		if(array_key_exists('item_group_02_code', $data))
        {
            unset($data['item_group_02_code']);
		}
		if(array_key_exists('item_group_03_code', $data))
        {
            unset($data['item_group_03_code']);
		}
		if(array_key_exists('item_group_04_code', $data))
        {
            unset($data['item_group_04_code']);
		}
		if(array_key_exists('item_group_05_code', $data))
        {
            unset($data['item_group_05_code']);
		}
		if(array_key_exists('item_cases_per_pallet_length', $data))
        {
            unset($data['item_cases_per_pallet_length']);
		}
		if(array_key_exists('item_cases_per_pallet_width', $data))
        {
            unset($data['item_cases_per_pallet_width']);
		}
		if(array_key_exists('item_no_of_layers', $data))
        {
            unset($data['item_no_of_layers']);
		}

		if(array_key_exists('unit_qty', $data))
        {
            unset($data['unit_qty']);
		}
		if(array_key_exists('item_unit_uom_code', $data))
        {
            unset($data['item_unit_uom_code']);
		}
		if(array_key_exists('loose_qty', $data))
        {
            unset($data['loose_qty']);
		}
		if(array_key_exists('item_loose_uom_code', $data))
        {
            unset($data['item_loose_uom_code']);
        }
        if(array_key_exists('loose_uom_id', $data))
        {
            unset($data['loose_uom_id']);
		}
		if(array_key_exists('loose_uom_rate', $data))
        {
            unset($data['loose_uom_rate']);
		}
		if(array_key_exists('case_qty', $data))
        {
            unset($data['case_qty']);
		}
		if(array_key_exists('case_uom_rate', $data))
        {
            unset($data['case_uom_rate']);
		}
        if(array_key_exists('item_case_uom_code', $data))
        {
            unset($data['item_case_uom_code']);
        }
        if(array_key_exists('uom_code', $data))
        {
            unset($data['uom_code']);
		}
		if(array_key_exists('item_unit_barcode', $data))
        {
            unset($data['item_unit_barcode']);
		}
		if(array_key_exists('item_case_barcode', $data))
        {
            unset($data['item_case_barcode']);
		}
		if(array_key_exists('gross_weight', $data))
        {
            unset($data['gross_weight']);
		}
		if(array_key_exists('cubic_meter', $data))
        {
            unset($data['cubic_meter']);
		}
		
		if(array_key_exists('location_code', $data))
        {
            unset($data['location_code']);
		}
		if(array_key_exists('location', $data))
        {
            unset($data['location']);
		}		
        return $data;
    }

    static public function processIncomingHeader($data, $isClearHdrDiscTax=false)
    {
        if(array_key_exists('doc_flows', $data))
        {
            unset($data['doc_flows']);
        }
        if(array_key_exists('submit_action', $data))
        {
            unset($data['submit_action']);
        }
        //preprocess the select2 and dropDown
        if(array_key_exists('debtor_select2', $data))
        {
            $data['debtor_id'] = $data['debtor_select2']['value'];
            unset($data['debtor_select2']);
        }
        if(array_key_exists('credit_term_select2', $data))
        {
            $data['credit_term_id'] = $data['credit_term_select2']['value'];
            unset($data['credit_term_select2']);
        }
        if(array_key_exists('currency_select2', $data))
        {
            $data['currency_id'] = $data['currency_select2']['value'];
            unset($data['currency_select2']);
        }   
        if(array_key_exists('salesman_select2', $data))
        {
            $data['salesman_id'] = $data['salesman_select2']['value'];
            unset($data['salesman_select2']);
        }
        // if(array_key_exists('delivery_point_select2', $data))
        // {
        //     $data['delivery_point_id'] = $data['delivery_point_select2']['value'];
        //     unset($data['delivery_point_select2']);
        // }
        if(array_key_exists('str_doc_status', $data))
        {
            unset($data['str_doc_status']);
        }
        if(array_key_exists('division_code', $data))
        {
            unset($data['division_code']);
        }
        if(array_key_exists('company_code', $data))
        {
            unset($data['company_code']);
        }
        if(array_key_exists('debtor_code', $data))
        {
            unset($data['debtor_code']);
        }
        if(array_key_exists('debtor_ref_code', $data))
        {
            unset($data['debtor_ref_code']);
        }
        if(array_key_exists('debtor_company_name_01', $data))
        {
            unset($data['debtor_company_name_01']);
        }
        if(array_key_exists('debtor_company_name_02', $data))
        {
            unset($data['debtor_company_name_02']);
        }
        // if(array_key_exists('delivery_point_code', $data))
        // {
        //     unset($data['delivery_point_code']);
        // }
        // if(array_key_exists('delivery_point_unit_no', $data))
        // {
        //     unset($data['delivery_point_unit_no']);
        // }
        // if(array_key_exists('delivery_point_building_name', $data))
        // {
        //     unset($data['delivery_point_building_name']);
        // }
        // if(array_key_exists('delivery_point_street_name', $data))
        // {
        //     unset($data['delivery_point_street_name']);
        // }
        // if(array_key_exists('delivery_point_district_01', $data))
        // {
        //     unset($data['delivery_point_district_01']);
        // }
        // if(array_key_exists('delivery_point_district_02', $data))
        // {
        //     unset($data['delivery_point_district_02']);
        // }
        // if(array_key_exists('delivery_point_postcode', $data))
        // {
        //     unset($data['delivery_point_postcode']);
        // }
        // if(array_key_exists('delivery_point_state_name', $data))
        // {
        //     unset($data['delivery_point_state_name']);
        // }
        // if(array_key_exists('delivery_point_country_name', $data))
        // {
        //     unset($data['delivery_point_country_name']);
        // }
        if(array_key_exists('division', $data))
        {
            unset($data['division']);
        }
        if(array_key_exists('company', $data))
        {
            unset($data['company']);
        }
        if(array_key_exists('salesman', $data))
        {
            unset($data['salesman']);
        }
        if(array_key_exists('salesman_username', $data))
        {
            unset($data['salesman_username']);
        }
        if(array_key_exists('delivery_point', $data))
        {
            unset($data['delivery_point']);
        }

        if($isClearHdrDiscTax)
        {  
            for($a = 1; $a <= 5; $a++)
            {
                if(array_key_exists('hdr_disc_val_0'.$a, $data))
                {
                    unset($data['hdr_disc_val_0'.$a]);
                }
                if(array_key_exists('hdr_disc_perc_0'.$a, $data))
                {
                    unset($data['hdr_disc_perc_0'.$a]);
                }
                if(array_key_exists('hdr_tax_val_0'.$a, $data))
                {
                    unset($data['hdr_tax_val_0'.$a]);
                }
                if(array_key_exists('hdr_tax_perc_0'.$a, $data))
                {
                    unset($data['hdr_tax_perc_0'.$a]);
                }
            }
        }
        return $data;
    }
}
