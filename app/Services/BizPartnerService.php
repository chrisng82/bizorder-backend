<?php

namespace App\Services;

use App\Services\Env\ProcType;
use App\Repositories\BizPartnerRepository;
class BizPartnerService 
{
    public function __construct() 
	{
    }
    
    public function select2($search, $filters)
    {
        $bizPartners = BizPartnerRepository::select2($search, $filters);
        return $bizPartners;
    }

    public function select2Init($id)
    {
        $bizPartner = null;
        if($id > 0) 
        {
            $bizPartner = BizPartnerRepository::findByPk($id);
        }
        else
        {
            $bizPartner = BizPartnerRepository::findTop();
        }
        $option = array('value'=>0, 'label'=>'');
        if(!empty($bizPartner))
        {
            $option = array(
                'value'=>$bizPartner->id, 
                'label'=>$bizPartner->code.' '.$bizPartner->company_name_01
            );
        }
        return $option;
    }
}
