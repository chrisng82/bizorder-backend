<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use App\User;
use App\Role;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;
use App\Services\Env\ResStatus;
use App\Repositories\UserRepository;
use App\Repositories\DeviceRepository;
use Illuminate\Support\Facades\Log;

use Exception;

class AuthService
{
    public function authenticate($username, $password, $rememberMe)
    {        
        Log::error($username.' '.$password.' '.$rememberMe);
        try 
        {
            if(Auth::attempt(array(
                'username' => $username,
                'password' => $password,
                'status' => ResStatus::$MAP['ACTIVE']
            )))
            { 
                $user = Auth::user(); 
                $user->last_login = Carbon::now()->toDateTimeString();
                $user->save();

                $user->token = $user->createToken('LuuWu')->accessToken;
                
                return ResponseServices::success(__('Auth.login_successfully', []))->data($user)->tojson();
            } 
            else
            { 
                throw new Exception("Invalid credentials for user ".$username);
            }
        } 
        catch (\Exception $e) 
        {
            return ResponseServices::error($e->getMessage())->toJson();
        }
    }

    public function logout($fcmToken)
    {
        try 
        {
            $user = Auth::user();
            $token = $user->token();
            $token->revoke();
            $token->delete();

            Auth::guard('web')->logout();
            
            $device = DeviceRepository::findByToken($fcmToken);

            if(!empty($device)) {
                $device = DeviceRepository::unregister($device);
            } 

            return ResponseServices::success(__('Auth.logout_successful', []))->tojson();
        } 
        catch (\Exception $e) 
        {
            return ResponseServices::error($e->getMessage())->toJson();
        }
    }

    public function changePassword($currentPassword, $newPassword)
    {
        $user = Auth::user();
        if ($user !== null) 
        {            
            // Checking current password
            if(!Hash::check($currentPassword, $user->password)) 
            {
                $exc = new ApiException(__('Auth.your_current_password_is_incorrect'));
                //$exc->addData(\App\User::class, $user->id);
                throw $exc;
            }

            if(strcmp($currentPassword, $newPassword) == 0)
            {
                $exc = new ApiException(__('Auth.you_must_set_a_new_password'));
                //$exc->addData(\App\User::class, $user->id);
                throw $exc;
            }

            $user = UserRepository::changePassword($user->id, $newPassword);
            if($user !== null)
            {
                $message = __('Auth.your_password_has_been_changed_successfully');
            }

            return array(
                'data' => array(
                    'password' => '',
                    'current_password' => '',
                    'new_password' => '',
                    'retype_new_password' => ''
                ),
                'message' => $message
            );

        } else {
            
            throw new ApiException(__('Auth.your_current_password_is_incorrect'));
        }
    }

    public static function authorize($permissions)
    {
        $user = Auth::user();
        $isAuthorize = false;
        foreach($permissions as $permission)
        {
            if($user->can($permission)) 
            {
                $isAuthorize = true;
                break;
            }
        }
        if($isAuthorize == false)
        {
            $names = '';
            for($a = 0; $a < count($permissions); $a++)
            {
                $permission = $permissions[$a];
                if($a == 0)
                {
                    $names = $permission;
                }
                else
                {
                    $names.= ' '.__('Auth.or').' '.$permission;
                }
            }
            $exc = new ApiException(__('Auth.permission_required', ['names'=>$names]));
            throw $exc;
        }
    }
}
