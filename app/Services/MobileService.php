<?php

namespace App\Services;

use App\MobileProfile;
use App\Repositories\MobileHistoryRepository;
use App\Repositories\MobileProfileRepository;
use App\Repositories\SMobileSchemaRepository;
use App\Services\Env\MobileApp;
use App\Services\Env\MobileOS;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\DB;

class MobileService 
{
	public function __construct() 
	{
    }

    public function register($deviceUuid, $mobileApp, $mobileOs, $osVersion, $appVersion) 
	{
        AuthService::authorize(
            array(
                'mobile_register'
            )
        );

        $mobileProfile = MobileProfileRepository::findByUuidAndMobileApp($deviceUuid, MobileApp::$MAP[$mobileApp]);
        if(empty($mobileProfile))
        {
            //if this profile is not found, need to create a new profile for this device
            $mobileProfile = MobileProfileRepository::create($deviceUuid, MobileApp::$MAP[$mobileApp], MobileOS::$MAP[$mobileOs], $osVersion, $appVersion);
        }
        else
        {
            $mobileProfile = MobileProfileRepository::update($mobileProfile->id, MobileOS::$MAP[$mobileOs], $osVersion, $appVersion);
        }
        //resolve the const back to string
        $mobileProfile->mobile_app = MobileApp::$MAP[$mobileProfile->mobile_app];
        $mobileProfile->mobile_os = MobileOS::$MAP[$mobileProfile->mobile_os];
        return $mobileProfile;
    }

    public function indexTableSchema($mobileProfileId) 
	{
        AuthService::authorize(
            array(
                'mobile_table_schema'
            )
        );

        $mobileProfile = MobileProfileRepository::findByPk($mobileProfileId);
        if(empty($mobileProfile))
		{
			$exc = new ApiException(__('Mobile.mobile_profile_not_found', ['mobileProfileId'=>$mobileProfileId]));
			$exc->addData(\App\MobileProfile::class, $mobileProfileId);
			throw $exc;
        }

        $sMobileSchemas = SMobileSchemaRepository::findAllByMobileApp($mobileProfile->mobile_app);

        //get the latest migration record for each table_name 
        //query the migrations table, and format it to hashtable by table_name
        $versionHashByTableName = array();
        $migrations = DB::table('migrations')->orderBy('id')->get();
        foreach($migrations as $migration)
        {
            foreach($sMobileSchemas as $sMobileSchema)
            {
                if (strpos($migration->migration, $sMobileSchema->table_name) !== false) 
                {
                    //found the table name migration
                    //extract the datetime of this migration, use this as version
                    $datetime = substr($migration->migration, 0, 17);
                    $versionHashByTableName[$sMobileSchema->table_name] = $datetime;
                }
            }            
        }

        foreach($sMobileSchemas as $sMobileSchema)
        {
            //direct access the DB because this is raw query
            $tableSchema = DB::select('DESCRIBE '.$sMobileSchema->table_name);
            $indexes = DB::select('SHOW INDEX FROM '.$sMobileSchema->table_name);

            $version = '';
            if(array_key_exists($sMobileSchema->table_name, $versionHashByTableName))
            {
                $version = $versionHashByTableName[$sMobileSchema->table_name];
            }
            $sMobileSchema->version = $version;
            $sMobileSchema->columns = $tableSchema;
            $sMobileSchema->indexes = $indexes;
        }
        
        return $sMobileSchemas;
    }

    public function indexData($mobileProfileId, $tableName, $pageSize = 10000) 
	{
        AuthService::authorize(
            array(
                'mobile_data'
            )
        );

        $mobileProfile = MobileProfileRepository::findByPk($mobileProfileId);
        if(empty($mobileProfile))
		{
			$exc = new ApiException(__('Mobile.mobile_profile_not_found', ['mobileProfileId'=>$mobileProfileId]));
			$exc->addData(\App\MobileProfile::class, $mobileProfileId);
			throw $exc;
        }

        $sMobileSchema = SMobileSchemaRepository::findByMobileAppAndTableName($mobileProfile->mobile_app, $tableName);
        if(empty($sMobileSchema))
		{
			$exc = new ApiException(__('Mobile.mobile_schema_not_found', ['mobileApp'=>$mobileProfile->mobile_app, 'tableName'=>$tableName]));
			throw $exc;
        }

        //direct access the DB because this is raw query
        try
        {
            //DB::connection()->enableQueryLog();
            $tableDatas = DB::table($sMobileSchema->table_name)
            ->when(!empty($sMobileSchema->join_type_01), function ($query) use ($sMobileSchema) {
                return $query->{$sMobileSchema->join_type_01}($sMobileSchema->join_table_01, $sMobileSchema->join_first_01, $sMobileSchema->join_operator_01, $sMobileSchema->join_second_01);
            })
            ->when(!empty($sMobileSchema->join_type_02), function ($query) use ($sMobileSchema) {
                return $query->{$sMobileSchema->join_type_02}($sMobileSchema->join_table_02, $sMobileSchema->join_first_02, $sMobileSchema->join_operator_02, $sMobileSchema->join_second_02);
            })
            ->when(!empty($sMobileSchema->join_type_03), function ($query) use ($sMobileSchema) {
                return $query->{$sMobileSchema->join_type_03}($sMobileSchema->join_table_03, $sMobileSchema->join_first_03, $sMobileSchema->join_operator_03, $sMobileSchema->join_second_03);
            })
            ->when(!empty($sMobileSchema->join_type_04), function ($query) use ($sMobileSchema) {
                return $query->{$sMobileSchema->join_type_04}($sMobileSchema->join_table_04, $sMobileSchema->join_first_04, $sMobileSchema->join_operator_04, $sMobileSchema->join_second_04);
            })
            ->when(!empty($sMobileSchema->join_type_05), function ($query) use ($sMobileSchema) {
                return $query->{$sMobileSchema->join_type_05}($sMobileSchema->join_table_05, $sMobileSchema->join_first_05, $sMobileSchema->join_operator_05, $sMobileSchema->join_second_05);
            })
            ->when(!empty($sMobileSchema->where), function ($query) use ($sMobileSchema) {
                return $query->whereRaw($sMobileSchema->where);
            });

            //union the leftJoin and rightJoin, so can know the update/insert/delete
            //START: LeftJoin query
            $first = DB::table('mobile_histories')
            ->selectRaw($sMobileSchema->table_name.'.*')
            ->selectRaw('IFNULL(mobile_histories.record_id, 0) AS record_id')
            ->selectRaw('IFNULL(mobile_histories.created_at, "0000-00-00 00:00:00") AS first_synced_at')
            ->selectRaw('IFNULL(mobile_histories.updated_at, "0000-00-00 00:00:00") AS last_synced_at')
            ->leftJoinSub($tableDatas, $sMobileSchema->table_name, function ($join) use ($mobileProfile, $sMobileSchema) {
                $join->on('mobile_histories.record_id', '=', $sMobileSchema->table_name.'.id');
            })
            ->where('mobile_histories.mobile_profile_id', $mobileProfile->id)
            ->where('mobile_histories.table_name', $sMobileSchema->table_name)
            ->whereNull($sMobileSchema->table_name.'.updated_at')
            ->lockForUpdate();
            //END: LeftJoin query

            //START: RightJoin query
            $query = DB::table('mobile_histories')
            ->selectRaw($sMobileSchema->table_name.'.*')
            ->selectRaw('IFNULL(mobile_histories.record_id, 0) AS record_id')
            ->selectRaw('IFNULL(mobile_histories.created_at, "0000-00-00 00:00:00") AS first_synced_at')
            ->selectRaw('IFNULL(mobile_histories.updated_at, "0000-00-00 00:00:00") AS last_synced_at')
            ->rightJoinSub($tableDatas, $sMobileSchema->table_name, function ($join) use ($mobileProfile, $sMobileSchema) {
                $join->on('mobile_histories.record_id', '=', $sMobileSchema->table_name.'.id')
                ->where('mobile_histories.mobile_profile_id', $mobileProfile->id)
                ->where('mobile_histories.table_name', $sMobileSchema->table_name);
            })
            ->where(function ($query) use ($sMobileSchema) {
                $query->whereRaw($sMobileSchema->table_name.'.updated_at > mobile_histories.updated_at')
                    ->orWhereNull('mobile_histories.updated_at');
            })
            ->lockForUpdate()
            ->union($first);
            //END: RightJoin query
            $records = $query->paginate($pageSize);
            DB::commit();
            //dd(DB::getQueryLog());
        }
        catch (Exception $e) 
        {
            DB::rollback();
            $exc = new ApiException(__('Mobile.process_history_error', ['msg'=>$e->getMessage()]));
			$exc->addData(\App\MobileProfile::class, $mobileProfileId);
			throw $exc;
        }
        return $records;
    }

    public function indexHistory($mobileProfileId, $tableName, $pageSize = 10000) 
	{
        AuthService::authorize(
            array(
                'mobile_history'
            )
        );

        $mobileProfile = MobileProfileRepository::findByPk($mobileProfileId);
        if(empty($mobileProfile))
		{
			$exc = new ApiException(__('Mobile.mobile_profile_not_found', ['mobileProfileId'=>$mobileProfileId]));
			$exc->addData(\App\MobileProfile::class, $mobileProfileId);
			throw $exc;
        }

        $sMobileSchema = SMobileSchemaRepository::findByMobileAppAndTableName($mobileProfile->mobile_app, $tableName);
        if(empty($sMobileSchema))
		{
			$exc = new ApiException(__('Mobile.mobile_schema_not_found', ['mobileApp'=>$mobileProfile->mobile_app, 'tableName'=>$tableName]));
			throw $exc;
        }

        $mobileHistories = MobileHistoryRepository::findAllByMobileProfileIdAndTableName($mobileProfileId, $tableName);
        return $mobileHistories;
    }

    public function updateHistory($mobileProfileId, $tableName, $dtlArray) 
	{
        AuthService::authorize(
            array(
                'mobile_history_update'
            )
        );

        //data is associative array of recordId/operation pair
        $mobileProfile = MobileProfileRepository::findByPk($mobileProfileId);
        if(empty($mobileProfile))
		{
			$exc = new ApiException(__('Mobile.mobile_profile_not_found', ['mobileProfileId'=>$mobileProfileId]));
			$exc->addData(\App\MobileProfile::class, $mobileProfileId);
			throw $exc;
        }

        $sMobileSchema = SMobileSchemaRepository::findByMobileAppAndTableName($mobileProfile->mobile_app, $tableName);
        if(empty($sMobileSchema))
		{
			$exc = new ApiException(__('Mobile.mobile_schema_not_found', ['mobileApp'=>$mobileProfile->mobile_app, 'tableName'=>$tableName]));
			throw $exc;
        }

        $affectedRows = MobileHistoryRepository::updateHistory($mobileProfileId, $tableName, $dtlArray);
        return array(
            'affected_rows' => $affectedRows
        );
    }

    public function resetHistory($mobileProfileId, $tableName) 
	{
        AuthService::authorize(
            array(
                'mobile_history_reset'
            )
        );

        //data is associative array of recordId/operation pair
        $mobileProfile = MobileProfileRepository::findByPk($mobileProfileId);
        if(empty($mobileProfile))
		{
			$exc = new ApiException(__('Mobile.mobile_profile_not_found', ['mobileProfileId'=>$mobileProfileId]));
			$exc->addData(\App\MobileProfile::class, $mobileProfileId);
			throw $exc;
        }

        $sMobileSchema = SMobileSchemaRepository::findByMobileAppAndTableName($mobileProfile->mobile_app, $tableName);
        if(empty($sMobileSchema))
		{
			$exc = new ApiException(__('Mobile.mobile_schema_not_found', ['mobileApp'=>$mobileProfile->mobile_app, 'tableName'=>$tableName]));
			throw $exc;
        }

        $affectedRows = MobileHistoryRepository::resetHistory($mobileProfileId, $tableName);
        return array(
            'affected_rows' => $affectedRows
        );
    }

    public function resetAllHistory($mobileProfileId) 
	{
        AuthService::authorize(
            array(
                'mobile_history_reset'
            )
        );
        
        //data is associative array of recordId/operation pair
        $mobileProfile = MobileProfileRepository::findByPk($mobileProfileId);
        if(empty($mobileProfile))
		{
			$exc = new ApiException(__('Mobile.mobile_profile_not_found', ['mobileProfileId'=>$mobileProfileId]));
			$exc->addData(\App\MobileProfile::class, $mobileProfileId);
			throw $exc;
        }

        $affectedRows = MobileHistoryRepository::resetAllHistory($mobileProfileId);
        return array(
            'affected_rows' => $affectedRows
        );
    }
}