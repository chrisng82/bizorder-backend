<?php

namespace App\Services;

use App\Uom;
use App\BinTrfHdr;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Services\Env\ResStatus;
use App\Services\Env\WhseJobType;
use App\Repositories\BinTrfHdrRepository;
use App\Repositories\BinTrfDtlRepository;
use App\Repositories\WhseJobHdrRepository;
use App\Repositories\SiteFlowRepository;
use App\Repositories\SiteDocNoRepository;
use App\Repositories\WhseTxnFlowRepository;
use App\Repositories\DocTxnFlowRepository;
use App\Repositories\QuantBalRepository;
use App\Repositories\PrintDocSettingRepository;
use App\Repositories\PrintDocTxnRepository;
use App\Repositories\DocNoRepository;
use App\Repositories\ItemRepository;
use App\Repositories\ItemUomRepository;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BinTrfService extends InventoryService
{
    public function __construct() 
	{
    }

    public function createProcess($strProcType, $siteFlowId, $ids, $data)
    {
        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['BIN_TRF_02']) 
            {
                //BIN_TRF, UNIVERSAL BIN TRANSFER
                $result = $this->createBinTrf02(ProcType::$MAP[$strProcType], $siteFlowId, $data);
                return $result;
            }
		}
    }

	/*
    protected function createBinTrf02($procType, $siteFlowId, $data)
    {        
        $siteFlow = SiteFlowRepository::findByPk($siteFlowId);
        
        //build the data
        $hdrData = array(
            'site_flow_id' => $siteFlow->id,
            'ref_code_01' => '',
            'ref_code_02' => '',
            'doc_date' => date('Y-m-d'),
            'desc_01' => '',
            'desc_02' => '',
        );

        $docSiteDocNo = SiteDocNoRepository::findSiteDocNo($siteFlow->site_id, \App\BinTrfHdr::class);
        if(empty($docSiteDocNo))
        {
            $exc = new ApiException(__('SiteDocNo.doc_no_not_found', ['siteId'=>$siteFlow->site_id, 'docType'=>\App\BinTrfHdr::class]));
            //$exc->addData(\App\BinTrfHdr::class, $siteFlow->site_id);
            throw $exc;
        }

        $whseTxnFlow = WhseTxnFlowRepository::findByPk($siteFlow->id, $procType);

        //DRAFT
        $binTrfHdr = BinTrfHdrRepository::createProcess($procType, $docSiteDocNo->doc_no_id, $hdrData, array());

        $binTrfHdr = self::transitionToWip($binTrfHdr->id);

        //create whseJob            
        $hdrData = array(
            'ref_code_01' => '',
            'ref_code_02' => '',
            'doc_date' => date('Y-m-d'),
            'desc_01' => '',
            'desc_02' => '',
            'site_flow_id' => $siteFlow->id,
            'mobile_profile_id' => 0,
            'worker_01_id' => 0,
            'worker_02_id' => 0,
            'worker_03_id' => 0,
            'worker_04_id' => 0,
            'worker_05_id' => 0,
            'equipment_01_id' => 0,
            'equipment_02_id' => 0,
            'equipment_03_id' => 0,
            'equipment_04_id' => 0,
            'equipment_05_id' => 0
        );

        $tmpDocTxnFlowData = array(
            'fr_doc_hdr_type' => \App\BinTrfHdr::class,
            'fr_doc_hdr_id' => $binTrfHdr->id,
            'fr_doc_hdr_code' => $binTrfHdr->doc_code,
            'is_closed' => 1
        );
        $docTxnFlowDataList = array($tmpDocTxnFlowData);

        $whseSiteDocNo = SiteDocNoRepository::findSiteDocNo($siteFlow->site_id, \App\WhseJobHdr::class);
        if(empty($whseSiteDocNo))
        {
            $exc = new ApiException(__('SiteDocNo.doc_no_not_found', ['siteId'=>$siteFlow->site_id, 'docType'=>\App\WhseJobHdr::class]));
            //$exc->addData(\App\SiteDocNo::class, $siteFlow->site_id);
            throw $exc;
        }

        $whseJobHdr = WhseJobHdrRepository::createProcess($procType, $whseSiteDocNo->doc_no_id, $hdrData, array(), $docTxnFlowDataList);

        $message = __('BinTrf.document_successfully_created', ['docCode'=>$binTrfHdr->doc_code]);

		return array(
			'data' => array($binTrfHdr),
			'message' => $message
		);
	}
	*/

    public function indexProcess($strProcType, $siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['BIN_TRF_02']) 
			{
				//show the pending mobile bin transfer 
				$binTrfHdrs = $this->indexBinTrf02($siteFlowId, $sorts, $filters, $pageSize);
				return $binTrfHdrs;
			}
		}
	}
    
    protected function indexBinTrf02($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
            array(
                'bin_trf_read'
            )
		);
		
		//DB::connection()->enableQueryLog();
        $qStatuses = array(DocStatus::$MAP['DRAFT'], DocStatus::$MAP['WIP']);
        $binTrfHdrs = BinTrfHdrRepository::findAllBinTrf02($siteFlowId, $qStatuses, $sorts, $filters, $pageSize);
        $binTrfHdrs->load('siteFlow', 'toDocTxnFlows', 'toDocTxnFlows.toDocHdr');
		foreach($binTrfHdrs as $binTrfHdr)
		{
            $binTrfHdr = self::processOutgoingHeader($binTrfHdr);

            //calculate the pallet_qty, case_qty, gross_weight and cubic_meter
            $caseQty = 0;
			$grossWeight = 0;
            $cubicMeter = 0;
            
			$binTrfHdr->case_qty = $caseQty;
			$binTrfHdr->gross_weight = $grossWeight;
			$binTrfHdr->cubic_meter = $cubicMeter;
		}
		//Log::error(DB::getQueryLog());
        return $binTrfHdrs;
    }

    public function transitionToStatus($hdrId, $strDocStatus)
    {
        $hdrModel = null;
        if(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['WIP'])
        {
            $hdrModel = self::transitionToWip($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['COMPLETE'])
        {
            $hdrModel = self::transitionToComplete($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['DRAFT'])
        {
            $hdrModel = self::transitionToDraft($hdrId);
		}
		elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['VOID'])
        {
            $hdrModel = self::transitionToVoid($hdrId);
        }

		$documentHeader = self::processOutgoingHeader($hdrModel);
        $message = __('BinTrf.document_successfully_transition', ['docCode'=>$documentHeader->doc_code,'strDocStatus'=>$strDocStatus]);
        
        return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>array(),
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }

    static public function transitionToWip($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = BinTrfHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status >= DocStatus::$MAP['WIP'])
		{
			$exc = new ApiException(__('BinTrf.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\BinTrfHdr::class, $hdrModel->id);
            throw $exc;
		}

		//commit the document
		$hdrModel = BinTrfHdrRepository::commitToWip($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('BinTrf.commit_to_wip_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\BinTrfHdr::class, $hdrModel->id);
        throw $exc;
	}

	static public function transitionToComplete($hdrId)
    {
		AuthService::authorize(
            array(
                'bin_trf_confirm'
            )
		);

		$isSuccess = false;
		//use transaction to make sure this is latest value
		$hdrModel = BinTrfHdrRepository::txnFindByPk($hdrId);

		//only DRAFT or above can transition to COMPLETE
		if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE']
		|| $hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('BinTrf.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\BinTrfHdr::class, $hdrModel->id);
            throw $exc;
		}

		$dtlModels = BinTrfDtlRepository::findAllByHdrId($hdrModel->id);

		//commit the document
		$hdrModel = BinTrfHdrRepository::commitToComplete($hdrModel->id, true);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
        }
        
		$exc = new ApiException(__('BinTrf.commit_to_complete_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\BinTrfHdr::class, $hdrModel->id);
        throw $exc;
    }
    
    static public function transitionToDraft($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = BinTrfHdrRepository::txnFindByPk($hdrId);

		//only WIP or COMPLETE can transition to DRAFT
		if($hdrModel->doc_status == DocStatus::$MAP['WIP'])
		{
            $hdrModel = BinTrfHdrRepository::revertWipToDraft($hdrModel->id);
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		elseif($hdrModel->doc_status == DocStatus::$MAP['COMPLETE'])
		{
			AuthService::authorize(
                array(
                    'bin_trf_revert'
                )
			);
			
            $hdrModel = BinTrfHdrRepository::revertCompleteToDraft($hdrModel->id);
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
        }
        
        $exc = new ApiException(__('BinTrf.doc_status_is_not_wip_or_complete', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\BinTrfHdr::class, $hdrModel->id);
		throw $exc;
    }

    static public function processIncomingDetail($data)
    {
        //preprocess the select2 and dropDown
        if(array_key_exists('item_select2', $data))
        {
			$select2Id = $data['item_select2']['value'];
			if($select2Id > 0)
			{
				$data['item_id'] = $select2Id;
			}
            unset($data['item_select2']);
		}
		$item = ItemRepository::findByPk($data['item_id']);
		if(empty($item))
		{
			$exc = new ApiException(__('BinTrf.item_is_required', ['lineNo'=>$data['line_no']]));
			//$exc->addData(\App\WhseJobDtl::class, $data['id']);
			throw $exc;
		}

		if(array_key_exists('qty', $data)
		&& !empty($item))
		{
			$data['qty'] = round($data['qty'], $item->qty_scale);
		}
        if(array_key_exists('storage_bin_select2', $data))
        {
			$select2Id = $data['storage_bin_select2']['value'];
			if($select2Id > 0)
			{
				$data['storage_bin_id'] = $select2Id;
			}
            unset($data['storage_bin_select2']);
        }
        if(array_key_exists('quant_bal_select2', $data))
        {
			$select2Id = $data['quant_bal_select2']['value'];
			if($select2Id > 0)
			{
				$data['quant_bal_id'] = $select2Id;
			}
            unset($data['quant_bal_select2']);
        }
        if(array_key_exists('uom_select2', $data))
        {
			$select2Id = $data['uom_select2']['value'];
			if($select2Id > 0)
			{
				$data['uom_id'] = $select2Id;
			}
            unset($data['uom_select2']);
        }
        if(array_key_exists('to_storage_bin_select2', $data))
        {
			$select2Id = $data['to_storage_bin_select2']['value'];
			if($select2Id > 0)
			{
				$data['to_storage_bin_id'] = $select2Id;
			}
            unset($data['to_storage_bin_select2']);
		}
		if(array_key_exists('to_handling_unit_select2', $data))
        {
			$select2Id = $data['to_handling_unit_select2']['value'];
			if($select2Id > 0)
			{
				$data['to_handling_unit_id'] = $select2Id;
			}
            unset($data['to_handling_unit_select2']);
        }

		if(array_key_exists('storage_bin_code', $data))
        {
            unset($data['storage_bin_code']);
		}
		if(array_key_exists('storage_row_code', $data))
        {
            unset($data['storage_row_code']);
		}
		if(array_key_exists('storage_bay_code', $data))
        {
            unset($data['storage_bay_code']);
		}
		if(array_key_exists('item_bay_sequence', $data))
        {
            unset($data['item_bay_sequence']);
		}

		if(array_key_exists('to_storage_bin_code', $data))
        {
            unset($data['to_storage_bin_code']);
		}
		if(array_key_exists('to_storage_row_code', $data))
        {
            unset($data['to_storage_row_code']);
		}
		if(array_key_exists('to_storage_bay_code', $data))
        {
            unset($data['to_storage_bay_code']);
		}
		if(array_key_exists('to_item_bay_sequence', $data))
        {
            unset($data['to_item_bay_sequence']);
		}

		if(array_key_exists('str_whse_job_type', $data))
        {
            unset($data['str_whse_job_type']);
		}

        if(array_key_exists('handling_unit_barcode', $data))
        {
            unset($data['handling_unit_barcode']);
		}
		if(array_key_exists('handling_unit_ref_code_01', $data))
        {
            unset($data['handling_unit_ref_code_01']);
		}
		/*
        if(array_key_exists('batch_serial_no', $data))
        {
            unset($data['batch_serial_no']);
        }
        if(array_key_exists('expiry_date', $data))
        {
            unset($data['expiry_date']);
        }
        if(array_key_exists('receipt_date', $data))
        {
            unset($data['receipt_date']);
		}
		*/
        if(array_key_exists('item_code', $data))
        {
            unset($data['item_code']);
		}
		if(array_key_exists('item_ref_code_01', $data))
        {
            unset($data['item_ref_code_01']);
		}
		if(array_key_exists('item_desc_01', $data))
        {
            unset($data['item_desc_01']);
		}
		if(array_key_exists('item_desc_02', $data))
        {
            unset($data['item_desc_02']);
		}
		if(array_key_exists('storage_class', $data))
        {
            unset($data['storage_class']);
		}
		if(array_key_exists('item_group_01_code', $data))
        {
            unset($data['item_group_01_code']);
		}
		if(array_key_exists('item_group_02_code', $data))
        {
            unset($data['item_group_02_code']);
		}
		if(array_key_exists('item_group_03_code', $data))
        {
            unset($data['item_group_03_code']);
		}
		if(array_key_exists('item_group_04_code', $data))
        {
            unset($data['item_group_04_code']);
		}
		if(array_key_exists('item_group_05_code', $data))
        {
            unset($data['item_group_05_code']);
		}
		if(array_key_exists('item_cases_per_pallet_length', $data))
        {
            unset($data['item_cases_per_pallet_length']);
		}
		if(array_key_exists('item_cases_per_pallet_width', $data))
        {
            unset($data['item_cases_per_pallet_width']);
		}
		if(array_key_exists('item_no_of_layers', $data))
        {
            unset($data['item_no_of_layers']);
		}

		if(array_key_exists('unit_qty', $data))
        {
            unset($data['unit_qty']);
		}
		if(array_key_exists('item_unit_uom_code', $data))
        {
            unset($data['item_unit_uom_code']);
		}
		if(array_key_exists('loose_qty', $data))
        {
            unset($data['loose_qty']);
		}
		if(array_key_exists('item_loose_uom_code', $data))
        {
            unset($data['item_loose_uom_code']);
		}
		if(array_key_exists('loose_uom_rate', $data))
        {
            unset($data['loose_uom_rate']);
		}
		if(array_key_exists('case_qty', $data))
        {
            unset($data['case_qty']);
		}
		if(array_key_exists('case_uom_rate', $data))
        {
            unset($data['case_uom_rate']);
		}
        if(array_key_exists('item_case_uom_code', $data))
        {
            unset($data['item_case_uom_code']);
        }
        if(array_key_exists('uom_code', $data))
        {
            unset($data['uom_code']);
		}
		if(array_key_exists('item_unit_barcode', $data))
        {
            unset($data['item_unit_barcode']);
		}
		if(array_key_exists('item_case_barcode', $data))
        {
            unset($data['item_case_barcode']);
		}
		if(array_key_exists('gross_weight', $data))
        {
            unset($data['gross_weight']);
		}
		if(array_key_exists('cubic_meter', $data))
        {
            unset($data['cubic_meter']);
        }
        if(array_key_exists('item', $data))
        {
            unset($data['item']);
        }
        if(array_key_exists('uom', $data))
        {
            unset($data['uom']);
        }
        return $data;
    }
    
    static public function processOutgoingDetail($model)
  	{
		//relationships:
		$item = $model->item;
		$storageBin = $model->storageBin;
		$handlingUnit = $model->handlingUnit;
		$quantBal = $model->quantBal;
		$uom = $model->uom;
		$toStorageBin = $model->toStorageBin;
		$toHandlingUnit = $model->toHandlingUnit;
		
		$model->str_whse_job_type = WhseJobType::$MAP[$model->whse_job_type];
		$model->str_whse_job_type = __('WhseJob.'.strtolower($model->str_whse_job_type));

		$model->storage_bin_code = '';
		$model->storage_row_code = '';
		$model->storage_bay_code = '';
		$model->handling_unit_barcode = '';

		$model->batch_serial_no = '';
		$model->expiry_date = '';
		$model->receipt_date = '';

		$model->to_storage_bin_code = '';
		$model->to_storage_row_code = '';
		$model->to_storage_bay_code = '';

		//this whseJobDtl is delivery
		$itemBatch = null;
		$storageRow = null;
		if(!empty($storageBin))
		{
			$storageRow = $storageBin->storageRow;
			$model->storage_bin_code = $storageBin->code;
		}
		$storageBay = null;
		if(!empty($quantBal))
		{
			$handlingUnit = $quantBal->handlingUnit;
			$itemBatch = $quantBal->itemBatch;
			$storageBin = $quantBal->storageBin;
			$storageRow = $quantBal->storageRow;
			$storageBay = $quantBal->storageBay;
		}
		if(!empty($handlingUnit))
		{
			$model->handling_unit_barcode = $handlingUnit->getBarcode();
		}
		if(!empty($storageRow))
		{
			$model->storage_row_code = $storageRow->code;
		}
		$model->item_bay_sequence = 0;
		if(!empty($storageBay))
		{
			$model->storage_bay_code = $storageBay->code;
			$model->item_bay_sequence = $storageBay->bay_sequence;
		}
		if(!empty($itemBatch))
		{
			$model->batch_serial_no = $itemBatch->batch_serial_no;
			$model->expiry_date = $itemBatch->expiry_date;
			$model->receipt_date = $itemBatch->receipt_date;
		}

		if(!empty($toStorageBin))
		{
			$toStorageRow = $toStorageBin->storageRow;
			$toStorageBay = $toStorageBin->storageBay;

			$model->to_storage_bin_code = $toStorageBin->code;
			if(!empty($toStorageRow))
			{
				$model->to_storage_row_code = $toStorageRow->code;
			}
			$model->to_item_bay_sequence = 0;
			if(!empty($toStorageBay))
			{
				$model->to_storage_bay_code = $toStorageBay->code;
				$model->to_item_bay_sequence = $toStorageBay->bay_sequence;
			}
		}

		//calculate the pallet qty, case qty, gross weight, and m3
		if($model->uom_id == Uom::$PALLET
		&& bccomp($model->uom_rate, 0, 5) == 0)
		{
			//this is pallet picking
			$model->unit_qty = 0;
			$model->case_qty = 0;
			$model->gross_weight = 0;
			$model->cubic_meter = 0;

			$handlingQuantBals = QuantBalRepository::findAllByHandlingUnitIdAndStorageBinId($quantBal->handling_unit_id, $model->storage_bin_id);
			$handlingQuantBals->load('item');
			foreach($handlingQuantBals as $handlingQuantBal)
			{
				$handlingItem = $handlingQuantBal->item;

				$handlingQuantBal = ItemService::processCaseLoose($handlingQuantBal, $handlingItem, 1);

				$model->item_code = $handlingQuantBal->item_code;
				$model->item_ref_code_01 = $handlingQuantBal->item_ref_code_01;
				$model->item_desc_01 = $handlingQuantBal->item_desc_01;
				$model->item_desc_02 = $handlingQuantBal->item_desc_02;
				$model->storage_class = $handlingQuantBal->storage_class;
				$model->item_group_01_code = $handlingQuantBal->item_group_01_code;
				$model->item_group_02_code = $handlingQuantBal->item_group_02_code;
				$model->item_group_03_code = $handlingQuantBal->item_group_03_code;
				$model->item_group_04_code = $handlingQuantBal->item_group_04_code;
				$model->item_group_05_code = $handlingQuantBal->item_group_05_code;
				$model->item_cases_per_pallet_length = $handlingQuantBal->item_cases_per_pallet_length;
				$model->item_cases_per_pallet_width = $handlingQuantBal->item_cases_per_pallet_width;
				$model->item_no_of_layers = $handlingQuantBal->item_no_of_layers;

				$model->unit_qty = bcadd($model->unit_qty, $handlingQuantBal->balance_unit_qty, 10);
				$model->case_qty = bcadd($model->case_qty, $handlingQuantBal->case_qty, 10);
				$model->gross_weight = bcadd($model->gross_weight, $handlingQuantBal->gross_weight, 10);
				$model->cubic_meter = bcadd($model->cubic_meter, $handlingQuantBal->cubic_meter, 10);
			}
		}
		else
		{
			$item = $model->item;
			$model = ItemService::processCaseLoose($model, $item);
		}

        //storageBin select2
        $initStorageBinOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($storageBin))
        {
            $initStorageBinOption = array('value'=>$storageBin->id,'label'=>$storageBin->code);
        }        
		$model->storage_bin_select2 = $initStorageBinOption;

		$initQuantBalOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($quantBal))
        {
			$label = $model->handling_unit_barcode;
            $initQuantBalOption = array(
				'value'=>$quantBal->id,
				'label'=>$label
			);
        }        
        $model->quant_bal_select2 = $initQuantBalOption;
		
		$initToStorageBinOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($toStorageBin))
        {
            $initToStorageBinOption = array('value'=>$toStorageBin->id,'label'=>$toStorageBin->code);
        }        
		$model->to_storage_bin_select2 = $initToStorageBinOption;
		
		$initToHandlingUnitOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($toHandlingUnit))
        {
            $initToHandlingUnitOption = array('value'=>$toHandlingUnit->id,'label'=>$toHandlingUnit->getBarcode());
        }        
		$model->to_handling_unit_select2 = $initToHandlingUnitOption;
		
		$initUomOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($uom))
        {
            $initUomOption = array('value'=>$uom->id,'label'=>$uom->code);
        }        
		$model->uom_select2 = $initUomOption;
		
		unset($model->item);
		unset($model->storageBin);
		unset($model->handlingUnit);
		unset($model->quantBal);
		unset($model->uom);
		unset($model->toStorageBin);
		unset($model->toHandlingUnit);

		return $model;
	}

	public function printProcess($strProcType, $siteFlowId, $hdrIds)
  	{
		$user = Auth::user();

		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['BIN_TRF_02']) 
			{
				//print BIN_TRF_02 whse job
				return $this->printBinTrf02($siteFlowId, $user, $hdrIds);
			}
		}
	}

	public function printBinTrf02($siteFlowId, $user, $hdrIds)
	{
		AuthService::authorize(
			array(
				'bin_trf_print'
			)
		);

		$docHdrType = \App\BinTrfHdr::class.'02';
		$printDocSetting = PrintDocSettingRepository::findByDocHdrTypeAndSiteFlowId($docHdrType, $siteFlowId);
		if(empty($printDocSetting))
		{
			$exc = new ApiException(__('PrintDoc.report_template_not_found', ['docType'=>\App\BinTrfHdr::class.'02']));
			$exc->addData(\App\PrintDocSetting::class, \App\BinTrfHdr::class);
			throw $exc;
		}

		$binTrfHdrs = BinTrfHdrRepository::findAllByIds($hdrIds);
		$printDocTxnDataArray = array();
		foreach($binTrfHdrs as $binTrfHdr)
		{
			$binTrfHdr->view_name = $printDocSetting->view_name;

			$printDocTxnData = array();
			$printDocTxnData['doc_hdr_type'] = \App\BinTrfHdr::class;
			$printDocTxnData['doc_hdr_id'] = $binTrfHdr->id;
			$printDocTxnData['user_id'] = $user->id;
			$printDocTxnDataArray[] = $printDocTxnData;
		}
		
		foreach($binTrfHdrs as $binTrfHdr)
		{
			$binTrfHdr = self::processOutgoingHeader($binTrfHdr, false);

			$binTrfHdr->details = array();
		}

		$printedAt = PrintDocTxnRepository::createProcess($printDocTxnDataArray);

		$pdf = \Illuminate\Support\Facades\App::make('dompdf.wrapper');
		$pdf->getDomPDF()->set_option("enable_php", true);
		$pdf->setPaper(array(
			0,
			0,
			$printDocSetting->page_width, 
			$printDocSetting->page_height
		), $printDocSetting->page_orientation);

		//each data must have the view_name
		$pdf->loadHTML(
			view('batchPrintDocuments',
				array(
					'title' => 'Bin Transfer Whse Job',
					'data' => $binTrfHdrs, 
					'printedAt' => $printedAt,
					'printedBy' => $user,
					'isPageBreakAfter' => true,
				)
			)
		);

		return $pdf->stream();
	}

	static public function processOutgoingHeader($model, $isShowPrint = false)
	{
		$docFlows = self::processDocFlows($model);
		$model->doc_flows = $docFlows;
		
		if($isShowPrint)
		{
			$printDocTxn = PrintDocTxnRepository::queryPrintDocTxn(BinTrfHdr::class, $model->id);
			$model->print_count = $printDocTxn->print_count;
			$model->first_printed_at = $printDocTxn->first_printed_at;
			$model->last_printed_at = $printDocTxn->last_printed_at;
		}

		$model->str_doc_status = DocStatus::$MAP[$model->doc_status];

		$model->whse_job_hdr_id = 0;
		$model->whse_job_doc_code = '';
		$toDocTxnFlows = $model->toDocTxnFlows;
		foreach($toDocTxnFlows as $toDocTxnFlow)
		{
			$model->whse_job_hdr_id = $toDocTxnFlow->to_doc_hdr_id;
			$toDocHdr = $toDocTxnFlow->toDocHdr;
			if(!empty($toDocHdr))
			{
				$model->whse_job_doc_code = $toDocHdr->doc_code;
			}
		}
		
		return $model;
	}

	public function showHeader($hdrId)
    {
		AuthService::authorize(
			array(
				'bin_trf_read',
				'bin_trf_update'
			)
		);

        $model = BinTrfHdrRepository::findByPk($hdrId);
		if(!empty($model))
        {
            $model = self::processOutgoingHeader($model);
        }
        
        return $model;
	}
	
	public function showDetails($hdrId) 
	{
		AuthService::authorize(
			array(
				'bin_trf_read',
				'bin_trf_update'
			)
		);

		$binTrfDtls = BinTrfDtlRepository::findAllByHdrId($hdrId);
		$binTrfDtls->load(
            'item', 'item.itemUoms', 'item.itemUoms.uom'
        );
        foreach($binTrfDtls as $binTrfDtl)
        {
			$binTrfDtl = self::processOutgoingDetail($binTrfDtl);
        }
        return $binTrfDtls;
	}

	public function createHeader($data)
    {		
		AuthService::authorize(
			array(
				'bin_trf_create'
			)
		);

        $data = self::processIncomingHeader($data, true);
        $docNoId = $data['doc_no_id'];
        unset($data['doc_no_id']);
        $hdrModel = BinTrfHdrRepository::createHeader(ProcType::$MAP['BIN_TRF_02'], $docNoId, $data);

        $message = __('BinTrf.document_successfully_created', ['docCode'=>$hdrModel->doc_code]);

		return array(
			'data' => $hdrModel->id,
			'message' => $message
		);
	}
	
	public function updateDetails($hdrId, $dtlArray)
	{
		AuthService::authorize(
			array(
				'bin_trf_update'
			)
		);

		$binTrfHdr = BinTrfHdrRepository::txnFindByPk($hdrId);
        if($binTrfHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('BinTrf.doc_status_is_wip_or_complete', ['docCode'=>$binTrfHdr->doc_code]));
            $exc->addData(\App\BinTrfHdr::class, $binTrfHdr->id);
            throw $exc;
        }
		$binTrfHdrData = $binTrfHdr->toArray();

		//query the binTrfDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$binTrfDtlArray = array();
        $binTrfDtlModels = BinTrfDtlRepository::findAllByHdrId($hdrId);
		foreach($binTrfDtlModels as $binTrfDtlModel)
		{
			$binTrfDtlData = $binTrfDtlModel->toArray();
			$binTrfDtlData['is_modified'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{                
				if($dtlData['id'] == $binTrfDtlData['id'])
				{
					$dtlData = self::processIncomingDetail($dtlData);

					foreach($dtlData as $fieldName => $value)
					{
						$binTrfDtlData[$fieldName] = $value;
                    }

					$binTrfDtlData['is_modified'] = 1;

					break;
				}
			}
			$binTrfDtlArray[] = $binTrfDtlData;
		}
		
		$result = BinTrfHdrRepository::updateDetails($binTrfHdrData, $binTrfDtlArray, array(), array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = self::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('BinTrf.details_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}
	
	public function createDetail($hdrId, $data)
	{
		AuthService::authorize(
			array(
				'bin_trf_update'
			)
		);

        $data = self::processIncomingDetail($data);
		//$data = $this->updateWarehouseItemUom($data, 0, 0);

        $binTrfHdr = BinTrfHdrRepository::txnFindByPk($hdrId);
        if($binTrfHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('BinTrf.doc_status_is_wip_or_complete', ['docCode'=>$binTrfHdr->doc_code]));
            $exc->addData(\App\BinTrfHdr::class, $binTrfHdr->id);
            throw $exc;
        }

		$binTrfHdrData = $binTrfHdr->toArray();

		$binTrfDtlArray = array();
		$binTrfDtlModels = BinTrfDtlRepository::findAllByHdrId($hdrId);
		foreach($binTrfDtlModels as $binTrfDtlModel)
		{
			$binTrfDtlData = $binTrfDtlModel->toArray();
			$binTrfDtlData['is_modified'] = 0;
			$binTrfDtlArray[] = $binTrfDtlData;
		}
		$lastLineNo = count($binTrfDtlModels);

		//assign temp id
		$tmpUuid = uniqid();
		//append NEW here to make sure it will be insert in repository later
		$data['id'] = 'NEW'.$tmpUuid;
		$data['line_no'] = $lastLineNo + 1;
		$data['is_modified'] = 1;
		
        $binTrfDtlData = $this->initBinTrfDtlData($data);

		$binTrfDtlArray[] = $binTrfDtlData;

		$result = BinTrfHdrRepository::updateDetails($binTrfHdrData, $binTrfDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('BinTrf.detail_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function deleteDetails($hdrId, $dtlArray)
	{
		AuthService::authorize(
			array(
				'bin_trf_update'
			)
		);

        $binTrfHdr = BinTrfHdrRepository::txnFindByPk($hdrId);
        if($binTrfHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('BinTrf.doc_status_is_wip_or_complete', ['docCode'=>$binTrfHdr->doc_code]));
            $exc->addData(\App\BinTrfHdr::class, $binTrfHdr->id);
            throw $exc;
        }
		$binTrfHdrData = $binTrfHdr->toArray();

		//query the binTrfDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$delBinTrfDtlArray = array();
		$binTrfDtlArray = array();
        $binTrfDtlModels = BinTrfDtlRepository::findAllByHdrId($hdrId);
        $lineNo = 1;
		foreach($binTrfDtlModels as $binTrfDtlModel)
		{
			$binTrfDtlData = $binTrfDtlModel->toArray();
			$binTrfDtlData['is_modified'] = 0;
			$binTrfDtlData['is_deleted'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{
				if($dtlData['id'] == $binTrfDtlData['id'])
				{
					//this is deleted dtl
					$binTrfDtlData['is_deleted'] = 1;
					break;
				}
			}
			if($binTrfDtlData['is_deleted'] > 0)
			{
				$delBinTrfDtlArray[] = $binTrfDtlData;
			}
			else
			{
                $binTrfDtlData['line_no'] = $lineNo;
                $binTrfDtlData['is_modified'] = 1;
                $binTrfDtlArray[] = $binTrfDtlData;
                $lineNo++;
			}
        }
		
		$result = BinTrfHdrRepository::updateDetails($binTrfHdrData, $binTrfDtlArray, $delBinTrfDtlArray);
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = self::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('BinTrf.details_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>$delBinTrfDtlArray
            ),
			'message' => $message
		);
	}

	public function updateHeader($hdrData)
	{
		AuthService::authorize(
			array(
				'bin_trf_update'
			)
		);

        $hdrData = self::processIncomingHeader($hdrData);

        $binTrfHdr = BinTrfHdrRepository::txnFindByPk($hdrData['id']);
        if($binTrfHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('BinTrf.doc_status_is_wip_or_complete', ['docCode'=>$binTrfHdr->doc_code]));
            $exc->addData(\App\BinTrfHdr::class, $binTrfHdr->id);
            throw $exc;
        }
        $binTrfHdrData = $binTrfHdr->toArray();
        //assign hdrData to model
        foreach($hdrData as $field => $value) 
        {
            $binTrfHdrData[$field] = $value;
        }

		//$binTrfHdr->load('worker01');
        //$worker01 = $binTrfHdr->worker01;
		
		//no detail to update
        $binTrfDtlArray = array();

        $result = BinTrfHdrRepository::updateDetails($binTrfHdrData, $binTrfDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];

        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = self::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('BinTrf.document_successfully_updated', ['docCode'=>$documentHeader->doc_code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function initHeader($siteFlowId)
    {
		AuthService::authorize(
			array(
				'bin_trf_create'
			)
		);

		$user = Auth::user();

        $siteFlow = SiteFlowRepository::findByPk($siteFlowId);
        $model = new BinTrfHdr();
        $model->doc_flows = array();
        $model->doc_status = DocStatus::$MAP['DRAFT'];
        $model->str_doc_status = DocStatus::$MAP[$model->doc_status];
        $model->doc_code = '';
        $model->ref_code_01 = '';
        $model->ref_code_02 = '';
        $model->doc_date = date('Y-m-d');
        $model->desc_01 = '';
        $model->desc_02 = '';

        $model->site_flow_id = $siteFlowId;

        $model->doc_no_id = 0;
        $docNoIdOptions = array();
        $docNos = DocNoRepository::findAllSiteDocNo($siteFlow->site_id, \App\BinTrfHdr::class, $model->doc_date);
        for($a = 0; $a < count($docNos); $a++)
        {
            $docNo = $docNos[$a];
            if($a == 0)
            {
                $model->doc_no_id = $docNo->id;
            }
            $docNoIdOptions[] = array('value'=>$docNo->id, 'label'=>$docNo->latest_code);
        }
        $model->doc_no_id_options = $docNoIdOptions;

        //company select2
        $initCompanyOption = array(
            'value'=>0,
            'label'=>''
        );
        $model->company_select2 = $initCompanyOption;

        return $model;
	}
	
	static public function processIncomingHeader($data)
    {
		if(array_key_exists('doc_flows', $data))
        {
            unset($data['doc_flows']);
		}
		if(array_key_exists('to_doc_txn_flows', $data))
        {
            unset($data['to_doc_txn_flows']);
		}	
		if(array_key_exists('submit_action', $data))
        {
            unset($data['submit_action']);
		}
		if(array_key_exists('whse_job_hdr_id', $data))
        {
            unset($data['whse_job_hdr_id']);
		}
		if(array_key_exists('whse_job_doc_code', $data))
        {
            unset($data['whse_job_doc_code']);
		}
        //preprocess the select2 and dropDown
        
        if(array_key_exists('str_doc_status', $data))
        {
            unset($data['str_doc_status']);
        }
        
        if(array_key_exists('company_select2', $data))
        {
			$select2Id = $data['company_select2']['value'];
			if($select2Id > 0)
			{
				$data['company_id'] = $select2Id;
			}
            unset($data['company_select2']);
        }        

        if(array_key_exists('company_code', $data))
        {
            unset($data['company_code']);
		}	
        return $data;
	}
	
	public function index($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
			array(
				'bin_trf_read'
			)
		);

        //DB::connection()->enableQueryLog();
        $binTrfHdrs = BinTrfHdrRepository::findAll($siteFlowId, $sorts, $filters, $pageSize);
        $binTrfHdrs->load(
            'binTrfDtls', 
            'binTrfDtls.item', 'binTrfDtls.item.itemUoms', 'binTrfDtls.item.itemUoms.uom'
        );
		foreach($binTrfHdrs as $binTrfHdr)
		{
			$binTrfHdr->str_doc_status = DocStatus::$MAP[$binTrfHdr->doc_status];

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$binTrfDtls = $binTrfHdr->binTrfDtls;
			foreach($binTrfDtls as $binTrfDtl)
			{
				$item = $binTrfDtl->item;

				//calculate the pallet qty, case qty, gross weight, and m3
				$binTrfDtl = ItemService::processCaseLoose($binTrfDtl, $item);

                $caseQty = bcadd($caseQty, $binTrfDtl->case_qty, 8);
                $grossWeight = bcadd($grossWeight, $binTrfDtl->gross_weight, 8);
                $cubicMeter = bcadd($cubicMeter, $binTrfDtl->cubic_meter, 8);

				unset($binTrfDtl->quantBal);
				unset($binTrfDtl->item);
            }
            
			$binTrfHdr->case_qty = $caseQty;
			$binTrfHdr->gross_weight = $grossWeight;
			$binTrfHdr->cubic_meter = $cubicMeter;

			$binTrfHdr->details = $binTrfDtls;
        }
        //Log::error(DB::getQueryLog());
    	return $binTrfHdrs;
	}
	
	public function changeQuantBal($hdrId, $quantBalId)
    {
		AuthService::authorize(
			array(
				'bin_trf_update'
			)
		);

		$binTrfHdr = BinTrfHdrRepository::findByPk($hdrId);

		$results = array();
		$quantBal = QuantBalRepository::findByPk($quantBalId);
        if(!empty($quantBal))
        {
			$item = $quantBal->item;
			$itemBatch = $quantBal->itemBatch;
			$handlingUnit = $quantBal->handlingUnit;
			
			$quantBal = ItemService::processCaseLoose($quantBal, $item, 1);

			$results['item_id'] = $quantBal->item_id;
			$results['item_code'] = $quantBal->item_code;
			$results['item_desc_01'] = $quantBal->item_desc_01;
			$results['item_desc_02'] = $quantBal->item_desc_02;
			$results['desc_01'] = $quantBal->item_desc_01;
			$results['desc_02'] = $quantBal->item_desc_02;
			$results['case_qty'] = $quantBal->case_qty;
			$results['item_case_uom_code'] = $quantBal->item_case_uom_code;
            $results['batch_serial_no'] = empty($itemBatch) ? '' : $itemBatch->batch_serial_no;
            $results['expiry_date'] = empty($itemBatch) ? '' : $itemBatch->expiry_date;
            $results['receipt_date'] = empty($itemBatch) ? '' : $itemBatch->receipt_date;
            $results['handling_unit_barcode'] = empty($handlingUnit) ? '' : $handlingUnit->getBarcode();
			
		}

        return $results;
	}

	public function changeItemUom($hdrId, $itemId, $uomId)
    {
		AuthService::authorize(
			array(
				'bin_trf_update'
			)
		);

        $binTrfHdr = BinTrfHdrRepository::findByPk($hdrId);

        $results = array();
        $itemUom = ItemUomRepository::findByItemIdAndUomId($itemId, $uomId);
        if(!empty($itemUom)
        && !empty($binTrfHdr))
        {
            $results['item_id'] = $itemUom->item_id;
            $results['uom_id'] = $itemUom->uom_id;
            $results['uom_rate'] = $itemUom->uom_rate;

			$results = $this->updateWarehouseItemUom($results, 0, 0);
        }
        return $results;
	}

	static public function transitionToVoid($hdrId)
    {
		AuthService::authorize(
			array(
				'bin_trf_revert'
			)
		);

		//use transaction to make sure this is latest value
		$hdrModel = BinTrfHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('BinTrf.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\BinTrfHdr::class, $hdrModel->id);
            throw $exc;
		}

		//revert the document
		$hdrModel = BinTrfHdrRepository::revertDraftToVoid($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('BinTrf.commit_to_void_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\BinTrfHdr::class, $hdrModel->id);
        throw $exc;
    }
}
