<?php

namespace App\Services;

use App\Repositories\ItemCond01Repository;
class ItemCond01Service 
{
    public function __construct() 
	{
    }
    
    public function select2($search, $filters)
    {
        $itemCond01s = ItemCond01Repository::select2($search, $filters);
        return $itemCond01s;
    }
}
