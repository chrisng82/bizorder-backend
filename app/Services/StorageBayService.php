<?php

namespace App\Services;

use App\Services\Env\ProcType;
use App\Services\Env\ResStatus;
use App\Repositories\BatchJobStatusRepository;
use App\Repositories\StorageBayRepository;
use App\Repositories\QuantBalRepository;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class StorageBayService 
{
    public function __construct() 
	{
    }

    public function select2($search, $filters)
    {
        $storageBays = StorageBayRepository::select2($search, $filters);
        return $storageBays;
    }
}
