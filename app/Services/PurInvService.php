<?php

namespace App\Services;

use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Services\Env\ResType;
use App\Repositories\SiteDocNoRepository;
use App\Repositories\DocTxnFlowRepository;
use App\Repositories\GdsRcptHdrRepository;
use App\Repositories\GdsRcptDtlRepository;
use App\Repositories\GdsRcptInbOrdRepository;
use App\Repositories\InbOrdHdrRepository;
use App\Repositories\InbOrdDtlRepository;
use App\Repositories\ItemCond01Repository;
use App\Services\Utils\ApiException;
use App\Services\ItemService;

class PurInvService 
{
  	public function __construct() 
	{
  	}
    
	public function indexProcess($strProcType, $divisionId, $sorts, $filters = array(), $pageSize = 20) 
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['PUR_INV_01']) 
			{
				//InbOrd -> Pur Inv
				$inbOrdHdrs = $this->indexPurInv01($divisionId, $sorts, $filters, $pageSize);
				return $inbOrdHdrs;
			}
		}
	}
    
	protected function indexPurInv01($divisionId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
			array(
				'inb_ord_read'
			)
		);

		//findAll the itemCond01
		$itemCond01s = ItemCond01Repository::findAll();
		$inbOrdHdrs = InbOrdHdrRepository::findAllNotExistPurInv01Txn($divisionId, DocStatus::$MAP['COMPLETE'], $sorts, $filters, $pageSize);
		foreach($inbOrdHdrs as $inbOrdHdr)
		{
			$inbOrdHdr->str_doc_status = DocStatus::$MAP[$inbOrdHdr->doc_status];

			$division = $inbOrdHdr->division;
			$inbOrdHdr->division_code = $division->code;

			$bizPartner = $inbOrdHdr->bizPartner;
			$inbOrdHdr->biz_partner_code = $bizPartner->code;
			$inbOrdHdr->biz_partner_company_name_01 = $bizPartner->company_name_01;

			$purchaser = $inbOrdHdr->purchaser;
			$inbOrdHdr->purchaser_username = $purchaser->username;

			//format gdsRcptDtls to hashtable
			$gdsRcptDtlsHashByKey = array();
			$ttlRcptUnitQtyHashByKey = array();

			$gdsRcptHdrIds = array();
			$gdsRcptInbOrds = GdsRcptInbOrdRepository::findAllByInbOrdHdrId($inbOrdHdr->id);
			foreach($gdsRcptInbOrds as $gdsRcptInbOrd)
			{
				$gdsRcptHdrIds[] = $gdsRcptInbOrd->hdr_id;
			}

			$gdsRcptDtls = GdsRcptDtlRepository::findAllByHdrIds($gdsRcptHdrIds);
			$gdsRcptDtls->load('item');
			foreach($gdsRcptDtls as $gdsRcptDtl)
			{
				$key = $gdsRcptDtl->item_id;
				$item = $gdsRcptDtl->item;
				$unitQty = bcmul($gdsRcptDtl->qty, $gdsRcptDtl->uom_rate, $item->qty_scale);

				$tmpDtls = array();
				if(array_key_exists($key, $gdsRcptDtlsHashByKey))
				{
					$tmpDtls = $gdsRcptDtlsHashByKey[$key];
				}
				$tmpDtls[] = $gdsRcptDtl;

				$gdsRcptDtlsHashByKey[$key] = $tmpDtls;

				//process the ttlRcptUnitQty
				$rcptUnitQty = 0;
				if(array_key_exists($key, $ttlRcptUnitQtyHashByKey))
				{
					$rcptUnitQty = $ttlRcptUnitQtyHashByKey[$key];
				}
				$rcptUnitQty = bcadd($rcptUnitQty, $unitQty, $item->qty_scale);
				$ttlRcptUnitQtyHashByKey[$key] = $rcptUnitQty;
			}

			$inbOrdDtls = InbOrdDtlRepository::findAllByHdrId($inbOrdHdr->id);
			$inbOrdDtls->load('item');
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			foreach($inbOrdDtls as $inbOrdDtl)
			{
				$item = $inbOrdDtl->item;
				$key = $inbOrdDtl->item_id;

				$inbOrdDtl = self::processOutgoingDetail($inbOrdDtl);

				$rcptUnitQty = 0;
				if(array_key_exists($key, $ttlRcptUnitQtyHashByKey))
				{
					$rcptUnitQty = $ttlRcptUnitQtyHashByKey[$key];
				}
				$inbOrdDtl->rcpt_unit_qty = $rcptUnitQty;

				//calculate the receive qty %
				$inbOrdDtl->rcpt_perc = bcmul(bcdiv($inbOrdDtl->rcpt_unit_qty, $inbOrdDtl->unit_qty, 10), 100, 10);

				$caseQty = bcadd($caseQty, $inbOrdDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $inbOrdDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $inbOrdDtl->cubic_meter, 8);
			}

			$inbOrdHdr->case_qty = $caseQty;
			$inbOrdHdr->gross_weight = $grossWeight;
			$inbOrdHdr->cubic_meter = $cubicMeter;
			$inbOrdHdr->details = $inbOrdDtls;

			//unset the item, so it wont send in json
			unset($inbOrdHdr->division);
			unset($inbOrdHdr->creditor);
			unset($inbOrdHdr->purchaser);
		}

		return $inbOrdHdrs;
  	}

	public function createProcess($strProcType, $hdrIds) 
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['PUR_INV_01'])
			{
				$toDocHdrs = $this->createPurInv01(ProcType::$MAP[$strProcType], $hdrIds);
				return $toDocHdrs;
			}
		}
	}

	protected function createPurInv01($procType, $hdrIds)
  	{
		AuthService::authorize(
			array(
				'pur_inv_create'
			)
		);
	}

	static public function processOutgoingDetail($model)
    {
		$item = $model->item;
        
		$model = ItemService::processCaseLoose($model, $item);
		
		return $model;
    }
}
