<?php

namespace App\Services;

use App\Debtor;
use App\DeliveryPoint;
use App\Division;
use App\Repositories\DebtorRepository;
use App\Repositories\DivisionRepository;
use App\Repositories\DebtorDivisionRepository;
use App\Repositories\BatchJobStatusRepository;
use App\Repositories\SyncSettingHdrRepository;
use App\Repositories\SyncSettingDtlRepository;
use App\Repositories\SiteFlowRepository;
use App\BatchJobStatus;
use App\Services\Env\ProcType;
use App\Services\Env\ResStatus;
use App\Services\Utils\ApiException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;

class DebtorService
{
    public function __construct()
    {
    }

    public function initHeader($divisionId)
    {
        $model = new Debtor();
        $model->status = ResStatus::$MAP['ACTIVE'];
        $model->code = '';
        $model->ref_code_01 = '';

        return $model;
	}
    
    public function showHeader($hdrId)
    {
		$model = DebtorRepository::findByPk($hdrId);
        
		return $model;
    }

    public function updateHeader($data)
	{
        $data = self::processIncomingModel($data);

        $debtor = DebtorRepository::findByPk($data['id']);
        $debtorData = $debtor->toArray();
        //assign data to model
        foreach($data as $field => $value) 
        {
            $debtorData[$field] = $value;
        }

        $result = DebtorRepository::updateHeader($debtorData);
        $model = $result['model'];

        // $model = self::processOutgoingModel($model);

        $message = __('Debtor.record_successfully_updated', ['code'=>$model->code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'model'=>$model
            ),
			'message' => $message
		);
    }

    public function deleteHeader($data)
	{
        $result = DebtorRepository::deleteHeader($data['id']);
        $model = $result['model'];

        $message = __('Debtor.details_successfully_deleted', ['code' => $model->code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'deleted_model'=>$model,
            ),
			'message' => $message
		);
    }

    public function changeStatus($data)
	{
        // $data = self::processIncomingModel($data);
        
        $debtor = DebtorRepository::findByPk($data['id']);
        $debtorData = $debtor->toArray();
        
        $debtorData['status'] = $data['status'];

        $result = DebtorRepository::updateHeader($debtorData);
        $model = $result['model'];


        $message = __('Debtor.record_successfully_updated', ['code'=>$model->code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'model'=>$model
            ),
			'message' => $message
		);
    }

    public function createHeader($data, $divisionId)
    {
        $data = self::processIncomingModel($data);
        $model = DebtorRepository::createHeader($data);

        $model->address_html = "";
        $model->str_status = "ACTIVE";
        $division = DivisionRepository::findByPk($divisionId);
        if(!empty($division))
        {
            $model->divisions = [$division];
        }
        $model->delivery_points = [];

        $debtor_division = array();
        $debtor_division['debtor_id'] = $model->id;
        $debtor_division['division_id'] = $divisionId;
        $debtor_division['ref_code_01'] = $model->ref_code_01;
        $debtor_division['ref_code_02'] = $model->ref_code_02;
        DebtorDivisionRepository::createModel($debtor_division);

        $message = __('Debtor.record_successfully_created.', ['code' => $model->code]);

        return array(
            'data' => $model->id,
            'message' => $message,
        );
    }

    public function indexProcess($strProcType, $sorts, $filters = array(), $pageSize = 20) 
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['DEBTOR_LIST_01']) 
			{
				//debtor listing
				$debtors = $this->indexDebtorList01($sorts, $filters, $pageSize);
				return $debtors;
            }
			else if(ProcType::$MAP[$strProcType] == ProcType::$MAP['DEBTOR_LIST_02']) 
			{
				//item listing
				// $items = $this->indexItemList02($sorts, $filters, $pageSize);
				// return $items;
			}
		}
	}
    
    public function indexDivision($divisionId, $sorts, $filters = array(), $pageSize = 20,$status) 
	{
        //debtor listing
        $debtors = $this->indexDebtorList01($sorts, $filters, $pageSize, $divisionId,$status);
        return $debtors;
	}

    protected function indexDebtorList01($sorts, $filters = array(), $pageSize = 20, $divisionId = 0,$status)
	{
        // AuthService::authorize(
        //     array(
        //         'item_read'
        //     )
        // );
		$debtors = DebtorRepository::findAll($sorts, $filters, $pageSize, $divisionId,$status);
        
        //$debtors->load('unitUom', 'caseUom');
		//query the docDtls, and format into group details
		foreach($debtors as $debtor)
		{
            $address_html = '';
            // if (!empty($debtor->unit_no)) {
            //     $address_html .= $debtor->unit_no . ', ';
            // }
            // if (!empty($debtor->building_name)) {
            //     $address_html .= $debtor->building_name . '<br>';
            // }
            // if (!empty($debtor->street_name)) {
            //     $address_html .= $debtor->street_name . '<br>';
            // }
            // if (!empty($debtor->district_01)) {
            //     $address_html .= $debtor->district_01 . '<br>';
            // }
            // if (!empty($debtor->district_02)) {
            //     $address_html .= $debtor->district_02 . '<br>';
            // }
            // if (!empty($debtor->postcode)) {
            //     $address_html .= $debtor->postcode . ' ';
            // }
            // if (!empty($debtor->state_name)) {
            //     $address_html .= $debtor->state_name . ', ';
            // }
            // if (!empty($debtor->country_name)) {
            //     $address_html .= $debtor->country_name;
            // }
            
            if (!empty($debtor->address_01)) {
                $address_html .= $debtor->address_01 . '<br>';
            }
            if (!empty($debtor->address_02)) {
                $address_html .= $debtor->address_02 . '<br>';
            }
            if (!empty($debtor->address_03)) {
                $address_html .= $debtor->address_03 . '<br>';
            }
            if (!empty($debtor->address_04)) {
                $address_html .= $debtor->address_04 . '<br>';
            }

            $address_html = rtrim($address_html, ', ');
            $address_html = rtrim($address_html, '<br>');
            $debtor->address_html = $address_html;

            $debtor->str_status = ResStatus::$MAP[$debtor->status];
            
            $debtorDeliveryPoints = $debtor->deliveryPoints;
            $deliveryPoints = array();
            foreach($debtorDeliveryPoints as $debtorDeliveryPoint)
            {
				if($debtorDeliveryPoint->state)
				{
					$debtorDeliveryPoint->state_select2 = [
						'value'=>$debtorDeliveryPoint->state->id, 
						'label'=>$debtorDeliveryPoint->state->desc_01
					];
				} else {
					$debtorDeliveryPoint->state_select2 = ['value'=>0, 'label'=>''];
				}				
				
				if($debtorDeliveryPoint->area)
				{
					$debtorDeliveryPoint->area_select2 = [
						'value'=>$debtorDeliveryPoint->area->id, 
						'label'=>$debtorDeliveryPoint->area->code
					];
				} else {
					$debtorDeliveryPoint->area_select2 = ['value'=>0, 'label'=>''];
				}
				
                $addressLines = array();

				if(!empty($debtorDeliveryPoint->street_name)
					|| !empty($debtorDeliveryPoint->unit_no)) 
				{
					$tempLines = array();
					
					if(!empty($debtorDeliveryPoint->unit_no)) {
						$tempLines[] = $debtorDeliveryPoint->unit_no;
					}
					
					if(!empty($debtorDeliveryPoint->street_name)) {
						$tempLines[] = $debtorDeliveryPoint->street_name;
					}
					
					$addressLines[] = join(', ', $tempLines);
				}
				if(!empty($debtorDeliveryPoint->building_name)) 
				{
					$addressLines[] = $debtorDeliveryPoint->building_name;
				}
				if(!empty($debtorDeliveryPoint->postcode) 
					|| !empty($debtorDeliveryPoint->area)) 
				{
					$tempLines = array();
					
					if(!empty($debtorDeliveryPoint->postcode)) {
						$tempLines[] = $debtorDeliveryPoint->postcode;
					}
					
					if(!empty($debtorDeliveryPoint->area)) {
						$tempLines[] = $debtorDeliveryPoint->area->code;
					}
					
					$addressLines[] = join(' ', $tempLines);
				}

				if(!empty($debtorDeliveryPoint->state)) {
					$addressLines[] = $debtorDeliveryPoint->state->desc_01;
				}

				$debtorDeliveryPoint->address_html = join('<br>', $addressLines);

                $deliveryPoints[] = $debtorDeliveryPoint;
            }
            $debtor->deliveryPoints = $deliveryPoints;

            $debtorDivisions = $debtor->debtorDivisions;
            $divisions = array();
            foreach($debtorDivisions as $debtorDivision)
            {
                $divisions[] = $debtorDivision->division;
            }
            $debtor->divisions = $divisions;
		}
        return $debtors;
    }
    
    public function select2($search, $filters)
    {
        //DB::connection()->enableQueryLog();
        $debtors = DebtorRepository::select2($search, $filters);
        //dd(DB::getQueryLog());
        return $debtors;
    }

    public function select2Init($id)
    {
        $debtor = null;
        if($id > 0) 
        {
            $debtor = DebtorRepository::findByPk($id);
        }
        else
        {
            $debtor = DebtorRepository::findTop();
        }
        $option = array('value'=>0, 'label'=>'');
        if(!empty($debtor))
        {
            $option = array(
                'value'=>$debtor->id, 
                'label'=>$debtor->code.' '.$debtor->company_name_01
            );
        }
        return $option;
    }

    public static function select2Debtor($data)
    {
        $debtors = Debtor::
            orWhere('company_name_01', 'like', '%' . $data . '%')
            ->orWhere('code', 'like', '%' . $data . '%')
            ->paginate(3);

        return $debtors;
    }

    public static function select2UpdateDebtor($data)
    {
        $debtor = Debtor::where('id',$data)
            ->first();
        return $debtor;
    }

    public static function getDeliveryPoint($debtor_id){
        $deliveryPoints = DeliveryPoint::with('area', 'state')
			->where('debtor_id',$debtor_id)->get();

        $deliveryPointsArray = [];
        foreach ($deliveryPoints as $key => $value) {	
			$deliveryPoint = [
                "id"=>$value['id'],
                "code"=>$value['code'],
                "company_name_01"=>$value['company_name_01'],
                "company_name_02"=>$value['company_name_02'],
                "area_id"=>$value['area_id'],
                "area_code"=>$value['area_code'],
                "unit_no"=>$value['unit_no'],
                "building_name"=>$value['building_name'],
                "street_name"=>$value['street_name'],
                "district_01"=>$value['district_01'],
                "district_02"=>$value['district_02'],
                "postcode"=>$value['postcode'],
                "state_name"=>$value['state_name'],
                "attention"=>$value['attention'],
                "phone_01"=>$value['phone_01'],
                "phone_02"=>$value['phone_02'],
                "fax_01"=>$value['fax_01'],
                "fax_02"=>$value['fax_02'],
                "email_01"=>$value['email_01'],
                "email_02"=>$value['email_02']
			];
			
			if($value['state'])
			{
				$deliveryPoint['state']=[
					"id"=>$value['state']->id,
					"code"=>$value['state']->code,
					"desc_01"=>$value['state']->desc_01,
					"desc_02"=>$value['state']->desc_02
				];
			}

			if($value['area'])
			{
				$deliveryPoint['area']=[
					"id"=>$value['area']->id,
					"code"=>$value['area']->code,
					"desc_01"=>$value['area']->desc_01,
					"desc_02"=>$value['area']->desc_02,
					"state_id"=>$value['area']->state_id,
				];
			}

            $deliveryPointsArray[]=$deliveryPoint;
        }
        
        return $deliveryPointsArray;
    }

    public function showModel($divisionId)
    {
        $model = DebtorRepository::findByPk($divisionId);
        return $model;
    }

    public function showDivisions($debtorId) 
	{
        // AuthService::authorize(
        //     array(
        //         'debtor_read',
		// 		'debtor_update'
        //     )
        // );

        $debtor = DebtorRepository::findByPk($debtorId);
        $debtorDivisions = $debtor->debtorDivisions;
        $debtorDivisions->load(
            'division', 'division.company', 'division.siteFlow'
        );
        foreach($debtorDivisions as $debtorDivision)
        {
            $debtorDivision = DebtorDivisionService::processOutgoingModel($debtorDivision);
        }
        return $debtorDivisions;
    }

    public function deleteDivisions($debtorId, $debtorDivisionDataArray)
	{
        // AuthService::authorize(
        //     array(
        //         'debtor_update'
        //     )
        // );

        $deleted_divisions = array();
        DebtorRepository::updateDivisions($debtorId, array(), $debtorDivisionDataArray);
        foreach($debtorDivisionDataArray as $delDebtorDivisionData)
        {
            $deleted_divisions[] = DivisionRepository::findByPk($delDebtorDivisionData['division_id']);
        }

        $message = __('Debtor.divisions_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'divisions'=>array(),
                'deleted_divisions'=>$deleted_divisions,
                'deleted_debtor_divisions'=>$debtorDivisionDataArray
            ),
			'message' => $message
		);
	}

	public function createDivisions($debtorId, $dataArray)
	{
        // AuthService::authorize(
        //     array(
        //         'debtor_update'
        //     )
        // );

        $debtorDivisionArray = array();

        foreach($dataArray as $data)
        {
            //assign temp id
            $tmpUuid = uniqid();
            //append NEW here to make sure it will be insert in repository later
            $data['id'] = 'NEW'.$tmpUuid;
            $data['is_modified'] = 1;
            $debtorDivisionArray[] = $data;
        }

        $result = DebtorRepository::updateDivisions($debtorId, $debtorDivisionArray, array());
        $debtorDivisionModels = $result['debtorDivisionModels'];
        $debtorDivisions = array();
        $divisions = array();
        foreach($debtorDivisionModels as $debtorDivisionModel)
        {
            $debtorDivisions[] = DebtorDivisionService::processOutgoingModel($debtorDivisionModel);
            $divisions[] = $debtorDivisionModel->division;
        }

        $message = __('Debtor.divisions_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'divisions'=>$divisions,
                'debtor_divisions'=>$debtorDivisions,
                'deleted_divisions'=>array()
            ),
			'message' => $message
		);
    }

    public function syncProcess($strProcType, $siteFlowId, $divisionId)
    {
        $user = Auth::user();

        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['DEBTOR_SYNC_01']) 
			{
				//sync items from EfiChain
				return $this->syncDebtorSync01($siteFlowId, $user->id, $divisionId);
            }
		}
    }
    
    protected function syncDebtorSync01($siteFlowId, $userId, $divisionId)
    {
        // AuthService::authorize(
        //     array(
        //         'debtor_import'
        //     )
        // );

        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['DEBTOR_SYNC_01'], $userId);

        $syncSettingHdr = SyncSettingHdrRepository::findByProcTypeAndDivisionId(ProcType::$MAP['SYNC_EFICHAIN_01'], $divisionId);
        if(empty($syncSettingHdr))
        {
            $exc = new ApiException(__('Debtor.sync_setting_not_found', ['divisionId'=>$divisionId]));
            //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
            throw $exc;
        }

        $client = new Client();
        $header = array();

        $page = 1;
        $lastPage = 1;
        $pageSize = 1000;
        $total = 0;

        $usernameSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'username');
        $passwordSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'password');
        $formParams = array(
            'UserLogin[username]' => $usernameSetting->value,
            'UserLogin[password]' => $passwordSetting->value
        );
        $pageSizeSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'page_size');
        if(!empty($pageSizeSetting))
        {
            $pageSize = $pageSizeSetting->value;
        }
        
        //use division code 
        $divisionDB = Division::where('id', $divisionId)->first();

        while($page <= $lastPage)
        {
            $url = $syncSettingHdr->url.'/index.php?r=bizOrder/getCustomers&division='.$divisionDB->code.'&page_size='.$pageSize.'&page='.$page;
            
            $response = $client->request('POST', $url, array('headers' => $header, 'form_params' => $formParams));
            $result = $response->getBody()->getContents();
            $result = json_decode($result, true);
            if(empty($result))
            {
                $exc = new ApiException(__('Debtor.fail_to_sync', ['url'=>$url]));
                //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
                throw $exc;
            }

            $total = $result['total'];
            for ($a = 0; $a < count($result['data']); $a++)
            {
                $debtorData = $result['data'][$a];
                $debtorModel = DebtorRepository::syncDebtorSync01($debtorData, $divisionId, $syncSettingHdr->id);

                $statusNumber = 100;
                if($total > 0)
                {
                    $statusNumber = bcmul(bcdiv(($a + ($page - 1) * $pageSize), $total, 8), 100, 2);
                }
                BatchJobStatusRepository::updateStatusNumber($batchJobStatusModel->id, $statusNumber);
            }

            $lastPage = $total % $pageSize == 0 ? ($total / $pageSize) : ($total / $pageSize) + 1;

            $page++;
        }

        $batchJobStatusModel = BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

        return $batchJobStatusModel;
    }

    static public function processIncomingModel($data)
    {
        $attributes = [
            'id', 'code', 'ref_code_01', 'company_name_01', 'company_name_02', 'address_01', 'address_02', 'address_03', 
            'address_04', 'attention', 'phone_01', 'phone_02', 'fax_01', 'fax_02', 'email_01', 'email_02', 'status'];
        foreach($data as $field => $value) {
            if(!in_array($field, $attributes)){
                unset($data[$field]);
            }
        }

        return $data;
    }

    static public function processOutgoingModel($debtor, $divisionId)
	{
        $address_html = '';
        
        if (!empty($debtor->address_01)) {
            $address_html .= $debtor->address_01 . '<br>';
        }
        if (!empty($debtor->address_02)) {
            $address_html .= $debtor->address_02 . '<br>';
        }
        if (!empty($debtor->address_03)) {
            $address_html .= $debtor->address_03 . '<br>';
        }
        if (!empty($debtor->address_04)) {
            $address_html .= $debtor->address_04 . '<br>';
        }

        $address_html = rtrim($address_html, ', ');
        $address_html = rtrim($address_html, '<br>');
        $debtor->address_html = $address_html;

        $debtor->str_status = ResStatus::$MAP[$debtor->status];
        
        $debtorDeliveryPoints = $debtor->deliveryPoints;
        $deliveryPoints = array();
        foreach($debtorDeliveryPoints as $debtorDeliveryPoint)
        {
            if($debtorDeliveryPoint->state)
            {
                $debtorDeliveryPoint->state_select2 = [
                    'value'=>$debtorDeliveryPoint->state->id, 
                    'label'=>$debtorDeliveryPoint->state->desc_01
                ];
            } else {
                $debtorDeliveryPoint->state_select2 = ['value'=>0, 'label'=>''];
            }				
            
            if($debtorDeliveryPoint->area)
            {
                $debtorDeliveryPoint->area_select2 = [
                    'value'=>$debtorDeliveryPoint->area->id, 
                    'label'=>$debtorDeliveryPoint->area->code
                ];
            } else {
                $debtorDeliveryPoint->area_select2 = ['value'=>0, 'label'=>''];
            }
            
            $addressLines = array();

            if(!empty($debtorDeliveryPoint->street_name)
                || !empty($debtorDeliveryPoint->unit_no)) 
            {
                $tempLines = array();
                
                if(!empty($debtorDeliveryPoint->unit_no)) {
                    $tempLines[] = $debtorDeliveryPoint->unit_no;
                }
                
                if(!empty($debtorDeliveryPoint->street_name)) {
                    $tempLines[] = $debtorDeliveryPoint->street_name;
                }
                
                $addressLines[] = join(', ', $tempLines);
            }
            if(!empty($debtorDeliveryPoint->building_name)) 
            {
                $addressLines[] = $debtorDeliveryPoint->building_name;
            }
            if(!empty($debtorDeliveryPoint->postcode) 
                || !empty($debtorDeliveryPoint->area)) 
            {
                $tempLines = array();
                
                if(!empty($debtorDeliveryPoint->postcode)) {
                    $tempLines[] = $debtorDeliveryPoint->postcode;
                }
                
                if(!empty($debtorDeliveryPoint->area)) {
                    $tempLines[] = $debtorDeliveryPoint->area->code;
                }
                
                $addressLines[] = join(' ', $tempLines);
            }

            if(!empty($debtorDeliveryPoint->state)) {
                $addressLines[] = $debtorDeliveryPoint->state->desc_01;
            }

            $debtorDeliveryPoint->address_html = join('<br>', $addressLines);

            $deliveryPoints[] = $debtorDeliveryPoint;
        }
        $debtor->deliveryPoints = $deliveryPoints;

        $debtorDivisions = $debtor->debtorDivisions;
        $divisions = array();
        foreach($debtorDivisions as $debtorDivision)
        {
            $divisions[] = $debtorDivision->division;
        }
        $debtor->divisions = $divisions;

		return $model;
    }
}