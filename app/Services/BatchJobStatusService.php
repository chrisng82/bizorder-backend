<?php

namespace App\Services;

use App\Services\Env\ProcType;
use App\Repositories\BatchJobStatusRepository;
use Illuminate\Support\Facades\Auth;

class BatchJobStatusService 
{
    public function __construct() 
	{
    }
    
    public function showBatchJobStatus($strProcType)
    {
        $user = Auth::user();

        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP[$strProcType], $user->id);
        return $batchJobStatusModel;
    }

    public function resetBatchJobStatus($strProcType)
    {
        $user = Auth::user();

        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP[$strProcType], $user->id);
        BatchJobStatusRepository::updateStatusNumber($batchJobStatusModel->id, 0);
        return $batchJobStatusModel;
    }
}
