<?php

namespace App\Services;

class UserDivisionService 
{
    public function __construct() 
	{
    }

    static public function processOutgoingModel($model)
  	{
        //relationships:
        $division = $model->division;

		$model->division_code = '';
		$model->division_name_01 = '';
        $model->division_name_02 = '';
        $model->company_code = '';
        if(!empty($division))
        {
            $model->division_code = $division->code;
            $model->division_name_01 = $division->name_01;
            $model->division_name_02 = $division->name_02;

            $company = $division->company;
            $model->company_code = $company->code;
        }
        
        unset($model->division);

		return $model;
    }
}