<?php

namespace App\Services;

use App\RtnRcptHdr;
use App\InbOrdHdr;
use App\GdsRcptHdr;
use App\Services\Env\ResType;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Repositories\ItemRepository;
use App\Repositories\RtnRcptHdrRepository;
use App\Repositories\RtnRcptDtlRepository;
use App\Repositories\DocTxnFlowRepository;
use App\Repositories\DivisionDocNoRepository;
use App\Repositories\PromoDtlRepository;
use App\Repositories\GdsRcptInbOrdRepository;
use App\Repositories\CreditTermRepository;
use App\Repositories\BatchJobStatusRepository;
use App\Repositories\SyncSettingHdrRepository;
use App\Repositories\SyncSettingDtlRepository;
use App\Services\Utils\ApiException;
use App\Services\ItemService;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class RtnRcptService extends InventoryService
{
	public static $HDR_ESCAPE = array(
		'id',
		'doc_code',
		'proc_type',
		'pur_ord_hdr_id',
		'pur_ord_hdr_code',
		'adv_ship_hdr_id',
		'adv_ship_hdr_code',
		'pur_inv_hdr_id',
		'pur_inv_hdr_code',
		'sls_rtn_hdr_id',
        'sls_rtn_hdr_code',
        'rtn_rcpt_hdr_id',
		'rtn_rcpt_hdr_code',
		'sls_cn_hdr_id',
		'sls_cn_hdr_code',
		'sign',
		'cur_res_type',
		'doc_date',
        'doc_status',
        
        'purchaser_id',
        'biz_partner_id',

		'created_at',
		'updated_at',
		
		'inb_ord_dtls',
	);
	
	public static $DTL_ESCAPE = array(
		'id',
		'hdr_id',
		'created_at',
		'updated_at',
		'inb_ord_hdr',
		'item'
    );

    public function __construct() 
    {

    }

	static public function transitionToComplete($hdrId)
    {
		AuthService::authorize(
            array(
                'rtn_rcpt_confirm'
            )
		);
		
		//use transaction to make sure this is latest value
		$hdrModel = RtnRcptHdrRepository::txnFindByPk($hdrId);

		//only DRAFT or above can transition to COMPLETE
		if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE']
		|| $hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('RtnRcpt.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
			$exc->addData(\App\RtnRcptHdr::class, $hdrModel->id);
			throw $exc;
		}

		//preliminary checking

		//commit the document
		$hdrModel = RtnRcptHdrRepository::commitToComplete($hdrModel->id);
		if(!empty($hdrModel))
		{
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		
		$exc = new ApiException(__('RtnRcpt.commit_to_complete_error', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\RtnRcptHdr::class, $hdrModel->id);
		throw $exc;
	}

	static public function transitionToWip($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = RtnRcptHdrRepository::txnFindByPk($hdrId);
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('RtnRcpt.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
			$exc->addData(\App\RtnRcptHdr::class, $hdrModel->id);
			throw $exc;
		}

		//preliminary checking

		//commit the document
		$hdrModel = RtnRcptHdrRepository::commitToWip($hdrModel->id);
		if(!empty($hdrModel))
		{
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			$isSuccess = true;
		}

		$exc = new ApiException(__('RtnRcpt.commit_to_wip_error', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\RtnRcptHdr::class, $hdrModel->id);
		throw $exc;
	}

	static public function transitionToDraft($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = RtnRcptHdrRepository::txnFindByPk($hdrId);

		//only WIP or COMPLETE can transition to DRAFT
		if($hdrModel->doc_status == DocStatus::$MAP['WIP'])
		{
			$hdrModel = RtnRcptHdrRepository::revertWipToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		elseif($hdrModel->doc_status == DocStatus::$MAP['COMPLETE'])
		{
			AuthService::authorize(
				array(
					'rtn_rcpt_revert'
				)
			);

			$hdrModel = RtnRcptHdrRepository::revertCompleteToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		
		$exc = new ApiException(__('RtnRcpt.doc_status_is_not_wip_or_complete', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\RtnRcptHdr::class, $hdrModel->id);
		throw $exc;
	}

	static public function createFrInbOrd($inbOrdHdr, $inbOrdDtls, $gdsRcptHdr)
	{
		AuthService::authorize(
            array(
                'rtn_rcpt_create'
            )
		);

		$dtlDataList = array();

		$lineNo = 1;
		foreach($inbOrdDtls as $inbOrdDtl) 
		{
			$item = $inbOrdDtl->item;

			$dtlData = array();
			foreach ($inbOrdDtl->toArray() as $fieldName => $value) 
			{
				if(in_array($fieldName, self::$DTL_ESCAPE))
				{
					continue;
				}
				$dtlData[$fieldName] = $value;
			}
			$dtlDataList[$lineNo] = $dtlData;
			$lineNo++;
		}
		
		//build the hdrData
		$hdrData = array();
		foreach($inbOrdHdr->toArray() as $fieldName => $value)
		{
			if(in_array($fieldName, self::$HDR_ESCAPE))
			{
				continue;
			}
			$hdrData[$fieldName] = $value;
		}
		$hdrData['doc_date'] = date('Y-m-d');

		$creditTermType = 0;
		if(!empty($hdrData['credit_term_id']))
		{
			$creditTerm = CreditTermRepository::findByPk($hdrData['credit_term_id']);
			$creditTermType = $creditTerm->credit_term_type;
		}
		$divisionDocNo = DivisionDocNoRepository::findDivisionDocNo($inbOrdHdr->division_id, \App\RtnRcptHdr::class, $creditTermType);
		if(empty($divisionDocNo))
		{
			$exc = new ApiException(__('DivisionDocNo.doc_no_not_found', ['divisionId'=>$inbOrdHdr->division_id, 'docType'=>\App\RtnRcptHdr::class]));
			//$exc->addData(\App\DivisionDocNo::class, $inbOrdHdr->division_id);
			throw $exc;
		}

		$docTxnFlowData = array(
			'fr_doc_hdr_type' => \App\GdsRcptHdr::class,
			'fr_doc_hdr_id' => $gdsRcptHdr->id,
			'fr_doc_hdr_code' => $gdsRcptHdr->doc_code,
			'is_closed' => 1
		);

		//return associative array, doc_type, doc_no_id, hdr_data, dtl_array, inb_ord_hdr_id
		return array(
			'doc_type' => \App\RtnRcptHdr::class,
			'doc_no_id' => $divisionDocNo->doc_no_id,
			'hdr_data' => $hdrData,
			'dtl_array' => $dtlDataList,
			'inb_ord_hdr_id' => $inbOrdHdr->id,
			'doc_txn_flow_data' => $docTxnFlowData
		);
	}

	public function showDetails($hdrId) 
	{
		AuthService::authorize(
			array(
				'rtn_rcpt_read',
				'rtn_rcpt_update'
			)
		);

		$rtnRcptDtls = RtnRcptDtlRepository::findAllByHdrId($hdrId);
		$rtnRcptDtls->load(
            'item', 'item.itemUoms', 'item.itemUoms.uom'
        );
        foreach($rtnRcptDtls as $rtnRcptDtl)
        {
            $rtnRcptDtl = self::processOutgoingDetail($rtnRcptDtl);
        }
        return $rtnRcptDtls;
    }

	public function updateDetails($hdrId, $dtlArray)
	{
		AuthService::authorize(
			array(
				'rtn_rcpt_update'
			)
		);

		//get the associated inbOrdHdr
		$rtnRcptHdr = RtnRcptHdrRepository::txnFindByPk($hdrId);
		if($rtnRcptHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('RtnRcpt.doc_status_is_wip_or_complete', ['docCode'=>$rtnRcptHdr->doc_code]));
            $exc->addData(\App\RtnRcptHdr::class, $rtnRcptHdr->id);
            throw $exc;
        }
		$rtnRcptHdrData = $rtnRcptHdr->toArray();
		$rtnRcptHdr->load('inbOrdHdr', 'deliveryPoint');
		$deliveryPoint = $rtnRcptHdr->deliveryPoint;

		//query the rtnRcptDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$rtnRcptDtlArray = array();
		$rtnRcptDtlModels = RtnRcptDtlRepository::findAllByHdrId($hdrId);
		foreach($rtnRcptDtlModels as $rtnRcptDtlModel)
		{
			$rtnRcptDtlData = $rtnRcptDtlModel->toArray();
			$rtnRcptDtlData['is_modified'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{
				if($dtlData['id'] == $rtnRcptDtlData['id'])
				{
					$dtlData = $this->updateSaleItemUom($dtlData, $rtnRcptHdr->doc_date, 0, $rtnRcptHdr->currency_id, 0, 0);
					foreach($dtlData as $fieldName => $value)
					{
						$rtnRcptDtlData[$fieldName] = $value;
					}
					$rtnRcptDtlData['is_modified'] = 1;

					break;
				}
			}
			$rtnRcptDtlArray[] = $rtnRcptDtlData;
		}

		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($rtnRcptHdrData, $rtnRcptDtlArray);
		$rtnRcptHdrData = $result['hdrData'];
		$rtnRcptDtlArray = $result['dtlArray'];

		//need to update the associated inbOrdHdr
		$inbOrdHdrData = array();
		$inbOrdDtlArray = array();
		$inbOrdHdr = $rtnRcptHdr->inbOrdHdr;
		if(!empty($inbOrdHdr))
		{
			if($inbOrdHdr->cur_res_type == ResType::$MAP['RTN_RCPT'])
			{
				$result = $this->tallyInbOrd($rtnRcptHdrData, $rtnRcptDtlArray);
				$inbOrdHdrData = $result['inbOrdHdrData'];
				$inbOrdDtlArray = $result['inbOrdDtlArray'];
			}
		}
		
		$result = RtnRcptHdrRepository::updateDetails($rtnRcptHdrData, $rtnRcptDtlArray, array(), $inbOrdHdrData, $inbOrdDtlArray, array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('RtnRcpt.details_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function createDetail($hdrId, $data)
	{
		AuthService::authorize(
			array(
				'rtn_rcpt_update'
			)
		);

		$rtnRcptHdr = RtnRcptHdrRepository::txnFindByPk($hdrId);
		$rtnRcptHdrData = $rtnRcptHdr->toArray();
		$rtnRcptHdr->load('inbOrdHdr', 'deliveryPoint');
		$deliveryPoint = $rtnRcptHdr->deliveryPoint;

		$lastLineNo = 0;
		$rtnRcptDtlArray = array();
		$rtnRcptDtlModels = RtnRcptDtlRepository::findAllByHdrId($hdrId);
		foreach($rtnRcptDtlModels as $rtnRcptDtlModel)
		{
			$lastLineNo = $rtnRcptDtlModel->line_no;
			$rtnRcptDtlData = $rtnRcptDtlModel->toArray();
			$rtnRcptDtlData['is_modified'] = 0;
			$rtnRcptDtlArray[] = $rtnRcptDtlData;
		}

		//assign temp id
		$tmpUuid = uniqid();
		//append NEW here to make sure it will be insert in repository later
		$data['id'] = 'NEW'.$tmpUuid;
		$data['line_no'] = $lastLineNo + 1;
		$data['is_modified'] = 1;
		$data = $this->updateSaleItemUom($data, $rtnRcptHdr['doc_date'], 0, $rtnRcptHdr['currency_id'], 0, 0);
		$rtnRcptDtlData = $this->initRtnRcptDtlData($data);
		$rtnRcptDtlArray[] = $rtnRcptDtlData;

		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($rtnRcptHdrData, $rtnRcptDtlArray);
		$rtnRcptHdrData = $result['hdrData'];
		$rtnRcptDtlArray = $result['dtlArray'];		

		//need to update the associated inbOrdHdr
		$inbOrdHdrData = array();
		$inbOrdDtlArray = array();
		$inbOrdHdr = $rtnRcptHdr->inbOrdHdr;
		if(!empty($inbOrdHdr))
		{
			if($inbOrdHdr->cur_res_type == ResType::$MAP['RTN_RCPT'])
			{
				$result = $this->tallyInbOrd($rtnRcptHdrData, $rtnRcptDtlArray);
				$inbOrdHdrData = $result['inbOrdHdrData'];
				$inbOrdDtlArray = $result['inbOrdDtlArray'];
			}
		}

		$result = RtnRcptHdrRepository::updateDetails($rtnRcptHdrData, $rtnRcptDtlArray, array(), $inbOrdHdrData, $inbOrdDtlArray, array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('RtnRcpt.detail_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function deleteDetails($hdrId, $dtlArray)
	{
		AuthService::authorize(
			array(
				'rtn_rcpt_update'
			)
		);

		//get the associated inbOrdHdr
		$rtnRcptHdr = RtnRcptHdrRepository::txnFindByPk($hdrId);
		$rtnRcptHdrData = $rtnRcptHdr->toArray();
		$rtnRcptHdr->load('inbOrdHdr', 'deliveryPoint');
		$deliveryPoint = $rtnRcptHdr->deliveryPoint;

		//query the rtnRcptDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$delRtnRcptDtlArray = array();
		$rtnRcptDtlArray = array();
		$rtnRcptDtlModels = RtnRcptDtlRepository::findAllByHdrId($hdrId);
		foreach($rtnRcptDtlModels as $rtnRcptDtlModel)
		{
			$rtnRcptDtlData = $rtnRcptDtlModel->toArray();
			$rtnRcptDtlData['is_modified'] = 0;
			$rtnRcptDtlData['is_deleted'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{
				if($dtlData['id'] == $rtnRcptDtlData['id'])
				{
					//this is deleted dtl
					$rtnRcptDtlData['is_deleted'] = 1;
					break;
				}
			}
			if($rtnRcptDtlData['is_deleted'] > 0)
			{
				$delRtnRcptDtlArray[] = $rtnRcptDtlData;
			}
			else
			{
				$rtnRcptDtlArray[] = $rtnRcptDtlData;
			}
		}

		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($rtnRcptHdrData, $rtnRcptDtlArray, $delRtnRcptDtlArray);
		$rtnRcptHdrData = $result['hdrData'];
		$modifiedDtlArray = $result['dtlArray'];
		foreach($rtnRcptDtlArray as $rtnRcptDtlSeq => $rtnRcptDtlData)
		{
			foreach($modifiedDtlArray as $modifiedDtlData)
			{
				if($modifiedDtlData['id'] == $rtnRcptDtlData['id'])
				{
					foreach($modifiedDtlData as $field => $value)
					{
						$rtnRcptDtlData[$field] = $value;
					}
					$rtnRcptDtlData['is_modified'] = 1;
					$rtnRcptDtlArray[$rtnRcptDtlSeq] = $rtnRcptDtlData;
					break;
				}
			}
		}

		//need to update the associated inbOrdHdr
		$inbOrdHdrData = array();
		$inbOrdDtlArray = array();
		$delInbOrdDtlArray = array();
		$inbOrdHdr = $rtnRcptHdr->inbOrdHdr;
		if(!empty($inbOrdHdr))
		{
			if($inbOrdHdr->cur_res_type == ResType::$MAP['RTN_RCPT'])
			{
				$result = $this->tallyInbOrd($rtnRcptHdrData, $rtnRcptDtlArray, $delRtnRcptDtlArray);
				$inbOrdHdrData = $result['inbOrdHdrData'];
				$inbOrdDtlArray = $result['inbOrdDtlArray'];
				$delInbOrdDtlArray = $result['delInbOrdDtlArray'];
			}
		}
		
		$result = RtnRcptHdrRepository::updateDetails($rtnRcptHdrData, $rtnRcptDtlArray, $delRtnRcptDtlArray, $inbOrdHdrData, $inbOrdDtlArray, $delInbOrdDtlArray);
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('RtnRcpt.details_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>$delSlsRtnDtlArray
            ),
			'message' => $message
		);
	}

	public function index($divisionId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
			array(
				'rtn_rcpt_read'
			)
		);

        //DB::connection()->enableQueryLog();
        $rtnRcptHdrs = RtnRcptHdrRepository::findAll($divisionId, $sorts, $filters, $pageSize);
        $rtnRcptHdrs->load(
            'rtnRcptDtls'
            //'rtnRcptDtls.item', 'rtnRcptDtls.item.itemUoms', 'rtnRcptDtls.item.itemUoms.uom', 
        );
		foreach($rtnRcptHdrs as $rtnRcptHdr)
		{
			$rtnRcptHdr->str_doc_status = DocStatus::$MAP[$rtnRcptHdr->doc_status];

			$deliveryPoint = $rtnRcptHdr->deliveryPoint;
			$rtnRcptHdr->delivery_point_code = $deliveryPoint->code;
			$rtnRcptHdr->delivery_point_company_name_01 = $deliveryPoint->company_name_01;
			$rtnRcptHdr->delivery_point_area_code = $deliveryPoint->area_code;
			$area = $deliveryPoint->area;
			if(!empty($area))
			{
				$rtnRcptHdr->delivery_point_area_desc_01 = $area->desc_01;
			}

			$salesman = $rtnRcptHdr->salesman;
			$rtnRcptHdr->salesman_username = $salesman->username;

			$rtnRcptDtls = $rtnRcptHdr->rtnRcptDtls;
			$rtnRcptHdr->details = $rtnRcptDtls;
        }
        //Log::error(DB::getQueryLog());
    	return $rtnRcptHdrs;
	}

	public function showHeader($hdrId)
	{
		AuthService::authorize(
			array(
				'rtn_rcpt_read',
				'rtn_rcpt_update'
			)
		);

		$model = RtnRcptHdrRepository::findByPk($hdrId);
		if(!empty($model))
        {
            $model = self::processOutgoingHeader($model);
		}
		
		return $model;
	}

	static public function processOutgoingHeader($model, $isShowPrint = false)
	{
		$docFlows = self::processDocFlows($model);
		$model->doc_flows = $docFlows;
		
		if($isShowPrint)
		{
			$printDocTxn = PrintDocTxnRepository::queryPrintDocTxn(\App\RtnRcptHdr::class, $model->id);
			$model->print_count = $printDocTxn->print_count;
			$model->first_printed_at = $printDocTxn->first_printed_at;
			$model->last_printed_at = $printDocTxn->last_printed_at;
		}

		$division = $model->division;
        $model->division_code = $division->code;
        $company = $model->company;
        $model->company_code = $company->code;

        $model->str_doc_status = DocStatus::$MAP[$model->doc_status];

        //currency select2
		$initCurrencyOption = array('value'=>0,'label'=>'');
		$model->currency_symbol = '';
        $currency = $model->currency;
        if(!empty($currency))
        {
			$model->currency_symbol = $currency->symbol;
            $initCurrencyOption = array('value'=>$currency->id, 'label'=>$currency->symbol);
        }
        $model->currency_select2 = $initCurrencyOption;

        //creditTerm select2
		$initCreditTermOption = array('value'=>0,'label'=>'');
		$model->credit_term_code = '';
		$model->credit_term_type = 0;
        $creditTerm = $model->creditTerm;
        if(!empty($creditTerm))
        {
			$model->credit_term_code = $creditTerm->code;
			$model->credit_term_type = $creditTerm->credit_term_type;
            $initCreditTermOption = array('value'=>$creditTerm->id, 'label'=>$creditTerm->code);
        }
        $model->credit_term_select2 = $initCreditTermOption;

		//salesman select2
		$initSalesmanOption = array('value'=>0,'label'=>'');
		$model->salesman_code = '';
		$salesman = $model->salesman;
		if(!empty($salesman))
		{
			$model->salesman_code = $salesman->username;
			$initSalesmanOption = array(
				'value'=>$salesman->id,
				'label'=>$salesman->username
			);
		}
		$model->salesman_select2 = $initSalesmanOption;

        //delivery point select2
		$deliveryPoint = $model->deliveryPoint;
		$model->delivery_point_code = $deliveryPoint->code;
		$model->delivery_point_area_code = $deliveryPoint->area_code;
		$model->delivery_point_code = $deliveryPoint->code;
		$model->delivery_point_company_name_01 = $deliveryPoint->company_name_01;
		$model->delivery_point_company_name_02 = $deliveryPoint->company_name_02;
        $model->delivery_point_unit_no = $deliveryPoint->unit_no;
        $model->delivery_point_building_name = $deliveryPoint->building_name;
        $model->delivery_point_street_name = $deliveryPoint->street_name;
        $model->delivery_point_district_01 = $deliveryPoint->district_01;
        $model->delivery_point_district_02 = $deliveryPoint->district_02;
        $model->delivery_point_postcode = $deliveryPoint->postcode;
        $model->delivery_point_state_name = $deliveryPoint->state_name;
		$model->delivery_point_country_name = $deliveryPoint->country_name;
		$model->delivery_point_attention = $deliveryPoint->attention;
		$model->delivery_point_phone_01 = $deliveryPoint->phone_01;
		$model->delivery_point_phone_02 = $deliveryPoint->phone_02;
		$model->delivery_point_fax_01 = $deliveryPoint->fax_01;
		$model->delivery_point_fax_02 = $deliveryPoint->fax_02;
        $initDeliveryPointOption = array(
            'value'=>$deliveryPoint->id,
            'label'=>$deliveryPoint->code.' '.$deliveryPoint->company_name_01
        );
		$model->delivery_point_select2 = $initDeliveryPointOption;
		
		//debtor
		$debtor = $deliveryPoint->debtor;
		$model->debtor_code = $debtor->code;
		$model->debtor_area_code = $debtor->area_code;
		$model->debtor_company_name_01 = $debtor->company_name_01;
		$model->debtor_company_name_02 = $debtor->company_name_02;
        $model->debtor_unit_no = $debtor->unit_no;
        $model->debtor_building_name = $debtor->building_name;
        $model->debtor_street_name = $debtor->street_name;
        $model->debtor_district_01 = $debtor->district_01;
        $model->debtor_district_02 = $debtor->district_02;
        $model->debtor_postcode = $debtor->postcode;
        $model->debtor_state_name = $debtor->state_name;
		$model->debtor_country_name = $debtor->country_name;
		$model->debtor_attention = $debtor->attention;
		$model->debtor_phone_01 = $debtor->phone_01;
		$model->debtor_phone_02 = $debtor->phone_02;
		$model->debtor_fax_01 = $debtor->fax_01;
		$model->debtor_fax_02 = $debtor->fax_02;

        $model->hdr_disc_val_01 = 0;
        $model->hdr_disc_perc_01 = 0;
        $model->hdr_disc_val_02 = 0;
        $model->hdr_disc_perc_02 = 0;
        $model->hdr_disc_val_03 = 0;
        $model->hdr_disc_perc_03 = 0;
        $model->hdr_disc_val_04 = 0;
        $model->hdr_disc_perc_04 = 0;
        $model->hdr_disc_val_05 = 0;
        $model->hdr_disc_perc_05 = 0;
        $firstDtlModel = RtnRcptDtlRepository::findTopByHdrId($model->id);
        if(!empty($firstDtlModel))
        {
            $model->hdr_disc_val_01 = $firstDtlModel->hdr_disc_val_01;
            $model->hdr_disc_perc_01 = $firstDtlModel->hdr_disc_perc_01;
            $model->hdr_disc_val_02 = $firstDtlModel->hdr_disc_val_02;
            $model->hdr_disc_perc_02 = $firstDtlModel->hdr_disc_perc_02;
            $model->hdr_disc_val_03 = $firstDtlModel->hdr_disc_val_03;
            $model->hdr_disc_perc_03 = $firstDtlModel->hdr_disc_perc_03;
            $model->hdr_disc_val_04 = $firstDtlModel->hdr_disc_val_04;
            $model->hdr_disc_perc_04 = $firstDtlModel->hdr_disc_perc_04;
            $model->hdr_disc_val_05 = $firstDtlModel->hdr_disc_val_05;
            $model->hdr_disc_perc_05 = $firstDtlModel->hdr_disc_perc_05;
		}
		
		//GdsRcptInbOrds
		$model->sls_rtn_hdr_doc_code = '';
		$model->gds_rcpt_hdr_doc_code = '';
		$model->gds_rcpt_hdr_doc_date = '1970-01-01';
		$gdsRcptInbOrds = GdsRcptInbOrdRepository::findAllCompleteGdsRcptByInbOrdHdrId($model->inb_ord_hdr_id);
		foreach($gdsRcptInbOrds as $gdsRcptInbOrd)
		{
			$model->sls_rtn_hdr_doc_code = $gdsRcptInbOrd->sls_rtn_hdr_doc_code;
			$model->gds_rcpt_hdr_doc_code = $gdsRcptInbOrd->gds_rcpt_hdr_doc_code;
			$model->pgds_rcpt_hdr_doc_date = $gdsRcptInbOrd->gds_rcpt_hdr_doc_date;
			break;
		}

		$model->net_amt_english = InventoryService::numberToWords($model->net_amt);

        return $model;
	}

	static public function formatDocuments($rtnRcptHdrs, $isShowPrint = false)
	{
		AuthService::authorize(
			array(
				'rtn_rcpt_print'
			)
		);

		foreach($rtnRcptHdrs as $rtnRcptHdr)
		{
			$rtnRcptHdr = self::processOutgoingHeader($rtnRcptHdr);

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletQty = 0;
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			$ttlDtlDiscAmt = 0;
			//query the docDtls
			$rtnRcptDtls = $rtnRcptHdr->rtnRcptDtls;
			$rtnRcptDtls = $rtnRcptDtls->sortBy('line_no');

			$formatRtnRcptDtls = array();
			foreach($rtnRcptDtls as $rtnRcptDtl)
			{
				$item = $rtnRcptDtl->item;
				
				$rtnRcptDtl = self::processOutgoingDetail($rtnRcptDtl);
				$priceDiscAmt = bcmul($rtnRcptDtl->price_disc, $rtnRcptDtl->qty);
				$rtnRcptDtl->ttl_dtl_disc_amt = bcadd($priceDiscAmt, $rtnRcptDtl->dtl_disc_amt, 8);

				$caseQty = bcadd($caseQty, $rtnRcptDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $rtnRcptDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $rtnRcptDtl->cubic_meter, 8);
				$ttlDtlDiscAmt = bcadd($ttlDtlDiscAmt, $rtnRcptDtl->ttl_dtl_disc_amt, 8);

				unset($rtnRcptDtl->item);
				$formatRtnRcptDtls[] = $rtnRcptDtl;
			}

			$rtnRcptHdr->case_qty = $caseQty;
			$rtnRcptHdr->gross_weight = $grossWeight;
			$rtnRcptHdr->cubic_meter = $cubicMeter;
			$rtnRcptHdr->ttl_dtl_disc_amt = $ttlDtlDiscAmt;

			$rtnRcptHdr->details = $formatRtnRcptDtls;
		}
		return $rtnRcptHdrs;
	}

	static public function processOutgoingDetail($model)
    {
        $item = $model->item;
        $model->item_code = $item->code;
        //$model->item_desc_01 = $item->desc_01;
        //$model->item_desc_02 = $item->desc_02;
        //item select2
        $initItemOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($item))
        {
            $initItemOption = array(
                'value'=>$item->id, 
                'label'=>$item->code.' '.$item->desc_01,
            );
        }
		$model->item_select2 = $initItemOption;
		
		//calculate the pallet qty, case qty, gross weight, and m3
        $model = ItemService::processCaseLoose($model, $item);

		$model->uom_code = '';
        $uom = $model->uom;
        //uom select2
        $initUomOption = array('value'=>0, 'label'=>'');
        if(!empty($uom))
        {
			$model->uom_code = $uom->code;
            $initUomOption = array('value'=>$uom->id,'label'=>$uom->code);
        }        
		$model->uom_select2 = $initUomOption;
		
		//location
		$location = $model->location;
		$model->location_code = '';
		$initLocationOption = array('value'=>0, 'label'=>'');
        if(!empty($location))
        {
			$model->location_code = $location->code;
            $initLocationOption = array('value'=>$location->id,'label'=>$location->code);
        }        
		$model->location_select2 = $initLocationOption;

		$reason01 = $model->reason01;
		$model->reason01_code = '';
		if(!empty($reason01))
		{
			$model->reason01_code = $reason01->code;
		}

        return $model;
	}
	
	public function syncProcess($strProcType, $divisionId, $startDate, $endDate)
    {
		$user = Auth::user();

        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['RTN_RCPT_SYNC_01']) 
			{
				//sync return receipt to EfiChain
				return $this->syncRtnRcptSync01($divisionId, $user->id, $startDate, $endDate);
            }
		}
    }

    protected function syncRtnRcptSync01($divisionId, $userId, $startDate, $endDate)
    {
		AuthService::authorize(
			array(
				'rtn_rcpt_export'
			)
		);

        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['RTN_RCPT_SYNC_01'], $userId);

        $syncSettingHdr = SyncSettingHdrRepository::findByProcTypeAndDivisionId(ProcType::$MAP['SYNC_EFICHAIN_01'], $divisionId);
        if(empty($syncSettingHdr))
        {
            $exc = new ApiException(__('RtnRcpt.sync_setting_not_found', ['divisionId'=>$divisionId]));
            //$exc->addData(\App\InbOrdDtl::class, $delDtlData['id']);
            throw $exc;
		}
		
		//query the completed GdsRcpt associated inbOrdHdrs from DB

		//DB::connection()->enableQueryLog();
		$rtnRcptHdrs = RtnRcptHdrRepository::findAllNotExistRtnRcptSync01Txn($divisionId, array(), array(), 20);
		//dd(DB::getQueryLog());
		$postRtnRcpts = array();
		foreach($rtnRcptHdrs as $rtnRcptHdr)
		{
			//this code will use transaction to make sure this document is not duplicate for this process
			$tmpRtnRcptSync01DocTxn = DocTxnFlowRepository::txnFindByProcTypeAndFrHdrId(
				ProcType::$MAP['RTN_RCPT_SYNC_01'], 
				\App\RtnRcptHdr::class, 
				$rtnRcptHdr->id
			);
			if(!empty($tmpRtnRcptSync01DocTxn))
			{
				continue;
			}

			$rtnRcptHdr = self::processOutgoingHeader($rtnRcptHdr);

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletQty = 0;
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$rtnRcptDtls = $rtnRcptHdr->rtnRcptDtls;
			$rtnRcptDtls = $rtnRcptDtls->sortBy('line_no');

			$formatRtnRcptDtls = array();
			foreach($rtnRcptDtls as $rtnRcptDtl)
			{
				$item = $rtnRcptDtl->item;
				
				$rtnRcptDtl = self::processOutgoingDetail($rtnRcptDtl);

				$caseQty = bcadd($caseQty, $rtnRcptDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $rtnRcptDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $rtnRcptDtl->cubic_meter, 8);

				unset($rtnRcptDtl->item);
				$formatRtnRcptDtls[] = $rtnRcptDtl->toArray();
			}

			$rtnRcptHdr->case_qty = $caseQty;
			$rtnRcptHdr->gross_weight = $grossWeight;
			$rtnRcptHdr->cubic_meter = $cubicMeter;

			$rtnRcptHdr->details = $formatRtnRcptDtls;

			$hdrData = $rtnRcptHdr->toArray();
			$postRtnRcpts[] = $hdrData;
		}

        $client = new Client();
        $header = array();

        $usernameSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'username');
        $passwordSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'password');
        $formParams = array(
            'UserLogin[username]' => $usernameSetting->value,
            'UserLogin[password]' => $passwordSetting->value
		);		
		$formParams['RtnRcptHdrs'] = $postRtnRcpts;

		//Log::error($formParams);
        $url = $syncSettingHdr->url.'/index.php?r=luuWu/postReturnReceipts';
            
		$response = $client->request('POST', $url, array('headers' => $header, 'form_params' => $formParams));
		
		$result = $response->getBody()->getContents();
		$result = json_decode($result, true);
		if(empty($result))
		{
			BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

			$exc = new ApiException(__('RtnRcpt.fail_to_sync', ['url'=>$url]));
			//$exc->addData(\App\RtnRcptDtl::class, $delDtlData['id']);
			throw $exc;
		}
		else
		{
			if($result['success'] === false)
			{
				BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

				$exc = new ApiException(__('RtnRcpt.fail_to_sync', ['url'=>$url,'message'=>$result['message']]));
				//$exc->addData(\App\RtnRcptDtl::class, $delDtlData['id']);
				throw $exc;
			}
			else
			{
				//mark the rtnRcpt as success
				$docTxnFlowDataList = array();
				foreach($result['data'] as $successRtnRcpt)
				{
					$tmpDocTxnFlowData = array(
						'fr_doc_hdr_type' => \App\RtnRcptHdr::class,
						'fr_doc_hdr_id' => $successRtnRcpt['id'],
						'fr_doc_hdr_code' => $successRtnRcpt['doc_code'],
						'is_closed' => 1
					);
					$docTxnFlowDataList[] = $tmpDocTxnFlowData;
				}
				DocTxnFlowRepository::createProcess(ProcType::$MAP['RTN_RCPT_SYNC_01'], $docTxnFlowDataList);
			}
		}

		/*
		$successGdsRcptDocCodes = array();
		for ($a = 0; $a < count($result['data']); $a++)
		{
			$successGdsRcptDocCodes[] = $result['data'][$a];
		}
		*/
		$batchJobStatusModel = BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

		return array(
			'data' => $batchJobStatusModel,
			'message' => $result['message']
		);
	}

	public function transitionToStatus($hdrId, $strDocStatus)
    {
        $hdrModel = null;
        if(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['WIP'])
        {
            $hdrModel = self::transitionToWip($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['COMPLETE'])
        {
            $hdrModel = self::transitionToComplete($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['DRAFT'])
        {
            $hdrModel = self::transitionToDraft($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['VOID'])
        {
            $hdrModel = self::transitionToVoid($hdrId);
        }
        
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $message = __('RtnRcpt.document_successfully_transition', ['docCode'=>$documentHeader->doc_code,'strDocStatus'=>$strDocStatus]);
        
        return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>array(),
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	static public function transitionToVoid($hdrId)
    {
		AuthService::authorize(
			array(
				'rtn_rcpt_revert'
			)
		);

		//use transaction to make sure this is latest value
		$hdrModel = RtnRcptHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('RtnRcpt.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\RtnRcptHdr::class, $hdrModel->id);
            throw $exc;
		}

		//revert the document
		$hdrModel = RtnRcptHdrRepository::revertDraftToVoid($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('RtnRcpt.commit_to_void_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\RtnRcptHdr::class, $hdrModel->id);
        throw $exc;
	}

	public function initHeader($divisionId)
    {
		AuthService::authorize(
			array(
				'rtn_rcpt_create'
			)
		);

        $user = Auth::user();

        $model = new RtnRcptHdr();
        $model->doc_flows = array();
        $model->doc_status = DocStatus::$MAP['DRAFT'];
        $model->str_doc_status = DocStatus::$MAP[$model->doc_status];
        $model->doc_code = '';
        $model->ref_code_01 = '';
        $model->ref_code_02 = '';
        $model->doc_date = date('Y-m-d');
        $model->est_del_date = date('Y-m-d');
        $model->desc_01 = '';
        $model->desc_02 = '';

        $division = DivisionRepository::findByPk($divisionId);
        $model->division_id = $divisionId;
        $model->division_code = '';
        $model->site_flow_id = 0;
        $model->company_id = 0;
        $model->company_code = '';
        if(!empty($division))
        {
            $model->division_code = $division->code;
            $model->site_flow_id = $division->site_flow_id;
            $company = $division->company;
            $model->company_id = $company->id;
            $model->company_code = $company->code;
        }

        $model->doc_no_id = 0;
        $docNoIdOptions = array();
        $docNos = DocNoRepository::findAllDivisionDocNo($divisionId, \App\RtnRcptHdr::class, $model->doc_date);
        for($a = 0; $a < count($docNos); $a++)
        {
            $docNo = $docNos[$a];
            if($a == 0)
            {
                $model->doc_no_id = $docNo->id;
            }
            $docNoIdOptions[] = array('value'=>$docNo->id, 'label'=>$docNo->latest_code);
        }
        $model->doc_no_id_options = $docNoIdOptions;

        //currency dropdown
        $model->currency_id = 0;
        $model->currency_rate = 1;
        $initCurrencyOption = array('value'=>0,'label'=>'');
        $model->currency_select2 = $initCurrencyOption;

        //creditTerm select2
        $initCreditTermOption = array('value'=>0,'label'=>'');
        $model->credit_term_select2 = $initCreditTermOption;

        //salesman select2
        $initSalesmanOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($user))
        {
            $initSalesmanOption = array(
                'value'=>$user->id, 
                'label'=>$user->username
            );
        }
        $model->salesman_select2 = $initSalesmanOption;

        //delivery point select2
        $model->delivery_point_unit_no = '';
        $model->delivery_point_building_name = '';
        $model->delivery_point_street_name = '';
        $model->delivery_point_district_01 = '';
        $model->delivery_point_district_02 = '';
        $model->delivery_point_postcode = '';
        $model->delivery_point_state_name = '';
        $model->delivery_point_country_name = '';
        $initDeliveryPointOption = array(
            'value'=>0,
            'label'=>''
        );
        $deliveryPoint = DeliveryPointRepository::findTop();
        if(!empty($deliveryPoint))
        {
            $initDeliveryPointOption = array(
                'value'=>$deliveryPoint->id, 
                'label'=>$deliveryPoint->code.' '.$deliveryPoint->company_name_01
            );

            $model->delivery_point_unit_no = $deliveryPoint->unit_no;
            $model->delivery_point_building_name = $deliveryPoint->building_name;
            $model->delivery_point_street_name = $deliveryPoint->street_name;
            $model->delivery_point_district_01 = $deliveryPoint->district_01;
            $model->delivery_point_district_02 = $deliveryPoint->district_02;
            $model->delivery_point_postcode = $deliveryPoint->postcode;
            $model->delivery_point_state_name = $deliveryPoint->state_name;
            $model->delivery_point_country_name = $deliveryPoint->country_name;
        }
        $model->delivery_point_select2 = $initDeliveryPointOption;

        $model->hdr_disc_val_01 = 0;
        $model->hdr_disc_perc_01 = 0;
        $model->hdr_disc_val_02 = 0;
        $model->hdr_disc_perc_02 = 0;
        $model->hdr_disc_val_03 = 0;
        $model->hdr_disc_perc_03 = 0;
        $model->hdr_disc_val_04 = 0;
        $model->hdr_disc_perc_04 = 0;
        $model->hdr_disc_val_05 = 0;
        $model->hdr_disc_perc_05 = 0;

        $model->disc_amt = 0;
        $model->tax_amt = 0;
        $model->round_adj_amt = 0;
        $model->net_amt = 0;

        return $model;
	}

	public function createHeader($data)
    {		
		AuthService::authorize(
			array(
				'rtn_rcpt_create'
			)
		);

        $data = self::processIncomingHeader($data, true);
        $docNoId = $data['doc_no_id'];
        unset($data['doc_no_id']);
        $hdrModel = RtnRcptHdrRepository::createHeader(ProcType::$MAP['NULL'], $docNoId, $data);

        $message = __('RtnRcpt.document_successfully_created', ['docCode'=>$hdrModel->doc_code]);

		return array(
			'data' => $hdrModel->id,
			'message' => $message
		);
	}

	public function updateHeader($hdrData)
	{
		AuthService::authorize(
			array(
				'rtn_rcpt_update'
			)
		);

        $hdrData = self::processIncomingHeader($hdrData);

		//get the associated inbOrdHdr
        $rtnRcptHdr = RtnRcptHdrRepository::txnFindByPk($hdrData['id']);
        if($rtnRcptHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('RtnRcpt.doc_status_is_wip_or_complete', ['docCode'=>$rtnRcptHdr->doc_code]));
            $exc->addData(\App\RtnRcptHdr::class, $rtnRcptHdr->id);
            throw $exc;
        }
        $rtnRcptHdrData = $rtnRcptHdr->toArray();
        //assign hdrData to model
        foreach($hdrData as $field => $value) 
        {
            if(strpos($field, 'hdr_disc_val') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_disc_perc') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_tax_val') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_tax_perc') !== false)
            {
                continue;
            }
            $rtnRcptHdrData[$field] = $value;
        }

		//$rtnRcptHdr->load('inbOrdHdr', 'deliveryPoint');
        //$deliveryPoint = $rtnRcptHdr->deliveryPoint;
        
        $rtnRcptDtlArray = array();

        $inbOrdHdrData = array();
        $inbOrdDtlArray = array();

        //check if hdr disc or hdr tax is set and more than 0
        $needToProcessDetails = false;
        $firstDtlModel = RtnRcptDtlRepository::findTopByHdrId($hdrData['id']);
        if(!empty($firstDtlModel))
        {
            $firstDtlData = $firstDtlModel->toArray();
            for($a = 1; $a <= 5; $a++)
            {
                if(array_key_exists('hdr_disc_val_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_disc_val_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_disc_val_0'.$a], $firstDtlData['hdr_disc_val_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
                if(array_key_exists('hdr_disc_perc_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_disc_perc_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_disc_perc_0'.$a], $firstDtlData['hdr_disc_perc_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }

                if(array_key_exists('hdr_tax_val_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_tax_val_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_tax_val_0'.$a], $firstDtlData['hdr_tax_val_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
                if(array_key_exists('hdr_tax_perc_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_tax_perc_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_tax_perc_0'.$a], $firstDtlData['hdr_tax_perc_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
            }
        }

        if($needToProcessDetails)
        {
            //query the rtnRcptDtlModels from DB
            //format to array, with is_modified field to indicate it is changed
            $rtnRcptDtlModels = RtnRcptDtlRepository::findAllByHdrId($hdrData['id']);
            foreach($rtnRcptDtlModels as $rtnRcptDtlModel)
            {
                $rtnRcptDtlData = $rtnRcptDtlModel->toArray();
                for($a = 1; $a <= 5; $a++)
                {
                    if(array_key_exists('hdr_disc_val_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_disc_val_0'.$a], 0, 5) > 0)
                    {
                        $rtnRcptDtlData['hdr_disc_val_0'.$a] = $hdrData['hdr_disc_val_0'.$a];
                    }
                    if(array_key_exists('hdr_disc_perc_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_disc_perc_0'.$a], 0, 5) > 0)
                    {
                        $rtnRcptDtlData['hdr_disc_perc_0'.$a] = $hdrData['hdr_disc_perc_0'.$a];
                    }

                    if(array_key_exists('hdr_tax_val_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_tax_val_0'.$a], 0, 5) > 0)
                    {
                        $rtnRcptDtlData['hdr_tax_val_0'.$a] = $hdrData['hdr_tax_val_0'.$a];
                    }
                    if(array_key_exists('hdr_tax_perc_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_tax_perc_0'.$a], 0, 5) > 0)
                    {
                        $rtnRcptDtlData['hdr_tax_perc_0'.$a] = $hdrData['hdr_tax_perc_0'.$a];
                    }
                }
                $rtnRcptDtlData['is_modified'] = 1;
                $rtnRcptDtlArray[] = $rtnRcptDtlData;
            }
            
            //calculate details amount, use adaptive rounding tax
            $result = $this->calculateAmount($rtnRcptHdrData, $rtnRcptDtlArray);
            $rtnRcptHdrData = $result['hdrData'];
            $rtnRcptDtlArray = $result['dtlArray'];

            //need to update the associated inbOrdHdr
            $inbOrdHdr = $rtnRcptHdr->inbOrdHdr;
            if(!empty($inbOrdHdr))
            {
                if($inbOrdHdr->cur_res_type == ResType::$MAP['RTN_RCPT'])
                {
                    $result = $this->tallyInbOrd($rtnRcptHdrData, $rtnRcptDtlArray);
                    $inbOrdHdrData = $result['inbOrdHdrData'];
                    $inbOrdDtlArray = $result['inbOrdDtlArray'];
                }
            }
        }
            
        $result = RtnRcptHdrRepository::updateDetails($rtnRcptHdrData, $rtnRcptDtlArray, array(), $inbOrdHdrData, $inbOrdDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];

        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('RtnRcpt.document_successfully_updated', ['docCode'=>$documentHeader->doc_code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}
}
