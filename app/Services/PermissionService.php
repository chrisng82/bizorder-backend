<?php

namespace App\Services;

use ErrorException;
use App\Repositories\PermissionRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class PermissionService
{
    public static $PERMISSIONS = array(
        //sales order
        array(
            'name' => 'sls_ord_create',
            'display_name' => 'create_sales_order',
            'description' => 'create_sales_order',
        ),
        array(
            'name' => 'sls_ord_read',
            'display_name' => 'read_sales_order',
            'description' => 'read_sales_order',
        ),
        array(
            'name' => 'sls_ord_update',
            'display_name' => 'update_sales_order',
            'description' => 'update_sales_order',
        ),
        array(
            'name' => 'sls_ord_delete',
            'display_name' => 'delete_sales_order',
            'description' => 'delete_sales_order',
        ),     
        array(
            'name' => 'sls_ord_confirm',
            'display_name' => 'confirm_sales_order',
            'description' => 'confirm_sales_order',
        ),
        array(
            'name' => 'sls_ord_revert',
            'display_name' => 'revert_sales_order',
            'description' => 'revert_sales_order',
        ),
        array(
            'name' => 'sls_ord_print',
            'display_name' => 'print_sales_order',
            'description' => 'print_sales_order',
        ),
        array(
            'name' => 'sls_ord_import',
            'display_name' => 'import_sales_order',
            'description' => 'import_sales_order',
        ),
        array(
            'name' => 'sls_ord_export',
            'display_name' => 'export_sales_order',
            'description' => 'export_sales_order',
        ),

        //outbound order
        array(
            'name' => 'outb_ord_create',
            'display_name' => 'create_outbound_order',
            'description' => 'create_outbound_order',
        ),
        array(
            'name' => 'outb_ord_read',
            'display_name' => 'read_outbound_order',
            'description' => 'read_outbound_order',
        ),
        array(
            'name' => 'outb_ord_update',
            'display_name' => 'update_outbound_order',
            'description' => 'update_outbound_order',
        ),
        array(
            'name' => 'outb_ord_delete',
            'display_name' => 'delete_outbound_order',
            'description' => 'delete_outbound_order',
        ),     
        array(
            'name' => 'outb_ord_confirm',
            'display_name' => 'confirm_outbound_order',
            'description' => 'confirm_outbound_order',
        ),
        array(
            'name' => 'outb_ord_revert',
            'display_name' => 'revert_outbound_order',
            'description' => 'revert_outbound_order',
        ),
        array(
            'name' => 'outb_ord_print',
            'display_name' => 'print_outbound_order',
            'description' => 'print_outbound_order',
        ),
        array(
            'name' => 'outb_ord_import',
            'display_name' => 'import_outbound_order',
            'description' => 'import_outbound_order',
        ),
        array(
            'name' => 'outb_ord_export',
            'display_name' => 'export_outbound_order',
            'description' => 'export_outbound_order',
        ),

        //pick list
        array(
            'name' => 'pick_list_create',
            'display_name' => 'create_pick_list',
            'description' => 'create_pick_list',
        ),
        array(
            'name' => 'pick_list_read',
            'display_name' => 'read_pick_list',
            'description' => 'read_pick_list',
        ),
        array(
            'name' => 'pick_list_update',
            'display_name' => 'update_pick_list',
            'description' => 'update_pick_list',
        ),
        array(
            'name' => 'pick_list_delete',
            'display_name' => 'delete_pick_list',
            'description' => 'delete_pick_list',
        ),     
        array(
            'name' => 'pick_list_confirm',
            'display_name' => 'confirm_pick_list',
            'description' => 'confirm_pick_list',
        ),
        array(
            'name' => 'pick_list_revert',
            'display_name' => 'revert_pick_list',
            'description' => 'revert_pick_list',
        ),
        array(
            'name' => 'pick_list_print',
            'display_name' => 'print_pick_list',
            'description' => 'print_pick_list',
        ),
        array(
            'name' => 'pick_list_import',
            'display_name' => 'import_pick_list',
            'description' => 'import_pick_list',
        ),
        array(
            'name' => 'pick_list_export',
            'display_name' => 'export_pick_list',
            'description' => 'export_pick_list',
        ),

        //sales invoice
        array(
            'name' => 'sls_inv_create',
            'display_name' => 'create_sales_invoice',
            'description' => 'create_sales_invoice',
        ),
        array(
            'name' => 'sls_inv_read',
            'display_name' => 'read_sales_invoice',
            'description' => 'read_sales_invoice',
        ),
        array(
            'name' => 'sls_inv_update',
            'display_name' => 'update_sales_invoice',
            'description' => 'update_sales_invoice',
        ),
        array(
            'name' => 'sls_inv_delete',
            'display_name' => 'delete_sales_invoice',
            'description' => 'delete_sales_invoice',
        ),     
        array(
            'name' => 'sls_inv_confirm',
            'display_name' => 'confirm_sales_invoice',
            'description' => 'confirm_sales_invoice',
        ),
        array(
            'name' => 'sls_inv_revert',
            'display_name' => 'revert_sales_invoice',
            'description' => 'revert_sales_invoice',
        ),
        array(
            'name' => 'sls_inv_print',
            'display_name' => 'print_sales_invoice',
            'description' => 'print_sales_invoice',
        ),
        array(
            'name' => 'sls_inv_import',
            'display_name' => 'import_sales_invoice',
            'description' => 'import_sales_invoice',
        ),
        array(
            'name' => 'sls_inv_export',
            'display_name' => 'export_sales_invoice',
            'description' => 'export_sales_invoice',
        ),

        //pack list
        array(
            'name' => 'pack_list_create',
            'display_name' => 'create_pack_list',
            'description' => 'create_pack_list',
        ),
        array(
            'name' => 'pack_list_read',
            'display_name' => 'read_pack_list',
            'description' => 'read_pack_list',
        ),
        array(
            'name' => 'pack_list_update',
            'display_name' => 'update_pack_list',
            'description' => 'update_pack_list',
        ),
        array(
            'name' => 'pack_list_delete',
            'display_name' => 'delete_pack_list',
            'description' => 'delete_pack_list',
        ),     
        array(
            'name' => 'pack_list_confirm',
            'display_name' => 'confirm_pack_list',
            'description' => 'confirm_pack_list',
        ),
        array(
            'name' => 'pack_list_revert',
            'display_name' => 'revert_pack_list',
            'description' => 'revert_pack_list',
        ),
        array(
            'name' => 'pack_list_print',
            'display_name' => 'print_pack_list',
            'description' => 'print_pack_list',
        ),
        array(
            'name' => 'pack_list_import',
            'display_name' => 'import_pack_list',
            'description' => 'import_pack_list',
        ),
        array(
            'name' => 'pack_list_export',
            'display_name' => 'export_pack_list',
            'description' => 'export_pack_list',
        ),

        //load list
        array(
            'name' => 'load_list_create',
            'display_name' => 'create_load_list',
            'description' => 'create_load_list',
        ),
        array(
            'name' => 'load_list_read',
            'display_name' => 'read_load_list',
            'description' => 'read_load_list',
        ),
        array(
            'name' => 'load_list_update',
            'display_name' => 'update_load_list',
            'description' => 'update_load_list',
        ),
        array(
            'name' => 'load_list_delete',
            'display_name' => 'delete_load_list',
            'description' => 'delete_load_list',
        ),     
        array(
            'name' => 'load_list_confirm',
            'display_name' => 'confirm_load_list',
            'description' => 'confirm_load_list',
        ),
        array(
            'name' => 'load_list_revert',
            'display_name' => 'revert_load_list',
            'description' => 'revert_load_list',
        ),
        array(
            'name' => 'load_list_print',
            'display_name' => 'print_load_list',
            'description' => 'print_load_list',
        ),
        array(
            'name' => 'load_list_import',
            'display_name' => 'import_load_list',
            'description' => 'import_load_list',
        ),
        array(
            'name' => 'load_list_export',
            'display_name' => 'export_load_list',
            'description' => 'export_load_list',
        ),

        //goods issue
        array(
            'name' => 'gds_iss_create',
            'display_name' => 'create_goods_issue',
            'description' => 'create_goods_issue',
        ),
        array(
            'name' => 'gds_iss_read',
            'display_name' => 'read_goods_issue',
            'description' => 'read_goods_issue',
        ),
        array(
            'name' => 'gds_iss_update',
            'display_name' => 'update_goods_issue',
            'description' => 'update_goods_issue',
        ),
        array(
            'name' => 'gds_iss_delete',
            'display_name' => 'delete_goods_issue',
            'description' => 'delete_goods_issue',
        ),     
        array(
            'name' => 'gds_iss_confirm',
            'display_name' => 'confirm_goods_issue',
            'description' => 'confirm_goods_issue',
        ),
        array(
            'name' => 'gds_iss_revert',
            'display_name' => 'revert_goods_issue',
            'description' => 'revert_goods_issue',
        ),
        array(
            'name' => 'gds_iss_print',
            'display_name' => 'print_goods_issue',
            'description' => 'print_goods_issue',
        ),
        array(
            'name' => 'gds_iss_import',
            'display_name' => 'import_goods_issue',
            'description' => 'import_goods_issue',
        ),
        array(
            'name' => 'gds_iss_export',
            'display_name' => 'export_goods_issue',
            'description' => 'export_goods_issue',
        ),

        //PRF_DEL
        array(
            'name' => 'prf_del_create',
            'display_name' => 'create_proof_of_delivery',
            'description' => 'create_proof_of_delivery',
        ),
        array(
            'name' => 'prf_del_read',
            'display_name' => 'read_proof_of_delivery',
            'description' => 'read_proof_of_delivery',
        ),
        array(
            'name' => 'prf_del_update',
            'display_name' => 'update_proof_of_delivery',
            'description' => 'update_proof_of_delivery',
        ),
        array(
            'name' => 'prf_del_delete',
            'display_name' => 'delete_proof_of_delivery',
            'description' => 'delete_proof_of_delivery',
        ),     
        array(
            'name' => 'prf_del_confirm',
            'display_name' => 'confirm_proof_of_delivery',
            'description' => 'confirm_proof_of_delivery',
        ),
        array(
            'name' => 'prf_del_revert',
            'display_name' => 'revert_proof_of_delivery',
            'description' => 'revert_proof_of_delivery',
        ),
        array(
            'name' => 'prf_del_print',
            'display_name' => 'print_proof_of_delivery',
            'description' => 'print_proof_of_delivery',
        ),
        array(
            'name' => 'prf_del_import',
            'display_name' => 'import_proof_of_delivery',
            'description' => 'import_proof_of_delivery',
        ),
        array(
            'name' => 'prf_del_export',
            'display_name' => 'export_proof_of_delivery',
            'description' => 'export_proof_of_delivery',
        ),

        //FAIL_DEL
        array(
            'name' => 'fail_del_create',
            'display_name' => 'create_fail_of_delivery',
            'description' => 'create_fail_of_delivery',
        ),
        array(
            'name' => 'fail_del_read',
            'display_name' => 'read_fail_of_delivery',
            'description' => 'read_fail_of_delivery',
        ),
        array(
            'name' => 'fail_del_update',
            'display_name' => 'update_fail_of_delivery',
            'description' => 'update_fail_of_delivery',
        ),
        array(
            'name' => 'fail_del_delete',
            'display_name' => 'delete_fail_of_delivery',
            'description' => 'delete_fail_of_delivery',
        ),     
        array(
            'name' => 'fail_del_confirm',
            'display_name' => 'confirm_fail_of_delivery',
            'description' => 'confirm_fail_of_delivery',
        ),
        array(
            'name' => 'fail_del_revert',
            'display_name' => 'revert_fail_of_delivery',
            'description' => 'revert_fail_of_delivery',
        ),
        array(
            'name' => 'fail_del_print',
            'display_name' => 'print_fail_of_delivery',
            'description' => 'print_fail_of_delivery',
        ),
        array(
            'name' => 'fail_del_import',
            'display_name' => 'import_fail_of_delivery',
            'description' => 'import_fail_of_delivery',
        ),
        array(
            'name' => 'fail_del_export',
            'display_name' => 'export_fail_of_delivery',
            'description' => 'export_fail_of_delivery',
        ),

        //DEL_ORD
        array(
            'name' => 'del_ord_create',
            'display_name' => 'create_delivery_order',
            'description' => 'create_delivery_order',
        ),
        array(
            'name' => 'del_ord_read',
            'display_name' => 'read_delivery_order',
            'description' => 'read_delivery_order',
        ),
        array(
            'name' => 'del_ord_update',
            'display_name' => 'update_delivery_order',
            'description' => 'update_delivery_order',
        ),
        array(
            'name' => 'del_ord_delete',
            'display_name' => 'delete_delivery_order',
            'description' => 'delete_delivery_order',
        ),     
        array(
            'name' => 'del_ord_confirm',
            'display_name' => 'confirm_delivery_order',
            'description' => 'confirm_delivery_order',
        ),
        array(
            'name' => 'del_ord_revert',
            'display_name' => 'revert_delivery_order',
            'description' => 'revert_delivery_order',
        ),
        array(
            'name' => 'del_ord_print',
            'display_name' => 'print_delivery_order',
            'description' => 'print_delivery_order',
        ),
        array(
            'name' => 'del_ord_import',
            'display_name' => 'import_delivery_order',
            'description' => 'import_delivery_order',
        ),
        array(
            'name' => 'del_ord_export',
            'display_name' => 'export_delivery_order',
            'description' => 'export_delivery_order',
        ),

        //PUR_ORD
        array(
            'name' => 'pur_ord_create',
            'display_name' => 'create_purchase_order',
            'description' => 'create_purchase_order',
        ),
        array(
            'name' => 'pur_ord_read',
            'display_name' => 'read_purchase_order',
            'description' => 'read_purchase_order',
        ),
        array(
            'name' => 'pur_ord_update',
            'display_name' => 'update_purchase_order',
            'description' => 'update_purchase_order',
        ),
        array(
            'name' => 'pur_ord_delete',
            'display_name' => 'delete_purchase_order',
            'description' => 'delete_purchase_order',
        ),     
        array(
            'name' => 'pur_ord_confirm',
            'display_name' => 'confirm_purchase_order',
            'description' => 'confirm_purchase_order',
        ),
        array(
            'name' => 'pur_ord_revert',
            'display_name' => 'revert_purchase_order',
            'description' => 'revert_purchase_order',
        ),
        array(
            'name' => 'pur_ord_print',
            'display_name' => 'print_purchase_order',
            'description' => 'print_purchase_order',
        ),
        array(
            'name' => 'pur_ord_import',
            'display_name' => 'import_purchase_order',
            'description' => 'import_purchase_order',
        ),
        array(
            'name' => 'pur_ord_export',
            'display_name' => 'export_purchase_order',
            'description' => 'export_purchase_order',
        ),

        //advanced shipment
        array(
            'name' => 'adv_ship_create',
            'display_name' => 'create_advanced_shipment_notice',
            'description' => 'create_advanced_shipment_notice',
        ),
        array(
            'name' => 'adv_ship_read',
            'display_name' => 'read_advanced_shipment_notice',
            'description' => 'read_advanced_shipment_notice',
        ),
        array(
            'name' => 'adv_ship_update',
            'display_name' => 'update_advanced_shipment_notice',
            'description' => 'update_advanced_shipment_notice',
        ),
        array(
            'name' => 'adv_ship_delete',
            'display_name' => 'delete_advanced_shipment_notice',
            'description' => 'delete_advanced_shipment_notice',
        ),     
        array(
            'name' => 'adv_ship_confirm',
            'display_name' => 'confirm_advanced_shipment_notice',
            'description' => 'confirm_advanced_shipment_notice',
        ),
        array(
            'name' => 'adv_ship_revert',
            'display_name' => 'revert_advanced_shipment_notice',
            'description' => 'revert_advanced_shipment_notice',
        ),
        array(
            'name' => 'adv_ship_print',
            'display_name' => 'print_advanced_shipment_notice',
            'description' => 'print_advanced_shipment_notice',
        ),
        array(
            'name' => 'adv_ship_import',
            'display_name' => 'import_advanced_shipment_notice',
            'description' => 'import_advanced_shipment_notice',
        ),
        array(
            'name' => 'adv_ship_export',
            'display_name' => 'export_advanced_shipment_notice',
            'description' => 'export_advanced_shipment_notice',
        ),

        //goods receipt
        array(
            'name' => 'gds_rcpt_create',
            'display_name' => 'create_goods_receipt',
            'description' => 'create_goods_receipt',
        ),
        array(
            'name' => 'gds_rcpt_read',
            'display_name' => 'read_goods_receipt',
            'description' => 'read_goods_receipt',
        ),
        array(
            'name' => 'gds_rcpt_update',
            'display_name' => 'update_goods_receipt',
            'description' => 'update_goods_receipt',
        ),
        array(
            'name' => 'gds_rcpt_delete',
            'display_name' => 'delete_goods_receipt',
            'description' => 'delete_goods_receipt',
        ),     
        array(
            'name' => 'gds_rcpt_confirm',
            'display_name' => 'confirm_goods_receipt',
            'description' => 'confirm_goods_receipt',
        ),
        array(
            'name' => 'gds_rcpt_revert',
            'display_name' => 'revert_goods_receipt',
            'description' => 'revert_goods_receipt',
        ),
        array(
            'name' => 'gds_rcpt_print',
            'display_name' => 'print_goods_receipt',
            'description' => 'print_goods_receipt',
        ),
        array(
            'name' => 'gds_rcpt_import',
            'display_name' => 'import_goods_receipt',
            'description' => 'import_goods_receipt',
        ),
        array(
            'name' => 'gds_rcpt_export',
            'display_name' => 'export_goods_receipt',
            'description' => 'export_goods_receipt',
        ),

        //put away
        array(
            'name' => 'put_away_create',
            'display_name' => 'create_put_away',
            'description' => 'create_put_away',
        ),
        array(
            'name' => 'put_away_read',
            'display_name' => 'read_put_away',
            'description' => 'read_put_away',
        ),
        array(
            'name' => 'put_away_update',
            'display_name' => 'update_put_away',
            'description' => 'update_put_away',
        ),
        array(
            'name' => 'put_away_delete',
            'display_name' => 'delete_put_away',
            'description' => 'delete_put_away',
        ),     
        array(
            'name' => 'put_away_confirm',
            'display_name' => 'confirm_put_away',
            'description' => 'confirm_put_away',
        ),
        array(
            'name' => 'put_away_revert',
            'display_name' => 'revert_put_away',
            'description' => 'revert_put_away',
        ),
        array(
            'name' => 'put_away_print',
            'display_name' => 'print_put_away',
            'description' => 'print_put_away',
        ),
        array(
            'name' => 'put_away_import',
            'display_name' => 'import_put_away',
            'description' => 'import_put_away',
        ),
        array(
            'name' => 'put_away_export',
            'display_name' => 'export_put_away',
            'description' => 'export_put_away',
        ),

        //cycle count
        array(
            'name' => 'cycle_count_create',
            'display_name' => 'create_cycle_count',
            'description' => 'create_cycle_count',
        ),
        array(
            'name' => 'cycle_count_read',
            'display_name' => 'read_cycle_count',
            'description' => 'read_cycle_count',
        ),
        array(
            'name' => 'cycle_count_update',
            'display_name' => 'update_cycle_count',
            'description' => 'update_cycle_count',
        ),
        array(
            'name' => 'cycle_count_delete',
            'display_name' => 'delete_cycle_count',
            'description' => 'delete_cycle_count',
        ),     
        array(
            'name' => 'cycle_count_confirm',
            'display_name' => 'confirm_cycle_count',
            'description' => 'confirm_cycle_count',
        ),
        array(
            'name' => 'cycle_count_revert',
            'display_name' => 'revert_cycle_count',
            'description' => 'revert_cycle_count',
        ),
        array(
            'name' => 'cycle_count_print',
            'display_name' => 'print_cycle_count',
            'description' => 'print_cycle_count',
        ),
        array(
            'name' => 'cycle_count_import',
            'display_name' => 'import_cycle_count',
            'description' => 'import_cycle_count',
        ),
        array(
            'name' => 'cycle_count_export',
            'display_name' => 'export_cycle_count',
            'description' => 'export_cycle_count',
        ),

        //bin trf
        array(
            'name' => 'bin_trf_create',
            'display_name' => 'create_bin_transfer',
            'description' => 'create_bin_transfer',
        ),
        array(
            'name' => 'bin_trf_read',
            'display_name' => 'read_bin_transfer',
            'description' => 'read_bin_transfer',
        ),
        array(
            'name' => 'bin_trf_update',
            'display_name' => 'update_bin_transfer',
            'description' => 'update_bin_transfer',
        ),
        array(
            'name' => 'bin_trf_delete',
            'display_name' => 'delete_bin_transfer',
            'description' => 'delete_bin_transfer',
        ),     
        array(
            'name' => 'bin_trf_confirm',
            'display_name' => 'confirm_bin_transfer',
            'description' => 'confirm_bin_transfer',
        ),
        array(
            'name' => 'bin_trf_revert',
            'display_name' => 'revert_bin_transfer',
            'description' => 'revert_bin_transfer',
        ),
        array(
            'name' => 'bin_trf_print',
            'display_name' => 'print_bin_transfer',
            'description' => 'print_bin_transfer',
        ),
        array(
            'name' => 'bin_trf_import',
            'display_name' => 'import_bin_transfer',
            'description' => 'import_bin_transfer',
        ),
        array(
            'name' => 'bin_trf_export',
            'display_name' => 'export_bin_transfer',
            'description' => 'export_bin_transfer',
        ),

        //count adj
        array(
            'name' => 'count_adj_create',
            'display_name' => 'create_count_adjustment',
            'description' => 'create_count_adjustment',
        ),
        array(
            'name' => 'count_adj_read',
            'display_name' => 'read_count_adjustment',
            'description' => 'read_count_adjustment',
        ),
        array(
            'name' => 'count_adj_update',
            'display_name' => 'update_count_adjustment',
            'description' => 'update_count_adjustment',
        ),
        array(
            'name' => 'count_adj_delete',
            'display_name' => 'delete_count_adjustment',
            'description' => 'delete_count_adjustment',
        ),     
        array(
            'name' => 'count_adj_confirm',
            'display_name' => 'confirm_count_adjustment',
            'description' => 'confirm_count_adjustment',
        ),
        array(
            'name' => 'count_adj_revert',
            'display_name' => 'revert_count_adjustment',
            'description' => 'revert_count_adjustment',
        ),
        array(
            'name' => 'count_adj_print',
            'display_name' => 'print_count_adjustment',
            'description' => 'print_count_adjustment',
        ),
        array(
            'name' => 'count_adj_import',
            'display_name' => 'import_count_adjustment',
            'description' => 'import_count_adjustment',
        ),
        array(
            'name' => 'count_adj_export',
            'display_name' => 'export_count_adjustment',
            'description' => 'export_count_adjustment',
        ),

        //inbound order
        array(
            'name' => 'inb_ord_create',
            'display_name' => 'create_inbound_order',
            'description' => 'create_inbound_order',
        ),
        array(
            'name' => 'inb_ord_read',
            'display_name' => 'read_inbound_order',
            'description' => 'read_inbound_order',
        ),
        array(
            'name' => 'inb_ord_update',
            'display_name' => 'update_inbound_order',
            'description' => 'update_inbound_order',
        ),
        array(
            'name' => 'inb_ord_delete',
            'display_name' => 'delete_inbound_order',
            'description' => 'delete_inbound_order',
        ),     
        array(
            'name' => 'inb_ord_confirm',
            'display_name' => 'confirm_inbound_order',
            'description' => 'confirm_inbound_order',
        ),
        array(
            'name' => 'inb_ord_revert',
            'display_name' => 'revert_inbound_order',
            'description' => 'revert_inbound_order',
        ),
        array(
            'name' => 'inb_ord_print',
            'display_name' => 'print_inbound_order',
            'description' => 'print_inbound_order',
        ),
        array(
            'name' => 'inb_ord_import',
            'display_name' => 'import_inbound_order',
            'description' => 'import_inbound_order',
        ),
        array(
            'name' => 'inb_ord_export',
            'display_name' => 'export_inbound_order',
            'description' => 'export_inbound_order',
        ),

        //purchase invoice
        array(
            'name' => 'pur_inv_create',
            'display_name' => 'create_purchase_invoice',
            'description' => 'create_purchase_invoice',
        ),
        array(
            'name' => 'pur_inv_read',
            'display_name' => 'read_purchase_invoice',
            'description' => 'read_purchase_invoice',
        ),
        array(
            'name' => 'pur_inv_update',
            'display_name' => 'update_purchase_invoice',
            'description' => 'update_purchase_invoice',
        ),
        array(
            'name' => 'pur_inv_delete',
            'display_name' => 'delete_purchase_invoice',
            'description' => 'delete_purchase_invoice',
        ),     
        array(
            'name' => 'pur_inv_confirm',
            'display_name' => 'confirm_purchase_invoice',
            'description' => 'confirm_purchase_invoice',
        ),
        array(
            'name' => 'pur_inv_revert',
            'display_name' => 'revert_purchase_invoice',
            'description' => 'revert_purchase_invoice',
        ),
        array(
            'name' => 'pur_inv_print',
            'display_name' => 'print_purchase_invoice',
            'description' => 'print_purchase_invoice',
        ),
        array(
            'name' => 'pur_inv_import',
            'display_name' => 'import_purchase_invoice',
            'description' => 'import_purchase_invoice',
        ),
        array(
            'name' => 'pur_inv_export',
            'display_name' => 'export_purchase_invoice',
            'description' => 'export_purchase_invoice',
        ),

        //SLS_RTN
        array(
            'name' => 'sls_rtn_create',
            'display_name' => 'create_sales_return',
            'description' => 'create_sales_return',
        ),
        array(
            'name' => 'sls_rtn_read',
            'display_name' => 'read_sales_return',
            'description' => 'read_sales_return',
        ),
        array(
            'name' => 'sls_rtn_update',
            'display_name' => 'update_sales_return',
            'description' => 'update_sales_return',
        ),
        array(
            'name' => 'sls_rtn_delete',
            'display_name' => 'delete_sales_return',
            'description' => 'delete_sales_return',
        ),     
        array(
            'name' => 'sls_rtn_confirm',
            'display_name' => 'confirm_sales_return',
            'description' => 'confirm_sales_return',
        ),
        array(
            'name' => 'sls_rtn_revert',
            'display_name' => 'revert_sales_return',
            'description' => 'revert_sales_return',
        ),
        array(
            'name' => 'sls_rtn_print',
            'display_name' => 'print_sales_return',
            'description' => 'print_sales_return',
        ),
        array(
            'name' => 'sls_rtn_import',
            'display_name' => 'import_sales_return',
            'description' => 'import_sales_return',
        ),
        array(
            'name' => 'sls_rtn_export',
            'display_name' => 'export_sales_return',
            'description' => 'export_sales_return',
        ),

        //RTN_RCPT
        array(
            'name' => 'rtn_rcpt_create',
            'display_name' => 'create_return_receipt',
            'description' => 'create_return_receipt',
        ),
        array(
            'name' => 'rtn_rcpt_read',
            'display_name' => 'read_return_receipt',
            'description' => 'read_return_receipt',
        ),
        array(
            'name' => 'rtn_rcpt_update',
            'display_name' => 'update_return_receipt',
            'description' => 'update_return_receipt',
        ),
        array(
            'name' => 'rtn_rcpt_delete',
            'display_name' => 'delete_return_receipt',
            'description' => 'delete_return_receipt',
        ),     
        array(
            'name' => 'rtn_rcpt_confirm',
            'display_name' => 'confirm_return_receipt',
            'description' => 'confirm_return_receipt',
        ),
        array(
            'name' => 'rtn_rcpt_revert',
            'display_name' => 'revert_return_receipt',
            'description' => 'revert_return_receipt',
        ),
        array(
            'name' => 'rtn_rcpt_print',
            'display_name' => 'print_return_receipt',
            'description' => 'print_return_receipt',
        ),
        array(
            'name' => 'rtn_rcpt_import',
            'display_name' => 'import_return_receipt',
            'description' => 'import_return_receipt',
        ),
        array(
            'name' => 'rtn_rcpt_export',
            'display_name' => 'export_return_receipt',
            'description' => 'export_return_receipt',
        ),

        //ITEM
        array(
            'name' => 'item_create',
            'display_name' => 'create_item',
            'description' => 'create_item',
        ),
        array(
            'name' => 'item_read',
            'display_name' => 'read_item',
            'description' => 'read_item',
        ),
        array(
            'name' => 'item_update',
            'display_name' => 'update_item',
            'description' => 'update_item',
        ),
        array(
            'name' => 'item_delete',
            'display_name' => 'delete_item',
            'description' => 'delete_item',
        ),     
        array(
            'name' => 'item_confirm',
            'display_name' => 'confirm_item',
            'description' => 'confirm_item',
        ),
        array(
            'name' => 'item_revert',
            'display_name' => 'revert_item',
            'description' => 'revert_item',
        ),
        array(
            'name' => 'item_print',
            'display_name' => 'print_item',
            'description' => 'print_item',
        ),
        array(
            'name' => 'item_import',
            'display_name' => 'import_item',
            'description' => 'import_item',
        ),
        array(
            'name' => 'item_export',
            'display_name' => 'export_item',
            'description' => 'export_item',
        ),

        //USER
        array(
            'name' => 'user_create',
            'display_name' => 'create_user',
            'description' => 'create_user',
        ),
        array(
            'name' => 'user_read',
            'display_name' => 'read_user',
            'description' => 'read_user',
        ),
        array(
            'name' => 'user_update',
            'display_name' => 'update_user',
            'description' => 'update_user',
        ),
        array(
            'name' => 'user_delete',
            'display_name' => 'delete_user',
            'description' => 'delete_user',
        ),     
        array(
            'name' => 'user_confirm',
            'display_name' => 'confirm_user',
            'description' => 'confirm_user',
        ),
        array(
            'name' => 'user_revert',
            'display_name' => 'revert_user',
            'description' => 'revert_user',
        ),
        array(
            'name' => 'user_print',
            'display_name' => 'print_user',
            'description' => 'print_user',
        ),
        array(
            'name' => 'user_import',
            'display_name' => 'import_user',
            'description' => 'import_user',
        ),
        array(
            'name' => 'user_export',
            'display_name' => 'export_user',
            'description' => 'export_user',
        ),

        //HANDLING_UNIT
        array(
            'name' => 'handling_unit_create',
            'display_name' => 'create_handling_unit',
            'description' => 'create_handling_unit',
        ),
        array(
            'name' => 'handling_unit_read',
            'display_name' => 'read_handling_unit',
            'description' => 'read_handling_unit',
        ),
        array(
            'name' => 'handling_unit_update',
            'display_name' => 'update_handling_unit',
            'description' => 'update_handling_unit',
        ),
        array(
            'name' => 'handling_unit_delete',
            'display_name' => 'delete_handling_unit',
            'description' => 'delete_handling_unit',
        ),     
        array(
            'name' => 'handling_unit_confirm',
            'display_name' => 'confirm_handling_unit',
            'description' => 'confirm_handling_unit',
        ),
        array(
            'name' => 'handling_unit_revert',
            'display_name' => 'revert_handling_unit',
            'description' => 'revert_handling_unit',
        ),
        array(
            'name' => 'handling_unit_print',
            'display_name' => 'print_handling_unit',
            'description' => 'print_handling_unit',
        ),
        array(
            'name' => 'handling_unit_import',
            'display_name' => 'import_handling_unit',
            'description' => 'import_handling_unit',
        ),
        array(
            'name' => 'handling_unit_export',
            'display_name' => 'export_handling_unit',
            'description' => 'export_handling_unit',
        ),

        //STORAGE_BIN
        array(
            'name' => 'storage_bin_create',
            'display_name' => 'create_storage_bin',
            'description' => 'create_storage_bin',
        ),
        array(
            'name' => 'storage_bin_read',
            'display_name' => 'read_storage_bin',
            'description' => 'read_storage_bin',
        ),
        array(
            'name' => 'storage_bin_update',
            'display_name' => 'update_storage_bin',
            'description' => 'update_storage_bin',
        ),
        array(
            'name' => 'storage_bin_delete',
            'display_name' => 'delete_storage_bin',
            'description' => 'delete_storage_bin',
        ),     
        array(
            'name' => 'storage_bin_confirm',
            'display_name' => 'confirm_storage_bin',
            'description' => 'confirm_storage_bin',
        ),
        array(
            'name' => 'storage_bin_revert',
            'display_name' => 'revert_storage_bin',
            'description' => 'revert_storage_bin',
        ),
        array(
            'name' => 'storage_bin_print',
            'display_name' => 'print_storage_bin',
            'description' => 'print_storage_bin',
        ),
        array(
            'name' => 'storage_bin_import',
            'display_name' => 'import_storage_bin',
            'description' => 'import_storage_bin',
        ),
        array(
            'name' => 'storage_bin_export',
            'display_name' => 'export_storage_bin',
            'description' => 'export_storage_bin',
        ),
    );

    public function __construct() 
	{
    }

    public function refresh()
	{
        //DB::connection()->enableQueryLog();
        $permissions = PermissionRepository::refresh(self::$PERMISSIONS);
        //Log::error(DB::getQueryLog());

    	return $permissions;
    }
    
    public function index($sorts, $filters = array(), $pageSize = 20)
	{
        //DB::connection()->enableQueryLog();
        $permissions = PermissionRepository::findAll($sorts, $filters, $pageSize);
        //Log::error(DB::getQueryLog());

    	return $permissions;
    }

    static public function processOutgoingModel($model)
	{
        $model->desc_01 = __('Permission.'.$model->name.'_desc_01');
		return $model;
    }

    public function select2($search, $filters)
    {
        //DB::connection()->enableQueryLog();
        $permissions = PermissionRepository::select2($search, $filters);
        //Log::error(DB::getQueryLog());
        foreach($permissions as $permission)
        {
            $permission = self::processOutgoingModel($permission);
        }
        return $permissions;
    }
}
