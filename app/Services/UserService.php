<?php

namespace App\Services;

use App\User;
use App\Services\Env\ProcType;
use App\Services\Env\UserStatus;
use App\Repositories\UserRepository;
use App\Repositories\RoleRepository;
use App\Repositories\DebtorRepository;
use App\Repositories\DivisionRepository;
use App\Repositories\BatchJobStatusRepository;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Imports\UserExcel03Import;
use Maatwebsite\Excel\Facades\Excel;

class UserService 
{
    public function __construct() 
	{
    }
    
    public function select2($search, $filters)
    {
        $users = UserRepository::select2($search, $filters);
        return $users;
    }

    public function select2Init($id)
    {
        $user = null;
        if($id > 0) 
        {
            $user = UserRepository::findByPk($id);
        }
        else
        {
            $user = UserRepository::findTop();
        }
        $option = array('value'=>0, 'label'=>'');
        if(!empty($user))
        {
            $option = array(
                'value'=>$user->id, 
                'label'=>$user->username
            );
        }
        return $option;
    }

    public function uploadProcess($strProcType, $siteFlowId, $divisionId, $file)
    {
        $user = Auth::user();

        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['USER_EXCEL_01']) 
			{
                //process item excel
                $path = Storage::putFileAs('upload/', $file, 'USER_EXCEL_01.XLSX');
				return $this->uploadUserExcel01($siteFlowId, $user->id, $path);
			}
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['USER_EXCEL_03']) 
			{
                //process item excel
                $path = Storage::putFileAs('upload/', $file, 'USER_EXCEL_03.XLSX');
				return $this->uploadUserExcel03($divisionId, $user->id, $path);
			}
		}
    }

    public function uploadUserExcel01($siteFlowId, $userId, $path)
    {
        AuthService::authorize(
            array(
                'user_import'
            )
        );

        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['USER_EXCEL_01'], $userId);

        $siteFlow = SiteFlowRepository::findByPk($siteFlowId);

        $userExcel01Import = new UserExcel01Import($siteFlow->site_id, $userId, $batchJobStatusModel);
        $excel = Excel::import($userExcel01Import, $path);

        $message = __('User.file_successfully_uploaded', ['total'=>$userExcel01Import->getTotal()]);

		return array(
			'data' => $userExcel01Import->getTotal(),
			'message' => $message
		);
    }
    
    public function uploadUserExcel03($divisionId, $userId, $path)
    {
        AuthService::authorize(
            array(
                'user_import'
            )
        );

        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['USER_EXCEL_03'], $userId);

        $division = DivisionRepository::findByPk($divisionId);

        $userExcel03Import = new UserExcel03Import($division->id, $userId, $batchJobStatusModel);
        $excel = Excel::import($userExcel03Import, $path);
        
        $userHashByName = array();
        //process the user role first
        $userDataHash = $userExcel03Import->getUserDataHash();
        foreach($userDataHash as $userName => $userData)
        {
            $user = UserRepository::syncUserSync03($division, $userData);
            $userHashByName[$user->username] = $user;
        }

        $message = __('User.file_successfully_uploaded', ['total'=>$userExcel03Import->getTotal()]);

		return array(
			'data' => $userExcel03Import->getTotal(),
			'message' => $message
		);
    }

    public function indexProcess($strProcType, $sorts, $filters = array(), $pageSize = 20) 
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['USER_LIST_01']) 
			{
				//item listing
				$users = $this->indexUserList01($sorts, $filters, $pageSize);
				return $users;
            }
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['USER_LIST_02']) 
			{
				//item listing
				$users = $this->indexUserList02($sorts, $filters, $pageSize);
				return $users;
			}
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['USER_LIST_03']) 
			{
				//item listing
				$users = $this->indexUserList03($sorts, $filters, $pageSize);
				return $users;
			}
		}
    }
    
    public function indexUserList01($sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
            array(
                'user_read'
            )
        );

        //DB::connection()->enableQueryLog();
        $filters[] = ['field'=>'login_type', 'value'=>'w'];
        $users = UserRepository::findAll($sorts, $filters, $pageSize);
        //Log::error(DB::getQueryLog());
        foreach($users as $user)
        {
            $user->str_status = UserStatus::$MAP[$user->status];
            
            $userDivisions = $user->userDivisions;
            $divisions = array();
            foreach($userDivisions as $userDivision)
            {
                $divisions[] = $userDivision->division;
            }

            $user->divisions = $divisions;

            $roles = $user->roles;
            foreach($roles as $role)
            {
                $role = RoleService::processOutgoingModel($role);
            }
        }

    	return $users;
    }

    public function showRoles($userId) 
	{
        AuthService::authorize(
            array(
                'user_read',
				'user_update'
            )
        );

        $user = UserRepository::findByPk($userId);
        $roles = array();
        if(!empty($user))
        {
            $roles = $user->roles;
            foreach($roles as $role)
            {
                $role = RoleService::processOutgoingModel($role);
            }
        }
        return $roles;
    }

    public function deleteRoles($userId, $roleDataArray)
	{
        AuthService::authorize(
            array(
                'user_update'
            )
        );

        $user = UserRepository::findByPk($userId);
        
		$deletedRoles = array();
        foreach($roleDataArray as $roleData)
        {
            $role = RoleRepository::findByPk($roleData['role_id']);
            if(!empty($role))
            {
                $user->removeRole($role);
                $role = RoleService::processOutgoingModel($role);

                $deletedRoles[] = $role;
            }
        }

        $message = __('User.roles_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'roles'=>array(),
                'deleted_roles'=>$deletedRoles
            ),
			'message' => $message
		);
	}

	public function updateRoles($userId, $roleDataArray)
	{
        AuthService::authorize(
            array(
                'user_update'
            )
        );

        $user = UserRepository::findByPk($userId);
        
        $updatedRoles = array();
        foreach($roleDataArray as $roleData)
        {
            $role = RoleRepository::findByPk($roleData['role_id']);
            if(!empty($role))
            {
                $user->assignRole($role);
                $role = RoleService::processOutgoingModel($role);

                $updatedRoles[] = $role;
            }
        }

        $message = __('User.roles_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'roles'=>$updatedRoles,
                'deleted_roles'=>array()
            ),
			'message' => $message
		);
    }
    
    public function indexUserList02($sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
            array(
                'user_read'
            )
        );

        //DB::connection()->enableQueryLog();
        $filters[] = ['field'=>'login_type', 'value'=>'v'];
        $users = UserRepository::findAll($sorts, $filters, $pageSize);
        //Log::error(DB::getQueryLog());
        foreach($users as $user)
        {
            $user->str_status = UserStatus::$MAP[$user->status];

            $userDivisions = $user->userDivisions;
            $divisions = array();
            foreach($userDivisions as $userDivision)
            {
                $divisions[] = $userDivision->division;
            }

            $user->divisions = $divisions;
        }

    	return $users;
    }

    public function indexUserList03($sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
            array(
                'user_read'
            )
        );

        //DB::connection()->enableQueryLog();
        $filters[] = ['field'=>'login_type', 'value'=>'c'];
        $users = UserRepository::findAll($sorts, $filters, $pageSize);
        //Log::error(DB::getQueryLog());
        foreach($users as $user)
        {
            $user->str_status = UserStatus::$MAP[$user->status];

            if (!empty($user->debtor_id)) {
                $debtor = DebtorRepository::findByPk($user->debtor_id);
                if(!empty($debtor))
                {
                    $user->debtor_code = $debtor->code;
                    $user->debtor_company_name_01 = $debtor->company_name_01;
                    $user->debtor_company_name_02 = $debtor->company_name_02;
                }
                else
                {
                    $user->debtor_code = 'Invalid debtor';
                }
            }

            // $roles = $user->roles;
            // foreach($roles as $role)
            // {
            //     $role = RoleService::processOutgoingModel($role);
            // }
        }

    	return $users;
    }
    
    public function showModel($userId)
    {
        AuthService::authorize(
            array(
                'user_read',
				'user_update'
            )
        );

        $model = UserRepository::findByPk($userId);
		if(!empty($model))
        {
            $model = self::processOutgoingModel($model);
        }
        
        return $model;
    }
    
    static public function processOutgoingModel($model)
	{
        if(empty($model->password_changed_at))
        {
            $model->password_changed_at = '';
        }

        $model->password = '';
        $model->current_password = '';
        $model->new_password = '';
        $model->retype_new_password = '';
        
        if ($model->login_type == 'c') {
            //debtor id select2
            $initDebtorOption = array('value'=>0,'label'=>'');
            $debtor = DebtorRepository::findByPk($model->debtor_id);
            if(!empty($debtor))
            {
                $initDebtorOption = array('value'=>$debtor->id, 'label'=>$debtor->code . ' ' . $debtor->company_name_01);
            }
            $model->debtor_id_select2 = $initDebtorOption;
        }

		return $model;
    }
    
    public function initModel()
    {
        AuthService::authorize(
            array(
                'user_create'
            )
        );

        $model = new User();
        $model->username = '';
        $model->email = '';
        $model->current_password = '';
        $model->status = UserStatus::$MAP['ACTIVE'];
        $model->first_name = '';
        $model->last_name = '';
        $model->new_password = '';
        $model->retype_new_password = '';
        $model->last_login = '';
        $model->password_changed_at = '';

        return $model;
    }

    public function createModel($data)
    {		
        AuthService::authorize(
            array(
                'user_create'
            )
        );

        $data = self::processIncomingModel($data);
        $model = UserRepository::createModel($data);

        //temp to assign all roles to user.
        $roles = RoleRepository::findAll(array());
        foreach ($roles as $role)
        {
            $model->assignRole($role);
        }

        $message = __('User.record_successfully_created', ['docCode'=>$model->username]);

		return array(
            'data' => $model->id,
			'message' => $message
		);
    }
    
    public function updateModel($data)
	{
        AuthService::authorize(
            array(
                'user_update'
            )
        );

        $data = self::processIncomingModel($data);

        $user = UserRepository::txnFindByPk($data['id']);
        $userData = $user->toArray();
        //assign data to model
        foreach($data as $field => $value) 
        {
            $userData[$field] = $value;
        }

        $result = UserRepository::updateModel($userData);
        $model = $result['model'];

        $model = self::processOutgoingModel($model);

        $message = __('User.record_successfully_updated', ['username'=>$model->username]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'model'=>$model
            ),
			'message' => $message
		);
    }
    
    static public function processIncomingModel($data)
    {
        if(array_key_exists('submit_action', $data))
        {
            unset($data['submit_action']);
        }
        
        //preprocess the select2 and dropDown
        if(array_key_exists('debtor_id_select2', $data))
        {
            $data['debtor_id'] = $data['debtor_id_select2']['value'];
            unset($data['debtor_id_select2']);
        }

        if(array_key_exists('new_password', $data))
        {
            unset($data['new_password']);
        }
        if(array_key_exists('retype_new_password', $data))
        {
            unset($data['retype_new_password']);
        }
        if(array_key_exists('current_password', $data))
        {
            $data['password'] = $data['current_password'];
            unset($data['current_password']);
        }
        return $data;
    }
    
    public function changePassword($userId, $newPassword)
    {
        AuthService::authorize(
            array(
                'user_update'
            )
        );
        
        $user = UserRepository::changePassword($userId, $newPassword);
        if ($user !== null) 
        {
            $message = __('User.your_password_has_been_changed_successfully');

            return array(
                'data' => array(
                    'timestamp'=>time(),
                    'model'=>self::processOutgoingModel($user)
                ),
                'message' => $message
            );

        } else {            
            throw new ApiException(__('User.id_not_found', ['id'=>$userId]));
        }
    }

    public function showDivisions($userId) 
	{
        AuthService::authorize(
            array(
                'user_read',
				'user_update'
            )
        );

        $user = UserRepository::findByPk($userId);
        $userDivisions = $user->userDivisions;
        $userDivisions->load(
            'division', 'division.company', 'division.siteFlow'
        );
        foreach($userDivisions as $userDivision)
        {
            $userDivision = UserDivisionService::processOutgoingModel($userDivision);
        }
        return $userDivisions;
    }

    public function deleteDivisions($userId, $userDivisionDataArray)
	{
        AuthService::authorize(
            array(
                'user_update'
            )
        );

        $deleted_divisions = array();
        UserRepository::updateDivisions($userId, array(), $userDivisionDataArray);
        foreach($userDivisionDataArray as $delUserDivisionData)
        {
            $deleted_divisions[] = DivisionRepository::findByPk($delUserDivisionData['division_id']);
        }

        $message = __('User.divisions_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'divisions'=>array(),
                'deleted_divisions'=>$deleted_divisions,
                'deleted_user_divisions'=>$userDivisionDataArray
            ),
			'message' => $message
		);
	}

	public function createDivisions($userId, $dataArray)
	{
        AuthService::authorize(
            array(
                'user_update'
            )
        );

        $userDivisionArray = array();

        foreach($dataArray as $data)
        {
            //assign temp id
            $tmpUuid = uniqid();
            //append NEW here to make sure it will be insert in repository later
            $data['id'] = 'NEW'.$tmpUuid;
            $data['is_modified'] = 1;
            $userDivisionArray[] = $data;
        }

        $result = UserRepository::updateDivisions($userId, $userDivisionArray, array());
        $userDivisionModels = $result['userDivisionModels'];
        $userDivisions = array();
        $divisions = array();
        foreach($userDivisionModels as $userDivisionModel)
        {
            $userDivisions[] = UserDivisionService::processOutgoingModel($userDivisionModel);
            $divisions[] = $userDivisionModel->division;
        }

        $message = __('User.divisions_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'divisions'=>$divisions,
                'user_divisions'=>$userDivisions,
                'deleted_divisions'=>array()
            ),
			'message' => $message
		);
    }
}
