<?php

namespace App\Services;

use App\Services\Env\ProcType;
use App\Services\Env\DocStatus;
use App\Services\Env\WhseJobType;
use App\Repositories\OutbOrdHdrRepository;
use App\Repositories\OutbOrdDtlRepository;
use App\Repositories\PickListHdrRepository;
use App\Repositories\PickListDtlRepository;
use App\Repositories\SiteDocNoRepository;
use App\Repositories\DocTxnFlowRepository;
use App\Repositories\QuantBalTxnRepository;
use App\Repositories\PackListHdrRepository;
use App\Repositories\WhseTxnFlowRepository;
use App\Services\Utils\ApiException;
use App\Services\ItemService;

class PackListService 
{
  public function __construct() 
	{
  }
    
  public function indexProcess($strProcType, $siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['PACK_LIST_01']) 
			{
				//Pick List -> PackList, packed by pick list
				$pickListHdrs = $this->indexPackList01($siteFlowId, $sorts, $filters, $pageSize);
				return $pickListHdrs;
			}
		}
  }
  
	protected function indexPackList01($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
			array(
				'pick_list_read'
			)
		);

		$pickListHdrs = PickListHdrRepository::findAllNotExistPackList01Txn($siteFlowId, DocStatus::$MAP['WIP'], ProcType::$MAP['INV_DOC_01'], $sorts, $filters, $pageSize);
		$pickListHdrs->load('pickListOutbOrds');
		foreach($pickListHdrs as $pickListHdr)
		{
			$pickListHdr->str_doc_status = DocStatus::$MAP[$pickListHdr->doc_status];

			$pickListHdr->unit_qty = 0;
			$pickListHdr->case_qty = 0;
			$pickListHdr->gross_weight = 0;
			$pickListHdr->cubic_meter = 0;
			
			$outbOrdHdrs = array();
			$pickListOutbOrds = $pickListHdr->pickListOutbOrds;
			foreach($pickListOutbOrds as $pickListOutbOrd)
			{
				$outbOrdDtls = OutbOrdDtlRepository::findAllByHdrId($pickListOutbOrd->outb_ord_hdr_id);
				$outbOrdDtls->load('item');
				//calculate the case_qty, gross_weight and cubic_meter
				$outbOrdHdr = OutbOrdHdrRepository::findByPk($pickListOutbOrd->outb_ord_hdr_id);
				$outbOrdHdr->unit_qty = 0;
				$outbOrdHdr->case_qty = 0;
				$outbOrdHdr->gross_weight = 0;
				$outbOrdHdr->cubic_meter = 0;
				foreach($outbOrdDtls as $outbOrdDtl)
				{
					$item = $outbOrdDtl->item;

					$outbOrdDtl = ItemService::processCaseLoose($outbOrdDtl, $item);

					$outbOrdHdr->unit_qty = bcadd($outbOrdHdr->unit_qty, $outbOrdDtl->unit_qty, 8);
					$outbOrdHdr->case_qty = bcadd($outbOrdHdr->case_qty, $outbOrdDtl->case_qty, 8);
					$outbOrdHdr->gross_weight = bcadd($outbOrdHdr->gross_weight, $outbOrdDtl->gross_weight, 8);
					$outbOrdHdr->cubic_meter = bcadd($outbOrdHdr->cubic_meter, $outbOrdDtl->cubic_meter, 8);
				}
				$outbOrdHdrs[] = $outbOrdHdr;

				$pickListHdr->unit_qty = bcadd($pickListHdr->unit_qty, $outbOrdHdr->unit_qty, 8);
				$pickListHdr->case_qty = bcadd($pickListHdr->case_qty, $outbOrdHdr->case_qty, 8);
				$pickListHdr->gross_weight = bcadd($pickListHdr->gross_weight, $outbOrdHdr->gross_weight, 8);
				$pickListHdr->cubic_meter = bcadd($pickListHdr->cubic_meter, $outbOrdHdr->cubic_meter, 8);
			}

			$pickListHdr->details = $outbOrdHdrs;
			unset($pickListHdr['pickListOutbOrds']);
		}

		return $pickListHdrs;
	}

  	public function createProcess($strProcType, $hdrIds, $toStorageBinId) 
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['PACK_LIST_01'])
			{
				$result = $this->createPackList01(ProcType::$MAP[$strProcType], $hdrIds, $toStorageBinId);
				return $result;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['PACK_LIST_02'])
			{
				$result = $this->createPackList02(ProcType::$MAP[$strProcType], $hdrIds, $toStorageBinId);
				return $result;
			}
		}
	}
	
	protected function createPackList01($procType, $hdrIds, $toStorageBinId)
	{
		AuthService::authorize(
			array(
				'pack_list_create'
			)
		);

		$packListHdrs = array();
		//1) 1 Pick List -> 1 PackList
		$pickListHdrs = PickListHdrRepository::findAllByHdrIds($hdrIds);
		$pickListHdrs->load('pickListOutbOrds');
		//2) loop for each pick list
		foreach($pickListHdrs as $pickListHdr)
		{
			//2.1) variables to store the pack list
			$hdrData = array();
			$dtlDataList = array();
			$outbOrdHdrIds = array();
			$docTxnFlowDataList = array();
			$siteFlow = null;
			$lineNo = 1;

			//2.2) get the sign (1) toQuantBalTxns of this pickList
			$toQuantBalTxns = QuantBalTxnRepository::findAllByDocHdrId(\App\PickListHdr::class, $pickListHdr->id, 1);
			//2.3) format to toQuantBalTxnsHashByItemId
			$toQuantBalTxnsHashByItemId = array();
			foreach($toQuantBalTxns as $toQuantBalTxn)
			{
				$tmpToQuantBalTxns = array();
				if(array_key_exists($toQuantBalTxn->item_id, $toQuantBalTxnsHashByItemId))
				{
					$tmpToQuantBalTxns = $toQuantBalTxnsHashByItemId[$toQuantBalTxn->item_id];
				}
				$tmpToQuantBalTxns[] = $toQuantBalTxn;
				$toQuantBalTxnsHashByItemId[$toQuantBalTxn->item_id] = $tmpToQuantBalTxns;
			}

			//2.4) get the picking underlying outbOrdHdrs
			$pickListOutbOrds = $pickListHdr->pickListOutbOrds;
			foreach($pickListOutbOrds as $pickListOutbOrd)
			{
				$outbOrdHdrIds[] = $pickListOutbOrd->outb_ord_hdr_id;
			}

			$outbOrdDtls = OutbOrdDtlRepository::findAllByHdrIds($outbOrdHdrIds);
			$outbOrdDtls->load('outbOrdHdr','item');
			//2.5) format to outbOrdDtlsHashByItemId
			$outbOrdDtlsHashByItemId = array();
			foreach($outbOrdDtls as $outbOrdDtl)
			{
				$outbOrdHdr = $outbOrdDtl->outbOrdHdr;
				if(empty($siteFlow))
				{
					$siteFlow = $outbOrdHdr->siteFlow;
				}
				
				$tmpOutbOrdDtls = array();
				if(array_key_exists($outbOrdDtl->item_id, $outbOrdDtlsHashByItemId))
				{
					$tmpOutbOrdDtls = $outbOrdDtlsHashByItemId[$outbOrdDtl->item_id];
				}
				$tmpOutbOrdDtls[] = $outbOrdDtl;
				$outbOrdDtlsHashByItemId[$outbOrdDtl->item_id] = $tmpOutbOrdDtls;
			}
			
			//2.6) loop each item in outbOrdDtlsHashByItemId, check against toQuantBalTxnsHashByItemId
			//2.6) verify the outbOrdDtlsHashByItemId qty same with toQuantBalTxnsHashByItemId
			//2.6) total the unitQty and build the packList detail
			foreach($outbOrdDtlsHashByItemId as $itemId => $tmpOutbOrdDtls)
			{
				$outbOrdHdr = $outbOrdDtl->outbOrdHdr;

				$item = null;
				$ordTtlUnitQty = 0;
				//2.6.1) calculate the ordTtlUnitQty and get the item details
				foreach($tmpOutbOrdDtls as $tmpOutbOrdDtl) 
				{
					$item = $tmpOutbOrdDtl->item;
					$tmpQty = bcmul($tmpOutbOrdDtl->uom_rate, $tmpOutbOrdDtl->qty, $item->qty_scale);
					$ordTtlUnitQty = bcadd($ordTtlUnitQty, $tmpQty, $item->qty_scale);
				}

				//2.6.2) get the item toQuantBalTxns
				$toQuantBalTxns = array();
				if(array_key_exists($itemId, $toQuantBalTxnsHashByItemId))
				{
					$toQuantBalTxns = $toQuantBalTxnsHashByItemId[$itemId];
				}
				else
				{
					$exc = new ApiException(__('PickList.outb_ord_item_not_tally', ['docCode'=>$pickListHdr->doc_code, 'itemCode'=>$item->code, 'ordUnitQty'=>$ordTtlUnitQty, 'pickUnitQty'=>0]));
					//$exc->addData(\App\PickListHdr::class, $siteFlow->site_id);
					throw $exc;
				}

				$pickTtlUnitQty = 0;
				//2.6.3) calculate the pickTtlUnitQty
				foreach($toQuantBalTxns as $toQuantBalTxn)
				{
					$pickTtlUnitQty = bcadd($pickTtlUnitQty, $toQuantBalTxn->unit_qty, $item->qty_scale);

					//2.6.3.1) build the detail
					$dtlData = array(
						'company_id' => $outbOrdHdr->company_id,
						'item_id' => $item->id,
						'desc_01' => $item->desc_01,
						'desc_02' => $item->desc_02,
						'uom_id' => $item->unit_uom_id,
						'uom_rate' => 1,
						'qty' => $toQuantBalTxn->unit_qty,
						'quant_bal_id' => $toQuantBalTxn->quant_bal_id,
						'to_storage_bin_id' => $toStorageBinId,
						'to_handling_unit_id' => 0,
						'whse_job_type' => WhseJobType::$MAP['NULL'],
						'whse_job_code' => ''
					);
					$dtlDataList[$lineNo] = $dtlData;
		
					$lineNo++;
				}

				//2.6.4) verify the pickTtlUnitQty and ordTtlUnitQty, throw exception is not tally
				if(bccomp($pickTtlUnitQty, $ordTtlUnitQty, $item->qty_scale) != 0)
				{
					$exc = new ApiException(__('PickList.outb_ord_item_not_tally', ['docCode'=>$pickListHdr->doc_code, 'itemCode'=>$item->code, 'ordUnitQty'=>$ordTtlUnitQty, 'pickUnitQty'=>$pickTtlUnitQty]));
					//$exc->addData(\App\PickListHdr::class, $siteFlow->site_id);
					throw $exc;
				}
			}
			
			//2.7) build the hdrData
			$hdrData = array(
				'ref_code_01' => '',
				'ref_code_02' => '',
				'doc_date' => date('Y-m-d'),
				'desc_01' => '',
				'desc_02' => '',
				'site_flow_id' => $siteFlow->id
			);

			$tmpDocTxnFlowData = array(
				'fr_doc_hdr_type' => \App\PickListHdr::class,
				'fr_doc_hdr_id' => $pickListHdr->id,
				'fr_doc_hdr_code' => $pickListHdr->doc_code,
				'is_closed' => 1
			);
			$docTxnFlowDataList[] = $tmpDocTxnFlowData;

			//2.8) get the packing doc no
			$siteDocNo = SiteDocNoRepository::findSiteDocNo($siteFlow->site_id, \App\PackListHdr::class);
			if(empty($siteDocNo))
			{
				$exc = new ApiException(__('SiteDocNo.doc_no_not_found', ['siteId'=>$siteFlow->site_id, 'docType'=>\App\PackListHdr::class]));
				//$exc->addData(\App\SiteDocNo::class, $siteFlow->site_id);
				throw $exc;
			}

			//2.9) query the whseTxnFlow, so know the next step
			$whseTxnFlow = WhseTxnFlowRepository::findByPk($siteFlow->id, $procType);
			
			//2.10) create the packList
			$packListHdr = PackListHdrRepository::createProcess($procType, $siteDocNo->doc_no_id, $hdrData, $dtlDataList, $outbOrdHdrIds, $docTxnFlowDataList);
			
			//2.11) commit packList to WIP/Complete according to whseTxnFlow
			if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
			{
				self::transitionToWip($packListHdr->id);
			}
			elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
			{
				self::transitionToComplete($packListHdr->id);
			}
			
			$packListHdrs[] = $packListHdr;
		}
		
		$docCodeMsg = '';
		for ($a = 0; $a < count($packListHdrs); $a++)
		{
			$packListHdr = $packListHdrs[$a];
			if($a == 0)
			{
				$docCodeMsg .= $packListHdr->doc_code;
			}
			else
			{
				$docCodeMsg .= ', '.$packListHdr->doc_code;
			}
		}
		$message = __('PackList.document_successfully_created', ['docCode'=>$docCodeMsg]);

		return array(
			'data' => $packListHdrs,
			'message' => $message
		);
	}

	static public function transitionToWip($hdrId)
  	{
		//use transaction to make sure this is latest value
		$hdrModel = PackListHdrRepository::txnFindByPk($hdrId);
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status >= DocStatus::$MAP['WIP'])
		{
			$exc = new ApiException(__('PackList.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
			$exc->addData(\App\PackListHdr::class, $hdrModel->id);
			throw $exc;
		}
		
		//assign quantBal if details quant_bal_id is 0
		//format to packListDtlsHashByCompanyIdItemId
		$packListDtlsHashByCompanyIdItemId = array();
		$packListDtls = PackListDtlRepository::findAllByHdrId($hdrModel->id);
		foreach($packListDtls as $packListDtl)
		{
			if($packListDtl->quant_bal_id > 0)
			{
				continue;
			}

			$tmpPackListDtls = array();
			$key = $packListDtl->company_id . '/' . $packListDtl->item_id;
			if(array_key_exists($key, $packListDtlsHashByCompanyIdItemId))
			{
				$tmpPackListDtls = $packListDtlsHashByCompanyIdItemId[$key];
			}
			$tmpPackListDtls[] = $packListDtl;

			$packListDtlsHashByCompanyIdItemId[$key] = $tmpPackListDtls;
		}

		foreach($packListDtlsHashByCompanyIdItemId as $key => $packListDtls)
		{
			$keyParts = explode('/',$key);
			$companyId = $keyParts[0];
			$itemId = $keyParts[1];
			
			self::assignItemPacking($siteFlow, $companyId, $itemId, $packListDtls);
		}
		
		//commit the document
		$hdrModel = PackListHdrRepository::commitToWip($hdrModel->id);
		if(!empty($hdrModel))
		{
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

		$exc = new ApiException(__('LoadList.commit_to_wip_error', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
		throw $exc;
	}
	
	static public function transitionToComplete($hdrId)
  	{
		AuthService::authorize(
			array(
				'pack_list_confirm'
			)
		);

		//use transaction to make sure this is latest value
		$hdrModel = PackListHdrRepository::txnFindByPk($hdrId);

		//only DRAFT or above can transition to COMPLETE
		if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE']
		|| $hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('PackList.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
			$exc->addData(\App\PackListHdr::class, $hdrModel->id);
			throw $exc;
		}

		$dtlModels = PackListDtlRepository::findAllByHdrId($hdrModel->id);
		//preliminary checking
		//check the detail stock make sure the detail to_storage_bin_id and to_handling_unit_id > 0
		foreach($dtlModels as $dtlModel)
		{
			if($dtlModel->to_storage_bin_id == 0)
			{
				$exc = new ApiException(__('PackList.to_storage_bin_is_required', ['lineNo'=>$dtlModel->line_no]));
				$exc->addData(\App\PackListDtl::class, $dtlModel->id);
				throw $exc;
			}

			if($dtlModel->to_handling_unit_id == 0)
			{
				$exc = new ApiException(__('PackList.to_handling_unit_is_required', ['lineNo'=>$dtlModel->line_no]));
				$exc->addData(\App\PackListDtl::class, $dtlModel->id);
				throw $exc;
			}
		}

		//commit the document
		$hdrModel = PackListHdrRepository::commitToComplete($hdrModel->id, false);
		if(!empty($hdrModel))
		{
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		
		$exc = new ApiException(__('PackList.commit_to_complete_error', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\PackListHdr::class, $hdrModel->id);
		throw $exc;
	}

	static public function transitionToDraft($hdrId)
  	{
		//use transaction to make sure this is latest value
		$hdrModel = PackListHdrRepository::txnFindByPk($hdrId);

		//only WIP or COMPLETE can transition to DRAFT
		if($hdrModel->doc_status == DocStatus::$MAP['WIP'])
		{
			$hdrModel = PackListHdrRepository::revertWipToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		elseif($hdrModel->doc_status == DocStatus::$MAP['COMPLETE'])
		{
			AuthService::authorize(
				array(
					'pack_list_revert'
				)
			);

			$hdrModel = PackListHdrRepository::revertCompleteToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		
		$exc = new ApiException(__('PackList.doc_status_is_not_wip_or_complete', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\PackListHdr::class, $hdrModel->id);
		throw $exc;
	}

	public function submitProcess($hdrId)
  	{
		$packListHdr = PackListHdrRepository::txnFindByPk($hdrId);

		$whseTxnFlow = WhseTxnFlowRepository::findByPk($packListHdr->site_flow_id, $packListHdr->proc_type);

		if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
		{
			self::transitionToWip($packListHdr->id);
		}
		elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
		{
			self::transitionToComplete($packListHdr->id);
		}

		return $packListHdr;
	}

	public function completeProcess($hdrId)
  	{
		//used by api engine for testing, should not be access by users
		$packListHdr = PackListHdrRepository::txnFindByPk($hdrId);

		$whseTxnFlow = WhseTxnFlowRepository::findByPk($packListHdr->site_flow_id, $packListHdr->proc_type);

		if($packListHdr->doc_status < DocStatus::$MAP['WIP'])
		{
			$exc = new ApiException(__('PackList.doc_status_is_not_wip', ['docCode'=>$packListHdr->doc_code]));
			$exc->addData(\App\PackListHdr::class, $packListHdr->id);
			throw $exc;
		}
		if($packListHdr->doc_status >= DocStatus::$MAP['COMPLETE'])
		{
			$exc = new ApiException(__('PackList.doc_status_is_complete', ['docCode'=>$packListHdr->doc_code]));
			$exc->addData(\App\PackListHdr::class, $packListHdr->id);
			throw $exc;
		}

		self::transitionToComplete($packListHdr->id);

		return $packListHdr;
	}

	public function revertProcess($hdrId)
  	{
		$packListHdr = PackListHdrRepository::findByPk($hdrId);

		$whseTxnFlow = WhseTxnFlowRepository::findByPk($packListHdr->site_flow_id, $packListHdr->proc_type);

		self::transitionToDraft($packListHdr->id);

		return $packListHdr;
	}

	static public function transitionToVoid($hdrId)
    {
		AuthService::authorize(
			array(
				'pack_list_revert'
			)
		);
		
		//use transaction to make sure this is latest value
		$hdrModel = PackListHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('PackList.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\PackListHdr::class, $hdrModel->id);
            throw $exc;
		}

		//revert the document
		$hdrModel = PackListHdrRepository::revertDraftToVoid($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('PackList.commit_to_void_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\PackListHdr::class, $hdrModel->id);
        throw $exc;
    }
}
