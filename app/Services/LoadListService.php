<?php

namespace App\Services;

use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Repositories\OutbOrdHdrRepository;
use App\Repositories\LoadListHdrRepository;
use App\Repositories\DocTxnFlowRepository;
use App\Repositories\QuantBalTxnRepository;
use App\Services\Utils\ApiException;

class LoadListService 
{
  public function __construct() 
	{
  }
    
  public function indexProcess($strProcType, $siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
    if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['LOAD_LIST_01']) 
			{
				//outbOrds(pickList) -> LoadList
				$outbOrdHdrs = $this->indexLoadList01($siteFlowId, $sorts, $filters = array(), $pageSize = 20);
				return $outbOrdHdrs;
			}
		}
  }
    
  protected function indexLoadList01($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		$outbOrdHdrs = OutbOrdHdrRepository::findAllNotExistLoadList01Txn($siteFlowId, DocStatus::$MAP['COMPLETE'], $sorts, $filters, $pageSize);
    return $outbOrdHdrs;
  }

  public function createProcess($strProcType, $hdrIds, $toStorageBinId) 
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['LOAD_LIST_01'])
			{
				$result = $this->createLoadList01(ProcType::$MAP[$strProcType], $hdrIds, $toStorageBinId);
				return $result;
			}
		}
  }
    
	protected function createLoadList01($procType, $hdrIds, $toStorageBinId)
	{
		//1) M Pack List -> 1 loading list
		//2) variables to store the pack list
		$hdrData = array();
		$dtlDataList = array();
		$docTxnFlowDataList = array();
		$siteFlow = null;
		$lineNo = 1;

		$packListHdrs = PackListHdrRepository::findAllByHdrIds($hdrIds);
		$packListHdrs->load('packListOutbOrds');
		//3) loop for each pack list
		foreach($packListHdrs as $packListHdr)
		{
			//3.1) get the sign (1) toQuantBalTxns of this packList
			$toQuantBalTxns = QuantBalTxnRepository::findAllByDocHdrId(\App\PackListHdr::class, $packListHdr->id, 1);
			//3.2) format to toQuantBalTxnsHashByItemId
			$toQuantBalTxnsHashByItemId = array();
			foreach($toQuantBalTxns as $toQuantBalTxn)
			{
				$tmpToQuantBalTxns = array();
				if(array_key_exists($toQuantBalTxn->item_id, $toQuantBalTxnsHashByItemId))
				{
					$tmpToQuantBalTxns = $toQuantBalTxnsHashByItemId[$toQuantBalTxn->item_id];
				}
				$tmpToQuantBalTxns[] = $toQuantBalTxn;
				$toQuantBalTxnsHashByItemId[$toQuantBalTxn->item_id] = $tmpToQuantBalTxns;
			}

			//3.3) get the pack list underlying outbOrdHdrs
			$tmpOutbOrdHdrIds = array();
			$packListOutbOrds = $packListHdr->packListOutbOrds;
			foreach($packListOutbOrds as $packListOutbOrd)
			{
				$tmpOutbOrdHdrIds[] = $packListOutbOrd->outb_ord_hdr_id;
			}

			$outbOrdDtls = OutbOrdDtlRepository::findAllByHdrIds($tmpOutbOrdHdrIds);
			$outbOrdDtls->load('outbOrdHdr', 'item');
			//3.4) format to outbOrdDtlsHashByItemId
			$outbOrdDtlsHashByItemId = array();
			foreach($outbOrdDtls as $outbOrdDtl)
			{
				$outbOrdHdr = $outbOrdDtl->outbOrdHdr;
				if(empty($siteFlow))
				{
					$siteFlow = $outbOrdHdr->siteFlow;
				}
				
				$tmpOutbOrdDtls = array();
				if(array_key_exists($outbOrdDtl->item_id, $outbOrdDtlsHashByItemId))
				{
					$tmpOutbOrdDtls = $outbOrdDtlsHashByItemId[$outbOrdDtl->item_id];
				}
				$tmpOutbOrdDtls[] = $outbOrdDtl;
				$outbOrdDtlsHashByItemId[$outbOrdDtl->item_id] = $tmpOutbOrdDtls;
			}

			//3.5) loop each item in outbOrdDtlsHashByItemId, check against toQuantBalTxnsHashByItemId
			//3.5) verify the outbOrdDtlsHashByItemId qty same with toQuantBalTxnsHashByItemId
			foreach($outbOrdDtlsHashByItemId as $itemId => $tmpOutbOrdDtls)
			{
				$outbOrdHdr = $outbOrdDtl->outbOrdHdr;

				$item = null;
				$ordTtlUnitQty = 0;
				//3.5.1) calculate the ordTtlUnitQty and get the item details
				foreach($tmpOutbOrdDtls as $tmpOutbOrdDtl) 
				{
					$tmpOutbOrdDtl->alloc_unit_qty = 0;
					$item = $tmpOutbOrdDtl->item;
					$tmpQty = bcmul($tmpOutbOrdDtl->uom_rate, $tmpOutbOrdDtl->qty, $item->qty_scale);
					$ordTtlUnitQty = bcadd($ordTtlUnitQty, $tmpQty, $item->qty_scale);
				}

				//3.5.2) get the item toQuantBalTxns
				$toQuantBalTxns = array();
				if(array_key_exists($itemId, $toQuantBalTxnsHashByItemId))
				{
					$toQuantBalTxns = $toQuantBalTxnsHashByItemId[$itemId];
				}
				else
				{
					$exc = new ApiException(__('PackList.outb_ord_item_not_tally', ['docCode'=>$pickListHdr->doc_code, 'itemCode'=>$item->code, 'ordUnitQty'=>$ordTtlUnitQty, 'packUnitQty'=>0]));
					//$exc->addData(\App\PickListHdr::class, $siteFlow->site_id);
					throw $exc;
				}

				$packTtlUnitQty = 0;
				//3.5.3) calculate the packTtlUnitQty
				foreach($toQuantBalTxns as $toQuantBalTxn)
				{
					$toQuantBalTxn->alloc_unit_qty = 0;
					$packTtlUnitQty = bcadd($packTtlUnitQty, $toQuantBalTxn->unit_qty, $item->qty_scale);
				}

				//3.5.4) verify the packTtlUnitQty and ordTtlUnitQty, throw exception is not tally
				if(bccomp($packTtlUnitQty, $ordTtlUnitQty, $item->qty_scale) != 0)
				{
					$exc = new ApiException(__('PackList.outb_ord_item_not_tally', ['docCode'=>$packListHdr->doc_code, 'itemCode'=>$item->code, 'ordUnitQty'=>$ordTtlUnitQty, 'packUnitQty'=>$packTtlUnitQty]));
					//$exc->addData(\App\PickListHdr::class, $siteFlow->site_id);
					throw $exc;
				}

				//3.5.5) loop the item's outbOrdDtls and build the detail based on toQuantBalTxns
				foreach($tmpOutbOrdDtls as $tmpOutbOrdDtl) 
				{
					$tmpItem = $tmpOutbOrdDtl->item;
					$tmpOutbOrdHdr = $tmpOutbOrdDtl->outbOrdHdr;
					$ordUnitQty = bcmul($tmpOutbOrdDtl->uom_rate, $tmpOutbOrdDtl->qty, $item->qty_scale);
					$ordAvailUnitQty = bcsub($ordUnitQty, $tmpOutbOrdDtl->alloc_unit_qty, 10);
					if(bccomp($ordAvailUnitQty, 0, 10) <= 0)
					{
						continue;
					}

					foreach($toQuantBalTxns as $toQuantBalTxn)
					{
						$toQuantAvailUnitQty = bcsub($toQuantBalTxn->unit_qty, $toQuantBalTxn->alloc_unit_qty, 10);
						if(bccomp($toQuantAvailUnitQty, 0, 10) <= 0)
						{
							continue;
						}
						
						if(bccomp($toQuantAvailUnitQty, $ordAvailUnitQty, 10) >= 0)
						{
							$tmpOutbOrdDtl->alloc_unit_qty = $ordAvailUnitQty;
							$toQuantBalTxn->alloc_unit_qty = $ordAvailUnitQty;
							//3.5.5.1 quantBal enough
							$dtlData = array(
								'outb_ord_hdr_id' => $tmpOutbOrdHdr->id,
								'outb_ord_dtl_id' => $tmpOutbOrdDtl->id,
								'company_id' => $tmpOutbOrdHdr->company_id,
								'item_id' => $tmpOutbOrdDtl->item_id,
								'desc_01' => $tmpOutbOrdDtl->desc_01,
								'desc_02' => $tmpOutbOrdDtl->desc_02,
								'uom_id' => $tmpItem->unit_uom_id,
								'uom_rate' => 1,
								'qty' => $ordAvailUnitQty,
								'quant_bal_id' => $toQuantBalTxn->quant_bal_id,
								'to_storage_bin_id' => $toStorageBinId,
								'to_handling_unit_id' => 0,
								'whse_job_type' => WhseJobType::$MAP['NULL'],
								'whse_job_code' => ''
							);
							$dtlDataList[$lineNo] = $dtlData;
			
							$lineNo++;
						}
						else
						{
							$tmpOutbOrdDtl->alloc_unit_qty = $toQuantBalTxn->unit_qty;
							$toQuantBalTxn->alloc_unit_qty = $toQuantBalTxn->unit_qty;
							//3.5.5.2 quantBal not enough
							$dtlData = array(
								'outb_ord_hdr_id' => $tmpOutbOrdHdr->id,
								'outb_ord_dtl_id' => $tmpOutbOrdDtl->id,
								'company_id' => $tmpOutbOrdHdr->company_id,
								'item_id' => $tmpOutbOrdDtl->item_id,
								'desc_01' => $tmpOutbOrdDtl->desc_01,
								'desc_02' => $tmpOutbOrdDtl->desc_02,
								'uom_id' => $tmpItem->unit_uom_id,
								'uom_rate' => 1,
								'qty' => $toQuantBalTxn->unit_qty,
								'quant_bal_id' => $toQuantBalTxn->quant_bal_id,
								'to_storage_bin_id' => $toStorageBinId,
								'to_handling_unit_id' => 0,
								'whse_job_type' => WhseJobType::$MAP['NULL'],
								'whse_job_code' => ''
							);
							$dtlDataList[$lineNo] = $dtlData;
			
							$lineNo++;
						}
					}
				}
			}

			//3.6) build the docTxnFlow list
			$tmpDocTxnFlowData = array(
				'fr_doc_hdr_type' => \App\PackListHdr::class,
				'fr_doc_hdr_id' => $packListHdr->id,
				'fr_doc_hdr_code' => $packListHdr->doc_code,
				'is_closed' => 1
			);
			$docTxnFlowDataList[] = $tmpDocTxnFlowData;
		}

		//4) build the hdrData
		$hdrData = array(
			'ref_code_01' => '',
			'ref_code_02' => '',
			'doc_date' => date('Y-m-d'),
			'desc_01' => '',
			'desc_02' => '',
			'site_flow_id' => $siteFlow->id
		);

		//5) get the loadList doc no
		$siteDocNo = SiteDocNoRepository::findSiteDocNo($siteFlow->site_id, \App\LoadListHdr::class);
		if(empty($siteDocNo))
		{
			$exc = new ApiException(__('SiteDocNo.doc_no_not_found', ['siteId'=>$siteFlow->site_id, 'docType'=>\App\LoadListHdr::class]));
			//$exc->addData(\App\LoadListHdr::class, $siteFlow->site_id);
			throw $exc;
		}

		//6) query the whseTxnFlow, so know the next step
		$whseTxnFlow = WhseTxnFlowRepository::findByPk($siteFlow->id, $procType);

		//7) create the loadList
		$loadListHdr = LoadListHdrRepository::createProcess($procType, $siteDocNo->doc_no_id, $hdrData, $dtlDataList, $docTxnFlowDataList);

		//8) commit packList to WIP/Complete according to whseTxnFlow
		if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
		{
			self::transitionToWip($loadListHdr->id);
		}
		elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
		{
			self::transitionToComplete($loadListHdr->id);
		}

		$message = __('LoadList.document_successfully_created', ['docCode'=>$loadListHdr->doc_code]);

		return array(
			'data' => $loadListHdr,
			'message' => $message
		);
	}
    
  static public function transitionToWip($hdrId)
  {
		//use transaction to make sure this is latest value
		$hdrModel = LoadListHdrRepository::txnFindByPk($hdrId);
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status >= DocStatus::$MAP['WIP'])
		{
			$exc = new ApiException(__('LoadList.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
			$exc->addData(\App\LoadListHdr::class, $hdrModel->id);
			throw $exc;
    }
		
		//commit the document
		$hdrModel = LoadListHdrRepository::commitToWip($loadListHdr->id);
		if(!empty($loadListHdr))
		{
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

		$exc = new ApiException(__('LoadList.commit_to_wip_error', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\LoadListHdr::class, $hdrModel->id);
		throw $exc;
	}

	static public function transitionToComplete($hdrId)
  {
		//use transaction to make sure this is latest value
		$hdrModel = LoadListHdrRepository::txnFindByPk($hdrId);

		//only DRAFT or above can transition to COMPLETE
		if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE']
		|| $hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('LoadList.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
			$exc->addData(\App\LoadListHdr::class, $hdrModel->id);
			throw $exc;
		}

		$dtlModels = LoadListDtlRepository::findAllByHdrId($hdrModel->id);
		//preliminary checking
		//check the detail stock make sure the detail to_storage_bin_id and to_handling_unit_id > 0
		foreach($dtlModels as $dtlModel)
		{
			//loadList the to_storage_bin_id and to_handling_unit_id is not used
			if($dtlModel->to_storage_bin_id == 0)
			{
				$exc = new ApiException(__('LoadList.to_storage_bin_is_required', ['lineNo'=>$dtlModel->line_no]));
				$exc->addData(\App\LoadListDtl::class, $dtlModel->id);
				throw $exc;
			}
		}

		//commit the document
		$hdrModel = LoadListHdrRepository::commitToComplete($hdrModel->id, false);
		if(!empty($hdrModel))
		{
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			$hdrModel = true;
		}
		
		$exc = new ApiException(__('LoadList.commit_to_complete_error', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\LoadListHdr::class, $hdrModel->id);
		throw $exc;
	}

	static public function transitionToDraft($hdrId)
  {
		//use transaction to make sure this is latest value
		$hdrModel = LoadListHdrRepository::txnFindByPk($hdrId);

		//only WIP or COMPLETE can transition to DRAFT
		if($hdrModel->doc_status == DocStatus::$MAP['WIP'])
		{
			$hdrModel = LoadListHdrRepository::revertWipToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		elseif($hdrModel->doc_status == DocStatus::$MAP['COMPLETE'])
		{
			$hdrModel = LoadListHdrRepository::revertCompleteToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		
		$exc = new ApiException(__('LoadList.doc_status_is_not_wip_or_complete', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\LoadListHdr::class, $hdrModel->id);
		throw $exc;
	}

	public function revertProcess($hdrId)
  {
		$loadListHdr = LoadListHdrRepository::findByPk($hdrId);

		$whseTxnFlow = WhseTxnFlowRepository::findByPk($loadListHdr->site_flow_id, $loadListHdr->proc_type);

		self::transitionToDraft($loadListHdr->id);

		return $loadListHdr;
	}

	public function completeProcess($hdrId)
  {
		//used by api engine for testing, should not be access by users
		$loadListHdr = LoadListHdrRepository::txnFindByPk($hdrId);

		$whseTxnFlow = WhseTxnFlowRepository::findByPk($loadListHdr->site_flow_id, $loadListHdr->proc_type);

		if($loadListHdr->doc_status < DocStatus::$MAP['WIP'])
		{
			$exc = new ApiException(__('LoadList.doc_status_is_not_wip', ['docCode'=>$loadListHdr->doc_code]));
			$exc->addData(\App\LoadListHdr::class, $loadListHdr->id);
			throw $exc;
		}
		if($loadListHdr->doc_status >= DocStatus::$MAP['COMPLETE'])
		{
			$exc = new ApiException(__('LoadList.doc_status_is_complete', ['docCode'=>$loadListHdr->doc_code]));
			$exc->addData(\App\LoadListHdr::class, $loadListHdr->id);
			throw $exc;
		}

		self::transitionToComplete($loadListHdr->id);

		return $loadListHdr;
	}

	public function submitProcess($hdrId)
  {
		$loadListHdr = LoadListHdrRepository::txnFindByPk($hdrId);

		$whseTxnFlow = WhseTxnFlowRepository::findByPk($loadListHdr->site_flow_id, $loadListHdr->proc_type);

		if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
		{
			self::transitionToWip($loadListHdr->id);
		}
		elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
		{
			self::transitionToComplete($loadListHdr->id);
		}

		return $loadListHdr;
	}
}
