<?php

namespace App\Services;

use App\OutbOrdHdr;
use App\OutbOrdDtl;
use App\PickListHdr;
use App\Uom;
use App\Services\Env\BinType;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Services\Env\WhseJobType;
use App\Repositories\SiteDocNoRepository;
use App\Repositories\OutbOrdHdrRepository;
use App\Repositories\OutbOrdDtlRepository;
use App\Repositories\PickListHdrRepository;
use App\Repositories\PickListDtlRepository;
use App\Repositories\PickFaceStrategyRepository;
use App\Repositories\QuantBalRepository;
use App\Repositories\QuantBalRsvdTxnRepository;
use App\Repositories\StorageBinRepository;
use App\Repositories\WhseTxnFlowRepository;
use App\Repositories\WhseJobHdrRepository;
use App\Repositories\DocTxnFlowRepository;
use App\Repositories\AreaRepository;
use App\Repositories\BatchJobStatusRepository;
use App\Repositories\SyncSettingHdrRepository;
use App\Repositories\PickListOutbOrdRepository;
use App\Repositories\SyncSettingDtlRepository;
use App\Repositories\ItemRepository;
use App\Repositories\ItemUomRepository;
use App\Repositories\PrintDocTxnRepository;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PickListService extends InventoryService
{
	/**
	 */
	public function __construct() 
	{
    }

	public function indexProcess($strProcType, $siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
		$user = Auth::user();

		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['PICK_LIST_01']) 
			{
				//outbound order -> picking list, by whole OutbOrd
				$outbOrdHdrs = $this->indexPickList01($user, $siteFlowId, $sorts, $filters, $pageSize);
				return $outbOrdHdrs;
			}
		}
	}
	
	protected function indexPickList01($user, $siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
            array(
                'outb_ord_read'
            )
		);
		
		$divisionIds = array();
		$userDivisions = $user->userDivisions;
		foreach($userDivisions as $userDivision)
		{
			$divisionIds[] = $userDivision->division_id;
		}

		//DB::connection()->enableQueryLog();
		$outbOrdHdrs = OutbOrdHdrRepository::findAllNotExistPickList01Txn($divisionIds, DocStatus::$MAP['COMPLETE'], $sorts, $filters, $pageSize);
		//Log::error(DB::getQueryLog());
		$outbOrdHdrs->load(
			'division', 'deliveryPoint', 'salesman',
			'outbOrdDtls', 'outbOrdDtls.item'
		);
		//query the docDtls, and format into group details
		foreach($outbOrdHdrs as $outbOrdHdr)
		{
			$outbOrdHdr->str_doc_status = DocStatus::$MAP[$outbOrdHdr->doc_status];

			$division = $outbOrdHdr->division;
			$outbOrdHdr->division_code = $division->code;

			$deliveryPoint = $outbOrdHdr->deliveryPoint;
			$outbOrdHdr->delivery_point_code = $deliveryPoint->code;
			$outbOrdHdr->delivery_point_company_name_01 = $deliveryPoint->company_name_01;
			$outbOrdHdr->delivery_point_area_code = $deliveryPoint->area_code;
			$area = AreaRepository::findByPk($deliveryPoint->area_id);
			$outbOrdHdr->delivery_point_area_desc_01 = $area->desc_01;

			$salesman = $outbOrdHdr->salesman;
			$outbOrdHdr->salesman_username = $salesman->username;

			$outbOrdDtls = $outbOrdHdr->outbOrdDtls;
			//calculate the case_qty, gross_weight and cubic_meter
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			foreach($outbOrdDtls as $outbOrdDtl)
			{
				$item = $outbOrdDtl->item;
				$outbOrdDtl = OutbOrdService::processOutgoingDetail($outbOrdDtl);

				$outbOrdDtl->case_qty = round($outbOrdDtl->case_qty, 8);
				$outbOrdDtl->gross_weight = round($outbOrdDtl->gross_weight, 8);
				$outbOrdDtl->cubic_meter = round($outbOrdDtl->cubic_meter, 8);
				$caseQty = bcadd($caseQty, $outbOrdDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $outbOrdDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $outbOrdDtl->cubic_meter, 8);
			}

			$outbOrdHdr->case_qty = $caseQty;
			$outbOrdHdr->gross_weight = $grossWeight;
			$outbOrdHdr->cubic_meter = $cubicMeter;
			$outbOrdHdr->details = $outbOrdDtls;

			//unset the item, so it wont send in json
			unset($outbOrdHdr->division);
			unset($outbOrdHdr->deliveryPoint);
			unset($outbOrdHdr->salesman);
		}
        return $outbOrdHdrs;
	}
    
	public function createProcess($strProcType, $hdrIds, $toStorageBinId) 
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['PICK_LIST_01'])
			{
				$result = $this->createPickList01(ProcType::$MAP[$strProcType], $hdrIds, $toStorageBinId);
				return $result;
			}
		}
	}

	protected function createPickList01($procType, $hdrIds, $toStorageBinId)
    {
		AuthService::authorize(
            array(
                'pick_list_create'
            )
		);

		$pickListHdrs = array();

		$outbOrdDtlsHashByLocationId = array();
		$outbOrdHdrHashByHdrId = array();

		$siteFlow = null;

		$allOutbOrdDtls = OutbOrdDtlRepository::findAllByHdrIds($hdrIds);
		$allOutbOrdDtls->load('outbOrdHdr', 'item', 'outbOrdHdr.siteFlow');
		foreach($allOutbOrdDtls as $allOutbOrdDtl) 
		{
			$outbOrdHdr = $allOutbOrdDtl->outbOrdHdr;
			$siteFlow = $outbOrdHdr->siteFlow;

			//check the frDocHdr to make sure it is not closed
			$tmpDocTxnFlow = DocTxnFlowRepository::findByProcTypeAndFrHdrId($procType, \App\OutbOrdHdr::class, $outbOrdHdr->id, 1);
			if(!empty($tmpDocTxnFlow))
			{
				$exc = new ApiException(__('PickList.fr_doc_is_closed', ['docType'=>$tmpDocTxnFlow->fr_doc_hdr_type, 'docCode'=>$tmpDocTxnFlow->fr_doc_hdr_code]));
				$exc->addData($tmpDocTxnFlow->fr_doc_hdr_type, $tmpDocTxnFlow->fr_doc_hdr_code);
				throw $exc;
			}

			$tmpOutbOrdDtls = array();
			if(array_key_exists($allOutbOrdDtl->location_id, $outbOrdDtlsHashByLocationId))
			{
				$tmpOutbOrdDtls = $outbOrdDtlsHashByLocationId[$allOutbOrdDtl->location_id];
			}
			$tmpOutbOrdDtls[] = $allOutbOrdDtl;

			$outbOrdDtlsHashByLocationId[$allOutbOrdDtl->location_id] = $tmpOutbOrdDtls;

			$outbOrdHdrHashByHdrId[$allOutbOrdDtl->hdr_id] = $outbOrdHdr;
		}

		$a = 0;
		foreach($outbOrdDtlsHashByLocationId as $locationId => $locOutbOrdDtls)
		{
			$hdrData = array();
			$dtlDataList = array();
			$outbOrdHdrIds = array();
			$docTxnFlowDataList = array();
			
			$lineNo = 1;
			$outbOrdDtlsHashByItemId = array();
			//calculate the rqdUnitQty and get the item details
			foreach($locOutbOrdDtls as $locOutbOrdDtl) 
			{
				$tmpOutbOrdDtls = array();
				if(array_key_exists($locOutbOrdDtl->item_id, $outbOrdDtlsHashByItemId))
				{
					$tmpOutbOrdDtls = $outbOrdDtlsHashByItemId[$locOutbOrdDtl->item_id];
				}
				$tmpOutbOrdDtls[] = $locOutbOrdDtl;

				$outbOrdDtlsHashByItemId[$locOutbOrdDtl->item_id] = $tmpOutbOrdDtls;
			}

			//loop the dtls to generate pick list
			foreach($outbOrdDtlsHashByItemId as $itemId => $itemOutbOrdDtls)
			{
				$outbOrdHdr = null;
				$deliveryPointIdHash = array();
				$item = null;
				$ttUnitQty = 0;
				//calculate the rqdUnitQty and get the item details
				foreach($itemOutbOrdDtls as $itemOutbOrdDtl) 
				{
					$outbOrdHdr = $itemOutbOrdDtl->outbOrdHdr;
					$deliveryPointIdHash[$outbOrdHdr->delivery_point_id] = $outbOrdHdr->delivery_point_id;
					$item = $itemOutbOrdDtl->item;
					$tmpQty = bcmul($itemOutbOrdDtl->uom_rate, $itemOutbOrdDtl->qty, $item->qty_scale);
					$ttUnitQty = bcadd($ttUnitQty, $tmpQty, $item->qty_scale);
				}

				$isSplit = 0;
				if(count($deliveryPointIdHash) > 1)
				{
					$isSplit = 1;
				}

				$dtlData = array(
					'line_no' => $lineNo,
					'is_split' => $isSplit,
					'company_id' => $outbOrdHdr->company_id,
					'item_id' => $item->id,
					'desc_01' => $item->desc_01,
					'desc_02' => $item->desc_02,
					'uom_id' => $item->unit_uom_id,
					'uom_rate' => 1,
					'qty' => $ttUnitQty,
					'storage_bin_id' => 0,
					'quant_bal_id' => 0,
					'to_storage_bin_id' => $toStorageBinId,
					'to_handling_unit_id' => 0,
					'whse_job_type' => WhseJobType::$MAP['NULL'],
					'whse_job_code' => ''
				);
				$dtlDataList[$lineNo] = $dtlData;

				$lineNo++;
			}

			//build the hdrData
			$hdrData = array(
				'ref_code_01' => '',
				'ref_code_02' => '',
				'doc_date' => date('Y-m-d'),
				'desc_01' => '',
				'desc_02' => '',
				'site_flow_id' => $siteFlow->id,
				'location_id' => $locationId,
			);

			foreach($outbOrdHdrHashByHdrId as $hdrId => $outbOrdHdr)
			{
				$tmpDocTxnFlowData = array(
					'fr_doc_hdr_type' => \App\OutbOrdHdr::class,
					'fr_doc_hdr_id' => $outbOrdHdr->id,
					'fr_doc_hdr_code' => $outbOrdHdr->doc_code,
					'is_closed' => $a == count($outbOrdDtlsHashByLocationId) - 1 ? 1 : 0
				);
				$docTxnFlowDataList[] = $tmpDocTxnFlowData;
				
				$outbOrdHdrIds[] = $outbOrdHdr->id;
			}

			$siteDocNo = SiteDocNoRepository::findSiteDocNo($siteFlow->site_id, \App\PickListHdr::class);
			if(empty($siteDocNo))
			{
				$exc = new ApiException(__('SiteDocNo.doc_no_not_found', ['siteId'=>$siteFlow->site_id, 'docType'=>\App\PickListHdr::class]));
				//$exc->addData(\App\SiteDocNo::class, $siteFlow->site_id);
				throw $exc;
			}

			$whseTxnFlow = WhseTxnFlowRepository::findByPk($siteFlow->id, $procType);

			//DRAFT
			$pickListHdr = PickListHdrRepository::createProcess($procType, $siteDocNo->doc_no_id, $hdrData, $dtlDataList, $outbOrdHdrIds, $docTxnFlowDataList);

			if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
			{
				$pickListHdr = self::transitionToWip($pickListHdr->id);
			}
			elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
			{
				$pickListHdr = self::transitionToComplete($pickListHdr->id);
			}

			$pickListHdrs[] = $pickListHdr;

			$a++;
		}

		$docCodeMsg = '';
        for ($a = 0; $a < count($pickListHdrs); $a++)
        {
            $pickListHdr = $pickListHdrs[$a];
            if($a == 0)
            {
                $docCodeMsg .= $pickListHdr->doc_code;
            }
            else
            {
                $docCodeMsg .= ', '.$pickListHdr->doc_code;
            }
        }
		$message = __('PickList.document_successfully_created', ['docCode'=>$docCodeMsg]);

		return array(
			'data' => $pickListHdrs,
			'message' => $message
		);
	}

	static public function transitionToWip($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = PickListHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('PickList.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\PickListHdr::class, $hdrModel->id);
            throw $exc;
		}

		//reserve stock if details quant_bal_id is 0
		//format to pickListDtlsHashByCompanyIdItemId
		$pickListDtlsHashByCompanyIdItemId = array();
		$pickListDtls = PickListDtlRepository::findAllByHdrId($hdrModel->id);
		$pickListDtls->load('pickListHdr', 'item');
		foreach($pickListDtls as $pickListDtl)
		{
			if($pickListDtl->quant_bal_id > 0)
			{
				continue;
			}

			$tmpPickListDtls = array();
			$key = $pickListDtl->company_id . '/' . $pickListDtl->item_id;
			if(array_key_exists($key, $pickListDtlsHashByCompanyIdItemId))
			{
				$tmpPickListDtls = $pickListDtlsHashByCompanyIdItemId[$key];
			}
			$tmpPickListDtls[] = $pickListDtl;

			$pickListDtlsHashByCompanyIdItemId[$key] = $tmpPickListDtls;
		}

		foreach($pickListDtlsHashByCompanyIdItemId as $key => $pickListDtls)
		{
			$keyParts = explode('/',$key);
			$companyId = $keyParts[0];
			$itemId = $keyParts[1];

			foreach($pickListDtls as $pickListDtl) 
			{
				//mark as pick no fulfilled first
				PickListDtlRepository::updateWhseJobType($pickListDtl->id, WhseJobType::$MAP['PICK_NOT_FULFILLED']);
			}

			//2) sort the pickList array by unitQty DESC
			usort($pickListDtls, function ($a, $b) {    
				$aUnitQty = bcmul($a->qty, $a->uom_rate, 0);
				$bUnitQty = bcmul($b->qty, $b->uom_rate, 0);
				return ($aUnitQty < $bUnitQty) ? 1 : -1;
			});

			foreach($pickListDtls as $pickListDtl)
			{
				$item = $pickListDtl->item;
				//query the pickingCriteria
				$pickingCriterias = PickingCriteriaService::processByPickListHdrId($siteFlow, $item, $hdrModel->id);

				self::planItemPicking($hdrModel->location_id, $siteFlow, $companyId, $pickListDtl, $pickingCriterias);
			}
		}
		//commit the document
		$hdrModel = PickListHdrRepository::commitToWip($hdrModel->id);
		if(!empty($hdrModel))
		{
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

		$exc = new ApiException(__('PickList.commit_to_wip_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\PickListHdr::class, $hdrModel->id);
        throw $exc;
	}

	static public function transitionToComplete($hdrId)
    {
		AuthService::authorize(
            array(
                'pick_list_confirm'
            )
		);

		//use transaction to make sure this is latest value
		$hdrModel = PickListHdrRepository::txnFindByPk($hdrId);

		//only DRAFT or above can transition to COMPLETE
		if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE']
		|| $hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('PickList.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\PickListHdr::class, $hdrModel->id);
            throw $exc;
		}

		$dtlModels = PickListDtlRepository::findAllByHdrId($hdrModel->id);
		//preliminary checking
		//check the detail stock make sure the detail quant_bal_id > 0
		foreach($dtlModels as $dtlModel)
		{
			if($dtlModel->quant_bal_id == 0)
			{
				$exc = new ApiException(__('PickList.quant_bal_is_required', ['lineNo'=>$dtlModel->line_no]));
				$exc->addData(\App\PickListDtl::class, $dtlModel->id);
				throw $exc;
			}
		}

		//hardcode the code, block some division pickList to from complete if stock not enough
		$isNonZeroBal = true;
		/*
		$pickListOutbOrds = $hdrModel->pickListOutbOrds;
		foreach($pickListOutbOrds as $pickListOutbOrd)
		{
			$outbOrdHdr = $pickListOutbOrd->outbOrdHdr;
			if($outbOrdHdr->division_id == 1
			|| $outbOrdHdr->division_id == 3
			|| $outbOrdHdr->division_id == 4)
			{
				//gen/yeos division
				$isNonZeroBal = true;
				break;
			}
		}
		*/

		//commit the document
		$hdrModel = PickListHdrRepository::commitToComplete($hdrModel->id, $isNonZeroBal);
		if(!empty($hdrModel))
		{
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		
		$exc = new ApiException(__('PickList.commit_to_complete_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\PickListHdr::class, $hdrModel->id);
        throw $exc;
	}

	static public function checkReservation($strProcType, $hdrIds)
	{		
		$siteFlow = null;
		$pickListHdrs = PickListHdrRepository::findAllByHdrIds($hdrIds);
		foreach($pickListHdrs as $pickListHdr) 
		{
			$tmpDocTxnFlow = DocTxnFlowRepository::findByProcTypeAndFrHdrId(ProcType::$MAP[$strProcType], \App\PickListHdr::class, $pickListHdr->id, 1);
			if(!empty($tmpDocTxnFlow))
			{
				$exc = new ApiException(__('PickList.doc_is_closed', ['docType'=>$tmpDocTxnFlow->fr_doc_hdr_type, 'docCode'=>$tmpDocTxnFlow->fr_doc_hdr_code]));
				$exc->addData($tmpDocTxnFlow->fr_doc_hdr_type, $tmpDocTxnFlow->fr_doc_hdr_code);
				throw $exc;
			}

			$siteFlow = $pickListHdr->siteFlow;
		}

		$siteFlowLatestTimestamp = WhseJobHdrRepository::querySiteFlowLatestTimestamp($siteFlow->id);
		foreach($hdrIds as $hdrId)
		{
			$rsvdTxnEarliestTimestamp = QuantBalRsvdTxnRepository::queryEarliestTimestamp(\App\PickListHdr::class, $hdrId);
			if($rsvdTxnEarliestTimestamp->lt($siteFlowLatestTimestamp))
			{
				$hdrModel = self::transitionToDraft($hdrId);
				if(!empty($hdrModel))
				{
					self::transitionToWip($hdrId);
				}
			}
		}		
	}

	static public function transitionToDraft($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = PickListHdrRepository::txnFindByPk($hdrId);

		//only WIP or COMPLETE can transition to DRAFT
		if($hdrModel->doc_status == DocStatus::$MAP['WIP'])
		{
			$hdrModel = PickListHdrRepository::revertWipToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		elseif($hdrModel->doc_status == DocStatus::$MAP['COMPLETE'])
		{
			AuthService::authorize(
				array(
					'pick_list_revert'
				)
			);

			$hdrModel = PickListHdrRepository::revertCompleteToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		elseif($hdrModel->doc_status == DocStatus::$MAP['VOID'])
		{
			AuthService::authorize(
				array(
					'pick_list_confirm'
				)
			);

			$hdrModel = PickListHdrRepository::commitVoidToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

		$exc = new ApiException(__('PickList.doc_status_is_not_wip_or_complete', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\PickListHdr::class, $hdrModel->id);
		throw $exc;
	}

	protected function convertWhseJobTypeToBinTypes($whseJobType)
    {
        $binTypes = array();
        if($whseJobType == WhseJobType::$MAP['PICK_FACE_REPLENISHMENT'])
        {
            $binTypes[] = BinType::$MAP['PALLET_STORAGE'];
            $binTypes[] = BinType::$MAP['BROKEN_PALLET_STORAGE'];
        }
        elseif($whseJobType == WhseJobType::$MAP['PICK_FULL_PALLET'])
        {
            $binTypes[] = BinType::$MAP['BULK_STORAGE'];
            $binTypes[] = BinType::$MAP['PALLET_STORAGE'];
            $binTypes[] = BinType::$MAP['BROKEN_PALLET_STORAGE'];
        }
        elseif($whseJobType == WhseJobType::$MAP['PICK_BROKEN_PALLET'])
        {
            $binTypes[] = BinType::$MAP['BROKEN_PALLET_STORAGE'];
        }
        elseif($whseJobType == WhseJobType::$MAP['PICK_FULL_CASE'])
        {
            $binTypes[] = BinType::$MAP['CASE_STORAGE'];
        }
        elseif($whseJobType == WhseJobType::$MAP['PICK_BROKEN_CASE'])
        {
            $binTypes[] = BinType::$MAP['BROKEN_CASE_STORAGE'];
        }
        return $binTypes;
    }

    static protected function convertBinTypeToStorageBinIds($siteId, $binTypes, $locationId)
    {
        $storageBins = StorageBinRepository::findAllByBinTypes($siteId, $binTypes, $locationId);
		$storageBinIds = array();
        foreach($storageBins as $storageBin)
        {
            $storageBinIds[$storageBin->id] = $storageBin->id;
		}
        return $storageBinIds;
	}

	static protected function breakUpPickListDtl($pickListDtl, $quantBal, $item)
	{
		//return result, default to be current pickListDtl, and newPickListDtl is null
		$result = array(
			'old_pick_list_dtl' => $pickListDtl,
			'new_pick_list_dtl' => null,
		);

		$dtlUnitQty = bcmul($pickListDtl->qty, $pickListDtl->uom_rate, $item->qty_scale);

		$reservedUnitQty = QuantBalRsvdTxnRepository::queryReservedUnitQty($quantBal->id);
		$availUnitQty = bcsub($quantBal->balance_unit_qty, $reservedUnitQty, $item->qty_scale);

		if(bccomp($availUnitQty, 0, $item->qty_scale) > 0
		&& bccomp($dtlUnitQty, $availUnitQty, $item->qty_scale) > 0)
		{
			$remainUnitQty = bcsub($dtlUnitQty, $availUnitQty, $item->qty_scale);

			$oldDtlData = array(
				'uom_id' => $item->unit_uom_id,
				'uom_rate' => 1,
				'qty' => $availUnitQty,
			);

			$newDtlData = array(
				'hdr_id' => $pickListDtl->hdr_id,
				//if this original already have parent, then the new dtl will assign to the original parent, not orignal id
				'parent_dtl_id' => $pickListDtl->parent_dtl_id > 0 ? $pickListDtl->parent_dtl_id : $pickListDtl->id,
				'is_split' => $pickListDtl->is_split,
				'line_no' => PickListHdrRepository::BREAK_UP_LINE_NO,
				'company_id' => $pickListDtl->company_id,
				'item_id' => $pickListDtl->item_id,
				'desc_01' => $pickListDtl->desc_01,
				'desc_02' => $pickListDtl->desc_02,
				'uom_id' => $item->unit_uom_id,
				'uom_rate' => 1,
				'qty' => $remainUnitQty,
				'storage_bin_id' => 0,
				'quant_bal_id' => 0,
				'to_storage_bin_id' => $pickListDtl->to_storage_bin_id,
				'to_handling_unit_id' => $pickListDtl->to_handling_unit_id,
				'whse_job_type' => 0,
				'whse_job_code' => ''
			);

			$result = PickListDtlRepository::breakUp($pickListDtl->id, $oldDtlData, $newDtlData);
		}
		return $result;
	}

	static protected function createPickFaceReplenishment($pickListDtl, $item, $quantBal)
	{
		$quantBalStorageBin = $quantBal->storageBin;
		$pickListHdr = $pickListDtl->pickListHdr;
		$siteFlow = $pickListHdr->siteFlow;

		$toStorageBinId = 0;
		$pfPickListDtl = PickListDtlRepository::findWIPByQuantBalIdAndWhseJobType($quantBal->id, WhseJobType::$MAP['PICK_FACE_REPLENISHMENT']);
		if(!empty($pfPickListDtl))
		{
			$toStorageBinId = $pfPickListDtl->to_storage_bin_id;
		}
		else
		{
			$pickFaceStrategies = PickFaceStrategyRepository::findAllByItemId(
				$siteFlow->site_id, $item->id);

			if(count($pickFaceStrategies) >= 1)
			{
				//more than 1 pick face for this item
				//loop the strategies and get the first top strategy to find the toStorageBinId
				$storageBinDataList = array();
				foreach($pickFaceStrategies as $pickFaceStrategy)
				{
					//query the bin current capacity
					$storageBinCapacity = QuantBalRepository::queryStorageBinCapacity($pickFaceStrategy->storage_bin_id);
					$reservedCapacity = QuantBalRsvdTxnRepository::queryReservedCapacity($pickFaceStrategy->storage_bin_id);

					$netCubicMeter = bcsub($storageBinCapacity['cubic_meter'], $reservedCapacity['cubic_meter'], 5);
					$netGrossWeight = bcsub($storageBinCapacity['gross_weight'], $reservedCapacity['gross_weight'], 5);

					$storageBinDataList[] = array(
						'storage_bin_id' => $pickFaceStrategy->storage_bin_id,
						'cubic_meter' => $netCubicMeter,
						'gross_weight' => $netGrossWeight,
					);
				}

				//sort by cubic_meter, gross_weight
				usort($storageBinDataList, function ($a, $b) {
					if(bccomp($a['cubic_meter'], $b['cubic_meter'], 5) == 0)
					{
						return bccomp($a['gross_weight'], $b['gross_weight'], 5) < 0 ? -1 : 1;
					}
					return bccomp($a['cubic_meter'], $b['cubic_meter'], 5) < 0 ? -1 : 1;
				});

				//read the least/first cubic_meter, gross_weight
				foreach($storageBinDataList as $storageBinData)
				{
					$toStorageBinId = $storageBinData['storage_bin_id'];
					break;
				}
			}
			elseif(count($pickFaceStrategies) == 1)
			{
				//only 1 pick face for this item
				$pickFaceStrategy = $pickFaceStrategies[0];
				$toStorageBinId = $pickFaceStrategy->storage_bin_id;
			}
			
			if($toStorageBinId == 0)
			{
				//no pick face set for this item
				//if strategic fail to assign any bin, then just assign to bottom pick face bin
				$storageBinDataList = array();
				$pfStorageBins = StorageBinRepository::findPickFaceByStorageBayId($quantBal->storage_bay_id);
				foreach($pfStorageBins as $pfStorageBin)
				{
					//query the bin current capacity
					$storageBinCapacity = QuantBalRepository::queryStorageBinCapacity($pfStorageBin->id);
					$reservedCapacity = QuantBalRsvdTxnRepository::queryReservedCapacity($pfStorageBin->id);

					$netCubicMeter = bcsub($storageBinCapacity['cubic_meter'], $reservedCapacity['cubic_meter'], 5);
					$netGrossWeight = bcsub($storageBinCapacity['gross_weight'], $reservedCapacity['gross_weight'], 5);

					$storageBinDataList[] = array(
						'storage_bin_id' => $pfStorageBin->id,
						'cubic_meter' => $netCubicMeter,
						'gross_weight' => $netGrossWeight,
					);
				}

				//sort by cubic_meter, gross_weight
				usort($storageBinDataList, function ($a, $b) {
					if(bccomp($a['cubic_meter'], $b['cubic_meter'], 5) == 0)
					{
						return bccomp($a['gross_weight'], $b['gross_weight'], 5) < 0 ? -1 : 1;
					}
					return bccomp($a['cubic_meter'], $b['cubic_meter'], 5) < 0 ? -1 : 1;
				});

				//read the least/first cubic_meter, gross_weight
				foreach($storageBinDataList as $storageBinData)
				{
					$toStorageBinId = $storageBinData['storage_bin_id'];
					break;
				}
			}
		}
		
		$dtlData = array(
			'hdr_id' => $pickListDtl->hdr_id,
			'line_no' => 0,
			'company_id' => $pickListDtl->company_id,
			'item_id' => $pickListDtl->item_id,
			'desc_01' => $pickListDtl->desc_01,
			'desc_02' => $pickListDtl->desc_02,
			'uom_id' => Uom::$PALLET,
			'uom_rate' => 0,
			'qty' => 1,
			'storage_bin_id' => $quantBal->storage_bin_id,
			'quant_bal_id' => $quantBal->id,
			'to_storage_bin_id' => $toStorageBinId,
			'to_handling_unit_id' => 0,
			'whse_job_type' => WhseJobType::$MAP['PICK_FACE_REPLENISHMENT'],
			'whse_job_code' => ''
		);
		
		$newPickListDtl = PickListDtlRepository::createPickFaceReplenishment($dtlData);
		return $newPickListDtl;
	}
	
	static protected function reserveQuantBal($item, $pickListDtl, $quantBal)
	{
		//calculate the pickUnitQty
		$pickUnitQty = bcmul($pickListDtl->qty, $pickListDtl->uom_rate, $item->qty_scale);

		//check quantBal binType
		$storageBin = $quantBal->storageBin;
		$storageType = $quantBal->storageType;
		if($storageType->bin_type == BinType::$MAP['BROKEN_CASE_STORAGE'])
		{
			//pickface, loose picking
			$tmpIsSuccess = QuantBalRepository::pickListDtlReserve(WhseJobType::$MAP['PICK_BROKEN_CASE'], $pickListDtl->id, $pickListDtl->pickListHdr, $item, $quantBal->id, $pickUnitQty);
			return $tmpIsSuccess;
		}
		elseif($storageType->bin_type == BinType::$MAP['CASE_STORAGE'])
		{
			//pickface, case picking
			//make sure the pickUnitQty is multiple of case
			$noOfCases = bcmod($pickUnitQty, $item->case_uom_rate);
			$remainder = bcsub($pickUnitQty, bcmul($noOfCases, $item->case_uom_rate, $item->qty_scale), $item->qty_scale);

			if(bccomp($remainder, 0, $item->qty_scale) == 0)
			{
				//no remainder, pickUnitQty is multiple of case
				$tmpIsSuccess = QuantBalRepository::pickListDtlReserve(WhseJobType::$MAP['PICK_FULL_CASE'], $pickListDtl->id, $pickListDtl->pickListHdr, $item, $quantBal->id, $pickUnitQty);
				return $tmpIsSuccess;
			}
		}
		elseif($storageType->bin_type == BinType::$MAP['BROKEN_PALLET_STORAGE'])
		{
			//pallet, broken pallet allow loose picking from pallet, but require forklift
			$otherQuantBals = QuantBalRepository::findOtherQuantsInPallet($quantBal->id, $quantBal->handling_unit_id, $quantBal->storage_bin_id);

			//if no other reservation, and pickUnitQty = balanceUnitQty, and not mixed pallet
			if(count($otherQuantBals) == 0)
			{
				//this can move in full pallet
				$tmpIsSuccess = QuantBalRepository::pickListDtlReserve(WhseJobType::$MAP['PICK_FULL_PALLET'], $pickListDtl->id, $pickListDtl->pickListHdr, $item, $quantBal->id, $pickUnitQty);
				return $tmpIsSuccess;
			}
			else
			{
				//not full pallet, then check for availUnitQty
				//broken pallet allow loose picking, so no need go for pick face replenishment
				$tmpIsSuccess = QuantBalRepository::pickListDtlReserve(WhseJobType::$MAP['PICK_BROKEN_PALLET'], $pickListDtl->id, $pickListDtl->pickListHdr, $item, $quantBal->id, $pickUnitQty);
				return $tmpIsSuccess;
			}
		}
		elseif($storageType->bin_type == BinType::$MAP['PALLET_STORAGE'])
		{
			//pallet storage, require forklift, and only can move in pallet
			$reservedUnitQty = QuantBalRsvdTxnRepository::queryReservedUnitQty($quantBal->id);
			$availUnitQty = bcsub($quantBal->balance_unit_qty, $reservedUnitQty, $item->qty_scale);
			$otherQuantBals = QuantBalRepository::findOtherQuantsInPallet($quantBal->id, $quantBal->handling_unit_id, $quantBal->storage_bin_id);

			//if no other reservation, and pickUnitQty = balanceUnitQty, and not mixed pallet
			if(bccomp($reservedUnitQty, 0, $item->qty_scale) == 0
			&& bccomp($pickUnitQty, $availUnitQty, $item->qty_scale) == 0
			&& count($otherQuantBals) == 0)
			{
				//this can move in full pallet
				$tmpIsSuccess = QuantBalRepository::pickListDtlReserve(WhseJobType::$MAP['PICK_FULL_PALLET'], $pickListDtl->id, $pickListDtl->pickListHdr, $item, $quantBal->id, $pickUnitQty);
				return $tmpIsSuccess;
			}
			else
			{
				//mark the reservation
				$tmpIsSuccess = QuantBalRepository::pickListDtlReserve(WhseJobType::$MAP['WAITING_REPLENISHMENT'], $pickListDtl->id, $pickListDtl->pickListHdr, $item, $quantBal->id, $pickUnitQty);
				if($tmpIsSuccess === true)
				{
					//not full pallet, then check for availUnitQty
					//trigger pick face replenishment
					self::createPickFaceReplenishment($pickListDtl, $item, $quantBal);
				}
				return $tmpIsSuccess;
			}
		}
		elseif($storageType->bin_type == BinType::$MAP['BULK_STORAGE'])
		{
			//bulk storage, require forklift, and only can move in pallet
			$reservedUnitQty = QuantBalRsvdTxnRepository::queryReservedUnitQty($quantBal->id);
			$availUnitQty = bcsub($quantBal->balance_unit_qty, $reservedUnitQty, $item->qty_scale);
			$otherQuantBals = QuantBalRepository::findOtherQuantsInPallet($quantBal->id, $quantBal->handling_unit_id, $quantBal->storage_bin_id);

			//if no other reservation, and pickUnitQty = balanceUnitQty, and not mixed pallet
			if(bccomp($reservedUnitQty, 0, $item->qty_scale) == 0
			&& bccomp($pickUnitQty, $availUnitQty, $item->qty_scale) == 0
			&& count($otherQuantBals) == 0)
			{
				//this can move in full pallet
				$tmpIsSuccess = QuantBalRepository::pickListDtlReserve(WhseJobType::$MAP['PICK_FULL_PALLET'], $pickListDtl->id, $pickListDtl->pickListHdr, $item, $quantBal->id, $pickUnitQty);
				return $tmpIsSuccess;
			}
			else
			{
				//mark the reservation
				$tmpIsSuccess = QuantBalRepository::pickListDtlReserve(WhseJobType::$MAP['WAITING_REPLENISHMENT'], $pickListDtl->id, $pickListDtl->pickListHdr, $item, $quantBal->id, $pickUnitQty);
				if($tmpIsSuccess === true)
				{
					//not full pallet, then check for availUnitQty
					//trigger pick face replenishment
					self::createPickFaceReplenishment($pickListDtl, $item, $quantBal);
				}
				return $tmpIsSuccess;
			}
		}
		return false;
	}

	//recursive function
	static protected function planItemPicking($locationId, $siteFlow, $companyId, $pickListDtl, $pickingCriterias, $isForcePickface = false)
	{
		$item = $pickListDtl->item;
		$rqdUnitQty = bcmul($pickListDtl->uom_rate, $pickListDtl->qty, $item->qty_scale);

		//DB::connection()->enableQueryLog();
		$pickFaceAvailUnitQty = QuantBalRepository::queryAvailUnitQty(
			$siteFlow->site_id, array(
				BinType::$MAP['CASE_STORAGE'], 
				BinType::$MAP['BROKEN_CASE_STORAGE']
			),
			$locationId,
			$companyId, $item->id, $pickingCriterias);
		//Log::error(DB::getQueryLog());

		$isPalletPick = false;
		//check if rqdUnitQty is more than a pallet, using pallet configuration
		$ttlCasesPerPallet = bcmul($item->cases_per_pallet_length, $item->cases_per_pallet_width);
        $ttlCasesPerPallet = bcmul($ttlCasesPerPallet, $item->no_of_layers);
		if(bccomp($ttlCasesPerPallet, 1) > 0
		&& bccomp($item->pallet_uom_rate, 1) != 0)
		{
			$ttlUnitPerPallet = bcmul($ttlCasesPerPallet, $item->case_uom_rate);
			if(bccomp($rqdUnitQty, $ttlUnitPerPallet) >= 0
			&& $isForcePickface == false)
			{
				$isPalletPick = true;
			}
		}

		//3) check if pick face available unitQty can fulfill item required unitQty
		if(bccomp($rqdUnitQty, $pickFaceAvailUnitQty, $item->qty_scale) <= 0
		&& $isPalletPick == false)
		{
			//pick face quantBals can fulfill item required unitQty
			//get all the pick face quantBals, order by item retrievalMethod
			//DB::connection()->enableQueryLog();
			$pickFaceQuantBals = QuantBalRepository::findAllActiveByPickingCriteria(
				$rqdUnitQty,
				$siteFlow->site_id, array(
					BinType::$MAP['CASE_STORAGE'], 
					BinType::$MAP['BROKEN_CASE_STORAGE']
				),
				$locationId,
				$companyId, $item->id, $item->retrieval_method, $pickingCriterias);
			//Log::error(DB::getQueryLog());

			$pickFaceQuantBals->load('itemBatch');
			//loop the pickFace quantBal to find 1 quant to fill the pickListDtl
			foreach($pickFaceQuantBals as $pickFaceQuantBal)
			{
				//here will filter the criteria

				//try to reserve this quantBal to the pickListDtl
				$isSuccess = self::reserveQuantBal($item, $pickListDtl, $pickFaceQuantBal);
				if($isSuccess === true)
				{
					//break the loop if the reservation is success
					return true;
				}
			}

			//if fail to find 1 quantBal to fulfill the pickListDtl
			//need to break up the dtl
			$newPickListDtl = null;
			foreach($pickFaceQuantBals as $pickFaceQuantBal)
			{
				//here will filter the criteria

				//break up the pickListDtl based on this quantBal unitQty
				$result = self::breakUpPickListDtl($pickListDtl, $pickFaceQuantBal, $item);
				//old will adjust to this quantBal unitQty
				$oldPickListDtl = $result['old_pick_list_dtl'];
				//reserve the quantBal for the old pickListDtl
				$isSuccess = self::reserveQuantBal($item, $oldPickListDtl, $pickFaceQuantBal);
				if($isSuccess === true)
				{
					//new will adjust to the remaining unitQty
					$newPickListDtl = $result['new_pick_list_dtl'];
					break;
				}					
			}
			if(!empty($newPickListDtl))
			{
				self::planItemPicking($locationId, $siteFlow, $companyId, $newPickListDtl, $pickingCriterias);
			}
		}
		else
		{
			//pick face quantBals can't fulfill item required unitQty

			//get all the pallet quantBals, order by item retrievalMethod
			//DB::connection()->enableQueryLog();
			$palletQuantBals = QuantBalRepository::findAllActiveByPickingCriteria(
				$rqdUnitQty,
				$siteFlow->site_id, array(
					BinType::$MAP['BULK_STORAGE'], 
					BinType::$MAP['PALLET_STORAGE'],
					BinType::$MAP['BROKEN_PALLET_STORAGE']
				),
				$locationId,
				$companyId, $item->id, $item->retrieval_method, $pickingCriterias);
			//Log::error(DB::getQueryLog());
			$palletQuantBals->load('itemBatch');
			//loop the pallet quantBals, to find 1 quant to fill the pickListDtl
			foreach($palletQuantBals as $palletQuantBal)
			{
				//here will filter the criteria

				//try to reserve this quantBal to the pickListDtl
				$isSuccess = self::reserveQuantBal($item, $pickListDtl, $palletQuantBal);
				if($isSuccess === true)
				{
					//break the loop if the reservation is success
					return true;
				}
			}

			//if fail to find 1 quantBal to fulfill the pickListDtl
			//if this pickListDtl need more than 1 quantBal, need to break up the pickListDtl
			$newPickListDtl = null;
			//loop the quantBals to break up the pickListDtl accordingly
			foreach($palletQuantBals as $palletQuantBal)
			{
				//here will filter the criteria

				//break up the pickListDtl based on this quantBal unitQty
				$result = self::breakUpPickListDtl($pickListDtl, $palletQuantBal, $item);
				//old will adjust to this quantBal unitQty
				$oldPickListDtl = $result['old_pick_list_dtl'];
				//reserve the quantBal for the old pickListDtl
				$isSuccess = self::reserveQuantBal($item, $oldPickListDtl, $palletQuantBal);
				if($isSuccess === true)
				{
					//new will adjust to the remaining unitQty
					$newPickListDtl = $result['new_pick_list_dtl'];
					break;
				}
			}
			if(empty($newPickListDtl))
			{
				//this condition is to cater if pallet storage totally do not have the item, it will not break the line also
				$newPickListDtl = $pickListDtl;
			}
			if(!empty($newPickListDtl))
			{
				$newUnitQty = bcmul($newPickListDtl->uom_rate, $newPickListDtl->qty, $item->qty_scale);
				$forcePickface = false;
				if(bccomp($newUnitQty, $rqdUnitQty) == 0)
				{
					if($isForcePickface == true)
					{
						//if already force to pick face, need to return now to prevent infinity loop
						return false;
					}
					//if pallet storage can't fulfill the required qty, and required qty is same with new qty
					//force to search at pickface
					$forcePickface = true;
				}
				self::planItemPicking($locationId, $siteFlow, $companyId, $newPickListDtl, $pickingCriterias, $forcePickface);
			}
		}
		return true;
	}

	public function printPickListPdf($pickListId)
	{

		$OutbOrdHdr = OutbOrdHdr::where('id',$pickListId)->with('outb_dtl')->first();
		
		
		return $OutbOrdHdr;
		// $barcode = DNS2D::getBarcodePNGPath("4445645656", "PDF417");
		
        // $pdf->loadHTML(
        //     view('pickList',
        //         ['data' => $OutbOrdHdr]
        //     )
        // );
        // return $pdf->stream();
		// return $pickList;
	}

	public function revertProcess($hdrId)
    {
		$hdrModel = PickListHdrRepository::findByPk($hdrId);

		$whseTxnFlow = WhseTxnFlowRepository::findByPk($hdrModel->site_flow_id, $hdrModel->proc_type);

		$hdrModel = self::transitionToDraft($hdrModel->id);

		return $hdrModel;
	}

	public function submitProcess($hdrId)
    {
		$hdrModel = PickListHdrRepository::txnFindByPk($hdrId);

		$whseTxnFlow = WhseTxnFlowRepository::findByPk($hdrModel->site_flow_id, $hdrModel->proc_type);

		if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
		{
			$hdrModel = self::transitionToWip($hdrModel->id);
		}
		elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
		{
			$hdrModel = self::transitionToComplete($hdrModel->id);
		}

		return $hdrModel;
	}

	public function completeProcess($hdrId)
    {
		$hdrModel = PickListHdrRepository::txnFindByPk($hdrId);

		$whseTxnFlow = WhseTxnFlowRepository::findByPk($hdrModel->site_flow_id, $hdrModel->proc_type);

		if($hdrModel->doc_status < DocStatus::$MAP['WIP'])
		{
			$exc = new ApiException(__('PickList.doc_status_is_not_wip', ['docCode'=>$hdrModel->doc_code]));
			$exc->addData(\App\PickListHdr::class, $hdrModel->id);
			throw $exc;
		}
		if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
		{
			$exc = new ApiException(__('PickList.doc_status_is_complete', ['docCode'=>$hdrModel->doc_code]));
			$exc->addData(\App\PickListHdr::class, $hdrModel->id);
			throw $exc;
		}

		$hdrModel = self::transitionToComplete($hdrModel->id);

		return $hdrModel;
	}

	public function transitionToStatus($hdrId, $strDocStatus)
    {
		$hdrModel = null;
		$documentDetails = array();
		$delDocumentDetails = array();
        if(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['WIP'])
        {
			$hdrModel = self::transitionToWip($hdrId);
			$dtlModels = PickListDtlRepository::findAllByHdrId($hdrId);
            foreach($dtlModels as $dtlModel)
            {
                $documentDetails[] = self::processOutgoingDetail($dtlModel);
            }
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['COMPLETE'])
        {
			$hdrModel = self::transitionToComplete($hdrId);
			$dtlModels = PickListDtlRepository::findAllByHdrId($hdrId);
            foreach($dtlModels as $dtlModel)
            {
                $documentDetails[] = self::processOutgoingDetail($dtlModel);
            }
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['DRAFT'])
        {
			$oriDtlModels = PickListDtlRepository::findAllByHdrId($hdrId);
			$oriDtlModelsHashById = array();
			foreach($oriDtlModels as $oriDtlModel)
			{
				$oriDtlModelsHashById[$oriDtlModel->id] = $oriDtlModel;
			}
			$hdrModel = self::transitionToDraft($hdrId);
			$dtlModels = PickListDtlRepository::findAllByHdrId($hdrId, 'line_no', 'ASC');
            foreach($dtlModels as $dtlModel)
            {
				if(array_key_exists($dtlModel->id, $oriDtlModelsHashById))
				{
					unset($oriDtlModelsHashById[$dtlModel->id]);
				}
                $documentDetails[] = self::processOutgoingDetail($dtlModel);
			}
			foreach($oriDtlModelsHashById as $id => $delDtlModel)
			{
				$delDtlModel->is_deleted = 1;
				$delDocumentDetails[] = $delDtlModel;
			}
		}
		elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['VOID'])
        {
            $hdrModel = self::transitionToVoid($hdrId);
        }
        
		$documentHeader = self::processOutgoingHeader($hdrModel);
        $message = __('PickList.document_successfully_transition', ['docCode'=>$documentHeader->doc_code,'strDocStatus'=>$strDocStatus]);
        
        return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>$delDocumentDetails
            ),
			'message' => $message
		);
	}

	static public function processOutgoingHeader($model, $isShowPrint = false)
	{
		$docFlows = self::processDocFlows($model);
		$model->doc_flows = $docFlows;
		
		if($isShowPrint)
		{
			$printDocTxn = PrintDocTxnRepository::queryPrintDocTxn(PickListHdr::class, $model->id);
			$model->print_count = $printDocTxn->print_count;
			$model->first_printed_at = $printDocTxn->first_printed_at;
			$model->last_printed_at = $printDocTxn->last_printed_at;
		}

		$model->str_doc_status = DocStatus::$MAP[$model->doc_status];
		return $model;
	}
	
	public function verifyTxn() 
	{
        $invalidRows = PickListHdrRepository::verifyTxn();        
        return $invalidRows;
	}
	
	public function syncProcess($strProcType, $divisionId, $startDate, $endDate)
    {
		$user = Auth::user();

        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['PICK_LIST_SYNC_01']) 
			{
				//sync sales order from EfiChain
				return $this->syncPickListSync01($divisionId, $user->id, $startDate, $endDate);
            }
		}
    }

    protected function syncPickListSync01($divisionId, $userId, $startDate, $endDate)
    {
		AuthService::authorize(
			array(
				'pick_list_export'
			)
		);

        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['PICK_LIST_SYNC_01'], $userId);

		//query the completed pickList associated outbOrdHdrs from DB

		//DB::connection()->enableQueryLog();
		$pickListOutbOrds = PickListOutbOrdRepository::findAllByCompletePickList($divisionId, $startDate, $endDate);
		//dd(DB::getQueryLog());
		$postSlsOrds = array();
		foreach($pickListOutbOrds as $pickListOutbOrd)
		{
			$data = array(
				'sls_ord_doc_code' => $pickListOutbOrd->sls_ord_hdr_code,
				'pick_list_doc_code' => $pickListOutbOrd->pick_list_doc_code,
				'pick_list_doc_date' => $pickListOutbOrd->pick_list_doc_date,
			);
			$postSlsOrds[] = $data;
		}

        $syncSettingHdr = SyncSettingHdrRepository::findByProcTypeAndDivisionId(ProcType::$MAP['SYNC_EFICHAIN_01'], $divisionId);
        if(empty($syncSettingHdr))
        {
            $exc = new ApiException(__('PickList.sync_setting_not_found', ['divisionId'=>$divisionId]));
            //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
            throw $exc;
        }

        $client = new Client();
        $header = array();

        $usernameSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'username');
        $passwordSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'password');
        $formParams = array(
            'UserLogin[username]' => $usernameSetting->value,
            'UserLogin[password]' => $passwordSetting->value
		);		
		$formParams['SlsOrdHdrs'] = $postSlsOrds;

        $url = $syncSettingHdr->url.'/index.php?r=luuWu/postPickLists';
            
		$response = $client->post($url, array('headers' => $header, 'form_params' => $formParams));
		
		$result = $response->getBody()->getContents();
		$result = json_decode($result, true);
		if(empty($result))
		{
			BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

			$exc = new ApiException(__('PickList.fail_to_sync', ['url'=>$url]));
			//$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
			throw $exc;
		}
		else
		{
			if($result['success'] === false)
			{
				BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

				$exc = new ApiException(__('PickList.fail_to_sync', ['url'=>$url,'message'=>$result['message']]));
				//$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
				throw $exc;
			}
		}

		/*
		$successPickListDocCodes = array();
		for ($a = 0; $a < count($result['data']); $a++)
		{
			$successPickListDocCodes[] = $result['data'][$a];
		}
		*/
		$batchJobStatusModel = BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

		return array(
			'data' => $batchJobStatusModel,
			'message' => $result['message']
		);
	}
	
	static public function processOutgoingDetail($model)
  	{
		//relationships:
		$item = $model->item;
		$quantBal = $model->quantBal;
		$uom = $model->uom;
		$toStorageBin = $model->toStorageBin;
		$toHandlingUnit = $model->toHandlingUnit;
		
		$model->str_whse_job_type = WhseJobType::$MAP[$model->whse_job_type];
		$model->str_whse_job_type = __('WhseJob.'.strtolower($model->str_whse_job_type));

		$model->storage_bin_code = '';
		$model->storage_row_code = '';
		$model->storage_bay_code = '';
		$model->handling_unit_barcode = '';

		$model->batch_serial_no = '';
		$model->expiry_date = '';
		$model->receipt_date = '';

		$model->to_storage_bin_code = '';
		$model->to_storage_row_code = '';
		$model->to_storage_bay_code = '';

		//this whseJobDtl is delivery
		$company = null;
		$storageBin = null;
		$handlingUnit = null;
		$itemBatch = null;
		$storageRow = null;
		$storageBay = null;
		if(!empty($quantBal))
		{
			$company = $quantBal->company;
			$handlingUnit = $quantBal->handlingUnit;
			$itemBatch = $quantBal->itemBatch;
			$storageBin = $quantBal->storageBin;
			$storageRow = $quantBal->storageRow;
			$storageBay = $quantBal->storageBay;
		}
		//if job type is WAITING REPLENISHMENT, find the storageBinCode
		if($model->whse_job_type == WhseJobType::$MAP['WAITING_REPLENISHMENT'])
        {
			$pickListDtl = PickListDtlRepository::findWIPByQuantBalIdAndWhseJobType($model->quant_bal_id, WhseJobType::$MAP['PICK_FACE_REPLENISHMENT']);
			if(!empty($pickListDtl))
			{
				$storageBin = $pickListDtl->toStorageBin;
			}
		}
		if(!empty($storageBin))
		{
			$storageRow = $storageBin->storageRow;
			$model->storage_bin_code = $storageBin->code;
		}
		if(!empty($handlingUnit))
		{
			$model->handling_unit_barcode = $handlingUnit->getBarcode();
		}
		if(!empty($storageRow))
		{
			$model->storage_row_code = $storageRow->code;
		}
		$model->item_bay_sequence = 0;
		if(!empty($storageBay))
		{
			$model->storage_bay_code = $storageBay->code;
			$model->item_bay_sequence = $storageBay->bay_sequence;
		}
		if(!empty($itemBatch))
		{
			$model->batch_serial_no = $itemBatch->batch_serial_no;
			$model->expiry_date = $itemBatch->expiry_date;
			$model->receipt_date = $itemBatch->receipt_date;
		}

		//this whseJobDtl is receipt
		if(!empty($toStorageBin))
		{
			$toStorageRow = $toStorageBin->storageRow;
			$toStorageBay = $toStorageBin->storageBay;

			$model->to_storage_bin_code = $toStorageBin->code;
			if(!empty($toStorageRow))
			{
				$model->to_storage_row_code = $toStorageRow->code;
			}
			$model->to_item_bay_sequence = 0;
			if(!empty($toStorageBay))
			{
				$model->to_storage_bay_code = $toStorageBay->code;
				$model->to_item_bay_sequence = $toStorageBay->bay_sequence;
			}
		}

		//calculate the pallet qty, case qty, gross weight, and m3
		if($model->uom_id == Uom::$PALLET
		&& bccomp($model->uom_rate, 0, 5) == 0)
		{
			//this is pallet picking
			$model->unit_qty = 0;
			$model->case_qty = 0;
			$model->gross_weight = 0;
			$model->cubic_meter = 0;

			$handlingQuantBals = QuantBalRepository::findAllByHandlingUnitIdAndStorageBinId($quantBal->handling_unit_id, $quantBal->storage_bin_id);
			$handlingQuantBals->load('item');
			foreach($handlingQuantBals as $handlingQuantBal)
			{
				if($handlingQuantBal->balance_unit_qty == 0)
				{
					continue;
				}
				$handlingItem = $handlingQuantBal->item;

				$handlingQuantBal = ItemService::processCaseLoose($handlingQuantBal, $handlingItem, 1);

				$model->item_code = $handlingQuantBal->item_code;
				$model->item_ref_code_01 = $handlingQuantBal->item_ref_code_01;
				$model->item_desc_01 = $handlingQuantBal->item_desc_01;
				$model->item_desc_02 = $handlingQuantBal->item_desc_02;
				$model->storage_class = $handlingQuantBal->storage_class;
				$model->item_group_01_code = $handlingQuantBal->item_group_01_code;
				$model->item_group_02_code = $handlingQuantBal->item_group_02_code;
				$model->item_group_03_code = $handlingQuantBal->item_group_03_code;
				$model->item_group_04_code = $handlingQuantBal->item_group_04_code;
				$model->item_group_05_code = $handlingQuantBal->item_group_05_code;
				$model->item_cases_per_pallet_length = $handlingQuantBal->item_cases_per_pallet_length;
				$model->item_cases_per_pallet_width = $handlingQuantBal->item_cases_per_pallet_width;
				$model->item_no_of_layers = $handlingQuantBal->item_no_of_layers;

				$model->unit_qty = bcadd($model->unit_qty, $handlingQuantBal->balance_unit_qty, 10);
				$model->case_qty = bcadd($model->case_qty, $handlingQuantBal->case_qty, 10);
				$model->gross_weight = bcadd($model->gross_weight, $handlingQuantBal->gross_weight, 10);
				$model->cubic_meter = bcadd($model->cubic_meter, $handlingQuantBal->cubic_meter, 10);
			}
		}
		else
		{
			$item = $model->item;
			$model = ItemService::processCaseLoose($model, $item);
		}

		//storageBin select2
		$initCompanyOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($company))
        {
            $initCompanyOption = array('value'=>$company->id,'label'=>$company->code);
        }        
		$model->company_select2 = $initCompanyOption;

		$initStorageBinOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($storageBin))
        {
            $initStorageBinOption = array('value'=>$storageBin->id,'label'=>$storageBin->code);
        }        
		$model->storage_bin_select2 = $initStorageBinOption;

		$initQuantBalOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($quantBal))
        {
            $label = '';
            if(!empty($model->handling_unit_barcode))
            {
                $label = $model->handling_unit_barcode.' | ';
            }
			$label = $label.$model->item_code;
            $initQuantBalOption = array(
				'value'=>$quantBal->id,
				'label'=>$label
			);
        }        
		$model->quant_bal_select2 = $initQuantBalOption;
		
		$initToStorageBinOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($toStorageBin))
        {
            $initToStorageBinOption = array('value'=>$toStorageBin->id,'label'=>$toStorageBin->code);
        }        
        $model->to_storage_bin_select2 = $initToStorageBinOption;
        
        $initUomOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($uom))
        {
            $initUomOption = array('value'=>$uom->id,'label'=>$uom->code);
        }        
		$model->uom_select2 = $initUomOption;
		
		unset($model->item);
		unset($model->company);
		unset($model->storageBin);
		unset($model->handlingUnit);
		unset($model->quantBal);
		unset($model->uom);
		unset($model->toStorageBin);
		unset($model->toHandlingUnit);

		return $model;
	}

	static public function processIncomingDetail($data)
    {
        //preprocess the select2 and dropDown
        if(array_key_exists('item_select2', $data))
        {
			$select2Id = $data['item_select2']['value'];
			if($select2Id > 0)
			{
				$data['item_id'] = $select2Id;
			}
            unset($data['item_select2']);
        }
        $item = ItemRepository::findByPk($data['item_id']);
        if(empty($item))
		{
			$exc = new ApiException(__('BinTrf.item_is_required', ['lineNo'=>$data['line_no']]));
			//$exc->addData(\App\WhseJobDtl::class, $data['id']);
			throw $exc;
        }
        
		if(array_key_exists('qty', $data)
		&& !empty($item))
		{
			$data['qty'] = round($data['qty'], $item->qty_scale);
        }

		if(array_key_exists('company_select2', $data))
        {
			$select2Id = $data['company_select2']['value'];
			if($select2Id > 0)
			{
				$data['company_id'] = $select2Id;
			}
            unset($data['company_select2']);
        }
        if(array_key_exists('storage_bin_select2', $data))
        {
			$select2Id = $data['storage_bin_select2']['value'];
			if($select2Id > 0)
			{
				$data['storage_bin_id'] = $select2Id;
			}
            unset($data['storage_bin_select2']);
        }
        if(array_key_exists('quant_bal_select2', $data))
        {
			$select2Id = $data['quant_bal_select2']['value'];
			if($select2Id > 0)
			{
				$data['quant_bal_id'] = $select2Id;
			}
            unset($data['quant_bal_select2']);
        }
        if(array_key_exists('uom_select2', $data))
        {
			$select2Id = $data['uom_select2']['value'];
			if($select2Id > 0)
			{
				$data['uom_id'] = $select2Id;
			}
            unset($data['uom_select2']);
        }
        if(array_key_exists('to_storage_bin_select2', $data))
        {
			$select2Id = $data['to_storage_bin_select2']['value'];
			if($select2Id > 0)
			{
				$data['to_storage_bin_id'] = $select2Id;
			}
            unset($data['to_storage_bin_select2']);
		}

        if(array_key_exists('item_cond_01_code', $data))
        {            
            unset($data['item_cond_01_code']);
        }
        if(array_key_exists('storage_bin_code', $data))
        {
            unset($data['storage_bin_code']);
		}
		if(array_key_exists('storage_row_code', $data))
        {
            unset($data['storage_row_code']);
		}
		if(array_key_exists('storage_bay_code', $data))
        {
            unset($data['storage_bay_code']);
		}
		if(array_key_exists('item_bay_sequence', $data))
        {
            unset($data['item_bay_sequence']);
		}
		if(array_key_exists('to_storage_bin_code', $data))
        {
            unset($data['to_storage_bin_code']);
		}
		if(array_key_exists('to_storage_row_code', $data))
        {
            unset($data['to_storage_row_code']);
		}
		if(array_key_exists('to_storage_bay_code', $data))
        {
            unset($data['to_storage_bay_code']);
		}
		if(array_key_exists('to_item_bay_sequence', $data))
        {
            unset($data['to_item_bay_sequence']);
		}

		if(array_key_exists('str_whse_job_type', $data))
        {
            unset($data['str_whse_job_type']);
		}
        
        if(array_key_exists('handling_unit_barcode', $data))
        {
            unset($data['handling_unit_barcode']);
		}
		if(array_key_exists('handling_unit_ref_code_01', $data))
        {
            unset($data['handling_unit_ref_code_01']);
        }
        if(array_key_exists('batch_serial_no', $data))
        {
            unset($data['batch_serial_no']);
        }
        if(array_key_exists('expiry_date', $data))
        {
            unset($data['expiry_date']);
        }
        if(array_key_exists('receipt_date', $data))
        {
            unset($data['receipt_date']);
        }
        if(array_key_exists('item_code', $data))
        {
            unset($data['item_code']);
		}
		if(array_key_exists('item_ref_code_01', $data))
        {
            unset($data['item_ref_code_01']);
		}
		if(array_key_exists('item_desc_01', $data))
        {
            unset($data['item_desc_01']);
		}
		if(array_key_exists('item_desc_02', $data))
        {
            unset($data['item_desc_02']);
		}
		if(array_key_exists('storage_class', $data))
        {
            unset($data['storage_class']);
		}
		if(array_key_exists('item_group_01_code', $data))
        {
            unset($data['item_group_01_code']);
		}
		if(array_key_exists('item_group_02_code', $data))
        {
            unset($data['item_group_02_code']);
		}
		if(array_key_exists('item_group_03_code', $data))
        {
            unset($data['item_group_03_code']);
		}
		if(array_key_exists('item_group_04_code', $data))
        {
            unset($data['item_group_04_code']);
		}
		if(array_key_exists('item_group_05_code', $data))
        {
            unset($data['item_group_05_code']);
		}
		if(array_key_exists('item_cases_per_pallet_length', $data))
        {
            unset($data['item_cases_per_pallet_length']);
		}
		if(array_key_exists('item_cases_per_pallet_width', $data))
        {
            unset($data['item_cases_per_pallet_width']);
		}
		if(array_key_exists('item_no_of_layers', $data))
        {
            unset($data['item_no_of_layers']);
		}

		if(array_key_exists('unit_qty', $data))
        {
            unset($data['unit_qty']);
		}
		if(array_key_exists('item_unit_uom_code', $data))
        {
            unset($data['item_unit_uom_code']);
		}
		if(array_key_exists('loose_qty', $data))
        {
            unset($data['loose_qty']);
		}
		if(array_key_exists('item_loose_uom_code', $data))
        {
            unset($data['item_loose_uom_code']);
        }
        if(array_key_exists('loose_uom_id', $data))
        {
            unset($data['loose_uom_id']);
		}
		if(array_key_exists('loose_uom_rate', $data))
        {
            unset($data['loose_uom_rate']);
		}
		if(array_key_exists('case_qty', $data))
        {
            unset($data['case_qty']);
		}
		if(array_key_exists('case_uom_rate', $data))
        {
            unset($data['case_uom_rate']);
		}
        if(array_key_exists('item_case_uom_code', $data))
        {
            unset($data['item_case_uom_code']);
        }
        if(array_key_exists('uom_code', $data))
        {
            unset($data['uom_code']);
		}
		if(array_key_exists('item_unit_barcode', $data))
        {
            unset($data['item_unit_barcode']);
		}
		if(array_key_exists('item_case_barcode', $data))
        {
            unset($data['item_case_barcode']);
		}
		if(array_key_exists('gross_weight', $data))
        {
            unset($data['gross_weight']);
		}
		if(array_key_exists('cubic_meter', $data))
        {
            unset($data['cubic_meter']);
        }
        return $data;
	}
	
	static public function transitionToVoid($hdrId)
    {
		AuthService::authorize(
			array(
				'pick_list_revert'
			)
		);

		//use transaction to make sure this is latest value
		$hdrModel = PickListHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('PickList.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\PickListHdr::class, $hdrModel->id);
            throw $exc;
		}

		//revert the document
		$hdrModel = PickListHdrRepository::revertDraftToVoid($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('PickList.commit_to_void_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\PickListHdr::class, $hdrModel->id);
        throw $exc;
	}
	
	public function changeQuantBal($hdrId, $quantBalId)
    {
		AuthService::authorize(
			array(
				'pick_list_update'
			)
		);

		$pickListHdr = PickListHdrRepository::findByPk($hdrId);

		$results = array();
		$quantBal = QuantBalRepository::findByPk($quantBalId);
        if(!empty($quantBal))
        {
			$item = $quantBal->item;
			$itemBatch = $quantBal->itemBatch;
			$handlingUnit = $quantBal->handlingUnit;
			
			$quantBal = ItemService::processCaseLoose($quantBal, $item, 1);

			$results['item_id'] = $quantBal->item_id;
			$results['item_code'] = $quantBal->item_code;
			$results['item_desc_01'] = $quantBal->item_desc_01;
			$results['item_desc_02'] = $quantBal->item_desc_02;
			$results['desc_01'] = $quantBal->item_desc_01;
			$results['desc_02'] = $quantBal->item_desc_02;
			$results['case_qty'] = $quantBal->case_qty;
			$results['item_case_uom_code'] = $quantBal->item_case_uom_code;
            $results['batch_serial_no'] = empty($itemBatch) ? '' : $itemBatch->batch_serial_no;
            $results['expiry_date'] = empty($itemBatch) ? '' : $itemBatch->expiry_date;
            $results['receipt_date'] = empty($itemBatch) ? '' : $itemBatch->receipt_date;
            $results['handling_unit_barcode'] = empty($handlingUnit) ? '' : $handlingUnit->getBarcode();
			
		}

        return $results;
	}

	public function changeItemUom($hdrId, $itemId, $uomId)
    {
		AuthService::authorize(
			array(
				'pick_list_update'
			)
		);

        $pickListHdr = PickListHdrRepository::findByPk($hdrId);

        $results = array();
        $itemUom = ItemUomRepository::findByItemIdAndUomId($itemId, $uomId);
        if(!empty($itemUom)
        && !empty($pickListHdr))
        {
            $results['item_id'] = $itemUom->item_id;
            $results['uom_id'] = $itemUom->uom_id;
            $results['uom_rate'] = $itemUom->uom_rate;

			$results = $this->updateWarehouseItemUom($results, 0, 0);
        }
        return $results;
	}

	public function showHeader($hdrId)
    {
		AuthService::authorize(
			array(
				'pick_list_read',
				'pick_list_update'
			)
		);

        $model = PickListHdrRepository::findByPk($hdrId);
		if(!empty($model))
        {
            $model = self::processOutgoingHeader($model);
        }
        
        return $model;
	}
	
	public function showDetails($hdrId) 
	{
		AuthService::authorize(
			array(
				'pick_list_read',
				'pick_list_update'
			)
		);

		$pickListDtls = PickListDtlRepository::findAllByHdrId($hdrId);
		$pickListDtls->load(
            'item', 'item.itemUoms', 'item.itemUoms.uom'
        );
        foreach($pickListDtls as $pickListDtl)
        {
			$pickListDtl = self::processOutgoingDetail($pickListDtl);
        }
        return $pickListDtls;
	}

	static public function processIncomingHeader($data, $isClearHdrDiscTax=false)
    {
		if(array_key_exists('doc_flows', $data))
        {
            unset($data['doc_flows']);
        }
		if(array_key_exists('submit_action', $data))
        {
            unset($data['submit_action']);
        }
        //preprocess the select2 and dropDown
        
        if(array_key_exists('str_doc_status', $data))
        {
            unset($data['str_doc_status']);
		}
        
        return $data;
    }

	public function updateHeader($hdrData)
	{
		AuthService::authorize(
			array(
				'pick_list_update'
			)
		);

        $hdrData = self::processIncomingHeader($hdrData);

        $pickListHdr = PickListHdrRepository::txnFindByPk($hdrData['id']);
        if($pickListHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('PickList.doc_status_is_wip_or_complete', ['docCode'=>$pickListHdr->doc_code]));
            $exc->addData(\App\PickListHdr::class, $pickListHdr->id);
            throw $exc;
        }
        $pickListHdrData = $pickListHdr->toArray();
        //assign hdrData to model
        foreach($hdrData as $field => $value) 
        {
            $pickListHdrData[$field] = $value;
        }

		//$pickListHdr->load('worker01');
        //$worker01 = $pickListHdr->worker01;
		
		//no detail to update
        $pickListDtlArray = array();

        $result = PickListHdrRepository::updateDetails($pickListHdrData, $pickListDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];

        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = self::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('PickList.document_successfully_updated', ['docCode'=>$documentHeader->doc_code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}
	
	public function createDetail($hdrId, $data)
	{
		AuthService::authorize(
			array(
				'pick_list_update'
			)
		);

        $data = self::processIncomingDetail($data);
		//$data = $this->updateWarehouseItemUom($data, 0, 0);

        $pickListHdr = PickListHdrRepository::txnFindByPk($hdrId);
        if($pickListHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('PickList.doc_status_is_wip_or_complete', ['docCode'=>$pickListHdr->doc_code]));
            $exc->addData(\App\PickListHdr::class, $pickListHdr->id);
            throw $exc;
        }
		$pickListHdrData = $pickListHdr->toArray();

		$pickListDtlArray = array();
		$pickListDtlModels = PickListDtlRepository::findAllByHdrId($hdrId);
		foreach($pickListDtlModels as $pickListDtlModel)
		{
			$pickListDtlData = $pickListDtlModel->toArray();
			$pickListDtlData['is_modified'] = 0;
			$pickListDtlArray[] = $pickListDtlData;
		}
		$lastLineNo = count($pickListDtlModels);

		//assign temp id
		$tmpUuid = uniqid();
		//append NEW here to make sure it will be insert in repository later
		$data['id'] = 'NEW'.$tmpUuid;
		$data['line_no'] = $lastLineNo + 1;
		$data['is_modified'] = 1;
		
		$pickListDtlData = $this->initPickListDtlData($data);

		$pickListDtlArray[] = $pickListDtlData;

		$result = PickListHdrRepository::updateDetails($pickListHdrData, $pickListDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('PickList.detail_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function deleteDetails($hdrId, $dtlArray)
	{
		AuthService::authorize(
			array(
				'pick_list_update'
			)
		);

        $pickListHdr = PickListHdrRepository::txnFindByPk($hdrId);
        if($pickListHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('PickList.doc_status_is_wip_or_complete', ['docCode'=>$pickListHdr->doc_code]));
            $exc->addData(\App\PickListHdr::class, $pickListHdr->id);
            throw $exc;
        }
		$pickListHdrData = $pickListHdr->toArray();

		//query the pickListDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$delPickListDtlArray = array();
		$pickListDtlArray = array();
        $pickListDtlModels = PickListDtlRepository::findAllByHdrId($hdrId);
        $lineNo = 1;
		foreach($pickListDtlModels as $pickListDtlModel)
		{
			$pickListDtlData = $pickListDtlModel->toArray();
			$pickListDtlData['is_modified'] = 0;
			$pickListDtlData['is_deleted'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{
				if($dtlData['id'] == $pickListDtlData['id'])
				{
					//this is deleted dtl
					$pickListDtlData['is_deleted'] = 1;
					break;
				}
			}
			if($pickListDtlData['is_deleted'] > 0)
			{
				$delPickListDtlArray[] = $pickListDtlData;
			}
			else
			{
                $pickListDtlData['line_no'] = $lineNo;
                $pickListDtlData['is_modified'] = 1;
                $pickListDtlArray[] = $pickListDtlData;
                $lineNo++;
			}
        }
		
		$result = PickListHdrRepository::updateDetails($pickListHdrData, $pickListDtlArray, $delPickListDtlArray, array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = self::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('PickList.details_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>$delPickListDtlArray
            ),
			'message' => $message
		);
	}

	public function updateDetails($hdrId, $dtlArray)
	{
		AuthService::authorize(
			array(
				'pick_list_update'
			)
		);

		$pickListHdr = PickListHdrRepository::txnFindByPk($hdrId);
        if($pickListHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('PickList.doc_status_is_wip_or_complete', ['docCode'=>$pickListHdr->doc_code]));
            $exc->addData(\App\PickListHdr::class, $pickListHdr->id);
            throw $exc;
        }
		$pickListHdrData = $pickListHdr->toArray();

		//query the pickListDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$pickListDtlArray = array();
        $pickListDtlModels = PickListDtlRepository::findAllByHdrId($hdrId);
		foreach($pickListDtlModels as $pickListDtlModel)
		{
			$pickListDtlData = $pickListDtlModel->toArray();
			$pickListDtlData['is_modified'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{                
				if($dtlData['id'] == $pickListDtlData['id'])
				{
					$dtlData = self::processIncomingDetail($dtlData);

					foreach($dtlData as $fieldName => $value)
					{
						$pickListDtlData[$fieldName] = $value;
					}

					$pickListDtlData['is_modified'] = 1;

					break;
				}
			}
			$pickListDtlArray[] = $pickListDtlData;
		}
		
		$result = PickListHdrRepository::updateDetails($pickListHdrData, $pickListDtlArray, array(), array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = self::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('PickList.details_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function index($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
			array(
				'pick_list_read'
			)
		);

		$pickListHdrs = PickListHdrRepository::findAll($siteFlowId, $sorts, $filters, $pageSize);
		foreach($pickListHdrs as $pickListHdr)
		{
			$pickListHdr->str_doc_status = DocStatus::$MAP[$pickListHdr->doc_status];

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$pickListDtls = PickListDtlRepository::findAllByHdrId($pickListHdr->id);
			$pickListDtls->load('item', 'quantBal', 'quantBal.itemBatch', 'quantBal.storageBin', 'quantBal.storageRow', 'quantBal.storageBay');

			$jobPickListDtls = array();
			/*
			foreach($pickListDtls as $pickListDtl)
			{
				$item = $pickListDtl->item;
				$quantBal = $pickListDtl->quantBal;
				$itemBatch = null;
				$storageBin = null;
				$storageRow = null;
				$storageBay = null;
				if(!empty($quantBal))
				{
					$itemBatch = $quantBal->itemBatch;
					$storageBin = $quantBal->storageBin;
					$storageRow = $quantBal->storageRow;
					$storageBay = $quantBal->storageBay;
				}

				$whse_job_key = $pickListDtl->whse_job_type.':'.$pickListDtl->whse_job_code;
				$pickListDtl->whse_job_key = $whse_job_key;
				$pickListDtl->str_whse_job_type = WhseJobType::$MAP[$pickListDtl->whse_job_type];
				$pickListDtl->str_whse_job_type = __('WhseJob.'.strtolower($pickListDtl->str_whse_job_type));

				$pickListDtl->batch_serial_no = '';
				$pickListDtl->expiry_date = '';
				$pickListDtl->receipt_date = '';
				if(!empty($itemBatch))
				{
					$pickListDtl->batch_serial_no = $itemBatch->batch_serial_no;
					$pickListDtl->expiry_date = $itemBatch->expiry_date;
					$pickListDtl->receipt_date = $itemBatch->receipt_date;
				}

				$pickListDtl->storage_bin_code = '';
				if(!empty($storageBin))
				{
					$pickListDtl->storage_bin_code = $storageBin->code;
				}
				$pickListDtl->storage_row_code = '';
				if(!empty($storageRow))
				{
					$pickListDtl->storage_row_code = $storageRow->code;
				}
				$pickListDtl->storage_bay_code = '';
				$pickListDtl->item_bay_sequence = 0;
				if(!empty($storageBay))
				{
					$pickListDtl->storage_bay_code = $storageBay->code;
					$pickListDtl->item_bay_sequence = $storageBay->bay_sequence;
				}

				//calculate the pallet qty, case qty, gross weight, and m3
				if($pickListDtl->uom_id == Uom::$PALLET
        		&& bccomp($pickListDtl->uom_rate, 0, 5) == 0)
				{
					//this is pallet picking
					$pickListDtl->ttl_unit_qty = 0;
					$pickListDtl->case_qty = 0;
					$pickListDtl->gross_weight = 0;
					$pickListDtl->cubic_meter = 0;

					$handlingQuantBals = QuantBalRepository::findAllByHandlingUnitId($quantBal->handling_unit_id);
					$handlingQuantBals->load('item');
					foreach($handlingQuantBals as $handlingQuantBal)
					{
						$handlingItem = $handlingQuantBal->item;

						$handlingQuantBal = ItemService::processCaseLoose($handlingQuantBal, $handlingItem);

						$pickListDtl->unit_qty = bcadd($pickListDtl->unit_qty, $handlingQuantBal->balance_unit_qty, 10);
						$pickListDtl->case_qty = bcadd($pickListDtl->case_qty, $handlingQuantBal->case_qty, 10);
						$pickListDtl->gross_weight = bcadd($pickListDtl->gross_weight, $handlingQuantBal->gross_weight, 10);
						$pickListDtl->cubic_meter = bcadd($pickListDtl->cubic_meter, $handlingQuantBal->cubic_meter, 10);
					}

					$caseQty = bcadd($caseQty, $pickListDtl->case_qty, 8);
					$grossWeight = bcadd($grossWeight, $pickListDtl->gross_weight, 8);
					$cubicMeter = bcadd($cubicMeter, $pickListDtl->cubic_meter, 8);
				}
				else
				{
					$pickListDtl = ItemService::processCaseLoose($pickListDtl, $item);

					$caseQty = bcadd($caseQty, $pickListDtl->case_qty, 8);
					$grossWeight = bcadd($grossWeight, $pickListDtl->gross_weight, 8);
					$cubicMeter = bcadd($cubicMeter, $pickListDtl->cubic_meter, 8);
				}

				if($pickListDtl->handling_unit_id > 0)
				{
					$palletHashByHandingUnitId[$pickListDtl->handling_unit_id] = $pickListDtl->handling_unit_id;
				}

				unset($pickListDtl->quantBal);
				unset($pickListDtl->item);
				$jobPickListDtls[] = $pickListDtl;
			}
			*/

			$pickListHdr->pallet_qty = count($palletHashByHandingUnitId);
			$pickListHdr->case_qty = $caseQty;
			$pickListHdr->gross_weight = $grossWeight;
			$pickListHdr->cubic_meter = $cubicMeter;

			$pickListHdr->details = $jobPickListDtls;
		}
    	return $pickListHdrs;
	}
	
	public function repostAll()
    {
		//fix action, so not follow pattern
		//find all complete pick list
		$pickListHdrs = PickListHdr::select('pick_list_hdrs.*')
			->where('doc_status', 100)
			->get();

		//loop to revert to draft
		foreach($pickListHdrs as $pickListHdr)
		{	
			$hdrModel = self::transitionToDraft($pickListHdr->id);
		}
		//loop to commit to complete
		foreach($pickListHdrs as $pickListHdr)
		{	
			$hdrModel = self::transitionToComplete($pickListHdr->id);
		}

        return array(
			'data' => array(),
			'message' => 'success'
		);
	}
}
