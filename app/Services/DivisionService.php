<?php

namespace App\Services;

use App\Division;
use App\Services\Env\TxnFlowType;
use App\Services\Env\ProcType;
use App\Services\Env\ScanMode;
use App\Services\Env\ResType;
use App\Services\Env\DocStatus;
use App\Repositories\DivisionRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\InvTxnFlowRepository;
use Illuminate\Support\Facades\Auth;

class DivisionService 
{
	public function __construct() 
	{
    }
    
    public function index($sorts, $filters = array(), $pageSize = 20)
	{
        // AuthService::authorize(
        //     array(
        //         'user_read'
        //     )
        // );

        $divisions = DivisionRepository::findAll($sorts, $filters, $pageSize);
        
        foreach($divisions as $division)
        {
            if (!empty($division->company_id)) {
                $company = CompanyRepository::findByPk($division->company_id);
                if(!empty($company))
                {
                    $division->company_code = $company->code;
                    $division->company_name_01 = $company->name_01;
                    $division->company_name_02 = $company->name_02;
                }
            }
        }

    	return $divisions;
    }

    public function indexByUser() 
	{
        $user = Auth::user();

        $divisions = DivisionRepository::findAllByUserId($user->id);
        
        return $divisions;
    }
    
    public function indexPurchaseFlow($divisionId) 
	{
        $user = Auth::user();

        $invTxnFlows = array();
        $divisionIds = array();
        /*
        $divisions = DivisionRepository::findAllByUserId($userId);
        foreach($divisions as $division)
        {
            $divisionIds[] = $division->id;
        }

        if(in_array($divisionId, $divisionIds) === FALSE)
        {
            //return empty array is user do not have permission to this site
            return $invTxnFlows;
        }
        */
        $invTxnFlows = InvTxnFlowRepository::findAllByDivisionIdAndTxnFlowType($divisionId, TxnFlowType::$MAP['PURCHASE']);
        foreach($invTxnFlows as $invTxnFlow)
        {
            $invTxnFlow->txn_flow_type = TxnFlowType::$MAP[$invTxnFlow->txn_flow_type];
            $invTxnFlow->proc_type = ProcType::$MAP[$invTxnFlow->proc_type];
            $invTxnFlow->scan_mode = ScanMode::$MAP[$invTxnFlow->scan_mode];
            $invTxnFlow->fr_res_type = ResType::$MAP[$invTxnFlow->fr_res_type];
            $invTxnFlow->to_res_type = ResType::$MAP[$invTxnFlow->to_res_type];
            $invTxnFlow->to_doc_status = DocStatus::$MAP[$invTxnFlow->to_doc_status];
        }
        return $invTxnFlows;
    }

    public function indexSalesFlow($divisionId) 
	{
        $user = Auth::user();

        $invTxnFlows = array();
        $divisionIds = array();
        /*
        $divisions = DivisionRepository::findAllByUserId($userId);
        foreach($divisions as $division)
        {
            $divisionIds[] = $division->id;
        }

        if(in_array($divisionId, $divisionIds) === FALSE)
        {
            //return empty array is user do not have permission to this site
            return $invTxnFlows;
        }
        */
        $invTxnFlows = InvTxnFlowRepository::findAllByDivisionIdAndTxnFlowType($divisionId, TxnFlowType::$MAP['SALES']);
        foreach($invTxnFlows as $invTxnFlow)
        {
            $invTxnFlow->txn_flow_type = TxnFlowType::$MAP[$invTxnFlow->txn_flow_type];
            $invTxnFlow->proc_type = ProcType::$MAP[$invTxnFlow->proc_type];
            $invTxnFlow->scan_mode = ScanMode::$MAP[$invTxnFlow->scan_mode];
            $invTxnFlow->fr_res_type = ResType::$MAP[$invTxnFlow->fr_res_type];
            $invTxnFlow->to_res_type = ResType::$MAP[$invTxnFlow->to_res_type];
            $invTxnFlow->to_doc_status = DocStatus::$MAP[$invTxnFlow->to_doc_status];
        }
        return $invTxnFlows;
    }

    public function indexSalesReturnFlow($divisionId) 
	{
        $user = Auth::user();

        $invTxnFlows = array();
        $divisionIds = array();
        /*
        $divisions = DivisionRepository::findAllByUserId($userId);
        foreach($divisions as $division)
        {
            $divisionIds[] = $division->id;
        }

        if(in_array($divisionId, $divisionIds) === FALSE)
        {
            //return empty array is user do not have permission to this site
            return $invTxnFlows;
        }
        */
        $invTxnFlows = InvTxnFlowRepository::findAllByDivisionIdAndTxnFlowType($divisionId, TxnFlowType::$MAP['SALES_RETURN']);
        foreach($invTxnFlows as $invTxnFlow)
        {
            $invTxnFlow->txn_flow_type = TxnFlowType::$MAP[$invTxnFlow->txn_flow_type];
            $invTxnFlow->proc_type = ProcType::$MAP[$invTxnFlow->proc_type];
            $invTxnFlow->scan_mode = ScanMode::$MAP[$invTxnFlow->scan_mode];
            $invTxnFlow->fr_res_type = ResType::$MAP[$invTxnFlow->fr_res_type];
            $invTxnFlow->to_res_type = ResType::$MAP[$invTxnFlow->to_res_type];
            $invTxnFlow->to_doc_status = DocStatus::$MAP[$invTxnFlow->to_doc_status];
        }
        return $invTxnFlows;
    }

    public function select2($search, $filters)
    {
        $user = Auth::user();

        $divisionIds = array();
        $userDivisions = $user->userDivisions;
        foreach($userDivisions as $userDivision)
        {
            $divisionIds[] = $userDivision->division_id;
        }

        //DB::connection()->enableQueryLog();
        $divisions = DivisionRepository::select2($divisionIds, $search, $filters);
        //dd(DB::getQueryLog());
        return $divisions;
    }

    static public function processIncomingModel($data)
    {
        if(array_key_exists('submit_action', $data))
        {
            unset($data['submit_action']);
        }
        
        //preprocess the select2 and dropDown
        if(array_key_exists('company_id_select2', $data))
        {
            $data['company_id'] = $data['company_id_select2']['value'];
            unset($data['company_id_select2']);
        }
        if(array_key_exists('company', $data))
        {
            unset($data['company']);
        }
        if(array_key_exists('company_code', $data))
        {
            unset($data['company_code']);
        }
        if(array_key_exists('company_name_01', $data))
        {
            unset($data['company_name_01']);
        }
        if(array_key_exists('company_name_02', $data))
        {
            unset($data['company_name_02']);
        }

        return $data;
    }

    static public function processOutgoingModel($model)
	{
        $company = $model->company;
        $model->company_code = $company->code;
        $model->company_name_01 = $company->name_01;
        $model->company_name_02 = $company->name_02;
        
        //company id select2
        $initCompanyOption = array('value'=>0,'label'=>'');
        $company = CompanyRepository::findByPk($model->company_id);
        if(!empty($company))
        {
            $initCompanyOption = array('value'=>$company->id, 'label'=>$company->code . ' ' . $company->company_name_01);
        }
        $model->company_id_select2 = $initCompanyOption;

		return $model;
    }

    public function initModel()
    {
        // AuthService::authorize(
        //     array(
        //         'item_create'
        //     )
        // );

        $model = new Division();
        $model->code = '';
        $model->site_id = 0;
        $model->site_flow_id = 0;
        $model->ref_code_01 = '';
        $model->name_01 = '';
        $model->name_02 = '';

        return $model;
    }
    
    public function createModel($data)
    {		
        // AuthService::authorize(
        //     array(
        //         'item_create'
        //     )
        // );

        $data = self::processIncomingModel($data, true);
        $model = DivisionRepository::createModel($data);

        //create a sync hdr setting for new division

        $message = __('Division.record_successfully_created', ['docCode'=>$model->code]);

		return array(
            'data' => $model->id,
			'message' => $message
		);
    }
    
    public function showModel($itemId)
    {
        // AuthService::authorize(
        //     array(
        //         'item_read',
		// 		'item_update'
        //     )
        // );

        $model = DivisionRepository::findByPk($itemId);
		if(!empty($model))
        {
            $model = self::processOutgoingModel($model);
        }
        
        return $model;
    }

    public function updateModel($data)
	{
        // AuthService::authorize(
        //     array(
        //         'item_update'
        //     )
        // );

        $data = self::processIncomingModel($data);

        $item = DivisionRepository::findByPk($data['id']);
        $itemData = $item->toArray();
        //assign data to model
        foreach($data as $field => $value) 
        {
            $itemData[$field] = $value;
        }

        $result = DivisionRepository::updateModel($itemData);
        $model = $result['model'];

        $model = self::processOutgoingModel($model);

        $message = __('Division.record_successfully_updated', ['code'=>$model->code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'model'=>$model
            ),
			'message' => $message
		);
    }
}