<?php

namespace App\Services;

use App\ItemUom;
use App\Item;
use App\Uom;
use App\Division;

use App\Services\Env\ProcType;
use App\Services\Env\ResStatus;
use App\Services\Env\ScanMode;
use App\Services\Env\RetrievalMethod;
use App\Services\Env\ItemType;
use App\Services\Env\StorageClass;
use App\Services\Env\PromotionRuleType;
use App\Repositories\BatchJobStatusRepository;
use App\Repositories\ItemRepository;
use App\Repositories\UserDivisionRepository;
use App\Repositories\ItemDivisionRepository;
use App\Repositories\ItemUomRepository;
use App\Repositories\ItemSalePriceRepository;
use App\Repositories\ItemPhotoRepository;
use App\Repositories\SyncSettingHdrRepository;
use App\Repositories\SyncSettingDtlRepository;
use App\Repositories\SiteFlowRepository;
use App\Repositories\PromotionRepository;
use App\Repositories\ItemGroup01Repository;
use App\Repositories\ItemGroup02Repository;
use App\Repositories\ItemGroup03Repository;
use App\BatchJobStatus;
use App\Imports\ItemExcel01Import;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;
use App\Services\Utils\SimpleImage;
use Milon\Barcode\DNS2D;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Libs\Promotion\DebtorChecker;
use App\Libs\Promotion\DebtorGroupChecker;

class ItemService
{
    public function __construct()
    {
    }

    public static function calculatePalletConfiguration($sitePalletConfig, $item)
    {
        //helper function for item pallet configuration
        $result = array();
        /*
        last time use pallet m3 to calculate
        //support basic first, only height can allowed to be vertical
        $caseExtLength = 1;
        $caseExtWidth = 1;
        $caseExtHeight = 1;
        if($item->case_ext_length > 0)
        {
            $caseExtLength = $item->case_ext_length;
            $caseExtWidth = $item->case_ext_width;
            $caseExtHeight = $item->case_ext_height;
        }
        $result['max_cases_per_pallet_length'] = bcdiv($sitePalletConfig['pallet_max_load_length'], $caseExtLength, 0);
        $result['max_cases_per_pallet_width'] = bcdiv($sitePalletConfig['pallet_max_load_width'], $caseExtWidth, 0);
        $result['max_no_of_layers'] = bcdiv($sitePalletConfig['pallet_max_load_height'], $caseExtHeight, 0);
        $result['max_ttl_cases_per_pallet'] = bcmul($result['max_no_of_layers'], bcmul($result['max_cases_per_pallet_length'], $result['max_cases_per_pallet_width'], 0), 0);
        $result['max_ttl_unit_per_pallet'] = bcmul($result['max_ttl_cases_per_pallet'], $item->case_uom_rate, 0);

        $result['min_cases_per_pallet_length'] = bcdiv($sitePalletConfig['pallet_min_load_length'], $caseExtLength, 0);
        $result['min_cases_per_pallet_width'] = bcdiv($sitePalletConfig['pallet_min_load_width'], $caseExtWidth, 0);
        $result['min_no_of_layers'] = bcdiv($sitePalletConfig['pallet_min_load_height'], $caseExtHeight, 0);
        $result['min_ttl_cases_per_pallet'] = bcmul($result['min_no_of_layers'], bcmul($result['min_cases_per_pallet_length'], $result['min_cases_per_pallet_width'], 0), 0);
        $result['min_ttl_unit_per_pallet'] = bcmul($result['min_ttl_cases_per_pallet'], $item->case_uom_rate, 0);
        */

        $casesPerPalletLength = 1;
        if($item->cases_per_pallet_length > 0)
        {
            $casesPerPalletLength = $item->cases_per_pallet_length;
        }
        $casesPerPalletWidth = 1;
        if($item->cases_per_pallet_width > 0)
        {
            $casesPerPalletWidth = $item->cases_per_pallet_width;
        }
        $noOfLayers = 1;
        if($item->no_of_layers > 0)
        {
            $noOfLayers = $item->no_of_layers;
        }

        $ttlCasesPerPallet = bcmul($casesPerPalletLength, $casesPerPalletWidth, 0);
        $ttlCasesPerPallet = bcmul($ttlCasesPerPallet, $noOfLayers, 0);
        $result['max_cases_per_pallet_length'] = $casesPerPalletLength;
        $result['max_cases_per_pallet_width'] = $casesPerPalletWidth;
        $result['max_no_of_layers'] = $noOfLayers;
        $result['max_ttl_cases_per_pallet'] = $ttlCasesPerPallet;
        $result['max_ttl_unit_per_pallet'] = bcmul($ttlCasesPerPallet, $item->case_uom_rate, 0);

        $result['min_cases_per_pallet_length'] = 0;
        $result['min_cases_per_pallet_width'] = 0;
        $result['min_no_of_layers'] = 0;
        $result['min_ttl_cases_per_pallet'] = 0;
        $result['min_ttl_unit_per_pallet'] = 0;
        return $result;
    }

    public static function select2Item($data)
    {
        $items = Item::
            orWhere('desc_01', 'like', '%' . $data . '%')
            ->orWhere('code', 'like', '%' . $data . '%')
            ->paginate(3);

        return $items;
    }

    public static function select2UpdateItem($data)
    {
        $item = Item::where('id',$data)
            ->with('itemUoms.uom')
            ->first();
        return $item;
    }

    public function uploadProcess($strProcType, $siteFlowId, $divisionId, $file)
    {
        $user = Auth::user();

        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['ITEM_EXCEL_01'])
			{
                //process item excel
                $path = Storage::putFileAs('upload/', $file, 'ITEM_EXCEL_01.XLSX');
				return $this->uploadItemExcel01($siteFlowId, $divisionId, $user->id, $path);
			}
		}
    }

    public function uploadItemExcel01($siteFlowId, $divisionId, $userId, $path)
    {
        AuthService::authorize(
            array(
                'item_import'
            )
        );

        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['ITEM_EXCEL_01'], $userId);

        $siteFlow = SiteFlowRepository::findByPk($siteFlowId);

        //count total first
        $itemExcel01Import = new ItemExcel01Import($siteFlow->site_id, $userId, $batchJobStatusModel);
        $itemExcel01Import->setIsCount(true);
        $excel = Excel::import($itemExcel01Import, $path);

        //process row now
        $itemExcel01Import->setIsCount(false);
        $excel = Excel::import($itemExcel01Import, $path);

        $message = __('Item.file_successfully_uploaded', ['total'=>$itemExcel01Import->getTotal()]);

		return array(
			'data' => $itemExcel01Import->getTotal(),
			'message' => $message
		);
    }

    public function syncProcess($strProcType, $siteFlowId, $divisionId)
    {
        $user = Auth::user();

        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['ITEM_SYNC_01'])
			{
				//sync items from EfiChain
				return $this->syncItemSync01($user->id, $divisionId);
            }
            elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['ITEM_SYNC_01_01'])
			{
				//sync item photos from EfiChain
				return $this->syncItemSync0101($user->id, $divisionId);
            }
            elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['ITEM_SYNC_01_02'])
			{
				//download item photos from local upload folder
				return $this->syncItemSync0102($siteFlowId, $user->id);
			}
		}
    }

    protected function syncItemSync01($userId, $divisionId)
    {
        AuthService::authorize(
            array(
                'item_import'
            )
        );

        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['ITEM_SYNC_01'], $userId);

        $syncSettingHdr = SyncSettingHdrRepository::findByProcTypeAndDivisionId(ProcType::$MAP['SYNC_EFICHAIN_01'], $divisionId);
        if(empty($syncSettingHdr))
        {
            $exc = new ApiException(__('Item.sync_setting_not_found', ['divisionId'=>$divisionId]));
            //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
            throw $exc;
        }

        $client = new Client();
        $header = array();

        $page = 1;
        $lastPage = 1;
        $pageSize = 1000;
        $total = 0;

        $usernameSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'username');
        $passwordSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'password');
        $formParams = array(
            'UserLogin[username]' => $usernameSetting->value,
            'UserLogin[password]' => $passwordSetting->value
        );
        $pageSizeSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'page_size');
        if(!empty($pageSizeSetting))
        {
            $pageSize = $pageSizeSetting->value;
        }

        //use division code
        $divisionDB = Division::where('id', $divisionId)->first();

        while($page <= $lastPage)
        {
            $url = $syncSettingHdr->url.'/index.php?r=bizOrder/getItems&division='.$divisionDB->code.'&page_size='.$pageSize.'&page='.$page;

            $response = $client->request('POST', $url, array('headers' => $header, 'form_params' => $formParams));
            $result = $response->getBody()->getContents();
            $result = json_decode($result, true);
            if(empty($result))
            {
                $exc = new ApiException(__('Item.fail_to_sync', ['url'=>$url]));
                //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
                throw $exc;
            }

            $total = $result['total'];
            for ($a = 0; $a < count($result['data']); $a++)
            {
                $itemData = $result['data'][$a];
                $itemModel = ItemRepository::syncItemSync01($itemData, $divisionId, $syncSettingHdr->id);

                $statusNumber = 100;
                if($total > 0)
                {
                    $statusNumber = bcmul(bcdiv(($a + ($page - 1) * $pageSize), $total, 8), 100, 2);
                }
                BatchJobStatusRepository::updateStatusNumber($batchJobStatusModel->id, $statusNumber);
            }

            $lastPage = $total % $pageSize == 0 ? ($total / $pageSize) : ($total / $pageSize) + 1;

            $page++;
        }

        $batchJobStatusModel = BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

        $message = __('Item.sync_success', ['total'=>$total]);
		return array(
			'data' => array(
                'timestamp'=>time(),
                'batchJobStatusModel'=>$batchJobStatusModel
            ),
			'message' => $message
		);
    }

    protected function syncItemSync0101($userId, $divisionId)
    {
        AuthService::authorize(
            array(
                'item_import'
            )
        );

        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['ITEM_SYNC_01'], $userId);

        $syncSettingHdr = SyncSettingHdrRepository::findByProcTypeAndDivisionId(ProcType::$MAP['SYNC_EFICHAIN_01'], $divisionId);
        if(empty($syncSettingHdr))
        {
            $exc = new ApiException(__('Item.sync_setting_not_found', ['divisionId'=>$divisionId]));
            //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
            throw $exc;
        }

        $client = new Client();
        $header = array();

        $page = 1;
        $lastPage = 1;
        $pageSize = 1000;
        $total = 0;

        $usernameSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'username');
        $passwordSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'password');
        $formParams = array(
            'UserLogin[username]' => $usernameSetting->value,
            'UserLogin[password]' => $passwordSetting->value
        );
        $pageSizeSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'page_size');
        if(!empty($pageSizeSetting))
        {
            $pageSize = $pageSizeSetting->value;
        }

        //use division code
        $divisionDB = Division::where('id', $divisionId)->first();

        while($page <= $lastPage)
        {
            $url = $syncSettingHdr->url.'/index.php?r=bizOrder/getItemImages&division='.$divisionDB->code.'&page_size='.$pageSize.'&page='.$page;

            $response = $client->post($url, array('headers' => $header, 'form_params' => $formParams));
            $result = $response->getBody()->getContents();
            $result = json_decode($result, true);
            if(empty($result))
            {
                $exc = new ApiException(__('Item.fail_to_sync', ['url'=>$url]));
                //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
                throw $exc;
            }

            $total = $result['total'];
            for ($a = 0; $a < count($result['data']); $a++)
            {
                $syncPhotoData = $result['data'][$a];
                if(empty($syncPhotoData['description']))
                {
                    //must have description to identify item image has been sync
                    continue;
                }
                //set the image download link
                $syncPhotoData['url'] = $syncSettingHdr->url.'/index.php?r=bizOrder/loadImage&id='.$syncPhotoData['id'];

                $itemModel = ItemRepository::findByCodeAndDivision($syncPhotoData['item_code'], $divisionId);
                if(empty($itemModel))
                {
                    continue;
                }
                $itemPhoto = ItemPhotoRepository::findByDesc01($itemModel->id, $syncPhotoData['description']);
                if (!empty($itemPhoto))
                {
                    //delete
                    $delItemPhotoDataArray = array();
                    $delItemPhotoData = array();
                    $delItemPhotoData['item_photo_id'] = $itemPhoto->id;
                    $delItemPhotoDataArray[] = $delItemPhotoData;
                    $deletedItemPhotoModelArray = ItemPhotoRepository::deleteItemPhotos($itemModel->id, $delItemPhotoDataArray);
                }

                $itemPhotoData = array();
                $itemPhotoData['item_id'] = $itemModel->id;
                $itemPhotoData['desc_01'] = $syncPhotoData['description'];
                $itemPhotoData['desc_02'] = $syncPhotoData['description1'];
                // $itemPhotoData['old_filename'] = $itemModel->id;
                //call to server to get the image
                $imgResponse = $client->post($syncPhotoData['url'], array('headers' => $header, 'form_params' => $formParams));
                $imgResult = $imgResponse->getBody()->getContents();
                $itemPhotoData['blob'] = $imgResult;
                $itemPhotoModel = ItemPhotoRepository::saveItemPhoto($itemPhotoData);

                $statusNumber = 100;
                if($total > 0)
                {
                    $statusNumber = bcmul(bcdiv(($a + ($page - 1) * $pageSize), $total, 8), 100, 2);
                }
                BatchJobStatusRepository::updateStatusNumber($batchJobStatusModel->id, $statusNumber);
            }

            $lastPage = $total % $pageSize == 0 ? ($total / $pageSize) : ($total / $pageSize) + 1;

            $page++;
        }

        $batchJobStatusModel = BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

        $message = __('Item.sync_image_success', ['total'=>$total]);
		return array(
			'data' => array(
                'timestamp'=>time(),
                'batchJobStatusModel'=>$batchJobStatusModel
            ),
			'message' => $message
		);
    }

    // protected function syncItemSync0102($siteFlowId, $userId)
    // {
    //     AuthService::authorize(
    //         array(
    //             'item_import'
    //         )
    //     );

    //     $files = Storage::allFiles('temp/');
    //     foreach($files as $file)
    //     {
    //         $fileParts = explode('/', $file);
    //         $fileName = $fileParts[count($fileParts) - 1];

    //         $fileParts = explode('.', $fileName);
    //         $itemCode = $fileParts[0];
    //         $itemModel = ItemRepository::findByCode($itemCode);
    //         if(empty($itemModel))
    //         {
    //             continue;
    //         }

    //         $imgData = Storage::get($file);
    //         $image = new SimpleImage();
    //         $image->loadFromString($imgData);
    //         $oldWidth = $image->getWidth();
    //         $oldHeight = $image->getHeight();
    //         if($oldWidth > 2048
    //         || $oldHeight > 2048)
    //         {
    //             $image->resizeProportionally(2048, 2048);
    //             ob_start();
    //             $image->output();
    //             $imgData = ob_get_clean();
    //         }

    //         $itemPhotoData = array();
    //         $itemPhotoData['item_id'] = $itemModel->id;
    //         $itemPhotoData['uom_id'] = 0;
    //         $itemPhotoData['blob'] = $imgData;
    //         $itemPhotoData['desc_01'] = '';
    //         $itemPhotoData['desc_02'] = '';
    //         $itemPhotoData['old_filename'] = $fileName;
    //         $itemPhotoData['old_width'] = $oldWidth;
    //         $itemPhotoData['old_height'] = $oldHeight;

    //         $itemPhotoModel = ItemPhotoRepository::syncItemSync0101($itemPhotoData);
    //     }

    //     return null;
    // }

    public function indexProcess($strProcType, $sorts, $filters = array(), $pageSize = 20)
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['ITEM_LIST_01'])
			{
				//item listing
				$items = $this->indexItemList01($sorts, $filters, $pageSize);
				return $items;
            }
			else if(ProcType::$MAP[$strProcType] == ProcType::$MAP['ITEM_LIST_02'])
			{
				//item listing
				$items = $this->indexItemList02($sorts, $filters, $pageSize);
				return $items;
			}
		}
    }

    public function indexDivision($divisionId, $sorts, $filters = array(), $pageSize = 20,$status)
	{
        //item listing
        $items = $this->indexItemList01($sorts, $filters, $pageSize, $divisionId,$status);
        return $items;
	}

	protected function indexItemList01($sorts, $filters = array(), $pageSize = 20, $divisionId = 0,$status)
	{
        // AuthService::authorize(
        //     array(
        //         'item_read'
        //     )
        // );

		$items = ItemRepository::findAllWithDeleteFilterStatus($sorts, $filters, $pageSize, $divisionId,$status);

        $items->load('unitUom', 'caseUom');

		//query the docDtls, and format into group details
		foreach($items as $item)
		{
            $item = self::processOutgoingModel($item);
		}
        return $items;
    }

	protected function indexItemList02($sorts, $filters = array(), $pageSize = 20)
	{
        // AuthService::authorize(
        //     array(
        //         'item_read'
        //     )
        // );

        $user = Auth::user();
        $userId = $user->id;
        $debtorId = $user->debtor_id;

        //always filter by active status only
        // $statusFilter = array();
        // $statusFilter['field'] = 'status';
        // $statusFilter['value'] = ResStatus::$MAP['ACTIVE'];
        // $filters = array_merge($filters, $statusFilter);
        $filters[] = array(
            'field' => 'status',
            'value' => ResStatus::$MAP['ACTIVE']
        );

		$items = ItemRepository::findAll($sorts, $filters, $pageSize);

        $items->load('unitUom', 'caseUom', 'itemPrices', 'itemPhotos');
		//query the docDtls, and format into group details
		foreach($items as $item)
		{
            $item->str_storage_class = StorageClass::$MAP[$item->storage_class];
            $item->str_item_type = ItemType::$MAP[$item->item_type];
            $item->str_scan_mode = ScanMode::$MAP[$item->scan_mode];
            $item->str_retrieval_method = RetrievalMethod::$MAP[$item->retrieval_method];
            $item->str_status = ResStatus::$MAP[$item->status];

            $itemGroup01 = $item->itemGroup01;
            $item->item_group_01_code = !empty($itemGroup01) ? $itemGroup01->code : '';

            $itemGroup02 = $item->itemGroup02;
            $item->item_group_02_code = !empty($itemGroup02) ? $itemGroup02->code : '';

            $itemGroup03 = $item->itemGroup03;
            $item->item_group_03_code = !empty($itemGroup03) ? $itemGroup03->code : '';

            /*

            $itemGroup04 = $item->itemGroup04;
            $item->item_group_04_code = !empty($itemGroup04) ? $itemGroup04->code : '';

            $itemGroup05 = $item->itemGroup05;
            $item->item_group_05_code = !empty($itemGroup05) ? $itemGroup05->code : '';
            */

            $unitUom = $item->unitUom;
            $item->unit_uom_code = !empty($unitUom) ? $unitUom->code : '';

            $caseUom = $item->caseUom;
            $item->case_uom_code = !empty($caseUom) ? $caseUom->code : '';

            //query itemUom
            $itemUoms = ItemUomRepository::findAllByItemId($item->id);
            $itemUoms->load('uom');
            foreach($itemUoms as $itemUom)
            {
                $uom = $itemUom->uom;
                $itemUom->uom_code = !empty($uom) ? $uom->code : '';

                //query itemPrice
                $salePrice = ItemSalePriceRepository::findByItemIdAndUomId(now(),0,1,$item->id,$itemUom->uom_id);
                $itemUom->salePrice = $salePrice;
                unset($itemUom->uom);
            }

            $item->details = $itemUoms;

            $promotions = PromotionRepository::findAllActiveByItem($item->id);
            if (!empty($debtorId)) {
                //UPDATE: Old method causes error in mobile side where returned list of promotions per item have indexes in them which disrupts the logic in mobile side when looping.
                //check promotion rule eligible
                $filtered_promotions = [];
                foreach($promotions as $promotion) {
                    $rules = $promotion->rules;
                    if($rules->isEmpty()) {
                        //empty rules means all are entitled for promotion
                        $filtered_promotions[] = $promotion;
                        continue;
                    } else {
                        $data = ['debtor_id' => $debtorId];
                        foreach($rules as $rule) {
                            //failing any one of the rules will return false
                            //DebtorChecker
                            if($rule->type == PromotionRuleType::$MAP['debtor']) {
                                if((new DebtorChecker($data))->isEligible($rule->config)) {
                                    $filtered_promotions[] = $promotion;
                                    break;
                                }
                            }
                            if($rule->type == PromotionRuleType::$MAP['debtor_group']) {
                                if((new DebtorGroupChecker($data))->isEligible($rule->config)) {
                                    $filtered_promotions[] = $promotion;
                                    break;
                                }
                            }
                        }
                    }
                }
                // $promotions = $promotions->filter(function ($promotion) use ($debtorId) {
                //     $rules = $promotion->rules;
                //     if ($rules->isEmpty()) {
                //         //empty rules means all are entitled for promotion
                //         return true;
                //     }
                //     $data = ['debtor_id'=>$debtorId];
                //     foreach ($rules as $rule) {
                //         // failing any one of rules will return false
                //         // DebtorChecker
                //         if ($rule->type == PromotionRuleType::$MAP['debtor']) {
                //             return (new DebtorChecker($data))->isEligible($rule->config);
                //         }
                //         if ($rule->type == PromotionRuleType::$MAP['debtor_group']) {
                //             return (new DebtorGroupChecker($data))->isEligible($rule->config);
                //         }
                //     }
                // });
            }

            $item->promotion = ItemService::processOutgoingPromotions($item, $filtered_promotions);

            /*
            unset($item->itemGroup01);
            unset($item->itemGroup02);
            unset($item->itemGroup03);
            unset($item->itemGroup04);
            unset($item->itemGroup05);
            */
            unset($item->unitUom);
            unset($item->caseUom);

		}
        return $items;
	}

    public static function getItemUom($item_id){
        $itemUoms = ItemUom::where('item_id',$item_id)->with('uom')->get();
        $uoms = [];
        foreach ($itemUoms as $key => $value) {
            $uoms[]=[
                "name"=>$value['uom']['code'],
                "code"=>$value['uom']['id'],
                "uom_rate"=>$value['uom_rate']
            ];
        }

        return $uoms;
    }

    public function select2($search, $filters)
    {
        //DB::connection()->enableQueryLog();
        $items = ItemRepository::select2($search, $filters);
        //dd(DB::getQueryLog());
        return $items;
    }

    public function select2Init($id)
    {
        $item = null;
        if($id > 0)
        {
            $item = ItemRepository::findByPk($id);
        }
        else
        {
            $item = ItemRepository::findTop();
        }
        $option = array('value'=>0, 'label'=>'');
        if(!empty($item))
        {
            $option = array(
                'value'=>$item->id,
                'label'=>$item->code.' '.$item->desc_01
            );
        }
        return $option;
    }

    public static function processCaseLoose($dtlModel, $item, $mode = 0, $isShowBarcode = true)
    {
        $dtlModel->item_code = '';
        $dtlModel->item_ref_code_01 = '';
        $dtlModel->item_desc_01 = '';
        $dtlModel->item_desc_02 = '';
        $dtlModel->storage_class = '';
        $dtlModel->item_group_01_code = '';
        $dtlModel->item_group_02_code = '';
        $dtlModel->item_group_03_code = '';
        $dtlModel->item_group_04_code = '';
        $dtlModel->item_group_05_code = '';
        $dtlModel->item_cases_per_pallet_length = 1;
        $dtlModel->item_cases_per_pallet_width = 1;
        $dtlModel->item_no_of_layers = 1;

        if(!empty($item))
        {
            $dtlModel->item_code = $item->code;
            $dtlModel->item_ref_code_01 = $item->ref_code_01;
            $dtlModel->item_desc_01 = $item->desc_01;
            $dtlModel->item_desc_02 = $item->desc_02;
            $dtlModel->storage_class = $item->storage_class;
            $dtlModel->item_group_01_code = $item->item_group_01_code;
            $dtlModel->item_group_02_code = $item->item_group_02_code;
            $dtlModel->item_group_03_code = $item->item_group_03_code;
            $dtlModel->item_group_04_code = $item->item_group_04_code;
            $dtlModel->item_group_05_code = $item->item_group_05_code;
            $dtlModel->item_cases_per_pallet_length = $item->cases_per_pallet_length;
            $dtlModel->item_cases_per_pallet_width = $item->cases_per_pallet_width;
            $dtlModel->item_no_of_layers = $item->no_of_layers;
        }

        $dtlModel->item_unit_barcode = '';
		$dtlModel->item_case_barcode = '';
		//process the case_uom_code, unit_uom_code, calculate the loose_qty and lose_uom_code
		$dtlModel->item_unit_uom_code = '';
		$dtlModel->item_case_uom_code = '';
		$dtlModel->item_loose_uom_code = '';
		$dtlModel->loose_uom_rate = 1;
		$dtlModel->loose_qty = 0;
        //calculate the pallet qty, case qty, gross weight, and m3

        $caseExtLength = 0;
        $caseExtWidth = 0;
        $caseExtHeight = 0;
        $caseUomRate = 1;
        $caseGrossWeight = 0;
        if(!empty($item))
        {
            $caseExtLength = $item->case_ext_length;
            $caseExtWidth = $item->case_ext_width;
            $caseExtHeight = $item->case_ext_height;
            $caseUomRate = $item->case_uom_rate;
            $caseGrossWeight = $item->case_gross_weight;

            $cm2 = bcmul($caseExtLength, $caseExtWidth, 10);
        }

        $cm2 = bcmul($caseExtLength, $caseExtWidth, 10);
        $cm3 = bcmul($cm2, $caseExtHeight, 10);
        $m3 = bcdiv($cm3, 1000000000, 10);

        if($mode === 2)
        {
            //$dtlModel->unit_qty = $dtlModel->unit_qty;
        }
        elseif($mode === 1)
        {
            $dtlModel->unit_qty = $dtlModel->balance_unit_qty;
        }
        else
        {
            $dtlModel->unit_qty = bcmul($dtlModel->qty, $dtlModel->uom_rate, 10);
        }
        $dtlModel->case_qty = bcdiv($dtlModel->unit_qty, $caseUomRate, 10);
        $dtlModel->case_uom_rate = $caseUomRate;
        $dtlModel->gross_weight = bcmul($dtlModel->case_qty, $caseGrossWeight, 10);
        $dtlModel->gross_weight = round($dtlModel->gross_weight, 8);
        $dtlModel->cubic_meter = bcmul($dtlModel->case_qty, $m3, 10);
        $dtlModel->cubic_meter = round($dtlModel->cubic_meter, 8);

        $dtlModel->case_qty = floor($dtlModel->case_qty);
        $dtlModel->gross_weight = round($dtlModel->gross_weight, 8);

        //process the case_uom_code, unit_uom_code, calculate the loose_qty and lose_uom_code
        $remainderUnitQty = bcsub(
            $dtlModel->unit_qty,
            bcmul($dtlModel->case_qty, $caseUomRate, 10),
            10
        );
        $modulus = 0;
        if(!empty($item))
        {
            $modulus = bcmod($remainderUnitQty, $item->case_uom_rate);
        }

        //process the barcode
        $itemUoms = array();
        if(!empty($item)
        && $isShowBarcode == true)
        {
            $itemUoms = $item->itemUoms;
            //sort by uom_rate DESC
            $itemUoms = $itemUoms->sortByDesc('uom_rate');
            //usort($itemUoms, function ($a, $b) {
            //    return ($a->uom_rate < $b->uom_rate) ? 1 : -1;
            //});
        }
        foreach($itemUoms as $itemUom)
        {
            $uom = $itemUom->uom;
            if($itemUom->uom_id == $item->unit_uom_id)
            {
                $dtlModel->item_unit_barcode = $itemUom->barcode;
                $dtlModel->item_unit_uom_code = $uom->code;
            }
            if($itemUom->uom_id == $item->case_uom_id)
            {
                $dtlModel->item_case_barcode = $itemUom->barcode;
                $dtlModel->item_case_uom_code = $uom->code;
            }

            //calculate the loose uom
            if(bccomp($modulus, 0, 10) != 0
            && bccomp($itemUom->uom_rate, $item->case_uom_rate, 10) < 0)
            {
                $modulus = bcmod($remainderUnitQty, $itemUom->uom_rate);
                if(bccomp($modulus, 0, 10) == 0)
                {
                    $dtlModel->loose_uom_id = $itemUom->uom_id;
                    $dtlModel->item_loose_uom_code = $uom->code;
                    $dtlModel->loose_uom_rate = $itemUom->uom_rate;
                    $dtlModel->loose_qty = bcdiv($remainderUnitQty, $itemUom->uom_rate, 10);
                }
            }
        }

        if(bccomp($dtlModel->loose_uom_rate, 1) == 0)
        {
            $dtlModel->item_loose_uom_code = $dtlModel->item_unit_uom_code;
        }

        if(!empty($item))
        {
            $dtlModel->unit_qty = number_format($dtlModel->unit_qty, $item->qty_scale, '.', '');
            $dtlModel->case_qty = number_format($dtlModel->case_qty, $item->qty_scale, '.', '');
            $dtlModel->loose_qty = number_format($dtlModel->loose_qty, $item->qty_scale, '.', '');
        }

        return $dtlModel;
    }

    public function initModel()
    {
        AuthService::authorize(
            array(
                'item_create'
            )
        );

        $model = new Item();
        $model->code = '';
        $model->desc_01 = '';
        $model->status = ResStatus::$MAP['ACTIVE'];

        return $model;
    }

    public function createModel($data, $divisionId)
    {
        AuthService::authorize(
            array(
                'item_create'
            )
        );

        $data = self::processIncomingModel($data, true);
        $model = ItemRepository::createModel($data);

        $itemDivisionData = array();
        $itemDivisionData['item_id'] = $model->id;
        $itemDivisionData['division_id'] = $divisionId;
        ItemDivisionRepository::createModel($itemDivisionData);

        $message = __('Item.record_successfully_created', ['docCode'=>$model->code]);

		return array(
            'data' => $model->id,
			'message' => $message
		);
    }

    public function showModel($itemId)
    {
        AuthService::authorize(
            array(
                'item_read',
				'item_update'
            )
        );

        $model = ItemRepository::findByPk($itemId);
		if(!empty($model))
        {
            $model = self::processOutgoingModel($model);
        }

        return $model;
    }

    public function updateModel($data)
	{
        AuthService::authorize(
            array(
                'item_update'
            )
        );

        $data = self::processIncomingModel($data);

        $item = ItemRepository::findByPk($data['id']);
        $itemData = $item->toArray();
        //assign data to model
        foreach($data as $field => $value)
        {
            $itemData[$field] = $value;
        }

        $result = ItemRepository::updateModel($itemData);
        $model = $result['model'];

        $model = self::processOutgoingModel($model);

        $message = __('Item.record_successfully_updated', ['code'=>$model->code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'model'=>$model
            ),
			'message' => $message
		);
    }

    public function createPriceModel($data)
    {
        AuthService::authorize(
            array(
                'item_create'
            )
        );

        $uomId = 0;
        $uomRate = 0;
        if(array_key_exists('uom_id', $data))
        {
            $uomId = $data['uom_id'];
            // unset($data['uom_id']);
        }
        if(array_key_exists('uom_rate', $data))
        {
            $uomRate = $data['uom_rate'];
            unset($data['uom_rate']);
        }
        if ($uomId == 0 || $uomRate == 0) {
            $exc = new ApiException(__('Item.invalid_uom'));
            throw $exc;
        }
        $findItemUom = ItemUomRepository::findByItemIdAndUomId($data['item_id'], $uomId);
        if ($findItemUom) {
            $findItemUomRate = ItemUomRepository::findByItemIdAndUomRate($data['item_id'], $uomRate);
            if ($findItemUomRate && $findItemUomRate->uom_id != $uomId) {
                $exc = new ApiException(__('Item.uom_rate_already_exists'));
                throw $exc;
            }
            if ($findItemUom->uom_rate != $uomRate) {
                $itemUomData = array();
                $itemUomData['id'] = $findItemUom->id;
                $itemUomData['item_id'] = $data['item_id'];
                $itemUomData['uom_id'] = $uomId;
                $itemUomData['uom_rate'] = $uomRate;
                $itemUomData['barcode'] = '';
                ItemUomRepository::updateModel($itemUomData);
            }
        }
        else {
            $findItemUomRate = ItemUomRepository::findByItemIdAndUomRate($data['item_id'], $uomRate);
            if ($findItemUomRate) {
                $exc = new ApiException(__('Item.uom_rate_already_exists'));
                throw $exc;
            }
            $itemUomData = array();
            $itemUomData['item_id'] = $data['item_id'];
            $itemUomData['uom_id'] = $uomId;
            $itemUomData['uom_rate'] = $uomRate;
            $itemUomData['barcode'] = '';
            ItemUomRepository::createModel($itemUomData);
        }

        // $data = self::processIncomingModel($data, true);
        $model = ItemSalePriceRepository::createModel($data);

        $message = __('Item.record_successfully_created', ['docCode'=>$model->code]);

		return array(
            'data' => $model->id,
			'message' => $message
		);
    }

    public function showPriceModel($itemSalePriceId)
    {
        AuthService::authorize(
            array(
                'item_read',
				'item_update'
            )
        );

        $model = ItemSalePriceRepository::findByPk($itemSalePriceId);
		if(!empty($model))
        {
            $model = self::processOutgoingModel($model);
        }

        return $model;
    }

    public function updatePriceModel($data)
	{
        AuthService::authorize(
            array(
                'item_update'
            )
        );

        // $data = self::processIncomingModel($data);
        $uomId = 0;
        $uomRate = 0;

        if(array_key_exists('uom_id', $data))
        {
            $uomId = $data['uom_id'];
            // unset($data['uom_id']);
        }
        if(array_key_exists('uom_rate', $data))
        {
            $uomRate = $data['uom_rate'];
            unset($data['uom_rate']);
        }
        if ($uomId == 0 || $uomRate == 0) {
            $exc = new ApiException(__('Item.invalid_uom'));
            throw $exc;
        }
        $findItemUom = ItemUomRepository::findByItemIdAndUomId($data['item_id'], $uomId);
        if ($findItemUom) {
            $findItemUomRate = ItemUomRepository::findByItemIdAndUomRate($data['item_id'], $uomRate);
            if ($findItemUomRate && $findItemUomRate->uom_id != $uomId) {
                $exc = new ApiException(__('Item.uom_rate_already_exists'));
                throw $exc;
            }
            if ($findItemUom->uom_rate != $uomRate) {
                $itemUomData = array();
                $itemUomData['id'] = $findItemUom->id;
                $itemUomData['item_id'] = $data['item_id'];
                $itemUomData['uom_id'] = $uomId;
                $itemUomData['uom_rate'] = $uomRate;
                $itemUomData['barcode'] = '';
                ItemUomRepository::updateModel($itemUomData);
            }
        }
        else {
            $exc = new ApiException(__('Item.uom_not_exists'));
            throw $exc;
        }


        $itemSalePrice = ItemSalePriceRepository::findByPk($data['id']);
        $itemSalePriceData = $itemSalePrice->toArray();
        //assign data to model
        foreach($data as $field => $value)
        {
            $itemSalePriceData[$field] = $value;
        }

        $result = ItemSalePriceRepository::updateModel($itemSalePriceData);
        $model = $result['model'];

        // $model = self::processOutgoingModel($model);

        $message = __('Item.record_successfully_updated', ['code'=>$model->code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'model'=>$model
            ),
			'message' => $message
		);
    }

    static public function processIncomingModel($data)
    {
        if(array_key_exists('brand_id_select2', $data))
        {
            $data['item_group_01_id'] = $data['brand_id_select2']['value'];
            unset($data['brand_id_select2']);
        }
        if(array_key_exists('category_id_select2', $data))
        {
            $data['item_group_02_id'] = $data['category_id_select2']['value'];
            unset($data['category_id_select2']);
        }
        if(array_key_exists('manufacturer_id_select2', $data))
        {
            $data['item_group_03_id'] = $data['manufacturer_id_select2']['value'];
            unset($data['manufacturer_id_select2']);
        }
        if(array_key_exists('submit_action', $data))
        {
            unset($data['submit_action']);
        }
        if(array_key_exists('str_status', $data))
        {
            $data['status'] = ResStatus::$MAP[$data['str_status']];
            unset($data['str_status']);
        }
        if(array_key_exists('item_group01', $data))
        {
            unset($data['item_group01']);
        }
        if(array_key_exists('item_group_01_code', $data))
        {
            unset($data['item_group_01_code']);
        }
        if(array_key_exists('item_group02', $data))
        {
            unset($data['item_group02']);
        }
        if(array_key_exists('item_group_02_code', $data))
        {
            unset($data['item_group_02_code']);
        }
        if(array_key_exists('item_group03', $data))
        {
            unset($data['item_group03']);
        }
        if(array_key_exists('item_group_03_code', $data))
        {
            unset($data['item_group_03_code']);
        }
        if(array_key_exists('unit_uom_code', $data))
        {
            unset($data['unit_uom_code']);
        }
        if(array_key_exists('case_uom_code', $data))
        {
            unset($data['case_uom_code']);
        }
        if(array_key_exists('details', $data))
        {
            unset($data['details']);
        }
        if(array_key_exists('photos', $data))
        {
            unset($data['photos']);
        }
        if(array_key_exists('item_photos', $data))
        {
            unset($data['item_photos']);
        }
        if(array_key_exists('prices', $data))
        {
            unset($data['prices']);
        }
        if(array_key_exists('item_prices', $data))
        {
            unset($data['item_prices']);
        }
        if(array_key_exists('promotions', $data))
        {
            unset($data['promotions']);
        }
        //preprocess the select2 and dropDown
        // if(array_key_exists('debtor_id_select2', $data))
        // {
        //     $data['debtor_id'] = $data['debtor_id_select2']['value'];
        //     unset($data['debtor_id_select2']);
        // }

        return $data;
    }

    static public function processOutgoingModel($model)
	{
        $model->str_status = ResStatus::$MAP[$model->status];

        $itemGroup01 = $model->itemGroup01;
        $model->item_group_01_code = !empty($itemGroup01) ? $itemGroup01->code : '';

        $itemGroup02 = $model->itemGroup02;
        $model->item_group_02_code = !empty($itemGroup02) ? $itemGroup02->code : '';

        $itemGroup03 = $model->itemGroup03;
        $model->item_group_03_code = !empty($itemGroup03) ? $itemGroup03->code : '';


        /*

        $itemGroup04 = $model->itemGroup04;
        $model->item_group_04_code = !empty($itemGroup04) ? $itemGroup04->code : '';

        $itemGroup05 = $model->itemGroup05;
        $model->item_group_05_code = !empty($itemGroup05) ? $itemGroup05->code : '';
        */

        $unitUom = $model->unitUom;
        $model->unit_uom_code = !empty($unitUom) ? $unitUom->code : '';

        $caseUom = $model->caseUom;
        $model->case_uom_code = !empty($caseUom) ? $caseUom->code : '';

        //query itemUom
        $itemUoms = ItemUomRepository::findAllByItemId($model->id);
        $itemUoms->load('uom');
        foreach($itemUoms as $itemUom)
        {
            $uom = $itemUom->uom;
            $itemUom->uom_code = !empty($uom) ? $uom->code : '';

            unset($itemUom->uom);
        }

        $model->details = $itemUoms;

        /*
        unset($model->itemGroup01);
        unset($model->itemGroup02);
        unset($model->itemGroup03);
        unset($model->itemGroup04);
        unset($model->itemGroup05);
        */
        unset($model->unitUom);
        unset($model->caseUom);

        $itemPhotos = $model->itemPhotos;
        $model->photos = $itemPhotos;

        //query item price
        $itemSalePrices = $model->itemPrices;
        $itemSalePrices->load('uom');
        foreach($itemSalePrices as $itemSalePrice)
        {
            $uom = $itemSalePrice->uom;
            $itemSalePrice->uom_code = !empty($uom) ? $uom->code : '';
            foreach ($model->details as $itemUom) {
                if ($itemUom->uom_id == $uom->id) {
					$itemSalePrice->uom_select2 = [
						'value'=>$uom->id,
						'label'=>$uom->code
					];
                    $itemSalePrice->uom_rate = $itemUom->uom_rate;
                    break;
                }
            }

            unset($itemSalePrice->uom);
        }
        $model->prices = $itemSalePrices;

        $promotions = PromotionRepository::findAllByItem($model->id);
        $model->promotions = $promotions;

        $initBrandOption = array('value'=>0,'label'=>'');
        $brand = ItemGroup01Repository::findByPk($model->item_group_01_id);
        if(!empty($brand))
        {
            $initBrandOption = array('value'=>$brand->id, 'label'=>$brand->code);
        }
        $model->brand_id_select2 = $initBrandOption;

        $initCategoryOption = array('value'=>0,'label'=>'');
        $category = ItemGroup02Repository::findByPk($model->item_group_02_id);
        if(!empty($category))
        {
            $initCategoryOption = array('value'=>$category->id, 'label'=>$category->code);
        }
        $model->category_id_select2 = $initCategoryOption;

        $initManufacturerOption = array('value'=>0,'label'=>'');
        $manufacturer = ItemGroup03Repository::findByPk($model->item_group_03_id);
        if(!empty($manufacturer))
        {
            $initManufacturerOption = array('value'=>$manufacturer->id, 'label'=>$manufacturer->code);
        }
        $model->manufacturer_id_select2 = $initManufacturerOption;


		return $model;
    }

    static public function processOutgoingPromotions($item, $promotions) {
        $discount = array(
            'price'=>0,
            'rate'=>0,
            'type'=>''
        );

        //Add the types of discounts
        $applied = array('rate' => 0, 'price' => 0);

        foreach ($promotions as $promotion) {
            $actions = $promotion->actions;
            $variants = $promotion->variants;
            $config = $actions[0]->config;

            if ($promotion->type == 0) {
                foreach ($variants as $key => $variant) {
                    $overide = false;
                    if ($variant->variant_type === 'item_group01') {
                        // brand
                        if ($discount['type'] !== 'item'
                            && $item->item_group_01_id === $variant->item_group_01_id) {
                            $overide = true;
                        }
                    } else if ($variant->variant_type === 'item_group02') {
                        // category
                        if ($discount['type'] !== 'item'
                            && $item->item_group_02_id === $variant->item_group_02_id) {
                            $overide = true;
                        }
                    } else if ($variant->variant_type === 'item_group03') {
                        // manufacturer
                        if ($discount['type'] !== 'item'
                            && $item->item_group_03_id === $variant->item_group_03_id) {
                            $overide = true;
                        }
                    } else {
                        $overide = true;
                    }

                    if ($overide && $variant->min_qty >= 0 && $variant->max_qty >= 0) {
                        if($promotion->type == 0 && $discount['rate'] < $variant->disc_perc_01) {
                            $discount['rate'] = $variant->disc_perc_01;
                            $applied['rate'] = $promotion->id;
                        }
                        $discount['type'] = $variant->type;
                    }
                }
            } else if ($promotion->type == 1 && isset($config['disc_fixed_price'])) {
                if($discount['price'] < $config['disc_fixed_price']) {
                    $discount['price'] = $config['disc_fixed_price'];
                    $applied['price'] = $promotion->id;
                }
            }
        }

        return array(
            'discount_rate'=>$discount['rate'],
            'discount_price'=>$discount['price'],
            'promotions'=>$promotions,
            'applied'=>$applied
        );
    }

    public function uploadPhotos($itemId, $fileArray)
	{
        AuthService::authorize(
            array(
                'item_update'
            )
        );

        $itemPhotoArray = array();

        foreach($fileArray as $files)
        {
            foreach ($files as $file)
            {
                // Log::info(print_r($file, true));
                $itemPhotoData = array();
                $itemPhotoData['item_id'] = $itemId;
                $itemPhotoData['desc_01'] = $file->getClientOriginalName();
                $itemPhotoData['desc_02'] = '';
                $itemPhotoData['old_filename'] = $file->getRealPath();
                $itemPhotoData['blob'] = $file;
                $itemPhotoModel = ItemPhotoRepository::saveItemPhoto($itemPhotoData);

                $itemPhotoArray[] = $itemPhotoModel;
            }
        }
        if (count($itemPhotoArray) > 0) {
            $message = __('Item.photo_successfully_uploaded', []);
        }
        else {
            $message = __('Item.no_photo_uploaded', []);
        }

		return array(
			'data' => array(
                'timestamp'=>time(),
                'photos'=>$itemPhotoArray,
                'deleted_photos'=>array(),
            ),
			'message' => $message
		);
    }

    public function deletePhotos($itemId, $delItemPhotoDataArray)
	{
        AuthService::authorize(
            array(
                'item_update'
            )
        );

        $deletedItemPhotoModelArray = ItemPhotoRepository::deleteItemPhotos($itemId, $delItemPhotoDataArray);
        $message = __('Item.photos_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'photos'=>array(),
                'deleted_photos'=>$deletedItemPhotoModelArray,
            ),
			'message' => $message
		);
    }

    public function resetPhotos($divisionId)
	{
        AuthService::authorize(
            array(
                'item_update'
            )
        );

        $deletedItemPhotoModelArray = array();

        $items = ItemRepository::findAll(array(), array(), 5000, $divisionId);
        foreach ($items as $item)
        {
            $itemPhotos = $item->itemPhotos;
            foreach ($itemPhotos as $itemPhoto)
            {
                $delItemPhotoDataArray = array();
                $delItemPhotoData = array();
                $delItemPhotoData['item_photo_id'] = $itemPhoto->id;
                $delItemPhotoDataArray[] = $delItemPhotoData;
                $result = ItemPhotoRepository::deleteItemPhotos($item->id, $delItemPhotoDataArray);
                $deletedItemPhotoModelArray = array_merge($deletedItemPhotoModelArray, $result);
            }
        }

        $message = __('Item.photos_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'photos'=>array(),
                'deleted_photos'=>$deletedItemPhotoModelArray,
            ),
			'message' => $message
		);
	}

	public function getItemGroup01($search, $filters, $sorts)
    {
		$itemGroups = ItemRepository::findItemGroup01($search, $filters, $sorts);
        return $itemGroups;
    }

	public function getItemGroup02($search, $filters)
    {
		$itemGroups = ItemRepository::findItemGroup02($search, $filters);
        return $itemGroups;
    }

	public function getItemGroup03($search, $filters)
    {
		$itemGroups = ItemRepository::findItemGroup03($search, $filters);
        return $itemGroups;
    }
}
