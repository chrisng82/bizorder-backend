<?php

namespace App\Services;

use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Services\Env\ResType;
use App\Repositories\PickListHdrRepository;
use App\Repositories\PickListDtlRepository;
use App\Repositories\OutbOrdHdrRepository;
use App\Repositories\OutbOrdDtlRepository;
use App\Repositories\SiteDocNoRepository;
use App\Repositories\DocTxnFlowRepository;
use App\Repositories\QuantBalTxnRepository;
use App\Repositories\InvTxnFlowRepository;
use App\Repositories\InvDocHdrRepository;
use App\Repositories\SlsInvHdrRepository;
use App\Repositories\DelOrdHdrRepository;
use App\Repositories\PrintDocSettingRepository;
use App\Repositories\PrintDocTxnRepository;
use App\Repositories\GdsRcptHdrRepository;
use App\Services\Utils\ApiException;
use App\Services\SlsInvService;
use App\Services\DelOrdService;
use App\Services\RtnRcptService;
use App\Services\ItemService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class InvDocService 
{
  	public function __construct() 
	{
  	}
    
	public function indexProcess($strProcType, $siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
		$user = Auth::user();
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['INV_DOC_01']) 
			{
				//Pick List -> Inv/DO
				$pickListHdrs = $this->indexInvDoc01($user, $siteFlowId, $sorts, $filters, $pageSize);
				return $pickListHdrs;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['INV_DOC_01_01'])
			{
				//Pick List print Inv Doc
				$pickListHdrs = $this->indexInvDoc0101($user, $siteFlowId, $sorts, $filters, $pageSize);
				return $pickListHdrs;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['INV_DOC_01_02'])
			{
				//Pick List print Inv Doc
				$pickListHdrs = $this->indexInvDoc0102($siteFlowId, $sorts, $filters, $pageSize);
				return $pickListHdrs;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['INV_DOC_02']) 
			{
				//Gds Rcpt -> RtnRcpt
				$gdsRcptHdrs = $this->indexInvDoc02($user, $siteFlowId, $sorts, $filters, $pageSize);
				return $gdsRcptHdrs;
			}
		}
	}
	  
	protected function indexInvDoc0101($user, $siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
            array(
                'pick_list_read'
            )
		);
		
		$divisionIds = array();
		$userDivisions = $user->userDivisions;
		foreach($userDivisions as $userDivision)
		{
			$divisionIds[] = $userDivision->division_id;
		}

		//DB::connection()->enableQueryLog();
		$pickListHdrs = PickListHdrRepository::findAllExistInvDoc01TxnAndNotExistInvDoc0101($divisionIds, $sorts, $filters, $pageSize);
		$pickListHdrs->load(
			'pickListOutbOrds','pickListOutbOrds.outbOrdHdr',
			'pickListOutbOrds.outbOrdHdr.deliveryPoint', 
			'pickListOutbOrds.outbOrdHdr.deliveryPoint.area', 
			'pickListOutbOrds.outbOrdHdr.salesman'
		);
		//Log::error(DB::getQueryLog());
		foreach($pickListHdrs as $pickListHdr)
		{
			$pickListHdr->str_doc_status = DocStatus::$MAP[$pickListHdr->doc_status];

			$deliveryPointHash = array();
			$areaHash = array();
			$salesmanHash = array();
			$outbOrdHdrHash = array();

			$pickListOutbOrds = $pickListHdr->pickListOutbOrds;
			foreach($pickListOutbOrds as $pickListOutbOrd)
			{
				$outbOrdHdr = $pickListOutbOrd->outbOrdHdr;

				$deliveryPoint = $outbOrdHdr->deliveryPoint;
				$deliveryPointHash[$deliveryPoint->id] = $deliveryPoint;

				$area = $deliveryPoint->area;
				$areaHash[$area->id] = $area;

				$salesman = $outbOrdHdr->salesman;
				$salesmanHash[$salesman->id] = $salesman;

				$outbOrdHdrHash[$outbOrdHdr->id] = $outbOrdHdr;
			}

			$pickListHdr->delivery_points = array_values($deliveryPointHash);
			$pickListHdr->areas = array_values($areaHash);
			$pickListHdr->salesmans = array_values($salesmanHash);
			$pickListHdr->outb_ord_hdrs = array_values($outbOrdHdrHash);
			unset($pickListHdr->pickListOutbOrds);
		}
		return $pickListHdrs;
	}

	protected function indexInvDoc0102($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
            array(
                'pick_list_read'
            )
		);

		//DB::connection()->enableQueryLog();
		$pickListHdrs = PickListHdrRepository::findAllExistInvDoc01Txn($siteFlowId, $sorts, $filters, $pageSize);
		$pickListHdrs->load(
			'pickListOutbOrds','pickListOutbOrds.outbOrdHdr',
			'pickListOutbOrds.outbOrdHdr.deliveryPoint', 
			'pickListOutbOrds.outbOrdHdr.deliveryPoint.area', 
			'pickListOutbOrds.outbOrdHdr.salesman'
		);
		//Log::error(DB::getQueryLog());
		foreach($pickListHdrs as $pickListHdr)
		{
			$pickListHdr->str_doc_status = DocStatus::$MAP[$pickListHdr->doc_status];

			$deliveryPointHash = array();
			$areaHash = array();
			$salesmanHash = array();
			$outbOrdHdrHash = array();

			$pickListOutbOrds = $pickListHdr->pickListOutbOrds;
			foreach($pickListOutbOrds as $pickListOutbOrd)
			{
				$outbOrdHdr = $pickListOutbOrd->outbOrdHdr;

				$deliveryPoint = $outbOrdHdr->deliveryPoint;
				$deliveryPointHash[$deliveryPoint->id] = $deliveryPoint;

				$area = $deliveryPoint->area;
				$areaHash[$area->id] = $area;

				$salesman = $outbOrdHdr->salesman;
				$salesmanHash[$salesman->id] = $salesman;

				$outbOrdHdrHash[$outbOrdHdr->id] = $outbOrdHdr;
			}

			$pickListHdr->delivery_points = array_values($deliveryPointHash);
			$pickListHdr->areas = array_values($areaHash);
			$pickListHdr->salesmans = array_values($salesmanHash);
			$pickListHdr->outb_ord_hdrs = array_values($outbOrdHdrHash);
			unset($pickListHdr->pickListOutbOrds);
		}
		return $pickListHdrs;
	}
    
	protected function indexInvDoc01($user, $siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
            array(
                'pick_list_read'
            )
		);

		$divisionIds = array();
		$userDivisions = $user->userDivisions;
		foreach($userDivisions as $userDivision)
		{
			$divisionIds[] = $userDivision->division_id;
		}

		$outbOrdDtlsHashByPickListHdrIdAndItemId = array();
		$itemAttrHashByPickListHdrIdAndItemId = array();

		//DB::connection()->enableQueryLog();
		$pickListHdrs = PickListHdrRepository::findAllNotExistInvDoc01Txn($divisionIds, $sorts, $filters, $pageSize);
		//Log::error(DB::getQueryLog());
		$pickListHdrs->load(
			'pickListOutbOrds', 'pickListOutbOrds.outbOrdHdr.outbOrdDtls',
			'pickListOutbOrds.outbOrdHdr.outbOrdDtls.item', 
			'pickListOutbOrds.outbOrdHdr', 'pickListOutbOrds.outbOrdHdr.division', 
			'pickListOutbOrds.outbOrdHdr.deliveryPoint', 'pickListOutbOrds.outbOrdHdr.salesman',
			'pickListDtls', 'pickListDtls.item'
		);
		foreach($pickListHdrs as $pickListHdr)
		{
			$pickListHdr->str_doc_status = DocStatus::$MAP[$pickListHdr->doc_status];

			$pickListOutbOrds = $pickListHdr->pickListOutbOrds;
			foreach($pickListOutbOrds as $pickListOutbOrd)
			{
				//process the pick list outbOrdDtls
				$outbOrdHdr = $pickListOutbOrd->outbOrdHdr;

				$outbOrdDtls = $outbOrdHdr->outbOrdDtls;
				//calculate the outbOrdDtls case_qty, gross_weight and cubic_meter, sum by itemId
				$unitQty = 0;
				$caseQty = 0;
				$grossWeight = 0;
				$cubicMeter = 0;
				foreach($outbOrdDtls as $outbOrdDtl)
				{
					if($pickListHdr->location_id != $outbOrdDtl->location_id)
					{
						//if this order detail location id != pick list location id
						//then just skip
						continue;
					}

					$key = $pickListHdr->id.'/'.$outbOrdDtl->item_id;
					$item = $outbOrdDtl->item;				

					//fill the doc details
					$outbOrdDtl->doc_code = $outbOrdHdr->doc_code;
					if(strcmp($outbOrdHdr->cur_res_type, ResType::$MAP['SLS_INV']) == 0)
					{
						$outbOrdDtl->doc_code = $outbOrdHdr->sls_inv_hdr_code;
					}
					elseif(strcmp($outbOrdHdr->cur_res_type, ResType::$MAP['DEL_ORD']) == 0)
					{
						$outbOrdDtl->doc_code = $outbOrdHdr->del_ord_hdr_code;
					}
					$outbOrdDtl->doc_date = $outbOrdHdr->doc_date;
					$outbOrdDtl->est_del_date = $outbOrdHdr->est_del_date;

					$division = $outbOrdHdr->division;
					$outbOrdDtl->division_code = $division->code;

					$deliveryPoint = $outbOrdHdr->deliveryPoint;
					$outbOrdDtl->delivery_point_code = $deliveryPoint->code;
					$outbOrdDtl->delivery_point_company_name_01 = $deliveryPoint->company_name_01;
					$outbOrdDtl->delivery_point_area_code = $deliveryPoint->area_code;

					$salesman = $outbOrdHdr->salesman;
					$outbOrdDtl->salesman_username = $salesman->username;

					//if status is not COMPLETE, then all qty is 0
					if($outbOrdHdr->doc_status == DocStatus::$MAP['COMPLETE'])
					{
						$outbOrdDtl = ItemService::processCaseLoose($outbOrdDtl, $item, false);	
					}
					else
					{
						$outbOrdDtl->unit_qty = 0;
						$outbOrdDtl->case_qty = 0;
						$outbOrdDtl->gross_weight = 0;
						$outbOrdDtl->cubic_meter = 0;
					}			

					$itemAttr = array(
						'pick_item_unit_qty' => 0,
						'pick_item_case_qty' => 0,
						'pick_item_gross_weight' => 0,
						'pick_item_cubic_meter' => 0,
						'ord_item_unit_qty' => 0,
						'ord_item_case_qty' => 0,
						'ord_item_gross_weight' => 0,
						'ord_item_cubic_meter' => 0
					);
					if(array_key_exists($key, $itemAttrHashByPickListHdrIdAndItemId))
					{
						$itemAttr = $itemAttrHashByPickListHdrIdAndItemId[$key];
					}
					$itemAttr['ord_item_unit_qty'] = bcadd($itemAttr['ord_item_unit_qty'], $outbOrdDtl->unit_qty, 8);
					$itemAttr['ord_item_case_qty'] = bcadd($itemAttr['ord_item_case_qty'], $outbOrdDtl->case_qty, 8);
					$itemAttr['ord_item_gross_weight'] = bcadd($itemAttr['ord_item_gross_weight'], $outbOrdDtl->gross_weight, 8);
					$itemAttr['ord_item_cubic_meter'] = bcadd($itemAttr['ord_item_cubic_meter'], $outbOrdDtl->cubic_meter, 8);
					$itemAttrHashByPickListHdrIdAndItemId[$key] = $itemAttr;

					$unitQty = bcadd($unitQty, $outbOrdDtl->unit_qty, 8);
					$caseQty = bcadd($caseQty, $outbOrdDtl->case_qty, 8);
					$grossWeight = bcadd($grossWeight, $outbOrdDtl->gross_weight, 8);
					$cubicMeter = bcadd($cubicMeter, $outbOrdDtl->cubic_meter, 8);

					$tmpOutbOrdDtls = array();
					if(array_key_exists($key, $outbOrdDtlsHashByPickListHdrIdAndItemId))
					{
						$tmpOutbOrdDtls = $outbOrdDtlsHashByPickListHdrIdAndItemId[$key];
					}
					$tmpOutbOrdDtls[] = $outbOrdDtl;
					$outbOrdDtlsHashByPickListHdrIdAndItemId[$key] = $tmpOutbOrdDtls;

					//unset the item, so it wont send in json
					unset($outbOrdDtl->item);
				}
			}

			//process the pick list dtls
			$pickListDtls = $pickListHdr->pickListDtls;
			//calculate the case_qty, gross_weight and cubic_meter, sum by itemId
			$pickUnitQty = 0;
			$pickCaseQty = 0;
			$pickGrossWeight = 0;
			$pickCubicMeter = 0;
			foreach($pickListDtls as $pickListDtl)
			{
				$key = $pickListDtl->hdr_id.'/'.$pickListDtl->item_id;
				$item = $pickListDtl->item;
				$pickListDtl = ItemService::processCaseLoose($pickListDtl, $item, false);	

				$pickListDtl->unit_qty = round($pickListDtl->unit_qty, 8);
				$pickListDtl->case_qty = round($pickListDtl->case_qty, 8);
				$pickListDtl->gross_weight = round($pickListDtl->gross_weight, 8);
				$pickListDtl->cubic_meter = round($pickListDtl->cubic_meter, 8);

				$itemAttr = array(
					'pick_item_unit_qty' => 0,
					'pick_item_case_qty' => 0,
					'pick_item_gross_weight' => 0,
					'pick_item_cubic_meter' => 0,
					'ord_item_unit_qty' => 0,
					'ord_item_case_qty' => 0,
					'ord_item_gross_weight' => 0,
					'ord_item_cubic_meter' => 0
				);
				if(array_key_exists($key, $itemAttrHashByPickListHdrIdAndItemId))
				{
					$itemAttr = $itemAttrHashByPickListHdrIdAndItemId[$key];
				}
				$itemAttr['pick_item_unit_qty'] = bcadd($itemAttr['pick_item_unit_qty'], $pickListDtl->unit_qty, 8);
				$itemAttr['pick_item_case_qty'] = bcadd($itemAttr['pick_item_case_qty'], $pickListDtl->case_qty, 8);
				$itemAttr['pick_item_gross_weight'] = bcadd($itemAttr['pick_item_gross_weight'], $pickListDtl->gross_weight, 8);
				$itemAttr['pick_item_cubic_meter'] = bcadd($itemAttr['pick_item_cubic_meter'], $pickListDtl->cubic_meter, 8);
				$itemAttrHashByPickListHdrIdAndItemId[$key] = $itemAttr;

				$pickUnitQty = bcadd($pickUnitQty, $pickListDtl->unit_qty, 8);
				$pickCaseQty = bcadd($pickCaseQty, $pickListDtl->case_qty, 8);
				$pickGrossWeight = bcadd($pickGrossWeight, $pickListDtl->gross_weight, 8);
				$pickCubicMeter = bcadd($pickCubicMeter, $pickListDtl->cubic_meter, 8);
			}

			$pickListHdr->pick_unit_qty = $pickUnitQty;
			$pickListHdr->pick_case_qty = $pickCaseQty;
			$pickListHdr->pick_gross_weight = $pickGrossWeight;
			$pickListHdr->pick_cubic_meter = $pickCubicMeter;
		}

		foreach($pickListHdrs as $pickListHdr)
		{
			$pickListHdr->is_tally = true;

			$ordUnitQty = 0;
			$ordCaseQty = 0;
			$ordGrossWeight = 0;
			$ordCubicMeter = 0;
			$pickOutbOrdDtls = array();
			foreach($itemAttrHashByPickListHdrIdAndItemId as $key => $itemAttr)
			{
				$keyParts = explode('/',$key);
				$pickListHdrId = $keyParts[0];
				$itemId = $keyParts[1];
				if($pickListHdrId != $pickListHdr->id)
				{
					continue;
				}

				//query outbOrdDtlsHashByPickListHdrIdAndItemId 
				$tmpOutbOrdDtls = array();
				if(array_key_exists($key, $outbOrdDtlsHashByPickListHdrIdAndItemId))
				{
					$tmpOutbOrdDtls = $outbOrdDtlsHashByPickListHdrIdAndItemId[$key];
				}

				$ordUnitQty = bcadd($ordUnitQty, $itemAttr['ord_item_unit_qty'], 8);
				$ordCaseQty = bcadd($ordCaseQty, $itemAttr['ord_item_case_qty'], 8);
				$ordGrossWeight = bcadd($ordGrossWeight, $itemAttr['ord_item_gross_weight'], 8);
				$ordCubicMeter = bcadd($ordCubicMeter, $itemAttr['ord_item_cubic_meter'], 8);

				foreach($tmpOutbOrdDtls as $tmpOutbOrdDtl)
				{
					$tmpOutbOrdDtl->pick_item_ttl_unit_qty = $itemAttr['pick_item_unit_qty'];
					$tmpOutbOrdDtl->pick_item_ttl_case_qty = $itemAttr['pick_item_case_qty'];
					$tmpOutbOrdDtl->pick_item_ttl_gross_weight = $itemAttr['pick_item_gross_weight'];
					$tmpOutbOrdDtl->pick_item_ttl_cubic_meter = $itemAttr['pick_item_cubic_meter'];
					$tmpOutbOrdDtl->is_tally = true;
					if(bccomp($itemAttr['ord_item_unit_qty'], $itemAttr['pick_item_unit_qty'], 8) != 0)
					{
						$tmpOutbOrdDtl->is_tally = false;
						$pickListHdr->is_tally = false;
					}

					$pickOutbOrdDtls[] = $tmpOutbOrdDtl;
				}
			}

			$pickListHdr->ord_unit_qty = $ordUnitQty;
			$pickListHdr->ord_case_qty = $ordCaseQty;
			$pickListHdr->ord_gross_weight = $ordGrossWeight;
			$pickListHdr->ord_cubic_meter = $ordCubicMeter;

			$pickListHdr->details = $pickOutbOrdDtls;

			unset($pickListHdr->pickListOutbOrds);
		}

		return $pickListHdrs;
  	}

	public function createProcess($strProcType, $hdrIds) 
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['INV_DOC_01'])
			{
				//PICK LIST -> SHIP DOC
				$result = $this->createInvDoc01(ProcType::$MAP[$strProcType], $hdrIds);
				return $result;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['INV_DOC_02'])
			{
				//GDS RCPT(SLS_RTN) -> RTN_RCPT
				$result = $this->createInvDoc02(ProcType::$MAP[$strProcType], $hdrIds);
				return $result;
			}
		}
	}

	protected static function checkPickListTally($pickListHdr, $pickListDtls, $outbOrdDtls)
  	{
		//format to outbOrdDtlsHashByItemId
		$outbOrdDtlsHashByItemId = array();
		foreach($outbOrdDtls as $outbOrdDtl)
		{
			$tmpOutbOrdDtls = array();
			if(array_key_exists($outbOrdDtl->item_id, $outbOrdDtlsHashByItemId))
			{
				$tmpOutbOrdDtls = $outbOrdDtlsHashByItemId[$outbOrdDtl->item_id];
			}
			$tmpOutbOrdDtls[] = $outbOrdDtl;
			$outbOrdDtlsHashByItemId[$outbOrdDtl->item_id] = $tmpOutbOrdDtls;
		}

		//format to pickListDtlsHashByItemId
		$pickListDtlsHashByItemId = array();
		foreach($pickListDtls as $pickListDtl)
		{
			$tmpPickListDtls = array();
			if(array_key_exists($pickListDtl->item_id, $pickListDtlsHashByItemId))
			{
				$tmpPickListDtls = $pickListDtlsHashByItemId[$pickListDtl->item_id];
			}
			$tmpPickListDtls[] = $pickListDtl;
			$pickListDtlsHashByItemId[$pickListDtl->item_id] = $tmpPickListDtls;
		}

		//loop the pickListDtlsHashByItemId, and check for each itemId
		foreach($pickListDtlsHashByItemId as $itemId => $pickListDtls)
		{
			$itemModel = null;
			$pickListUnitQty = 0;
			foreach($pickListDtls as $pickListDtl)
			{
				$itemModel = $pickListDtl->item;
				$unitQty = bcmul($pickListDtl->uom_rate, $pickListDtl->qty, 10);
				$pickListUnitQty = bcadd($pickListUnitQty, $unitQty, 10);
			}

			if(array_key_exists($itemId, $outbOrdDtlsHashByItemId) === false)
			{
				$exc = new ApiException(__('PickList.outb_ord_item_not_tally', ['docCode'=>$pickListHdr->doc_code, 'itemCode'=>$itemModel->code, 'pickUnitQty'=>number_format($pickListUnitQty, config('scm.decimal_scale')), 'ordUnitQty'=>0]));
				$exc->addData(\App\PickListDtl::class, $pickListHdr->id);
				throw $exc;
			}

			$outbOrdUnitQty = 0;
			$outbOrdDtls = $outbOrdDtlsHashByItemId[$itemId];
			foreach($outbOrdDtls as $outbOrdDtl)
			{
				$unitQty = bcmul($outbOrdDtl->uom_rate, $outbOrdDtl->qty, 10);
				$outbOrdUnitQty = bcadd($outbOrdUnitQty, $unitQty, 10);
			}

			if(bccomp($pickListUnitQty, $outbOrdUnitQty, 10) != 0)
			{
				$exc = new ApiException(__('PickList.outb_ord_item_not_tally', ['docCode'=>$pickListHdr->doc_code, 'itemCode'=>$itemModel->code, 'pickUnitQty'=>number_format($pickListUnitQty, config('scm.decimal_scale')), 'ordUnitQty'=>number_format($outbOrdUnitQty, config('scm.decimal_scale'))]));
				$exc->addData(\App\PickListDtl::class, $pickListHdr->id);
				throw $exc;
			}

			unset($outbOrdDtlsHashByItemId[$itemId]);
		}

		foreach($outbOrdDtlsHashByItemId as $itemId => $remOutbOrdDtls)
		{
			foreach($remOutbOrdDtls as $remOutbOrdDtl)
			{
				$itemModel = $remOutbOrdDtl->item;
				$outbOrdUnitQty = bcmul($remOutbOrdDtl->uom_rate, $remOutbOrdDtl->qty, 10);

				$exc = new ApiException(__('PickList.outb_ord_item_not_tally', ['docCode'=>$pickListHdr->doc_code, 'itemCode'=>$itemModel->code, 'pickUnitQty'=>0, 'ordUnitQty'=>number_format($outbOrdUnitQty, config('scm.decimal_scale'))]));
				$exc->addData(\App\PickListDtl::class, $pickListHdr->id);
				throw $exc;
			}
		}
		
		return true;
	}

	private function getOutbOrdHdrIdsAsc($pickListOutbOrds)
	{
		$outbOrdHdrIds = array();
		foreach($pickListOutbOrds as $pickListOutbOrd)
		{
			$outbOrdHdrIds[] = $pickListOutbOrd->outb_ord_hdr_id;
		}
		sort($outbOrdHdrIds);

		$key = '';
		foreach($outbOrdHdrIds as $outbOrdHdrId)
		{
			$key .= $outbOrdHdrId . '/';
		}

		return $key;
	}

	protected function createInvDoc01($procType, $hdrIds)
  	{
		$invDocArray = array();
		$toDocTxnFlowArray = array();
		//to keep the previous processed sister pickListHdrId
		//will use for create inv/do later, if already exist,
		//then just add the toDocTxnFlowData
		$processedSisterHdrIdHash = array();

		$pickListHdrs = PickListHdrRepository::findAllByHdrIds($hdrIds);
		$pickListHdrs->load(
			'pickListDtls', 'pickListDtls.item', 
			'pickListOutbOrds', 'pickListOutbOrds.outbOrdHdr',
			'pickListOutbOrds.outbOrdHdr.outbOrdDtls',
			'pickListOutbOrds.outbOrdHdr.outbOrdDtls.item'
		);

		//now will expect the sister pick lists will be selected at the same time
		//loop the pickListHdrs, and find out the sister pickLists (same outOrdHdrIds)
		//using hashtable to structure the sister pickLists by outbOrdHdrIds
		$pickListHdrIdsHashByOutbOrdHdrIds = array();
		$pickListDtlsHashByHdrId = array();
		foreach($pickListHdrs as $pickListHdr)
		{
			$pickListDtls = $pickListHdr->pickListDtls;
			$pickListDtlsHashByHdrId[$pickListHdr->id] = $pickListDtls;

			$pickListOutbOrds = $pickListHdr->pickListOutbOrds;

			$key = $this->getOutbOrdHdrIdsAsc($pickListOutbOrds);

			$pickListHdrIds = array();
			if(array_key_exists($key, $pickListHdrIdsHashByOutbOrdHdrIds))
			{
				$pickListHdrIds = $pickListHdrIdsHashByOutbOrdHdrIds[$key];
			}
			$pickListHdrIds[] = $pickListHdr->id;

			$pickListHdrIdsHashByOutbOrdHdrIds[$key] = $pickListHdrIds;
		}

		$invTxnFlowHashByDivisionIdAndToResType = array();
		foreach($pickListHdrs as $pickListHdr)
		{
			$pickListDtls = $pickListHdr->pickListDtls;
			$pickListOutbOrds = $pickListHdr->pickListOutbOrds;

			$key = $this->getOutbOrdHdrIdsAsc($pickListOutbOrds);

			//check if this pick list got sister pick list
			if(array_key_exists($key, $pickListHdrIdsHashByOutbOrdHdrIds))
			{
				$sisterPickListHdrId = 0;
				$samePickListHdrIds = $pickListHdrIdsHashByOutbOrdHdrIds[$key];
				foreach($samePickListHdrIds as $samePickListHdrId)
				{
					if($samePickListHdrId != $pickListHdr->id)
					{
						$sisterPickListHdrId = $samePickListHdrId;
					}
				}

				if($sisterPickListHdrId > 0)
				{
					//sister pick list exist when they share same outbOrdHdrs, but different locations
					//like order A has 2 dtls, dtl A is location 1, dtl B is location 2, then will have 2 pick list
					//pick list A for location 1, and pick list B for location 2
					//in this case, pick list A and pick list B is sister pick lists
					//here we combine the other sister pick list dtl to this pick list dtls
					//so both sister pick lists can pass the checkPickListTally checking
					$sisterPickListDtls = $pickListDtlsHashByHdrId[$sisterPickListHdrId];
					$pickListDtls = $pickListDtls->concat($sisterPickListDtls);

					$processedSisterHdrIdHash[$sisterPickListHdrId] = $sisterPickListHdrId;
				}
			}

			$outbOrdDtlsHashByHdrId = array();
			$outbOrdHdrs = array();
			$allOutbOrdDtls = array();
			foreach($pickListOutbOrds as $pickListOutbOrd)
			{
				$outbOrdHdr = $pickListOutbOrd->outbOrdHdr;
				//if status is not COMPLETE, then skip
				if($outbOrdHdr->doc_status != DocStatus::$MAP['COMPLETE'])
				{
					continue;
				}

				$outbOrdDtls = $outbOrdHdr->outbOrdDtls;
				foreach($outbOrdDtls as $outbOrdDtl)
				{
					$tmpOutbOrdDtls = array();
					if(array_key_exists($outbOrdDtl->hdr_id, $outbOrdDtlsHashByHdrId))
					{
						$tmpOutbOrdDtls = $outbOrdDtlsHashByHdrId[$outbOrdDtl->hdr_id];
					}
					$tmpOutbOrdDtls[] = $outbOrdDtl;
					$outbOrdDtlsHashByHdrId[$outbOrdDtl->hdr_id] = $tmpOutbOrdDtls;
					$allOutbOrdDtls[] = $outbOrdDtl;
				}
				unset($outbOrdHdr->outbOrdDtls);
				$outbOrdHdrs[$outbOrdHdr->id] = $outbOrdHdr;
			}
			
			//validation
			//make sure pickQty is same as ordQty
			self::checkPickListTally($pickListHdr, $pickListDtls, $allOutbOrdDtls);

			//loop each outbOrdHdr, query the invTxnFlow, to see the next inv doc
			//if next inv doc not yet generate, generate here
			foreach($outbOrdHdrs as $outbOrdHdr)
			{
				$invTxnFlow = InvTxnFlowRepository::findByDivisionIdAndFrResType($outbOrdHdr->division_id, ResType::$MAP['OUTB_ORD']);
				$key = $invTxnFlow->division_id.'/'.$invTxnFlow->to_res_type;
				$invTxnFlowHashByDivisionIdAndToResType[$key] = $invTxnFlow;
				if(strcmp($invTxnFlow->to_res_type, ResType::$MAP['SLS_INV']) == 0)
				{
					if($outbOrdHdr->sls_inv_hdr_id > 0
					|| array_key_exists($pickListHdr->id, $processedSisterHdrIdHash))
					{
						//this outbOrd oledi issue invoice, so only add the docTxnFlow
						$toDocTxnFlowData = array(
							'proc_type' => $procType,
							'fr_doc_hdr_type' => \App\PickListHdr::class,
							'fr_doc_hdr_id' => $pickListHdr->id,
							'fr_doc_hdr_code' => $pickListHdr->doc_code,
							'to_doc_hdr_type' => \App\SlsInvHdr::class,
							'to_doc_hdr_id' => $outbOrdHdr->sls_inv_hdr_id,
							'is_closed' => 1
						);
						$toDocTxnFlowArray[] = $toDocTxnFlowData;
					}
					else
					{
						//return associative array, doc_type, doc_no_id, hdr_data, dtl_array, outb_ord_hdr_id
						$invDocInfo = SlsInvService::createFrOutbOrd($outbOrdHdr, $outbOrdDtlsHashByHdrId[$outbOrdHdr->id], $pickListHdr);
						$invDocArray[] = $invDocInfo;
					}
				}
				elseif(strcmp($invTxnFlow->to_res_type, ResType::$MAP['DEL_ORD']) == 0
				&& $outbOrdHdr->del_ord_hdr_id == 0)
				{
					if($outbOrdHdr->del_ord_hdr_id > 0
					|| array_key_exists($pickListHdr->id, $processedSisterHdrIdHash))
					{
						//this outbOrd oledi issue delivery order, so only add the docTxnFlow
						$toDocTxnFlowData = array(
							'proc_type' => $procType,
							'fr_doc_hdr_type' => \App\PickListHdr::class,
							'fr_doc_hdr_id' => $pickListHdr->id,
							'fr_doc_hdr_code' => $pickListHdr->doc_code,
							'to_doc_hdr_type' => \App\DelOrdHdr::class,
							'to_doc_hdr_id' => $outbOrdHdr->del_ord_hdr_id,
							'is_closed' => 1
						);
						$toDocTxnFlowArray[] = $toDocTxnFlowData;
					}
					else
					{
						$invDocInfo = DelOrdService::createFrOutbOrd($outbOrdHdr, $outbOrdDtlsHashByHdrId[$outbOrdHdr->id], $pickListHdr);
						$invDocArray[] = $invDocInfo;
					}
				}
			}
		}

		$invDocHdrArray = InvDocHdrRepository::createProcess($procType, $invDocArray, $toDocTxnFlowArray);
		foreach($invDocHdrArray as $invDocHdrModel)
		{
			$toResType = '';
			if($invDocHdrModel instanceof \App\SlsInvHdr)
			{
				$toResType = ResType::$MAP['SLS_INV'];
			}
			elseif($invDocHdrModel instanceof \App\DelOrdHdr)
			{
				$toResType = ResType::$MAP['DEL_ORD'];
			}

			$key = $invDocHdrModel->division_id.'/'.$toResType;
			$invTxnFlow = $invTxnFlowHashByDivisionIdAndToResType[$key];

			if($invDocHdrModel instanceof \App\SlsInvHdr)
			{
				if($invTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
				{
					SlsInvService::transitionToWip($invDocHdrModel->id);
				}
				elseif($invTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
				{
					SlsInvService::transitionToComplete($invDocHdrModel->id);
				}
			}
			elseif($invDocHdrModel instanceof \App\DelOrdHdr)
			{
				if($invTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
				{
					DelOrdService::transitionToWip($invDocHdrModel->id);
				}
				elseif($invTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
				{
					DelOrdService::transitionToComplete($invDocHdrModel->id);
				}
			}
		}

		$c = 0;
        $docCodes = '';
        foreach($invDocHdrArray as $invDocHdr)
        {
            if ($c == 0) 
            {
                $docCodes .= $invDocHdr->doc_code;
            }
            else
            {
                $docCodes .= ', '.$invDocHdr->doc_code;
            }
            $c++;
        }
        $message = __('InvDoc.document_successfully_created', ['docCode'=>$docCodes]);

		return array(
			'data' => $invDocHdrArray,
			'message' => $message
		);
	}

	public function printProcess($strProcType, $hdrIds)
  	{		  
		$user = Auth::user();
		
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['INV_DOC_01_01']) 
			{
				//print inv docs from pick list
				return $this->printInvDoc0101(ProcType::$MAP[$strProcType], $user, $hdrIds);
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['INV_DOC_01_02']) 
			{
				//print inv docs from pick list, will not add INV_DOC_01_01
				return $this->printInvDoc0101(ProcType::$MAP[$strProcType], $user, $hdrIds, true);
			}
		}
	}

	public function printInvDoc0101($procType, $user, $hdrIds, $isInvDoc0102 = false)
  	{
		$toDocTxnFlowArray = array();
		$invDocHdrs = array();
		$pickListHdrs = PickListHdrRepository::findAllByHdrIds($hdrIds);
		$pickListHdrs->load('pickListOutbOrds');
		foreach($pickListHdrs as $pickListHdr)
		{
			$pickListOutbOrds = $pickListHdr->pickListOutbOrds;
			$outbOrdHdrIds = array();
			foreach($pickListOutbOrds as $pickListOutbOrd)
			{
				$outbOrdHdrIds[] = $pickListOutbOrd->outb_ord_hdr_id;
			}

			$outbOrdHdrs = OutbOrdHdrRepository::findAllByIds($outbOrdHdrIds);
			foreach($outbOrdHdrs as $outbOrdHdr)
			{
				$docHdrType = '';
				$invDocHdr = null;
				if(strcmp($outbOrdHdr->cur_res_type, ResType::$MAP['SLS_INV']) == 0)
				{
					$invDocHdr = SlsInvHdrRepository::findByPk($outbOrdHdr->sls_inv_hdr_id);
					$tmpDocHdrs = SlsInvService::formatDocuments(array($invDocHdr));
					$invDocHdr = $tmpDocHdrs[0];
				}
				elseif(strcmp($outbOrdHdr->cur_res_type, ResType::$MAP['DEL_ORD']) == 0)
				{
					$invDocHdr = DelOrdHdrRepository::findByPk($outbOrdHdr->del_ord_hdr_id);
					$tmpDocHdrs = DelOrdService::formatDocuments(array($invDocHdr));
					$invDocHdr = $tmpDocHdrs[0];
				}

				$invDocHdr->pick_list_hdr_id = $pickListHdr->id;
				$invDocHdr->pick_list_hdr_doc_code = $pickListHdr->doc_code;

				$docHdrType = get_class($invDocHdr);
				$printDocSetting = PrintDocSettingRepository::findByDocHdrTypeAndDivisionId($docHdrType, $invDocHdr->division_id);
				if(empty($printDocSetting))
				{
					$exc = new ApiException(__('PrintDoc.report_template_not_found', ['docType'=>$docHdrType]));
					$exc->addData(\App\PrintDocSetting::class, $docHdrType);
					throw $exc;
				}

				$invDocHdr->view_name = $printDocSetting->view_name;
				$invDocHdrs[] = $invDocHdr;
			}

			if($isInvDoc0102 == false)
			{
				$toDocTxnFlowData = array(
					'proc_type' => $procType,
					'fr_doc_hdr_type' => \App\PickListHdr::class,
					'fr_doc_hdr_id' => $pickListHdr->id,
					'fr_doc_hdr_code' => $pickListHdr->doc_code,
					'to_doc_hdr_type' => '',
					'to_doc_hdr_id' => 0,
					'is_closed' => 1
				);
				$toDocTxnFlowArray[] = $toDocTxnFlowData;
			}
		}

		$printDocTxnDataArray = array();
		foreach($invDocHdrs as $invDocHdr)
		{
			$printDocTxnData = array();
			$printDocTxnData['doc_hdr_type'] = get_class($invDocHdr);
			$printDocTxnData['doc_hdr_id'] = $invDocHdr->id;
			$printDocTxnData['user_id'] = $user->id;
			$printDocTxnDataArray[] = $printDocTxnData;
		}

		$printedAt = PrintDocTxnRepository::createProcess($printDocTxnDataArray, $toDocTxnFlowArray);

		$pdf = \Illuminate\Support\Facades\App::make('dompdf.wrapper');
		$pdf->getDomPDF()->set_option("enable_php", true);
		$pdf->setPaper(array(
			0,
			0,
			$printDocSetting->page_width, 
			$printDocSetting->page_height
		), $printDocSetting->page_orientation);

		//each data must have the view_name
		$pdf->loadHTML(
			view('batchPrintDocuments',
				array(
					'data' => $invDocHdrs, 
					'printedAt' => $printedAt,
					'printedBy' => $user,
					'isPageBreakAfter' => true,
				)
			)
		);

		return $pdf->stream();
	}

	protected function indexInvDoc02($user, $siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
            array(
                'gds_rcpt_read'
            )
		);

		$divisionIds = array();
		$userDivisions = $user->userDivisions;
		foreach($userDivisions as $userDivision)
		{
			$divisionIds[] = $userDivision->division_id;
		}

		$gdsRcptHdrs = GdsRcptHdrRepository::findAllNotExistInvDoc02Txn($divisionIds, ProcType::$MAP['GDS_RCPT_02'], $sorts, $filters, $pageSize);
		$gdsRcptHdrs->load(
            'gdsRcptInbOrds', 'gdsRcptInbOrds.inbOrdHdr',
            'gdsRcptDtls', 'gdsRcptDtls.item',
            'gdsRcptInbOrds', 'gdsRcptInbOrds.inbOrdHdr',
            'gdsRcptInbOrds.inbOrdHdr.deliveryPoint',
            'gdsRcptInbOrds.inbOrdHdr.deliveryPoint.area',
            'gdsRcptInbOrds.inbOrdHdr.salesman'
        );
		foreach($gdsRcptHdrs as $gdsRcptHdr)
		{
            $gdsRcptHdr = GdsRcptService::processOutgoingHeader($gdsRcptHdr, true);
            
            $areaHashById = array();
            $deliveryPointHashById = array();
			$salesmanHash = array();
            
            //find all the gdsRcptInbOrds
			$gdsRcptInbOrds = $gdsRcptHdr->gdsRcptInbOrds;
			$inbOrdHdrs = array();
			
			foreach($gdsRcptInbOrds as $gdsRcptInbOrd)
			{
				$inbOrdHdr = $gdsRcptInbOrd->inbOrdHdr;
				$deliveryPoint = $inbOrdHdr->deliveryPoint;
				$deliveryPointHashById[$deliveryPoint->id] = $deliveryPoint;

				$inbOrdHdr->delivery_point_code = $deliveryPoint->code;
				$inbOrdHdr->delivery_point_area_code = $deliveryPoint->area_code;
				$inbOrdHdr->delivery_point_area_desc_01 = $deliveryPoint->area_code;
				$area = $deliveryPoint->area;
				if(!empty($area))
				{
					$areaHashById[$area->id] = $area;
					$inbOrdHdr->delivery_point_area_desc_01 = $area->desc_01;
				}
				$inbOrdHdr->delivery_point_company_name_01 = $deliveryPoint->company_name_01;
                $inbOrdHdr->delivery_point_company_name_02 = $deliveryPoint->company_name_02;
                
                $salesman = $inbOrdHdr->salesman;
				$salesmanHash[$salesman->id] = $salesman;

				$gdsRcptHdr->net_amt = bcadd($gdsRcptHdr->net_amt, $inbOrdHdr->net_amt, 8);

				$inbOrdHdrs[] = $inbOrdHdr;
			}
			$gdsRcptHdr->inb_ord_hdrs = $inbOrdHdrs;
			$gdsRcptHdr->areas = array_values($areaHashById);
			$gdsRcptHdr->delivery_points = array_values($deliveryPointHashById);
			$gdsRcptHdr->salesmans = array_values($salesmanHash);
			
			//query the inbOrds
			$gdsRcptInbOrds = $gdsRcptHdr->gdsRcptInbOrds;
			for($a = 0; $a < count($gdsRcptInbOrds); $a++)
			{
				$gdsRcptInbOrd = $gdsRcptInbOrds[$a];
				$inbOrdHdr = $gdsRcptInbOrd->inbOrdHdr;
				$gdsRcptHdr->sls_rtn_hdr_code = $inbOrdHdr->sls_rtn_hdr_code;
                $gdsRcptHdr->ref_code_01 = $inbOrdHdr->ref_code_01;
                $gdsRcptHdr->ref_code_02 = $inbOrdHdr->ref_code_02;
                $gdsRcptHdr->ref_code_03 = $inbOrdHdr->ref_code_03;
                $gdsRcptHdr->ref_code_04 = $inbOrdHdr->ref_code_04;
                $gdsRcptHdr->ref_code_05 = $inbOrdHdr->ref_code_05;
                $gdsRcptHdr->ref_code_06 = $inbOrdHdr->ref_code_06;
                $gdsRcptHdr->ref_code_07 = $inbOrdHdr->ref_code_07;
                $gdsRcptHdr->ref_code_08 = $inbOrdHdr->ref_code_08;
                break;
			}

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$gdsRcptDtls = $gdsRcptHdr->gdsRcptDtls;
			foreach($gdsRcptDtls as $gdsRcptDtl)
			{
				$gdsRcptDtl = GdsRcptService::processOutgoingDetail($gdsRcptDtl);

				$caseQty = bcadd($caseQty, $gdsRcptDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $gdsRcptDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $gdsRcptDtl->cubic_meter, 8);

				unset($gdsRcptDtl->item);
			}
			
			$gdsRcptHdr->case_qty = $caseQty;
			$gdsRcptHdr->gross_weight = $grossWeight;
			$gdsRcptHdr->cubic_meter = $cubicMeter;

			$gdsRcptHdr->details = $gdsRcptDtls;
		}
    	return $gdsRcptHdrs;
	}

	protected function createInvDoc02($procType, $hdrIds)
  	{
		$allInbOrdHdrs = array();
		$inbOrdDtlsHashByHdrId = array();
		$invDocArray = array();
		$toDocTxnFlowArray = array();
		$gdsRcptHdrHashByInbOrdHdrId = array();

		$gdsRcptHdrs = GdsRcptHdrRepository::findAllByHdrIds($hdrIds);
		$gdsRcptHdrs->load(
			'gdsRcptDtls', 'gdsRcptDtls.item',
			'gdsRcptInbOrds', 'gdsRcptInbOrds.inbOrdHdr',
			'gdsRcptInbOrds.inbOrdHdr.inbOrdDtls'
		);
		foreach($gdsRcptHdrs as $gdsRcptHdr)
		{
			$invTxnFlowHashByDivisionIdAndToResType = array();
			//loop each inbOrdHdr, query the invTxnFlow, to see the next inv doc
			//if next inv doc not yet generate, generate here
			$allInbOrdDtls = array();

			$gdsRcptDtls = $gdsRcptHdr->gdsRcptDtls;
			$gdsRcptInbOrds = $gdsRcptHdr->gdsRcptInbOrds;
			foreach($gdsRcptInbOrds as $gdsRcptInbOrd)
			{
				$inbOrdHdr = $gdsRcptInbOrd->inbOrdHdr;
				$allInbOrdHdrs[] = $inbOrdHdr;

				$inbOrdDtls = $inbOrdHdr->inbOrdDtls;
				foreach($inbOrdDtls as $inbOrdDtl)
				{
					$allInbOrdDtls[] = $inbOrdDtl;
				}
				
				$inbOrdDtlsHashByHdrId[$inbOrdHdr->id] = $inbOrdDtls;
				$gdsRcptHdrHashByInbOrdHdrId[$inbOrdHdr->id] = $gdsRcptHdr;
			}			
			
			//validation
			//make sure pickQty is same as ordQty
			self::checkGdsRcptTally($gdsRcptHdr, $gdsRcptDtls, $allInbOrdDtls);
		}

		foreach($allInbOrdHdrs as $inbOrdHdr)
		{
			$gdsRcptHdr = $gdsRcptHdrHashByInbOrdHdrId[$inbOrdHdr->id];
			$invTxnFlow = InvTxnFlowRepository::findByDivisionIdAndFrResType($inbOrdHdr->division_id, ResType::$MAP['INB_ORD']);
			$key = $invTxnFlow->division_id.'/'.$invTxnFlow->to_res_type;
			$invTxnFlowHashByDivisionIdAndToResType[$key] = $invTxnFlow;
			if(strcmp($invTxnFlow->to_res_type, ResType::$MAP['RTN_RCPT']) == 0)
			{
				if($inbOrdHdr->rtn_rcpt_hdr_id > 0)
				{
					//this inbOrd oledi issue return receipt, so only add the docTxnFlow
					$toDocTxnFlowData = array(
						'proc_type' => $procType,
						'fr_doc_hdr_type' => \App\GdsRcptHdr::class,
						'fr_doc_hdr_id' => $gdsRcptHdr->id,
						'fr_doc_hdr_code' => $gdsRcptHdr->doc_code,
						'to_doc_hdr_type' => \App\GdsRcptHdr::class,
						'to_doc_hdr_id' => $inbOrdHdr->rtn_rcpt_hdr_id,
						'is_closed' => 1
					);
					$toDocTxnFlowArray[] = $toDocTxnFlowData;
				}
				else
				{
					//return associative array, doc_type, doc_no_id, hdr_data, dtl_array, outb_ord_hdr_id
					$invDocInfo = RtnRcptService::createFrInbOrd(
						$inbOrdHdr, 
						$inbOrdDtlsHashByHdrId[$inbOrdHdr->id], 
						$gdsRcptHdr
					);
					$invDocArray[] = $invDocInfo;
				}
			}
		}

		$invDocHdrArray = InvDocHdrRepository::createProcess(
			$procType, 
			$invDocArray, 
			$toDocTxnFlowArray
		);
		foreach($invDocHdrArray as $invDocHdrModel)
		{
			$toResType = '';
			if($invDocHdrModel instanceof \App\RtnRcptHdr)
			{
				$toResType = ResType::$MAP['RTN_RCPT'];
			}

			$key = $invDocHdrModel->division_id.'/'.$toResType;
			$invTxnFlow = $invTxnFlowHashByDivisionIdAndToResType[$key];

			if($invDocHdrModel instanceof \App\RtnRcptHdr)
			{
				if($invTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
				{
					RtnRcptService::transitionToWip($invDocHdrModel->id);
				}
				elseif($invTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
				{
					RtnRcptService::transitionToComplete($invDocHdrModel->id);
				}
			}
		}

		$c = 0;
        $docCodes = '';
        foreach($invDocHdrArray as $invDocHdr)
        {
            if ($c == 0) 
            {
                $docCodes .= $invDocHdr->doc_code;
            }
            else
            {
                $docCodes .= ', '.$invDocHdr->doc_code;
            }
            $c++;
        }
        $message = __('InvDoc.document_successfully_created', ['docCode'=>$docCodes]);

		return array(
			'data' => $invDocHdrArray,
			'message' => $message
		);
	}

	protected static function checkGdsRcptTally($gdsRcptHdr, $gdsRcptDtls, $inbOrdDtls)
  	{
		//format to inbOrdDtlsHashByItemId
		$inbOrdDtlsHashByItemId = array();
		foreach($inbOrdDtls as $inbOrdDtl)
		{
			$tmpInbOrdDtls = array();
			if(array_key_exists($inbOrdDtl->item_id, $inbOrdDtlsHashByItemId))
			{
				$tmpInbOrdDtls = $inbOrdDtlsHashByItemId[$inbOrdDtl->item_id];
			}
			$tmpInbOrdDtls[] = $inbOrdDtl;
			$inbOrdDtlsHashByItemId[$inbOrdDtl->item_id] = $tmpInbOrdDtls;
		}

		//format to gdsRcptDtlsHashByItemId
		$gdsRcptDtlsHashByItemId = array();
		foreach($gdsRcptDtls as $gdsRcptDtl)
		{
			$tmpGdsRcptDtls = array();
			if(array_key_exists($gdsRcptDtl->item_id, $gdsRcptDtlsHashByItemId))
			{
				$tmpGdsRcptDtls = $gdsRcptDtlsHashByItemId[$gdsRcptDtl->item_id];
			}
			$tmpGdsRcptDtls[] = $gdsRcptDtl;
			$gdsRcptDtlsHashByItemId[$gdsRcptDtl->item_id] = $tmpGdsRcptDtls;
		}

		//loop the gdsRcptDtlsHashByItemId, and check for each itemId
		foreach($gdsRcptDtlsHashByItemId as $itemId => $gdsRcptDtls)
		{
			$itemModel = null;
			$gdsRcptUnitQty = 0;
			foreach($gdsRcptDtls as $gdsRcptDtl)
			{
				$itemModel = $gdsRcptDtl->item;
				$unitQty = bcmul($gdsRcptDtl->uom_rate, $gdsRcptDtl->qty, 10);
				$gdsRcptUnitQty = bcadd($gdsRcptUnitQty, $unitQty, 10);
			}

			if(array_key_exists($itemId, $inbOrdDtlsHashByItemId) === false)
			{
				$exc = new ApiException(__('GdsRcpt.inb_ord_item_not_tally', [
					'docCode'=>$gdsRcptHdr->doc_code, 
					'itemCode'=>$itemModel->code, 
					'pickUnitQty'=>number_format($gdsRcptUnitQty, config('scm.decimal_scale')), 
					'ordUnitQty'=>0
					]));
				$exc->addData(\App\GdsRcptHdr::class, $gdsRcptHdr->id);
				throw $exc;
			}

			$inbOrdUnitQty = 0;
			$inbOrdDtls = $inbOrdDtlsHashByItemId[$itemId];
			foreach($inbOrdDtls as $inbOrdDtl)
			{
				$unitQty = bcmul($inbOrdDtl->uom_rate, $inbOrdDtl->qty, 10);
				$inbOrdUnitQty = bcadd($inbOrdUnitQty, $unitQty, 10);
			}

			if(bccomp($gdsRcptUnitQty, $inbOrdUnitQty, 10) != 0)
			{
				$exc = new ApiException(__('GdsRcpt.inb_ord_item_not_tally', [
					'docCode'=>$gdsRcptHdr->doc_code, 
					'itemCode'=>$itemModel->code, 
					'pickUnitQty'=>number_format($gdsRcptUnitQty, config('scm.decimal_scale')), 
					'ordUnitQty'=>number_format($inbOrdUnitQty, config('scm.decimal_scale'))
					]));
				$exc->addData(\App\GdsRcptDtl::class, $gdsRcptHdr->id);
				throw $exc;
			}
		}
		
		return true;
	}
}
