<?php

namespace App\Services;

use App\Message;
use App\Repositories\MessageRepository;

class MessageService 
{
    public function __construct() 
	{
    }
    
    public function initModel()
    {
        // AuthService::authorize(
        //     array(
        //         'message_create'
        //     )
        // );

        $model = new Message();
        $model->priority = 0;
        $model->status = 0;

        return $model;
    }

    public function createModel($data)
    {		
        // AuthService::authorize(
        //     array(
        //         'message_create'
        //     )
        // );

        $data = self::processIncomingModel($data, true);
        $model = MessageRepository::createModel($data);

        //create a sync hdr setting for new division

        $message = __('Message.record_successfully_created');

		return array(
            'data' => $model->id,
			'message' => $message
		);
    }
    
    public function showModel($id)
    {
        // AuthService::authorize(
        //     array(
        //         'message_read',
		// 		'message_update'
        //     )
        // );

        $model = MessageRepository::findByPk($id);
		if(!empty($model))
        {
            $model = self::processOutgoingModel($model);
        }
        
        return $model;
    }

    public function deleteModel($id)
    {
        // AuthService::authorize(
        //     array(
        //         'message_read',
		// 		'message_update'
        //     )
        // );

        $result = MessageRepository::deleteModel($id);
        $model = $result['model'];
        
		$message = __('Message.record_successfully_updated', ['id'=>$model->id]);
		
		return array(
			'data' => array(
                'timestamp'=>time(),
                'model'=>$model,
            ),
			'message' => $message
		);
    }

    public function updateModel($data)
	{
        // AuthService::authorize(
        //     array(
        //         'message_update'
        //     )
        // );

        $data = self::processIncomingModel($data);

        $message = MessageRepository::findByPk($data['id']);
        $messageData = $message->toArray();
        //assign data to model
        foreach($data as $field => $value) 
        {
            $messageData[$field] = $value;
        }

        $result = MessageRepository::updateModel($messageData);
        $model = $result['model'];

        $model = self::processOutgoingModel($model);

        $message = __('Message.record_successfully_updated', ['id'=>$model->id]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'model'=>$model
            ),
			'message' => $message
		);
    }

    public function markAllAsSeen($userId, $sorts, $filters = array(), $currentPage, $pageSize, $data)
	{
        // AuthService::authorize(
        //     array(
        //         'message_update'
        //     )
        // );
        
        $data = self::processIncomingModel($data);

        $messages = MessageRepository::findAllByUserId($userId, $sorts, $filters, 0);

        $models = [];
        $limit = $currentPage * $pageSize;
        foreach($messages as $key => $message) {
            if($key < $limit) {
                if($message->status == 0) {
                    $data['id'] = $message->id;
                    $result = $this->updateModel($data);

                    $models[] = $result['data']['model'];
                } else {
                    $models[] = $message;
                }
            }
        }

		return $models;
    }

    public function clearNotifications($userId, $sorts, $filters = array())
	{
        // AuthService::authorize(
        //     array(
        //         'message_update'
        //     )
        // );

        $messages = MessageRepository::findAllByUserId($userId, $sorts, $filters, 0);
        
        $result = [];
        
        foreach($messages as $key => $message) {
            $result[] = $this->deleteModel($message->id, $userId);
        }

		return array(
            'deleted_models' => $result
        );
    }

    public function index($userId, $sorts, $filters = array(), $pageSize = 20)
    {
        $messages = MessageRepository::findAllByUserId($userId, $sorts, $filters, $pageSize);

        return $messages;
    }
    
    static public function processIncomingModel($data)
    {
        if(array_key_exists('submit_action', $data))
        {
            unset($data['submit_action']);
        }

        return $data;
    }

    static public function processOutgoingModel($model)
	{
		return $model;
    }
}
