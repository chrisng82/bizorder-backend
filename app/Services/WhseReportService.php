<?php

namespace App\Services;

use App\Services\Env\ResStatus;
use App\Services\Env\ResType;
use App\Services\Env\PhysicalCountStatus;
use App\Repositories\WhseReportRepository;
use App\Repositories\LocationRepository;
use App\Repositories\SiteFlowRepository;
use App\Repositories\OutbOrdHdrRepository;
use App\Repositories\InbOrdHdrRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class WhseReportService 
{
    public function __construct() 
	{
    }
    
    public function stockBalance($siteFlowId, $sorts, $criteria = array(), $columns = array(), $pageSize = 20) 
	{
        AuthService::authorize(
			array(
				'stock_balance_report'
			)
        );
        
        //DB::connection()->enableQueryLog();
        $quantBals = WhseReportRepository::stockBalance($siteFlowId, $sorts, $criteria, $columns, $pageSize);
        //Log::error(DB::getQueryLog());
        
        $quantBals->load('item');
		foreach($quantBals as $quantBal)
		{
            $item = $quantBal->item;

            $quantBal = ItemService::processCaseLoose($quantBal, $item, 1);
            
            unset($quantBal->item);
		}
        return $quantBals;
    }

    public function initStockBalance($siteFlowId)
    {
        AuthService::authorize(
			array(
				'stock_balance_report'
			)
        );

        $siteFlow = SiteFlowRepository::findByPk($siteFlowId);

        $locations = LocationRepository::findAllByGoodStock($siteFlow->site_id);
        $locationOptions = array();
        foreach($locations as $location)
        {
            $locationOptions[] = array(
                'value'=>$location->id, 
                'label'=>$location->code
            );
        }

        $criteria = array();
        $criteria['columns'] = array(
            'source'=>array(
                array('column'=>'storage_bin_code'),
                array('column'=>'storage_row_code'),
                array('column'=>'location_code'),
                array('column'=>'storage_bay_code'),
                array('column'=>'item_desc_02'),
                array('column'=>'batch_serial_no'),
                array('column'=>'receipt_date'),
                array('column'=>'unit_qty'),
                array('column'=>'handling_unit'),
                array('column'=>'picking_unit_qty'),                
            ),
            'target'=>array(
                array('column'=>'item_code'),
                array('column'=>'item_desc_01'),
                array('column'=>'expiry_date'),
                array('column'=>'case_qty'),
                array('column'=>'item_case_uom_code'),
                array('column'=>'case_uom_rate'),
                array('column'=>'loose_qty'),
                array('column'=>'item_loose_uom_code'),
                array('column'=>'loose_uom_rate'),
            ),
        );
        $criteria['date'] = date('Y-m-d');
        $criteria['item_ids_select2'] = array();
        $criteria['item_group_01_ids_select2'] = array();
        $criteria['item_group_02_ids_select2'] = array();
        $criteria['item_group_03_ids_select2'] = array();
        $criteria['storage_bin_ids_select2'] = array();
        $criteria['storage_row_ids_select2'] = array();
        $criteria['storage_bay_ids_select2'] = array();
        $criteria['location_ids_select2'] = $locationOptions;
        $criteria['is_show_zero_balance'] = false;
        $criteria['expiry_days'] = 0;

        return $criteria;
    }


    public function cycleCountAnalysis($siteFlowId, $sorts, $criteria = array(), $columns = array(), $pageSize = 20) 
	{
        AuthService::authorize(
			array(
				'cycle_count_analysis_report'
			)
        );

        //DB::connection()->enableQueryLog();
        $cycleCountDtls = WhseReportRepository::cycleCountAnalysis($siteFlowId, $sorts, $criteria, $columns, $pageSize);
        //Log::error(DB::getQueryLog());

        $cycleCountDtls->load('item');
		foreach($cycleCountDtls as $cycleCountDtl)
		{
            $item = $cycleCountDtl->item;

            $cycleCountDtl = ItemService::processCaseLoose($cycleCountDtl, $item, 1);
            $cycleCountDtl->str_physical_count_status = PhysicalCountStatus::$MAP[$cycleCountDtl->physical_count_status];
            
            unset($cycleCountDtl->item);
		}
        return $cycleCountDtls;
    }

    public function initCycleCountAnalysis($siteFlowId)
    {        
        AuthService::authorize(
			array(
				'cycle_count_analysis_report'
			)
        );

        $siteFlow = SiteFlowRepository::findByPk($siteFlowId);

        $criteria = array();
        $criteria['columns'] = array(
            'source'=>array(
                array('column'=>'job_no'),
                array('column'=>'ref_code_01'),
                array('column'=>'doc_date'),
                array('column'=>'storage_bin_code'),
                array('column'=>'storage_row_code'),
                array('column'=>'location_code'),
                array('column'=>'storage_bay_code'),
                array('column'=>'item_desc_02'),
                array('column'=>'batch_serial_no'),
                array('column'=>'receipt_date'),
                array('column'=>'unit_qty'),
                array('column'=>'handling_unit'),
            ),
            'target'=>array(
                array('column'=>'doc_code'),
                array('column'=>'group_no'),
                array('column'=>'item_code'),
                array('column'=>'item_desc_01'),
                array('column'=>'expiry_date'),
                array('column'=>'case_qty'),
                array('column'=>'item_case_uom_code'),
                array('column'=>'case_uom_rate'),
                array('column'=>'loose_qty'),
                array('column'=>'item_loose_uom_code'),
                array('column'=>'loose_uom_rate'),
                array('column'=>'str_physical_count_status'),
            ),
        );
        $criteria['start_date'] = date('Y-m-d');
        $criteria['end_date'] = date('Y-m-d');
        $criteria['item_ids_select2'] = array();
        $criteria['item_group_01_ids_select2'] = array();
        $criteria['item_group_02_ids_select2'] = array();
        $criteria['item_group_03_ids_select2'] = array();
        $criteria['storage_bin_ids_select2'] = array();
        $criteria['storage_row_ids_select2'] = array();
        $criteria['storage_bay_ids_select2'] = array();
        $criteria['location_ids_select2'] = array();

        return $criteria;
    }

    public function stockCard($siteFlowId, $sorts, $criteria = array(), $columns = array(), $pageSize = 20) 
	{
        AuthService::authorize(
			array(
				'stock_card_report'
			)
        );

        //DB::connection()->enableQueryLog();
        $quantBalTxns = WhseReportRepository::stockCard($siteFlowId, $sorts, $criteria, $columns, $pageSize);
        //Log::error(DB::getQueryLog());

        $quantBalTxns->load(
            'item', 'docHdr'
        );

        //first loop to calculate the cummulative total
        $cumUnitQtyHash = array();
        $cumUnitQtyHashByQuantBalTxnId = array();
		for($a = count($quantBalTxns) - 1; $a >= 0; $a--)
		{
            $quantBalTxn = $quantBalTxns[$a];

            $cumUnitQty = 0;
            if(array_key_exists($quantBalTxn->hash_id, $cumUnitQtyHash))
            {
                $cumUnitQty = $cumUnitQtyHash[$quantBalTxn->hash_id];
            }
            
            $cumUnitQty = bcadd($cumUnitQty, bcmul($quantBalTxn->sign, $quantBalTxn->unit_qty));
            $cumUnitQtyHash[$quantBalTxn->hash_id] = $cumUnitQty;

            $cumUnitQtyHashByQuantBalTxnId[$quantBalTxn->id] = $cumUnitQty;
		}
        
		foreach($quantBalTxns as $quantBalTxn)
		{
            $item = $quantBalTxn->item;
            
            $quantBalTxn = ItemService::processCaseLoose($quantBalTxn, $item, 2);

            $docHdr = $quantBalTxn->docHdr;
            $quantBalTxn->doc_code = $docHdr->doc_code;

            $quantBalTxn->ttl_unit_qty = bcmul($quantBalTxn->sign, $quantBalTxn->unit_qty, $item->qty_scale);

            $quantBalTxn->cum_unit_qty = 0;
            if(array_key_exists($quantBalTxn->id, $cumUnitQtyHashByQuantBalTxnId))
            {
                $quantBalTxn->cum_unit_qty = $cumUnitQtyHashByQuantBalTxnId[$quantBalTxn->id];
            }
            $quantBalTxn->cf_balance_unit_qty = bcsub($quantBalTxn->cur_balance_unit_qty, $quantBalTxn->cum_unit_qty, $item->qty_scale);
            $quantBalTxn->balance_unit_qty = bcadd($quantBalTxn->cf_balance_unit_qty, $quantBalTxn->ttl_unit_qty, $item->qty_scale);

            $quantBalTxn->adv_ship_hdr_code = '';
            $quantBalTxn->pur_inv_hdr_code = '';
            $quantBalTxn->rtn_rcpt_hdr_code = '';
            $quantBalTxn->sls_cn_hdr_code = '';
            $quantBalTxn->del_ord_hdr_code = '';
            $quantBalTxn->sls_inv_hdr_code = '';
            if(strcmp($quantBalTxn->doc_hdr_type, \App\PickListHdr::class) == 0)
            {
                //DB::connection()->enableQueryLog();
                $outbOrdHdrs = OutbOrdHdrRepository::findAllByPickListHdrIdAndItemId($quantBalTxn->doc_hdr_id, $quantBalTxn->item_id);
                //Log::error(DB::getQueryLog());
                foreach($outbOrdHdrs as $outbOrdHdr)
                {
                    if(empty($quantBalTxn->del_ord_hdr_code))
                    {
                        $quantBalTxn->del_ord_hdr_code = $outbOrdHdr->del_ord_hdr_code;
                    }
                    else
                    {
                        $quantBalTxn->del_ord_hdr_code .= ','.$outbOrdHdr->del_ord_hdr_code;
                    }

                    if(empty($quantBalTxn->sls_inv_hdr_code))
                    {
                        $quantBalTxn->sls_inv_hdr_code = $outbOrdHdr->sls_inv_hdr_code;
                    }
                    else
                    {
                        $quantBalTxn->sls_inv_hdr_code .= ','.$outbOrdHdr->sls_inv_hdr_code;
                    }
                }
            }
            elseif(strcmp($quantBalTxn->doc_hdr_type, \App\GdsRcptHdr::class) == 0)
            {
                $inbOrdHdrs = InbOrdHdrRepository::findAllByGdsRcptHdrIdAndItemId($quantBalTxn->doc_hdr_id, $quantBalTxn->item_id);
                foreach($inbOrdHdrs as $inbOrdHdr)
                {
                    if(empty($quantBalTxn->adv_ship_hdr_code))
                    {
                        $quantBalTxn->adv_ship_hdr_code = $inbOrdHdr->adv_ship_hdr_code;
                    }
                    else
                    {
                        $quantBalTxn->adv_ship_hdr_code .= ','.$inbOrdHdr->adv_ship_hdr_code;
                    }

                    if(empty($quantBalTxn->pur_inv_hdr_code))
                    {
                        $quantBalTxn->pur_inv_hdr_code = $inbOrdHdr->pur_inv_hdr_code;
                    }
                    else
                    {
                        $quantBalTxn->pur_inv_hdr_code .= ','.$inbOrdHdr->pur_inv_hdr_code;
                    }

                    if(empty($quantBalTxn->rtn_rcpt_hdr_code))
                    {
                        $quantBalTxn->rtn_rcpt_hdr_code = $inbOrdHdr->rtn_rcpt_hdr_code;
                    }
                    else
                    {
                        $quantBalTxn->rtn_rcpt_hdr_code .= ','.$inbOrdHdr->rtn_rcpt_hdr_code;
                    }

                    if(empty($quantBalTxn->sls_cn_hdr_code))
                    {
                        $quantBalTxn->sls_cn_hdr_code = $inbOrdHdr->sls_cn_hdr_code;
                    }
                    else
                    {
                        $quantBalTxn->sls_cn_hdr_code .= ','.$inbOrdHdr->sls_cn_hdr_code;
                    }
                }
            }
            if(in_array('sls_inv_hdr_code', $columns))
            {

            }

            unset($quantBalTxn->item);
		}
        return $quantBalTxns;
    }

    public function initStockCard($siteFlowId)
    {
        AuthService::authorize(
			array(
				'stock_card_report'
			)
        );

        $siteFlow = SiteFlowRepository::findByPk($siteFlowId);

        $locations = LocationRepository::findAllByGoodStock($siteFlow->site_id);
        $locationOptions = array();
        foreach($locations as $location)
        {
            $locationOptions[] = array(
                'value'=>$location->id, 
                'label'=>$location->code
            );
        }

        $criteria = array();
        $criteria['columns'] = array(
            'source'=>array(
                array('column'=>'pur_inv_hdr_code'),  
                array('column'=>'sls_cn_hdr_code'), 
                array('column'=>'del_ord_hdr_code'),                 
                array('column'=>'storage_bin_code'),
                array('column'=>'storage_row_code'),
                array('column'=>'location_code'),
                array('column'=>'storage_bay_code'),
                array('column'=>'item_desc_02'),
                array('column'=>'batch_serial_no'),
                array('column'=>'receipt_date'),
                array('column'=>'expiry_date'),
                array('column'=>'handling_unit'),
                array('column'=>'cur_balance_unit_qty'),
                array('column'=>'cf_balance_unit_qty'),       
            ),
            'target'=>array(
                array('column'=>'posted_at'),
                array('column'=>'item_code'),
                array('column'=>'item_desc_01'),
                array('column'=>'case_uom_rate'),
                array('column'=>'doc_date'),
                array('column'=>'doc_code'),
                array('column'=>'adv_ship_hdr_code'),
                array('column'=>'rtn_rcpt_hdr_code'),
                array('column'=>'sls_inv_hdr_code'),        
                array('column'=>'ttl_unit_qty'),                      
                array('column'=>'balance_unit_qty'),                 
            ),
        );
        $criteria['start_date'] = date('Y-m-d');
        $criteria['end_date'] = date('Y-m-d');
        $criteria['item_ids_select2'] = array();
        $criteria['item_group_01_ids_select2'] = array();
        $criteria['item_group_02_ids_select2'] = array();
        $criteria['item_group_03_ids_select2'] = array();
        $criteria['storage_bin_ids_select2'] = array();
        $criteria['storage_row_ids_select2'] = array();
        $criteria['storage_bay_ids_select2'] = array();
        $criteria['location_ids_select2'] = $locationOptions;

        return $criteria;
    }

    public function countAdjAnalysis($siteFlowId, $sorts, $criteria = array(), $columns = array(), $pageSize = 20) 
	{
        AuthService::authorize(
			array(
				'count_adj_analysis_report'
			)
        );

        //DB::connection()->enableQueryLog();
        $countAdjDtls = WhseReportRepository::countAdjAnalysis($siteFlowId, $sorts, $criteria, $columns, $pageSize);
        //Log::error(DB::getQueryLog());

        $countAdjDtls->load('item');
		foreach($countAdjDtls as $countAdjDtl)
		{
            $item = $countAdjDtl->item;

            $countAdjDtl = ItemService::processCaseLoose($countAdjDtl, $item, 1);
            
            unset($countAdjDtl->item);
		}
        return $countAdjDtls;
    }

    public function initCountAdjAnalysis($siteFlowId)
    {
        AuthService::authorize(
			array(
				'count_adj_analysis_report'
			)
        );

        $siteFlow = SiteFlowRepository::findByPk($siteFlowId);

        $criteria = array();
        $criteria['columns'] = array(
            'source'=>array(
                array('column'=>'ref_code_01'),
                array('column'=>'doc_date'),
                array('column'=>'storage_row_code'),
                array('column'=>'location_code'),
                array('column'=>'storage_bay_code'),
                array('column'=>'item_desc_02'),
                array('column'=>'batch_serial_no'),
                array('column'=>'receipt_date'),
                
                array('column'=>'case_qty'),
                array('column'=>'item_case_uom_code'),
                array('column'=>'loose_qty'),
                array('column'=>'item_loose_uom_code'),
                array('column'=>'loose_uom_rate'),
            ),
            'target'=>array(
                array('column'=>'doc_code'),
                array('column'=>'group_no'),
                array('column'=>'storage_bin_code'),
                array('column'=>'item_code'),
                array('column'=>'item_desc_01'),
                array('column'=>'expiry_date'),
                array('column'=>'handling_unit'),
                array('column'=>'case_uom_rate'),
                array('column'=>'unit_qty'),                
            ),
        );
        $criteria['start_date'] = date('Y-m-d');
        $criteria['end_date'] = date('Y-m-d');
        $criteria['item_ids_select2'] = array();
        $criteria['item_group_01_ids_select2'] = array();
        $criteria['item_group_02_ids_select2'] = array();
        $criteria['item_group_03_ids_select2'] = array();
        $criteria['storage_bin_ids_select2'] = array();
        $criteria['storage_row_ids_select2'] = array();
        $criteria['storage_bay_ids_select2'] = array();
        $criteria['location_ids_select2'] = array();

        return $criteria;
    }

    public function initReservedStock($siteFlowId)
    {
        /*
        AuthService::authorize(
			array(
				'reserved_stock_report'
			)
        );
        */

        $criteria = array();
        $criteria['columns'] = array(
            'source'=>array(
                array('column'=>'del_ord_hdr_code'),
                array('column'=>'sls_inv_hdr_code'),
                array('column'=>'storage_row_code'),
                array('column'=>'location_code'),
                array('column'=>'storage_bay_code'),
                array('column'=>'item_desc_02'),
                array('column'=>'batch_serial_no'),
                array('column'=>'receipt_date'),
                array('column'=>'expiry_date'),
                array('column'=>'handling_unit'),
                array('column'=>'case_qty'),
                array('column'=>'item_case_uom_code'),
                array('column'=>'case_uom_rate'),
                array('column'=>'loose_qty'),
                array('column'=>'item_loose_uom_code'),
                array('column'=>'loose_uom_rate'),
            ),
            'target'=>array(
                array('column'=>'posted_at'),
                array('column'=>'storage_bin_code'),
                array('column'=>'item_code'),
                array('column'=>'item_desc_01'),
                array('column'=>'case_uom_rate'),
                array('column'=>'doc_date'),
                array('column'=>'doc_code'),
                array('column'=>'unit_qty'),          
            ),
        );
        $criteria['start_date'] = date('Y-m-d');
        $criteria['end_date'] = date('Y-m-d');
        $criteria['item_ids_select2'] = array();
        $criteria['item_group_01_ids_select2'] = array();
        $criteria['item_group_02_ids_select2'] = array();
        $criteria['item_group_03_ids_select2'] = array();
        $criteria['storage_bin_ids_select2'] = array();
        $criteria['storage_row_ids_select2'] = array();
        $criteria['storage_bay_ids_select2'] = array();

        return $criteria;
    }

    public function reservedStock($siteFlowId, $sorts, $criteria = array(), $columns = array(), $pageSize = 20) 
	{
        /*
        AuthService::authorize(
			array(
				'reserved_stock_report'
			)
        );
        */

        //DB::connection()->enableQueryLog();
        $quantBalRsvTxns = WhseReportRepository::reservedStock($siteFlowId, $sorts, $criteria, $columns, $pageSize);
        //Log::error(DB::getQueryLog());

        $quantBalRsvTxns->load(
            'item', 'docHdr'
        );
        
		foreach($quantBalRsvTxns as $quantBalRsvTxn)
		{
            $item = $quantBalRsvTxn->item;
            
            $quantBalRsvTxn = ItemService::processCaseLoose($quantBalRsvTxn, $item, 1);

            $docHdr = $quantBalRsvTxn->docHdr;
            $quantBalRsvTxn->doc_code = $docHdr->doc_code;

            $quantBalRsvTxn->sls_cn_hdr_code = '';
            $quantBalRsvTxn->del_ord_hdr_code = '';
            $quantBalRsvTxn->sls_inv_hdr_code = '';
            if(strcmp($quantBalRsvTxn->doc_hdr_type, \App\PickListHdr::class) == 0)
            {
                //DB::connection()->enableQueryLog();
                $outbOrdHdrs = OutbOrdHdrRepository::findAllByPickListHdrIdAndItemId($quantBalRsvTxn->doc_hdr_id, $quantBalRsvTxn->item_id);
                //Log::error(DB::getQueryLog());
                foreach($outbOrdHdrs as $outbOrdHdr)
                {
                    if(empty($quantBalRsvTxn->del_ord_hdr_code))
                    {
                        $quantBalRsvTxn->del_ord_hdr_code = $outbOrdHdr->del_ord_hdr_code;
                    }
                    else
                    {
                        $quantBalRsvTxn->del_ord_hdr_code .= ','.$outbOrdHdr->del_ord_hdr_code;
                    }

                    if(empty($quantBalRsvTxn->sls_inv_hdr_code))
                    {
                        $quantBalRsvTxn->sls_inv_hdr_code = $outbOrdHdr->sls_inv_hdr_code;
                    }
                    else
                    {
                        $quantBalRsvTxn->sls_inv_hdr_code .= ','.$outbOrdHdr->sls_inv_hdr_code;
                    }
                }
            }
            if(in_array('sls_inv_hdr_code', $columns))
            {

            }

            unset($quantBalRsvTxn->item);
		}
        return $quantBalRsvTxns;
    }
}
