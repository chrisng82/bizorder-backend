<?php

namespace App\Services;

use App\Repositories\DeviceRepository;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class NotificationService 
{
    public function __construct() 
	{
    }

    public function register($fcmToken)
    {   
        $device = DeviceRepository::findByToken($fcmToken);

        if(empty($device)) {
            $device = DeviceRepository::register($fcmToken);
        } 

        return $device;
    }

    // public function unregister($fcmToken)
    // {   
    //     $device = DeviceRepository::findByToken($fcmToken);

    //     if(!empty($device)) {
    //         $device = DeviceRepository::unregister($device);
    //     } 

    //     return $device;
    // }

    public function getTokensByUserId($receiver_ids) {
        $tokens = DeviceRepository::findTokensByUserId($receiver_ids);

        $serialize = array();

        foreach($tokens as $token) {
            array_push($serialize, $token['fcm_token']);
        }

        return $serialize;
    }

    //Sending message to a topic by topic name
    public function sendToTopic( $to, $message ) {
        $fields = array(
            'to'   => '/topics/' . $to,
            'data' => $message,
        );

        return $this->sendPushNotification( $fields );
    }


    //Sending push message to multiple users by firebase registration ids
    public function send( $tokens, $message ) {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder();
        $notificationBuilder->setTitle($message['data']['title'])
                            ->setBody($message['data']['message'])
                            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['a_data' => 'my_data']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        //Query tokens here using repository

        return $this->sendPushNotification( $tokens, $option, $notification, $data );
    }

    //Brozot/Laravel-FCM request to firebase servers
    private function sendPushNotification( $tokens, $option, $notification, $data ) {
        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

        $response = array();
        $response['success'] = $downstreamResponse->numberSuccess();
        $response['failure'] = $downstreamResponse->numberFailure();
        $response['modification'] = $downstreamResponse->numberModification();
        $response['tokens_to_delete'] = $downstreamResponse->tokensToDelete();
        $response['tokens_to_modify'] = $downstreamResponse->tokensToModify();
        $response['tokens_to_retry'] = $downstreamResponse->tokensToRetry();
        $response['tokens_with_error'] = $downstreamResponse->tokensWithError();

        return $response;
    }

    public function getPush($title, $message, $data) {
        $response                      = array();
        $response['data']['title']     = $title;
        $response['data']['message']   = $message;
        $response['data']['payload']   = $data;
        $response['data']['timestamp'] = date( 'Y-m-d G:i:s' );
    
        return $response;
    }
}
