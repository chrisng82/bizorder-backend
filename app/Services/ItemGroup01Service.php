<?php

namespace App\Services;

use App\ItemGroup01;
use App\Repositories\ItemGroup01Repository;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;
use App\Services\Utils\SimpleImage;
use Milon\Barcode\DNS2D;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;

class ItemGroup01Service
{
    public function __construct()
    {
    }

    public function select2($search, $filters)
    {
        //DB::connection()->enableQueryLog();
        $itemGroup01s = ItemGroup01Repository::select2($search, $filters);
        //dd(DB::getQueryLog());
        return $itemGroup01s;
    }

    public function select2ByDivision($divisionID, $search, $filters)
    {
        //DB::connection()->enableQueryLog();
        $itemGroup01s = ItemGroup01Repository::select2ByDivision($divisionID, $search, $filters);
        //dd(DB::getQueryLog());
        return $itemGroup01s;
    }

    public function index($sorts, $filters = array(), $pageSize = 20)
	{
        $itemGroup01s = ItemGroup01Repository::findAll($sorts, $filters, $pageSize);
        return $itemGroup01s;
    }

    public function uploadPhoto($id, $file)
	{
		$data = array();
		$data['id'] = $id;
		$data['desc_01'] = $file->getClientOriginalName();
		$data['desc_02'] = '';
		$data['filename'] = $file->getRealPath();
		$data['blob'] = $file;

		$group = ItemGroup01Repository::savePhoto($data);

        $message = __('ItemGroup01.photo_successfully_uploaded', []);

		return array(
			'data' => $group,
			'message' => $message
		);
    }

    public function deletePhoto($id)
	{
        $group = ItemGroup01Repository::deletePhoto($id);
        $message = __('ItemGroup01.photo_successfully_deleted', []);

		return array(
			'data' => $group,
			'message' => $message
		);
    }

    public function initModel()
    {
        // AuthService::authorize(
        //     array(
        //         'item_create'
        //     )
        // );

        $model = new ItemGroup01();
        $model->code = '';
        $model->site_id = 0;
        $model->site_flow_id = 0;
        $model->ref_code_01 = '';
        $model->desc_01 = '';

        return $model;
    }

    public function showModel($itemId)
    {
        // AuthService::authorize(
        //     array(
        //         'item_read',
		// 		'item_update'
        //     )
        // );

        $model = ItemGroup01Repository::findByPk($itemId);
		if(!empty($model))
        {
            $model = self::processOutgoingModel($model);
        }

        return $model;
    }

    static public function processOutgoingModel($model)
	{
        $brand = $model->brand;
        $model->brand_code = $brand->code;

        //company id select2
        $initBrandOption = array('value'=>0,'label'=>'');
        $brand = ItemGroup01Repository::findByPk($model->brand_id);
        if(!empty($brand))
        {
            $initBrandOption = array('value'=>$brand->id, 'label'=>$brand->code);
        }
        $model->brand_id_select2 = $initBrandOption;

		return $model;
    }


}
