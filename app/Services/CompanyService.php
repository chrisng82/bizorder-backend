<?php

namespace App\Services;

use App\Company;
use App\Services\Env\ProcType;
use App\Repositories\CompanyRepository;
class CompanyService 
{
    public function __construct() 
	{
    }
    
    public function index($sorts, $filters = array(), $pageSize = 20)
	{
        // AuthService::authorize(
        //     array(
        //         'user_read'
        //     )
        // );

        $companies = CompanyRepository::findAll($sorts, $filters, $pageSize);

    	return $companies;
    }
    
    public function select2($search, $filters)
    {
        $companies = CompanyRepository::select2($search, $filters);
        return $companies;
    }

    public function select2Init($id)
    {
        $company = null;
        if($id > 0) 
        {
            $company = CompanyRepository::findByPk($id);
        }
        else
        {
            $company = CompanyRepository::findTop();
        }
        $option = array('value'=>0, 'label'=>'');
        if(!empty($company))
        {
            $option = array(
                'value'=>$company->id, 
                'label'=>$company->code.' '.$company->name_01
            );
        }
        return $option;
    }

    public function initModel()
    {
        // AuthService::authorize(
        //     array(
        //         'item_create'
        //     )
        // );

        $model = new Company();
        $model->code = '';
        $model->co_reg_no = '';
        $model->tax_reg_no = '';
        $model->name_01 = '';
        $model->name_02 = '';
        $model->unit_no = '';
        $model->building_name = '';
        $model->street_name = '';
        $model->district_01 = '';
        $model->district_02 = '';
        $model->postcode = '';
        $model->state_name = '';
        $model->country_name = '';
        $model->attention = '';
        $model->phone_01 = '';
        $model->phone_02 = '';
        $model->fax_01 = '';
        $model->fax_02 = '';
        $model->email_01 = '';
        $model->email_02 = '';
        $model->latitude = 0.00;
        $model->longitude = 0.00;

        return $model;
    }

    public function createModel($data)
    {		
        // AuthService::authorize(
        //     array(
        //         'item_create'
        //     )
        // );

        $data = self::processIncomingModel($data, true);
        $model = CompanyRepository::createModel($data);

        $message = __('Company.record_successfully_created', ['docCode'=>$model->code]);

		return array(
            'data' => $model->id,
			'message' => $message
		);
    }
    
    public function showModel($itemId)
    {
        // AuthService::authorize(
        //     array(
        //         'item_read',
		// 		'item_update'
        //     )
        // );

        $model = CompanyRepository::findByPk($itemId);
		if(!empty($model))
        {
            $model = self::processOutgoingModel($model);
        }
        
        return $model;
    }

    public function updateModel($data)
	{
        // AuthService::authorize(
        //     array(
        //         'item_update'
        //     )
        // );

        $data = self::processIncomingModel($data);

        $item = CompanyRepository::findByPk($data['id']);
        $itemData = $item->toArray();
        //assign data to model
        foreach($data as $field => $value) 
        {
            $itemData[$field] = $value;
        }

        $result = CompanyRepository::updateModel($itemData);
        $model = $result['model'];

        $model = self::processOutgoingModel($model);

        $message = __('Company.record_successfully_updated', ['code'=>$model->code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'model'=>$model
            ),
			'message' => $message
		);
    }

    static public function processIncomingModel($data)
    {
        if(array_key_exists('submit_action', $data))
        {
            unset($data['submit_action']);
        }
        return $data;
    }

    static public function processOutgoingModel($model)
	{
		return $model;
    }
}
