<?php

namespace App\Services;

use App\AdvShipHdr;
use App\Services\Env\ResType;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Services\Utils\ApiException;
use App\Repositories\DocNoRepository;
use App\Repositories\DivisionRepository;
use App\Repositories\CurrencyRepository;
use App\Repositories\BizPartnerRepository;
use App\Repositories\CreditTermRepository;
use App\Repositories\AdvShipHdrRepository;
use App\Repositories\AdvShipDtlRepository;
use App\Repositories\ItemUomRepository;
use App\Repositories\ItemRepository;
use App\Repositories\BatchJobStatusRepository;
use App\Repositories\SyncSettingHdrRepository;
use App\Repositories\SyncSettingDtlRepository;
use App\Repositories\InvTxnFlowRepository;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Auth;

class AdvShipService extends InventoryService
{
    public function __construct() 
	{
    }
    
    public function indexProcess($strProcType, $divisionId, $sorts, $filters = array(), $pageSize = 20) 
	{        
        $advShipHdrs = array();
		if(ProcType::$MAP[$strProcType] == ProcType::$MAP['ADV_SHIP_01']) 
		{
            //PO -> ADV_SHIP
			$advShipHdrs = $this->indexAdvShip01($divisionId, $sorts, $filters, $pageSize);
        }
        
        return $advShipHdrs;
    }

    protected function indexAdvShip01($divisionId, $sorts, $filters = array(), $pageSize = 20)
	{
        //PO -> ADV_SHIP
    }
    
    public function createProcess($strProcType)
    {
        
    }

    public function changeBizPartner($bizPartnerId)
    {
        AuthService::authorize(
            array(
                'adv_ship_update'
            )
        );

        $results = array();
        $bizPartner = BizPartnerRepository::findByPk($bizPartnerId);
        if(!empty($bizPartner))
        {
            $results['biz_partner_unit_no'] = $bizPartner->unit_no;
            $results['biz_partner_building_name'] = $bizPartner->building_name;
            $results['biz_partner_street_name'] = $bizPartner->street_name;
            $results['biz_partner_district_01'] = $bizPartner->district_01;
            $results['biz_partner_district_02'] = $bizPartner->district_02;
            $results['biz_partner_postcode'] = $bizPartner->postcode;
            $results['biz_partner_state_name'] = $bizPartner->state_name;
            $results['biz_partner_country_name'] = $bizPartner->country_name;
        }
        return $results;
    }

    public function changeItem($hdrId, $itemId)
    {
        AuthService::authorize(
            array(
                'adv_ship_update'
            )
        );

        $advShipHdr = AdvShipHdrRepository::findByPk($hdrId);

        $results = array();
        $item = ItemRepository::findByPk($itemId);
        if(!empty($item)
        && !empty($advShipHdr))
        {
            $results['desc_01'] = $item->desc_01;
            $results['desc_02'] = $item->desc_02;
        }
        return $results;
    }

    public function changeItemUom($hdrId, $itemId, $uomId)
    {
        AuthService::authorize(
            array(
                'adv_ship_update'
            )
        );

        $advShipHdr = AdvShipHdrRepository::findByPk($hdrId);

        $results = array();
        $itemUom = ItemUomRepository::findByItemIdAndUomId($itemId, $uomId);
        if(!empty($itemUom)
        && !empty($advShipHdr))
        {
            $results['item_id'] = $itemUom->item_id;
            $results['uom_id'] = $itemUom->uom_id;
            $results['uom_rate'] = $itemUom->uom_rate;

            $results = $this->updatePurchaseItemUom($results, $advShipHdr->doc_date, 0, $advShipHdr->currency_id, 0, 0);
        }
        return $results;
    }

    public function initHeader($divisionId)
    {
        AuthService::authorize(
            array(
                'adv_ship_create'
            )
        );    

        $user = Auth::user();

        $model = new AdvShipHdr();
        $model->doc_flows = array();
        $model->doc_status = DocStatus::$MAP['DRAFT'];
        $model->str_doc_status = DocStatus::$MAP[$model->doc_status];
        $model->doc_code = '';
        $model->ref_code_01 = '';
        $model->ref_code_02 = '';
        $model->ref_code_03 = '';
        $model->ref_code_04 = '';
        $model->ref_code_05 = '';
        $model->ref_code_06 = '';
        $model->doc_date = date('Y-m-d');
        $model->est_del_date = date('Y-m-d');
        $model->desc_01 = '';
        $model->desc_02 = '';

        $division = DivisionRepository::findByPk($divisionId);
        $model->division_id = $divisionId;
        $model->division_code = '';
        $model->site_flow_id = 0;
        $model->company_id = 0;
        $model->company_code = '';
        if(!empty($division))
        {
            $model->division_code = $division->code;
            $model->site_flow_id = $division->site_flow_id;
            $company = $division->company;
            $model->company_id = $company->id;
            $model->company_code = $company->code;
        }

        $model->doc_no_id = 0;
        $docNoIdOptions = array();
        $docNos = DocNoRepository::findAllDivisionDocNo($divisionId, \App\AdvShipHdr::class, $model->doc_date);
        for($a = 0; $a < count($docNos); $a++)
        {
            $docNo = $docNos[$a];
            if($a == 0)
            {
                $model->doc_no_id = $docNo->id;
            }
            $docNoIdOptions[] = array('value'=>$docNo->id, 'label'=>$docNo->latest_code);
        }
        $model->doc_no_id_options = $docNoIdOptions;

        //currency dropdown
        $model->currency_id = 0;
        $model->currency_rate = 1;
        $initCurrencyOption = array('value'=>0,'label'=>'');
        $model->currency_select2 = $initCurrencyOption;

        //creditTerm select2
        $initCreditTermOption = array('value'=>0,'label'=>'');
        $model->credit_term_select2 = $initCreditTermOption;

        //purchaser select2
        $initPurchaserOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($user))
        {
            $initPurchaserOption = array(
                'value'=>$user->id, 
                'label'=>$user->username
            );
        }
        $model->purchaser_select2 = $initPurchaserOption;

        //biz partner select2
        
        $model->biz_partner_unit_no = '';
        $model->biz_partner_building_name = '';
        $model->biz_partner_street_name = '';
        $model->biz_partner_district_01 = '';
        $model->biz_partner_district_02 = '';
        $model->biz_partner_postcode = '';
        $model->biz_partner_state_name = '';
        $model->biz_partner_country_name = '';
        $initBizPartnerOption = array(
            'value'=>0,
            'label'=>''
        );
        $bizPartner = BizPartnerRepository::findTop();
        if(!empty($bizPartner))
        {
            $initBizPartnerOption = array(
                'value'=>$bizPartner->id, 
                'label'=>$bizPartner->code.' '.$bizPartner->company_name_01
            );

            $model->biz_partner_unit_no = $bizPartner->unit_no;
            $model->biz_partner_building_name = $bizPartner->building_name;
            $model->biz_partner_street_name = $bizPartner->street_name;
            $model->biz_partner_district_01 = $bizPartner->district_01;
            $model->biz_partner_district_02 = $bizPartner->district_02;
            $model->biz_partner_postcode = $bizPartner->postcode;
            $model->biz_partner_state_name = $bizPartner->state_name;
            $model->biz_partner_country_name = $bizPartner->country_name;
        }
        $model->biz_partner_select2 = $initBizPartnerOption;

        $model->hdr_disc_val_01 = 0;
        $model->hdr_disc_perc_01 = 0;
        $model->hdr_disc_val_02 = 0;
        $model->hdr_disc_perc_02 = 0;
        $model->hdr_disc_val_03 = 0;
        $model->hdr_disc_perc_03 = 0;
        $model->hdr_disc_val_04 = 0;
        $model->hdr_disc_perc_04 = 0;
        $model->hdr_disc_val_05 = 0;
        $model->hdr_disc_perc_05 = 0;

        $model->disc_amt = 0;
        $model->tax_amt = 0;
        $model->round_adj_amt = 0;
        $model->net_amt = 0;

        return $model;
    }

    public function createHeader($data)
    {		
        AuthService::authorize(
            array(
                'adv_ship_create'
            )
        );

        $data = self::processIncomingHeader($data, true);
        $docNoId = $data['doc_no_id'];
        unset($data['doc_no_id']);
        $ordHdrModel = AdvShipHdrRepository::createHeader(ProcType::$MAP['NULL'], $docNoId, $data);

        $message = __('AdvShip.document_successfully_created', ['docCode'=>$ordHdrModel->doc_code]);

		return array(
			'data' => $ordHdrModel->id,
			'message' => $message
		);
    }

    public function showHeader($hdrId)
    {
        AuthService::authorize(
            array(
                'adv_ship_read',
                'adv_ship_update'
            )
        );

        $model = AdvShipHdrRepository::findByPk($hdrId);
        if(!empty($model))
        {
            $model = self::processOutgoingHeader($model);
        }

        return $model;
    }

    public function showDetails($hdrId) 
	{
        AuthService::authorize(
            array(
                'adv_ship_read',
                'adv_ship_update'
            )
        );

        $advShipDtls = AdvShipDtlRepository::findAllByHdrId($hdrId);
        $advShipDtls->load(
            'item', 'item.itemUoms', 'item.itemUoms.uom'
        );
        foreach($advShipDtls as $advShipDtl)
        {
            $advShipDtl = self::processOutgoingDetail($advShipDtl);
        }
        return $advShipDtls;
    }

    static public function processIncomingDetail($data, $item)
    {
        $data['qty'] = round($data['qty'], $item->qty_scale);

        //preprocess the select2 and dropDown
        if(array_key_exists('item_select2', $data))
        {
            $data['item_id'] = $data['item_select2']['value'];
            unset($data['item_select2']);
        }
        if(array_key_exists('uom_select2', $data))
        {
            $data['uom_id'] = $data['uom_select2']['value'];
            unset($data['uom_select2']);
        }
        if(array_key_exists('item_code', $data))
        {
            unset($data['item_code']);
        }
        if(array_key_exists('uom_code', $data))
        {
            unset($data['uom_code']);
        }
        if(array_key_exists('item', $data))
        {
            unset($data['item']);
        }
        if(array_key_exists('uom', $data))
        {
            unset($data['uom']);
        }

        if(array_key_exists('item_code', $data))
        {
            unset($data['item_code']);
		}
		if(array_key_exists('item_ref_code_01', $data))
        {
            unset($data['item_ref_code_01']);
		}
		if(array_key_exists('item_desc_01', $data))
        {
            unset($data['item_desc_01']);
		}
		if(array_key_exists('item_desc_02', $data))
        {
            unset($data['item_desc_02']);
		}
		if(array_key_exists('storage_class', $data))
        {
            unset($data['storage_class']);
		}
		if(array_key_exists('item_group_01_code', $data))
        {
            unset($data['item_group_01_code']);
		}
		if(array_key_exists('item_group_02_code', $data))
        {
            unset($data['item_group_02_code']);
		}
		if(array_key_exists('item_group_03_code', $data))
        {
            unset($data['item_group_03_code']);
		}
		if(array_key_exists('item_group_04_code', $data))
        {
            unset($data['item_group_04_code']);
		}
		if(array_key_exists('item_group_05_code', $data))
        {
            unset($data['item_group_05_code']);
		}
		if(array_key_exists('item_cases_per_pallet_length', $data))
        {
            unset($data['item_cases_per_pallet_length']);
		}
		if(array_key_exists('item_cases_per_pallet_width', $data))
        {
            unset($data['item_cases_per_pallet_width']);
		}
		if(array_key_exists('item_no_of_layers', $data))
        {
            unset($data['item_no_of_layers']);
		}

		if(array_key_exists('unit_qty', $data))
        {
            unset($data['unit_qty']);
		}
		if(array_key_exists('item_unit_uom_code', $data))
        {
            unset($data['item_unit_uom_code']);
		}
		if(array_key_exists('loose_qty', $data))
        {
            unset($data['loose_qty']);
		}
		if(array_key_exists('item_loose_uom_code', $data))
        {
            unset($data['item_loose_uom_code']);
        }
        if(array_key_exists('loose_uom_id', $data))
        {
            unset($data['loose_uom_id']);
		}
		if(array_key_exists('loose_uom_rate', $data))
        {
            unset($data['loose_uom_rate']);
		}
		if(array_key_exists('case_qty', $data))
        {
            unset($data['case_qty']);
		}
		if(array_key_exists('case_uom_rate', $data))
        {
            unset($data['case_uom_rate']);
		}
        if(array_key_exists('item_case_uom_code', $data))
        {
            unset($data['item_case_uom_code']);
        }
        if(array_key_exists('uom_code', $data))
        {
            unset($data['uom_code']);
		}
		if(array_key_exists('item_unit_barcode', $data))
        {
            unset($data['item_unit_barcode']);
		}
		if(array_key_exists('item_case_barcode', $data))
        {
            unset($data['item_case_barcode']);
		}
		if(array_key_exists('gross_weight', $data))
        {
            unset($data['gross_weight']);
		}
		if(array_key_exists('cubic_meter', $data))
        {
            unset($data['cubic_meter']);
        }

        if(array_key_exists('location_code', $data))
        {
            unset($data['location_code']);
		}
        return $data;
    }

    static public function processOutgoingDetail($model)
    {
        $item = $model->item;
        $model->item_code = $item->code;
        //$model->item_desc_01 = $item->desc_01;
        //$model->item_desc_02 = $item->desc_02;
        //item select2
        $initItemOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($item))
        {
            $initItemOption = array(
                'value'=>$item->id, 
                'label'=>$item->code.' '.$item->desc_01,
            );
        }
        $model->item_select2 = $initItemOption;

        $uom = $model->uom;
        //uom select2
        $initUomOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($uom))
        {
            $initUomOption = array('value'=>$uom->id,'label'=>$uom->code);
        }        
        $model->uom_select2 = $initUomOption;

        return $model;
    }

    static public function processIncomingHeader($data, $isClearHdrDiscTax=false)
    {
        if(array_key_exists('doc_flows', $data))
        {
            unset($data['doc_flows']);
        }
        if(array_key_exists('submit_action', $data))
        {
            unset($data['submit_action']);
        }
        //preprocess the select2 and dropDown
        if(array_key_exists('credit_term_select2', $data))
        {
            $data['credit_term_id'] = $data['credit_term_select2']['value'];
            unset($data['credit_term_select2']);
        }
        if(array_key_exists('currency_select2', $data))
        {
            $data['currency_id'] = $data['currency_select2']['value'];
            unset($data['currency_select2']);
        }   
        if(array_key_exists('purchaser_select2', $data))
        {
            $data['purchaser_id'] = $data['purchaser_select2']['value'];
            unset($data['purchaser_select2']);
        }
        if(array_key_exists('biz_partner_select2', $data))
        {
            $data['biz_partner_id'] = $data['biz_partner_select2']['value'];
            unset($data['biz_partner_select2']);
        }
        if(array_key_exists('str_doc_status', $data))
        {
            unset($data['str_doc_status']);
        }
        if(array_key_exists('division_code', $data))
        {
            unset($data['division_code']);
        }
        if(array_key_exists('company_code', $data))
        {
            unset($data['company_code']);
        }
        if(array_key_exists('biz_partner_unit_no', $data))
        {
            unset($data['biz_partner_unit_no']);
        }
        if(array_key_exists('biz_partner_building_name', $data))
        {
            unset($data['biz_partner_building_name']);
        }
        if(array_key_exists('biz_partner_street_name', $data))
        {
            unset($data['biz_partner_street_name']);
        }
        if(array_key_exists('biz_partner_district_01', $data))
        {
            unset($data['biz_partner_district_01']);
        }
        if(array_key_exists('biz_partner_district_02', $data))
        {
            unset($data['biz_partner_district_02']);
        }
        if(array_key_exists('biz_partner_postcode', $data))
        {
            unset($data['biz_partner_postcode']);
        }
        if(array_key_exists('biz_partner_state_name', $data))
        {
            unset($data['biz_partner_state_name']);
        }
        if(array_key_exists('biz_partner_country_name', $data))
        {
            unset($data['biz_partner_country_name']);
        }
        if(array_key_exists('division', $data))
        {
            unset($data['division']);
        }
        if(array_key_exists('company', $data))
        {
            unset($data['company']);
        }
        if(array_key_exists('purchaser', $data))
        {
            unset($data['purchaser']);
        }
        if(array_key_exists('biz_partner', $data))
        {
            unset($data['biz_partner']);
        }

        if($isClearHdrDiscTax)
        {  
            for($a = 1; $a <= 5; $a++)
            {
                if(array_key_exists('hdr_disc_val_0'.$a, $data))
                {
                    unset($data['hdr_disc_val_0'.$a]);
                }
                if(array_key_exists('hdr_disc_perc_0'.$a, $data))
                {
                    unset($data['hdr_disc_perc_0'.$a]);
                }
                if(array_key_exists('hdr_tax_val_0'.$a, $data))
                {
                    unset($data['hdr_tax_val_0'.$a]);
                }
                if(array_key_exists('hdr_tax_perc_0'.$a, $data))
                {
                    unset($data['hdr_tax_perc_0'.$a]);
                }
            }
        }
        return $data;
    }

    static public function processOutgoingHeader($model)
    {
        $docFlows = self::processDocFlows($model);
        $model->doc_flows = $docFlows;
        
        $division = $model->division;
        $model->division_code = $division->code;
        $company = $model->company;
        $model->company_code = $company->code;

        $model->str_doc_status = DocStatus::$MAP[$model->doc_status];

        //currency select2
        $initCurrencyOption = array('value'=>0,'label'=>'');
        $currency = CurrencyRepository::findByPk($model->currency_id);
        if(!empty($currency))
        {
            $initCurrencyOption = array('value'=>$currency->id, 'label'=>$currency->symbol);
        }
        $model->currency_select2 = $initCurrencyOption;

        //creditTerm select2
        $initCreditTermOption = array('value'=>0,'label'=>'');
        $creditTerm = CreditTermRepository::findByPk($model->credit_term_id);
        if(!empty($creditTerm))
        {
            $initCreditTermOption = array('value'=>$creditTerm->id, 'label'=>$creditTerm->code);
        }
        $model->credit_term_select2 = $initCreditTermOption;

        //purchaser select2
        $purchaser = $model->purchaser;
        $initPurchaserOption = array(
            'value'=>$purchaser->id,
            'label'=>$purchaser->username
        );
        $model->purchaser_select2 = $initPurchaserOption;

        //biz partner select2
        $bizPartner = $model->bizPartner;
        $model->biz_partner_unit_no = $bizPartner->unit_no;
        $model->biz_partner_building_name = $bizPartner->building_name;
        $model->biz_partner_street_name = $bizPartner->street_name;
        $model->biz_partner_district_01 = $bizPartner->district_01;
        $model->biz_partner_district_02 = $bizPartner->district_02;
        $model->biz_partner_postcode = $bizPartner->postcode;
        $model->biz_partner_state_name = $bizPartner->state_name;
        $model->biz_partner_country_name = $bizPartner->country_name;
        $initBizPartnerOption = array(
            'value'=>$bizPartner->id,
            'label'=>$bizPartner->code.' '.$bizPartner->company_name_01
        );
        $model->biz_partner_select2 = $initBizPartnerOption;

        $model->hdr_disc_val_01 = 0;
        $model->hdr_disc_perc_01 = 0;
        $model->hdr_disc_val_02 = 0;
        $model->hdr_disc_perc_02 = 0;
        $model->hdr_disc_val_03 = 0;
        $model->hdr_disc_perc_03 = 0;
        $model->hdr_disc_val_04 = 0;
        $model->hdr_disc_perc_04 = 0;
        $model->hdr_disc_val_05 = 0;
        $model->hdr_disc_perc_05 = 0;
        $firstDtlModel = AdvShipDtlRepository::findTopByHdrId($model->id);
        if(!empty($firstDtlModel))
        {
            $model->hdr_disc_val_01 = $firstDtlModel->hdr_disc_val_01;
            $model->hdr_disc_perc_01 = $firstDtlModel->hdr_disc_perc_01;
            $model->hdr_disc_val_02 = $firstDtlModel->hdr_disc_val_02;
            $model->hdr_disc_perc_02 = $firstDtlModel->hdr_disc_perc_02;
            $model->hdr_disc_val_03 = $firstDtlModel->hdr_disc_val_03;
            $model->hdr_disc_perc_03 = $firstDtlModel->hdr_disc_perc_03;
            $model->hdr_disc_val_04 = $firstDtlModel->hdr_disc_val_04;
            $model->hdr_disc_perc_04 = $firstDtlModel->hdr_disc_perc_04;
            $model->hdr_disc_val_05 = $firstDtlModel->hdr_disc_val_05;
            $model->hdr_disc_perc_05 = $firstDtlModel->hdr_disc_perc_05;
        }

        return $model;
    }
    
    public function createDetail($hdrId, $data)
	{
        AuthService::authorize(
            array(
                'adv_ship_update'
            )
        );

        $item = ItemRepository::findByPk($data['item_id']);
        $data = self::processIncomingDetail($data, $item);

        $advShipHdr = AdvShipHdrRepository::txnFindByPk($hdrId);
        if($advShipHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('AdvShip.doc_status_is_wip_or_complete', ['docCode'=>$advShipHdr->doc_code]));
            $exc->addData(\App\AdvShipHdr::class, $advShipHdr->id);
            throw $exc;
        }
		$advShipHdrData = $advShipHdr->toArray();
		$advShipHdr->load('inbOrdHdr', 'bizPartner');
		$bizPartner = $advShipHdr->bizPartner;

		$advShipDtlArray = array();
		$advShipDtlModels = AdvShipDtlRepository::findAllByHdrId($hdrId);
		foreach($advShipDtlModels as $advShipDtlModel)
		{
			$advShipDtlData = $advShipDtlModel->toArray();
			$advShipDtlData['is_modified'] = 0;
			$advShipDtlArray[] = $advShipDtlData;
		}
		$lastLineNo = count($advShipDtlModels);

		//assign temp id
		$tmpUuid = uniqid();
		//append NEW here to make sure it will be insert in repository later
		$data['id'] = 'NEW'.$tmpUuid;
		$data['line_no'] = $lastLineNo + 1;
        $data['is_modified'] = 1;
		$data = $this->updatePurchaseItemUom($data, $advShipHdr['doc_date'], 0, $advShipHdr['currency_id'], 0, 0);
		$advShipDtlData = $this->initAdvShipDtlData($data);
		$advShipDtlArray[] = $advShipDtlData;

		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($advShipHdrData, $advShipDtlArray);
		$advShipHdrData = $result['hdrData'];
        $advShipDtlArray = $result['dtlArray'];
        
        //need to update the associated inbOrdHdr
		$inbOrdHdrData = array();
		$inbOrdDtlArray = array();
		$inbOrdHdr = $advShipHdr->inbOrdHdr;
		if(!empty($inbOrdHdr))
		{
			if($inbOrdHdr->cur_res_type == ResType::$MAP['ADV_SHIP'])
			{
				$result = $this->tallyInbOrd($advShipHdrData, $advShipDtlArray);
				$inbOrdHdrData = $result['inbOrdHdrData'];
				$inbOrdDtlArray = $result['inbOrdDtlArray'];
			}
		}

		$result = AdvShipHdrRepository::updateDetails($advShipHdrData, $advShipDtlArray, array(), $inbOrdHdrData, $inbOrdDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('AdvShip.detail_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function deleteDetails($hdrId, $dtlArray)
	{
        AuthService::authorize(
            array(
                'adv_ship_update'
            )
        );

		//get the associated inbOrdHdr
        $advShipHdr = AdvShipHdrRepository::txnFindByPk($hdrId);
        if($advShipHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('AdvShip.doc_status_is_wip_or_complete', ['docCode'=>$advShipHdr->doc_code]));
            $exc->addData(\App\AdvShipHdr::class, $advShipHdr->id);
            throw $exc;
        }
		$advShipHdrData = $advShipHdr->toArray();
		$advShipHdr->load('inbOrdHdr', 'bizPartner');
		$bizPartner = $advShipHdr->bizPartner;

		//query the advShipDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$delAdvShipDtlArray = array();
		$advShipDtlArray = array();
        $advShipDtlModels = AdvShipDtlRepository::findAllByHdrId($hdrId);
        $lineNo = 1;
		foreach($advShipDtlModels as $advShipDtlModel)
		{
			$advShipDtlData = $advShipDtlModel->toArray();
			$advShipDtlData['is_modified'] = 0;
			$advShipDtlData['is_deleted'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{
				if($dtlData['id'] == $advShipDtlData['id'])
				{
					//this is deleted dtl
					$advShipDtlData['is_deleted'] = 1;
					break;
				}
			}
			if($advShipDtlData['is_deleted'] > 0)
			{
				$delAdvShipDtlArray[] = $advShipDtlData;
			}
			else
			{
                $advShipDtlData['line_no'] = $lineNo;
                $advShipDtlData['is_modified'] = 1;
                $advShipDtlArray[] = $advShipDtlData;
                $lineNo++;
			}
        }
        
		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($advShipHdrData, $advShipDtlArray, $delAdvShipDtlArray);
		$advShipHdrData = $result['hdrData'];
		$modifiedDtlArray = $result['dtlArray'];
		foreach($advShipDtlArray as $advShipDtlSeq => $advShipDtlData)
		{
			foreach($modifiedDtlArray as $modifiedDtlData)
			{
				if($modifiedDtlData['id'] == $advShipDtlData['id'])
				{
					foreach($modifiedDtlData as $field => $value)
					{
						$advShipDtlData[$field] = $value;
					}
					$advShipDtlData['is_modified'] = 1;
					$advShipDtlArray[$advShipDtlSeq] = $advShipDtlData;
					break;
				}
			}
		}

		//need to update the associated inbOrdHdr
		$inbOrdHdrData = array();
		$inbOrdDtlArray = array();
		$delInbOrdDtlArray = array();
		$inbOrdHdr = $advShipHdr->inbOrdHdr;
		if(!empty($inbOrdHdr))
		{
			if($inbOrdHdr->cur_res_type == ResType::$MAP['ADV_SHIP'])
			{
				$result = $this->tallyInbOrd($advShipHdrData, $advShipDtlArray, $delAdvShipDtlArray);
				$inbOrdHdrData = $result['inbOrdHdrData'];
				$inbOrdDtlArray = $result['inbOrdDtlArray'];
				$delInbOrdDtlArray = $result['delInbOrdDtlArray'];
			}
		}
		
		$result = AdvShipHdrRepository::updateDetails($advShipHdrData, $advShipDtlArray, $delAdvShipDtlArray, $inbOrdHdrData, $inbOrdDtlArray, $delInbOrdDtlArray);
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('AdvShip.details_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>$delAdvShipDtlArray
            ),
			'message' => $message
		);
    }
    
    public function updateDetails($hdrId, $dtlArray)
	{
        AuthService::authorize(
            array(
                'adv_ship_update'
            )
        );

		//get the associated inbOrdHdr
        $advShipHdr = AdvShipHdrRepository::txnFindByPk($hdrId);
        if($advShipHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('AdvShip.doc_status_is_wip_or_complete', ['docCode'=>$advShipHdr->doc_code]));
            $exc->addData(\App\AdvShipHdr::class, $advShipHdr->id);
            throw $exc;
        }
		$advShipHdrData = $advShipHdr->toArray();
		$advShipHdr->load('inbOrdHdr', 'bizPartner');
		$bizPartner = $advShipHdr->bizPartner;

		//query the advShipDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$advShipDtlArray = array();
        $advShipDtlModels = AdvShipDtlRepository::findAllByHdrId($hdrId);
		foreach($advShipDtlModels as $advShipDtlModel)
		{
			$advShipDtlData = $advShipDtlModel->toArray();
			$advShipDtlData['is_modified'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{                
				if($dtlData['id'] == $advShipDtlData['id'])
				{
                    $item = ItemRepository::findByPk($dtlData['item_id']);
                    $dtlData = self::processIncomingDetail($dtlData, $item);
					$dtlData = $this->updatePurchaseItemUom($dtlData, $advShipHdr->doc_date, 0, $advShipHdr->currency_id, 0, 0);
					foreach($dtlData as $fieldName => $value)
					{
						$advShipDtlData[$fieldName] = $value;
					}
					$advShipDtlData['is_modified'] = 1;

					break;
				}
			}
			$advShipDtlArray[] = $advShipDtlData;
		}
		
		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($advShipHdrData, $advShipDtlArray);
		$advShipHdrData = $result['hdrData'];
		$advShipDtlArray = $result['dtlArray'];

		//need to update the associated inbOrdHdr
		$inbOrdHdrData = array();
		$inbOrdDtlArray = array();
		$inbOrdHdr = $advShipHdr->inbOrdHdr;
		if(!empty($inbOrdHdr))
		{
			if($inbOrdHdr->cur_res_type == ResType::$MAP['ADV_SHIP'])
			{
				$result = $this->tallyInbOrd($advShipHdrData, $advShipDtlArray);
				$inbOrdHdrData = $result['inbOrdHdrData'];
				$inbOrdDtlArray = $result['inbOrdDtlArray'];
			}
		}
		
		$result = AdvShipHdrRepository::updateDetails($advShipHdrData, $advShipDtlArray, array(), $inbOrdHdrData, $inbOrdDtlArray, array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('AdvShip.details_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }
    
    public function updateHeader($hdrData)
	{
        AuthService::authorize(
            array(
                'adv_ship_update'
            )
        );
        
        $hdrData = self::processIncomingHeader($hdrData);

		//get the associated inbOrdHdr
        $advShipHdr = AdvShipHdrRepository::txnFindByPk($hdrData['id']);
        if($advShipHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('AdvShip.doc_status_is_wip_or_complete', ['docCode'=>$advShipHdr->doc_code]));
            $exc->addData(\App\AdvShipHdr::class, $advShipHdr->id);
            throw $exc;
        }
        $advShipHdrData = $advShipHdr->toArray();
        //assign hdrData to model
        foreach($hdrData as $field => $value) 
        {
            if(strpos($field, 'hdr_disc_val') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_disc_perc') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_tax_val') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_tax_perc') !== false)
            {
                continue;
            }
            $advShipHdrData[$field] = $value;
        }

		//$advShipHdr->load('inbOrdHdr', 'bizPartner');
        //$bizPartner = $advShipHdr->bizPartner;
        
        $advShipDtlArray = array();

        $inbOrdHdrData = array();
        $inbOrdDtlArray = array();

        //check if hdr disc or hdr tax is set and more than 0
        $needToProcessDetails = false;
        $firstDtlModel = AdvShipDtlRepository::findTopByHdrId($hdrData['id']);
        if(!empty($firstDtlModel))
        {
            $firstDtlData = $firstDtlModel->toArray();
            for($a = 1; $a <= 5; $a++)
            {
                if(array_key_exists('hdr_disc_val_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_disc_val_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_disc_val_0'.$a], $firstDtlData['hdr_disc_val_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
                if(array_key_exists('hdr_disc_perc_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_disc_perc_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_disc_perc_0'.$a], $firstDtlData['hdr_disc_perc_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }

                if(array_key_exists('hdr_tax_val_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_tax_val_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_tax_val_0'.$a], $firstDtlData['hdr_tax_val_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
                if(array_key_exists('hdr_tax_perc_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_tax_perc_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_tax_perc_0'.$a], $firstDtlData['hdr_tax_perc_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
            }
        }

        if($needToProcessDetails)
        {
            //query the advShipDtlModels from DB
            //format to array, with is_modified field to indicate it is changed
            $advShipDtlModels = AdvShipDtlRepository::findAllByHdrId($hdrData['id']);
            foreach($advShipDtlModels as $advShipDtlModel)
            {
                $advShipDtlData = $advShipDtlModel->toArray();
                for($a = 1; $a <= 5; $a++)
                {
                    if(array_key_exists('hdr_disc_val_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_disc_val_0'.$a], 0, 5) > 0)
                    {
                        $advShipDtlData['hdr_disc_val_0'.$a] = $hdrData['hdr_disc_val_0'.$a];
                    }
                    if(array_key_exists('hdr_disc_perc_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_disc_perc_0'.$a], 0, 5) > 0)
                    {
                        $advShipDtlData['hdr_disc_perc_0'.$a] = $hdrData['hdr_disc_perc_0'.$a];
                    }

                    if(array_key_exists('hdr_tax_val_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_tax_val_0'.$a], 0, 5) > 0)
                    {
                        $advShipDtlData['hdr_tax_val_0'.$a] = $hdrData['hdr_tax_val_0'.$a];
                    }
                    if(array_key_exists('hdr_tax_perc_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_tax_perc_0'.$a], 0, 5) > 0)
                    {
                        $advShipDtlData['hdr_tax_perc_0'.$a] = $hdrData['hdr_tax_perc_0'.$a];
                    }
                }
                $advShipDtlData['is_modified'] = 1;
                $advShipDtlArray[] = $advShipDtlData;
            }
            
            //calculate details amount, use adaptive rounding tax
            $result = $this->calculateAmount($advShipHdrData, $advShipDtlArray);
            $advShipHdrData = $result['hdrData'];
            $advShipDtlArray = $result['dtlArray'];

            //need to update the associated inbOrdHdr
            $inbOrdHdr = $advShipHdr->inbOrdHdr;
            if(!empty($inbOrdHdr))
            {
                if($inbOrdHdr->cur_res_type == ResType::$MAP['ADV_SHIP'])
                {
                    $result = $this->tallyInbOrd($advShipHdrData, $advShipDtlArray);
                    $inbOrdHdrData = $result['inbOrdHdrData'];
                    $inbOrdDtlArray = $result['inbOrdDtlArray'];
                }
            }
        }
            
        $result = AdvShipHdrRepository::updateDetails($advShipHdrData, $advShipDtlArray, array(), $inbOrdHdrData, $inbOrdDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];

        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('AdvShip.document_successfully_updated', ['docCode'=>$documentHeader->doc_code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }
    
    public function transitionToStatus($hdrId, $strDocStatus)
    {
        $hdrModel = null;
        if(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['WIP'])
        {
            $hdrModel = self::transitionToWip($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['COMPLETE'])
        {
            $hdrModel = self::transitionToComplete($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['DRAFT'])
        {
            $hdrModel = self::transitionToDraft($hdrId);
        }
        
        $documentHeader = self::processOutgoingHeader($hdrModel);

        $message = __('AdvShip.document_successfully_transition', ['docCode'=>$documentHeader->doc_code,'strDocStatus'=>$strDocStatus]);
        
        return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>array(),
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }
    
    static public function transitionToWip($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = AdvShipHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('AdvShip.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\AdvShipHdr::class, $hdrModel->id);
            throw $exc;
        }
        
		//commit the document
		$hdrModel = AdvShipHdrRepository::commitToWip($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

		$exc = new ApiException(__('AdvShip.commit_to_wip_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\AdvShipHdr::class, $hdrModel->id);
        throw $exc;
	}

	static public function transitionToComplete($hdrId)
    {
        AuthService::authorize(
            array(
                'adv_ship_confirm'
            )
        );

		//use transaction to make sure this is latest value
		$hdrModel = AdvShipHdrRepository::txnFindByPk($hdrId);

		//only DRAFT or above can transition to COMPLETE
		if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE']
		|| $hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('AdvShip.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\AdvShipHdr::class, $hdrModel->id);
            throw $exc;
		}

		$dtlModels = AdvShipDtlRepository::findAllByHdrId($hdrModel->id);
		//preliminary checking
		foreach($dtlModels as $dtlModel)
		{
			if($dtlModel->item_id == 0)
			{
				$exc = new ApiException(__('AdvShip.item_is_required', ['lineNo'=>$dtlModel->line_no]));
				$exc->addData(\App\AdvShipDtl::class, $dtlModel->id);
				throw $exc;
			}
		}

		//commit the document
		$hdrModel = AdvShipHdrRepository::commitToComplete($hdrModel->id, false);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		
		$exc = new ApiException(__('AdvShip.commit_to_complete_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\AdvShipHdr::class, $hdrModel->id);
        throw $exc;
	}

	static public function transitionToDraft($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = AdvShipHdrRepository::txnFindByPk($hdrId);

		//only WIP or COMPLETE can transition to DRAFT
		if($hdrModel->doc_status == DocStatus::$MAP['WIP'])
		{
            $hdrModel = AdvShipHdrRepository::revertWipToDraft($hdrModel->id);
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		elseif($hdrModel->doc_status == DocStatus::$MAP['COMPLETE'])
		{
            AuthService::authorize(
                array(
                    'adv_ship_revert'
                )
            );

            $hdrModel = AdvShipHdrRepository::revertCompleteToDraft($hdrModel->id);
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

		$exc = new ApiException(__('AdvShip.doc_status_is_not_wip_or_complete', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\AdvShipHdr::class, $hdrModel->id);
		throw $exc;
    }
    
    public function syncProcess($strProcType, $divisionId)
    {
        $user = Auth::user();

        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['ADV_SHIP_SYNC_01']) 
			{
				//sync adv ship from EfiChain
				return $this->syncAdvShipSync01($divisionId, $user->id);
            }
		}
    }

    protected function syncAdvShipSync01($divisionId, $userId)
    {
        AuthService::authorize(
            array(
                'adv_ship_import'
            )
        );

        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['ADV_SHIP_SYNC_01'], $userId);

        $syncSettingHdr = SyncSettingHdrRepository::findByProcTypeAndDivisionId(ProcType::$MAP['SYNC_EFICHAIN_01'], $divisionId);
        if(empty($syncSettingHdr))
        {
            $exc = new ApiException(__('AdvShip.sync_setting_not_found', ['divisionId'=>$divisionId]));
            //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
            throw $exc;
        }

        $division = DivisionRepository::findByPk($divisionId);
        $company = $division->company;
        $divUrl = '&divisions[]='.$division->code;

        $client = new Client();
        $header = array();

        $page = 1;
        $lastPage = 1;
        $pageSize = 1000;
        $total = 0;

        $usernameSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'username');
        $passwordSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'password');
        $formParams = array(
            'UserLogin[username]' => $usernameSetting->value,
            'UserLogin[password]' => $passwordSetting->value
        );
        $pageSizeSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'page_size');
        if(!empty($pageSizeSetting))
        {
            $pageSize = $pageSizeSetting->value;
        }

        while($page <= $lastPage)
        {
            $url = $syncSettingHdr->url.'/index.php?r=luuWu/getPurchaseInvoices&page_size='.$pageSize.'&page='.$page.$divUrl;
            
            $response = $client->post($url, array('headers' => $header, 'form_params' => $formParams));
            $result = $response->getBody()->getContents();
            $result = json_decode($result, true);
            if(empty($result))
            {
                $exc = new ApiException(__('Item.fail_to_sync', ['url'=>$url]));
                //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
                throw $exc;
            }

            $total = $result['total'];
            for ($a = 0; $a < count($result['data']); $a++)
            {
                $grPiHdrData = $result['data'][$a];
                //DRAFT
                $advShipHdrModel = AdvShipHdrRepository::syncAdvShipSync01($company, $division, $grPiHdrData);
                $advShipDtlModels = AdvShipDtlRepository::findAllByHdrId($advShipHdrModel->id);

                if($advShipHdrModel->doc_status < DocStatus::$MAP['COMPLETE']
                && count($advShipDtlModels) > 0)
                {
                    $invTxnFlow = InvTxnFlowRepository::findByPk($advShipHdrModel->division_id, $advShipHdrModel->proc_type);

                    if($invTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
                    {
                        $advShipHdrModel = self::transitionToWip($advShipHdrModel->id);
                    }
                    elseif($invTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
                    {
                        $advShipHdrModel = self::transitionToComplete($advShipHdrModel->id);
                    }
                }
                
                $statusNumber = 100;
                if($total > 0)
                {
                    $statusNumber = bcmul(bcdiv(($a + ($page - 1) * $pageSize), $total, 8), 100, 2);
                }
                BatchJobStatusRepository::updateStatusNumber($batchJobStatusModel->id, $statusNumber);
            }

            $lastPage = $total % $pageSize == 0 ? ($total / $pageSize) : ($total / $pageSize) + 1;

            $page++;
        }

        $batchJobStatusModel = BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

        return $batchJobStatusModel;
    }

    public function changeCurrency($currencyId)
    {
        AuthService::authorize(
            array(
                'adv_ship_update'
            )
        );

        $results = array();
        $currency = CurrencyRepository::findByPk($currencyId);
        if(!empty($currency))
        {
            $results['currency_rate'] = $currency->currency_rate;
        }
        return $results;
    }

    public function index($divisionId, $sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
            array(
                'adv_ship_read'
            )
        );

        //DB::connection()->enableQueryLog();
        $advShipHdrs = AdvShipHdrRepository::findAll($divisionId, $sorts, $filters, $pageSize);
        $advShipHdrs->load(
            'advShipDtls'
            //'advShipDtls.item', 'advShipDtls.item.itemUoms', 'advShipDtls.item.itemUoms.uom', 
        );
		foreach($advShipHdrs as $advShipHdr)
		{
			$advShipHdr->str_doc_status = DocStatus::$MAP[$advShipHdr->doc_status];

			$bizPartner = $advShipHdr->bizPartner;
			$advShipHdr->biz_partner_code = $bizPartner->code;
			$advShipHdr->biz_partner_company_name_01 = $bizPartner->company_name_01;
			$advShipHdr->biz_partner_area_code = $bizPartner->area_code;
			$area = $bizPartner->area;
			if(!empty($area))
			{
				$advShipHdr->biz_partner_area_desc_01 = $area->desc_01;
			}

			$purchaser = $advShipHdr->purchaser;
			$advShipHdr->purchaser_username = $purchaser->username;

			$advShipDtls = $advShipHdr->advShipDtls;
			$advShipHdr->details = $advShipDtls;
        }
        //Log::error(DB::getQueryLog());
    	return $advShipHdrs;
	}
}
