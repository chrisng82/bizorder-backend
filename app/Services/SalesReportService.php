<?php

namespace App\Services;

use App\Services\Env\DocStatus;
use App\Services\Env\ResStatus;
use App\Services\Env\ResType;
use App\Services\Env\PhysicalCountStatus;
use App\Repositories\SalesReportRepository;
use App\Repositories\LocationRepository;
use App\Repositories\SiteFlowRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SalesReportService 
{
    public function __construct() 
	{
    }
    
    public function outbOrdAnalysis($siteFlowId, $sorts, $criteria = array(), $columns = array(), $pageSize = 20) 
	{
        AuthService::authorize(
			array(
				'outb_ord_analysis_report'
			)
        );
        
        //DB::connection()->enableQueryLog();
        $outbOrdDtls = SalesReportRepository::outbOrdAnalysis($siteFlowId, $sorts, $criteria, $columns, $pageSize);
        //Log::error(DB::getQueryLog());

        //$outbOrdDtls->load('item');
		foreach($outbOrdDtls as $outbOrdDtl)
		{
            $outbOrdDtl->doc_status = DocStatus::$MAP[$outbOrdDtl->doc_status];
        }
        return $outbOrdDtls;
    }

    public function initOutbOrdAnalysis($siteFlowId)
    {
        AuthService::authorize(
			array(
				'outb_ord_analysis_report'
			)
        );

        $user = Auth::user();

        $siteFlow = SiteFlowRepository::findByPk($siteFlowId);

        $criteria = array();
        $criteria['columns'] = array(
            'source'=>array(                                
                array('column'=>'sls_inv_hdr_code'),
                array('column'=>'sls_inv_doc_date'), 
                array('column'=>'delivery_point_company_name_02'),
                array('column'=>'delivery_point_unit_no'),
                array('column'=>'delivery_point_building_name'),
                array('column'=>'delivery_point_street_name'),
                array('column'=>'delivery_point_district_01'),
                array('column'=>'delivery_point_district_02'),
                array('column'=>'delivery_point_postcode'),
                array('column'=>'delivery_point_state_name'),
                array('column'=>'delivery_point_country_name'),
                array('column'=>'delivery_point_attention'),
                array('column'=>'delivery_point_phone_01'),
                array('column'=>'delivery_point_phone_02'),
                array('column'=>'delivery_point_fax_01'),
                array('column'=>'delivery_point_fax_02'),
          
                array('column'=>'ref_code_01'),
                array('column'=>'item_code'),
                array('column'=>'item_desc_01'),
                array('column'=>'item_desc_02'),
            ),

            'target'=>array(
                array('column'=>'division_code'),
                array('column'=>'delivery_point_code'),
                array('column'=>'delivery_point_area_code'),
                array('column'=>'delivery_point_area_desc_01'),                
                array('column'=>'delivery_point_company_name_01'),
                array('column'=>'sls_ord_hdr_code'),
                array('column'=>'sls_ord_doc_date'),
                array('column'=>'doc_code'),
                array('column'=>'doc_status'),
                array('column'=>'doc_date'),
                array('column'=>'hdr_desc_01'),
                array('column'=>'pick_list_hdr_code'),
                array('column'=>'pick_list_doc_date'),
                array('column'=>'net_amt'),
                array('column'=>'case_qty'),
                array('column'=>'gross_weight'),
                array('column'=>'cubic_meter'),
                array('column'=>'hdr_created_at')
            ),
        );
        $criteria['start_date'] = date('Y-m-d');
        $criteria['end_date'] = date('Y-m-d');
        $criteria['division_ids_select2'] = array();
        $criteria['delivery_point_ids_select2'] = array();
        $criteria['item_ids_select2'] = array();
        $criteria['item_group_01_ids_select2'] = array();
        $criteria['item_group_02_ids_select2'] = array();
        $criteria['item_group_03_ids_select2'] = array();

        return $criteria;
    }
}
