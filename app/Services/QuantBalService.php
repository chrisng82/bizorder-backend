<?php

namespace App\Services;

use App\Repositories\QuantBalRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class QuantBalService 
{
	public function __construct() 
	{
    }

    public function verifyTxn() 
	{
        $invalidRows = QuantBalRepository::verifyTxn();        
        return $invalidRows;
    }

    public function select2($search, $filters)
    {
        //DB::connection()->enableQueryLog();
        $quantBals = QuantBalRepository::select2($search, $filters);
        //Log::error(DB::getQueryLog());
        $quantBals->load('item', 'itemBatch', 'handlingUnit');
        foreach($quantBals as $quantBal)
        {
            $item = $quantBal->item;
			$itemBatch = $quantBal->itemBatch;
            $handlingUnit = $quantBal->handlingUnit;
            
            $quantBal = ItemService::processCaseLoose($quantBal, $item, 1);

            $quantBal->batch_serial_no = empty($itemBatch) ? '' : $itemBatch->batch_serial_no;
            $quantBal->expiry_date = empty($itemBatch) ? '' : $itemBatch->expiry_date;
            $quantBal->receipt_date = empty($itemBatch) ? '' : $itemBatch->receipt_date;
            $quantBal->handling_unit_barcode = empty($handlingUnit) ? '' : $handlingUnit->getBarcode();

            unset($quantBal->item);
			unset($quantBal->itemBatch);
			unset($quantBal->handlingUnit);
        }
        return $quantBals;
    }

    public function select2Init($id, $filters)
    {
        $quantBal = null;
        if($id > 0) 
        {
            $quantBal = QuantBalRepository::findByPk($id);
        }
        else
        {
            $quantBal = QuantBalRepository::findTop($filters);
        }
        $option = array('value'=>0, 'label'=>'');
        if(!empty($quantBal))
        {
            $handlingUnit = $quantBal->handlingUnit;
            $item = $quantBal->item;
            $itemBatch = $quantBal->itemBatch;
            //item code | expiry date | handling unit
            $option = array(
                'value'=>$quantBal->id, 
                'label'=>$item->code.' '.$quantBal->balance_unit_qty,
                'item_code'=>$item->code,
                'batch_serial_no'=> empty($itemBatch) ? $itemBatch->batch_serial_no : '',
                'expiry_date'=> empty($itemBatch) ? $itemBatch->expiry_date : '',
                'receipt_date'=> empty($itemBatch) ? $itemBatch->receipt_date : '',
                'handling_unit'=> empty($handlingUnit) ? $handlingUnit->getBarcode() : '',
            );
        }
        return $option;
    }
}