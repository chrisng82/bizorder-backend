<?php

namespace App\Services;

use App\ItemUom;
use App\Item;
use App\Uom;
use App\Division;

use App\Services\Env\ProcType;
use App\Services\Env\ResStatus;
use App\Services\Env\ScanMode;
use App\Services\Env\RetrievalMethod;
use App\Services\Env\ItemType;
use App\Services\Env\StorageClass;
use App\Services\Env\PromotionRuleType;
use App\Repositories\BatchJobStatusRepository;
use App\Repositories\ItemRepository;
use App\Repositories\UserDivisionRepository;
use App\Repositories\ItemDivisionRepository;
use App\Repositories\ItemUomRepository;
use App\Repositories\ItemSalePriceRepository;
use App\Repositories\ItemPhotoRepository;
use App\Repositories\SyncSettingHdrRepository;
use App\Repositories\SyncSettingDtlRepository;
use App\Repositories\SiteFlowRepository;
use App\Repositories\PromotionRepository;
use App\Repositories\ItemGroup01Repository;
use App\Repositories\ItemGroup02Repository;
use App\Repositories\ItemGroup03Repository;
use App\BatchJobStatus;
use App\Imports\ItemExcel01Import;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;
use App\Services\Utils\SimpleImage;
use Milon\Barcode\DNS2D;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Libs\Promotion\DebtorChecker;
use App\Libs\Promotion\DebtorGroupChecker;
use App\Repositories\DashboardRepository;

class DashboardService
{
    public function __construct()
    {
    }

    public static function weeklyReport($week)
    {
        $items = DashboardRepository::getWeek($week);

        return $items;
    }

    public static function monthlyReport($month)
    {
        $items = DashboardRepository::getMonth($month);

        return $items;
    }

    public static function yearlyReport($year)
    {
        $items = DashboardRepository::getYear($year);

        return $items;
    }

    public static function summaryReport()
    {
        $items = DashboardRepository::summaryReport();

        return $items;
    }

    public static function createModel($setting)
    {
        $settings = DashboardRepository::createModel($setting);

        return $settings;
    }

    public static function updateModel($setting)
    {
        $settings = DashboardRepository::updateModel($setting);

        return $settings;
    }

    public static function retrieveModel($id)
    {
        $settings = DashboardRepository::retrieveModel($id);

        return $settings;
    }

    public static function initId($userId)
    {
        $settings = DashboardRepository::initId($userId);

        return $settings;
    }
}
