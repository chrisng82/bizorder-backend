<?php

namespace App\Services;

use ErrorException;
use App\CountAdjHdr;
use App\Services\Env\ResStatus;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Services\Env\WhseJobType;
use App\Services\Env\HandlingType;
use App\Services\Env\PhysicalCountStatus;
use App\Services\Utils\ApiException;
use App\Repositories\SiteDocNoRepository;
use App\Repositories\WhseTxnFlowRepository;
use App\Repositories\CycleCountHdrRepository;
use App\Repositories\CycleCountDtlRepository;
use App\Repositories\CountAdjHdrRepository;
use App\Repositories\CountAdjDtlRepository;
use App\Repositories\StorageBinRepository;
use App\Repositories\QuantBalRepository;
use App\Repositories\SiteFlowRepository;
use App\Repositories\StorageTypeRepository;
use App\Repositories\ItemBatchRepository;
use App\Repositories\ItemUomRepository;
use App\Repositories\UomRepository;
use App\Repositories\DocNoRepository;
use App\Repositories\ItemRepository;
use App\Repositories\BatchJobStatusRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\CountAdjDetailsImport;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class CountAdjService extends InventoryService
{
    public function __construct() 
	{
    }
    
    public function indexProcess($strProcType, $siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['COUNT_ADJ_01']) 
            {
                //CYCLE_COUNT -> COUNT_ADJ
                $storageBins = $this->indexCountAdj01($siteFlowId, $sorts, $filters = array(), $pageSize = 20);
                return $storageBins;
            }
		}

        return array();
    }

    protected function indexCountAdj01($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
            array(
                'cycle_count_read'
            )
        );

        $qStatuses = array(DocStatus::$MAP['COMPLETE']);
        $cycleCountHdrs = CycleCountHdrRepository::findAllNotExistCountAdj01Txn($siteFlowId, $qStatuses, $sorts, $filters, $pageSize);
        $cycleCountHdrs->load(
            'cycleCountDtls', 
            'cycleCountDtls.item', 'cycleCountDtls.item.itemUoms', 'cycleCountDtls.item.itemUoms.uom', 
            'cycleCountDtls.company', 'cycleCountDtls.storageBin'
        );
        foreach($cycleCountHdrs as $cycleCountHdr)
		{
            $ttlVariancePerc = 0;
            $ttlAccuracy = 0;

            $cycleCountHdr->str_doc_status = DocStatus::$MAP[$cycleCountHdr->doc_status];
            
            $jobCycleCountDtls = array();
            $cycleCountDtls = $cycleCountHdr->cycleCountDtls;
			//query quantBal, and calculate variance
			foreach($cycleCountDtls as $cycleCountDtl)
			{
                $item = $cycleCountDtl->item;
                $storageBin = $cycleCountDtl->storageBin;
                $storageRow = null;
				if(!empty($storageBin))
				{
					$storageRow = $storageBin->storageRow;
				}

                $cycleCountDtl->item_code = '';
				$cycleCountDtl->item_ref_code_01 = '';
				$cycleCountDtl->item_desc_01 = '';
				$cycleCountDtl->item_desc_02 = '';
				$cycleCountDtl->storage_class = '';
				$cycleCountDtl->item_group_01_code = '';
				$cycleCountDtl->item_group_02_code = '';
				$cycleCountDtl->item_group_03_code = '';
				$cycleCountDtl->item_group_04_code = '';
                $cycleCountDtl->item_group_05_code = '';
                if(!empty($item))
                {
                    $cycleCountDtl->item_code = $item->code;
                    $cycleCountDtl->item_ref_code_01 = $item->ref_code_01;
                    $cycleCountDtl->item_desc_01 = $item->desc_01;
                    $cycleCountDtl->item_desc_02 = $item->desc_02;
                    $cycleCountDtl->storage_class = $item->storage_class;
                    $cycleCountDtl->item_group_01_code = $item->item_group_01_code;
                    $cycleCountDtl->item_group_02_code = $item->item_group_02_code;
                    $cycleCountDtl->item_group_03_code = $item->item_group_03_code;
                    $cycleCountDtl->item_group_04_code = $item->item_group_04_code;
                    $cycleCountDtl->item_group_05_code = $item->item_group_05_code;
                }

                $company = $cycleCountDtl->company;
                if(!empty($company))
                {
                    $cycleCountDtl->company_code = $company->code;
                }

                $cycleCountDtl->storage_bin_code = '';
				if(!empty($storageBin))
				{
					$cycleCountDtl->storage_bin_code = $storageBin->code;
				}
				$cycleCountDtl->storage_row_code = '';
				if(!empty($storageRow))
				{
					$cycleCountDtl->storage_row_code = $storageRow->code;
				}
                
                /*
                $cycleCountDtl->is_recount = false;
                $toCycleCountDtl = CycleCountDtlRepository::findByFrCycleCountDtlId($cycleCountDtl->id);
                if(!empty($toCycleCountDtl))
                {
                    $cycleCountDtl->is_recount = true;
                }
                */

                $cycleCountDtl->count_unit_qty = 0;
                if(!empty($item))
                {
                    $cycleCountDtl->count_unit_qty = bcmul($cycleCountDtl->uom_rate, $cycleCountDtl->qty, $item->qty_scale);
                }

                $cycleCountDtl->balance_unit_qty = 0;
                /*
				$quantBal = QuantBalRepository::queryByAttributes(
                    $cycleCountDtl->storage_bin_id, $cycleCountDtl->handling_unit_id,
                    $cycleCountDtl->company_id, $cycleCountDtl->item_id, $cycleCountDtl->item_batch_id,
                    $cycleCountDtl->batch_serial_no, $cycleCountDtl->expiry_date, $cycleCountDtl->receipt_date);
                if(!empty($quantBal))
                {
                    $cycleCountDtl->balance_unit_qty = $quantBal->balance_unit_qty;
                }
                */

                $cycleCountDtl->variance_qty = 0;
                if(!empty($item))
                {
                    $cycleCountDtl->variance_qty = bcsub($cycleCountDtl->count_unit_qty, $cycleCountDtl->balance_unit_qty, $item->qty_scale);
                }
                $cycleCountDtl->abs_variance_qty = abs($cycleCountDtl->variance_qty);
                $cycleCountDtl->variance_perc = 0;
                $cycleCountDtl->accuracy = 100;
                if(bccomp($cycleCountDtl->balance_unit_qty, 0, 5) > 0)
                {
                    $cycleCountDtl->variance_perc = bcmul(bcdiv($cycleCountDtl->abs_variance_qty, $cycleCountDtl->balance_unit_qty, 10), 100, 10);
                    $cycleCountDtl->accuracy = bcmul(bcdiv($cycleCountDtl->count_unit_qty, $cycleCountDtl->balance_unit_qty, 10), 100, 10);
                }

                $ttlVariancePerc = bcadd($ttlVariancePerc, $cycleCountDtl->variance_perc, 10);
                $ttlAccuracy = bcadd($ttlAccuracy, $cycleCountDtl->accuracy, 10);

                unset($cycleCountDtl->item);
                unset($cycleCountDtl->storageBin);
                unset($cycleCountDtl->company);

                $jobCycleCountDtls[] = $cycleCountDtl;
            }
            
            $cycleCountHdr->avg_variance_perc = bcdiv($ttlVariancePerc, count($cycleCountDtls), 10);
            $cycleCountHdr->avg_accuracy = bcdiv($ttlAccuracy, count($cycleCountDtls), 10);

            $cycleCountHdr->details = $cycleCountDtls;

			//unset the item, so it wont send in json
		}
        return $cycleCountHdrs;
    }
    
    public function createProcess($strProcType, $siteFlowId, $hdrIds)
    {
        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['COUNT_ADJ_01']) 
            {
                //CYCLE_COUNT -> COUNT_ADJ
                $result = $this->createCountAdj01(ProcType::$MAP[$strProcType], $siteFlowId, $hdrIds);
                return $result;
            }
		}
    }

    protected function createCountAdj01($procType, $siteFlowId, $hdrIds)
    {
        AuthService::authorize(
            array(
                'count_adj_create'
            )
        );

        $countAdjHdrs = array();

        $siteFlow = SiteFlowRepository::findByPk($siteFlowId);

        //1 cycleCount to N countAdj, based on dtl company_id
        $cycleCountHdrs = CycleCountHdrRepository::findAllByHdrIds($hdrIds);
        foreach($cycleCountHdrs as $cycleCountHdr)
        {            
            $hdrData = array();
            $dtlDataList = array();
            $docTxnFlowDataList = array();
            $oriCycleCountDtlDataList = array();

            $lineNo = 1;
            
            $dbCycleCountDtls = CycleCountDtlRepository::findAllByHdrId($cycleCountHdr->id);
            $dbCycleCountDtls->load('item', 'cycleCountHdr', 'storageBin');

            //1) group cycleCountDtl by companyId
            $cycleCountDtlsHashByCompanyId = array();
            foreach($dbCycleCountDtls as $cycleCountDtl)
            {
                if($cycleCountDtl->physical_count_status == PhysicalCountStatus::$MAP['COUNTING'])
                {
                    $exc = new ApiException(__('CountAdj.physical_count_status_is_counting', ['lineNo'=>$cycleCountDtl->line_no]));
                    //$exc->addData(\App\CycleCountDtl::class, $cycleCountDtl->id);
                    throw $exc;
                }
                
                if($cycleCountDtl->physical_count_status != PhysicalCountStatus::$MAP['NULL']
                && $cycleCountDtl->physical_count_status != PhysicalCountStatus::$MAP['CONFIRMED']
                && $cycleCountDtl->physical_count_status != PhysicalCountStatus::$MAP['MARK_CONFIRMED'])
                {
                    //skip if this is recount
                    continue;
                }

                $tmpCycleCountDtls = array();
                if(array_key_exists($cycleCountDtl->company_id, $cycleCountDtlsHashByCompanyId))
                {
                    $tmpCycleCountDtls = $cycleCountDtlsHashByCompanyId[$cycleCountDtl->company_id];
                }
                $tmpCycleCountDtls[] = $cycleCountDtl;
                $cycleCountDtlsHashByCompanyId[$cycleCountDtl->company_id] = $tmpCycleCountDtls;

                //add to array, will use to change the physical_count_status later
                $oriCycleCountDtlDataList[] = array(
                    'id'=>$cycleCountDtl->id
                );
            }

            foreach($cycleCountDtlsHashByCompanyId as $companyId => $cmpCycleCountDtls)
            {
                //2) group cycleCountDtl by storageBinId
                $cycleCountDtlsHashByStorageBinId = array();
                foreach($cmpCycleCountDtls as $cycleCountDtl)
                {
                    $tmpCycleCountDtls = array();
                    if(array_key_exists($cycleCountDtl->storage_bin_id, $cycleCountDtlsHashByStorageBinId))
                    {
                        $tmpCycleCountDtls = $cycleCountDtlsHashByStorageBinId[$cycleCountDtl->storage_bin_id];
                    }
                    $tmpCycleCountDtls[] = $cycleCountDtl;
                    $cycleCountDtlsHashByStorageBinId[$cycleCountDtl->storage_bin_id] = $tmpCycleCountDtls;
                }
                
                //3) go to each bin, and group each bin cycleCountDtls by handlingUnitId, itemId, itemBatchId
                foreach($cycleCountDtlsHashByStorageBinId as $storageBinId => $tmpCycleCountDtls)
                {
                    //query the quantBal by storageBinId
                    $dbQuantBals = QuantBalRepository::findAllByStorageBinIdAndCompanyId($storageBinId, $companyId, $cycleCountHdr->doc_date->format('Y-m-d'));
                    $dbQuantBals->load('itemBatch');

                    //group by $handlingUnitId, $itemId, $itemBatchId
                    $quantBalsHashByKey = array();
                    foreach($dbQuantBals as $dbQuantBal)
                    {
                        $key = $dbQuantBal->handling_unit_id.'/'.$dbQuantBal->item_id.'/'.$dbQuantBal->item_batch_id;
                        $quantBalsHashByKey[$key] = $dbQuantBal;
                    }

                    //group cycleCountDtls by handlingUnitId + itemId + itemBatchId
                    $cycleCountDtlsHashByKey = array();
                    foreach($tmpCycleCountDtls as $cycleCountDtl)
                    {
                        $cycleCountBin = $cycleCountDtl->storageBin;
                        /*
                        //if bin type is pick face, handlingUnitId is 0
                        $key = '0'.'/'.$cycleCountDtl->item_id.'/'.$cycleCountDtl->item_batch_id;
                        $storageType = $cycleCountBin->storageType;
                        if($storageType->isPallet() == true)
                        {
                            $key = $cycleCountDtl->handling_unit_id.'/'.$cycleCountDtl->item_id.'/'.$cycleCountDtl->item_batch_id;
                        }
                        */
                        $key = $cycleCountDtl->handling_unit_id.'/'.$cycleCountDtl->item_id.'/'.$cycleCountDtl->item_batch_id;

                        $tmpCCDArray = array();
                        if(array_key_exists($key, $cycleCountDtlsHashByKey))
                        {
                            $tmpCCDArray = $cycleCountDtlsHashByKey[$key];
                        }
                        $tmpCCDArray[] = $cycleCountDtl;
                        $cycleCountDtlsHashByKey[$key] = $tmpCCDArray;
                    }

                    //loop for each handlingUnitId, itemId, itemBatchId
                    $zeroStockCycleCountDtl = null;
                    foreach($cycleCountDtlsHashByKey as $ccKey => $cycleCountDtls)
                    {
                        $cycleCountDtl = null;
                        $item = null;
                        $ttlCountUnitQty = 0;
                        foreach($cycleCountDtls as $lpCycleCountDtl)
                        {
                            $cycleCountDtl = $lpCycleCountDtl;
                            $item = $lpCycleCountDtl->item;
                            $countUnitQty = 0;
                            if(!empty($item))
                            {
                                $countUnitQty = bcmul($cycleCountDtl->uom_rate, $cycleCountDtl->qty, $item->qty_scale);
                                $ttlCountUnitQty = bcadd($ttlCountUnitQty, $countUnitQty, $item->qty_scale);
                            }
                        }

                        $cycleCountBin = $cycleCountDtl->storageBin;
                        $zeroStockCycleCountDtl = $cycleCountDtl;

                        /*
                        //if bin type is pick face, handlingUnitId is 0
                        $key = '0'.'/'.$cycleCountDtl->item_id.'/'.$cycleCountDtl->item_batch_id;
                        $storageType = $cycleCountBin->storageType;
                        if($storageType->isPallet() == true)
                        {
                            $key = $cycleCountDtl->handling_unit_id.'/'.$cycleCountDtl->item_id.'/'.$cycleCountDtl->item_batch_id;
                        }
                        */
                        $key = $cycleCountDtl->handling_unit_id.'/'.$cycleCountDtl->item_id.'/'.$cycleCountDtl->item_batch_id;
                        
                        $cycleCountDtl->quant_bal_id = 0;
                        $cycleCountDtl->balance_unit_qty = 0;
                        if(array_key_exists($key, $quantBalsHashByKey))
                        {
                            $quantBal = $quantBalsHashByKey[$key];
                            $cycleCountDtl->quant_bal_id = $quantBal->id;
                            $cycleCountDtl->balance_unit_qty = $quantBal->balance_unit_qty;
                            //unset the $quantBalsHashByKey[$key], so remaining will be adjust to 0
                            unset($quantBalsHashByKey[$key]);
                        }

                        $sign = -1;
                        $cycleCountDtl->variance_qty = 0;
                        if(!empty($item))
                        {
                            $cycleCountDtl->variance_qty = bcsub($ttlCountUnitQty, $cycleCountDtl->balance_unit_qty, $item->qty_scale);
                        }
                        if($cycleCountDtl->variance_qty >= 0)
                        {
                            $sign = 1;
                        }

                        if(bccomp($cycleCountDtl->variance_qty, 0, 10) == 0)
                        {
                            //skip if no need to adjust
                            continue;
                        }

                        $dtlData = array(
                            'line_no' => $lineNo,
                            'storage_bin_id' => $cycleCountDtl->storage_bin_id,
                            'item_id' => $cycleCountDtl->item_id,
                            'quant_bal_id' => $cycleCountDtl->quant_bal_id,
                            'item_batch_id' => $cycleCountDtl->item_batch_id,
                            'batch_serial_no' => $cycleCountDtl->batch_serial_no,
                            'expiry_date' => $cycleCountDtl->expiry_date,
                            'receipt_date' => $cycleCountDtl->receipt_date,
                            'handling_unit_id' => $cycleCountDtl->handling_unit_id,
                            'desc_01' => $cycleCountDtl->desc_01,
                            'desc_02' => $cycleCountDtl->desc_02,
                            'sign' => $sign,
                            'uom_id' => $item->unit_uom_id,
                            'uom_rate' => 1,
                            'qty' => abs($cycleCountDtl->variance_qty),
                            'fr_cycle_count_hdr_id' => $cycleCountDtl->hdr_id,
                            'fr_cycle_count_dtl_id' => $cycleCountDtl->id
                        );
                        $dtlDataList[] = $dtlData;

                        $lineNo++;
                    }

                    //for the remaining quantBals by handlingUnitId itemId, itemBatchId
                    //need to adjust qty to 0
                    foreach($quantBalsHashByKey as $key => $quantBal)
                    {
                        if(empty($item))
                        {
                            //this is case when do not have any cycle count
                            $item = $quantBal->item;
                        }
                        $itemBatch = $quantBal->itemBatch;

                        $sign = -1;
                        $varianceQty = bcsub(0, $quantBal->balance_unit_qty, $item->qty_scale);
                        if($varianceQty >= 0)
                        {
                            $sign = 1;
                        }

                        $dtlData = array(
                            'line_no' => $lineNo,
                            'storage_bin_id' => $quantBal->storage_bin_id,
                            'item_id' => $quantBal->item_id,
                            'quant_bal_id' => $quantBal->id,
                            'item_batch_id' => $quantBal->item_batch_id,
                            'batch_serial_no' => $itemBatch->batch_serial_no,
                            'expiry_date' => $itemBatch->expiry_date,
                            'receipt_date' => $itemBatch->receipt_date,
                            'handling_unit_id' => $quantBal->handling_unit_id,
                            'desc_01' => $item->desc_01,
                            'desc_02' => $item->desc_02,
                            'sign' => $sign,
                            'uom_id' => $item->unit_uom_id,
                            'uom_rate' => 1,
                            'qty' => abs($varianceQty),
                            'fr_cycle_count_hdr_id' => $zeroStockCycleCountDtl->hdr_id,
                            'fr_cycle_count_dtl_id' => $zeroStockCycleCountDtl->id
                        );
                        $dtlDataList[] = $dtlData;

                        $lineNo++;
                    }
                }

                $tmpDocTxnFlowData = array(
                    'fr_doc_hdr_type' => \App\CycleCountHdr::class,
                    'fr_doc_hdr_id' => $cycleCountHdr->id,
                    'fr_doc_hdr_code' => $cycleCountHdr->doc_code,
                    'is_closed' => 1
                );
                $docTxnFlowDataList[] = $tmpDocTxnFlowData;
                
                //build the hdrData
                $hdrData = array(
                    'site_flow_id' => $siteFlow->id,
                    'company_id' => $companyId,
                    'ref_code_01' => '',
                    'ref_code_02' => '',
                    'doc_date' => $cycleCountHdr->doc_date,
                    'desc_01' => '',
                    'desc_02' => '',
                    'site_flow_id' => $siteFlow->id
                );

                $siteDocNo = SiteDocNoRepository::findSiteDocNo($siteFlow->site_id, \App\CountAdjHdr::class);
                if(empty($siteDocNo))
                {
                    $exc = new ApiException(__('SiteDocNo.doc_no_not_found', ['siteId'=>$siteFlow->site_id, 'docType'=>\App\CountAdjHdr::class]));
                    //$exc->addData(\App\CountAdjHdr::class, $siteFlow->site_id);
                    throw $exc;
                }

                $whseTxnFlow = WhseTxnFlowRepository::findByPk($siteFlow->id, $procType);

                //DRAFT
                $countAdjHdr = CountAdjHdrRepository::createProcess($procType, $siteDocNo->doc_no_id, $hdrData, $dtlDataList, $docTxnFlowDataList, $oriCycleCountDtlDataList);

                if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
                {
                    $countAdjHdr = self::transitionToWip($countAdjHdr->id);
                }
                elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
                {
                    $countAdjHdr = self::transitionToComplete($countAdjHdr->id);
                }

                $countAdjHdrs[] = $countAdjHdr;
            }
        }

        $docCodeMsg = '';
        for ($a = 0; $a < count($countAdjHdrs); $a++)
        {
            $countAdjHdr = $countAdjHdrs[$a];
            if($a == 0)
            {
                $docCodeMsg .= $countAdjHdr->doc_code;
            }
            else
            {
                $docCodeMsg .= ', '.$countAdjHdr->doc_code;
            }
        }
        $message = __('CountAdj.document_successfully_created', ['docCode'=>$docCodeMsg]);

		return array(
			'data' => $countAdjHdrs,
			'message' => $message
		);
    }

    public function transitionToStatus($hdrId, $strDocStatus)
    {
        $hdrModel = null;
        if(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['WIP'])
        {
            $hdrModel = self::transitionToWip($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['COMPLETE'])
        {
            $hdrModel = self::transitionToComplete($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['DRAFT'])
        {
            $hdrModel = self::transitionToDraft($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['VOID'])
        {
            $hdrModel = self::transitionToVoid($hdrId);
        }
        
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $message = __('CountAdj.document_successfully_transition', ['docCode'=>$documentHeader->doc_code,'strDocStatus'=>$strDocStatus]);
        
        return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>array(),
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }

    static public function processOutgoingHeader($model, $isShowPrint = false)
	{
        $docFlows = self::processDocFlows($model);
        $model->doc_flows = $docFlows;
        
		if($isShowPrint)
		{
			$printDocTxn = PrintDocTxnRepository::queryPrintDocTxn(CountAdjHdr::class, $model->id);
			$model->print_count = $printDocTxn->print_count;
			$model->first_printed_at = $printDocTxn->first_printed_at;
			$model->last_printed_at = $printDocTxn->last_printed_at;
		}

        $model->str_doc_status = DocStatus::$MAP[$model->doc_status];
        
        $company = $model->company;
        $model->company_code = '';
        $initCompanyOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($company))
        {
            $model->company_code = $company->code;
            $initCompanyOption = array('value'=>$company->id,'label'=>$company->code);
        }
        $model->company_select2 = $initCompanyOption;

        unset($model->company);
		return $model;
	}

    static public function transitionToWip($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = CountAdjHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status >= DocStatus::$MAP['WIP'])
		{
			$exc = new ApiException(__('CountAdj.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\CountAdjHdr::class, $hdrModel->id);
            throw $exc;
		}

		//commit the document
		$hdrModel = CountAdjHdrRepository::commitToWip($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('CountAdj.commit_to_wip_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\CountAdjHdr::class, $hdrModel->id);
        throw $exc;
	}

	static public function transitionToComplete($hdrId)
    {
        AuthService::authorize(
            array(
                'count_adj_confirm'
            )
        );

		$isSuccess = false;
		//use transaction to make sure this is latest value
		$hdrModel = CountAdjHdrRepository::txnFindByPk($hdrId);

		//only DRAFT or above can transition to COMPLETE
		if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE']
		|| $hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('CountAdj.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\CountAdjHdr::class, $hdrModel->id);
            throw $exc;
		}

		$dtlModels = CountAdjDtlRepository::findAllByHdrId($hdrModel->id);
		//preliminary checking
		//check the detail stock make sure the detail quant_bal_id > 0
		foreach($dtlModels as $dtlModel)
		{
			if($dtlModel->item_id == 0)
			{
				$exc = new ApiException(__('CountAdj.item_is_required', ['lineNo'=>$dtlModel->line_no]));
				$exc->addData(\App\CountAdjDtl::class, $dtlModel->id);
				throw $exc;
			}
		}

		//commit the document
		$hdrModel = CountAdjHdrRepository::commitToComplete($hdrModel->id, true);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
        }
        
		$exc = new ApiException(__('CountAdj.commit_to_complete_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\CountAdjHdr::class, $hdrModel->id);
        throw $exc;
    }
    
    static public function transitionToDraft($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = CountAdjHdrRepository::txnFindByPk($hdrId);

		//only WIP or COMPLETE can transition to DRAFT
		if($hdrModel->doc_status == DocStatus::$MAP['WIP'])
		{
            $hdrModel = CountAdjHdrRepository::revertWipToDraft($hdrModel->id);
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		elseif($hdrModel->doc_status == DocStatus::$MAP['COMPLETE'])
		{
            AuthService::authorize(
                array(
                    'count_adj_revert'
                )
            );

            $hdrModel = CountAdjHdrRepository::revertCompleteToDraft($hdrModel->id);
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
        }
        elseif($hdrModel->doc_status == DocStatus::$MAP['VOID'])
		{
			$hdrModel = CountAdjHdrRepository::commitVoidToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
        
        $exc = new ApiException(__('CountAdj.doc_status_is_not_wip_or_complete', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\CountAdjHdr::class, $hdrModel->id);
		throw $exc;
    }
    
    static public function transitionToVoid($hdrId)
    {
        AuthService::authorize(
            array(
                'count_adj_revert'
            )
        );

		//use transaction to make sure this is latest value
		$hdrModel = CountAdjHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('CountAdj.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\CountAdjHdr::class, $hdrModel->id);
            throw $exc;
		}

		//revert the document
		$hdrModel = CountAdjHdrRepository::revertDraftToVoid($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('CountAdj.commit_to_void_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\CountAdjHdr::class, $hdrModel->id);
        throw $exc;
    }

    public function index($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
            array(
                'count_adj_read'
            )
        );

        //DB::connection()->enableQueryLog();
        $countAdjHdrs = CountAdjHdrRepository::findAll($siteFlowId, $sorts, $filters, $pageSize);
        $countAdjHdrs->load(
            'countAdjDtls', 
            'countAdjDtls.item', 'countAdjDtls.item.itemUoms', 'countAdjDtls.item.itemUoms.uom', 
            'countAdjDtls.quantBal', 'countAdjDtls.quantBal.itemBatch', 
            'countAdjDtls.quantBal.storageBin', 'countAdjDtls.quantBal.storageRow', 
            'countAdjDtls.quantBal.storageBay'
        );
		foreach($countAdjHdrs as $countAdjHdr)
		{
			$countAdjHdr->str_doc_status = DocStatus::$MAP[$countAdjHdr->doc_status];

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
            $cubicMeter = 0;
            
            $countAdjDtls = array();
            //query the docDtls
            /*
			$countAdjDtls = $countAdjHdr->countAdjDtls;
			foreach($countAdjDtls as $countAdjDtl)
			{
				$item = $countAdjDtl->item;
				$quantBal = $countAdjDtl->quantBal;
				$itemBatch = null;
				$storageBin = null;
				$storageRow = null;
				$storageBay = null;
				if(!empty($quantBal))
				{
					$itemBatch = $quantBal->itemBatch;
					$storageBin = $quantBal->storageBin;
					$storageRow = $quantBal->storageRow;
					$storageBay = $quantBal->storageBay;
				}

				$countAdjDtl->batch_serial_no = '';
				$countAdjDtl->expiry_date = '';
				$countAdjDtl->receipt_date = '';
				if(!empty($itemBatch))
				{
					$countAdjDtl->batch_serial_no = $itemBatch->batch_serial_no;
					$countAdjDtl->expiry_date = $itemBatch->expiry_date;
					$countAdjDtl->receipt_date = $itemBatch->receipt_date;
				}

				$countAdjDtl->storage_bin_code = '';
				if(!empty($storageBin))
				{
					$countAdjDtl->storage_bin_code = $storageBin->code;
				}
				$countAdjDtl->storage_row_code = '';
				if(!empty($storageRow))
				{
					$countAdjDtl->storage_row_code = $storageRow->code;
				}
				$countAdjDtl->storage_bay_code = '';
				$countAdjDtl->item_bay_sequence = 0;
				if(!empty($storageBay))
				{
					$countAdjDtl->storage_bay_code = $storageBay->code;
					$countAdjDtl->item_bay_sequence = $storageBay->bay_sequence;
				}

				//calculate the pallet qty, case qty, gross weight, and m3
				$countAdjDtl = ItemService::processCaseLoose($countAdjDtl, $item);

                $caseQty = bcadd($caseQty, $countAdjDtl->case_qty, 8);
                $grossWeight = bcadd($grossWeight, $countAdjDtl->gross_weight, 8);
                $cubicMeter = bcadd($cubicMeter, $countAdjDtl->cubic_meter, 8);

				unset($countAdjDtl->quantBal);
				unset($countAdjDtl->item);
            }
            */
			$countAdjHdr->case_qty = $caseQty;
			$countAdjHdr->gross_weight = $grossWeight;
			$countAdjHdr->cubic_meter = $cubicMeter;

			$countAdjHdr->details = $countAdjDtls;
        }
        //Log::error(DB::getQueryLog());
    	return $countAdjHdrs;
    }

    public function showHeader($hdrId)
    {
        AuthService::authorize(
            array(
                'count_adj_read',
                'count_adj_update'
            )
        );

        $model = CountAdjHdrRepository::findByPk($hdrId);
        if(!empty($model))
        {
            $model = self::processOutgoingHeader($model);
        }
        
        return $model;
	}
	
	public function showDetails($hdrId) 
	{
        AuthService::authorize(
            array(
                'count_adj_read',
                'count_adj_update'
            )
        );

		$countAdjDtls = CountAdjDtlRepository::findAllByHdrId($hdrId);
		$countAdjDtls->load(
            'item', 'item.itemUoms', 'item.itemUoms.uom'
        );
        foreach($countAdjDtls as $countAdjDtl)
        {
			$countAdjDtl = self::processOutgoingDetail($countAdjDtl);
        }
        return $countAdjDtls;
	}

	static public function processIncomingHeader($data)
    {
        if(array_key_exists('doc_flows', $data))
        {
            unset($data['doc_flows']);
        }
		if(array_key_exists('submit_action', $data))
        {
            unset($data['submit_action']);
        }
        //preprocess the select2 and dropDown
        
        if(array_key_exists('str_doc_status', $data))
        {
            unset($data['str_doc_status']);
        }
        
        if(array_key_exists('company_select2', $data))
        {
			$select2Id = $data['company_select2']['value'];
			if($select2Id > 0)
			{
				$data['company_id'] = $select2Id;
			}
            unset($data['company_select2']);
        }        

        if(array_key_exists('company_code', $data))
        {
            unset($data['company_code']);
		}
        return $data;
    }

	public function updateHeader($hdrData)
	{
        AuthService::authorize(
            array(
                'count_adj_update'
            )
        );

        $hdrData = self::processIncomingHeader($hdrData);

        $countAdjHdr = CountAdjHdrRepository::txnFindByPk($hdrData['id']);
        if($countAdjHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('CountAdj.doc_status_is_wip_or_complete', ['docCode'=>$countAdjHdr->doc_code]));
            $exc->addData(\App\CountAdjHdr::class, $countAdjHdr->id);
            throw $exc;
        }
        $countAdjHdrData = $countAdjHdr->toArray();
        //assign hdrData to model
        foreach($hdrData as $field => $value) 
        {
            $countAdjHdrData[$field] = $value;
        }

		//$countAdjHdr->load('worker01');
        //$worker01 = $countAdjHdr->worker01;
		
		//no detail to update
        $countAdjDtlArray = array();

        $result = CountAdjHdrRepository::updateDetails($countAdjHdrData, $countAdjDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];

        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = self::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('CountAdj.document_successfully_updated', ['docCode'=>$documentHeader->doc_code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}
	
	public function createDetail($hdrId, $data)
	{
        AuthService::authorize(
            array(
                'count_adj_update'
            )
        );

        $data = self::processIncomingDetail($data);
		//$data = $this->updateWarehouseItemUom($data, 0, 0);

        $countAdjHdr = CountAdjHdrRepository::txnFindByPk($hdrId);
        if($countAdjHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('CountAdj.doc_status_is_wip_or_complete', ['docCode'=>$countAdjHdr->doc_code]));
            $exc->addData(\App\CountAdjHdr::class, $countAdjHdr->id);
            throw $exc;
        }
        
        //validate the details
        $storageBin = StorageBinRepository::findByPk($data['storage_bin_id']);
        if(empty($storageBin))
        {
            $exc = new ApiException(__('CountAdj.storage_bin_is_required', ['lineNo'=>$data['line_no']]));
            $exc->addData(\App\CountAdjDtl::class, $data['id']);
            throw $exc;
        }

        /*
        //if bin type is pick face, handlingUnitId is 0
        $storageType = $storageBin->storageType;
        if($storageType->isPallet() == true)
        {
            //if not pick face, then is pallet storage, required handling_unit_id
            if($data['handling_unit_id'] <= 0)
            {
                $exc = new ApiException(__('CountAdj.handling_unit_is_required', ['lineNo'=>$data['line_no']]));
                $exc->addData(\App\CountAdjDtl::class, $data['id']);
                throw $exc;
            }
        }
        */

		$countAdjHdrData = $countAdjHdr->toArray();

		$countAdjDtlArray = array();
		$countAdjDtlModels = CountAdjDtlRepository::findAllByHdrId($hdrId);
		foreach($countAdjDtlModels as $countAdjDtlModel)
		{
			$countAdjDtlData = $countAdjDtlModel->toArray();
			$countAdjDtlData['is_modified'] = 0;
			$countAdjDtlArray[] = $countAdjDtlData;
		}
		$lastLineNo = count($countAdjDtlModels);

		//assign temp id
		$tmpUuid = uniqid();
		//append NEW here to make sure it will be insert in repository later
		$data['id'] = 'NEW'.$tmpUuid;
		$data['line_no'] = $lastLineNo + 1;
		$data['is_modified'] = 1;
		
        $countAdjDtlData = $this->initCountAdjDtlData($data);
        
        //verify expiry_date/receipt_date, if not same with item_batch_id
        //then set item_batch_id = 0
        $itemBatch = ItemBatchRepository::findByPk($countAdjDtlData['item_batch_id']);
        if(!empty($itemBatch)
        && strcmp($countAdjDtlData['expiry_date'], '1970-01-01') != 0
        && strcmp($countAdjDtlData['expiry_date'], '0000-00-00') != 0)
        {
            if(strcmp($itemBatch->expiry_date, $countAdjDtlData['expiry_date']) != 0)
            {
                $countAdjDtlData['item_batch_id'] = 0;
            }
        }

		$countAdjDtlArray[] = $countAdjDtlData;

		$result = CountAdjHdrRepository::updateDetails($countAdjHdrData, $countAdjDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('CountAdj.detail_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function deleteDetails($hdrId, $dtlArray)
	{
        AuthService::authorize(
            array(
                'count_adj_update'
            )
        );

        $countAdjHdr = CountAdjHdrRepository::txnFindByPk($hdrId);
        if($countAdjHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('CountAdj.doc_status_is_wip_or_complete', ['docCode'=>$countAdjHdr->doc_code]));
            $exc->addData(\App\CountAdjHdr::class, $countAdjHdr->id);
            throw $exc;
        }
		$countAdjHdrData = $countAdjHdr->toArray();

		//query the countAdjDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$delCountAdjDtlArray = array();
		$countAdjDtlArray = array();
        $countAdjDtlModels = CountAdjDtlRepository::findAllByHdrId($hdrId);
        $lineNo = 1;
		foreach($countAdjDtlModels as $countAdjDtlModel)
		{
			$countAdjDtlData = $countAdjDtlModel->toArray();
			$countAdjDtlData['is_modified'] = 0;
			$countAdjDtlData['is_deleted'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{
				if($dtlData['id'] == $countAdjDtlData['id'])
				{
					//this is deleted dtl
					$countAdjDtlData['is_deleted'] = 1;
					break;
				}
			}
			if($countAdjDtlData['is_deleted'] > 0)
			{
				$delCountAdjDtlArray[] = $countAdjDtlData;
			}
			else
			{
                $countAdjDtlData['line_no'] = $lineNo;
                $countAdjDtlData['is_modified'] = 1;
                $countAdjDtlArray[] = $countAdjDtlData;
                $lineNo++;
			}
        }
		
		$result = CountAdjHdrRepository::updateDetails($countAdjHdrData, $countAdjDtlArray, $delCountAdjDtlArray);
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = self::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('CountAdj.details_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>$delCountAdjDtlArray
            ),
			'message' => $message
		);
	}

	public function updateDetails($hdrId, $dtlArray)
	{
        AuthService::authorize(
            array(
                'count_adj_update'
            )
        );

		$countAdjHdr = CountAdjHdrRepository::txnFindByPk($hdrId);
        if($countAdjHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('CountAdj.doc_status_is_wip_or_complete', ['docCode'=>$countAdjHdr->doc_code]));
            $exc->addData(\App\CountAdjHdr::class, $countAdjHdr->id);
            throw $exc;
        }
		$countAdjHdrData = $countAdjHdr->toArray();

		//query the countAdjDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$countAdjDtlArray = array();
        $countAdjDtlModels = CountAdjDtlRepository::findAllByHdrId($hdrId);
		foreach($countAdjDtlModels as $countAdjDtlModel)
		{
			$countAdjDtlData = $countAdjDtlModel->toArray();
			$countAdjDtlData['is_modified'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{                
				if($dtlData['id'] == $countAdjDtlData['id'])
				{
                    $dtlData = self::processIncomingDetail($dtlData);
                    
                    //validate the detail data
                    $storageBin = StorageBinRepository::findByPk($dtlData['storage_bin_id']);
                    if(empty($storageBin))
                    {
                        $exc = new ApiException(__('CountAdj.storage_bin_is_required', ['lineNo'=>$dtlData['line_no']]));
                        $exc->addData(\App\CountAdjDtl::class, $dtlData['id']);
                        throw $exc;
                    }

                    $storageType = $storageBin->storageType;
                    if($storageType->isPallet() == true)
                    {
                        //if this is pallet storage, required handling_unit_id
                        if($dtlData['handling_unit_id'] <= 0)
                        {
                            $exc = new ApiException(__('CountAdj.handling_unit_is_required', ['lineNo'=>$dtlData['line_no']]));
                            $exc->addData(\App\CountAdjDtl::class, $dtlData['id']);
                            throw $exc;
                        }
                    }

					foreach($dtlData as $fieldName => $value)
					{
						$countAdjDtlData[$fieldName] = $value;
                    }
                    
                    //verify expiry_date/receipt_date, if not same with item_batch_id
                    //then set item_batch_id = 0
                    $itemBatch = ItemBatchRepository::findByPk($countAdjDtlData['item_batch_id']);
                    if(!empty($itemBatch)                    
                    && strcmp($countAdjDtlData['expiry_date'], '1970-01-01') != 0
                    && strcmp($countAdjDtlData['expiry_date'], '0000-00-00') != 0)
                    {
                        if(strcmp($itemBatch->expiry_date, $countAdjDtlData['expiry_date']) != 0)
                        {
                            $countAdjDtlData['item_batch_id'] = 0;
                        }
                    }

					$countAdjDtlData['is_modified'] = 1;

					break;
				}
			}
			$countAdjDtlArray[] = $countAdjDtlData;
		}
		
		$result = CountAdjHdrRepository::updateDetails($countAdjHdrData, $countAdjDtlArray, array(), array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = self::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('CountAdj.details_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }
    
    static public function processOutgoingDetail($model)
  	{
        //relationships:
        $quantBal = $model->quantBal;
        $storageBin = $model->storageBin;
		$handlingUnit = $model->handlingUnit;
        $item = $model->item;
        $itemBatch = $model->itemBatch;
		$uom = $model->uom;

		$model->storage_bin_code = '';
		$model->storage_row_code = '';
		$model->storage_bay_code = '';
        $model->handling_unit_barcode = '';
        
        $model->sign_select2 = array('value'=>-1,'label'=>'-');
        if($model->sign > 0)
        {
            $model->sign_select2 = array('value'=>1,'label'=>'+');
        }

        $storageRow = null;
        $storageBay = null;
		if(!empty($storageBin))
		{
            $storageRow = $storageBin->storageRow;
            $storageBay = $storageBin->storageBay;
			$model->storage_bin_code = $storageBin->code;
		}
		if(!empty($handlingUnit))
		{
			$model->handling_unit_barcode = $handlingUnit->getBarcode();
		}
		if(!empty($storageRow))
		{
			$model->storage_row_code = $storageRow->code;
		}
		$model->item_bay_sequence = 0;
		if(!empty($storageBay))
		{
			$model->storage_bay_code = $storageBay->code;
			$model->item_bay_sequence = $storageBay->bay_sequence;
        }

		if(!empty($itemBatch))
		{
			$model->batch_serial_no = $itemBatch->batch_serial_no;
			$model->expiry_date = $itemBatch->expiry_date;
			$model->receipt_date = $itemBatch->receipt_date;
		}

		//calculate the pallet qty, case qty, gross weight, and m3
        $model = ItemService::processCaseLoose($model, $item);

        //storageBin select2
        $initQuantBalOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($quantBal))
        {
            $quantBal = ItemService::processCaseLoose($quantBal, $item, 1);
            $label = $quantBal->handling_unit_barcode.' | '.$quantBal->item_code
            .' | '.$quantBal->case_qty.' '.$quantBal->item_case_uom_code
            .' | '.$quantBal->loose_qty.' '.$quantBal->item_loose_uom_code;
            $initQuantBalOption = array(
                'value'=>$quantBal->id,
                'label'=>$label
            );
        }        
		$model->quant_bal_select2 = $initQuantBalOption;

        $initStorageBinOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($storageBin))
        {
            $initStorageBinOption = array('value'=>$storageBin->id,'label'=>$storageBin->code);
        }        
		$model->storage_bin_select2 = $initStorageBinOption;
		
		$initHandlingUnitOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($handlingUnit))
        {
            $initHandlingUnitOption = array('value'=>$handlingUnit->id,'label'=>$handlingUnit->getBarcode());
        }        
		$model->handling_unit_select2 = $initHandlingUnitOption;

        $initItemOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($item))
        {
            $initItemOption = array('value'=>$item->id,'label'=>$item->code);
        } 
        $model->item_select2 = $initItemOption;

        $initItemBatchOption = array(
            'value'=>0,
            'label'=>'',
        );
        if(!empty($itemBatch))
        {
            $initItemBatchOption = array(
                'value'=>$itemBatch->id,
                'label'=>$itemBatch->expiry_date,
                'batch_serial_no'=>$itemBatch->batch_serial_no,
                'expiry_date'=>$itemBatch->expiry_date,
                'receipt_date'=>$itemBatch->receipt_date,
            );
        }
        $model->item_batch_select2 = $initItemBatchOption;

        $model->uom_code = '';
        $initUomOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($uom))
        {
            $model->uom_code = $uom->code;
            $initUomOption = array('value'=>$uom->id,'label'=>$uom->code);
        }        
        $model->uom_select2 = $initUomOption;
        
        unset($model->quantBal);
        unset($model->storageBin);
		unset($model->handlingUnit);
        unset($model->item);
        unset($model->itemBatch);
        unset($model->uom);

		return $model;
    }

    public function changeQuantBal($hdrId, $quantBalId)
    {
        AuthService::authorize(
            array(
                'count_adj_update'
            )
        );

		$countAdjHdr = CountAdjHdrRepository::findByPk($hdrId);

		$results = array();
		$quantBal = QuantBalRepository::findByPk($quantBalId);
        if(!empty($quantBal))
        {
            $handlingUnit = $quantBal->handlingUnit;
			$item = $quantBal->item;
			$itemBatch = $quantBal->itemBatch;
			
			$quantBal = ItemService::processCaseLoose($quantBal, $item, 1);

			$results['desc_01'] = $quantBal->item_desc_01;
			$results['desc_02'] = $quantBal->item_desc_02;

            $results['batch_serial_no'] = empty($itemBatch) ? '' : $itemBatch->batch_serial_no;
            $results['expiry_date'] = empty($itemBatch) ? '' : $itemBatch->expiry_date;
            $results['receipt_date'] = empty($itemBatch) ? '' : $itemBatch->receipt_date;

            $initHandlingUnitOption = array(
                'value'=>0,
                'label'=>''
            );
            if(!empty($handlingUnit))
            {
                $initHandlingUnitOption = array('value'=>$handlingUnit->id,'label'=>$handlingUnit->getBarcode());
            }        
            $results['handling_unit_select2'] = $initHandlingUnitOption;
    
            $initItemOption = array(
                'value'=>0,
                'label'=>''
            );
            if(!empty($item))
            {
                $initItemOption = array('value'=>$item->id,'label'=>$item->code);
            } 
            $results['item_select2'] = $initItemOption;
    
            $initItemBatchOption = array(
                'value'=>0,
                'label'=>'',
            );
            if(!empty($itemBatch))
            {
                $initItemBatchOption = array(
                    'value'=>$itemBatch->id,
                    'label'=>$itemBatch->expiry_date,
                    'batch_serial_no'=>$itemBatch->batch_serial_no,
                    'expiry_date'=>$itemBatch->expiry_date,
                    'receipt_date'=>$itemBatch->receipt_date,
                );
            }
            $results['item_batch_select2'] = $initItemBatchOption;
		}

        return $results;
    }

    public function changeItem($hdrId, $itemId)
    {
        AuthService::authorize(
            array(
                'count_adj_update'
            )
        );

        $countAdjHdr = CountAdjHdrRepository::findByPk($hdrId);

        $results = array();
        $item = ItemRepository::findByPk($itemId);
        if(!empty($item)
        && !empty($countAdjHdr))
        {
            $results['desc_01'] = $item->desc_01;
            $results['desc_02'] = $item->desc_02;
        }
        return $results;
    }

    public function changeItemBatch($hdrId, $itemBatchId)
    {
        AuthService::authorize(
            array(
                'count_adj_update'
            )
        );

        $countAdjHdr = CountAdjHdrRepository::findByPk($hdrId);

        $results = array();
        $itemBatch = ItemBatchRepository::findByPk($itemBatchId);
        if(!empty($itemBatch)
        && !empty($countAdjHdr))
        {
            $results['batch_serial_no'] = $itemBatch->batch_serial_no;
            $results['expiry_date'] = $itemBatch->expiry_date;
            $results['receipt_date'] = $itemBatch->receipt_date;
        }
        return $results;
    }

    public function changeItemUom($hdrId, $itemId, $uomId)
    {
        AuthService::authorize(
            array(
                'count_adj_update'
            )
        );

        $countAdjHdr = CountAdjHdrRepository::findByPk($hdrId);

        $results = array();
        $itemUom = ItemUomRepository::findByItemIdAndUomId($itemId, $uomId);
        if(!empty($itemUom)
        && !empty($countAdjHdr))
        {
            $results['item_id'] = $itemUom->item_id;
            $results['uom_id'] = $itemUom->uom_id;
            $results['uom_rate'] = $itemUom->uom_rate;

			$results = $this->updateWarehouseItemUom($results, 0, 0);
        }
        return $results;
    }

    static public function processIncomingDetail($data)
    {
        //preprocess the select2 and dropDown
        if(array_key_exists('storage_bin_select2', $data))
        {
			$select2Id = $data['storage_bin_select2']['value'];
			if($select2Id > 0)
			{
				$data['storage_bin_id'] = $select2Id;
			}
            unset($data['storage_bin_select2']);
		}
		if(array_key_exists('handling_unit_select2', $data))
        {
			$select2Id = $data['handling_unit_select2']['value'];
			if($select2Id > 0)
			{
				$data['handling_unit_id'] = $select2Id;
			}
            unset($data['handling_unit_select2']);
        }
        if(array_key_exists('item_select2', $data))
        {
			$select2Id = $data['item_select2']['value'];
			if($select2Id > 0)
			{
				$data['item_id'] = $select2Id;
			}
            unset($data['item_select2']);
        }
        if(array_key_exists('item_batch_select2', $data))
        {
			$select2Id = $data['item_batch_select2']['value'];
			if($select2Id > 0)
			{
				$data['item_batch_id'] = $select2Id;
			}
            unset($data['item_batch_select2']);
        }
        if(array_key_exists('uom_select2', $data))
        {
			$select2Id = $data['uom_select2']['value'];
			if($select2Id > 0)
			{
				$data['uom_id'] = $select2Id;
			}
            unset($data['uom_select2']);
        }
        if(array_key_exists('sign_select2', $data))
        {
			$select2Id = $data['sign_select2']['value'];
			$data['sign'] = $select2Id;
            unset($data['sign_select2']);
        }
        
		if(array_key_exists('qty', $data)
		&& !empty($item))
		{
			$data['qty'] = round($data['qty'], $item->qty_scale);
        }
     
        if(array_key_exists('quant_bal_select2', $data))
        {
            unset($data['quant_bal_select2']);
        }
        if(array_key_exists('company_code', $data))
        {
            unset($data['company_code']);
        }
        if(array_key_exists('storage_bin_code', $data))
        {
            unset($data['storage_bin_code']);
		}
		if(array_key_exists('storage_row_code', $data))
        {
            unset($data['storage_row_code']);
		}
		if(array_key_exists('storage_bay_code', $data))
        {
            unset($data['storage_bay_code']);
		}
		if(array_key_exists('item_bay_sequence', $data))
        {
            unset($data['item_bay_sequence']);
		}
		if(array_key_exists('item_bay_sequence', $data))
        {
            unset($data['item_bay_sequence']);
		}
		if(array_key_exists('to_storage_bin_code', $data))
        {
            unset($data['to_storage_bin_code']);
		}

		if(array_key_exists('str_whse_job_type', $data))
        {
            unset($data['str_whse_job_type']);
		}

        if(array_key_exists('handling_unit_barcode', $data))
        {
            unset($data['handling_unit_barcode']);
		}
		if(array_key_exists('handling_unit_ref_code_01', $data))
        {
            unset($data['handling_unit_ref_code_01']);
        }
        
        if(array_key_exists('item_code', $data))
        {
            unset($data['item_code']);
		}
		if(array_key_exists('item_ref_code_01', $data))
        {
            unset($data['item_ref_code_01']);
		}
		if(array_key_exists('item_desc_01', $data))
        {
            unset($data['item_desc_01']);
		}
		if(array_key_exists('item_desc_02', $data))
        {
            unset($data['item_desc_02']);
		}
		if(array_key_exists('storage_class', $data))
        {
            unset($data['storage_class']);
		}
		if(array_key_exists('item_group_01_code', $data))
        {
            unset($data['item_group_01_code']);
		}
		if(array_key_exists('item_group_02_code', $data))
        {
            unset($data['item_group_02_code']);
		}
		if(array_key_exists('item_group_03_code', $data))
        {
            unset($data['item_group_03_code']);
		}
		if(array_key_exists('item_group_04_code', $data))
        {
            unset($data['item_group_04_code']);
		}
		if(array_key_exists('item_group_05_code', $data))
        {
            unset($data['item_group_05_code']);
		}
		if(array_key_exists('item_cases_per_pallet_length', $data))
        {
            unset($data['item_cases_per_pallet_length']);
		}
		if(array_key_exists('item_cases_per_pallet_width', $data))
        {
            unset($data['item_cases_per_pallet_width']);
		}
		if(array_key_exists('item_no_of_layers', $data))
        {
            unset($data['item_no_of_layers']);
		}

		if(array_key_exists('unit_qty', $data))
        {
            unset($data['unit_qty']);
		}
		if(array_key_exists('item_unit_uom_code', $data))
        {
            unset($data['item_unit_uom_code']);
		}
		if(array_key_exists('loose_qty', $data))
        {
            unset($data['loose_qty']);
		}
		if(array_key_exists('item_loose_uom_code', $data))
        {
            unset($data['item_loose_uom_code']);
        }
        if(array_key_exists('loose_uom_id', $data))
        {
            unset($data['loose_uom_id']);
		}
		if(array_key_exists('loose_uom_rate', $data))
        {
            unset($data['loose_uom_rate']);
		}
		if(array_key_exists('case_qty', $data))
        {
            unset($data['case_qty']);
		}
		if(array_key_exists('case_uom_rate', $data))
        {
            unset($data['case_uom_rate']);
		}
        if(array_key_exists('item_case_uom_code', $data))
        {
            unset($data['item_case_uom_code']);
        }
        if(array_key_exists('uom_code', $data))
        {
            unset($data['uom_code']);
		}
		if(array_key_exists('item_unit_barcode', $data))
        {
            unset($data['item_unit_barcode']);
		}
		if(array_key_exists('item_case_barcode', $data))
        {
            unset($data['item_case_barcode']);
		}
		if(array_key_exists('gross_weight', $data))
        {
            unset($data['gross_weight']);
		}
		if(array_key_exists('cubic_meter', $data))
        {
            unset($data['cubic_meter']);
        }
        return $data;
    }

    public function initHeader($siteFlowId)
    {
        AuthService::authorize(
            array(
                'count_adj_create'
            )
        );

        $user = Auth::user();

        $siteFlow = SiteFlowRepository::findByPk($siteFlowId);
        $model = new CountAdjHdr();
        $model->doc_flows = array();
        $model->doc_status = DocStatus::$MAP['DRAFT'];
        $model->str_doc_status = DocStatus::$MAP[$model->doc_status];
        $model->doc_code = '';
        $model->ref_code_01 = '';
        $model->ref_code_02 = '';
        $model->doc_date = date('Y-m-d');
        $model->desc_01 = '';
        $model->desc_02 = '';

        $model->site_flow_id = $siteFlowId;

        $model->doc_no_id = 0;
        $docNoIdOptions = array();
        $docNos = DocNoRepository::findAllSiteDocNo($siteFlow->site_id, \App\CountAdjHdr::class, $model->doc_date);
        for($a = 0; $a < count($docNos); $a++)
        {
            $docNo = $docNos[$a];
            if($a == 0)
            {
                $model->doc_no_id = $docNo->id;
            }
            $docNoIdOptions[] = array('value'=>$docNo->id, 'label'=>$docNo->latest_code);
        }
        $model->doc_no_id_options = $docNoIdOptions;

        //company select2
        $initCompanyOption = array(
            'value'=>0,
            'label'=>''
        );
        $model->company_select2 = $initCompanyOption;

        return $model;
    }

    public function createHeader($data)
    {		
        AuthService::authorize(
            array(
                'count_adj_create'
            )
        );

        $data = self::processIncomingHeader($data, true);
        $docNoId = $data['doc_no_id'];
        unset($data['doc_no_id']);
        $ordHdrModel = CountAdjHdrRepository::createHeader(ProcType::$MAP['NULL'], $docNoId, $data);

        $message = __('CountAdj.document_successfully_created', ['docCode'=>$ordHdrModel->doc_code]);

		return array(
			'data' => $ordHdrModel->id,
			'message' => $message
		);
    }

    public function uploadDetails($hdrId, $file)
    {
        AuthService::authorize(
            array(
                'count_adj_update'
            )
        );

        $user = Auth::user();

        if(empty($file))
        {
            $exc = new ApiException(__('CountAdj.select_file_to_upload', []));
            $exc->addData(\App\CountAdjHdr::class, $hdrId);
            throw $exc;
        }

        $path = Storage::putFileAs('upload/', $file, 'COUNT_ADJ_DETAILS_'.$user->id.'.XLSX');

        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['COUNT_ADJ_DETAILS'], $user->id);

        //query the header
        $countAdjHdr = CountAdjHdrRepository::txnFindByPk($hdrId);
        if($countAdjHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('CountAdj.doc_status_is_wip_or_complete', ['docCode'=>$countAdjHdr->doc_code]));
            $exc->addData(\App\CountAdjHdr::class, $countAdjHdr->id);
            throw $exc;
        }
        $countAdjHdrData = $countAdjHdr->toArray();

        $siteFlow = $countAdjHdr->siteFlow;

        //hash by lineNo
        $countAdjDtlDataHashByLineNo = array();

        $countAdjDetailsImport = new CountAdjDetailsImport($hdrId, $user->id, $batchJobStatusModel);
        $excel = Excel::import($countAdjDetailsImport, $path);

        $rows = $countAdjDetailsImport->getCountAdjDetailRows();
        for($a = 0; $a < $countAdjDetailsImport->getTotal(); $a++)
        {
            $row = $rows[$a];

            //skip if code column is empty
            if(empty(trim($row['line_no'])))
            {
                continue;
            }

            $statusNumber = bcmul(bcdiv($a + 1, $countAdjDetailsImport->getTotal(), 8), 100, 2);
            BatchJobStatusRepository::updateStatusNumber($batchJobStatusModel->id, $statusNumber);
            
            $cCountAdjDtlData = array();
            $cCountAdjDtlData['line_no'] = $row['line_no'];

            $item = ItemRepository::findByCode($row['item_code']);
            if(empty($item))
            {
                $exc = new ApiException(__('CountAdj.item_is_required', ['lineNo'=>$cCountAdjDtlData['line_no']]));
                //$exc->addData(\App\CountAdjDtl::class, $row['id']);
                throw $exc;
            }
            $cCountAdjDtlData['item_id'] = $item->id;

            $storageBin = StorageBinRepository::findBySiteIdAndCode($siteFlow->site_id, $row['storage_bin_code']);
            if(empty($storageBin))
            {
                $exc = new ApiException(__('CountAdj.storage_bin_is_required', ['lineNo'=>$cCountAdjDtlData['line_no']]));
                //$exc->addData(\App\CountAdjDtl::class, $row['id']);
                throw $exc;
            }
            $cCountAdjDtlData['storage_bin_id'] = $storageBin->id;

            $row['handling_unit_id'] = str_replace('ZY', '', $row['handling_unit']);
            $row['handling_unit_id'] = intval($row['handling_unit_id']);

            $storageType = $storageBin->storageType;
            if($storageType->isPallet() == true)
            {
                //if this is pallet storage, required handling_unit_id
                /*
                if($row['handling_unit_id'] <= 0)
                {
                    $exc = new ApiException(__('CountAdj.handling_unit_is_required', ['lineNo'=>$cCountAdjDtlData['line_no']]));
                    //$exc->addData(\App\CountAdjDtl::class, $row['id']);
                    throw $exc;
                }
                */
            }
            $cCountAdjDtlData['handling_unit_id'] = $row['handling_unit_id'];

            $cCountAdjDtlData['batch_serial_no'] = $row['batch_no'];
            
            try
            {
                $cCountAdjDtlData['expiry_date'] = date('Y-m-d', Date::excelToTimestamp($row['expiry_date']));
            }
            catch(ErrorException $exp)
            {
                $cCountAdjDtlData['expiry_date'] = $row['expiry_date'];
            }

            try
            {
                $cCountAdjDtlData['receipt_date'] = date('Y-m-d', Date::excelToTimestamp($row['receipt_date']));
            }
            catch(ErrorException $exp)
            {
                $cCountAdjDtlData['receipt_date'] = $row['receipt_date'];
            }
            
            $cCountAdjDtlData['sign'] = $row['sign'];
            if(bccomp($cCountAdjDtlData['sign'], 1) > 0)
            {
                $cCountAdjDtlData['sign'] = 1;
            }
            if(bccomp($cCountAdjDtlData['sign'], -1) < 0)
            {
                $cCountAdjDtlData['sign'] = -1;
            }
            $cCountAdjDtlData['qty'] = $row['qty'];

            if(empty($row['uom_code']))
            {
                //if uom_code is empty, then assume it is unit uom
                $itemUom = ItemUomRepository::findByItemIdAndUomRate($cCountAdjDtlData['item_id'], 1);
                if(empty($itemUom))
                {
                    $exc = new ApiException(__('CountAdj.item_unit_uom_not_found', ['lineNo'=>$cCountAdjDtlData['line_no']]));
                    //$exc->addData(\App\CountAdjDtl::class, $row['id']);
                    throw $exc;
                }
                $cCountAdjDtlData['uom_id'] = $itemUom->uom_id;
                $cCountAdjDtlData['uom_rate'] = $itemUom->uom_rate;
            }
            else
            {
                $uom = UomRepository::findByCode($row['uom_code']);
                if(empty($uom))
                {
                    $exc = new ApiException(__('CountAdj.uom_is_required', ['lineNo'=>$cCountAdjDtlData['line_no']]));
                    //$exc->addData(\App\CountAdjDtl::class, $row['id']);
                    throw $exc;
                }
                $cCountAdjDtlData['uom_id'] = $uom->id;

                $itemUom = ItemUomRepository::findByItemIdAndUomId($cCountAdjDtlData['item_id'], $cCountAdjDtlData['uom_id']);
                if(empty($itemUom))
                {
                    $exc = new ApiException(__('CountAdj.item_uom_not_found', ['lineNo'=>$cCountAdjDtlData['line_no']]));
                    //$exc->addData(\App\CountAdjDtl::class, $row['id']);
                    throw $exc;
                }
                $cCountAdjDtlData['uom_rate'] = $itemUom->uom_rate;
            }

            $countAdjDtlDataHashByLineNo[$cCountAdjDtlData['line_no']] = $cCountAdjDtlData;
        }

        $allCountAdjDtlArray = array();
        $delCountAdjDtlArray = array();

        $countAdjDtlModels = CountAdjDtlRepository::findAllByHdrId($hdrId);
		foreach($countAdjDtlModels as $countAdjDtlModel)
		{
            $dbCountAdjDtlData = $countAdjDtlModel->toArray();
            $dbCountAdjDtlData['is_modified'] = 0;
            
            //check with going to updated records, to assign value to data, and set is_modified = true
            if(array_key_exists($dbCountAdjDtlData['line_no'], $countAdjDtlDataHashByLineNo))
            {
                //line no exist, so update
                $dtlData = $countAdjDtlDataHashByLineNo[$dbCountAdjDtlData['line_no']];

                foreach($dtlData as $fieldName => $value)
                {
                    $dbCountAdjDtlData[$fieldName] = $value;
                }

                $dbCountAdjDtlData['is_modified'] = 1;
            
                $itemBatch = ItemBatchRepository::findByPk($dbCountAdjDtlData['item_batch_id']);
                if(!empty($itemBatch)                    
                && strcmp($dbCountAdjDtlData['expiry_date'], '1970-01-01') != 0
                && strcmp($dbCountAdjDtlData['expiry_date'], '0000-00-00') != 0)
                {
                    if(strcmp($itemBatch->expiry_date, $dbCountAdjDtlData['expiry_date']) != 0)
                    {
                        $dbCountAdjDtlData['item_batch_id'] = 0;
                    }
                }

                unset($countAdjDtlDataHashByLineNo[$dbCountAdjDtlData['line_no']]);

                $allCountAdjDtlArray[] = $dbCountAdjDtlData;
            }
            else
            {
                //this line no not exist, should be deleted according to excel file
                $delCountAdjDtlArray[] = $dbCountAdjDtlData;
            }
        }

        //loop the remaining excel details, and create the detail
        foreach($countAdjDtlDataHashByLineNo as $lineNo => $cCountAdjDtlData)
        {
            //assign temp id
            $tmpUuid = uniqid();
            //append NEW here to make sure it will be insert in repository later
            $cCountAdjDtlData['id'] = 'NEW'.$tmpUuid;
            $cCountAdjDtlData['is_modified'] = 1;
            
            $cCountAdjDtlData = $this->initCountAdjDtlData($cCountAdjDtlData);
            
            $allCountAdjDtlArray[] = $cCountAdjDtlData;
        }

        $result = CountAdjHdrRepository::updateDetails($countAdjHdrData, $allCountAdjDtlArray, $delCountAdjDtlArray);
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('CountAdj.file_successfully_uploaded', ['total'=>$countAdjDetailsImport->getTotal()]);

        return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>$delCountAdjDtlArray
            ),
			'message' => $message
		);
    }
}
