<?php

namespace App\Services;

use App\BatchJobStatus;
use App\Services\Env\ProcType;
use App\Repositories\PickFaceStrategyRepository;
use App\Repositories\SiteFlowRepository;
use App\Repositories\BatchJobStatusRepository;
use App\Imports\PickFaceStrategyExcel01Import;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class PickFaceStrategyService
{
    public function __construct()
    {
    }
    
    public function uploadProcess($strProcType, $siteFlowId, $file)
    {
        $user = Auth::user();

        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['PICK_FACE_STRATEGY_EXCEL_01']) 
			{
                //process PICK_FACE_STRATEGY excel
                $path = Storage::putFileAs('upload/', $file, 'PICK_FACE_STRATEGY_EXCEL_01.XLSX');
				return $this->uploadPickFaceStrategyExcel01($siteFlowId, $user->id, $path);
			}
		}
    }

    public function uploadPickFaceStrategyExcel01($siteFlowId, $userId, $path)
    {
        AuthService::authorize(
            array(
                'pick_face_strategy_import'
            )
        );

        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['PICK_FACE_STRATEGY_EXCEL_01'], $userId);

        $siteFlow = SiteFlowRepository::findByPk($siteFlowId);

        //count total first
        $pickFaceStrategyExcel01Import = new PickFaceStrategyExcel01Import($siteFlow->site_id, $userId, $batchJobStatusModel);
        $excel = Excel::import($pickFaceStrategyExcel01Import, $path);

        $message = __('PickFaceStrategy.file_successfully_uploaded', ['total'=>$pickFaceStrategyExcel01Import->getTotal()]);

		return array(
			'data' => $pickFaceStrategyExcel01Import->getTotal(),
			'message' => $message
		);
    }

    public function indexProcess($strProcType, $siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['PICK_FACE_STRATEGY_LIST_01']) 
			{
				//pick face listing
				$pickFaceStrategies = $this->indexPickFaceStrategyList01($siteFlowId, $sorts, $filters, $pageSize);
				return $pickFaceStrategies;
			}
		}
	}
	
	protected function indexPickFaceStrategyList01($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
            array(
                'pick_face_strategy_read'
            )
        );

        $siteFlow = SiteFlowRepository::findByPk($siteFlowId);

        $filters[] = array(
            'field' => 'site_id',
            'value' => $siteFlow->site_id
        );

        $pickFaceStrategies = PickFaceStrategyRepository::findAll($sorts, $filters, $pageSize);
        $pickFaceStrategies->load(
            'storageBin', 'item'
        );
		foreach($pickFaceStrategies as $pickFaceStrategy)
		{
            $storageBin = $pickFaceStrategy->storageBin;
            $pickFaceStrategy->storage_bin_code = '';
            if(!empty($storageBin))
            {
                $pickFaceStrategy->storage_bin_code = $storageBin->code;
            }

            $item = $pickFaceStrategy->item;
            $pickFaceStrategy->item_code = '';
            $pickFaceStrategy->item_desc_01 = '';
            $pickFaceStrategy->item_desc_02 = '';
            if(!empty($item))
            {
                $pickFaceStrategy->item_code = $item->code;
                $pickFaceStrategy->item_desc_01 = $item->desc_01;
                $pickFaceStrategy->item_desc_02 = $item->desc_02;
            }

            unset($pickFaceStrategy->storageBin);
            unset($pickFaceStrategy->item);
		}
        return $pickFaceStrategies;
	}
}
