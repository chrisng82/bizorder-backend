<?php

namespace App\Services;

use App\Repositories\CurrencyRepository;
class CurrencyService 
{
    public function __construct() 
	{
    }
    
    public function select2($search, $filters)
    {
        $currencies = CurrencyRepository::select2($search, $filters);
        return $currencies;
    }
}
