<?php

namespace App\Services;

use App\Repositories\ItemGroup01DivisionRepository;

class ItemGroup01DivisionService {
    public function __construct(){ }

    public function reorderBrand($curBrandId, $prevBrandId, $curDivisionId)
    {
        $data = ItemGroup01DivisionRepository::reorderBrand($curBrandId, $prevBrandId, $curDivisionId);
        $message = __('ItemGroup01.brands_successfully_reordered', []);

        return array(
            'data' => $data,
            'message' => $message
        );
    }
}