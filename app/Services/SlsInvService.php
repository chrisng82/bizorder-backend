<?php

namespace App\Services;

use App\SlsInvHdr;
use App\OutbOrdHdr;
use App\PickListHdr;
use App\Division;
use App\DocTxnFlow;
use App\Services\Env\ResType;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Repositories\ItemRepository;
use App\Repositories\SlsInvHdrRepository;
use App\Repositories\SlsInvDtlRepository;
use App\Repositories\DocTxnFlowRepository;
use App\Repositories\DivisionDocNoRepository;
use App\Repositories\PromoDtlRepository;
use App\Repositories\PickListOutbOrdRepository;
use App\Repositories\CreditTermRepository;
use App\Repositories\CartHdrRepository;
use App\Repositories\BatchJobStatusRepository;
use App\Repositories\SyncSettingHdrRepository;
use App\Repositories\SyncSettingDtlRepository;
use App\Services\Utils\ApiException;
use App\Services\Utils\RepositoryUtils;
use App\Services\ItemService;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SlsInvService extends InventoryService
{
	public static $HDR_ESCAPE = array(
		'id',
		'doc_code',
		'proc_type',
		'sls_ord_hdr_id',
		'sls_ord_hdr_code',
		'del_ord_hdr_id',
		'del_ord_hdr_code',
		'sls_inv_hdr_id',
		'sls_inv_hdr_code',
		'sls_rtn_hdr_id',
		'sls_rtn_hdr_code',
		'sls_cn_hdr_id',
		'sls_cn_hdr_code',
		'sign',
		'cur_res_type',
		'doc_date',
		'doc_status',
		'created_at',
		'updated_at'
	);
	
	public static $DTL_ESCAPE = array(
		'id',
		'hdr_id',
		'created_at',
		'updated_at',
		'outb_ord_hdr',
		'item'
  	);

  	public function __construct() 
	{

	}
    
  	public function indexProcess($strProcType) 
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['SLS_INV_01'])
			{
				//outb ord -> sls inv
				$outOrdHdrs = $this->indexSlsInv01();
				return $outOrdHdrs;
			}
			elseif(ProcType::$MAP[$strProcType] == ProcType::$MAP['SLS_INV_02'])
			{
				//DO, PRF -> sales invoice
			}	
		}	
	}

	protected function indexSlsInv01()
	{
		$outbOrdHdrs = OutbOrdHdrRepository::findAllBySlsInvHdrId(0, DocStatus::$MAP['COMPLETE']);
    	return $outbOrdHdrs;
	}
	
	protected function indexSlsInv02()
	{
		
	}

	public function createProcess($strProcType, $hdrId) 
	{
		if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['SLS_INV_01'])
			{
				//outbound order -> invoicing
				$slsInvHdr = $this->createSlsInv01(ProcType::$MAP[$strProcType], $hdrId);
				return $slsInvHdr;
			}
		}
	}

	protected function createSlsInv01($procType, $hdrId)
  	{
		AuthService::authorize(
			array(
				'sls_inv_create'
			)
		);
		
		//outbound order -> invoicing
		$outbOrdHdr = null;

		$outbOrdDtls = OutbOrdDtlRepository::findAllByHdrId($hdrId);
		$outbOrdDtls->load('outbOrdHdr', 'item');
		$lineNo = 1;
		$txnFlowDataList = array();
		foreach($outbOrdDtls as $outbOrdDtl) 
		{
			$outbOrdHdr = $outbOrdDtl->outbOrdHdr;
			$item = $outbOrdDtl->item;

			$dtlData = array();
			foreach ($outbOrdDtl->toArray() as $fieldName => $value) 
			{
				if(in_array($fieldName, self::$DTL_ESCAPE))
				{
					continue;
				}
				$dtlData[$fieldName] = $value;
			}
			$dtlDataList[$lineNo] = $dtlData;
			$lineNo++;
		}

		//build the hdrData
		$hdrData = array();
		$hdrData['outb_ord_hdr_id'] = $outbOrdHdr->id;
		foreach($outbOrdHdr->toArray() as $fieldName => $value)
		{
			if(in_array($fieldName, self::$HDR_ESCAPE))
			{
				continue;
			}
			$hdrData[$fieldName] = $value;
		}
		
		$tmpDocTxnFlowData = array(
			'fr_doc_hdr_type' => \App\OutbOrdHdr::class,
			'fr_doc_hdr_id' => $outbOrdHdr->id,
			'fr_doc_hdr_code' => $outbOrdHdr->doc_code,
			'is_closed' => 1
		);
		$docTxnFlowDataList[] = $tmpDocTxnFlowData;

		$creditTermType = 0;
		$creditTerm = $outbOrdHdr->creditTerm;
		if(!empty($creditTerm))
		{
			$creditTermType = $creditTerm->credit_term_type;
		}
		$divisionDocNo = DivisionDocNoRepository::findDivisionDocNo($outbOrdHdr->division_id, \App\SlsInvHdr::class, $creditTermType);

		$invTxnFlow = InvTxnFlowRepository::findByPk($outbOrdHdr->division_id, $procType);

		//DRAFT
		$slsInvHdr = SlsInvHdrRepository::createProcess($procType, $divisionDocNo->doc_no_id, $hdrData, $dtlDataList, $docTxnFlowDataList);
		if($invTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
		{
			self::transitionToWip($slsInvHdr->id);
		}
		elseif($invTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
		{
			self::transitionToComplete($slsInvHdr->id);
		}

		return $slsInvHdr;
	}

	static public function transitionToComplete($hdrId)
  	{
		AuthService::authorize(
			array(
				'sls_inv_confirm'
			)
		);

		//use transaction to make sure this is latest value
		$hdrModel = SlsInvHdrRepository::txnFindByPk($hdrId);

		//only DRAFT or above can transition to COMPLETE
		if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE']
		|| $hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('SlsInv.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
			$exc->addData(\App\SlsInvHdr::class, $hdrModel->id);
			throw $exc;
		}

		//preliminary checking

		//commit the document
		$hdrModel = SlsInvHdrRepository::commitToComplete($hdrModel->id);
		if(!empty($hdrModel))
		{
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		
		$exc = new ApiException(__('SlsInv.commit_to_complete_error', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\SlsInvHdr::class, $hdrModel->id);
		throw $exc;
	}

	static public function transitionToWip($hdrId)
  	{
		//use transaction to make sure this is latest value
		$hdrModel = SlsInvHdrRepository::txnFindByPk($hdrId);
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('SlsInv.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
			$exc->addData(\App\SlsInvHdr::class, $hdrModel->id);
			throw $exc;
		}

		//preliminary checking

		//commit the document
		$hdrModel = SlsInvHdrRepository::commitToWip($hdrModel->id);
		if(!empty($hdrModel))
		{
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			$isSuccess = true;
		}

		$exc = new ApiException(__('SlsInv.commit_to_wip_error', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\SlsInvHdr::class, $hdrModel->id);
		throw $exc;
	}

	static public function transitionToDraft($hdrId)
  	{
		//use transaction to make sure this is latest value
		$hdrModel = SlsInvHdrRepository::txnFindByPk($hdrId);

		//only WIP or COMPLETE can transition to DRAFT
		if($hdrModel->doc_status == DocStatus::$MAP['WIP'])
		{
			$hdrModel = SlsInvHdrRepository::revertWipToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		elseif($hdrModel->doc_status == DocStatus::$MAP['COMPLETE'])
		{
			AuthService::authorize(
				array(
					'sls_inv_revert'
				)
			);

			$hdrModel = SlsInvHdrRepository::revertCompleteToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		
		$exc = new ApiException(__('SlsInv.doc_status_is_not_wip_or_complete', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\SlsInvHdr::class, $hdrModel->id);
		throw $exc;
	}

	static public function createFrOutbOrd($outbOrdHdr, $outbOrdDtls, $pickListHdr)
	{
		AuthService::authorize(
            array(
                'sls_inv_create'
            )
		);

		$dtlDataList = array();

		$lineNo = 1;
		foreach($outbOrdDtls as $outbOrdDtl) 
		{
			$item = $outbOrdDtl->item;

			$dtlData = array();
			foreach ($outbOrdDtl->toArray() as $fieldName => $value) 
			{
				if(in_array($fieldName, self::$DTL_ESCAPE))
				{
					continue;
				}
				$dtlData[$fieldName] = $value;
			}
			$dtlDataList[$lineNo] = $dtlData;
			$lineNo++;
		}
		
		//build the hdrData
		$hdrData = array();
		foreach($outbOrdHdr->toArray() as $fieldName => $value)
		{
			if(in_array($fieldName, self::$HDR_ESCAPE))
			{
				continue;
			}
			$hdrData[$fieldName] = $value;
		}
		$hdrData['doc_date'] = date('Y-m-d');

		$creditTermType = 0;
		if(!empty($hdrData['credit_term_id']))
		{
			$creditTerm = CreditTermRepository::findByPk($hdrData['credit_term_id']);
			$creditTermType = $creditTerm->credit_term_type;
		}
		$divisionDocNo = DivisionDocNoRepository::findDivisionDocNo($outbOrdHdr->division_id, \App\SlsInvHdr::class, $creditTermType);
		if(empty($divisionDocNo))
		{
			$exc = new ApiException(__('DivisionDocNo.doc_no_not_found', ['divisionId'=>$outbOrdHdr->division_id, 'docType'=>\App\SlsInvHdr::class]));
			//$exc->addData(\App\DivisionDocNo::class, $outbOrdHdr->division_id);
			throw $exc;
		}

		$docTxnFlowData = array(
			'fr_doc_hdr_type' => \App\PickListHdr::class,
			'fr_doc_hdr_id' => $pickListHdr->id,
			'fr_doc_hdr_code' => $pickListHdr->doc_code,
			'is_closed' => 1
		);

		//return associative array, doc_type, doc_no_id, hdr_data, dtl_array, outb_ord_hdr_id
		return array(
			'doc_type' => \App\SlsInvHdr::class,
			'doc_no_id' => $divisionDocNo->doc_no_id,
			'hdr_data' => $hdrData,
			'dtl_array' => $dtlDataList,
			'outb_ord_hdr_id' => $outbOrdHdr->id,
			'doc_txn_flow_data' => $docTxnFlowData
		);
	}

	public function showDetails($hdrId) 
	{
		AuthService::authorize(
			array(
				'sls_inv_read'
			)
		);

		$slsInvDtls = SlsInvDtlRepository::findAllByHdrId($hdrId);
		$slsInvDtls->load(
            'item', 'item.itemUoms', 'item.itemUoms.uom'
        );
        foreach($slsInvDtls as $slsInvDtl)
        {
            $slsInvDtl = self::processOutgoingDetail($slsInvDtl);
        }
        return $slsInvDtls;
    }

	public function updateDetails($hdrId, $dtlArray)
	{
		AuthService::authorize(
			array(
				'sls_inv_update'
			)
		);

		//get the associated outbOrdHdr
		$slsInvHdr = SlsInvHdrRepository::txnFindByPk($hdrId);
		if($slsInvHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('SlsInv.doc_status_is_wip_or_complete', ['docCode'=>$slsInvHdr->doc_code]));
            $exc->addData(\App\SlsInvHdr::class, $slsInvHdr->id);
            throw $exc;
        }
		$slsInvHdrData = $slsInvHdr->toArray();
		$slsInvHdr->load('outbOrdHdr', 'deliveryPoint');
		$deliveryPoint = $slsInvHdr->deliveryPoint;

		//query the slsInvDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$promoUuidArray = array();
		$slsInvDtlArray = array();
		$slsInvDtlModels = SlsInvDtlRepository::findAllByHdrId($hdrId);
		foreach($slsInvDtlModels as $slsInvDtlModel)
		{
			$slsInvDtlData = $slsInvDtlModel->toArray();
			$slsInvDtlData['is_modified'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{
				if($dtlData['id'] == $slsInvDtlData['id'])
				{
					$item = ItemRepository::findByPk($dtlData['item_id']);
					$dtlData = self::processIncomingDetail($dtlData, $item);
					
					$uuid = $slsInvDtlData['promo_uuid'];
					$dtlData = $this->updateSaleItemUom($dtlData, $slsInvHdr->doc_date, 0, $slsInvHdr->currency_id, 0, 0);
					foreach($dtlData as $fieldName => $value)
					{
						$slsInvDtlData[$fieldName] = $value;
					}
					//$slsInvDtlData = $this->clearPromotion($slsInvDtlData);
					$slsInvDtlData['is_modified'] = 1;

					if(!empty($uuid))
					{
						$promoUuidArray[] = $uuid;
					}
					break;
				}
			}
			$slsInvDtlArray[] = $slsInvDtlData;
		}

		//another loop to reset the promotion if this record is updated
		foreach($slsInvDtlArray as $slsInvDtlSeq => $slsInvDtlData)
		{
			foreach($promoUuidArray as $promoUuid)
			{
				if($promoUuid == $slsInvDtlData['promo_uuid'])
				{
					//$slsInvDtlData = $this->clearPromotion($slsInvDtlData);
					$slsInvDtlData['is_modified'] = 1;
					break;
				}
			}
			$slsInvDtlArray[$slsInvDtlSeq] = $slsInvDtlData;
		}
		
		//promotion
		$modifiedDtlArray = $this->processPromotion($slsInvDtlArray, $slsInvHdr->doc_date, $slsInvHdr->division_id, 
		$deliveryPoint->debtor_id, $deliveryPoint->debtor_group_01_id, $deliveryPoint->debtor_group_02_id, $deliveryPoint->debtor_group_03_id, $deliveryPoint->debtor_group_04_id, $deliveryPoint->debtor_group_05_id);
		foreach($slsInvDtlArray as $key => $slsInvDtlData)
		{
			foreach($modifiedDtlArray as $modifiedDtlData)
			{
				if($modifiedDtlData['id'] == $slsInvDtlData['id'])
				{
					foreach($modifiedDtlData as $field => $value)
					{
						$slsInvDtlData[$field] = $value;
					}
					$slsInvDtlData['is_modified'] = 1;
					$slsInvDtlArray[$key] = $slsInvDtlData;
				}
			}
		}

		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($slsInvHdrData, $slsInvDtlArray);
		$slsInvHdrData = $result['hdrData'];
		$slsInvDtlArray = $result['dtlArray'];

		//need to update the associated outbOrdHdr
		$outbOrdHdrData = array();
		$outbOrdDtlArray = array();
		$outbOrdHdr = $slsInvHdr->outbOrdHdr;
		if(!empty($outbOrdHdr))
		{
			if($outbOrdHdr->cur_res_type == ResType::$MAP['SLS_INV'])
			{
				$result = $this->tallyOutbOrd($slsInvHdrData, $slsInvDtlArray);
				$outbOrdHdrData = $result['outbOrdHdrData'];
				$outbOrdDtlArray = $result['outbOrdDtlArray'];
			}
		}
		
		$result = SlsInvHdrRepository::updateDetails($slsInvHdrData, $slsInvDtlArray, array(), $outbOrdHdrData, $outbOrdDtlArray, array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('SlsInv.details_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function createDetail($hdrId, $data)
	{
		AuthService::authorize(
			array(
				'sls_inv_update'
			)
		);

		$slsInvHdr = SlsInvHdrRepository::txnFindByPk($hdrId);
		$slsInvHdrData = $slsInvHdr->toArray();
		$slsInvHdr->load('outbOrdHdr', 'deliveryPoint');
		$deliveryPoint = $slsInvHdr->deliveryPoint;

		$lastLineNo = 0;
		$slsInvDtlArray = array();
		$slsInvDtlModels = SlsInvDtlRepository::findAllByHdrId($hdrId);
		foreach($slsInvDtlModels as $slsInvDtlModel)
		{
			$lastLineNo = $slsInvDtlModel->line_no;
			$slsInvDtlData = $slsInvDtlModel->toArray();
			$slsInvDtlData['is_modified'] = 0;
			$slsInvDtlArray[] = $slsInvDtlData;
		}

		//assign temp id
		$tmpUuid = uniqid();
		//append NEW here to make sure it will be insert in repository later
		$data['id'] = 'NEW'.$tmpUuid;
		$data['line_no'] = $lastLineNo + 1;
		$data['is_modified'] = 1;
		$data = $this->updateSaleItemUom($data, $slsInvHdr['doc_date'], 0, $slsInvHdr['currency_id'], 0, 0);
		$slsInvDtlData = $this->initSlsInvDtlData($data);
		$slsInvDtlArray[] = $slsInvDtlData;

		//promotion
		$modifiedDtlArray = $this->processPromotion($slsInvDtlArray, $slsInvHdr->doc_date, $slsInvHdr->division_id, 
		$deliveryPoint->debtor_id, $deliveryPoint->debtor_group_01_id, $deliveryPoint->debtor_group_02_id, $deliveryPoint->debtor_group_03_id, $deliveryPoint->debtor_group_04_id, $deliveryPoint->debtor_group_05_id);
		foreach($slsInvDtlArray as $key => $slsInvDtlData)
		{
			foreach($modifiedDtlArray as $modifiedDtlData)
			{
				if($modifiedDtlData['id'] == $slsInvDtlData['id'])
				{
					foreach($modifiedDtlData as $field => $value)
					{
						$slsInvDtlData[$field] = $value;
					}
					$slsInvDtlData['is_modified'] = 1;
					$slsInvDtlArray[$key] = $slsInvDtlData;
				}
			}
		}

		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($slsInvHdrData, $slsInvDtlArray);
		$slsInvHdrData = $result['hdrData'];
		$slsInvDtlArray = $result['dtlArray'];		

		//need to update the associated outbOrdHdr
		$outbOrdHdrData = array();
		$outbOrdDtlArray = array();
		$outbOrdHdr = $slsInvHdr->outbOrdHdr;
		if(!empty($outbOrdHdr))
		{
			if($outbOrdHdr->cur_res_type == ResType::$MAP['SLS_INV'])
			{
				$result = $this->tallyOutbOrd($slsInvHdrData, $slsInvDtlArray);
				$outbOrdHdrData = $result['outbOrdHdrData'];
				$outbOrdDtlArray = $result['outbOrdDtlArray'];
			}
		}

		$result = SlsInvHdrRepository::updateDetails($slsInvHdrData, $slsInvDtlArray, array(), $outbOrdHdrData, $outbOrdDtlArray, array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('SlsInv.detail_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}

	public function deleteDetails($hdrId, $dtlArray)
	{
		AuthService::authorize(
			array(
				'sls_inv_update'
			)
		);

		//get the associated outbOrdHdr
		$slsInvHdr = SlsInvHdrRepository::txnFindByPk($hdrId);
		$slsInvHdrData = $slsInvHdr->toArray();
		$slsInvHdr->load('outbOrdHdr', 'deliveryPoint');
		$deliveryPoint = $slsInvHdr->deliveryPoint;

		//query the slsInvDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$promoUuidArray = array();
		$delSlsInvDtlArray = array();
		$slsInvDtlArray = array();
		$slsInvDtlModels = SlsInvDtlRepository::findAllByHdrId($hdrId);
		foreach($slsInvDtlModels as $slsInvDtlModel)
		{
			$slsInvDtlData = $slsInvDtlModel->toArray();
			$slsInvDtlData['is_modified'] = 0;
			$slsInvDtlData['is_deleted'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{
				if($dtlData['id'] == $slsInvDtlData['id'])
				{
					//this is deleted dtl
					$uuid = $slsInvDtlData['promo_uuid'];
					$slsInvDtlData = $this->clearPromotion($slsInvDtlData);
					$slsInvDtlData['is_deleted'] = 1;

					if(!empty($uuid))
					{
						$promoUuidArray[] = $uuid;
					}
					break;
				}
			}
			if($slsInvDtlData['is_deleted'] > 0)
			{
				$delSlsInvDtlArray[] = $slsInvDtlData;
			}
			else
			{
				$slsInvDtlArray[] = $slsInvDtlData;
			}
		}

		//another loop to reset the promotion if this record is updated
		//and update the line no also
		$lineNo = 1;
		foreach($slsInvDtlArray as $slsInvDtlSeq => $slsInvDtlData)
		{
			foreach($promoUuidArray as $promoUuid)
			{
				if($promoUuid == $slsInvDtlData['promo_uuid'])
				{
					$slsInvDtlData = $this->clearPromotion($slsInvDtlData);
					$slsInvDtlData['is_modified'] = 1;
					break;
				}
			}
			$slsInvDtlData['line_no'] = $lineNo++;
			$slsInvDtlArray[$slsInvDtlSeq] = $slsInvDtlData;
		}
		
		//promotion
		$modifiedDtlArray = $this->processPromotion($slsInvDtlArray, $slsInvHdr->doc_date, $slsInvHdr->division_id, 
		$deliveryPoint->debtor_id, $deliveryPoint->debtor_group_01_id, $deliveryPoint->debtor_group_02_id, $deliveryPoint->debtor_group_03_id, $deliveryPoint->debtor_group_04_id, $deliveryPoint->debtor_group_05_id);
		foreach($slsInvDtlArray as $slsInvDtlSeq => $slsInvDtlData)
		{
			foreach($modifiedDtlArray as $modifiedDtlData)
			{
				if($modifiedDtlData['id'] == $slsInvDtlData['id'])
				{
					foreach($modifiedDtlData as $field => $value)
					{
						$slsInvDtlData[$field] = $value;
					}
					$slsInvDtlData['is_modified'] = 1;
					$slsInvDtlArray[$slsInvDtlSeq] = $slsInvDtlData;
					break;
				}
			}
		}

		//calculate details amount, use adaptive rounding tax
		$result = $this->calculateAmount($slsInvHdrData, $slsInvDtlArray, $delSlsInvDtlArray);
		$slsInvHdrData = $result['hdrData'];
		$modifiedDtlArray = $result['dtlArray'];
		foreach($slsInvDtlArray as $slsInvDtlSeq => $slsInvDtlData)
		{
			foreach($modifiedDtlArray as $modifiedDtlData)
			{
				if($modifiedDtlData['id'] == $slsInvDtlData['id'])
				{
					foreach($modifiedDtlData as $field => $value)
					{
						$slsInvDtlData[$field] = $value;
					}
					$slsInvDtlData['is_modified'] = 1;
					$slsInvDtlArray[$slsInvDtlSeq] = $slsInvDtlData;
					break;
				}
			}
		}

		//need to update the associated outbOrdHdr
		$outbOrdHdrData = array();
		$outbOrdDtlArray = array();
		$delOutbOrdDtlArray = array();
		$outbOrdHdr = $slsInvHdr->outbOrdHdr;
		if(!empty($outbOrdHdr))
		{
			if($outbOrdHdr->cur_res_type == ResType::$MAP['SLS_INV'])
			{
				$result = $this->tallyOutbOrd($slsInvHdrData, $slsInvDtlArray, $delSlsInvDtlArray);
				$outbOrdHdrData = $result['outbOrdHdrData'];
				$outbOrdDtlArray = $result['outbOrdDtlArray'];
				$delOutbOrdDtlArray = $result['delOutbOrdDtlArray'];
			}
		}
		
		$result = SlsInvHdrRepository::updateDetails($slsInvHdrData, $slsInvDtlArray, $delSlsInvDtlArray, $outbOrdHdrData, $outbOrdDtlArray, $delOutbOrdDtlArray);
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('SlsInv.details_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>$delSlsInvDtlArray
            ),
			'message' => $message
		);
	}

	public function showHeader($hdrId)
	{
		AuthService::authorize(
			array(
				'sls_inv_read',
				'sls_inv_update'
			)
		);

		$model = SlsInvHdrRepository::findByPk($hdrId);
		if(!empty($model))
        {
            $model = self::processOutgoingHeader($model);
		}
		
		return $model;
	}

	static public function processOutgoingHeader($model, $isShowPrint = false)
	{
		$docFlows = self::processDocFlows($model);
		$model->doc_flows = $docFlows;
		
		if($isShowPrint)
		{
			$printDocTxn = PrintDocTxnRepository::queryPrintDocTxn(SlsInvHdr::class, $model->id);
			$model->print_count = $printDocTxn->print_count;
			$model->first_printed_at = $printDocTxn->first_printed_at;
			$model->last_printed_at = $printDocTxn->last_printed_at;
		}

		$division = $model->division;
        $model->division_code = $division->code;
        $company = $model->company;
        $model->company_code = $company->code;

        $model->str_doc_status = DocStatus::$MAP[$model->doc_status];

        //currency select2
		$initCurrencyOption = array('value'=>0,'label'=>'');
		$model->currency_symbol = '';
        $currency = $model->currency;
        if(!empty($currency))
        {
			$model->currency_symbol = $currency->symbol;
            $initCurrencyOption = array('value'=>$currency->id, 'label'=>$currency->symbol);
        }
        $model->currency_select2 = $initCurrencyOption;

        //creditTerm select2
		$initCreditTermOption = array('value'=>0,'label'=>'');
		$model->credit_term_code = '';
		$model->credit_term_type = 0;
        $creditTerm = $model->creditTerm;
        if(!empty($creditTerm))
        {
			$model->credit_term_code = $creditTerm->code;
			$model->credit_term_type = $creditTerm->credit_term_type;
            $initCreditTermOption = array('value'=>$creditTerm->id, 'label'=>$creditTerm->code);
        }
        $model->credit_term_select2 = $initCreditTermOption;

		//salesman select2
		$initSalesmanOption = array('value'=>0,'label'=>'');
		$model->salesman_code = '';
		$salesman = $model->salesman;
		if(!empty($salesman))
		{
			$model->salesman_code = $salesman->username;
			$initSalesmanOption = array(
				'value'=>$salesman->id,
				'label'=>$salesman->username
			);
		}
		$model->salesman_select2 = $initSalesmanOption;

        //delivery point select2
		$deliveryPoint = $model->deliveryPoint;
		if(!is_null($deliveryPoint)) {
			$model->delivery_point_code = $deliveryPoint->code;
			$model->delivery_point_area_code = $deliveryPoint->area_code;
			$model->delivery_point_company_name_01 = $deliveryPoint->company_name_01;
			$model->delivery_point_company_name_02 = $deliveryPoint->company_name_02;
			$model->delivery_point_unit_no = $deliveryPoint->unit_no;
			$model->delivery_point_building_name = $deliveryPoint->building_name;
			$model->delivery_point_street_name = $deliveryPoint->street_name;
			$model->delivery_point_district_01 = $deliveryPoint->district_01;
			$model->delivery_point_district_02 = $deliveryPoint->district_02;
			$model->delivery_point_postcode = $deliveryPoint->postcode;
			$model->delivery_point_state_name = $deliveryPoint->state_name;
			$model->delivery_point_country_name = $deliveryPoint->country_name;
			$model->delivery_point_attention = $deliveryPoint->attention;
			$model->delivery_point_phone_01 = $deliveryPoint->phone_01;
			$model->delivery_point_phone_02 = $deliveryPoint->phone_02;
			$model->delivery_point_fax_01 = $deliveryPoint->fax_01;
			$model->delivery_point_fax_02 = $deliveryPoint->fax_02;
			$initDeliveryPointOption = array(
				'value'=>$deliveryPoint->id,
				'label'=>$deliveryPoint->code.' '.$deliveryPoint->company_name_01
			);
			$model->delivery_point_select2 = $initDeliveryPointOption;
			
			//debtor
			$debtor = $deliveryPoint->debtor;
			$model->debtor_code = $debtor->code;
			$model->debtor_area_code = $debtor->area_code;
			$model->debtor_company_name_01 = $debtor->company_name_01;
			$model->debtor_company_name_02 = $debtor->company_name_02;
			$model->debtor_unit_no = $debtor->unit_no;
			$model->debtor_building_name = $debtor->building_name;
			$model->debtor_street_name = $debtor->street_name;
			$model->debtor_district_01 = $debtor->district_01;
			$model->debtor_district_02 = $debtor->district_02;
			$model->debtor_postcode = $debtor->postcode;
			$model->debtor_state_name = $debtor->state_name;
			$model->debtor_country_name = $debtor->country_name;
			$model->debtor_attention = $debtor->attention;
			$model->debtor_phone_01 = $debtor->phone_01;
			$model->debtor_phone_02 = $debtor->phone_02;
			$model->debtor_fax_01 = $debtor->fax_01;
			$model->debtor_fax_02 = $debtor->fax_02;
		} else {
			$model->delivery_point_code = '';
			$model->delivery_point_area_code = '';
			$model->delivery_point_company_name_01 = '';
			$model->delivery_point_company_name_02 = '';
			$model->delivery_point_unit_no = '';
			$model->delivery_point_building_name = '';
			$model->delivery_point_street_name = '';
			$model->delivery_point_district_01 = '';
			$model->delivery_point_district_02 = '';
			$model->delivery_point_postcode = '';
			$model->delivery_point_state_name = '';
			$model->delivery_point_country_name = '';
			$model->delivery_point_attention = '';
			$model->delivery_point_phone_01 = '';
			$model->delivery_point_phone_02 = '';
			$model->delivery_point_fax_01 = '';
			$model->delivery_point_fax_02 = '';
			$initDeliveryPointOption = array(
				'value'=>'',
				'label'=>''
			);
			$model->delivery_point_select2 = $initDeliveryPointOption;

			//debtor
			$model->debtor_code = '';
			$model->debtor_area_code = '';
			$model->debtor_company_name_01 = '';
			$model->debtor_company_name_02 = '';
			$model->debtor_unit_no = '';
			$model->debtor_building_name = '';
			$model->debtor_street_name = '';
			$model->debtor_district_01 = '';
			$model->debtor_district_02 = '';
			$model->debtor_postcode = '';
			$model->debtor_state_name = '';
			$model->debtor_country_name = '';
			$model->debtor_attention = '';
			$model->debtor_phone_01 = '';
			$model->debtor_phone_02 = '';
			$model->debtor_fax_01 = '';
			$model->debtor_fax_02 = '';
		}
		
		

        $model->hdr_disc_val_01 = 0;
        $model->hdr_disc_perc_01 = 0;
        $model->hdr_disc_val_02 = 0;
        $model->hdr_disc_perc_02 = 0;
        $model->hdr_disc_val_03 = 0;
        $model->hdr_disc_perc_03 = 0;
        $model->hdr_disc_val_04 = 0;
        $model->hdr_disc_perc_04 = 0;
        $model->hdr_disc_val_05 = 0;
        $model->hdr_disc_perc_05 = 0;
        $firstDtlModel = SlsInvDtlRepository::findTopByHdrId($model->id);
        if(!empty($firstDtlModel))
        {
            $model->hdr_disc_val_01 = $firstDtlModel->hdr_disc_val_01;
            $model->hdr_disc_perc_01 = $firstDtlModel->hdr_disc_perc_01;
            $model->hdr_disc_val_02 = $firstDtlModel->hdr_disc_val_02;
            $model->hdr_disc_perc_02 = $firstDtlModel->hdr_disc_perc_02;
            $model->hdr_disc_val_03 = $firstDtlModel->hdr_disc_val_03;
            $model->hdr_disc_perc_03 = $firstDtlModel->hdr_disc_perc_03;
            $model->hdr_disc_val_04 = $firstDtlModel->hdr_disc_val_04;
            $model->hdr_disc_perc_04 = $firstDtlModel->hdr_disc_perc_04;
            $model->hdr_disc_val_05 = $firstDtlModel->hdr_disc_val_05;
            $model->hdr_disc_perc_05 = $firstDtlModel->hdr_disc_perc_05;
		}
		
		//pickListOutbOrds
		$model->sls_ord_hdr_doc_code = '';
		$model->pick_list_hdr_doc_code = '';
		$model->pick_list_hdr_doc_date = '1970-01-01';
		$pickListOutbOrds = PickListOutbOrdRepository::findAllCompletePickListByOutbOrdHdrId($model->outb_ord_hdr_id);
		foreach($pickListOutbOrds as $pickListOutbOrd)
		{
			$model->sls_ord_hdr_doc_code = $pickListOutbOrd->sls_ord_hdr_doc_code;
			$model->pick_list_hdr_doc_code = $pickListOutbOrd->pick_list_hdr_doc_code;
			$model->pick_list_hdr_doc_date = $pickListOutbOrd->pick_list_hdr_doc_date;
			break;
		}

		$model->net_amt_english = InventoryService::numberToWords($model->net_amt);

        return $model;
	}

	static public function formatDocuments($slsInvHdrs, $isShowPrint = false)
	{
		AuthService::authorize(
            array(
                'sls_inv_print'
            )
		);

		foreach($slsInvHdrs as $slsInvHdr)
		{
			$slsInvHdr = self::processOutgoingHeader($slsInvHdr);

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletQty = 0;
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			$ttlDtlDiscAmt = 0;
			//query the docDtls
			$slsInvDtls = $slsInvHdr->slsInvDtls;
			$slsInvDtls = $slsInvDtls->sortBy('line_no');

			$formatSlsInvDtls = array();
			foreach($slsInvDtls as $slsInvDtl)
			{
				$item = $slsInvDtl->item;
				
				$slsInvDtl = self::processOutgoingDetail($slsInvDtl);
				$priceDiscAmt = bcmul($slsInvDtl->price_disc, $slsInvDtl->qty);
				$slsInvDtl->ttl_dtl_disc_amt = bcadd($priceDiscAmt, $slsInvDtl->dtl_disc_amt, 8);

				$caseQty = bcadd($caseQty, $slsInvDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $slsInvDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $slsInvDtl->cubic_meter, 8);
				$ttlDtlDiscAmt = bcadd($ttlDtlDiscAmt, $slsInvDtl->ttl_dtl_disc_amt, 8);

				unset($slsInvDtl->item);
				$formatSlsInvDtls[] = $slsInvDtl;
			}

			$slsInvHdr->case_qty = $caseQty;
			$slsInvHdr->gross_weight = $grossWeight;
			$slsInvHdr->cubic_meter = $cubicMeter;
			$slsInvHdr->ttl_dtl_disc_amt = $ttlDtlDiscAmt;

			$slsInvHdr->details = $formatSlsInvDtls;
		}
		return $slsInvHdrs;
	}

	static public function processOutgoingDetail($model)
    {
        $item = $model->item;
        $model->item_code = $item->code;
        //$model->item_desc_01 = $item->desc_01;
        //$model->item_desc_02 = $item->desc_02;
        //item select2
        $initItemOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($item))
        {
            $initItemOption = array(
                'value'=>$item->id, 
                'label'=>$item->code.' '.$item->desc_01,
            );
        }
		$model->item_select2 = $initItemOption;
		
		//calculate the pallet qty, case qty, gross weight, and m3
        $model = ItemService::processCaseLoose($model, $item);

		$model->uom_code = '';
        $uom = $model->uom;
        //uom select2
        $initUomOption = array('value'=>0, 'label'=>'');
        if(!empty($uom))
        {
			$model->uom_code = $uom->code;
            $initUomOption = array('value'=>$uom->id,'label'=>$uom->code);
        }        
		$model->uom_select2 = $initUomOption;
		
		//location
		$location = $model->location;
		$model->location_code = '';
		$initLocationOption = array('value'=>0, 'label'=>'');
        if(!empty($location))
        {
			$model->location_code = $location->code;
            $initLocationOption = array('value'=>$location->id,'label'=>$location->code);
        }        
		$model->location_select2 = $initLocationOption;

        return $model;
	}
	
	public function syncProcess($strProcType, $divisionId, $startDate, $endDate)
    {		
		$user = Auth::user();

        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['SLS_INV_SYNC_01']) 
			{
				//sync sales order from EfiChain
				return $this->syncSlsInvSync01($divisionId, $user->id, $startDate, $endDate);
            }
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['SLS_INV_SYNC_02']) 
			{
				//sync sales invoice from EfiChain
				return $this->syncSlsInvSync02($divisionId, $user->id, $startDate, $endDate);
            }
		}
    }

    protected function syncSlsInvSync01($divisionId, $userId, $startDate, $endDate)
    {
		AuthService::authorize(
			array(
				'sls_inv_export'
			)
		);

        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['SLS_INV_SYNC_01'], $userId);

        $syncSettingHdr = SyncSettingHdrRepository::findByProcTypeAndDivisionId(ProcType::$MAP['SYNC_EFICHAIN_01'], $divisionId);
        if(empty($syncSettingHdr))
        {
            $exc = new ApiException(__('SlsInvs.sync_setting_not_found', ['divisionId'=>$divisionId]));
            //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
            throw $exc;
		}
		
		//query the completed pickList associated outbOrdHdrs from DB

		//DB::connection()->enableQueryLog();
		$slsInvHdrs = SlsInvHdrRepository::findAllNotExistSlsInvSync01Txn($divisionId, array(), array(), 20);
		//dd(DB::getQueryLog());
		$postSlsInvs = array();
		foreach($slsInvHdrs as $slsInvHdr)
		{
			//this code will use transaction to make sure this document is not duplicate for this process
			$tmpSlsInvSync01DocTxn = DocTxnFlowRepository::txnFindByProcTypeAndFrHdrId(
				ProcType::$MAP['SLS_INV_SYNC_01'], 
				\App\SlsInvHdr::class, 
				$slsInvHdr->id
			);
			if(!empty($tmpSlsInvSync01DocTxn))
			{
				continue;
			}

			$slsInvHdr = self::processOutgoingHeader($slsInvHdr);

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletQty = 0;
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$slsInvDtls = $slsInvHdr->slsInvDtls;
			$slsInvDtls = $slsInvDtls->sortBy('line_no');

			$formatSlsInvDtls = array();
			foreach($slsInvDtls as $slsInvDtl)
			{
				$item = $slsInvDtl->item;
				
				$slsInvDtl = self::processOutgoingDetail($slsInvDtl);

				$caseQty = bcadd($caseQty, $slsInvDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $slsInvDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $slsInvDtl->cubic_meter, 8);

				unset($slsInvDtl->item);
				$formatSlsInvDtls[] = $slsInvDtl->toArray();
			}

			$slsInvHdr->case_qty = $caseQty;
			$slsInvHdr->gross_weight = $grossWeight;
			$slsInvHdr->cubic_meter = $cubicMeter;

			$slsInvHdr->details = $formatSlsInvDtls;

			$hdrData = $slsInvHdr->toArray();
			$postSlsInvs[] = $hdrData;
		}

        $client = new Client();
        $header = array();

        $usernameSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'username');
        $passwordSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'password');
        $formParams = array(
            'UserLogin[username]' => $usernameSetting->value,
            'UserLogin[password]' => $passwordSetting->value
		);		
		$formParams['SlsInvHdrs'] = $postSlsInvs;

		//Log::error($formParams);
        $url = $syncSettingHdr->url.'/index.php?r=luuWu/postSalesInvoices';
            
		$response = $client->request('POST', $url, array('headers' => $header, 'form_params' => $formParams));
		
		$result = $response->getBody()->getContents();
		$result = json_decode($result, true);
		if(empty($result))
		{
			BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

			$exc = new ApiException(__('SlsInv.fail_to_sync', ['url'=>$url]));
			//$exc->addData(\App\SlsInvDtl::class, $delDtlData['id']);
			throw $exc;
		}
		else
		{
			if($result['success'] === false)
			{
				BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

				$exc = new ApiException(__('SlsInv.fail_to_sync', ['url'=>$url,'message'=>$result['message']]));
				//$exc->addData(\App\SlsInvDtl::class, $delDtlData['id']);
				throw $exc;
			}
			else
			{
				//mark the slsInv as success
				$docTxnFlowDataList = array();
				foreach($result['data'] as $successSlsInv)
				{
					$tmpDocTxnFlowData = array(
						'fr_doc_hdr_type' => \App\SlsInvHdr::class,
						'fr_doc_hdr_id' => $successSlsInv['id'],
						'fr_doc_hdr_code' => $successSlsInv['doc_code'],
						'is_closed' => 1
					);
					$docTxnFlowDataList[] = $tmpDocTxnFlowData;
				}
				DocTxnFlowRepository::createProcess(ProcType::$MAP['SLS_INV_SYNC_01'], $docTxnFlowDataList);
			}
		}

		/*
		$successPickListDocCodes = array();
		for ($a = 0; $a < count($result['data']); $a++)
		{
			$successPickListDocCodes[] = $result['data'][$a];
		}
		*/
		$batchJobStatusModel = BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

		return array(
			'data' => $batchJobStatusModel,
			'message' => $result['message']
		);
	}

	protected function syncSlsInvSync02($divisionId, $userId, $startDate, $endDate)
    {
        // AuthService::authorize(
		// 	array(
		// 		'sls_ord_import'
		// 	)
        // );
        
        $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['SLS_INV_SYNC_02'], $userId);

        $syncSettingHdr = SyncSettingHdrRepository::findByProcTypeAndDivisionId(ProcType::$MAP['SYNC_EFICHAIN_01'], $divisionId);
        if(empty($syncSettingHdr))
        {
            $exc = new ApiException(__('SlsInv.sync_setting_not_found', ['divisionId'=>$divisionId]));
            //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
            throw $exc;
        }

        //use division code 
		$division = Division::where('id', $divisionId)->first();
		$company = $division->company;
        $divUrl = '&division='.$division->code;

        $client = new Client();
        $header = array();

        $page = 1;
        $lastPage = 1;
        $pageSize = 1000;
        $total = 0;

        $usernameSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'username');
        $passwordSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'password');
        $formParams = array(
            'UserLogin[username]' => $usernameSetting->value,
            'UserLogin[password]' => $passwordSetting->value
        );
        $pageSizeSetting = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdr->id, 'page_size');
        if(!empty($pageSizeSetting))
        {
            $pageSize = $pageSizeSetting->value;
        }

		//&start_created_date=2019-07-01&end_created_date=2019-08-31
		$startDateParam = '&start_date='.$startDate;
		$endDateParam = '&end_date='.$endDate;

        //query from efichain, and update the sales order accordingly
        $slsInvHdrDataList = array();
        while($page <= $lastPage)
        {
            $url = $syncSettingHdr->url.'/index.php?r=bizOrder/getSalesInvoices&page_size='.$pageSize.'&page='.$page.$divUrl.$startDateParam.$endDateParam;
            
            $response = $client->request('POST', $url, array('headers' => $header, 'form_params' => $formParams));
            $result = $response->getBody()->getContents();
            $result = json_decode($result, true);
            if(empty($result))
            {
                $exc = new ApiException(__('SlsInv.fail_to_sync', ['url'=>$url]));
                //$exc->addData(\App\OutbOrdDtl::class, $delDtlData['id']);
                throw $exc;
            }

            $total = $result['total'];
            for ($a = 0; $a < count($result['data']); $a++)
            {
                $slsInvHdrData = $result['data'][$a];
                //DRAFT
                $slsInvHdrModel = SlsInvHdrRepository::syncSlsInvSync02($company, $division, $slsInvHdrData);

				if (!empty($slsInvHdrModel) && !empty($slsInvHdrModel->slsOrdHdr)) {
					$slsOrdHdr = $slsInvHdrModel->slsOrdHdr;
					$toDocTxnFlows = $slsOrdHdr->toDocTxnFlows;
					$hasDocTxnFlow = false;
					$docTxnFlowsToDeleteArray = array();
					foreach($toDocTxnFlows as $toDocTxnFlow)
					{
						if ($toDocTxnFlow->to_doc_hdr_id == $slsInvHdrModel->id) {
							$hasDocTxnFlow = true;
						}
						if ($toDocTxnFlow->to_doc_hdr_id != $slsInvHdrModel->id) {
							$docTxnFlowsToDeleteArray[] = $toDocTxnFlow;
						}
					}
					if (!$hasDocTxnFlow) {
						$toDocTxnFlowData = array(
							'fr_doc_hdr_type' => \App\SlsOrdHdr::class,
							'fr_doc_hdr_id' => $slsOrdHdr->id,
							'fr_doc_hdr_code' => $slsOrdHdr->doc_code,
							'to_doc_hdr_type' => \App\SlsInvHdr::class,
							'to_doc_hdr_id' => $slsInvHdrModel->id,
							'is_closed' => 1
						);
						
						$docTxnFlowModel = new DocTxnFlow;
						$docTxnFlowModel = RepositoryUtils::dataToModel($docTxnFlowModel, $toDocTxnFlowData);
						$docTxnFlowModel->save();
					}
					foreach ($docTxnFlowsToDeleteArray as $docTxnFlowsToDelete) {
						$docTxnFlowsToDelete->delete();
					}
				}
                
                $statusNumber = 100;
                if($total > 0)
                {
                    $statusNumber = bcmul(bcdiv(($a + ($page - 1) * $pageSize), $total, 8), 100, 2);
                }
                BatchJobStatusRepository::updateStatusNumber($batchJobStatusModel->id, $statusNumber);
            }

            $lastPage = $total % $pageSize == 0 ? ($total / $pageSize) : ($total / $pageSize) + 1;

            $page++;
        }

        $batchJobStatusModel = BatchJobStatusRepository::completeStatus($batchJobStatusModel->id);

        return $batchJobStatusModel;
    }
    

	public function transitionToStatus($hdrId, $strDocStatus)
    {
        $hdrModel = null;
        if(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['WIP'])
        {
            $hdrModel = self::transitionToWip($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['COMPLETE'])
        {
            $hdrModel = self::transitionToComplete($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['DRAFT'])
        {
            $hdrModel = self::transitionToDraft($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['VOID'])
        {
            $hdrModel = self::transitionToVoid($hdrId);
        }
        
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $message = __('SlsInv.document_successfully_transition', ['docCode'=>$documentHeader->doc_code,'strDocStatus'=>$strDocStatus]);
        
        return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>array(),
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}
	
	static public function transitionToVoid($hdrId)
    {
		AuthService::authorize(
			array(
				'sls_inv_revert'
			)
		);

		//use transaction to make sure this is latest value
		$hdrModel = SlsInvHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('SlsInv.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\SlsInvHdr::class, $hdrModel->id);
            throw $exc;
		}

		//revert the document
		$hdrModel = SlsInvHdrRepository::revertDraftToVoid($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('SlsInv.commit_to_void_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\SlsInvHdr::class, $hdrModel->id);
        throw $exc;
	}
	
	public function initHeader($divisionId)
    {
		AuthService::authorize(
			array(
				'sls_inv_create'
			)
		);

        $user = Auth::user();

        $model = new SlsInvHdr();
        $model->doc_flows = array();
        $model->doc_status = DocStatus::$MAP['DRAFT'];
        $model->str_doc_status = DocStatus::$MAP[$model->doc_status];
        $model->doc_code = '';
        $model->ref_code_01 = '';
        $model->ref_code_02 = '';
        $model->doc_date = date('Y-m-d');
        $model->est_del_date = date('Y-m-d');
        $model->desc_01 = '';
        $model->desc_02 = '';

        $division = DivisionRepository::findByPk($divisionId);
        $model->division_id = $divisionId;
        $model->division_code = '';
        $model->site_flow_id = 0;
        $model->company_id = 0;
        $model->company_code = '';
        if(!empty($division))
        {
            $model->division_code = $division->code;
            $model->site_flow_id = $division->site_flow_id;
            $company = $division->company;
            $model->company_id = $company->id;
            $model->company_code = $company->code;
        }

        $model->doc_no_id = 0;
        $docNoIdOptions = array();
        $docNos = DocNoRepository::findAllDivisionDocNo($divisionId, \App\SlsInvHdr::class, $model->doc_date);
        for($a = 0; $a < count($docNos); $a++)
        {
            $docNo = $docNos[$a];
            if($a == 0)
            {
                $model->doc_no_id = $docNo->id;
            }
            $docNoIdOptions[] = array('value'=>$docNo->id, 'label'=>$docNo->latest_code);
        }
        $model->doc_no_id_options = $docNoIdOptions;

        //currency dropdown
        $model->currency_id = 0;
        $model->currency_rate = 1;
        $initCurrencyOption = array('value'=>0,'label'=>'');
        $model->currency_select2 = $initCurrencyOption;

        //creditTerm select2
        $initCreditTermOption = array('value'=>0,'label'=>'');
        $model->credit_term_select2 = $initCreditTermOption;

        //salesman select2
        $initSalesmanOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($user))
        {
            $initSalesmanOption = array(
                'value'=>$user->id, 
                'label'=>$user->username
            );
        }
        $model->salesman_select2 = $initSalesmanOption;

        //delivery point select2
        $model->delivery_point_unit_no = '';
        $model->delivery_point_building_name = '';
        $model->delivery_point_street_name = '';
        $model->delivery_point_district_01 = '';
        $model->delivery_point_district_02 = '';
        $model->delivery_point_postcode = '';
        $model->delivery_point_state_name = '';
        $model->delivery_point_country_name = '';
        $initDeliveryPointOption = array(
            'value'=>0,
            'label'=>''
        );
        $deliveryPoint = DeliveryPointRepository::findTop();
        if(!empty($deliveryPoint))
        {
            $initDeliveryPointOption = array(
                'value'=>$deliveryPoint->id, 
                'label'=>$deliveryPoint->code.' '.$deliveryPoint->company_name_01
            );

            $model->delivery_point_unit_no = $deliveryPoint->unit_no;
            $model->delivery_point_building_name = $deliveryPoint->building_name;
            $model->delivery_point_street_name = $deliveryPoint->street_name;
            $model->delivery_point_district_01 = $deliveryPoint->district_01;
            $model->delivery_point_district_02 = $deliveryPoint->district_02;
            $model->delivery_point_postcode = $deliveryPoint->postcode;
            $model->delivery_point_state_name = $deliveryPoint->state_name;
            $model->delivery_point_country_name = $deliveryPoint->country_name;
        }
        $model->delivery_point_select2 = $initDeliveryPointOption;

        $model->hdr_disc_val_01 = 0;
        $model->hdr_disc_perc_01 = 0;
        $model->hdr_disc_val_02 = 0;
        $model->hdr_disc_perc_02 = 0;
        $model->hdr_disc_val_03 = 0;
        $model->hdr_disc_perc_03 = 0;
        $model->hdr_disc_val_04 = 0;
        $model->hdr_disc_perc_04 = 0;
        $model->hdr_disc_val_05 = 0;
        $model->hdr_disc_perc_05 = 0;

        $model->disc_amt = 0;
        $model->tax_amt = 0;
        $model->round_adj_amt = 0;
        $model->net_amt = 0;

        return $model;
	}

	static public function processIncomingHeader($data, $isClearHdrDiscTax=false)
    {
        if(array_key_exists('doc_flows', $data))
        {
            unset($data['doc_flows']);
        }
        if(array_key_exists('submit_action', $data))
        {
            unset($data['submit_action']);
        }
        //preprocess the select2 and dropDown
        if(array_key_exists('credit_term_select2', $data))
        {
            $data['credit_term_id'] = $data['credit_term_select2']['value'];
            unset($data['credit_term_select2']);
        }
        if(array_key_exists('currency_select2', $data))
        {
            $data['currency_id'] = $data['currency_select2']['value'];
            unset($data['currency_select2']);
        }   
        if(array_key_exists('salesman_select2', $data))
        {
            $data['salesman_id'] = $data['salesman_select2']['value'];
            unset($data['salesman_select2']);
        }
        if(array_key_exists('delivery_point_select2', $data))
        {
            $data['delivery_point_id'] = $data['delivery_point_select2']['value'];
            unset($data['delivery_point_select2']);
        }
        if(array_key_exists('str_doc_status', $data))
        {
            unset($data['str_doc_status']);
        }
        if(array_key_exists('division_code', $data))
        {
            unset($data['division_code']);
        }
        if(array_key_exists('company_code', $data))
        {
            unset($data['company_code']);
        }
        if(array_key_exists('delivery_point_unit_no', $data))
        {
            unset($data['delivery_point_unit_no']);
        }
        if(array_key_exists('delivery_point_building_name', $data))
        {
            unset($data['delivery_point_building_name']);
        }
        if(array_key_exists('delivery_point_street_name', $data))
        {
            unset($data['delivery_point_street_name']);
        }
        if(array_key_exists('delivery_point_district_01', $data))
        {
            unset($data['delivery_point_district_01']);
        }
        if(array_key_exists('delivery_point_district_02', $data))
        {
            unset($data['delivery_point_district_02']);
        }
        if(array_key_exists('delivery_point_postcode', $data))
        {
            unset($data['delivery_point_postcode']);
        }
        if(array_key_exists('delivery_point_state_name', $data))
        {
            unset($data['delivery_point_state_name']);
        }
        if(array_key_exists('delivery_point_country_name', $data))
        {
            unset($data['delivery_point_country_name']);
        }
        if(array_key_exists('division', $data))
        {
            unset($data['division']);
        }
        if(array_key_exists('company', $data))
        {
            unset($data['company']);
        }
        if(array_key_exists('salesman', $data))
        {
            unset($data['salesman']);
        }
        if(array_key_exists('delivery_point', $data))
        {
            unset($data['delivery_point']);
        }

        if($isClearHdrDiscTax)
        {  
            for($a = 1; $a <= 5; $a++)
            {
                if(array_key_exists('hdr_disc_val_0'.$a, $data))
                {
                    unset($data['hdr_disc_val_0'.$a]);
                }
                if(array_key_exists('hdr_disc_perc_0'.$a, $data))
                {
                    unset($data['hdr_disc_perc_0'.$a]);
                }
                if(array_key_exists('hdr_tax_val_0'.$a, $data))
                {
                    unset($data['hdr_tax_val_0'.$a]);
                }
                if(array_key_exists('hdr_tax_perc_0'.$a, $data))
                {
                    unset($data['hdr_tax_perc_0'.$a]);
                }
            }
        }
        return $data;
    }
	
	public function updateHeader($hdrData)
	{
		AuthService::authorize(
			array(
				'sls_inv_update'
			)
		);

        $hdrData = self::processIncomingHeader($hdrData);

		//get the associated inbOrdHdr
        $slsInvHdr = SlsInvHdrRepository::txnFindByPk($hdrData['id']);
        if($slsInvHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('SlsInv.doc_status_is_wip_or_complete', ['docCode'=>$slsInvHdr->doc_code]));
            $exc->addData(\App\SlsInvHdr::class, $slsInvHdr->id);
            throw $exc;
        }
        $slsInvHdrData = $slsInvHdr->toArray();
        //assign hdrData to model
        foreach($hdrData as $field => $value) 
        {
            if(strpos($field, 'hdr_disc_val') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_disc_perc') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_tax_val') !== false)
            {
                continue;
            }
            if(strpos($field, 'hdr_tax_perc') !== false)
            {
                continue;
            }
            $slsInvHdrData[$field] = $value;
        }

		//$slsInvHdr->load('outbOrdHdr', 'deliveryPoint');
        //$deliveryPoint = $slsInvHdr->deliveryPoint;
        
        $slsInvDtlArray = array();

        $outbOrdHdrData = array();
        $outbOrdDtlArray = array();

        //check if hdr disc or hdr tax is set and more than 0
        $needToProcessDetails = false;
        $firstDtlModel = SlsInvDtlRepository::findTopByHdrId($hdrData['id']);
        if(!empty($firstDtlModel))
        {
            $firstDtlData = $firstDtlModel->toArray();
            for($a = 1; $a <= 5; $a++)
            {
                if(array_key_exists('hdr_disc_val_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_disc_val_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_disc_val_0'.$a], $firstDtlData['hdr_disc_val_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
                if(array_key_exists('hdr_disc_perc_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_disc_perc_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_disc_perc_0'.$a], $firstDtlData['hdr_disc_perc_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }

                if(array_key_exists('hdr_tax_val_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_tax_val_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_tax_val_0'.$a], $firstDtlData['hdr_tax_val_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
                if(array_key_exists('hdr_tax_perc_0'.$a, $hdrData)
                && bccomp($hdrData['hdr_tax_perc_0'.$a], 0, 5) > 0
                && bccomp($hdrData['hdr_tax_perc_0'.$a], $firstDtlData['hdr_tax_perc_0'.$a], 5) != 0)
                {
                    $needToProcessDetails = true;
                }
            }
        }

        if($needToProcessDetails)
        {
            //query the slsInvDtlModels from DB
            //format to array, with is_modified field to indicate it is changed
            $slsInvDtlModels = SlsInvDtlRepository::findAllByHdrId($hdrData['id']);
            foreach($slsInvDtlModels as $slsInvDtlModel)
            {
                $slsInvDtlData = $slsInvDtlModel->toArray();
                for($a = 1; $a <= 5; $a++)
                {
                    if(array_key_exists('hdr_disc_val_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_disc_val_0'.$a], 0, 5) > 0)
                    {
                        $slsInvDtlData['hdr_disc_val_0'.$a] = $hdrData['hdr_disc_val_0'.$a];
                    }
                    if(array_key_exists('hdr_disc_perc_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_disc_perc_0'.$a], 0, 5) > 0)
                    {
                        $slsInvDtlData['hdr_disc_perc_0'.$a] = $hdrData['hdr_disc_perc_0'.$a];
                    }

                    if(array_key_exists('hdr_tax_val_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_tax_val_0'.$a], 0, 5) > 0)
                    {
                        $slsInvDtlData['hdr_tax_val_0'.$a] = $hdrData['hdr_tax_val_0'.$a];
                    }
                    if(array_key_exists('hdr_tax_perc_0'.$a, $hdrData)
                    && bccomp($hdrData['hdr_tax_perc_0'.$a], 0, 5) > 0)
                    {
                        $slsInvDtlData['hdr_tax_perc_0'.$a] = $hdrData['hdr_tax_perc_0'.$a];
                    }
                }
                $slsInvDtlData['is_modified'] = 1;
                $slsInvDtlArray[] = $slsInvDtlData;
            }
            
            //calculate details amount, use adaptive rounding tax
            $result = $this->calculateAmount($slsInvHdrData, $slsInvDtlArray);
            $slsInvHdrData = $result['hdrData'];
            $slsInvDtlArray = $result['dtlArray'];

            //need to update the associated outbOrdHdr
            $outbOrdHdr = $slsInvHdr->outbOrdHdr;
            if(!empty($outbOrdHdr))
            {
                if($outbOrdHdr->cur_res_type == ResType::$MAP['SLS_INV'])
                {
                    $result = $this->tallyOutbOrd($slsInvHdrData, $slsInvDtlArray);
                    $outbOrdHdrData = $result['outbOrdHdrData'];
                    $outbOrdDtlArray = $result['outbOrdDtlArray'];
                }
            }
        }
            
        $result = SlsInvHdrRepository::updateDetails($slsInvHdrData, $slsInvDtlArray, array(), $outbOrdHdrData, $outbOrdDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];

        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('SlsInv.document_successfully_updated', ['docCode'=>$documentHeader->doc_code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
	}
	
	public function createHeader($data)
    {		
		AuthService::authorize(
			array(
				'sls_inv_create'
			)
		);

        $data = self::processIncomingHeader($data, true);
        $docNoId = $data['doc_no_id'];
        unset($data['doc_no_id']);
        $hdrModel = SlsInvHdrRepository::createHeader(ProcType::$MAP['NULL'], $docNoId, $data);

        $message = __('SlsInv.document_successfully_created', ['docCode'=>$hdrModel->doc_code]);

		return array(
			'data' => $hdrModel->id,
			'message' => $message
		);
	}
	
	public function index($divisionId, $sorts, $filters = array(), $pageSize = 20)
	{
		AuthService::authorize(
			array(
				'sls_inv_read'
			)
		);

        //DB::connection()->enableQueryLog();
        $slsInvHdrs = SlsInvHdrRepository::findAll($divisionId, $sorts, $filters, $pageSize);
        $slsInvHdrs->load(
            'slsInvDtls'
            //'slsInvDtls.item', 'slsInvDtls.item.itemUoms', 'slsInvDtls.item.itemUoms.uom', 
        );
		foreach($slsInvHdrs as $slsInvHdr)
		{
			$slsInvHdr->str_doc_status = DocStatus::$MAP[$slsInvHdr->doc_status];

			// $deliveryPoint = $slsInvHdr->deliveryPoint;
			// $slsInvHdr->delivery_point_code = $deliveryPoint->code;
			// $slsInvHdr->delivery_point_company_name_01 = $deliveryPoint->company_name_01;
			// $slsInvHdr->delivery_point_area_code = $deliveryPoint->area_code;
			// $area = $deliveryPoint->area;
			// if(!empty($area))
			// {
			// 	$slsInvHdr->delivery_point_area_desc_01 = $area->desc_01;
			// }

			$salesman = $slsInvHdr->salesman;
			$slsInvHdr->salesman_username = $salesman->username;

			$slsInvDtls = $slsInvHdr->slsInvDtls;
			$slsInvHdr->details = $slsInvDtls;
        }
        //Log::error(DB::getQueryLog());
    	return $slsInvHdrs;
	}
	
	static public function processIncomingDetail($data, $item)
    {
        $data['qty'] = round($data['qty'], $item->qty_scale);

        //preprocess the select2 and dropDown
        if(array_key_exists('item_select2', $data))
        {
            $data['item_id'] = $data['item_select2']['value'];
            unset($data['item_select2']);
        }
        if(array_key_exists('uom_select2', $data))
        {
            $data['uom_id'] = $data['uom_select2']['value'];
            unset($data['uom_select2']);
        }
        if(array_key_exists('item_code', $data))
        {
            unset($data['item_code']);
        }
        if(array_key_exists('uom_code', $data))
        {
            unset($data['uom_code']);
        }
        if(array_key_exists('item', $data))
        {
            unset($data['item']);
        }
        if(array_key_exists('uom', $data))
        {
            unset($data['uom']);
		}
		
		if(array_key_exists('item_code', $data))
        {
            unset($data['item_code']);
		}
		if(array_key_exists('item_ref_code_01', $data))
        {
            unset($data['item_ref_code_01']);
		}
		if(array_key_exists('item_desc_01', $data))
        {
            unset($data['item_desc_01']);
		}
		if(array_key_exists('item_desc_02', $data))
        {
            unset($data['item_desc_02']);
		}
		if(array_key_exists('storage_class', $data))
        {
            unset($data['storage_class']);
		}
		if(array_key_exists('item_group_01_code', $data))
        {
            unset($data['item_group_01_code']);
		}
		if(array_key_exists('item_group_02_code', $data))
        {
            unset($data['item_group_02_code']);
		}
		if(array_key_exists('item_group_03_code', $data))
        {
            unset($data['item_group_03_code']);
		}
		if(array_key_exists('item_group_04_code', $data))
        {
            unset($data['item_group_04_code']);
		}
		if(array_key_exists('item_group_05_code', $data))
        {
            unset($data['item_group_05_code']);
		}
		if(array_key_exists('item_cases_per_pallet_length', $data))
        {
            unset($data['item_cases_per_pallet_length']);
		}
		if(array_key_exists('item_cases_per_pallet_width', $data))
        {
            unset($data['item_cases_per_pallet_width']);
		}
		if(array_key_exists('item_no_of_layers', $data))
        {
            unset($data['item_no_of_layers']);
		}

		if(array_key_exists('unit_qty', $data))
        {
            unset($data['unit_qty']);
		}
		if(array_key_exists('item_unit_uom_code', $data))
        {
            unset($data['item_unit_uom_code']);
		}
		if(array_key_exists('loose_qty', $data))
        {
            unset($data['loose_qty']);
		}
		if(array_key_exists('item_loose_uom_code', $data))
        {
            unset($data['item_loose_uom_code']);
        }
        if(array_key_exists('loose_uom_id', $data))
        {
            unset($data['loose_uom_id']);
		}
		if(array_key_exists('loose_uom_rate', $data))
        {
            unset($data['loose_uom_rate']);
		}
		if(array_key_exists('case_qty', $data))
        {
            unset($data['case_qty']);
		}
		if(array_key_exists('case_uom_rate', $data))
        {
            unset($data['case_uom_rate']);
		}
        if(array_key_exists('item_case_uom_code', $data))
        {
            unset($data['item_case_uom_code']);
        }
        if(array_key_exists('uom_code', $data))
        {
            unset($data['uom_code']);
		}
		if(array_key_exists('item_unit_barcode', $data))
        {
            unset($data['item_unit_barcode']);
		}
		if(array_key_exists('item_case_barcode', $data))
        {
            unset($data['item_case_barcode']);
		}
		if(array_key_exists('gross_weight', $data))
        {
            unset($data['gross_weight']);
		}
		if(array_key_exists('cubic_meter', $data))
        {
            unset($data['cubic_meter']);
		}
		
		if(array_key_exists('location_code', $data))
        {
            unset($data['location_code']);
		}
		if(array_key_exists('location', $data))
        {
            unset($data['location']);
		}		
        return $data;
    }
}
