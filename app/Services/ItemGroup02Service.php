<?php

namespace App\Services;

use App\Repositories\ItemGroup02Repository;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;
use App\Services\Utils\SimpleImage;
use Milon\Barcode\DNS2D;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;

class ItemGroup02Service
{
    public function __construct()
    {
    }

    public function select2($search, $filters)
    {
        //DB::connection()->enableQueryLog();
        $itemGroup02s = ItemGroup02Repository::select2($search, $filters);
        //dd(DB::getQueryLog());
        return $itemGroup02s;
    }
	
    public function select2ByDivision($divisionID, $search, $filters)
    {
        //DB::connection()->enableQueryLog();
        $itemGroup01s = ItemGroup02Repository::select2ByDivision($divisionID, $search, $filters);
        //dd(DB::getQueryLog());
        return $itemGroup01s;
    }	

    public function index($sorts, $filters = array(), $pageSize = 20)
	{
        $itemGroup02s = ItemGroup02Repository::findAll($sorts, $filters, $pageSize);
    	return $itemGroup02s;
    }

    public function uploadPhoto($id, $file)
	{
		$data = array();
		$data['id'] = $id;
		$data['desc_01'] = $file->getClientOriginalName();
		$data['desc_02'] = '';
		$data['filename'] = $file->getRealPath();
		$data['blob'] = $file;

		$group = ItemGroup02Repository::savePhoto($data);

        $message = __('ItemGroup02.photo_successfully_uploaded', []);

		return array(
			'data' => $group,
			'message' => $message
		);
    }

    public function deletePhoto($id)
	{
        $group = ItemGroup02Repository::deletePhoto($id);
        $message = __('ItemGroup02.photo_successfully_deleted', []);

		return array(
			'data' => $group,
			'message' => $message
		);
	}
}
