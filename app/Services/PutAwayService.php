<?php

namespace App\Services;

use App\Uom;
use App\HandlingUnit;
use App\StorageStrategy;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Services\Env\RackType;
use App\Services\Env\WhseJobType;
use App\Services\Env\HandlingType;
use App\Services\ItemService;
use App\Repositories\StorageBinRepository;
use App\Repositories\HandlingUnitRepository;
use App\Repositories\ItemRepository;
use App\Repositories\ItemCond01Repository;
use App\Repositories\GdsRcptHdrRepository;
use App\Repositories\GdsRcptDtlRepository;
use App\Repositories\QuantBalTxnRepository;
use App\Repositories\QuantBalAdvTxnRepository;
use App\Repositories\SiteDocNoRepository;
use App\Repositories\WhseTxnFlowRepository;
use App\Repositories\PutAwayHdrRepository;
use App\Repositories\PutAwayDtlRepository;
use App\Repositories\PickFaceStrategyRepository;
use App\Repositories\QuantBalRepository;
use App\Repositories\ItemUomRepository;
use App\Repositories\LocationRepository;
use App\Services\Utils\ApiException;
use App\Services\GdsRcptService;
use Illuminate\Support\Facades\Auth;

class PutAwayService extends InventoryService
{
    public function __construct() 
	{
    }
    
    public function indexProcess($strProcType, $siteFlowId, $strDocStatus, $filters = array(), $pageSize = 20) 
	{
        if(isset(ProcType::$MAP[$strProcType]))
		{
            if(ProcType::$MAP[$strProcType] == ProcType::$MAP['PUT_AWAY_01']) 
            {
                //GDS_RCPT -> PUT_AWAY
                $gdsRcptHdrs = $this->indexPutAway01($siteFlowId, $strDocStatus, $filters = array(), $pageSize = 20);
                return $gdsRcptHdrs;
            }
        }		
    }

    protected function indexPutAway01($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
			array(
				'gds_rcpt_read'
			)
        );
        
		$gdsRcptHdrs = GdsRcptHdrRepository::findAllNotExistPutAway01Txn($siteFlowId, DocStatus::$MAP['COMPLETE'], $sorts, $filters, $pageSize);
        $gdsRcptHdrs->load(
            'gdsRcptDtls', 
            'gdsRcptDtls.item', 'gdsRcptDtls.item.itemUoms', 'gdsRcptDtls.item.itemUoms.uom', 
            'gdsRcptDtls.toStorageBin', 'gdsRcptDtls.toStorageBin.storageBay', 'gdsRcptDtls.toStorageBin.storageRow',
            'gdsRcptInbOrds', 'gdsRcptInbOrds.inbOrdHdr'
        );
        foreach($gdsRcptHdrs as $gdsRcptHdr)
		{
            $gdsRcptHdr = GdsRcptService::processOutgoingHeader($gdsRcptHdr, false);
            
            //query the inbOrds
			$gdsRcptInbOrds = $gdsRcptHdr->gdsRcptInbOrds;
			for($a = 0; $a < count($gdsRcptInbOrds); $a++)
			{
				$gdsRcptInbOrd = $gdsRcptInbOrds[$a];
				$inbOrdHdr = $gdsRcptInbOrd->inbOrdHdr;
				if($a == 0)
				{
					$gdsRcptHdr->adv_ship_hdr_code = $inbOrdHdr->adv_ship_hdr_code;
					$gdsRcptHdr->ref_code_01 = $inbOrdHdr->ref_code_01;
					$gdsRcptHdr->ref_code_02 = $inbOrdHdr->ref_code_02;
					$gdsRcptHdr->ref_code_03 = $inbOrdHdr->ref_code_03;
					$gdsRcptHdr->ref_code_04 = $inbOrdHdr->ref_code_04;
					$gdsRcptHdr->ref_code_05 = $inbOrdHdr->ref_code_05;
					$gdsRcptHdr->ref_code_06 = $inbOrdHdr->ref_code_06;
					$gdsRcptHdr->ref_code_07 = $inbOrdHdr->ref_code_07;
					$gdsRcptHdr->ref_code_08 = $inbOrdHdr->ref_code_08;
				}
				else
				{
					$gdsRcptHdr->adv_ship_hdr_code .= '/'.$inbOrdHdr->adv_ship_hdr_code;
					$gdsRcptHdr->ref_code_01 .= '/'.$inbOrdHdr->ref_code_01;
					$gdsRcptHdr->ref_code_02 .= '/'.$inbOrdHdr->ref_code_02;
					$gdsRcptHdr->ref_code_03 .= '/'.$inbOrdHdr->ref_code_03;
					$gdsRcptHdr->ref_code_04 .= '/'.$inbOrdHdr->ref_code_04;
					$gdsRcptHdr->ref_code_05 .= '/'.$inbOrdHdr->ref_code_05;
					$gdsRcptHdr->ref_code_06 .= '/'.$inbOrdHdr->ref_code_06;
					$gdsRcptHdr->ref_code_07 .= '/'.$inbOrdHdr->ref_code_07;
					$gdsRcptHdr->ref_code_08 .= '/'.$inbOrdHdr->ref_code_08;
				}
			}

            //calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$gdsRcptDtls = $gdsRcptHdr->gdsRcptDtls;

			foreach($gdsRcptDtls as $gdsRcptDtl)
			{
                $gdsRcptDtl = GdsRcptService::processOutgoingDetail($gdsRcptDtl);

				$caseQty = bcadd($caseQty, $gdsRcptDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $gdsRcptDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $gdsRcptDtl->cubic_meter, 8);

                if($gdsRcptDtl->to_handling_unit_id > 0)
				{
					$palletHashByHandingUnitId[$gdsRcptDtl->to_handling_unit_id] = $gdsRcptDtl->to_handling_unit_id;
				}
				unset($gdsRcptDtl->toStorageBin);
				unset($gdsRcptDtl->item);
			}
			
			$gdsRcptHdr->pallet_qty = count($palletHashByHandingUnitId);
			$gdsRcptHdr->case_qty = $caseQty;
			$gdsRcptHdr->gross_weight = $grossWeight;
			$gdsRcptHdr->cubic_meter = $cubicMeter;

			$gdsRcptHdr->details = $gdsRcptDtls;
        }
        
        return $gdsRcptHdrs;
	}
    
    public function createProcess($strProcType, $hdrIds)
    {
        if(isset(ProcType::$MAP[$strProcType]))
		{
			if(ProcType::$MAP[$strProcType] == ProcType::$MAP['PUT_AWAY_01']) 
            {
                //GDS_RCPT -> PUT_AWAY
                $result = $this->createPutAway01(ProcType::$MAP[$strProcType], $hdrIds);
                return $result;
            }
		}
    }

    protected function createPutAway01($procType, $hdrIds)
    {
        AuthService::authorize(
			array(
				'put_away_create'
			)
        );

        $putAwayHdrs = array();
        //1) 1 Gds Rcpt -> 1 Put Away
        $gdsRcptHdrs = GdsRcptHdrRepository::findAllByHdrIds($hdrIds);
        $gdsRcptHdrs->load('gdsRcptInbOrds');
        //2) loop for each gds rcpt
        foreach($gdsRcptHdrs as $gdsRcptHdr)
        {
            //2.1) variables to store the put away
            $hdrData = array();
            $dtlDataList = array();
            $inbOrdHdrIds = array();
            $docTxnFlowDataList = array();
            $siteFlow = $gdsRcptHdr->siteFlow;
            $lineNo = 1;

            $gdsRcptDtls = GdsRcptDtlRepository::findAllByHdrId($gdsRcptHdr->id);
            foreach($gdsRcptDtls as $gdsRcptDtl)
            {
                if(bccomp($gdsRcptDtl->qty,0,5) == 0)
                {
                    continue;
                }

                //2.2) get the sign (1) toQuantBalTxns of this gdsRcpt
                $toQuantBalTxn = QuantBalTxnRepository::findByDocDtlId(\App\GdsRcptHdr::class, $gdsRcptHdr->id, $gdsRcptDtl->id, 1);
                if(empty($toQuantBalTxn))
                {
                    $exc = new ApiException(__('GdsRcpt.doc_status_is_not_complete', ['docCode'=>$gdsRcptHdr->doc_code]));
                    $exc->addData(\App\GdsRcptHdr::class, $gdsRcptHdr->id);
                    throw $exc;
                }

                $whseJobType = WhseJobType::$MAP['NULL'];
                if($gdsRcptDtl->whse_job_type == WhseJobType::$MAP['GOODS_RECEIPT_PALLET'])
                {
                    $whseJobType = WhseJobType::$MAP['PUT_AWAY_PALLET'];
                }
                elseif($gdsRcptDtl->whse_job_type == WhseJobType::$MAP['GOODS_RECEIPT_LOOSE'])
                {
                    $whseJobType = WhseJobType::$MAP['PUT_AWAY_LOOSE'];
                }

                $dtlData = array(
                    'line_no' => $lineNo,
                    'company_id' => $toQuantBalTxn->company_id,
                    'item_id' => $gdsRcptDtl->item_id,
                    'desc_01' => $gdsRcptDtl->desc_01,
                    'desc_02' => $gdsRcptDtl->desc_02,

                    'item_cond_01_id' => $gdsRcptDtl->item_cond_01_id,
                    'item_cond_02_id' => $gdsRcptDtl->item_cond_02_id,
                    'item_cond_03_id' => $gdsRcptDtl->item_cond_03_id,
                    'item_cond_04_id' => $gdsRcptDtl->item_cond_04_id,
                    'item_cond_05_id' => $gdsRcptDtl->item_cond_05_id,

                    'uom_id' => $gdsRcptDtl->uom_id,
                    'uom_rate' => $gdsRcptDtl->uom_rate,
                    'qty' => $gdsRcptDtl->qty,
                    'quant_bal_id' => $toQuantBalTxn->quant_bal_id,
                    'to_storage_bin_id' => 0,
                    'to_handling_unit_id' => $toQuantBalTxn->handling_unit_id,
                    'whse_job_type' => $whseJobType,
                    'whse_job_code' => ''
                );
                $dtlDataList[$lineNo] = $dtlData;
    
                $lineNo++;
            }

            //2.4) get the picking underlying outbOrdHdrs
			$gdsRcptInbOrds = $gdsRcptHdr->gdsRcptInbOrds;
			foreach($gdsRcptInbOrds as $gdsRcptInbOrd)
			{
				$inbOrdHdrIds[] = $gdsRcptInbOrd->inb_ord_hdr_id;
			}
            
            //2.7) build the hdrData
            $hdrData = array(
                'ref_code_01' => '',
                'ref_code_02' => '',
                'doc_date' => date('Y-m-d'),
                'desc_01' => '',
                'desc_02' => '',
                'site_flow_id' => $siteFlow->id
            );

            $tmpDocTxnFlowData = array(
                'fr_doc_hdr_type' => \App\GdsRcptHdr::class,
                'fr_doc_hdr_id' => $gdsRcptHdr->id,
                'fr_doc_hdr_code' => $gdsRcptHdr->doc_code,
                'is_closed' => 1
            );
            $docTxnFlowDataList[] = $tmpDocTxnFlowData;

            //2.8) get the putAwayHdr doc no
            $siteDocNo = SiteDocNoRepository::findSiteDocNo($siteFlow->site_id, \App\PutAwayHdr::class);
            if(empty($siteDocNo))
            {
                $exc = new ApiException(__('SiteDocNo.doc_no_not_found', ['siteId'=>$siteFlow->site_id, 'docType'=>\App\PutAwayHdr::class]));
                //$exc->addData(\App\SiteDocNo::class, $siteFlow->site_id);
                throw $exc;
            }

            //2.9) query the whseTxnFlow, so know the next step
            $whseTxnFlow = WhseTxnFlowRepository::findByPk($siteFlow->id, $procType);
            
            //2.10) create the putAwayHdr
            $putAwayHdr = PutAwayHdrRepository::createProcess($procType, $siteDocNo->doc_no_id, $hdrData, $dtlDataList, $inbOrdHdrIds, $docTxnFlowDataList);
            
            //2.11) commit putAwayHdr to WIP/Complete according to whseTxnFlow
            if($whseTxnFlow->to_doc_status == DocStatus::$MAP['WIP'])
            {
                $putAwayHdr = self::transitionToWip($putAwayHdr->id);
            }
            elseif($whseTxnFlow->to_doc_status == DocStatus::$MAP['COMPLETE'])
            {
                $putAwayHdr = self::transitionToComplete($putAwayHdr->id);
            }
            
            $putAwayHdrs[] = $putAwayHdr;
        }

        $docCodeMsg = '';
        for ($a = 0; $a < count($putAwayHdrs); $a++)
        {
            $putAwayHdr = $putAwayHdrs[$a];
            if($a == 0)
            {
                $docCodeMsg .= $putAwayHdr->doc_code;
            }
            else
            {
                $docCodeMsg .= ', '.$putAwayHdr->doc_code;
            }
        }
        $message = __('PutAway.document_successfully_created', ['docCode'=>$docCodeMsg]);

        return array(
            'data' => $putAwayHdrs,
            'message' => $message
        );
    }

    static public function transitionToComplete($hdrId)
    {
        AuthService::authorize(
			array(
				'put_away_confirm'
			)
        );

		//use transaction to make sure this is latest value
		$hdrModel = PutAwayHdrRepository::txnFindByPk($hdrId);

		//only DRAFT or above can transition to COMPLETE
		if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE']
		|| $hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
		{
            $exc = new ApiException(__('PutAway.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\PutAwayHdr::class, $hdrModel->id);
            throw $exc;
		}

		//commit the document
		$hdrModel = PutAwayHdrRepository::commitToComplete($hdrModel->id, true);
        if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
            return $hdrModel;
        }
        
		$exc = new ApiException(__('PutAway.commit_to_complete_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\PutAwayHdr::class, $hdrModel->id);
        throw $exc;
	}

    static public function transitionToWip($hdrId)
    {
		//use transaction to make sure this is latest value
        $hdrModel = PutAwayHdrRepository::txnFindByPk($hdrId);
        $siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status >= DocStatus::$MAP['WIP'])
		{
            $exc = new ApiException(__('PutAway.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\PutAwayHdr::class, $hdrModel->id);
            throw $exc;
        }

        //find all storageBin for performance issue
        $sorts = array(
            array('field'=>'code','order'=>'ASC'),
        );
        $allStorageBins = StorageBinRepository::findAll($sorts, array(), 0);
        $allStorageBins->load('storageType');
        
        //format putAwayDtls to putAwayDtlsHashByHandlingUnitId
		$putAwayDtlsHashByHandlingUnitId = array();
        $putAwayDtls = PutAwayDtlRepository::findAllByHdrId($hdrModel->id);
        $putAwayDtls->load('putAwayHdr', 'item');
		foreach($putAwayDtls as $putAwayDtl)
		{
            if($putAwayDtl->to_storage_bin_id > 0)
            {
                //skip if this put away already assign storage_bin
                continue;
            }

            $tmpPutAwayDtls = array();
			$key = $putAwayDtl->to_handling_unit_id;
			if(array_key_exists($key, $putAwayDtlsHashByHandlingUnitId))
			{
				$tmpPutAwayDtls = $putAwayDtlsHashByHandlingUnitId[$key];
			}
			$tmpPutAwayDtls[] = $putAwayDtl;

			$putAwayDtlsHashByHandlingUnitId[$key] = $tmpPutAwayDtls;
		}

		foreach($putAwayDtlsHashByHandlingUnitId as $key => $putAwayDtls)
		{
			$handlingUnitId = $key;
            
            if($handlingUnitId > 0)
            {

                self::planHandlingUnitPutAway($allStorageBins, $siteFlow, $handlingUnitId, $putAwayDtls);
            }
            else
            {
                foreach($putAwayDtls as $putAwayDtl)
                {
                    self::planLoosePutAway($allStorageBins, $siteFlow, $putAwayDtl);
                }                
            }
        }
        
        //commit the document
		$hdrModel = PutAwayHdrRepository::commitToWip($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
            return $hdrModel;
		}

		$exc = new ApiException(__('PutAway.commit_to_wip_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\PutAwayHdr::class, $hdrModel->id);
        throw $exc;
    }

    static public function transitionToDraft($hdrId)
    {
		//use transaction to make sure this is latest value
		$hdrModel = PutAwayHdrRepository::txnFindByPk($hdrId);

		//only WIP or COMPLETE can transition to DRAFT
		if($hdrModel->doc_status == DocStatus::$MAP['WIP'])
		{
            $hdrModel = PutAwayHdrRepository::revertWipToDraft($hdrModel->id);
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
		elseif($hdrModel->doc_status == DocStatus::$MAP['COMPLETE'])
		{
            AuthService::authorize(
                array(
                    'put_away_revert'
                )
            );

            $hdrModel = PutAwayHdrRepository::revertCompleteToDraft($hdrModel->id);
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
            return $hdrModel;
        }
        elseif($hdrModel->doc_status == DocStatus::$MAP['VOID'])
		{
            AuthService::authorize(
                array(
                    'put_away_confirm'
                )
            );

			$hdrModel = PutAwayHdrRepository::commitVoidToDraft($hdrModel->id);
			$hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}
        
        $exc = new ApiException(__('PutAway.doc_status_is_not_wip_or_complete', ['docCode'=>$hdrModel->doc_code]));
		$exc->addData(\App\PutAwayHdr::class, $hdrModel->id);
		throw $exc;
    }
    
    static protected function planHandlingUnitPutAway($allStorageBins, $siteFlow, $handlingUnitId, $putAwayDtls)
	{
        $handlingUnit = HandlingUnitRepository::findByPk($handlingUnitId);

        //1) find out the earliest and latest expiry date for quants and process into hashtable
        $huEarliestExpiryDate = date('Y-m-d');
        $huLatestExpiryDate = date('Y-m-d', 0);
        $putAwayDtlsHashByCompanyIdItemId = array();
        foreach($putAwayDtls as $putAwayDtl)
        {
            $quantBal = $putAwayDtl->quantBal;
            if(strtotime($quantBal->itemBatch->expiry_date) < strtotime($huEarliestExpiryDate))
            {
                $huEarliestExpiryDate = $quantBal->itemBatch->expiry_date;
            }
            if(strtotime($quantBal->itemBatch->expiry_date) > strtotime($huLatestExpiryDate))
            {
                $huLatestExpiryDate = $quantBal->itemBatch->expiry_date;
            }

            $key = $putAwayDtl->company_id.'/'.$putAwayDtl->item_id;
			if(array_key_exists($key, $putAwayDtlsHashByCompanyIdItemId))
			{
				$tmpPutAwayDtls = $putAwayDtlsHashByCompanyIdItemId[$key];
			}
			$tmpPutAwayDtls[] = $putAwayDtl;

			$putAwayDtlsHashByCompanyIdItemId[$key] = $tmpPutAwayDtls;
        }

        //2) summarise the storage strategy and quants
        $allPickFaceStrategies = array();
        foreach($putAwayDtlsHashByCompanyIdItemId as $key => $putAwayDtls)
        {
            $keyParts = explode('/',$key);
			$companyId = $keyParts[0];
            $itemId = $keyParts[1];

            $pickFaceStrategies = PickFaceStrategyRepository::findAllByItemId(
                $siteFlow->site_id, $itemId);

            foreach($pickFaceStrategies as $pickFaceStrategy)
            {
                $allPickFaceStrategies[] = $pickFaceStrategy;
            }
        }

        $defaultBaySequence = 0;
        $locationIds = array();
        $storageTypeIds = array();
        $storageRowIds = array();
        $storageBayIds = array();
        $storageBinIds = array();
        if(count($allPickFaceStrategies) > 0)
        {
            foreach($allPickFaceStrategies as $pickFaceStrategy)
            {
                $storageBin = $pickFaceStrategy->storageBin;

                $defaultBaySequence = $storageBin->bay_sequence;
                $locationIds[] = $storageBin->location_id;
                $storageRowIds[] = $storageBin->storage_row_id;
            }
        }
        else
        {
            //3) find out the existing handlingUnit (item) bay sequence, build a bay sequence histogram
            $baySequenceCountHash = array();
            foreach($putAwayDtlsHashByCompanyIdItemId as $key => $putAwayDtls)
            {
                $keyParts = explode('/',$key);
                $companyId = $keyParts[0];
                $itemId = $keyParts[1];

                $quantBals = QuantBalRepository::findAllActiveByPickFaceAndPallet($siteFlow->site_id, $companyId, $itemId);
                foreach($quantBals as $quantBal)
                {
                    $baySequenceCount = 0;
                    if(array_key_exists($quantBal->bay_sequence, $baySequenceCountHash))
                    {
                        $baySequenceCount = $baySequenceCountHash[$quantBal->bay_sequence];
                    }
                    $baySequenceCount++;

                    $baySequenceCountHash[$quantBal->bay_sequence] = $baySequenceCount;
                }
            }

            //4) sort bay sequence histogram by highest count
            arsort($baySequenceCountHash);
            foreach($baySequenceCountHash as $baySequence => $count)
            {
                $defaultBaySequence = $baySequence;
                break;
            }
        }

        //7) get the list of storageBin by storage strategy, order by bay sequence, level, the nearest to existing item storage bin will be top
        $storageBins = StorageBinRepository::filterByStrategy($allStorageBins, $siteFlow->site_id, $locationIds, $storageTypeIds, $storageRowIds, $storageBayIds, $storageBinIds, $defaultBaySequence);
        foreach($storageBins as $storageBin)
        {
            //7.1) get the storageBinType
            $storageBinType = $storageBin->storageType;

            if($handlingUnit->handling_type != $storageBinType->handling_type)
            {
                //skip this bin if handlingUnit type is not the same with storage bin
                continue;
            }

            //7.5) check the storage bin rackType, make sure follow FILO or FIFO
            if($storageBinType->rack_type == RackType::$MAP['FILO'])
            {
                $expiryDate = QuantBalRepository::queryHandlingUnitEarliestExpiryDate($storageBin->id, $handlingUnit->id);
                if(strtotime($huEarliestExpiryDate) > strtotime($expiryDate))
                {
                    //skip this bin if the handling unit expiry date is later than existing quants expiry date
                    continue;
                }
            }
            elseif($storageBinType->rack_type == RackType::$MAP['FIFO'])
            {
                $expiryDate = QuantBalRepository::queryHandlingUnitEarliestExpiryDate($storageBin->id, $handlingUnit->id);
                if(strtotime($huEarliestExpiryDate) < strtotime($expiryDate))
                {
                    //skip this bin if the handling unit expiry date is earlier than existing quants expiry date
                    continue;
                }
            }

            //7.6) assign the planning if all conditions pass
            $isHandlingUnitAssigned = self::assignStorageBin($siteFlow, $handlingUnit, $putAwayDtls, $storageBin, $storageBinType->hu_max_count);
            if($isHandlingUnitAssigned === true)
            {
                //7.6.1) break the loop if the assignation is success
                break;
            }
        }        
    }

    static protected function assignStorageBin($siteFlow, $handlingUnit, $putAwayDtls, $storageBin, $huMaxCount)
	{
        if($handlingUnit->handling_type == HandlingType::$MAP['PALLET'])
        {
            $tmpIsSuccess = true;
            foreach($putAwayDtls as $putAwayDtl)
            {
                $tmpIsSuccess = QuantBalRepository::putAwayDtlAssign($siteFlow->site_id, WhseJobType::$MAP['PUT_AWAY_PALLET'], $putAwayDtl->id, $putAwayDtl->putAwayHdr, $putAwayDtl->item, $handlingUnit, $storageBin->id, $huMaxCount);
            }
            return $tmpIsSuccess;
        }
        elseif($handlingUnit->handling_type == HandlingType::$MAP['CASE'])
        {
            $tmpIsSuccess = true;
            foreach($putAwayDtls as $putAwayDtl)
            {
                $tmpIsSuccess = QuantBalRepository::putAwayDtlAssign($siteFlow->site_id, WhseJobType::$MAP['PUT_AWAY_CASE'], $putAwayDtl->id, $putAwayDtl->putAwayHdr, $putAwayDtl->item, $handlingUnit, $storageBin->id, $huMaxCount);
            }
            return $tmpIsSuccess;
        }
        else
        {
            $tmpIsSuccess = true;
            foreach($putAwayDtls as $putAwayDtl)
            {
                $tmpIsSuccess = QuantBalRepository::putAwayDtlAssign($siteFlow->site_id, WhseJobType::$MAP['PUT_AWAY_LOOSE'], $putAwayDtl->id, $putAwayDtl->putAwayHdr, $putAwayDtl->item, $handlingUnit, $storageBin->id, $huMaxCount);
            }
            return $tmpIsSuccess;
        }
		return false;
    }

    static protected function planLoosePutAway($allStorageBins, $siteFlow, $putAwayDtl)
	{
        //1) initialize a fake LOOSE handlingUnit
        $handlingUnit = new HandlingUnit;
        $handlingUnit->id = 0;
        $handlingUnit->handling_type = HandlingType::$MAP['LOOSE'];

        //2) summarise the storage strategy and quants
        $locationIds = array();
        $storageTypeIds = array();
        $storageRowIds = array();
        $storageBayIds = array();
        $storageBinIds = array();
        $item = $putAwayDtl->item;

        $pickFaceStrategies = PickFaceStrategyRepository::findAllByItemId(
            $siteFlow->site_id, $item->id);
        if(count($pickFaceStrategies) > 0)
        {
            foreach($pickFaceStrategies as $pickFaceStrategy)
            {
                $storageBin = $pickFaceStrategy->storageBin;

                $defaultBaySequence = $storageBin->bay_sequence;
                $locationIds[] = $storageBin->location_id;
                $storageRowIds[] = $storageBin->storage_row_id;
            }
        }
        else
        {
            //3) find out the existing handlingUnit (item) bay sequence, build a bay sequence histogram
            $baySequenceCountHash = array();
            $quantBals = QuantBalRepository::findAllActiveByPickFaceAndPallet($siteFlow->site_id, $putAwayDtl->company_id, $putAwayDtl->item_id);
            foreach($quantBals as $quantBal)
            {
                $baySequenceCount = 0;
                if(array_key_exists($quantBal->bay_sequence, $baySequenceCountHash))
                {
                    $baySequenceCount = $baySequenceCountHash[$quantBal->bay_sequence];
                }
                $baySequenceCount++;

                $baySequenceCountHash[$quantBal->bay_sequence] = $baySequenceCount;
            }

            //4) sort bay sequence histogram by highest count
            arsort($baySequenceCountHash);
            $defaultBaySequence = 0;
            foreach($baySequenceCountHash as $baySequence => $count)
            {
                $defaultBaySequence = $baySequence;
                break;
            }
        }

        //7) get the list of storageBin by storage strategy, order by bay sequence, the nearest to existing item storage bin will be top
        $storageBins = StorageBinRepository::filterByStrategy($allStorageBins, $siteFlow->site_id, $locationIds, $storageTypeIds, $storageRowIds, $storageBayIds, $storageBinIds, $defaultBaySequence);
        foreach($storageBins as $storageBin)
        {
            //7.1) get the storageBinType
            $storageBinType = $storageBin->storageType;

            if($handlingUnit->handling_type != $storageBinType->handling_type)
            {
                //skip this bin if handlingUnit type is not the same with storage bin
                continue;
            }

            //7.6) assign the planning if all conditions pass
            $isHandlingUnitAssigned = self::assignStorageBin($siteFlow, $handlingUnit, array($putAwayDtl), $storageBin, $storageBinType->hu_max_count);
            if($isHandlingUnitAssigned === true)
            {
                //7.6.1) break the loop if the assignation is success
                break;
            }
        }        
    }

    public function transitionToStatus($hdrId, $strDocStatus)
    {
        $hdrModel = null;
        $documentDetails = array();
        if(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['WIP'])
        {
            $hdrModel = self::transitionToWip($hdrId);
            $dtlModels = PutAwayDtlRepository::findAllByHdrId($hdrId);
            foreach($dtlModels as $dtlModel)
            {
                $documentDetails[] = self::processOutgoingDetail($dtlModel);
            }
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['COMPLETE'])
        {
            $hdrModel = self::transitionToComplete($hdrId);
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['DRAFT'])
        {
            $hdrModel = self::transitionToDraft($hdrId);
            $dtlModels = PutAwayDtlRepository::findAllByHdrId($hdrId);
            foreach($dtlModels as $dtlModel)
            {
                $documentDetails[] = self::processOutgoingDetail($dtlModel);
            }
        }
        elseif(DocStatus::$MAP[$strDocStatus] == DocStatus::$MAP['VOID'])
        {
            $hdrModel = self::transitionToVoid($hdrId);
        }
        
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $message = __('PutAway.document_successfully_transition', ['docCode'=>$documentHeader->doc_code,'strDocStatus'=>$strDocStatus]);
        
        return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }

    public function verifyTxn() 
	{
        $invalidRows = PutAwayHdrRepository::verifyTxn();        
        return $invalidRows;
    }

    static public function processOutgoingHeader($model, $isShowPrint = false)
	{
        $docFlows = self::processDocFlows($model);
        $model->doc_flows = $docFlows;
        
		if($isShowPrint)
		{
			$printDocTxn = PrintDocTxnRepository::queryPrintDocTxn(WhseJobHdr::class, $model->id);
			$model->print_count = $printDocTxn->print_count;
			$model->first_printed_at = $printDocTxn->first_printed_at;
			$model->last_printed_at = $printDocTxn->last_printed_at;
		}

		$model->str_doc_status = DocStatus::$MAP[$model->doc_status];
		return $model;
	}

	static public function processOutgoingDetail($model)
    {
        //relationships:
		$item = $model->item;
		$handlingUnit = $model->handlingUnit;
		$quantBal = $model->quantBal;
        $itemCond01 = $model->itemCond01;
		$uom = $model->uom;
		$toStorageBin = $model->toStorageBin;
		$toHandlingUnit = $model->toHandlingUnit;
		
		$model->str_whse_job_type = WhseJobType::$MAP[$model->whse_job_type];
		$model->str_whse_job_type = __('WhseJob.'.strtolower($model->str_whse_job_type));

        $model->item_cond_01_code = '';
		$model->storage_bin_code = '';
		$model->storage_row_code = '';
		$model->storage_bay_code = '';
		$model->handling_unit_barcode = '';

		$model->batch_serial_no = '';
		$model->expiry_date = '';
		$model->receipt_date = '';

		$model->to_storage_bin_code = '';
		$model->to_storage_row_code = '';
        $model->to_storage_bay_code = '';

		//this whseJobDtl is delivery
        $itemBatch = null;
        $storageBin = null;
		$storageRow = null;
		$storageBay = null;
		if(!empty($quantBal))
		{
			$handlingUnit = $quantBal->handlingUnit;
			$itemBatch = $quantBal->itemBatch;
			$storageBin = $quantBal->storageBin;
			$storageRow = $quantBal->storageRow;
			$storageBay = $quantBal->storageBay;
        }
        if(!empty($storageBin))
		{
			$model->storage_bin_code = $storageBin->code;
		}
		if(!empty($handlingUnit))
		{
			$model->handling_unit_barcode = $handlingUnit->getBarcode();
		}
		if(!empty($storageRow))
		{
			$model->storage_row_code = $storageRow->code;
		}
		$model->item_bay_sequence = 0;
		if(!empty($storageBay))
		{
			$model->storage_bay_code = $storageBay->code;
			$model->item_bay_sequence = $storageBay->bay_sequence;
		}
		if(!empty($itemBatch))
		{
			$model->batch_serial_no = $itemBatch->batch_serial_no;
			$model->expiry_date = $itemBatch->expiry_date;
			$model->receipt_date = $itemBatch->receipt_date;
		}

		//this whseJobDtl is receipt
		if(!empty($toStorageBin))
		{
			$toStorageRow = $toStorageBin->storageRow;
			$toStorageBay = $toStorageBin->storageBay;

			$model->to_storage_bin_code = $toStorageBin->code;
			if(!empty($toStorageRow))
			{
				$model->to_storage_row_code = $toStorageRow->code;
			}
			$model->to_item_bay_sequence = 0;
			if(!empty($toStorageBay))
			{
				$model->to_storage_bay_code = $toStorageBay->code;
				$model->to_item_bay_sequence = $toStorageBay->bay_sequence;
			}
        }
        if(!empty($itemCond01))
        {
            $model->item_cond_01_code = $itemCond01->code;
        }

		//calculate the pallet qty, case qty, gross weight, and m3
		if($model->uom_id == Uom::$PALLET
		&& bccomp($model->uom_rate, 0, 5) == 0)
		{
			//this is pallet picking
			$model->unit_qty = 0;
			$model->case_qty = 0;
			$model->gross_weight = 0;
			$model->cubic_meter = 0;

			$handlingQuantBals = QuantBalRepository::findAllByHandlingUnitId($quantBal->handling_unit_id);
			$handlingQuantBals->load('item');
			foreach($handlingQuantBals as $handlingQuantBal)
			{
				$handlingItem = $handlingQuantBal->item;

				$handlingQuantBal = ItemService::processCaseLoose($handlingQuantBal, $handlingItem, 1);

				$model->item_code = $handlingQuantBal->item_code;
				$model->item_ref_code_01 = $handlingQuantBal->item_ref_code_01;
				$model->item_desc_01 = $handlingQuantBal->item_desc_01;
				$model->item_desc_02 = $handlingQuantBal->item_desc_02;
				$model->storage_class = $handlingQuantBal->storage_class;
				$model->item_group_01_code = $handlingQuantBal->item_group_01_code;
				$model->item_group_02_code = $handlingQuantBal->item_group_02_code;
				$model->item_group_03_code = $handlingQuantBal->item_group_03_code;
				$model->item_group_04_code = $handlingQuantBal->item_group_04_code;
				$model->item_group_05_code = $handlingQuantBal->item_group_05_code;
				$model->item_cases_per_pallet_length = $handlingQuantBal->item_cases_per_pallet_length;
				$model->item_cases_per_pallet_width = $handlingQuantBal->item_cases_per_pallet_width;
				$model->item_no_of_layers = $handlingQuantBal->item_no_of_layers;

				$model->unit_qty = bcadd($model->unit_qty, $handlingQuantBal->balance_unit_qty, 10);
				$model->case_qty = bcadd($model->case_qty, $handlingQuantBal->case_qty, 10);
				$model->gross_weight = bcadd($model->gross_weight, $handlingQuantBal->gross_weight, 10);
				$model->cubic_meter = bcadd($model->cubic_meter, $handlingQuantBal->cubic_meter, 10);
			}
		}
		else
		{
			$item = $model->item;
			$model = ItemService::processCaseLoose($model, $item);
		}

        //storageBin select2
        $initStorageBinOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($storageBin))
        {
            $initStorageBinOption = array('value'=>$storageBin->id,'label'=>$storageBin->code);
        }        
		$model->storage_bin_select2 = $initStorageBinOption;

		$initQuantBalOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($quantBal))
        {
            $label = '';
            if(!empty($model->handling_unit_barcode))
            {
                $label = $model->handling_unit_barcode.' | ';
            }
			$label = $label.$model->item_code;
            $initQuantBalOption = array(
				'value'=>$quantBal->id,
				'label'=>$label
			);
        }        
        $model->quant_bal_select2 = $initQuantBalOption;

        $initItemCond01Option = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($itemCond01))
        {
            $initItemCond01Option = array('value'=>$itemCond01->id,'label'=>$itemCond01->code);
        }        
		$model->item_cond_01_select2 = $initItemCond01Option;

        $initToStorageBinOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($toStorageBin))
        {
            $initToStorageBinOption = array('value'=>$toStorageBin->id,'label'=>$toStorageBin->code);
        }        
        $model->to_storage_bin_select2 = $initToStorageBinOption;
        
        $initUomOption = array(
            'value'=>0,
            'label'=>''
        );
        if(!empty($uom))
        {
            $initUomOption = array('value'=>$uom->id,'label'=>$uom->code);
        }        
		$model->uom_select2 = $initUomOption;
		
		unset($model->item);
		unset($model->storageBin);
		unset($model->handlingUnit);
        unset($model->quantBal);
        unset($model->itemCond01);
		unset($model->uom);
		unset($model->toStorageBin);
        unset($model->toHandlingUnit);

		return $model;
    }

    static public function processIncomingDetail($data)
    {
        //preprocess the select2 and dropDown
        if(array_key_exists('item_select2', $data))
        {
			$select2Id = $data['item_select2']['value'];
			if($select2Id > 0)
			{
				$data['item_id'] = $select2Id;
			}
            unset($data['item_select2']);
        }
        $item = ItemRepository::findByPk($data['item_id']);
        if(empty($item))
		{
			$exc = new ApiException(__('BinTrf.item_is_required', ['lineNo'=>$data['line_no']]));
			//$exc->addData(\App\WhseJobDtl::class, $data['id']);
			throw $exc;
        }
        
		if(array_key_exists('qty', $data)
		&& !empty($item))
		{
			$data['qty'] = round($data['qty'], $item->qty_scale);
        }

        if(array_key_exists('storage_bin_select2', $data))
        {
			$select2Id = $data['storage_bin_select2']['value'];
			if($select2Id > 0)
			{
				$data['storage_bin_id'] = $select2Id;
			}
            unset($data['storage_bin_select2']);
        }
        if(array_key_exists('quant_bal_select2', $data))
        {
			$select2Id = $data['quant_bal_select2']['value'];
			if($select2Id > 0)
			{
				$data['quant_bal_id'] = $select2Id;
			}
            unset($data['quant_bal_select2']);
        }
        if(array_key_exists('item_batch_select2', $data))
        {
            //just unset itemBatch select2, because it will fill correct value in other fields, 
            //expiry_date, receipt_date, when the itemBatch is selected
            unset($data['item_batch_select2']);
        }
        if(array_key_exists('uom_select2', $data))
        {
			$select2Id = $data['uom_select2']['value'];
			if($select2Id > 0)
			{
				$data['uom_id'] = $select2Id;
			}
            unset($data['uom_select2']);
        }
        if(array_key_exists('item_cond_01_select2', $data))
        {
			$select2Id = $data['item_cond_01_select2']['value'];
			if($select2Id > 0)
			{
				$data['item_cond_01_id'] = $select2Id;
			}
            unset($data['item_cond_01_select2']);
		}
        if(array_key_exists('to_storage_bin_select2', $data))
        {
			$select2Id = $data['to_storage_bin_select2']['value'];
			$data['to_storage_bin_id'] = $select2Id;
            unset($data['to_storage_bin_select2']);
		}
		if(array_key_exists('to_handling_unit_select2', $data))
        {
			$select2Id = $data['to_handling_unit_select2']['value'];
			if($select2Id > 0)
			{
				$data['to_handling_unit_id'] = $select2Id;
			}
            unset($data['to_handling_unit_select2']);
        }

        if(array_key_exists('item_cond_01_code', $data))
        {            
            unset($data['item_cond_01_code']);
        }
        if(array_key_exists('storage_bin_code', $data))
        {
            unset($data['storage_bin_code']);
		}
		if(array_key_exists('storage_row_code', $data))
        {
            unset($data['storage_row_code']);
		}
		if(array_key_exists('storage_bay_code', $data))
        {
            unset($data['storage_bay_code']);
		}
		if(array_key_exists('item_bay_sequence', $data))
        {
            unset($data['item_bay_sequence']);
		}
		if(array_key_exists('to_storage_bin_code', $data))
        {
            unset($data['to_storage_bin_code']);
		}
		if(array_key_exists('to_storage_row_code', $data))
        {
            unset($data['to_storage_row_code']);
		}
		if(array_key_exists('to_storage_bay_code', $data))
        {
            unset($data['to_storage_bay_code']);
		}
		if(array_key_exists('to_item_bay_sequence', $data))
        {
            unset($data['to_item_bay_sequence']);
		}

		if(array_key_exists('str_whse_job_type', $data))
        {
            unset($data['str_whse_job_type']);
		}
        
        if(array_key_exists('handling_unit_barcode', $data))
        {
            unset($data['handling_unit_barcode']);
		}
		if(array_key_exists('handling_unit_ref_code_01', $data))
        {
            unset($data['handling_unit_ref_code_01']);
        }
        if(array_key_exists('batch_serial_no', $data))
        {
            unset($data['batch_serial_no']);
        }
        if(array_key_exists('expiry_date', $data))
        {
            unset($data['expiry_date']);
        }
        if(array_key_exists('receipt_date', $data))
        {
            unset($data['receipt_date']);
        }
        if(array_key_exists('item_code', $data))
        {
            unset($data['item_code']);
		}
		if(array_key_exists('item_ref_code_01', $data))
        {
            unset($data['item_ref_code_01']);
		}
		if(array_key_exists('item_desc_01', $data))
        {
            unset($data['item_desc_01']);
		}
		if(array_key_exists('item_desc_02', $data))
        {
            unset($data['item_desc_02']);
		}
		if(array_key_exists('storage_class', $data))
        {
            unset($data['storage_class']);
		}
		if(array_key_exists('item_group_01_code', $data))
        {
            unset($data['item_group_01_code']);
		}
		if(array_key_exists('item_group_02_code', $data))
        {
            unset($data['item_group_02_code']);
		}
		if(array_key_exists('item_group_03_code', $data))
        {
            unset($data['item_group_03_code']);
		}
		if(array_key_exists('item_group_04_code', $data))
        {
            unset($data['item_group_04_code']);
		}
		if(array_key_exists('item_group_05_code', $data))
        {
            unset($data['item_group_05_code']);
		}
		if(array_key_exists('item_cases_per_pallet_length', $data))
        {
            unset($data['item_cases_per_pallet_length']);
		}
		if(array_key_exists('item_cases_per_pallet_width', $data))
        {
            unset($data['item_cases_per_pallet_width']);
		}
		if(array_key_exists('item_no_of_layers', $data))
        {
            unset($data['item_no_of_layers']);
		}

		if(array_key_exists('unit_qty', $data))
        {
            unset($data['unit_qty']);
		}
		if(array_key_exists('item_unit_uom_code', $data))
        {
            unset($data['item_unit_uom_code']);
		}
		if(array_key_exists('loose_qty', $data))
        {
            unset($data['loose_qty']);
		}
		if(array_key_exists('item_loose_uom_code', $data))
        {
            unset($data['item_loose_uom_code']);
        }
        if(array_key_exists('loose_uom_id', $data))
        {
            unset($data['loose_uom_id']);
		}
		if(array_key_exists('loose_uom_rate', $data))
        {
            unset($data['loose_uom_rate']);
		}
		if(array_key_exists('case_qty', $data))
        {
            unset($data['case_qty']);
		}
		if(array_key_exists('case_uom_rate', $data))
        {
            unset($data['case_uom_rate']);
		}
        if(array_key_exists('item_case_uom_code', $data))
        {
            unset($data['item_case_uom_code']);
        }
        if(array_key_exists('uom_code', $data))
        {
            unset($data['uom_code']);
		}
		if(array_key_exists('item_unit_barcode', $data))
        {
            unset($data['item_unit_barcode']);
		}
		if(array_key_exists('item_case_barcode', $data))
        {
            unset($data['item_case_barcode']);
		}
		if(array_key_exists('gross_weight', $data))
        {
            unset($data['gross_weight']);
		}
		if(array_key_exists('cubic_meter', $data))
        {
            unset($data['cubic_meter']);
        }

        if(array_key_exists('storage_bin_id', $data))
        {
            unset($data['storage_bin_id']);
		}
        return $data;
    }

    public function changeQuantBal($hdrId, $quantBalId)
    {
        AuthService::authorize(
            array(
                'put_away_update'
            )
        );

		$putAwayHdr = PutAwayHdrRepository::findByPk($hdrId);

		$results = array();
		$quantBal = QuantBalRepository::findByPk($quantBalId);
        if(!empty($quantBal))
        {
			$item = $quantBal->item;
			$itemBatch = $quantBal->itemBatch;
			$handlingUnit = $quantBal->handlingUnit;
			
			$quantBal = ItemService::processCaseLoose($quantBal, $item, 1);

			$results['item_id'] = $quantBal->item_id;
			$results['item_code'] = $quantBal->item_code;
			$results['item_desc_01'] = $quantBal->item_desc_01;
			$results['item_desc_02'] = $quantBal->item_desc_02;
			$results['desc_01'] = $quantBal->item_desc_01;
			$results['desc_02'] = $quantBal->item_desc_02;
			$results['case_qty'] = $quantBal->case_qty;
			$results['item_case_uom_code'] = $quantBal->item_case_uom_code;
            $results['batch_serial_no'] = empty($itemBatch) ? '' : $itemBatch->batch_serial_no;
            $results['expiry_date'] = empty($itemBatch) ? '' : $itemBatch->expiry_date;
            $results['receipt_date'] = empty($itemBatch) ? '' : $itemBatch->receipt_date;
            $results['handling_unit_barcode'] = empty($handlingUnit) ? '' : $handlingUnit->getBarcode();
            $results['qty'] = $quantBal->balance_unit_qty;
            $results['uom_id'] = $item->unit_uom_id;
            $results['uom_rate'] = 1;

            $results['uom_select2'] = array('value'=>$item->unit_uom_id,'label'=>$quantBal->item_unit_uom_code);
		}

        return $results;
    }
    
    public function showHeader($hdrId)
    {
        AuthService::authorize(
            array(
                'put_away_read',
				'put_away_update'
            )
        );

        $user = Auth::user();
        
        if (!$user->can('put_away_read')) 
        {
            $exc = new ApiException(__('Auth.permission_is_required', ['name'=>'put_away_read','desc_01'=>__('Permission.put_away_read_desc_01')]));
            throw $exc;
        }
        
        
        $model = PutAwayHdrRepository::findByPk($hdrId);
        if(!empty($model))
        {            
            $model = self::processOutgoingHeader($model);
        }
        
        return $model;
    }

    public function showDetails($hdrId) 
	{
        AuthService::authorize(
            array(
                'put_away_read',
				'put_away_update'
            )
        );

        $user = Auth::user();
		$putAwayDtls = PutAwayDtlRepository::findAllByHdrId($hdrId);
		$putAwayDtls->load(
            'item', 'item.itemUoms', 'item.itemUoms.uom'
        );
        foreach($putAwayDtls as $putAwayDtl)
        {
			$putAwayDtl = self::processOutgoingDetail($putAwayDtl);
        }
        return $putAwayDtls;
    }
    
    public function createDetail($hdrId, $data)
	{
        AuthService::authorize(
            array(
                'put_away_update'
            )
        );

        $data = self::processIncomingDetail($data);		
		$data = $this->updateWarehouseItemUom($data, 0, 0);

        $putAwayHdr = PutAwayHdrRepository::txnFindByPk($hdrId);
        if($putAwayHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('PutAway.doc_status_is_wip_or_complete', ['docCode'=>$putAwayHdr->doc_code]));
            $exc->addData(\App\PutAwayHdr::class, $putAwayHdr->id);
            throw $exc;
        }
		$putAwayHdrData = $putAwayHdr->toArray();

		$putAwayDtlArray = array();
		$putAwayDtlModels = PutAwayDtlRepository::findAllByHdrId($hdrId);
		foreach($putAwayDtlModels as $putAwayDtlModel)
		{
			$putAwayDtlData = $putAwayDtlModel->toArray();
			$putAwayDtlData['is_modified'] = 0;
			$putAwayDtlArray[] = $putAwayDtlData;
		}
		$lastLineNo = count($putAwayDtlModels);

		//assign temp id
		$tmpUuid = uniqid();
		//append NEW here to make sure it will be insert in repository later
		$data['id'] = 'NEW'.$tmpUuid;
		$data['line_no'] = $lastLineNo + 1;
		$data['is_modified'] = 1;
		
		$putAwayDtlData = $this->initPutAwayDtlData($data);

		$putAwayDtlArray[] = $putAwayDtlData;

		$result = PutAwayHdrRepository::updateDetails($putAwayHdrData, $putAwayDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
            $documentDetails[] = self::processOutgoingDetail($dtlModel);
        }

        $message = __('PutAway.detail_successfully_created', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }
    
    public function updateDetails($hdrId, $dtlArray)
	{
        AuthService::authorize(
            array(
                'put_away_update'
            )
        );

        $putAwayHdr = PutAwayHdrRepository::txnFindByPk($hdrId);
        if($putAwayHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('PutAway.doc_status_is_wip_or_complete', ['docCode'=>$putAwayHdr->doc_code]));
            $exc->addData(\App\PutAwayHdr::class, $putAwayHdr->id);
            throw $exc;
        }
		$putAwayHdrData = $putAwayHdr->toArray();

		//query the putAwayDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$putAwayDtlArray = array();
        $putAwayDtlModels = PutAwayDtlRepository::findAllByHdrId($hdrId);
		foreach($putAwayDtlModels as $putAwayDtlModel)
		{
			$putAwayDtlData = $putAwayDtlModel->toArray();
			$putAwayDtlData['is_modified'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{                
				if($dtlData['id'] == $putAwayDtlData['id'])
				{
					$dtlData = self::processIncomingDetail($dtlData);
					
					if($dtlData['uom_id'] == Uom::$PALLET
                    && bccomp($dtlData['uom_rate'], 0, 5) == 0)
                    {
					}
					else
					{
						$dtlData = $this->updateWarehouseItemUom($dtlData, 0, 0);
					}

					foreach($dtlData as $fieldName => $value)
					{
						$putAwayDtlData[$fieldName] = $value;
					}

					$putAwayDtlData['is_modified'] = 1;

					break;
				}
			}
			$putAwayDtlArray[] = $putAwayDtlData;
		}
		
		$result = PutAwayHdrRepository::updateDetails($putAwayHdrData, $putAwayDtlArray, array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = self::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('PutAway.details_successfully_updated', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }
    
    public function index($siteFlowId, $sorts, $filters = array(), $pageSize = 20)
	{
        AuthService::authorize(
            array(
                'put_away_read'
            )
        );

        $putAwayHdrs = PutAwayHdrRepository::findAll($siteFlowId, $sorts, $filters, $pageSize);
        $putAwayHdrs->load(
            'putAwayDtls', 
            'putAwayDtls.item', 'putAwayDtls.item.itemUoms', 'putAwayDtls.item.itemUoms.uom', 
            'putAwayDtls.quantBal', 'putAwayDtls.toStorageBin',
            'putAwayDtls.toStorageBin.storageBay', 'putAwayDtls.toStorageBin.storageRow'
        );
		foreach($putAwayHdrs as $putAwayHdr)
		{
			$putAwayHdr = PutAwayService::processOutgoingHeader($putAwayHdr);

			//calculate the pallet_qty, case_qty, gross_weight and cubic_meter
			$palletHashByHandingUnitId = array();
			$caseQty = 0;
			$grossWeight = 0;
			$cubicMeter = 0;
			//query the docDtls
			$putAwayDtls = $putAwayHdr->putAwayDtls;

			foreach($putAwayDtls as $putAwayDtl)
			{
				$putAwayDtl = PutAwayService::processOutgoingDetail($putAwayDtl);

				$caseQty = bcadd($caseQty, $putAwayDtl->case_qty, 8);
				$grossWeight = bcadd($grossWeight, $putAwayDtl->gross_weight, 8);
				$cubicMeter = bcadd($cubicMeter, $putAwayDtl->cubic_meter, 8);

				if($putAwayDtl->handling_unit_id > 0)
				{
					$palletHashByHandingUnitId[$putAwayDtl->handling_unit_id] = $putAwayDtl->handling_unit_id;
				}

				unset($putAwayDtl->toStorageBin);
				unset($putAwayDtl->item);
			}
			
			$putAwayHdr->pallet_qty = count($palletHashByHandingUnitId);
			$putAwayHdr->case_qty = $caseQty;
			$putAwayHdr->gross_weight = $grossWeight;
			$putAwayHdr->cubic_meter = $cubicMeter;

			$putAwayHdr->details = $putAwayDtls;
		}
    	return $putAwayHdrs;
    }
    
    static public function transitionToVoid($hdrId)
    {
        AuthService::authorize(
            array(
                'put_away_revert'
            )
        );

		//use transaction to make sure this is latest value
		$hdrModel = PutAwayHdrRepository::txnFindByPk($hdrId);
		$siteFlow = $hdrModel->siteFlow;
		
		//only DRAFT can transition to WIP
		if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
		{
			$exc = new ApiException(__('PutAway.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
            $exc->addData(\App\PutAwayHdr::class, $hdrModel->id);
            throw $exc;
		}

		//revert the document
		$hdrModel = PutAwayHdrRepository::revertDraftToVoid($hdrModel->id);
		if(!empty($hdrModel))
		{
            $hdrModel->str_doc_status = DocStatus::$MAP[$hdrModel->doc_status];
			return $hdrModel;
		}

        $exc = new ApiException(__('PutAway.commit_to_void_error', ['docCode'=>$hdrModel->doc_code]));
        $exc->addData(\App\PutAwayHdr::class, $hdrModel->id);
        throw $exc;
    }

    public function deleteDetails($hdrId, $dtlArray)
	{
        AuthService::authorize(
            array(
                'put_away_update'
            )
        );

        $putAwayHdr = PutAwayHdrRepository::txnFindByPk($hdrId);
        if($putAwayHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('PutAway.doc_status_is_wip_or_complete', ['docCode'=>$putAwayHdr->doc_code]));
            $exc->addData(\App\PutAwayHdr::class, $putAwayHdr->id);
            throw $exc;
        }
		$putAwayHdrData = $putAwayHdr->toArray();

		//query the putAwayHdrDtlModels from DB
		//format to array, with is_modified field to indicate it is changed
		$delPutAwayDtlArray = array();
		$putAwayDtlArray = array();
        $putAwayDtlModels = PutAwayDtlRepository::findAllByHdrId($hdrId);
        $lineNo = 1;
		foreach($putAwayDtlModels as $putAwayDtlModel)
		{
			$putAwayDtlData = $putAwayDtlModel->toArray();
			$putAwayDtlData['is_modified'] = 0;
			$putAwayDtlData['is_deleted'] = 0;
			//check with going to updated records, to assign value to data, and set is_modified = true
			foreach($dtlArray as $dtlData)
			{
				if($dtlData['id'] == $putAwayDtlData['id'])
				{
					//this is deleted dtl
					$putAwayDtlData['is_deleted'] = 1;
					break;
				}
			}
			if($putAwayDtlData['is_deleted'] > 0)
			{
				$delPutAwayDtlArray[] = $putAwayDtlData;
			}
			else
			{
                $putAwayDtlData['line_no'] = $lineNo;
                $putAwayDtlData['is_modified'] = 1;
                $putAwayDtlArray[] = $putAwayDtlData;
                $lineNo++;
			}
        }
		
		$result = PutAwayHdrRepository::updateDetails($putAwayHdrData, $putAwayDtlArray, $delPutAwayDtlArray, array());
		$hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = self::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('PutAway.details_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>$delPutAwayDtlArray
            ),
			'message' => $message
		);
    }
    
    public function updateHeader($hdrData)
	{
        AuthService::authorize(
            array(
                'put_away_update'
            )
        );

        $hdrData = self::processIncomingHeader($hdrData);

        $putAwayHdr = PutAwayHdrRepository::txnFindByPk($hdrData['id']);
        if($putAwayHdr->doc_status >= DocStatus::$MAP['WIP'])
        {
            //WIP and COMPLETE status can not be updated
            $exc = new ApiException(__('PutAway.doc_status_is_wip_or_complete', ['docCode'=>$putAwayHdr->doc_code]));
            $exc->addData(\App\PutAwayHdr::class, $putAwayHdr->id);
            throw $exc;
        }
        $putAwayHdrData = $putAwayHdr->toArray();
        //assign hdrData to model
        foreach($hdrData as $field => $value) 
        {
            $putAwayHdrData[$field] = $value;
        }

		//$putAwayHdr->load('worker01');
        //$worker01 = $putAwayHdr->worker01;
		
		//no detail to update
        $putAwayDtlArray = array();

        $result = PutAwayHdrRepository::updateDetails($putAwayHdrData, $putAwayDtlArray, array());
        $hdrModel = $result['hdrModel'];
        $dtlModels = $result['dtlModels'];

        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = self::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('PutAway.document_successfully_updated', ['docCode'=>$documentHeader->doc_code]);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>array()
            ),
			'message' => $message
		);
    }
    
    static public function processIncomingHeader($data)
    {
        if(array_key_exists('doc_flows', $data))
        {
            unset($data['doc_flows']);
        }
		if(array_key_exists('submit_action', $data))
        {
            unset($data['submit_action']);
        }
        //preprocess the select2 and dropDown
        
        if(array_key_exists('str_doc_status', $data))
        {
            unset($data['str_doc_status']);
        }

        return $data;
    }

    public function changeItemUom($hdrId, $itemId, $uomId)
    {
        AuthService::authorize(
            array(
                'put_away_update'
            )
        );
        
        $putAwayHdr = PutAwayHdrRepository::findByPk($hdrId);

        $results = array();
        $itemUom = ItemUomRepository::findByItemIdAndUomId($itemId, $uomId);
        if(!empty($itemUom)
        && !empty($putAwayHdr))
        {
            $results['item_id'] = $itemUom->item_id;
            $results['uom_id'] = $itemUom->uom_id;
            $results['uom_rate'] = $itemUom->uom_rate;

			$results = $this->updateWarehouseItemUom($results, 0, 0);
        }
        return $results;
    }
}
