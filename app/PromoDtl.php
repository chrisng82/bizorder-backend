<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PromoDtl extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $casts = [
        'min_net_amt' => 'decimal:8',
        'min_unit_qty' => 'decimal:6',
        'foc_uom_rate' => 'decimal:6',
        'foc_qty' => 'decimal:6',
        'foc_ratio' => 'decimal:8',
        'price_disc' => 'decimal:8',
        'min_disc_val_01' => 'decimal:8',
        'max_disc_val_01' => 'decimal:8',
        'min_disc_val_02' => 'decimal:8',
        'max_disc_val_02' => 'decimal:8',
        'min_disc_val_03' => 'decimal:8',
        'max_disc_val_03' => 'decimal:8',
        'min_disc_val_04' => 'decimal:8',
        'max_disc_val_04' => 'decimal:8',
        'min_disc_val_05' => 'decimal:8',
        'max_disc_val_05' => 'decimal:8',
        'min_disc_perc_01' => 'decimal:5',
        'max_disc_perc_01' => 'decimal:5',
        'min_disc_perc_02' => 'decimal:5',
        'max_disc_perc_02' => 'decimal:5',
        'min_disc_perc_03' => 'decimal:5',
        'max_disc_perc_03' => 'decimal:5',
        'min_disc_perc_04' => 'decimal:5',
        'max_disc_perc_04' => 'decimal:5',
        'min_disc_perc_05' => 'decimal:5',
        'max_disc_perc_05' => 'decimal:5',
    ];
    
    public function promoHdr()
    {
        return $this->belongsTo(\App\PromoHdr::class, 'hdr_id', 'id');
    }

    public function generateTags(): array
    {
        $code = $this->promoHdr->code;
        unset($this->promoHdr);
        return array(
            $code
        );
    }
}
