<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\NotificationService;
use App\Services\Utils\ApiException;
use App\Services\Utils\ResponseServices;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class NotificationController extends Controller
{
    private $notificationService;

    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    public function register() {
        try
        {
            $fcmToken = Input::post('fcmToken');
            $result = $this->notificationService->register($fcmToken);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    // public function unregister() {
    //     try
    //     {
    //         $fcmToken = Input::post('fcmToken');
    //         $result = $this->notificationService->unregister($fcmToken);
    //         return ResponseServices::success()->data($result)->toJson();
    //     }
    //     catch(ApiException $exp)
    //     {
    //         return ResponseServices::error($exp->getMessage())->toJson();
    //     }
    //     catch(Exception $exp)
    //     {
    //         return ResponseServices::error($exp->getMessage())->toJson();
    //     }
    // }

    // public function notifyPostman() {
    //     $data = json_decode( \request()->getContent() );

    //     return $this->notify($data);
    // }

    public function notify($data) {
        try {
            $tokens = $this->notificationService->getTokensByUserId($data->receiver_user); 

            $push = $this->notificationService->getPush($data->title, $data->message, $data->payload);

            $response = $this->notificationService->send( $tokens, $push );

            return response()->json( [
                'response' => $response
            ] );
        } catch ( \Exception $ex ) {
            return response()->json( [
            'error'   => true,
            'message' => $ex->getMessage()
            ] );
        }
    }
}
