<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\ItemGroup01DivisionService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;
use App\Repositories\BatchJobStatusRepository;

class ItemGroup01DivisionController extends Controller {
  private $itemGroup01DivisionService;

  public function __construct(ItemGroup01DivisionService $itemGroup01DivisionService) {
    $this->itemGroup01DivisionService = $itemGroup01DivisionService;
  }

  private function processSorts($oldSorts) {
    $newSorts = array();
    foreach ($oldSorts as $oldSort) {
      $sortParts = explode(':',$oldSort);
      $newSorts[] = array(
        'field' => $sortParts[0],
        'order' => $sortParts[1]
      );
    }
    return $newSorts;
  }

  private function processFilters($oldFilters) {
    $newFilters = array();
    foreach ($oldFilters as $oldFilter) {
      $filterParts = explode(':',$oldFilter);
      $newFilters[] = array(
        'field' => $filterParts[0],
        'value' => $filterParts[1]
      );
    }
    return $newFilters;
  }

  public function reorderBrand()
  {
      try
      {
          $curBrandId = Input::post('curBrandId', 0);
          $prevBrandId = Input::post('prevBrandId', 0);
          $curDivisionId = Input::post('curDivisionId', 0);

          $result = $this->itemGroup01DivisionService->reorderBrand($curBrandId, $prevBrandId, $curDivisionId);
          $data = $result['data'];
          $message = $result['message'];
          return ResponseServices::success($message)->data($data)->toJson();
      } 
      catch(ApiException $exp)
      {
          return ResponseServices::error($exp->getMessage())->toJson();
      }
      catch(Exception $exp)
      {
          return ResponseServices::error($exp->getMessage())->toJson();
      }
  }
}
