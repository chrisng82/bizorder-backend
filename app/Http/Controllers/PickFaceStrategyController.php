<?php

namespace App\Http\Controllers;

use App\Services\Env\ProcType;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Services\AuthService;
use App\Services\PickFaceStrategyService;
use App\Exports\PickFaceStrategyExcel01Export;
use App\Repositories\BatchJobStatusRepository;
use App\Services\Utils\ResponseServices;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Services\Utils\ApiException;

class PickFaceStrategyController extends Controller
{
    private $pickFaceStrategyService;

    public function __construct(PickFaceStrategyService $pickFaceStrategyService) {
		$this->pickFaceStrategyService = $pickFaceStrategyService;
    }

    public function indexProcess($strProcType, $siteFlowId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);

        try
        {
            $sorts = Input::post('sorts', array());
            $sorts = $this->processSorts($sorts);
            $filters = Input::post('filters', array());
            $filters = $this->processFilters($filters);
            $pageSize = Input::post('pageSize', 20);
            $result = $this->pickFaceStrategyService->indexProcess($strProcType, $siteFlowId, $sorts, $filters, $pageSize);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function downloadProcess($strProcType, $siteFlowId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);

        AuthService::authorize(
            array(
                'pick_face_strategy_export'
            )
        );

        try
        {
            $user = Auth::user();

            //download excel need to call within controller, else axios will get 0 byte in response body
            if(isset(ProcType::$MAP[$strProcType]))
            {
                if(ProcType::$MAP[$strProcType] == ProcType::$MAP['PICK_FACE_STRATEGY_EXCEL_01']) 
                {
                    $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['PICK_FACE_STRATEGY_EXCEL_01'], $user->id);

                    $pickFaceStrategyExcel01Export = new PickFaceStrategyExcel01Export($user->id, $batchJobStatusModel);
                    return Excel::download($pickFaceStrategyExcel01Export, 'PICK_FACE_STRATEGY_EXCEL_01.XLSX', \Maatwebsite\Excel\Excel::XLSX);
                }
            }
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function uploadProcess($strProcType, $siteFlowId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $file = Input::file('file');

            $result = $this->pickFaceStrategyService->uploadProcess($strProcType, $siteFlowId, $file);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(\PhpOffice\PhpSpreadsheet\Reader\Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    private function processSorts($oldSorts)
    {
        $newSorts = array();
        foreach ($oldSorts as $oldSort) {
            $sortParts = explode(':',$oldSort);
            $newSorts[] = array(
                'field' => $sortParts[0],
                'order' => $sortParts[1]
            );
        }
        return $newSorts;
    }

    private function processFilters($oldFilters)
    {
        $newFilters = array();
        foreach ($oldFilters as $oldFilter) {
            $filterParts = explode(':',$oldFilter);
            $newFilters[] = array(
                'field' => $filterParts[0],
                'value' => $filterParts[1]
            );
        }
        return $newFilters;
    }
}