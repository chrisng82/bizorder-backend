<?php

namespace App\Http\Controllers;

use App\Services\Utils\ResponseServices;
use App\Services\CurrencyService;
use Illuminate\Http\Request;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\Input;

class CurrencyController extends Controller
{
    private $currencyService;

    public function __construct(CurrencyService $currencyService) 
    {
		$this->currencyService = $currencyService;
    }

    public function select2()
    {
        try
        {
            $search = Input::post('search','***');
            $filters = Input::post('filters',array());
            $result = $this->currencyService->select2($search, $filters);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
}