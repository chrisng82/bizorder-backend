<?php

namespace App\Http\Controllers;

use App\Services\WhseJobService;
use App\Services\PickListService;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class WhseJobController extends Controller
{
    private $whseJobService;

    public function __construct(WhseJobService $whseJobService) 
    {
        $this->whseJobService = $whseJobService;
    }

    public function indexProcess($strProcType, $siteFlowId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $sorts = Input::post('sorts', array());
            $sorts = $this->processSorts($sorts);
            $filters = Input::post('filters', array());
            $filters = $this->processFilters($filters);
            $pageSize = Input::post('pageSize', 20);
            $result = $this->whseJobService->indexProcess($strProcType, $siteFlowId, $sorts, $filters, $pageSize);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function createProcess($strProcType)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $hdrIds = Input::post('hdrIds');
            $result = $this->whseJobService->createProcess($strProcType, $hdrIds);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            $data = array(
                array(
                    'doc_code'=>'', 
                    'doc_date'=>date('Y-m-d'),
                    'str_doc_status'=>$exp->getMessage(),
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                )
            );
            return ResponseServices::error($exp->getMessage())->data($data)->toJson();
        }
        catch(Exception $exp)
        {
            $data = array(
                array(
                    'doc_code'=>'', 
                    'doc_date'=>date('Y-m-d'),
                    'str_doc_status'=>$exp->getMessage(),
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                )
            );
            return ResponseServices::error($exp->getMessage())->data($data)->toJson();
        }
    }

    public function downloadProcess($hdrId, $mobileProfileId, $worker01Id)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);

        try
        {
            $worker02Id = Input::post('worker02Id', 0);
            $worker03Id = Input::post('worker03Id', 0);
            $worker04Id = Input::post('worker04Id', 0);
            $worker05Id = Input::post('worker05Id', 0);
            $equipment01Id = Input::post('equipment01Id', 0);
            $equipment02Id = Input::post('equipment02Id', 0);
            $equipment03Id = Input::post('equipment03Id', 0);
            $equipment04Id = Input::post('equipment04Id', 0);
            $equipment05Id = Input::post('equipment05Id', 0);
            $result = $this->whseJobService->downloadProcess($hdrId, $mobileProfileId, 
                $worker01Id, $worker02Id, $worker03Id, $worker04Id, $worker05Id,
                $equipment01Id, $equipment02Id, $equipment03Id, $equipment04Id, $equipment05Id);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function completeProcess($hdrId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);

        try
        {
            $json = Input::post('data');
            $details = $json['details'];
            $items = $json['items'];
            $result = $this->whseJobService->completeProcess($hdrId, $details, $items);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            $errorCode = 0;
            foreach($exp->getData() as $error)
            {
                if($error['type'] == 'ERROR_CODE')
                {
                    $errorCode = $error['id'];
                }
                else
                {
                    $errorData[] = $error;
                }
            }
            return ResponseServices::error($exp->getMessage(), $errorCode)
            ->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function revertProcess($hdrId)
    {
        try
        {
            $json = Input::post('data');
            $whseJobHdr = $this->whseJobService->revertProcess($hdrId);
            return ResponseServices::success()->data($whseJobHdr)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function printProcess($strProcType, $siteFlowId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $hdrIds = Input::get('hdrIds');
            $result = $this->whseJobService->printProcess($strProcType, $siteFlowId, $hdrIds);
            return $result;           
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function showHeader($hdrId)
    {
        try
        {
            $result = $this->whseJobService->showHeader($hdrId);
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function showDetails($strResType, $hdrId)
    {
        try
        {
            $result = $this->whseJobService->showDetails($strResType, $hdrId);
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function updateDetails($strResType, $hdrId)
    {
        try
        {
            $json = Input::post('data');
            $result = $this->whseJobService->updateDetails($strResType, $hdrId, $json);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function createDetail($strResType, $hdrId)
    {
        try
        {
            $json = Input::post('data');
            $result = $this->whseJobService->createDetail($strResType, $hdrId, $json);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function deleteDetails($hdrId)
    {
        try
        {
            $json = Input::post('data');
            $result = $this->whseJobService->deleteDetails($hdrId, $json);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function updateHeader()
    {
        try
        {
            $json = Input::post('data');
            $result = $this->whseJobService->updateHeader($json);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function transitionToStatus()
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $hdrId = Input::post('hdrId', 0);
            $docStatus = Input::post('docStatus', 0);
            $json = Input::post('data');
            $result = $this->whseJobService->transitionToStatus($hdrId, $docStatus, $json);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function changeQuantBal()
    {
        try
        {
            $hdrId = Input::post('hdrId', 0);
            $quantBalId = Input::post('quantBalId', 0);
            $result = $this->whseJobService->changeQuantBal($hdrId, $quantBalId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function changeItemUom()
    {
        try
        {
            $hdrId = Input::post('hdrId', 0);
            $itemId = Input::post('itemId', 0);
            $uomId = Input::post('uomId', 0);
            $result = $this->whseJobService->changeItemUom($hdrId, $itemId, $uomId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function changeItemBatch()
    {
        try
        {
            $hdrId = Input::post('hdrId', 0);
            $itemBatchId = Input::post('itemBatchId', 0);
            $result = $this->whseJobService->changeItemBatch($hdrId, $itemBatchId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function changeItem()
    {
        try
        {
            $hdrId = Input::post('hdrId', 0);
            $itemId = Input::post('itemId', 0);
            $result = $this->whseJobService->changeItem($hdrId, $itemId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    private function processSorts($oldSorts)
    {
        $newSorts = array();
        foreach ($oldSorts as $oldSort) {
            $sortParts = explode(':',$oldSort);
            $newSorts[] = array(
                'field' => $sortParts[0],
                'order' => $sortParts[1]
            );
        }
        return $newSorts;
    }

    private function processFilters($oldFilters)
    {
        $newFilters = array();
        foreach ($oldFilters as $oldFilter) {
            $filterParts = explode(':',$oldFilter);
            $newFilters[] = array(
                'field' => $filterParts[0],
                'value' => $filterParts[1]
            );
        }
        return $newFilters;
    }
}