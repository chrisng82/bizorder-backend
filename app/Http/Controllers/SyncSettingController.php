<?php

namespace App\Http\Controllers;

use App\Services\Utils\ResponseServices;
use App\Services\SyncSettingService;
use Illuminate\Http\Request;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\Input;

class SyncSettingController extends Controller
{
    private $syncSettingService;

    public function __construct(SyncSettingService $syncSettingService) 
    {
		$this->syncSettingService = $syncSettingService;
    }

    public function showSiteFlowSetting($strProcType, $siteFlowId)
    {
        try
        {
            $result = $this->syncSettingService->showSiteFlowSetting($strProcType, $siteFlowId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function showDivisionSetting($strProcType, $divisionId)
    {
        try
        {
            $result = $this->syncSettingService->showDivisionSetting($strProcType, $divisionId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function updateModel()
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);

        try
        {
            $json = Input::post('data');
            $result = $this->syncSettingService->updateModel($json);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
}