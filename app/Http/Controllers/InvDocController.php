<?php

namespace App\Http\Controllers;

use App\Services\Utils\ResponseServices;
use App\Services\InvDocService;
use Illuminate\Http\Request;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\Input;

class InvDocController extends Controller
{
    private $invDocService;

    public function __construct(InvDocService $invDocService) {
		$this->invDocService = $invDocService;
    }
    
    public function indexProcess($strProcType, $siteFlowId)
    {
        try
        {
            $sorts = Input::post('sorts', array());
            $sorts = $this->processSorts($sorts);
            $filters = Input::post('filters', array());
            $filters = $this->processFilters($filters);
            $pageSize = Input::post('pageSize', 20);
            $result = $this->invDocService->indexProcess($strProcType, $siteFlowId, $sorts, $filters, $pageSize);

            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }

        /*
        $result = array (
            'current_page' => 1,
            'data' => [
                "key"=>0,
                "data"=>
                  array(
                      'id' => 1,
                      'doc_code' => 'PCK00001',
                      'proc_type' => 6,
                      'ref_code_01' => '',
                      'ref_code_02' => '',
                      'doc_date' => '2019-01-28',
                      'desc_01' => '',
                      'desc_02' => '',
                      'site_flow_id' => 1,
                      'doc_status' => 50,
                      'created_at' => '2019-01-28 06:49:04',
                      'updated_at' => '2019-01-28 06:49:04',
                      'pick_unit_qty' => '1068.00000000',
                      'pick_case_qty' => '42.08333333',
                      'pick_gross_weight' => '360.46000000',
                      'pick_cubic_meter' => '0.59649475',
                      'ord_unit_qty' => '1068.00000000',
                      'ord_case_qty' => '42.08333333',
                      'ord_gross_weight' => '360.46000000',
                      'ord_cubic_meter' => '0.59649475',
                  ),
                  'children' => [
                  array (
                      "key"=>'child1',
                      "data"=>
                          array (
                          'pick_unit_qty' => '240.00000000',
                          'pick_case_qty' => '10.00000000',
                          'pick_gross_weight' => '131.76000000',
                          'pick_cubic_meter' => '0.23677500',
                          'ord_unit_qty' => '240.00000000',
                          'ord_case_qty' => '10.00000000',
                          'ord_gross_weight' => '131.76000000',
                          'ord_cubic_meter' => '0.23677500',
                          ),
                      'children' => [
                          array (
                              "key"=>'node1',
                              "data"=>
                              array (
                                  'id' => 6,
                                  'hdr_id' => 2,
                                  'line_no' => 3,
                                  'item_id' => 5,
                                  'desc_01' => 'POKKA OOLONG TEA - 500ml X 24',
                                  'desc_02' => '',
                                  'uom_id' => 2,
                                  'uom_rate' => '24.00000000',
                                  'sale_price' => '48.48000000',
                                  'qty' => '10.00000000',
                                  'gross_amt' => '484.80000000',
                                  'gross_local_amt' => '484.80000000',
                                  'promo_hdr_id' => 0,
                                  'disc_val_01' => '0.00000000',
                                  'disc_perc_01' => '0.00000000',
                                  'disc_val_02' => '0.00000000',
                                  'disc_perc_02' => '0.00000000',
                                  'disc_val_03' => '0.00000000',
                                  'disc_perc_03' => '0.00000000',
                                  'disc_val_04' => '0.00000000',
                                  'disc_perc_04' => '0.00000000',
                                  'disc_val_05' => '0.00000000',
                                  'disc_perc_05' => '0.00000000',
                                  'disc_amt' => '0.00000000',
                                  'disc_local_amt' => '0.00000000',
                                  'tax_hdr_id' => 0,
                                  'tax_val_01' => '0.00000000',
                                  'tax_perc_01' => '0.00000000',
                                  'tax_val_02' => '0.00000000',
                                  'tax_perc_02' => '0.00000000',
                                  'tax_val_03' => '0.00000000',
                                  'tax_perc_03' => '0.00000000',
                                  'tax_val_04' => '0.00000000',
                                  'tax_perc_04' => '0.00000000',
                                  'tax_val_05' => '0.00000000',
                                  'tax_perc_05' => '0.00000000',
                                  'tax_amt' => '0.00000000',
                                  'tax_local_amt' => '0.00000000',
                                  'net_amt' => '484.80000000',
                                  'net_local_amt' => '484.80000000',
                                  'created_at' => '1980-01-01 00:00:00',
                                  'updated_at' => '1980-01-01 00:00:00',
                                  'ttl_unit_qty' => '240.0000000000',
                                  'ttl_case_qty' => '10.0000000000',
                                  'ttl_gross_weight' => 131.759999999999990905052982270717620849609375,
                                  'ttl_cubic_meter' => 0.236775000000000013233858453531865961849689483642578125,
                                  'controller' => 'outbOrd',
                                  'read_action' => 'indexDetail',
                                  'update_action' => 'updateDetails',
                              ),
                          ),
                          ],
                  ),
                  array (
                    "key"=>'child1',
                    "data"=>array(
                      'pick_unit_qty' => '480.00000000',
                      'pick_case_qty' => '20.00000000',
                      'pick_gross_weight' => '144.00000000',
                      'pick_cubic_meter' => '0.20520000',
                      'ord_unit_qty' => '480.00000000',
                      'ord_case_qty' => '20.00000000',
                      'ord_gross_weight' => '144.00000000',
                      'ord_cubic_meter' => '0.20520000',
                    ),
                    'children' => [
                      array (
                          "key"=>'node2',
                          "data"=>
                              array (
                                  'id' => 3,
                                  'hdr_id' => 1,
                                  'line_no' => 3,
                                  'item_id' => 3,
                                  'desc_01' => 'RED BULL GOLD CAN - 250ml x 24',
                                  'desc_02' => '',
                                  'uom_id' => 2,
                                  'uom_rate' => '24.00000000',
                                  'sale_price' => '53.04000000',
                                  'qty' => '10.00000000',
                                  'gross_amt' => '530.40000000',
                                  'gross_local_amt' => '530.40000000',
                                  'promo_hdr_id' => 0,
                                  'disc_val_01' => '0.00000000',
                                  'disc_perc_01' => '0.00000000',
                                  'disc_val_02' => '0.00000000',
                                  'disc_perc_02' => '0.00000000',
                                  'disc_val_03' => '0.00000000',
                                  'disc_perc_03' => '0.00000000',
                                  'disc_val_04' => '0.00000000',
                                  'disc_perc_04' => '0.00000000',
                                  'disc_val_05' => '0.00000000',
                                  'disc_perc_05' => '0.00000000',
                                  'disc_amt' => '0.00000000',
                                  'disc_local_amt' => '0.00000000',
                                  'tax_hdr_id' => 0,
                                  'tax_val_01' => '0.00000000',
                                  'tax_perc_01' => '0.00000000',
                                  'tax_val_02' => '0.00000000',
                                  'tax_perc_02' => '0.00000000',
                                  'tax_val_03' => '0.00000000',
                                  'tax_perc_03' => '0.00000000',
                                  'tax_val_04' => '0.00000000',
                                  'tax_perc_04' => '0.00000000',
                                  'tax_val_05' => '0.00000000',
                                  'tax_perc_05' => '0.00000000',
                                  'tax_amt' => '0.00000000',
                                  'tax_local_amt' => '0.00000000',
                                  'net_amt' => '530.40000000',
                                  'net_local_amt' => '530.40000000',
                                  'created_at' => '1980-01-01 00:00:00',
                                  'updated_at' => '1980-01-01 00:00:00',
                                  'ttl_unit_qty' => '240.0000000000',
                                  'ttl_case_qty' => '10.0000000000',
                                  'ttl_gross_weight' => 72,
                                  'ttl_cubic_meter' => 0.1025999999999999967581487680945429019629955291748046875,
                                  'controller' => 'outbOrd',
                                  'read_action' => 'indexDetail',
                                  'update_action' => 'updateDetails',
                              ),
                      ),
                      array (
                          "key"=>'node3',
                          "data"=>
                          array (
                              'id' => 4,
                              'hdr_id' => 2,
                              'line_no' => 1,
                              'item_id' => 3,
                              'desc_01' => 'RED BULL GOLD CAN - 250ml x 24',
                              'desc_02' => '',
                              'uom_id' => 2,
                              'uom_rate' => '24.00000000',
                              'sale_price' => '53.04000000',
                              'qty' => '10.00000000',
                              'gross_amt' => '530.40000000',
                              'gross_local_amt' => '530.40000000',
                              'promo_hdr_id' => 0,
                              'disc_val_01' => '0.00000000',
                              'disc_perc_01' => '0.00000000',
                              'disc_val_02' => '0.00000000',
                              'disc_perc_02' => '0.00000000',
                              'disc_val_03' => '0.00000000',
                              'disc_perc_03' => '0.00000000',
                              'disc_val_04' => '0.00000000',
                              'disc_perc_04' => '0.00000000',
                              'disc_val_05' => '0.00000000',
                              'disc_perc_05' => '0.00000000',
                              'disc_amt' => '0.00000000',
                              'disc_local_amt' => '0.00000000',
                              'tax_hdr_id' => 0,
                              'tax_val_01' => '0.00000000',
                              'tax_perc_01' => '0.00000000',
                              'tax_val_02' => '0.00000000',
                              'tax_perc_02' => '0.00000000',
                              'tax_val_03' => '0.00000000',
                              'tax_perc_03' => '0.00000000',
                              'tax_val_04' => '0.00000000',
                              'tax_perc_04' => '0.00000000',
                              'tax_val_05' => '0.00000000',
                              'tax_perc_05' => '0.00000000',
                              'tax_amt' => '0.00000000',
                              'tax_local_amt' => '0.00000000',
                              'net_amt' => '530.40000000',
                              'net_local_amt' => '530.40000000',
                              'created_at' => '1980-01-01 00:00:00',
                              'updated_at' => '1980-01-01 00:00:00',
                              'ttl_unit_qty' => '240.0000000000',
                              'ttl_case_qty' => '10.0000000000',
                              'ttl_gross_weight' => 72,
                              'ttl_cubic_meter' => 0.1025999999999999967581487680945429019629955291748046875,
                              'controller' => 'outbOrd',
                              'read_action' => 'indexDetail',
                              'update_action' => 'updateDetails',
                          )
                      ),
                  ],
                  ),
                  array (
                      "key"=>'child2',
                          "data"=>array(
                              'pick_unit_qty' => '240.00000000',
                              'pick_case_qty' => '10.00000000',
                              'pick_gross_weight' => '72.00000000',
                              'pick_cubic_meter' => '0.11730000',
                              'ord_unit_qty' => '240.00000000',
                              'ord_case_qty' => '10.00000000',
                              'ord_gross_weight' => '72.00000000',
                              'ord_cubic_meter' => '0.11730000',
                          ),
                      'children' => [
                          array (
                              "key"=>'node4',
                              "data"=>array(
                                      'id' => 5,
                                      'hdr_id' => 2,
                                      'line_no' => 2,
                                      'item_id' => 4,
                                      'desc_01' => 'RED BULL BOTTLE - 150ml x 6 x 4',
                                      'desc_02' => '',
                                      'uom_id' => 2,
                                      'uom_rate' => '24.00000000',
                                      'sale_price' => '30.48000000',
                                      'qty' => '10.00000000',
                                      'gross_amt' => '304.80000000',
                                      'gross_local_amt' => '304.80000000',
                                      'promo_hdr_id' => 0,
                                      'disc_val_01' => '0.00000000',
                                      'disc_perc_01' => '0.00000000',
                                      'disc_val_02' => '0.00000000',
                                      'disc_perc_02' => '0.00000000',
                                      'disc_val_03' => '0.00000000',
                                      'disc_perc_03' => '0.00000000',
                                      'disc_val_04' => '0.00000000',
                                      'disc_perc_04' => '0.00000000',
                                      'disc_val_05' => '0.00000000',
                                      'disc_perc_05' => '0.00000000',
                                      'disc_amt' => '0.00000000',
                                      'disc_local_amt' => '0.00000000',
                                      'tax_hdr_id' => 0,
                                      'tax_val_01' => '0.00000000',
                                      'tax_perc_01' => '0.00000000',
                                      'tax_val_02' => '0.00000000',
                                      'tax_perc_02' => '0.00000000',
                                      'tax_val_03' => '0.00000000',
                                      'tax_perc_03' => '0.00000000',
                                      'tax_val_04' => '0.00000000',
                                      'tax_perc_04' => '0.00000000',
                                      'tax_val_05' => '0.00000000',
                                      'tax_perc_05' => '0.00000000',
                                      'tax_amt' => '0.00000000',
                                      'tax_local_amt' => '0.00000000',
                                      'net_amt' => '304.80000000',
                                      'net_local_amt' => '304.80000000',
                                      'created_at' => '1980-01-01 00:00:00',
                                      'updated_at' => '1980-01-01 00:00:00',
                                      'ttl_unit_qty' => '240.0000000000',
                                      'ttl_case_qty' => '10.0000000000',
                                      'ttl_gross_weight' => 72,
                                      'ttl_cubic_meter' => 0.11730000000000000148769885299770976416766643524169921875,
                                      'controller' => 'outbOrd',
                                      'read_action' => 'indexDetail',
                                      'update_action' => 'updateDetails',
                              ),
                          ),
                      ],
                  ),
                  array (
                      "key"=>'child3',
                      "data"=>array(
                          'pick_unit_qty' => '48.00000000',
                          'pick_case_qty' => '2.00000000',
                          'pick_gross_weight' => '11.80800000',
                          'pick_cubic_meter' => '0.03385600',
                          'ord_unit_qty' => '48.00000000',
                          'ord_case_qty' => '2.00000000',
                          'ord_gross_weight' => '11.80800000',
                          'ord_cubic_meter' => '0.03385600',
                      ),
                      'children' => [
                          array (
                              "key"=>'node5',
                              "data"=>array(
                                      'id' => 1,
                                      'hdr_id' => 1,
                                      'line_no' => 1,
                                      'item_id' => 1,
                                      'desc_01' => 'ATTACK ULTRA POWER 240Gx24',
                                      'desc_02' => '',
                                      'uom_id' => 2,
                                      'uom_rate' => '24.00000000',
                                      'sale_price' => '66.96000000',
                                      'qty' => '2.00000000',
                                      'gross_amt' => '133.92000000',
                                      'gross_local_amt' => '133.92000000',
                                      'promo_hdr_id' => 0,
                                      'disc_val_01' => '0.00000000',
                                      'disc_perc_01' => '0.00000000',
                                      'disc_val_02' => '0.00000000',
                                      'disc_perc_02' => '0.00000000',
                                      'disc_val_03' => '0.00000000',
                                      'disc_perc_03' => '0.00000000',
                                      'disc_val_04' => '0.00000000',
                                      'disc_perc_04' => '0.00000000',
                                      'disc_val_05' => '0.00000000',
                                      'disc_perc_05' => '0.00000000',
                                      'disc_amt' => '0.00000000',
                                      'disc_local_amt' => '0.00000000',
                                      'tax_hdr_id' => 0,
                                      'tax_val_01' => '0.00000000',
                                      'tax_perc_01' => '0.00000000',
                                      'tax_val_02' => '0.00000000',
                                      'tax_perc_02' => '0.00000000',
                                      'tax_val_03' => '0.00000000',
                                      'tax_perc_03' => '0.00000000',
                                      'tax_val_04' => '0.00000000',
                                      'tax_perc_04' => '0.00000000',
                                      'tax_val_05' => '0.00000000',
                                      'tax_perc_05' => '0.00000000',
                                      'tax_amt' => '0.00000000',
                                      'tax_local_amt' => '0.00000000',
                                      'net_amt' => '133.92000000',
                                      'net_local_amt' => '133.92000000',
                                      'created_at' => '1980-01-01 00:00:00',
                                      'updated_at' => '1980-01-01 00:00:00',
                                      'ttl_unit_qty' => '48.0000000000',
                                      'ttl_case_qty' => '2.0000000000',
                                      'ttl_gross_weight' => 11.80799999999999982946974341757595539093017578125,
                                      'ttl_cubic_meter' => 0.03385599999999999720756704846280626952648162841796875,
                                      'controller' => 'outbOrd',
                                      'read_action' => 'indexDetail',
                                      'update_action' => 'updateDetails',
                              ),
                          ),
                      ],
                  ),
                  array (
                      "key"=>'child4',
                      "data"=>array(
                          'pick_unit_qty' => '60.00000000',
                          'pick_case_qty' => '0.08333333',
                          'pick_gross_weight' => '0.89200000',
                          'pick_cubic_meter' => '0.00336375',
                          'ord_unit_qty' => '60.00000000',
                          'ord_case_qty' => '0.08333333',
                          'ord_gross_weight' => '0.89200000',
                          'ord_cubic_meter' => '0.00336375',
                      ),
                      'children' => [
                          array (
                              "key"=>'child1',
                              "data"=>array(
                                      'id' => 2,
                                      'hdr_id' => 1,
                                      'line_no' => 2,
                                      'item_id' => 6,
                                      'desc_01' => 'PANADOL SOLUBLE - 4s x 30card x 24 (NEW)',
                                      'desc_02' => '',
                                      'uom_id' => 5,
                                      'uom_rate' => '30.00000000',
                                      'sale_price' => '62.52000000',
                                      'qty' => '2.00000000',
                                      'gross_amt' => '125.04000000',
                                      'gross_local_amt' => '125.04000000',
                                      'promo_hdr_id' => 0,
                                      'disc_val_01' => '0.00000000',
                                      'disc_perc_01' => '0.00000000',
                                      'disc_val_02' => '0.00000000',
                                      'disc_perc_02' => '0.00000000',
                                      'disc_val_03' => '0.00000000',
                                      'disc_perc_03' => '0.00000000',
                                      'disc_val_04' => '0.00000000',
                                      'disc_perc_04' => '0.00000000',
                                      'disc_val_05' => '0.00000000',
                                      'disc_perc_05' => '0.00000000',
                                      'disc_amt' => '0.00000000',
                                      'disc_local_amt' => '0.00000000',
                                      'tax_hdr_id' => 0,
                                      'tax_val_01' => '0.00000000',
                                      'tax_perc_01' => '0.00000000',
                                      'tax_val_02' => '0.00000000',
                                      'tax_perc_02' => '0.00000000',
                                      'tax_val_03' => '0.00000000',
                                      'tax_perc_03' => '0.00000000',
                                      'tax_val_04' => '0.00000000',
                                      'tax_perc_04' => '0.00000000',
                                      'tax_val_05' => '0.00000000',
                                      'tax_perc_05' => '0.00000000',
                                      'tax_amt' => '0.00000000',
                                      'tax_local_amt' => '0.00000000',
                                      'net_amt' => '125.04000000',
                                      'net_local_amt' => '125.04000000',
                                      'created_at' => '1980-01-01 00:00:00',
                                      'updated_at' => '1980-01-01 00:00:00',
                                      'ttl_unit_qty' => '60.0000000000',
                                      'ttl_case_qty' => '0.0833333333',
                                      'ttl_gross_weight' => 0.89200000000000001509903313490212894976139068603515625,
                                      'ttl_cubic_meter' => 0.0033637499999999999865385458264199769473634660243988037109375,
                                      'controller' => 'outbOrd',
                                      'read_action' => 'indexDetail',
                                      'update_action' => 'updateDetails',
                              ),
                          ),
                      ],
                  ),
                  
              ],
              
              ],
            'first_page_url' => 'http://inverze.dyndns.org:8000/scm-backend/public/api/v1/invDoc/indexProcess/INV_DOC_01/1?page=1',
            'from' => 1,
            'last_page' => 1,
            'last_page_url' => 'http://inverze.dyndns.org:8000/scm-backend/public/api/v1/invDoc/indexProcess/INV_DOC_01/1?page=1',
            'next_page_url' => NULL,
            'path' => 'http://inverze.dyndns.org:8000/scm-backend/public/api/v1/invDoc/indexProcess/INV_DOC_01/1',
            'per_page' => 20,
            'prev_page_url' => NULL,
            'to' => 1,
            'total' => 1,
          );
          */
    }

    public function createProcess($strProcType)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $hdrIds = Input::post('hdrIds', array());
            $result = $this->invDocService->createProcess($strProcType, $hdrIds);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            $data = array(
                array(
                    'doc_code'=>'', 
                    'doc_date'=>date('Y-m-d'),
                    'str_doc_status'=>$exp->getMessage(),
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                )
            );
            return ResponseServices::error($exp->getMessage())->data($data)->toJson();
        }
        catch(Exception $exp)
        {
            $data = array(
                array(
                    'doc_code'=>'', 
                    'doc_date'=>date('Y-m-d'),
                    'str_doc_status'=>$exp->getMessage(),
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                )
            );
            return ResponseServices::error($exp->getMessage())->data($data)->toJson();
        }
    }

    public function printProcess($strProcType)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        try
        {
            $hdrIds = Input::get('hdrIds');
            return $this->invDocService->printProcess($strProcType, $hdrIds);           
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    private function processSorts($oldSorts)
    {
        $newSorts = array();
        foreach ($oldSorts as $oldSort) {
            $sortParts = explode(':',$oldSort);
            $newSorts[] = array(
                'field' => $sortParts[0],
                'order' => $sortParts[1]
            );
        }
        return $newSorts;
    }

    private function processFilters($oldFilters)
    {
        $newFilters = array();
        foreach ($oldFilters as $oldFilter) {
            $filterParts = explode(':',$oldFilter);
            $newFilters[] = array(
                'field' => $filterParts[0],
                'value' => $filterParts[1]
            );
        }
        return $newFilters;
    }
}
