<?php

namespace App\Http\Controllers;

use App\Services\Utils\ResponseServices;
use App\Services\EfiChainService;
use Illuminate\Http\Request;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\Input;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;

class EfiChainController extends Controller
{
    private $efiChainService;

    public function __construct(EfiChainService $efiChainService) 
    {
		$this->efiChainService = $efiChainService;
    }

    public function syncProcess($strProcType, $divisionId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $result = $this->efiChainService->syncProcess($strProcType, $divisionId);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();          
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(ClientException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(ConnectException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
}