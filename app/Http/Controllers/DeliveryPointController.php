<?php

namespace App\Http\Controllers;

use App\Services\Env\ProcType;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Services\AuthService;
use App\Services\DeliveryPointService;
use App\Exports\DeliveryPointExcel01Export;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Repositories\BatchJobStatusRepository;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ClientException;

class DeliveryPointController extends Controller
{
    private $deliveryPointService;

    public function __construct(DeliveryPointService $deliveryPointService) {
		$this->deliveryPointService = $deliveryPointService;
    }

    public function indexProcess($strProcType)
    {
        try
        {
            $sorts = Input::get('sorts', array());
            $sorts = $this->processSorts($sorts);
            $filters = Input::get('filters', array());
            $filters = $this->processFilters($filters);
            $pageSize = Input::get('pageSize', 20);
            $result = $this->deliveryPointService->indexProcess($strProcType, $sorts, $filters, $pageSize);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function downloadProcess($strProcType, $siteFlowId, $divisionId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);

        AuthService::authorize(
            array(
                'delivery_point_export'
            )
        );

        try
        {
            $user = Auth::user();

            //download excel need to call within controller, else axios will get 0 byte in response body
            if(isset(ProcType::$MAP[$strProcType]))
            {
                if(ProcType::$MAP[$strProcType] == ProcType::$MAP['DELIVERY_POINT_EXCEL_01']) 
                {
                    $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['DELIVERY_POINT_EXCEL_01'], $user->id);

                    $deliveryPointExcel01Export = new DeliveryPointExcel01Export($user->id, $batchJobStatusModel, DeliveryPointService::$DELIVERY_POINT_EXCEL_01);
                    return Excel::download($deliveryPointExcel01Export, 'DELIVERY_POINT_EXCEL_01.XLSX', \Maatwebsite\Excel\Excel::XLSX);
                }
            }            
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function uploadProcess($strProcType, $siteFlowId, $divisionId)
    {
        try
        {
            $file = Input::file('file');

            $result = $this->deliveryPointService->uploadProcess($strProcType, $siteFlowId, $divisionId, $file);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(\PhpOffice\PhpSpreadsheet\Reader\Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function syncProcess($strProcType, $siteFlowId, $divisionId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $result = $this->deliveryPointService->syncProcess($strProcType, $siteFlowId, $divisionId);
            return ResponseServices::success()->data($result)->toJson();          
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(ClientException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(ConnectException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function select2()
    {
        try
        {
            $search = Input::post('search','***');
            $filters = Input::post('filters',array());
            $result = $this->deliveryPointService->select2($search, $filters);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function showModel($deliveryPointId)
    {
        try
        {
            $result = $this->deliveryPointService->showModel($deliveryPointId);
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function initModel()
    {
        try
        {
            $result = $this->deliveryPointService->initModel();
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function createModel()
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);

        try
        {
            $json = Input::post('data');
            $result = $this->deliveryPointService->createModel($json);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function updateModel()
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);

        try
        {
            $json = Input::post('data');
            $result = $this->deliveryPointService->updateModel($json);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function deleteModel($deliveryPointId)
    {
        try
        {
            $result = $this->deliveryPointService->deleteModel($deliveryPointId);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }	
    
    private function processSorts($oldSorts)
    {
        $newSorts = array();
        foreach ($oldSorts as $oldSort) {
            $sortParts = explode(':',$oldSort);
            $newSorts[] = array(
                'field' => $sortParts[0],
                'order' => $sortParts[1]
            );
        }
        return $newSorts;
    }

    private function processFilters($oldFilters)
    {
        $newFilters = array();
        foreach ($oldFilters as $oldFilter) {
            $filterParts = explode(':',$oldFilter);
            $newFilters[] = array(
                'field' => $filterParts[0],
                'value' => $filterParts[1]
            );
        }
        return $newFilters;
    }
}