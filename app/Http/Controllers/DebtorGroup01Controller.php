<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\DebtorGroup01Service;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;
use App\Repositories\BatchJobStatusRepository;
use Illuminate\Http\Request;


class DebtorGroup01Controller extends Controller
{
    private $debtorGroup01Service;

    public function __construct(DebtorGroup01Service $debtorGroup01Service) {
		$this->debtorGroup01Service = $debtorGroup01Service;
    }

    public function select2()
    {
        try
        {
            $search = Input::post('search','***');
            $filters = Input::post('filters',array());
            $result = $this->debtorGroup01Service->select2($search, $filters);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
}