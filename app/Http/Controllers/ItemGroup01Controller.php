<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\ItemGroup01Service;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;
use App\Repositories\BatchJobStatusRepository;
use Illuminate\Http\Request;


class ItemGroup01Controller extends Controller
{
    private $itemGroup01Service;

    public function __construct(ItemGroup01Service $itemGroup01Service) {
		$this->itemGroup01Service = $itemGroup01Service;
    }

    public function select2()
    {
        try
        {
            $search = Input::post('search','***');
            $filters = Input::post('filters',array());
            $result = $this->itemGroup01Service->select2($search, $filters);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function select2ByDivision()
    {
        try
        {
            $divisionID = Input::post('divisionId', 0);
            $search = Input::post('search','***');
            $filters = Input::post('filters',array());
            $result = $this->itemGroup01Service->select2ByDivision($divisionID, $search, $filters);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function index()
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $sorts = Input::post('sorts', array());
            $sorts = $this->processSorts($sorts);
            $filters = Input::post('filters', array());
            $filters = $this->processFilters($filters);
            $pageSize = Input::post('pageSize', 20);
            $result = $this->itemGroup01Service->index($sorts, $filters, $pageSize);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }	
    public function uploadPhoto($id, Request $request)
    {
        try
        {
            $result = $this->itemGroup01Service->uploadPhoto($id, $request->file);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
	
    public function deletePhoto($id)
    {
        try
        {
            $result = $this->itemGroup01Service->deletePhoto($id);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    //Process functions
    private function processFilters($oldFilters)
    {
        $newFilters = array();
        foreach ($oldFilters as $oldFilter) {
            $filterParts = explode(':',$oldFilter);
            $newFilters[] = array(
                'field' => $filterParts[0],
                'value' => $filterParts[1]
            );
        }
        return $newFilters;
    }	

    private function processSorts($oldSorts)
    {
        $newSorts = array();
        foreach ($oldSorts as $oldSort) {
            $sortParts = explode(':',$oldSort);
            $newSorts[] = array(
                'field' => $sortParts[0],
                'order' => $sortParts[1]
            );
        }
        return $newSorts;
    }
}
