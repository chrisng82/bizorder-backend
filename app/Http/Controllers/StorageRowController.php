<?php

namespace App\Http\Controllers;

use App\Services\Env\ProcType;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Services\StorageRowService;
use App\Services\Utils\ResponseServices;
use Maatwebsite\Excel\Facades\Excel;
use App\Repositories\BatchJobStatusRepository;

class StorageRowController extends Controller
{
    private $storageRowService;

    public function __construct(StorageRowService $storageRowService) {
		$this->storageRowService = $storageRowService;
    }

    public function select2()
    {
        try
        {
            $search = Input::post('search','***');
            $filters = Input::post('filters',array());
            $result = $this->storageRowService->select2($search, $filters);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
}