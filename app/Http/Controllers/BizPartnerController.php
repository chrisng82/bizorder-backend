<?php

namespace App\Http\Controllers;

use App\Services\Utils\ResponseServices;
use App\Services\BizPartnerService;
use Illuminate\Http\Request;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\Input;

class BizPartnerController extends Controller
{
    private $bizPartnerService;

    public function __construct(BizPartnerService $bizPartnerService) 
    {
		$this->bizPartnerService = $bizPartnerService;
    }

    public function select2()
    {
        try
        {
            $search = Input::post('search','***');
            $filters = Input::post('filters',array());
            $result = $this->bizPartnerService->select2($search, $filters);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function select2Init()
    {
        try
        {
            $id = Input::post('id',0);
            $result = $this->bizPartnerService->select2Init($id);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
}