<?php

namespace App\Http\Controllers;

use App\Services\Utils\ResponseServices;
use App\Services\ItemBatchService;
use Illuminate\Http\Request;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\Input;

class ItemBatchController extends Controller
{
    private $itemBatchService;

    public function __construct(ItemBatchService $itemBatchService) 
    {
		$this->itemBatchService = $itemBatchService;
    }

    public function select2()
    {
        try
        {
            $search = Input::post('search','***');
            $filters = Input::post('filters',array());
            $result = $this->itemBatchService->select2($search, $filters);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
}