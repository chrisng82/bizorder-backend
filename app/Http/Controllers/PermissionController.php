<?php
namespace App\Http\Controllers;

use App\Services\Env\ProcType;
use App\Services\Utils\ResponseServices;
use App\Services\PermissionService;
use App\Services\Utils\ApiException;
use App\Repositories\BatchJobStatusRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PermissionController extends Controller
{
    private $permissionService;

    public function __construct(PermissionService $permissionService) {
		$this->permissionService = $permissionService;
    }

    public function select2()
    {
        try
        {
            $search = Input::post('search','***');
            $filters = Input::post('filters',array());
            $result = $this->permissionService->select2($search, $filters);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
}
