<?php

namespace App\Http\Controllers;

use App\Services\Env\ProcType;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Services\AuthService;
use App\Services\StorageBinService;
use App\Exports\StorageBinExcel01Export;
use App\Services\Utils\ResponseServices;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Repositories\BatchJobStatusRepository;

class StorageBinController extends Controller
{
    private $storageBinService;

    public function __construct(StorageBinService $storageBinService) {
		$this->storageBinService = $storageBinService;
    }

    public function indexProcess($strProcType, $siteFlowId)
    {
        try
        {
            $sorts = Input::post('sorts', array(
                array('field'=>'code','order'=>'ASC')
            ));
            $filters = Input::post('filters', array());
            $pageSize = Input::post('pageSize', 21);
            $result = $this->storageBinService->indexProcess($strProcType, $siteFlowId, $sorts, $filters, $pageSize);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function downloadProcess($strProcType, $siteFlowId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);

        AuthService::authorize(
            array(
                'storage_bin_export'
            )
        );

        try
        {
            $user = Auth::user();

            //download excel need to call within controller, else axios will get 0 byte in response body
            if(isset(ProcType::$MAP[$strProcType]))
            {
                if(ProcType::$MAP[$strProcType] == ProcType::$MAP['STORAGE_BIN_EXCEL_01']) 
                {
                    $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['STORAGE_BIN_EXCEL_01'], $user->id);

                    $storageBinExcel01Export = new StorageBinExcel01Export($user->id, $batchJobStatusModel, StorageBinService::$STORAGE_BIN_EXCEL_01);
                    return Excel::download($storageBinExcel01Export, 'STORAGE_BIN_EXCEL_01.XLSX', \Maatwebsite\Excel\Excel::XLSX);
                }
            }            
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function uploadProcess($strProcType, $siteFlowId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $file = Input::file('file');

            $result = $this->storageBinService->uploadProcess($strProcType, $siteFlowId, $file);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(\PhpOffice\PhpSpreadsheet\Reader\Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function select2()
    {
        try
        {
            $search = Input::post('search','***');
            $filters = Input::post('filters',array());
            $result = $this->storageBinService->select2($search, $filters);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function select2Init()
    {
        try
        {
            $id = Input::post('id',0);
            $filters = Input::post('filters',array());
            $result = $this->storageBinService->select2Init($id, $filters);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
}