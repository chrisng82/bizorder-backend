<?php

namespace App\Http\Controllers;

use App\Services\SlsInvService;
use App\Services\Utils\ResponseServices;
use Illuminate\Http\Request;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\Input;

class SlsInvController extends Controller
{
    private $slsInvService;

    public function __construct(SlsInvService $slsInvService) 
    {
		$this->slsInvService = $slsInvService;
    }

    public function showDetails($hdrId)
    {
        try
        {
            $result = $this->slsInvService->showDetails($hdrId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function updateDetails($hdrId)
    {
        try
        {
            $json = Input::post('data');
            $result = $this->slsInvService->updateDetails($hdrId, $json);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function createDetail($hdrId)
    {
        try
        {
            $json = Input::post('data');
            $result = $this->slsInvService->createDetail($hdrId, $json);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function deleteDetails($hdrId)
    {
        try
        {
            $json = Input::post('data');
            $result = $this->slsInvService->deleteDetails($hdrId, $json);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function index($divisionId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $sorts = Input::post('sorts', array());
            $sorts = $this->processSorts($sorts);
            $filters = Input::post('filters', array());
            $filters = $this->processFilters($filters);
            $pageSize = Input::post('pageSize', 20);
            $result = $this->slsInvService->index($divisionId, $sorts, $filters, $pageSize);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function showHeader($hdrId)
    {
        try
        {
            $result = $this->slsInvService->showHeader($hdrId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function syncProcess($strProcType, $divisionId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $startDate = Input::get('startDate', date('Y-m-d'));
            $endDate = Input::get('endDate', date('Y-m-d'));

            $result = $this->slsInvService->syncProcess($strProcType, $divisionId, $startDate, $endDate);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();          
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(ClientException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(ConnectException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function transitionToStatus()
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $hdrId = Input::post('hdrId', 0);
            $docStatus = Input::post('docStatus', 0);
            $result = $this->slsInvService->transitionToStatus($hdrId, $docStatus);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function initHeader($divisionId)
    {
        try
        {
            $result = $this->slsInvService->initHeader($divisionId);
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function updateHeader()
    {
        try
        {
            $json = Input::post('data');
            $result = $this->slsInvService->updateHeader($json);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function createHeader()
    {
        try
        {
            $data = Input::post('data', array());
            $result = $this->slsInvService->createHeader($data);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    private function processSorts($oldSorts)
    {
        $newSorts = array();
        foreach ($oldSorts as $oldSort) {
            $sortParts = explode(':',$oldSort);
            $newSorts[] = array(
                'field' => $sortParts[0],
                'order' => $sortParts[1]
            );
        }
        return $newSorts;
    }

    private function processFilters($oldFilters)
    {
        $newFilters = array();
        foreach ($oldFilters as $oldFilter) {
            $filterParts = explode(':',$oldFilter);
            $newFilters[] = array(
                'field' => $filterParts[0],
                'value' => $filterParts[1]
            );
        }
        return $newFilters;
    }
}