<?php

namespace App\Http\Controllers;

use App\Services\Utils\ResponseServices;
use Illuminate\Http\Request;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\Input;

use App\Services\AreaService;

class AreaController extends Controller
{
    private $areaService;

    public function __construct(AreaService $areaService) 
    {
		$this->areaService = $areaService;
    }
	
    public function select2()
    {
        try
        {
            $search = Input::post('search','***');
            $filters = Input::post('filters',array());
			$pageSize = Input::get('pageSize', 20);
            $result = $this->areaService->select2($search, $filters, $pageSize);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
}
