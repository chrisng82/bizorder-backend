<?php

namespace App\Http\Controllers;

use App\Services\Utils\ResponseServices;
use App\Services\MobileService;
use Illuminate\Http\Request;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

class MobileController extends Controller
{
    private $mobileService;

    public function __construct(MobileService $mobileService) 
    {
		$this->mobileService = $mobileService;
    }

    public function register($deviceUuid, $mobileApp, $mobileOs, $osVersion, $appVersion)
    {
        try
        {
            $result = $this->mobileService->register($deviceUuid, $mobileApp, $mobileOs, $osVersion, $appVersion);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function indexTableSchema($mobileProfileId)
    {
        try
        {
            $result = $this->mobileService->indexTableSchema($mobileProfileId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function indexData($mobileProfileId, $tableName)
    {
        try
        {
            $pageSize = Input::post('pageSize', 10000);
            $result = $this->mobileService->indexData($mobileProfileId, $tableName, $pageSize);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function indexHistory($mobileProfileId, $tableName)
    {
        try
        {
            $pageSize = Input::post('pageSize', 10000);
            $result = $this->mobileService->indexHistory($mobileProfileId, $tableName, $pageSize);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function updateHistory($mobileProfileId, $tableName)
    {
        try
        {
            $json = Input::post('data');
            $result = $this->mobileService->updateHistory($mobileProfileId, $tableName, $json);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function resetHistory($mobileProfileId, $tableName)
    {
        try
        {
            $result = $this->mobileService->resetHistory($mobileProfileId, $tableName);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function resetAllHistory($mobileProfileId)
    {
        try
        {
            $result = $this->mobileService->resetAllHistory($mobileProfileId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
}