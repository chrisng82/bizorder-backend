<?php

namespace App\Http\Controllers;

use App\Services\Utils\ResponseServices;
use App\Services\CreditTermService;
use Illuminate\Http\Request;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\Input;

class CreditTermController extends Controller
{
    private $creditTermService;

    public function __construct(CreditTermService $creditTermService) 
    {
		$this->creditTermService = $creditTermService;
    }

    public function select2()
    {
        try
        {
            $search = Input::post('search','***');
            $filters = Input::post('filters',array());
            $result = $this->creditTermService->select2($search, $filters);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
}