<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Access\AccessController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\View\View;
use Illuminate\Routing\Route;
use ReflectionClass;
use ReflectionMethod;
use App\Services\Utils\ResponseServices;

/**
 * Class LabelController
 * @package App\Http\Controllers\Test
 */
class LabelController extends Controller
{
    private $labels;

    public function setRules($label_rule_slug)
    {
        $this->labels = $this->labels->where('slug','=', $label_rule_slug);
        return $this;
    }

    public function setLanguage($language)
    {
        $this->labels = $this->labels->where('language','=',$language);
        return $this;
    }

    public function getLabels($label_slug = null)
    {
        if($label_slug == null){
            return $this->labels;
        } else if(is_array($label_slug))  {
            return '';
        } else {
            return $this->labels->where('slug','=',$label_slug);
        }
    }

    public function getLabel($controller)
    {
        $response = __(''.$controller.'');
        return ResponseServices::success()->data($response)->toJson();
    }
    
    public function getLanguage()
    {
        $data['Audit'] = __('Audit');
        $data['General'] = __('General');
        $data['Item'] = __('Item');
        $data['InbOrd'] = __('InbOrd');
        $data['OutbOrd'] = __('OutbOrd');
        $data['AdvShip'] = __('AdvShip');
        $data['GdsRcpt'] = __('GdsRcpt');
        $data['PutAway'] = __('PutAway');
        $data['PickList'] = __('PickList');
        $data['CountAdj'] = __('CountAdj');
        $data['CycleCount'] = __('CycleCount');
        $data['WhseJob'] = __('WhseJob');
        $data['SlsOrd'] = __('SlsOrd');
        $data['InvDoc'] = __('InvDoc');
        $data['PackList'] = __('PackList');        
        $data['LoadList'] = __('LoadList');        
        $data['PrfDel'] = __('PrfDel');        
        $data['FailDel'] = __('FailDel');
        $data['ProcType'] = __('ProcType');
        $data['StorageBin'] = __('StorageBin');
        $data['DeliveryPoint'] = __('DeliveryPoint');
        $data['HandlingUnit'] = __('HandlingUnit');
        $data['PurInv'] = __('PurInv');
        $data['WhseReport'] = __('WhseReport');
        $data['BinTrf'] = __('BinTrf');
        $data['SlsInv'] = __('SlsInv');
        $data['SlsRtn'] = __('SlsRtn');
        $data['RtnRcpt'] = __('RtnRcpt');
        $data['SalesReport'] = __('SalesReport');
        $data['PickFaceStrategy'] = __('PickFaceStrategy');
        $data['Role'] = __('Role');
        $data['Permission'] = __('Permission');
        $data['User'] = __('User');
        $data['DelOrd'] = __('DelOrd');
        $response = array(
            'timestamp'=>time(),
            'data'=>$data,
        );
        return ResponseServices::success()->data($response)->toJson();
    }
}