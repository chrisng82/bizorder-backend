<?php

namespace App\Http\Controllers;

use App\Services\Utils\ResponseServices;
use App\Services\ItemUomService;
use Illuminate\Http\Request;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\Input;

class ItemUomController extends Controller
{
    private $itemUomService;

    public function __construct(ItemUomService $itemUomService) 
    {
		$this->itemUomService = $itemUomService;
    }

    public function select2()
    {
        try
        {
            $search = Input::post('search','***');
            $filters = Input::post('filters',array());
            $result = $this->itemUomService->select2($search, $filters);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function deleteItemUom($item_sale_prices_id){
        try
        {
            $result = $this->itemUomService->deleteItemUom($item_sale_prices_id);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
}