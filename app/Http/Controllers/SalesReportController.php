<?php

namespace App\Http\Controllers;

use App\Services\Env\ProcType;
use App\Services\SalesReportService;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;
use App\Repositories\BatchJobStatusRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;

class SalesReportController extends Controller
{
    private $salesReportService;

    public function __construct(SalesReportService $salesReportService) 
    {
        $this->salesReportService = $salesReportService;
    }

    public function outbOrdAnalysis($siteFlowId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $pageSize = 0;
            $sorts = Input::post('sorts');
            $criteria = Input::post('criteria');
            $columns = Input::post('columns');
            
            $result = $this->salesReportService->outbOrdAnalysis($siteFlowId, $sorts, $criteria, $columns, $pageSize);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function initOutbOrdAnalysis($siteFlowId)
    {
        try
        {
            $result = $this->salesReportService->initOutbOrdAnalysis($siteFlowId);
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
}