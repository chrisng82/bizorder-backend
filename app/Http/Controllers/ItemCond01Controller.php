<?php

namespace App\Http\Controllers;

use App\Services\Utils\ResponseServices;
use App\Services\ItemCond01Service;
use Illuminate\Http\Request;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\Input;

class ItemCond01Controller extends Controller
{
    private $itemCond01Service;

    public function __construct(ItemCond01Service $itemCond01Service) 
    {
		$this->itemCond01Service = $itemCond01Service;
    }

    public function select2()
    {
        try
        {
            $search = Input::post('search','***');
            $filters = Input::post('filters',array());
            $result = $this->itemCond01Service->select2($search, $filters);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
}