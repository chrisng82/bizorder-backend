<?php

namespace App\Http\Controllers;

use App\Services\Env\ProcType;
use App\Http\Controllers\Controller;
use App\Services\AuthService;
use App\Services\ItemService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Services\Utils\ResponseServices;
use App\Exports\ItemExcel01Export;
use App\Services\Utils\ApiException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Repositories\BatchJobStatusRepository;
use Illuminate\Support\Facades\Log;

class ItemController extends Controller
{
    private $itemService;

    public function __construct(ItemService $itemService) {
		$this->itemService = $itemService;
    }
	
    private function processSorts($oldSorts)
    {
        $newSorts = array();
        foreach ($oldSorts as $oldSort) {
            $sortParts = explode(':',$oldSort);
            $newSorts[] = array(
                'field' => $sortParts[0],
                'order' => $sortParts[1]
            );
        }
        return $newSorts;
    }

    private function processFilters($oldFilters)
    {
        $newFilters = array();
        foreach ($oldFilters as $oldFilter) {
            $filterParts = explode(':',$oldFilter);
            //If value is an array, convert from string to array
            if(strpos($filterParts[1], ',') !== false) {
                $filterParts[1] = explode(',', $filterParts[1]);
            }
            $newFilters[] = array(
                'field' => $filterParts[0],
                'value' => $filterParts[1]
            );
        }
        return $newFilters;
    }

    public function indexProcess($strProcType)
    {
        try
        {
            $sorts = Input::get('sorts', array());
            $sorts = $this->processSorts($sorts);
            $filters = Input::get('filters', array());
			$filters = $this->processFilters($filters);
            $pageSize = Input::get('pageSize', 20);
            $result = $this->itemService->indexProcess($strProcType, $sorts, $filters, $pageSize);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function indexDivision($divisionId)
    {
        try
        {
            $sorts = Input::get('sorts', array());
            $sorts = $this->processSorts($sorts);
            $filters = Input::get('filters', array());
			$filters = $this->processFilters($filters);
            $pageSize = Input::get('pageSize', 20);
            $status  = Input::get('status');
            $result = $this->itemService->indexDivision($divisionId, $sorts, $filters, $pageSize,$status);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function uploadProcess($strProcType, $siteFlowId, $divisionId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $file = Input::file('file');

            $result = $this->itemService->uploadProcess($strProcType, $siteFlowId, $divisionId, $file);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(\PhpOffice\PhpSpreadsheet\Reader\Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function downloadProcess($strProcType, $siteFlowId, $divisionId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);

        AuthService::authorize(
            array(
                'item_export'
            )
        );

        try
        {
            $user = Auth::user();

            //download excel need to call within controller, else axios will get 0 byte in response body
            if(isset(ProcType::$MAP[$strProcType]))
            {
                if(ProcType::$MAP[$strProcType] == ProcType::$MAP['ITEM_EXCEL_01']) 
                {
                    $batchJobStatusModel = BatchJobStatusRepository::firstOrCreate(ProcType::$MAP['ITEM_EXCEL_01'], $user->id);

                    $itemExcel01Export = new ItemExcel01Export($user->id, $batchJobStatusModel);
                    return Excel::download($itemExcel01Export, 'ITEM_EXCEL_01.XLSX', \Maatwebsite\Excel\Excel::XLSX);
                }
            }            
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function syncProcess($strProcType, $siteFlowId, $divisionId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $result = $this->itemService->syncProcess($strProcType, $siteFlowId, $divisionId);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();          
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(ClientException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(ConnectException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function select2Item(){
        try
        {
            $data = Input::post('data','');
            $result = ItemService::select2Item($data);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function select2UpdateItem($item_id){
        try
        {
            $result = ItemService::select2UpdateItem($item_id);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function getItemUom($item_id){
        try
        {
            $result = ItemService::getItemUom($item_id);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    


    public function select2()
    {
        try
        {
            $search = Input::post('search','***');
            $filters = Input::post('filters',array());
            $result = $this->itemService->select2($search, $filters);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function select2Init()
    {
        try
        {
            $id = Input::post('id',0);
            $result = $this->itemService->select2Init($id);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function initModel()
    {
        try
        {
            $result = $this->itemService->initModel();
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function showModel($itemId)
    {
        try
        {
            $result = $this->itemService->showModel($itemId);
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function createModel($divisionId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);

        try
        {
            $data = Input::post('data', array());
            $result = $this->itemService->createModel($data, $divisionId);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function updateModel()
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);

        try
        {
            $json = Input::post('data');
            $result = $this->itemService->updateModel($json);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(PDOException $exp)
        {
            // if(strpos($exp->getMessage(), 'users_email_unique') !== false) 
            // {
            //     $message = __('User.email_is_already_in_use_by_another_user');
            //     return ResponseServices::error($message)->toJson();
            // }
            // elseif(strpos($exp->getMessage(), 'users_username_unique') !== false)
            // {
            //     $message = __('User.username_is_not_available');
            //     return ResponseServices::error($message)->toJson();
            // }
            // else 
            // {                
                return ResponseServices::error($exp->getMessage())->toJson();
            // }
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function createPriceModel()
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);

        try
        {
            $data = Input::post('data', array());
            $result = $this->itemService->createPriceModel($data);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function updatePriceModel()
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);

        try
        {
            $json = Input::post('data');
            $result = $this->itemService->updatePriceModel($json);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function uploadPhotos($itemId, Request $request)
    {
        try
        {
            //$file = Input::file('file');
            $result = $this->itemService->uploadPhotos($itemId, $request->files);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function deletePhotos($itemId)
    {
        try
        {
            $json = Input::post('data');
            $result = $this->itemService->deletePhotos($itemId, $json);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function resetPhotos($divisionId)
    {
        try
        {
            $result = $this->itemService->resetPhotos($divisionId);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

	public function itemGroup01() 
	{
		try
        {
            $search = Input::post('search','***');
            $filters = Input::post('filters',array());
            $filters = $this->processFilters($filters);
            $sorts = Input::post('sorts', array());
            $sorts = $this->processSorts($sorts);
            $result = $this->itemService->getItemGroup01($search, $filters, $sorts);
            //return ResponseServices::success()->data($filters)->toJson();
			return ResponseServices::success()->data($result)->toJson();
		}
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
	}

	public function itemGroup02() 
	{
		try
        {
            $search = Input::post('search','***');
            $filters = Input::post('filters',array());
			$filters = $this->processFilters($filters);
            $result = $this->itemService->getItemGroup02($search, $filters);
            //return ResponseServices::success()->data($filters)->toJson();
			return ResponseServices::success()->data($result)->toJson();
		}
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
	}

	public function itemGroup03() 
	{
		try
        {
            $search = Input::post('search','***');
            $filters = Input::post('filters',array());
			$filters = $this->processFilters($filters);
            $result = $this->itemService->getItemGroup03($search, $filters);
            //return ResponseServices::success()->data($filters)->toJson();
			return ResponseServices::success()->data($result)->toJson();
		}
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
	}
}
