<?php

namespace App\Http\Controllers;

use App\Services\Utils\ResponseServices;
use App\Services\PromotionService;
use Illuminate\Http\Request;
use App\Services\Utils\ApiException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Facades\Input;

class PromotionController extends Controller
{
    private $promotionService;

    public function __construct(PromotionService $promotionService) 
    {
		$this->promotionService = $promotionService;
    }

    public function broadcastPromo()
    {
        try
        {
            $data = Input::post('data', array());
            $divisionId = Input::post('divisionId', 0);
            $result_array = $this->promotionService->broadcastPromo($data, $divisionId);

            $result = $result_array['result'];
            $message = $result_array['message'];

            return ResponseServices::success($message)->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function changeItemGroup()
    {
        try
        {
            $promotionId = Input::post('promotionId', 0);
            $result = $this->promotionService->changeItemGroup($promotionId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function changeItem()
    {
        try
        {
            $itemId = Input::post('itemId', 0);
            $promotionId = Input::post('promotionId', 0);
            $result = $this->promotionService->changeItem($itemId, $promotionId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function createModel($divisionId, $strPromoType)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);

        try
        {
            $json = Input::post('data');
            $result = $this->promotionService->createModel($divisionId, $strPromoType, $json);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function createRule($promotionId)
    {
        try
        {
            $json = Input::post('data');
            $result = $this->promotionService->createRule($promotionId, $json);
			$data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        } catch (ApiException $exp) {
            return ResponseServices::error($exp->getMessage())->toJson();
        } catch (Exception $exp) {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function createVariant($promotionId)
    {
        try
        {
            $json = Input::post('data');
            $result = $this->promotionService->createVariant($promotionId, $json);
			$data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        } catch (ApiException $exp) {
            return ResponseServices::error($exp->getMessage())->toJson();
        } catch (Exception $exp) {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function deleteModel()
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);

        try
        {
            $json = Input::post('data');
            $result = $this->promotionService->deleteModel($json);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function deletePhotos($promotionId)
    {
        try
        {
            $result = $this->promotionService->deletePhotos($promotionId);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function deleteRule($promotionId)
    {
        try
        {
            $json = Input::post('data');
            $result = $this->promotionService->deleteRule($promotionId, $json);
			$data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        } catch (ApiException $exp) {
            return ResponseServices::error($exp->getMessage())->toJson();
        } catch (Exception $exp) {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function deleteVariant($promotionId)
    {
        try
        {
            $json = Input::post('data');
            $result = $this->promotionService->deleteVariant($promotionId, $json);
			$data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        } catch (ApiException $exp) {
            return ResponseServices::error($exp->getMessage())->toJson();
        } catch (Exception $exp) {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function findAllActiveByItem($itemId)
    {
        try
        {
            $result = $this->promotionService->findAllActiveByItem($itemId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function findAllRelatedItems()
    {
        try
        {
            $sorts = Input::post('sorts',array());
            $sorts = $this->processSorts($sorts);
            $filters = Input::post('filters',array());
            $filters = $this->processFilters($filters);
            $promotionIds = Input::post('promotionIds',array());
            $result = $this->promotionService->findAllRelatedItems($sorts, $filters, $promotionIds);
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function findPromotion()
    {
        try {
            $promotion_id = Input::post('promotionId', 0);
            $result = $this->promotionService->findPromotion($promotion_id);
            return ResponseServices::success()->data($result)->toJson();
        } catch (ApiException $exp) {
            return ResponseServices::error($exp->getMessage())->toJson();
        } catch (Exception $exp) {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function findPromotionItems()
    {
        try {
            $promotion_id = Input::post('promotionId', 0);
            $result = $this->promotionService->findPromotionItems($promotion_id);
            return ResponseServices::success()->data($result)->toJson();
        } catch (ApiException $exp) {
            return ResponseServices::error($exp->getMessage())->toJson();
        } catch (Exception $exp) {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function index($divisionId)
    {
        try
        {
            $sorts = Input::post('sorts', array());
            $sorts = $this->processSorts($sorts);
            $filters = Input::post('filters', array());
            $filters = $this->processFilters($filters);
            $pageSize = Input::post('pageSize', 20);
            $result = $this->promotionService->index($divisionId, $sorts, $filters, $pageSize);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function indexDisc($divisionId)
    {
        try
        {
            $sorts = Input::post('sorts', array());
            $sorts = $this->processSorts($sorts);
            $filters = Input::post('filters', array());
            $filters = $this->processFilters($filters);
            $pageSize = Input::post('pageSize', 20);
            $result = $this->promotionService->indexDisc($divisionId, $sorts, $filters, $pageSize);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function indexFoc($divisionId)
    {
        try
        {
            $sorts = Input::post('sorts', array());
            $sorts = $this->processSorts($sorts);
            $filters = Input::post('filters', array());
            $filters = $this->processFilters($filters);
            $pageSize = Input::post('pageSize', 20);
            $result = $this->promotionService->indexFoc($divisionId, $sorts, $filters, $pageSize);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function initModel($divisionId, $strPromoType)
    {
        try
        {
            $result = $this->promotionService->initModel($divisionId, $strPromoType);
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function reorderPromo()
    {
        try
        {
            $curPromoId = Input::post('curPromoId', 0);
            $prevPromoId = Input::post('prevPromoId', 0);
            $curDivisionId = Input::post('curDivisionId', 0);

            $result = $this->promotionService->reorderPromo($curPromoId, $prevPromoId, $curDivisionId);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        } 
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function select2()
    {
        try
        {
            $search = Input::post('search','***');
            $filters = Input::post('filters',array());
            $result = $this->promotionService->select2($search, $filters);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function select2Init()
    {
        try
        {
            $id = Input::post('id',0);
            $result = $this->promotionService->select2Init($id);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function showAddOnVariants($promotionId)
    {
        try
        {
            $result = $this->promotionService->showVariants($promotionId, 'add_on');
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function showMainVariants($promotionId)
    {
        try
        {
            $result = $this->promotionService->showVariants($promotionId, 'main');
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function showModel($promotionId)
    {
        try
        {
            $result = $this->promotionService->showModel($promotionId);
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function showRules($promotionId)
    {
        try
        {
            $result = $this->promotionService->showRules($promotionId);
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function showVariants($promotionId)
    {
        try
        {
            $result = $this->promotionService->showVariants($promotionId);
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function syncProcess($strProcType, $siteFlowId, $divisionId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $result = $this->promotionService->syncProcess($strProcType, $siteFlowId, $divisionId);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();         
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(ClientException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(ConnectException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function updateModel()
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);

        try
        {
            $json = Input::post('data');
            $result = $this->promotionService->updateModel($json);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function uploadPhotos($promotionId, Request $request)
    {
        try
        {
            $file = Input::file('file');
            $result = $this->promotionService->uploadPhotos($promotionId, $request->files);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function updateRule($promotionId)
    {
        try
        {
            $json = Input::post('data');
            $result = $this->promotionService->updateRule($promotionId, $json);
			$data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        } catch (ApiException $exp) {
            return ResponseServices::error($exp->getMessage())->toJson();
        } catch (Exception $exp) {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
	
    public function updateVariant($promotionId)
    {
        try
        {
            $json = Input::post('data');
            $result = $this->promotionService->updateVariant($promotionId, $json);
			$data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        } catch (ApiException $exp) {
            return ResponseServices::error($exp->getMessage())->toJson();
        } catch (Exception $exp) {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    private function processSorts($oldSorts)
    {
        $newSorts = array();
        foreach ($oldSorts as $oldSort) {
            $sortParts = explode(':',$oldSort);
            $newSorts[] = array(
                'field' => $sortParts[0],
                'order' => $sortParts[1]
            );
        }
        return $newSorts;
    }

    private function processFilters($oldFilters)
    {
        $newFilters = array();
        foreach ($oldFilters as $oldFilter) {
            $filterParts = explode(':',$oldFilter);
            $newFilters[] = array(
                'field' => $filterParts[0],
                'value' => $filterParts[1]
            );
        }
        return $newFilters;
    }
}