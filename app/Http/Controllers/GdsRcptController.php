<?php
namespace App\Http\Controllers;

use App\Services\Utils\ResponseServices;
use App\Services\GdsRcptService;
use Illuminate\Http\Request;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\Input;

class GdsRcptController extends Controller
{
    private $gdsRcptService;

    public function __construct(GdsRcptService $gdsRcptService) {
		$this->gdsRcptService = $gdsRcptService;
    }
    
    public function indexProcess($strProcType, $siteFlowId)
    {
        try
        {
            $sorts = Input::post('sorts', array());
            $sorts = $this->processSorts($sorts);
            $filters = Input::post('filters', array());
            $filters = $this->processFilters($filters);
            $pageSize = Input::post('pageSize', 20);
            $result = $this->gdsRcptService->indexProcess($strProcType, $siteFlowId, $sorts, $filters, $pageSize);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function createProcess($strProcType)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $hdrIds = Input::post('hdrIds', array());
            $toStorageBinId = Input::post('toStorageBinId', 0);
            $result = $this->gdsRcptService->createProcess($strProcType, $hdrIds, $toStorageBinId);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            $data = array(
                array(
                    'doc_code'=>'', 
                    'doc_date'=>date('Y-m-d'),
                    'str_doc_status'=>$exp->getMessage(),
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                )
            );
            return ResponseServices::error($exp->getMessage())->data($data)->toJson();
        }
        catch(Exception $exp)
        {
            $data = array(
                array(
                    'doc_code'=>'', 
                    'doc_date'=>date('Y-m-d'),
                    'str_doc_status'=>$exp->getMessage(),
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                )
            );
            return ResponseServices::error($exp->getMessage())->data($data)->toJson();
        }
    }

    public function batchPrintDocuments($hdrId)
    {
        try
        {
            $result = $this->pickListService->batchPrintDocuments($hdrId);
            
            $pdf_setting = config('pdf_file_setting');
            $pdf = \Illuminate\Support\Facades\App::make('dompdf.wrapper');
            $pdf->getDomPDF()->set_option("enable_php", true);
            $pdf->setPaper(array(
                0,
                0,
                $pdf_setting['height'], 
                $pdf_setting['width']
            ), 'landscape');
            $pdf->loadHTML(
                view('pickList',
                    ['data' => $result]
                )
            );
            return $pdf->stream();

            // return view('pickList',
            //         ['data' => $result]
            // );
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function revertProcess($hdrId)
    {
        try
        {
            $result = $this->gdsRcptService->revertProcess($hdrId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function submitProcess($hdrId)
    {
        try
        {
            $result = $this->gdsRcptService->submitProcess($hdrId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function completeProcess($hdrId)
    {
        try
        {
            $result = $this->gdsRcptService->completeProcess($hdrId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function transitionToStatus()
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $hdrId = Input::post('hdrId', 0);
            $docStatus = Input::post('docStatus', 0);
            $result = $this->gdsRcptService->transitionToStatus($hdrId, $docStatus);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function verifyTxn()
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $result = $this->gdsRcptService->verifyTxn();
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    private function processSorts($oldSorts)
    {
        $newSorts = array();
        foreach ($oldSorts as $oldSort) {
            $sortParts = explode(':',$oldSort);
            $newSorts[] = array(
                'field' => $sortParts[0],
                'order' => $sortParts[1]
            );
        }
        return $newSorts;
    }

    private function processFilters($oldFilters)
    {
        $newFilters = array();
        foreach ($oldFilters as $oldFilter) {
            $filterParts = explode(':',$oldFilter);
            $newFilters[] = array(
                'field' => $filterParts[0],
                'value' => $filterParts[1]
            );
        }
        return $newFilters;
    }

    public function showHeader($hdrId)
    {
        try
        {
            $result = $this->gdsRcptService->showHeader($hdrId);
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function showDetails($hdrId)
    {
        try
        {
            $result = $this->gdsRcptService->showDetails($hdrId);
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function updateDetails($hdrId)
    {
        try
        {
            $json = Input::post('data');
            $result = $this->gdsRcptService->updateDetails($hdrId, $json);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function createDetail($hdrId)
    {
        try
        {
            $json = Input::post('data');
            $result = $this->gdsRcptService->createDetail($hdrId, $json);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function deleteDetails($hdrId)
    {
        try
        {
            $json = Input::post('data');
            $result = $this->gdsRcptService->deleteDetails($hdrId, $json);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function updateHeader()
    {
        try
        {
            $json = Input::post('data');
            $result = $this->gdsRcptService->updateHeader($json);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function changeItemUom()
    {
        try
        {
            $hdrId = Input::post('hdrId', 0);
            $itemId = Input::post('itemId', 0);
            $uomId = Input::post('uomId', 0);
            $result = $this->gdsRcptService->changeItemUom($hdrId, $itemId, $uomId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function changeItemBatch()
    {
        try
        {
            $hdrId = Input::post('hdrId', 0);
            $itemBatchId = Input::post('itemBatchId', 0);
            $result = $this->gdsRcptService->changeItemBatch($hdrId, $itemBatchId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function changeItem()
    {
        try
        {
            $hdrId = Input::post('hdrId', 0);
            $itemId = Input::post('itemId', 0);
            $result = $this->gdsRcptService->changeItem($hdrId, $itemId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function index($siteFlowId)
    {
        try
        {
            $sorts = Input::post('sorts', array());
            $sorts = $this->processSorts($sorts);
            $filters = Input::post('filters', array());
            $filters = $this->processFilters($filters);
            $pageSize = Input::post('pageSize', 20);
            $result = $this->gdsRcptService->index($siteFlowId, $sorts, $filters, $pageSize);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function printProcess($strProcType, $siteFlowId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $hdrIds = Input::get('hdrIds');
            $result = $this->gdsRcptService->printProcess($strProcType, $siteFlowId, $hdrIds);
            return $result;           
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
}
