<?php

namespace App\Http\Controllers;

use App\Services\Utils\ResponseServices;
use App\Services\SiteService;
use Illuminate\Http\Request;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\Input;

class SiteController extends Controller
{
    private $siteService;

    public function __construct(SiteService $siteService) 
    {
		$this->siteService = $siteService;
    }

    public function indexSiteFlow()
    {
        try
        {
            $result = $this->siteService->indexSiteFlow();
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function indexGdsDelFlow($siteFlowId)
    {
        try
        {
            $result = $this->siteService->indexGdsDelFlow($siteFlowId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function indexGdsRcptFlow($siteFlowId)
    {
        try
        {
            $result = $this->siteService->indexGdsRcptFlow($siteFlowId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function indexInventoryAuditFlow($siteFlowId)
    {
        try
        {
            $result = $this->siteService->indexInventoryAuditFlow($siteFlowId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function indexBinTransferFlow($siteFlowId)
    {
        try
        {
            $result = $this->siteService->indexBinTransferFlow($siteFlowId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function indexRtnRcptFlow($siteFlowId)
    {
        try
        {
            $result = $this->siteService->indexRtnRcptFlow($siteFlowId);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
}