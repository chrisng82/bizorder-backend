<?php

namespace App\Http\Controllers;

use App\Services\Utils\ResponseServices;
use App\Services\BatchJobStatusService;
use Illuminate\Http\Request;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\Input;

class BatchJobStatusController extends Controller
{
    private $batchJobStatusService;

    public function __construct(BatchJobStatusService $batchJobStatusService) 
    {
		$this->batchJobStatusService = $batchJobStatusService;
    }

    public function showBatchJobStatus($strProcType)
    {
        try
        {
            $result = $this->batchJobStatusService->showBatchJobStatus($strProcType);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function resetBatchJobStatus($strProcType)
    {
        try
        {
            $result = $this->batchJobStatusService->resetBatchJobStatus($strProcType);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
}