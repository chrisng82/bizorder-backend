<?php

namespace App\Http\Controllers;

use App\Services\Env\ProcType;
use App\Services\WhseReportService;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;
use App\Repositories\BatchJobStatusRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;

class WhseReportController extends Controller
{
    private $whseReportService;

    public function __construct(WhseReportService $whseReportService) 
    {
        $this->whseReportService = $whseReportService;
    }

    public function stockBalance($siteFlowId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $pageSize = 0;
            $sorts = Input::post('sorts');
            $criteria = Input::post('criteria');
            $columns = Input::post('columns');
            
            $result = $this->whseReportService->stockBalance($siteFlowId, $sorts, $criteria, $columns, $pageSize);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function initStockBalance($siteFlowId)
    {
        try
        {
            $result = $this->whseReportService->initStockBalance($siteFlowId);
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function cycleCountAnalysis($siteFlowId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $pageSize = 0;
            $sorts = Input::post('sorts');
            $criteria = Input::post('criteria');
            $columns = Input::post('columns');
            
            $result = $this->whseReportService->cycleCountAnalysis($siteFlowId, $sorts, $criteria, $columns, $pageSize);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function initCycleCountAnalysis($siteFlowId)
    {
        try
        {
            $result = $this->whseReportService->initCycleCountAnalysis($siteFlowId);
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function stockCard($siteFlowId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $pageSize = 0;
            $sorts = Input::post('sorts');
            $criteria = Input::post('criteria');
            $columns = Input::post('columns');
            
            $result = $this->whseReportService->stockCard($siteFlowId, $sorts, $criteria, $columns, $pageSize);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function initStockCard($siteFlowId)
    {
        try
        {
            $result = $this->whseReportService->initStockCard($siteFlowId);
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function countAdjAnalysis($siteFlowId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $pageSize = 0;
            $sorts = Input::post('sorts');
            $criteria = Input::post('criteria');
            $columns = Input::post('columns');
            
            $result = $this->whseReportService->countAdjAnalysis($siteFlowId, $sorts, $criteria, $columns, $pageSize);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function initCountAdjAnalysis($siteFlowId)
    {
        try
        {
            $result = $this->whseReportService->initCountAdjAnalysis($siteFlowId);
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function reservedStock($siteFlowId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $pageSize = 0;
            $sorts = Input::post('sorts');
            $criteria = Input::post('criteria');
            $columns = Input::post('columns');
            
            $result = $this->whseReportService->reservedStock($siteFlowId, $sorts, $criteria, $columns, $pageSize);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function initReservedStock($siteFlowId)
    {
        try
        {
            $result = $this->whseReportService->initReservedStock($siteFlowId);
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
}