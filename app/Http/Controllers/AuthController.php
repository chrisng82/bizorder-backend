<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Input;
use App\Services\AuthService;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;

class AuthController extends Controller
{
    private $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function authenticate()
    {
        $username = Input::post('username');
        $password = Input::post('password');
        $rememberMe = Input::post('remember_me');

        return $this->authService->authenticate($username, $password, $rememberMe);
    }

    public function logout()
    {
        $fcmToken = Input::post('fcmToken') ?? '';
        return $this->authService->logout($fcmToken);
    }

    public function changePassword()
    {
        try
        {
            $currentPassword = Input::post('currentPassword', '');
            $newPassword = Input::post('newPassword', '');
            $result = $this->authService->changePassword($currentPassword, $newPassword);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
}
