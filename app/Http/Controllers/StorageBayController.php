<?php

namespace App\Http\Controllers;

use App\Services\Env\ProcType;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Services\StorageBayService;
use App\Services\Utils\ResponseServices;

class StorageBayController extends Controller
{
    private $storageBayService;

    public function __construct(StorageBayService $storageBayService) {
		$this->storageBayService = $storageBayService;
    }

    public function select2()
    {
        try
        {
            $search = Input::post('search','***');
            $filters = Input::post('filters',array());
            $result = $this->storageBayService->select2($search, $filters);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
}