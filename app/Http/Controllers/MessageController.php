<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\AuthService;
use App\Services\MessageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Services\Utils\ResponseServices;
use App\Services\Utils\ApiException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class MessageController extends Controller
{
    private $messageService;

    public function __construct(MessageService $messageService) {
		$this->messageService = $messageService;
    }

    public function index($userId)
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);
        
        try
        {
            $sorts = Input::post('sorts', array());
            $sorts = $this->processSorts($sorts);
            $filters = Input::post('filters', array());
            $filters = $this->processFilters($filters);
            $pageSize = Input::post('pageSize', 20);
            $result = $this->messageService->index($userId, $sorts, $filters, $pageSize);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    public function showModel($id)
    {
        try
        {
            $result = $this->messageService->showModel($id);
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function initModel()
    {
        try
        {
            $result = $this->messageService->initModel();
            return ResponseServices::success()->data($result)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function createModel()
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);

        try
        {
            $json = Input::post('data');
            $result = $this->messageService->createModel($json);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function updateModel()
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);

        try
        {
            $json = Input::post('data');
            $result = $this->messageService->updateModel($json);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    public function deleteModel($id)
    {
        try
        {
            $result = $this->messageService->deleteModel($id);
            $data = $result['data'];
            $message = $result['message'];
            return ResponseServices::success($message)->data($data)->toJson();
        } 
        catch (ApiException $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        } 
        catch (Exception $exp) 
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }	

    public function markAllAsSeen()
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);

        try
        {
            $sorts = Input::post('sorts', array());
            $sorts = $this->processSorts($sorts);
            $filters = Input::post('filters', array());
            $filters = $this->processFilters($filters);
            $currentPage = Input::post('currentPage');
            $pageSize = Input::post('pageSize', 20);
            $userId = Input::post('userId');
            $json = Input::post('data');
            $result = $this->messageService->markAllAsSeen($userId, $sorts, $filters, $currentPage, $pageSize, $json);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }

    public function clearNotifications()
    {
        ini_set('max_execution_time', 0); //set to max seconds
        ini_set('memory_limit', -1);

        try
        {
            $sorts = Input::post('sorts', array());
            $sorts = $this->processSorts($sorts);
            $filters = Input::post('filters', array());
            $filters = $this->processFilters($filters);
            $userId = Input::post('userId', 0);
            $result = $this->messageService->clearNotifications($userId, $sorts, $filters);
            return ResponseServices::success()->data($result)->toJson();
        }
        catch(ApiException $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
        catch(Exception $exp)
        {
            return ResponseServices::error($exp->getMessage())->toJson();
        }
    }
    
    private function processSorts($oldSorts)
    {
        $newSorts = array();
        foreach ($oldSorts as $oldSort) {
            $sortParts = explode(':',$oldSort);
            $newSorts[] = array(
                'field' => $sortParts[0],
                'order' => $sortParts[1]
            );
        }
        return $newSorts;
    }

    private function processFilters($oldFilters)
    {
        $newFilters = array();
        foreach ($oldFilters as $oldFilter) {
            $filterParts = explode(':',$oldFilter);
            $newFilters[] = array(
                'field' => $filterParts[0],
                'value' => $filterParts[1]
            );
        }
        return $newFilters;
    }
}