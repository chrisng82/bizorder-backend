<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use App\Services\Utils\ResponseServices;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('login');
        }
    }

    public function handle($request, Closure $next, ...$guards)
    {
        $data['token'] = false;
        try 
        {
            $this->authenticate($request, $guards);
        } 
        catch (AuthenticationException $e) 
        {
            return ResponseServices::error(__('Auth.token_expired', []))->data($data)->toJson();
        }

        return $next($request);
    }
}
