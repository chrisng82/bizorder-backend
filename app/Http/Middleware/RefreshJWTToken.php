<?php

/*
 * This file is part of jwt-auth.
 *
 * (c) Sean Tymon <tymon148@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Http\Middleware;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use App\Services\Utils\ResponseServices;

class RefreshJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        $response = $next($request);

        $data['token'] = false;
        try 
        {
            $newToken = JWTAuth::parseToken()->refresh();
        } 
        catch (TokenExpiredException $e) 
        {
            return ResponseServices::error(__('Auth.token_expired', []))->data($data)->toJson();
        } 
        catch (JWTException $e) 
        {
            return ResponseServices::error(__('Auth.token_invalid', []))->data($data)->toJson();
        }
        
        // send the refreshed token back to the client
        $response->headers->set('Access-Control-Expose-Headers', 'Authorization');
        $response->headers->set('Authorization', 'Bearer '.$newToken);

        return $response;
    }
}
