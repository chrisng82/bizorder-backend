<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use JWTAuth;
use Illuminate\Http\Request;
use App\Services\Utils\ResponseServices;
class VerifyJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $data['token'] = false;
        try 
        {
            if(!$user = JWTAuth::parseToken()->authenticate()) 
            {
                return ResponseServices::error(__('Auth.token_cannot_be_validated', []))->data($data)->toJson();
            }
        } 
        catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) 
        {
            return ResponseServices::error(__('Auth.token_expired', []))->data($data)->toJson();
        } 
        catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) 
        {
            return ResponseServices::error(__('Auth.token_invalid', []))->data($data)->toJson();
        } 
        catch (\Tymon\JWTAuth\Exceptions\JWTException $e) 
        {
            return ResponseServices::error(__('Auth.token_absent', []))->data($data)->toJson();
        }

        return $next($request);
    }
}