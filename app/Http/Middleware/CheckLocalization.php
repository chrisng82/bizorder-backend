<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;

class CheckLocalization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        switch ($lang = Input::get('lang')) {
            case 'en':
                App::setLocale('en');
                break;
            case 'ms':
                App::setLocale('ms');
                break;
            case 'ch':
                App::setLocale('ch');
                break;
            default:
                App::setLocale('en');

        }

        $request->headers->set('Accept', 'application/json');

        return $next($request);
    }

}
