<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Services\Utils\ResponseServices;

class CheckPasswordExpired
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if(!empty($user)
        && strcmp($user->username, 'admin') != 0)
        {  
            //Check if first time login, prompt to change password
            $password_changed_at = $user->password_changed_at == null ? false : true;

            if(!$password_changed_at) {
                return ResponseServices::error(__('Auth.first_time_login, please reset password', []))->data($data)->toJson();
            }
            // $password_changed_at = new Carbon(($user->password_changed_at) ? $user->password_changed_at : $user->created_at);

            // $passwordExpiresDays = config('auth.password_expires_days', 30);
            // if (Carbon::now()->diffInDays($password_changed_at) >= $passwordExpiresDays) {
            //     $data['token'] = -35;
            //     return ResponseServices::error(__('Auth.password_expired', []))->data($data)->toJson();
            // }
        }

        return $next($request);
    }
}