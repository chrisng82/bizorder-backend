<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ItemBatch extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //
    protected $fillable = ['item_id', 'batch_serial_no', 'expiry_date', 'receipt_date'];

    public function item()
    {
        return $this->belongsTo(\App\Item::class, 'item_id', 'id');
    }

    public function generateTags(): array
    {
        $code = $this->item->code;
        unset($this->item);
        return array(
            $code
        );
    }
}
