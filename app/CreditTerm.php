<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class CreditTerm extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //
    protected $fillable = ['code', 'credit_term_type', 'desc_01', 'desc_02', 'period_01', 'period_02'];

    public function generateTags(): array
    {
        return array(
            $this->code
        );
    }
}
