<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class LoadListDtl extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //
    protected $casts = [
        'uom_rate' => 'decimal:6',
        'qty' => 'decimal:6',
    ];

    public function loadListHdr()
    {
        return $this->belongsTo(\App\LoadListHdr::class, 'hdr_id', 'id');
    }

    public function generateTags(): array
    {
        $docCode = $this->loadListHdr->doc_code;
        unset($this->loadListHdr);
        return array(
            $docCode
        );
    }
}
