<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BatchJobStatus extends Model
{
    protected $table = 'batch_job_status';

    protected $fillable = ['proc_type', 'user_id', 'status_number'];    

    protected $timestamp = false;
}
