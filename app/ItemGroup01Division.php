<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemGroup01Division extends Model
{
    protected $guarded = [];

    protected $fillable = ['item_group01_id', 'division_id', 'ref_code_01', 'ref_code_02', 'sequence'];
}
