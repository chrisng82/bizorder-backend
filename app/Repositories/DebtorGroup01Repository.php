<?php

namespace App\Repositories;

use App\DebtorGroup01;
use App\Services\Utils\RepositoryUtils;
use Illuminate\Support\Facades\DB;

class DebtorGroup01Repository 
{
    static public function findByPk($id) 
	{
        $debtorGroup = DebtorGroup01::where('id', $id)
            ->first();

        return $debtorGroup;
    }

    static public function select2($search, $filters, $pageSize = 10) 
	{
        $debtorGroup01s = DebtorGroup01::select('debtor_group01s.*')
        ->where(function($q) use($search) {
            $q->orWhere('code', 'LIKE', '%' . $search . '%')
            ->orWhere('desc_01', 'LIKE', '%' . $search . '%')
            ->orWhere('desc_02', 'LIKE', '%' . $search . '%');
        });
        if(count($filters) >= 1)
        {
            foreach($filters as $filter) {
                if(strcmp($filter['field'], 'debtor_category_01_id') == 0)
                {
                    if(is_array($filter['value'])) {
                        $debtorGroup01s->whereIn('debtor_group01s.id', $filter['value']);
                    } else {
                        $debtorGroup01s->orWhere('debtor_group01s.id', $filter['value']);
                    }
                }
                else
                {
                    $debtorGroup01s = $debtorGroup01s->where($filter);
                }
            }
        }
        return $debtorGroup01s
            ->orderBy('code', 'ASC')
            ->paginate($pageSize);
    }
}