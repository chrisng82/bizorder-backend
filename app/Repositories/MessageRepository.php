<?php

namespace App\Repositories;

use App\Services\Env\ResStatus;
use App\Services\Utils\RepositoryUtils;
use App\Services\Utils\ApiException;
use App\Models\Message;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MessageRepository 
{
    static public function findByPk($id) 
	{
        $message = Message::where('id', $id)
            ->first();

        return $message;
    }

    static public function findAllByUserId($userId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $messages = Message::select('messages.*')
            ->where('user_id', $userId);
        
        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'title') == 0)
            {
                $messages->where('messages.title', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'content') == 0)
            {
                $messages->where('messages.content', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'division_id') == 0)
            {
                $messages->where('messages.division_id', $filter['value']);
            }
        }
        
        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'created_at') == 0)
            {
                $messages->orderBy('created_at', $sort['order']);
            }
        }

        if($pageSize > 0)
        {
            return $messages
                ->paginate($pageSize);
        }
        else
        {
            return $messages
                ->paginate(PHP_INT_MAX);
        }
    }
    
    static public function createModel($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = new Message;
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return $model;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function updateModel($data) 
	{
        $result = DB::transaction
        (
            function() use ($data)
            {
                //update Item
                $model = Message::lockForUpdate()->find($data['id']);
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return array(
                    'model' => $model
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function deleteModel($id) 
	{
        $result = DB::transaction
        (
            function() use ($id)
            {
                //update Item
                $model = Message::where('id', '=', $id)
                            ->first();
                if(!is_null($model)){
                    Message::destroy($id);
                } else {
                    throw new ApiException($message = "Unauthorized action.", $code = 403);
                }

                return array(
                    'model' => $model
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }
}