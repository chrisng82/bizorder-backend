<?php

namespace App\Repositories;

use App\ItemCond01;
use App\Services\Utils\ApiException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ItemCond01Repository
{
    static public function findAll() 
	{
        $models = ItemCond01::get();
        return $models;
    }

    static public function findByPk($id) 
	{
        $models = ItemCond01::where('id', $id)
            ->first();

        return $models;
    }

    static public function select2($search, $filters, $pageSize = 10) 
	{
        $itemCond01s = ItemCond01::select('item_cond01s.*')
        ->where(function($q) use($search) {
            $q->orWhere('item_cond01s.code', 'LIKE', '%' . $search . '%')
            ->orWhere('item_cond01s.desc_01', 'LIKE', '%' . $search . '%');
        });
        foreach($filters as $filter)
        {
            
        }
        return $itemCond01s
            ->orderBy('item_cond01s.code', 'ASC')
            ->paginate($pageSize);
    }
}