<?php

namespace App\Repositories;

use App\PickingCriteria;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class PickingCriteriaRepository 
{
    static public function findAllByAttributes(
        $siteId, 
        $storageClass, $itemGroup01Id, $itemGroup02Id,
        $itemGroup03Id, $itemGroup04Id, $itemGroup05Id, $itemId,
        $debtorGroup01Ids, $debtorGroup02Ids, $debtorGroup03Ids, 
        $debtorGroup04Ids, $debtorGroup05Ids, $deliveryPointIds) 
	{
        $debtorGroup01Ids = array_filter($debtorGroup01Ids, function ($var) {
            return $var > 0;
        });
        $debtorGroup02Ids = array_filter($debtorGroup02Ids, function ($var) {
            return $var > 0;
        });
        $debtorGroup03Ids = array_filter($debtorGroup03Ids, function ($var) {
            return $var > 0;
        });
        $debtorGroup04Ids = array_filter($debtorGroup04Ids, function ($var) {
            return $var > 0;
        });
        $debtorGroup05Ids = array_filter($debtorGroup05Ids, function ($var) {
            return $var > 0;
        });
        $deliveryPointIds = array_filter($deliveryPointIds, function ($var) {
            return $var > 0;
        });

        $pickingCriterias = PickingCriteria::select('picking_criterias.*')
            ->where(function ($query) use($storageClass, $itemGroup01Id, $itemGroup02Id,
            $itemGroup03Id, $itemGroup04Id, $itemGroup05Id, $itemId) {
                $query
                ->when($storageClass > 0, function ($q2) use ($storageClass) {
                    return $q2->orWhere('storage_class', $storageClass);
                })
                ->when($itemGroup01Id > 0, function ($q2) use ($itemGroup01Id) {
                    return $q2->orWhere('item_group_01_id', $itemGroup01Id);
                })
                ->when($itemGroup02Id > 0, function ($q2) use ($itemGroup02Id) {
                    return $q2->orWhere('item_group_02_id', $itemGroup02Id);
                })
                ->when($itemGroup03Id > 0, function ($q2) use ($itemGroup03Id) {
                    return $q2->orWhere('item_group_03_id', $itemGroup03Id);
                })
                ->when($itemGroup04Id > 0, function ($q2) use ($itemGroup04Id) {
                    return $q2->orWhere('item_group_04_id', $itemGroup04Id);
                })
                ->when($itemGroup05Id > 0, function ($q2) use ($itemGroup05Id) {
                    return $q2->orWhere('item_group_05_id', $itemGroup05Id);
                })
                ->when($itemId > 0, function ($q2) use ($itemId) {
                    return $q2->orWhere('item_id', $itemId);
                });
            })
            ->where(function ($query) use($debtorGroup01Ids, $debtorGroup02Ids, $debtorGroup03Ids, 
            $debtorGroup04Ids, $debtorGroup05Ids, $deliveryPointIds) {
                $query
                ->when(!empty($debtorGroup01Ids), function ($q2) use ($debtorGroup01Ids) {
                    return $q2->whereIn('debtor_group_01_id', $debtorGroup01Ids, 'OR');
                })
                ->when(!empty($debtorGroup02Ids), function ($q2) use ($debtorGroup02Ids) {
                    return $q2->whereIn('debtor_group_02_id', $debtorGroup02Ids, 'OR');
                })
                ->when(!empty($debtorGroup03Ids), function ($q2) use ($debtorGroup03Ids) {
                    return $q2->whereIn('debtor_group_03_id', $debtorGroup03Ids, 'OR');
                })
                ->when(!empty($debtorGroup04Ids), function ($q2) use ($debtorGroup04Ids) {
                    return $q2->whereIn('debtor_group_04_id', $debtorGroup04Ids, 'OR');
                })
                ->when(!empty($debtorGroup05Ids), function ($q2) use ($debtorGroup05Ids) {
                    return $q2->whereIn('debtor_group_05_id', $debtorGroup05Ids, 'OR');
                })
                ->when(!empty($deliveryPointIds), function ($q2) use ($deliveryPointIds) {
                    return $q2->whereIn('delivery_point_id', $deliveryPointIds, 'OR');
                });
            })
            ->where('site_id', $siteId)
            ->get();
            
        return $pickingCriterias;
    }
}