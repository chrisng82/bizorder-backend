<?php

namespace App\Repositories;

use App\GdsRcptInbOrd;
use App\Services\Env\DocStatus;
use Illuminate\Support\Facades\DB;

class GdsRcptInbOrdRepository 
{
    static public function findAllByInbOrdHdrId($hdrId) 
	{
        $gdsRcptInbOrds = GdsRcptInbOrd::where('inb_ord_hdr_id', $hdrId)
            ->get();

        return $gdsRcptInbOrds;
    }

    static public function findAllByHdrIds($hdrIds) 
	{
        $gdsRcptInbOrds = GdsRcptInbOrd::whereIn('hdr_id', $hdrIds)
            ->get();

        return $gdsRcptInbOrds;
    }

    static public function findAllByCompleteGdsRcpt($divisionId, $startDate, $endDate) 
	{
        $gdsRcptInbOrds = GdsRcptInbOrd::select('gds_rcpt_inb_ords.*')
            ->selectRaw('inb_ord_hdrs.sls_ord_hdr_code AS sls_ord_hdr_code')
            ->selectRaw('gds_rcpt_hdrs.doc_code AS gds_rcpt_doc_code')
            ->selectRaw('gds_rcpt_hdrs.doc_date AS gds_rcpt_doc_date')
            ->leftJoin('inb_ord_hdrs', 'inb_ord_hdrs.id', '=', 'gds_rcpt_inb_ords.inb_ord_hdr_id')
            ->leftJoin('gds_rcpt_hdrs', 'gds_rcpt_hdrs.id', '=', 'gds_rcpt_inb_ords.hdr_id')
            ->where('inb_ord_hdrs.division_id', $divisionId)
            ->where('inb_ord_hdrs.doc_date', '>=', $startDate)
            ->where('inb_ord_hdrs.doc_date', '<=', $endDate)
            ->where('gds_rcpt_hdrs.doc_status', DocStatus::$MAP['COMPLETE'])
            ->get();
            
        return $gdsRcptInbOrds;
    }

    static public function findAllCompleteGdsRcptByInbOrdHdrId($inb_ord_hdr_id) 
	{
        $gdsRcptInbOrds = GdsRcptInbOrd::select('gds_rcpt_inb_ords.*')
            ->selectRaw('inb_ord_hdrs.sls_rtn_hdr_code AS sls_rtn_hdr_doc_code')
            ->selectRaw('gds_rcpt_hdrs.doc_code AS gds_rcpt_hdr_doc_code')
            ->selectRaw('gds_rcpt_hdrs.doc_date AS gds_rcpt_hdr_doc_date')
            ->leftJoin('inb_ord_hdrs', 'inb_ord_hdrs.id', '=', 'gds_rcpt_inb_ords.inb_ord_hdr_id')
            ->leftJoin('gds_rcpt_hdrs', 'gds_rcpt_hdrs.id', '=', 'gds_rcpt_inb_ords.hdr_id')
            ->where('gds_rcpt_inb_ords.inb_ord_hdr_id', $inb_ord_hdr_id)
            ->where('gds_rcpt_hdrs.doc_status', DocStatus::$MAP['COMPLETE'])
            ->get();
            
        return $gdsRcptInbOrds;
    }
}