<?php

namespace App\Repositories;

use App\SlsInvHdr;
use App\SlsInvDtl;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use Illuminate\Support\Facades\DB;

class SlsInvDtlRepository 
{
    static public function findAllByHdrId($hdrId) 
	{
        $slsInvDtls = SlsInvDtl::where('hdr_id', $hdrId)
            ->orderBy('line_no')
            ->get();

        return $slsInvDtls;
    }

    static public function findTopByHdrId($hdrId)
    {
        $slsInvDtl = SlsInvDtl::where('hdr_id', $hdrId)
            ->orderBy('line_no')
            ->first();

        return $slsInvDtl;
    }
}