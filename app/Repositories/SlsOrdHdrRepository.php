<?php

namespace App\Repositories;

use App\OutbOrdHdr;
use App\SlsOrdHdr;
use App\SlsOrdDtl;
use App\DocTxnFlow;
use App\Services\Env\ProcType;
use App\Services\Env\DocStatus;
use App\Services\Env\ItemType;
use App\Services\Env\ResStatus;
use App\Services\Env\RetrievalMethod;
use App\Services\Env\ScanMode;
use App\Services\Env\StorageClass;
use App\Services\Utils\RepositoryUtils;
use App\Repositories\CurrencyRepository;
use App\Repositories\CreditTermRepository;
use App\Repositories\UserRepository;
use App\Repositories\DeliveryPointRepository;
use App\Repositories\ItemRepository;
use App\Repositories\LocationRepository;
use App\Repositories\SlsOrdDtlPromotionRepository;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SlsOrdHdrRepository 
{
    static public function syncSlsOrdSync01($company, $division, $data) 
	{
        $siteFlow = $division->siteFlow;

        $slsOrdHdrModel = DB::transaction
        (
            function() use ($siteFlow, $company, $division, $data)
            {
                $isDirty = false;
                $model = SlsOrdHdr::firstOrCreate(['doc_code' => $data['doc_code']]);
                //get this sales order docFlowTxn
                $outbOrdHdrId = 0;
                $toDocTxnFlows = $model->toDocTxnFlows;
                foreach($toDocTxnFlows as $toDocTxnFlow)
                {
                    if(strcmp($toDocTxnFlow->to_doc_hdr_type, \App\OutbOrdHdr::class) != 0)
                    {
                        //skip if this is not outbOrdHdr
                        continue;
                    }
                    $outbOrdHdrId = $toDocTxnFlow->to_doc_hdr_id;
                }

                //check if this order oledi have outb_ord_hdr_id
                if($outbOrdHdrId > 0)
                {
                    //if already transfer to outOrd, then no need to do any sync
                    //but update the est_del_date, desc_01, desc_02
                    /*
                    $model->est_del_date = $data['userdate_01'];
                    $model->desc_01 = $data['remark_01'];
                    $model->desc_02 = $data['remark_02'];
                    $model->save();
                    
                    $outbOrdHdr = OutbOrdHdr::find($model->outb_ord_hdr_id);
                    $outbOrdHdr->est_del_date = $model->est_del_date;
                    $outbOrdHdr->desc_01 = $model->desc_01;
                    $outbOrdHdr->desc_02 = $model->desc_02;
                    $outbOrdHdr->save();
                    */
                    return $model;
                }
                
                $model->proc_type = ProcType::$MAP['SLS_ORD_SYNC_01'];
                if($model->doc_status == DocStatus::$MAP['NULL'])
                {
                    //initialize with DRAFT status
                    $model->doc_status = DocStatus::$MAP['DRAFT'];
                }             
                $model->ref_code_01 = $data['ref_code'];
                $model->doc_date = $data['doc_date'];
                $model->est_del_date = $data['userdate_01'];
                $model->desc_01 = $data['remark_01'];
                $model->desc_02 = $data['remark_02'];

                $model->division_id = $division->id;
                $model->site_flow_id = $division->site_flow_id;
                $model->company_id = $company->id;

                //currency
                $model->currency_id = 1;
                $model->currency_rate = $data['currency_rate'];
                $currency = CurrencyRepository::syncSlsOrdSync01($data);
                if(!empty($currency))
                {
                    $model->currency_id = $currency->id;
                }
                
                //creditTerm
                $model->credit_term_id = 0;
                if(strcmp($data['sales_type'], 'c') == 0)
                {
                    //if sales order sales_type = 0, always set to CASH term
                    //so will generate another invoice code
                    $model->credit_term_id = 1;
                }
                else
                {
                    $creditTerm = CreditTermRepository::syncSlsOrdSync01($data);
                    if(!empty($creditTerm))
                    {
                        $model->credit_term_id = $creditTerm->id;
                    }
                }

                //salesman
                $salesman = UserRepository::syncSlsOrdSync01($division, $data);
                $model->salesman_id = $salesman->id;

                //delivery point                
                $deliveryPoint = DeliveryPointRepository::syncSlsOrdSync01($division, $data);
                $model->debtor_id = $deliveryPoint->debtor_id;
                $model->delivery_point_id = $deliveryPoint->id;

                $model->gross_amt = $data['order_amt'];
                $model->gross_local_amt = $data['order_local_amt'];
                $model->disc_amt = $data['disc_amt'];
                $model->disc_local_amt = $data['disc_local_amt'];
                $model->tax_amt = $data['tax_amt'];
                $model->tax_local_amt = $data['tax_local_amt'];
                $model->is_round_adj = 0;
                $model->round_adj_amt = 0;
                $model->round_adj_local_amt = 0;
                $model->net_amt = $data['net_amt'];
                $model->net_local_amt = $data['net_local_amt'];

                $slsOrdDtls = SlsOrdDtl::where('hdr_id', $model->id)
                                ->get();
                $slsOrdDtlsHashByLineNo = array();
                $updatedSlsOrdDtls = array();
                $deletedSlsOrdDtls = array();
                foreach($slsOrdDtls as $slsOrdDtl)
                {
                    if(array_key_exists($slsOrdDtl->line_no, $slsOrdDtlsHashByLineNo))
                    {
                        $deletedSlsOrdDtls[] = $slsOrdDtl;
                    }
                    else
                    {
                        $slsOrdDtlsHashByLineNo[$slsOrdDtl->line_no] = $slsOrdDtl;
                    }
                }

                $details = $data['details'];
                $delDate = $data['doc_date'];
                foreach($details as $detail)
                {
                    $delDate = $detail['del_date'];

                    $slsOrdDtl = null;
                    if(array_key_exists($detail['line_no'], $slsOrdDtlsHashByLineNo))
                    {
                        $slsOrdDtl = $slsOrdDtlsHashByLineNo[$detail['line_no']];
                        unset($slsOrdDtlsHashByLineNo[$detail['line_no']]);
                    }
                    else
                    {
                        $slsOrdDtl = new SlsOrdDtl();
                        $slsOrdDtl->hdr_id = $model->id;
                    }

                    $slsOrdDtl->line_no = $detail['line_no'];

                    $location = LocationRepository::syncSlsOrdSync01($siteFlow->site_id, $detail);
                    $slsOrdDtl->location_id = $location->id;

                    $uom = UomRepository::syncSlsOrdSync01($division, $detail);
                    
                    $item = ItemRepository::syncSlsOrdSync01($division, $uom, $detail);
                    $slsOrdDtl->item_id = $item->id;
                    $slsOrdDtl->desc_01 = $detail['description'];
                    $slsOrdDtl->desc_02 = '';
                    $slsOrdDtl->uom_id = $uom->id;
                    $slsOrdDtl->uom_rate = $detail['uom_rate'];
                    if(bccomp($detail['list_price'], $detail['price'], 2) > 0)
                    {
                        $slsOrdDtl->sale_price = $detail['list_price'];
                        $slsOrdDtl->price_disc = bcsub($detail['list_price'], $detail['price'], 2);
                    }
                    else
                    {
                        $slsOrdDtl->sale_price = $detail['price'];
                        $slsOrdDtl->price_disc = 0;
                    }
                    $slsOrdDtl->qty = $detail['qty'];
                    $slsOrdDtl->dtl_disc_perc_01 = $detail['disc_percent_01'] >= 0 ? $detail['disc_percent_01'] : 0; //for efichain disc amt
                    $slsOrdDtl->dtl_disc_val_01 = 0;
                    if(bccomp($detail['disc_percent_01'], -1) == 0)
                    {
                        $slsOrdDtl->dtl_disc_val_01 = $detail['usernumber_01'];
                    }
                    $slsOrdDtl->dtl_disc_perc_02 = $detail['disc_percent_02'];
                    $slsOrdDtl->dtl_disc_perc_03 = $detail['disc_percent_03'];
                    $slsOrdDtl->dtl_disc_perc_04 = $detail['disc_percent_04'];
                    $slsOrdDtl->dtl_disc_amt = $detail['disc_amt'];
                    $slsOrdDtl->dtl_disc_local_amt = $detail['disc_local_amt'];
                    $slsOrdDtl->hdr_disc_perc_01 = $detail['disc_percent_05'];
                    $slsOrdDtl->hdr_disc_perc_02 = $detail['disc_percent_06'];
                    $slsOrdDtl->hdr_disc_perc_03 = $detail['disc_percent_07'];
                    $slsOrdDtl->hdr_disc_perc_04 = $detail['disc_percent_08'];
                    $slsOrdDtl->dtl_tax_perc_01 = $detail['tax_percent_01'];
                    $slsOrdDtl->dtl_tax_perc_02 = $detail['tax_percent_02'];
                    $slsOrdDtl->dtl_tax_perc_03 = $detail['tax_percent_03'];
                    $slsOrdDtl->dtl_tax_perc_04 = $detail['tax_percent_04'];
                    $slsOrdDtl->dtl_tax_amt_01 = $detail['tax_amt'];
                    $slsOrdDtl->dtl_tax_local_amt_01 = $detail['tax_local_amt'];
                    $slsOrdDtl->gross_amt = $detail['order_amt'];
                    $slsOrdDtl->gross_local_amt = $detail['order_local_amt'];
                    $slsOrdDtl->net_amt = $detail['net_amt'];
                    $slsOrdDtl->net_local_amt = $detail['net_local_amt'];

                    if($slsOrdDtl->isDirty() == true)
                    {
                        $isDirty = $slsOrdDtl->isDirty();
                    }

                    $updatedSlsOrdDtls[] = $slsOrdDtl;
                }
                $model->est_del_date = $delDate;

                if($model->isDirty() == true)
                {
                    $isDirty = $model->isDirty();
                }
                if(count($slsOrdDtlsHashByLineNo) > 0)
                {
                    $isDirty = true;
                }
                if(count($deletedSlsOrdDtls) > 0)
                {
                    $isDirty = true;
                }

                if($isDirty == true)
                {
                    //will check for models dirty, if is dirty, then only will revert the model to DRAFT
                    if($model->doc_status >= DocStatus::$MAP['WIP'])
                    {
                        //return to draft, so can update the details
                        if($model->doc_status == DocStatus::$MAP['COMPLETE'])
                        {
                            $tmpModel = self::revertCompleteToDraft($model->id);
                            $model->doc_status = $tmpModel->doc_status;
                        }
                        elseif($model->doc_status == DocStatus::$MAP['WIP'])
                        {
                            $tmpModel = self::revertWipToDraft($model->id);
                            $model->doc_status = $tmpModel->doc_status;
                        }
                    }

                    foreach($updatedSlsOrdDtls as $updatedSlsOrdDtl)
                    {
                        $updatedSlsOrdDtl->save();
                    }
                    foreach($slsOrdDtlsHashByLineNo as $lineNo => $slsOrdDtl)
                    {
                        $slsOrdDtl->delete();
                    }
                    foreach($deletedSlsOrdDtls as $slsOrdDtl)
                    {
                        $slsOrdDtl->delete();
                    }
                    $model->save();
                }

                if($data['status'] == 3)
                {
                    //set to COMPLETE
                    if($model->doc_status == DocStatus::$MAP['VOID'])
                    {
                        $model = self::commitVoidToDraft($model->id);
                    }

                    if($model->doc_status < DocStatus::$MAP['COMPLETE'])
                    {
                        $model = self::commitToComplete($model->id);
                    }
                }
                else
                {
                    //set to WIP
                    if($model->doc_status == DocStatus::$MAP['COMPLETE'])
                    {
                        $model = self::revertCompleteToDraft($model->id);
                    }
                    if($model->doc_status == DocStatus::$MAP['DRAFT'])
                    {
                        $model = self::commitToWip($model->id);
                    }
                }
                
                return $model;
            }, 
            5 //reattempt times
        );
        return $slsOrdHdrModel;
    }

    static public function txnFindByPk($hdrId) 
	{
        $model = DB::transaction
        (
            function() use ($hdrId)
            {
                $slsOrdHdr = SlsOrdHdr::where('id', $hdrId)
                    ->first();
                return $slsOrdHdr;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function commitToWip($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = SlsOrdHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('SlsOrd.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }

                $dtlModels = SlsOrdDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->orderBy('line_no')
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('SlsOrd.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to WIP
                $hdrModel->doc_status = DocStatus::$MAP['WIP'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitToComplete($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = SlsOrdHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();
                    
                if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('SlsOrd.doc_status_is_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                if($hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('SlsOrd.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
        
                //1) update docStatus to COMPLETE
                $hdrModel->doc_status = DocStatus::$MAP['COMPLETE'];
                $hdrModel->save();

                $dtlModels = SlsOrdDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('SlsOrd.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                $dtlModels->load('item');
                foreach($dtlModels as $dtlModel)
                {                    
                    $item = $dtlModel->item;
                    if(empty($item))
                    {
                        //only DRAFT or above can transition to COMPLETE
                        $exc = new ApiException(__('SlsOrd.item_is_required', ['lineNo'=>$dtlModel->line_no]));
                        $exc->addData(\App\SlsOrdDtl::class, $dtlModel->id);
                        throw $exc;
                    }
                }  
                return $hdrModel;              
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertCompleteToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = SlsOrdHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                {
                    $exc = new ApiException(__('SlsOrd.doc_status_is_not_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertWipToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = SlsOrdHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['WIP'])
                {
                    $exc = new ApiException(__('SlsOrd.doc_status_is_not_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function findAllNotExistOutbOrd01Txn($divisionId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $qStatuses = array(DocStatus::$MAP['WIP'],DocStatus::$MAP['COMPLETE']);
        $outbOrd01TxnFlows = DocTxnFlow::select('doc_txn_flows.fr_doc_hdr_id')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['OUTB_ORD_01'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\SlsOrdHdr::class)
                    ->distinct();

        //$qStatuses = array(DocStatus::$MAP['DRAFT'], DocStatus::$MAP['WIP'], DocStatus::$MAP['COMPLETE']);
        $leftJoinHash = array();
        $slsOrdHdrs = SlsOrdHdr::select('sls_ord_hdrs.*')
            ->leftJoinSub($outbOrd01TxnFlows, 'outb_ord_01_txn_flows', function ($join) {
                $join->on('sls_ord_hdrs.id', '=', 'outb_ord_01_txn_flows.fr_doc_hdr_id');
            })
            ->whereNull('outb_ord_01_txn_flows.fr_doc_hdr_id')
            ->where('division_id', $divisionId)
            ->whereIn('doc_status', $qStatuses);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $slsOrdHdrs->where('doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'salesman') == 0)
            {
                $leftJoinHash['users'] = 'users';
                $slsOrdHdrs->where('users.username', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $slsOrdHdrs->where(function($q) use($filter) {
                    $q->where('delivery_points.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('delivery_points.company_name_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'delivery_point_area') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $leftJoinHash['areas'] = 'areas';
                $slsOrdHdrs->where(function($q) use($filter) {
                    $q->where('areas.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('areas.desc_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $slsOrdHdrs->orderBy('doc_code', $sort['order']);
            }
            if(strcmp($sort['field'], 'delivery_point_area_code') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $slsOrdHdrs->orderBy('delivery_points.area_code', $sort['order']);
            }
            if(strcmp($sort['field'], 'salesman_username') == 0)
            {
                $leftJoinHash['users'] = 'users';
                $slsOrdHdrs->orderBy('users.username', $sort['order']);
            }
            if(strcmp($sort['field'], 'delivery_point_code') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $slsOrdHdrs->orderBy('delivery_points.code', $sort['order']);
            }         
            if(strcmp($sort['field'], 'doc_date') == 0)
            {
                $slsOrdHdrs->orderBy('doc_date', $sort['order']);
            }
            if(strcmp($sort['field'], 'net_amt') == 0)
            {
                $slsOrdHdrs->orderBy('net_amt', $sort['order']);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'users') == 0)
            {
                $slsOrdHdrs->leftJoin('users', 'users.id', '=', 'sls_ord_hdrs.salesman_id');
            }
            if(strcmp($field, 'delivery_points') == 0)
            {
                $slsOrdHdrs->leftJoin('delivery_points', 'delivery_points.id', '=', 'sls_ord_hdrs.delivery_point_id');
            }
            if(strcmp($field, 'areas') == 0)
            {
                $slsOrdHdrs->leftJoin('areas', 'areas.id', '=', 'delivery_points.area_id');
            }
        }

        return $slsOrdHdrs
        ->paginate($pageSize);
    }

    static public function findAllByHdrIds($hdrIds)
    {
        $slsOrdHdrs = SlsOrdHdr::whereIn('id', $hdrIds)
            ->get();

        return $slsOrdHdrs;
    }

    static public function revertDraftToVoid($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = SlsOrdHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('SlsOrd.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                if($hdrModel->outb_ord_hdr_id > 0)
                {
                    //sales order can not be voided if got associated outbound order
                    $outbOrdHdrModel = OutbOrdHdr::where('id', $hdrModel->outb_ord_hdr_id)
                    ->lockForUpdate()
                    ->first();
                    if(!empty($outbOrdHdrModel))
                    {
                        $exc = new ApiException(__('SlsOrd.outbound_order_is_not_void', ['docCode'=>$outbOrdHdrModel->doc_code]));
                        $exc->addData(\App\SlsOrdHdr::class, $hdrModel->id);
                        throw $exc;
                    }
                }
                
                //1) update docStatus to VOID
                $hdrModel->doc_status = DocStatus::$MAP['VOID'];
                $hdrModel->save();

                $frDocTxnFlows = $hdrModel->frDocTxnFlows;
                foreach($frDocTxnFlows as $frDocTxnFlow)
                {
                    //move to voidTxn
                    $txnVoidModel = new DocTxnVoid;
                    $txnVoidModel->id = $frDocTxnFlow->id;
                    $txnVoidModel->proc_type = $frDocTxnFlow->proc_type;
                    $txnVoidModel->fr_doc_hdr_type = $frDocTxnFlow->fr_doc_hdr_type;
                    $txnVoidModel->fr_doc_hdr_id = $frDocTxnFlow->fr_doc_hdr_id;
                    $txnVoidModel->fr_doc_hdr_code = $frDocTxnFlow->fr_doc_hdr_code;
                    $txnVoidModel->to_doc_hdr_type = $frDocTxnFlow->to_doc_hdr_type;
                    $txnVoidModel->to_doc_hdr_id = $frDocTxnFlow->to_doc_hdr_id;
                    $txnVoidModel->is_closed = $frDocTxnFlow->is_closed;
                    $txnVoidModel->save();
        
                    //delete the txn
                    $frDocTxnFlow->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function findByPk($hdrId) 
	{
        $slsOrdHdr = SlsOrdHdr::where('id', $hdrId)
            ->first();

        return $slsOrdHdr;
    }

    static public function commitVoidToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = SlsOrdHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['VOID'])
                {
                    //only VOID can transition to DRAFT
                    $exc = new ApiException(__('SlsOrd.doc_status_is_not_void', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                $frDocTxnVoids = $hdrModel->frDocTxnVoids;
                foreach($frDocTxnVoids as $frDocTxnVoid)
                {
                    //verify first
                    $docTxnFlow = DocTxnFlow::where(array(
                        'proc_type' => $frDocTxnVoid->proc_type,
                        'fr_doc_hdr_type' => $frDocTxnVoid->fr_doc_hdr_type,
                        'fr_doc_hdr_id' => $frDocTxnVoid->fr_doc_hdr_id,
                        'to_doc_hdr_type' => $frDocTxnVoid->to_doc_hdr_type
                        ))
                        ->first();
                    if(!empty($docTxnFlow))
                    {
                        $exc = new ApiException(__('SlsOrd.fr_doc_is_closed', ['docCode'=>$frDocTxnVoid->fr_doc_hdr_code]));
                        $exc->addData(\App\SlsOrdHdr::class, $hdrModel->id);
                        throw $exc;
                    }

                    //move to txnFlow
                    $txnFlowModel = new DocTxnFlow;
                    $txnFlowModel->id = $frDocTxnVoid->id;
                    $txnFlowModel->proc_type = $frDocTxnVoid->proc_type;
                    $txnFlowModel->fr_doc_hdr_type = $frDocTxnVoid->fr_doc_hdr_type;
                    $txnFlowModel->fr_doc_hdr_id = $frDocTxnVoid->fr_doc_hdr_id;
                    $txnFlowModel->fr_doc_hdr_code = $frDocTxnVoid->fr_doc_hdr_code;
                    $txnFlowModel->to_doc_hdr_type = $frDocTxnVoid->to_doc_hdr_type;
                    $txnFlowModel->to_doc_hdr_id = $frDocTxnVoid->to_doc_hdr_id;
                    $txnFlowModel->is_closed = $frDocTxnVoid->is_closed;
                    $txnFlowModel->save();
        
                    //delete the txnVoid
                    $frDocTxnVoid->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function createHeader($procType, $docNoId, $hdrData) 
	{
        $hdrModel = DB::transaction
        (
            function() use ($procType, $docNoId, $hdrData)
            {
                $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

                $hdrModel = new SlsOrdHdr;
                $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
                $hdrModel->doc_code = $newCode;
                $hdrModel->proc_type = $procType;
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];

                $hdrModel->gross_amt = 0;
                $hdrModel->gross_local_amt = 0;
                $hdrModel->disc_amt = 0;
                $hdrModel->disc_local_amt = 0;
                $hdrModel->tax_amt = 0;
                $hdrModel->tax_local_amt = 0;
                $hdrModel->is_round_adj = 0;
                $hdrModel->round_adj_amt = 0;
                $hdrModel->round_adj_local_amt = 0;
                $hdrModel->net_amt = 0;
                $hdrModel->net_local_amt = 0;
                
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function updateDetails($slsOrdHdrData, $slsOrdDtlArray, $delSlsOrdDtlArray=array()) 
	{
        $delSlsOrdDtlIds = array();
        foreach($delSlsOrdDtlArray as $delSlsOrdDtlData)
        {
            $delSlsOrdDtlIds[] = $delSlsOrdDtlData['id'];
        }

        // $delOutbOrdDtlIds = array();
        // foreach($delOutbOrdDtlArray as $delOutbOrdDtlData)
        // {
        //     $delOutbOrdDtlIds[] = $delOutbOrdDtlData['id'];
        // }
        
        $result = DB::transaction
        (
            function() use ($slsOrdHdrData, $slsOrdDtlArray, $delSlsOrdDtlIds)
            {
                //update outbOrdHdr
                // $outbOrdHdrIdHashByUuid = array();
                // $outbOrdDtlIdHashByUuid = array();
                // if(!empty($outbOrdHdrData))
                // {
                //     $outbOrdHdrModel = OutbOrdHdr::lockForUpdate()->find($outbOrdHdrData['id']);
                //     if($outbOrdHdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                //     {
                //         //only DRAFT or above can transition to COMPLETE
                //         $exc = new ApiException(__('OutbOrd.doc_status_is_complete', ['docCode'=>$outbOrdHdrModel->doc_code]));
                //         $exc->addData(\App\OutbOrdHdr::class, $outbOrdHdrModel->id);
                //         throw $exc;
                //     }
                //     $outbOrdHdrModel = RepositoryUtils::dataToModel($outbOrdHdrModel, $outbOrdHdrData);
                //     $outbOrdHdrModel->save();

                //     //update outbOrdDtl array
                //     foreach($outbOrdDtlArray as $outbOrdDtlData)
                //     {
                //         $uuid = '';
                //         $outbOrdDtlModel = null;
                //         if (strpos($outbOrdDtlData['id'], 'NEW') !== false) 
                //         {
                //             $uuid = $outbOrdDtlData['id'];
                //             unset($outbOrdDtlData['id']);
                //             $outbOrdDtlModel = new OutbOrdDtl;
                //         }
                //         else
                //         {
                //             $outbOrdDtlModel = OutbOrdDtl::lockForUpdate()->find($outbOrdDtlModel['id']);
                //         }
                        
                //         $outbOrdDtlModel = RepositoryUtils::dataToModel($outbOrdDtlModel, $outbOrdDtlData);
                //         $outbOrdDtlModel->hdr_id = $outbOrdHdrModel->id;
                //         $outbOrdDtlModel->save();

                //         if(!empty($uuid))
                //         {
                //             $outbOrdHdrIdHashByUuid[$uuid] = $outbOrdDtlModel->hdr_id;
                //             $outbOrdDtlIdHashByUuid[$uuid] = $outbOrdDtlModel->id;
                //         }
                //     }

                //     if(!empty($delOutbOrdDtlIds))
                //     {
                //         OutbOrdDtl::destroy($delOutbOrdDtlIds);
                //     }
                // }

                //update slsOrdHdr
                $slsOrdHdrModel = SlsOrdHdr::lockForUpdate()->find($slsOrdHdrData['id']);
                if($slsOrdHdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('SlsOrd.doc_status_is_complete', ['docCode'=>$slsOrdHdrModel->doc_code]));
                    $exc->addData(\App\SlsOrdHdr::class, $slsOrdHdrModel->id);
                    throw $exc;
                }
                $slsOrdHdrModel = RepositoryUtils::dataToModel($slsOrdHdrModel, $slsOrdHdrData);
                $slsOrdHdrModel->save();

                //update slsOrdDtl array
                $slsOrdDtlModels = array();
                foreach($slsOrdDtlArray as $slsOrdDtlData)
                {
                    $slsOrdDtlModel = null;
                    if (strpos($slsOrdDtlData['id'], 'NEW') !== false) 
                    {
                        $uuid = $slsOrdDtlData['id'];
                        $slsOrdDtlModel = new SlsOrdDtl;
                        // if(array_key_exists($uuid, $outbOrdHdrIdHashByUuid))
                        // {
                        //     $slsOrdDtlModel->outb_ord_hdr_id = $outbOrdHdrIdHashByUuid[$uuid];
                        // }
                        // if(array_key_exists($uuid, $outbOrdDtlIdHashByUuid))
                        // {
                        //     $slsOrdDtlModel->outb_ord_dtl_id = $outbOrdDtlIdHashByUuid[$uuid];
                        // }
                        unset($slsOrdDtlData['id']);
                    }
                    else
                    {
                        $slsOrdDtlModel = SlsOrdDtl::lockForUpdate()->find($slsOrdDtlData['id']);
                    }

                    $dtlPromotions = array();
                    if (isset($slsOrdDtlData['dtl_promotions']))
                    {
                        $dtlPromotions = $slsOrdDtlData['dtl_promotions'];
                        unset($slsOrdDtlData['dtl_promotions']);
                    }
                    
                    $slsOrdDtlModel = RepositoryUtils::dataToModel($slsOrdDtlModel, $slsOrdDtlData);
                    $slsOrdDtlModel->hdr_id = $slsOrdHdrModel->id;
                    $slsOrdDtlModel->save();
                    $slsOrdDtlModels[] = $slsOrdDtlModel;

                    if (!empty($dtlPromotions))
                    {
                        foreach ($dtlPromotions as $dtlPromotionData)
                        {
                            $dtlPromotionData['sls_ord_dtl_id'] = $slsOrdDtlModel->id;
                            SlsOrdDtlPromotionRepository::createModel($dtlPromotionData);
                        }
                    }
                }

                if(!empty($delSlsOrdDtlIds))
                {
                    SlsOrdDtl::destroy($delSlsOrdDtlIds);
                    SlsOrdDtlPromotionRepository::deleteAllByDtlIds($delSlsOrdDtlIds);
                }

                return array(
                    'hdrModel' => $slsOrdHdrModel,
                    'dtlModels' => $slsOrdDtlModels
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function checkSlsOrdWithInverze($company, $division, $data) 
	{
        $siteFlow = $division->siteFlow;

        $slsOrdHdrModel = DB::transaction
        (
            function() use ($siteFlow, $company, $division, $data)
            {
                $isDirty = false;
                $model = SlsOrdHdr::where('doc_code', $data['doc_code'])->first();
                //get this sales order docFlowTxn
                $outbOrdHdrId = 0;
                if(!empty($model))
                {
                    $toDocTxnFlows = $model->toDocTxnFlows;
                    foreach($toDocTxnFlows as $toDocTxnFlow)
                    {
                        if(strcmp($toDocTxnFlow->to_doc_hdr_type, \App\OutbOrdHdr::class) != 0)
                        {
                            //skip if this is not outbOrdHdr
                            continue;
                        }
                        $outbOrdHdrId = $toDocTxnFlow->to_doc_hdr_id;
                    }
                }
                else
                {
                    //Log::error($data['doc_code'].' not found');
                }

                //check if this order oledi have outb_ord_hdr_id
                if($outbOrdHdrId > 0)
                {
                    $outbOrdHdrModel = OutbOrdHdr::find($outbOrdHdrId);

                    if(bccomp($data['net_amt'], $outbOrdHdrModel->net_amt, 5) != 0)
                    {
                        Log::error(
                            $outbOrdHdrModel->sls_ord_hdr_code.' '.
                            $outbOrdHdrModel->sls_inv_hdr_code.' '.
                            ' wrong amt '.
                            $outbOrdHdrModel->net_amt.
                            ' correct amt '.
                            $data['net_amt']
                        );
                    }
                }
                
                return $model;
            }, 
            5 //reattempt times
        );
        return $slsOrdHdrModel;
    }

    static public function findAll($divisionId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $leftJoinHash = array();
        $slsOrdHdrs = SlsOrdHdr::select('sls_ord_hdrs.*')
            ->where('division_id', $divisionId);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'debtor_id') == 0)
            {
                $slsOrdHdrs->where('sls_ord_hdrs.debtor_id', $filter['value']);
            }
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $slsOrdHdrs->where('sls_ord_hdrs.doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $slsOrdHdrs->where(function($q) use($filter) {
                    $q->where('delivery_points.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('delivery_points.company_name_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'salesman') == 0)
            {
                $leftJoinHash['users'] = 'users';
                $slsOrdHdrs->where('users.username', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point_area') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $leftJoinHash['areas'] = 'areas';
                $slsOrdHdrs->where(function($q) use($filter) {
                    $q->where('areas.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('areas.desc_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'status') == 0)
            {
                if(strpos($filter['value'], ',') !== false) {
                    $slsOrdHdrs->whereIn('sls_ord_hdrs.doc_status', explode(',', $filter['value']));
                }
                else if($filter['value'] > 0)
                {
                    $slsOrdHdrs->where('sls_ord_hdrs.doc_status', $filter['value']);
                }
            }
            if(strcmp($filter['field'], 'start_date') == 0)
            {
                $slsOrdHdrs->where('sls_ord_hdrs.doc_date', '>=', $filter['value']);
            }
            if(strcmp($filter['field'], 'end_date') == 0)
            {
                $slsOrdHdrs->where('sls_ord_hdrs.doc_date', '<=', $filter['value']);
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $slsOrdHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'delivery_points') == 0)
            {
                $slsOrdHdrs->leftJoin('delivery_points', 'delivery_points.id', '=', 'sls_ord_hdrs.delivery_point_id');
            }
            if(strcmp($field, 'users') == 0)
            {
                $slsOrdHdrs->leftJoin('users', 'users.id', '=', 'sls_ord_hdrs.salesman_id');
            }
            if(strcmp($field, 'areas') == 0)
            {
                $slsOrdHdrs->leftJoin('areas', 'areas.id', '=', 'delivery_points.area_id');
            }
        }
        
        if($pageSize > 0)
        {
            return $slsOrdHdrs
                ->paginate($pageSize);
        }
        else
        {
            return $slsOrdHdrs
                ->paginate(PHP_INT_MAX);
        }
    }
}