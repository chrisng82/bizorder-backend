<?php

namespace App\Repositories;

use App\InvTxnFlow;

class InvTxnFlowRepository 
{
    static public function findByPk($divisionId, $procType) 
	{
        $invTxnFlow = InvTxnFlow::where(array(
            'division_id' => $divisionId,
            'proc_type' => $procType
            ))
            ->first();

        return $invTxnFlow;
    }

    static public function findAllByDivisionIdAndTxnFlowType($divisionId, $txnFlowType) 
	{
        $invTxnFlows = InvTxnFlow::where(array(
            'division_id' => $divisionId,
            'txn_flow_type' => $txnFlowType
            ))
            ->orderBy('step')
            ->get();

        return $invTxnFlows;
    }

    static public function findByDivisionIdAndFrResType($divisionId, $frResType) 
	{
        $invTxnFlow = InvTxnFlow::where(array(
            'division_id' => $divisionId,
            'fr_res_type' => $frResType
            ))
            ->first();

        return $invTxnFlow;
    }
}