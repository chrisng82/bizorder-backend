<?php

namespace App\Repositories;

use App\PickListOutbOrd;
use App\Services\Env\DocStatus;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class PickListOutbOrdRepository 
{
    static public function findAllByHdrIds($hdrIds) 
	{
        $pickListOutbOrds = PickListOutbOrd::whereIn('hdr_id', $hdrIds)
            ->get();

        return $pickListOutbOrds;
    }

    static public function findAllByHdrId($hdrId) 
	{
        $pickListOutbOrds = PickListOutbOrd::where('hdr_id', $hdrId)
            ->get();

        return $pickListOutbOrds;
    }

    static public function findAllByCompletePickList($divisionId, $startDate, $endDate) 
	{
        $pickListOutbOrds = PickListOutbOrd::select('pick_list_outb_ords.*')
            ->selectRaw('outb_ord_hdrs.sls_ord_hdr_code AS sls_ord_hdr_code')
            ->selectRaw('pick_list_hdrs.doc_code AS pick_list_doc_code')
            ->selectRaw('pick_list_hdrs.doc_date AS pick_list_doc_date')
            ->leftJoin('outb_ord_hdrs', 'outb_ord_hdrs.id', '=', 'pick_list_outb_ords.outb_ord_hdr_id')
            ->leftJoin('pick_list_hdrs', 'pick_list_hdrs.id', '=', 'pick_list_outb_ords.hdr_id')
            ->where('outb_ord_hdrs.division_id', $divisionId)
            ->where('outb_ord_hdrs.doc_date', '>=', $startDate)
            ->where('outb_ord_hdrs.doc_date', '<=', $endDate)
            ->where('pick_list_hdrs.doc_status', DocStatus::$MAP['COMPLETE'])
            ->get();
            
        return $pickListOutbOrds;
    }

    static public function findAllCompletePickListByOutbOrdHdrId($outb_ord_hdr_id) 
	{
        $pickListOutbOrds = PickListOutbOrd::select('pick_list_outb_ords.*')
            ->selectRaw('outb_ord_hdrs.sls_ord_hdr_code AS sls_ord_hdr_doc_code')
            ->selectRaw('pick_list_hdrs.doc_code AS pick_list_hdr_doc_code')
            ->selectRaw('pick_list_hdrs.doc_date AS pick_list_hdr_doc_date')
            ->leftJoin('outb_ord_hdrs', 'outb_ord_hdrs.id', '=', 'pick_list_outb_ords.outb_ord_hdr_id')
            ->leftJoin('pick_list_hdrs', 'pick_list_hdrs.id', '=', 'pick_list_outb_ords.hdr_id')
            ->where('pick_list_outb_ords.outb_ord_hdr_id', $outb_ord_hdr_id)
            ->where('pick_list_hdrs.doc_status', DocStatus::$MAP['COMPLETE'])
            ->get();
            
        return $pickListOutbOrds;
    }
}