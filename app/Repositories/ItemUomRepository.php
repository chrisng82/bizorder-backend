<?php

namespace App\Repositories;

use App\ItemUom;
use App\Services\Utils\RepositoryUtils;
use Illuminate\Support\Facades\DB;

class ItemUomRepository 
{
    static public function findRandomByItemId($itemId) 
	{
        $model = ItemUom::where('item_id', $itemId)
            ->orderByRaw('RAND()')
            ->first();

        return $model;
    }

    static public function findByItemIdAndUomId($itemId, $uomId) 
	{
        $itemUom = ItemUom::where(array(
            'item_id' => $itemId,
            'uom_id' => $uomId
            ))
            ->first();

        return $itemUom;
    }
    static public function deleteByPK($id) 
	{
        $item = ItemUom::find($id);
        $temp=$item;
        $item->delete();
        return $temp;
    }

    static public function findByItemIdAndUomRate($itemId, $uomRate) 
	{
        $itemUom = ItemUom::where(array(
            'item_id' => $itemId,
            'uom_rate' => $uomRate
            ))
            ->first();

        return $itemUom;
    }

    static public function findAllByItemId($itemId) 
	{
        $itemUoms = ItemUom::where(array(
            'item_id' => $itemId
            ))
            ->orderBy('uom_rate', 'ASC')
            ->get();

        return $itemUoms;
    }
    
    static public function findSmallestByItemId($itemId) 
	{
        $itemUom = ItemUom::where(array(
            'item_id' => $itemId
            ))
            ->orderBy('uom_rate', 'ASC')
            ->first();

        return $itemUom;
    }

    static public function findAllByItemIdDesc($itemId) 
	{
        $itemUoms = ItemUom::where(array(
            'item_id' => $itemId
            ))
            ->orderBy('uom_rate', 'DESC')
            ->get();

        return $itemUoms;
    }
    
    static public function createModel($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = new ItemUom;
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return $model;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function updateModel($data) 
	{
        $result = DB::transaction
        (
            function() use ($data)
            {
                //update Item
                $model = ItemUom::lockForUpdate()->find($data['id']);
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return array(
                    'model' => $model
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function select2($search, $filters, $pageSize = 10)
    {
        $itemUoms = ItemUom::select('item_uoms.*', 'uoms.code AS uom_code')
        ->leftJoin('uoms', 'uoms.id', '=', 'item_uoms.uom_id')
        ->where(function($q) use($search) {
            $q->orWhere('uoms.code', 'LIKE', '%' . $search . '%');
        });
        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'item_id') == 0)
            {
                $operator = '=';
                if(isset($filter['operator']))
                {
                    $operator = $filter['operator'];
                }
                $itemUoms->where('item_uoms.item_id', $operator, $filter['value']);
            }
        }
        return $itemUoms
            ->orderBy('uom_rate', 'ASC')
            ->paginate($pageSize);
    }
}