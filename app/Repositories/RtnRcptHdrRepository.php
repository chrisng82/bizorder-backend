<?php

namespace App\Repositories;

use App\Services\Utils\RepositoryUtils;
use App\RtnRcptHdr;
use App\RtnRcptDtl;
use App\DocTxnFlow;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\DB;

class RtnRcptHdrRepository 
{
    static public function findAll($divisionId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $leftJoinHash = array();
        $rtnRcptHdrs = RtnRcptHdr::select('rtn_rcpt_hdrs.*')
            ->where('division_id', $divisionId);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $rtnRcptHdrs->where('rtn_rcpt_hdrs.doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $rtnRcptHdrs->where(function($q) use($filter) {
                    $q->where('delivery_points.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('delivery_points.company_name_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'salesman') == 0)
            {
                $leftJoinHash['users'] = 'users';
                $rtnRcptHdrs->where('users.username', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point_area') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $leftJoinHash['areas'] = 'areas';
                $rtnRcptHdrs->where(function($q) use($filter) {
                    $q->where('areas.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('areas.desc_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'status') == 0)
            {
                if($filter['value'] > 0)
                {
                    $rtnRcptHdrs->where('rtn_rcpt_hdrs.doc_status', $filter['value']);
                }
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $rtnRcptHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'delivery_points') == 0)
            {
                $rtnRcptHdrs->leftJoin('delivery_points', 'delivery_points.id', '=', 'rtn_rcpt_hdrs.delivery_point_id');
            }
            if(strcmp($field, 'users') == 0)
            {
                $rtnRcptHdrs->leftJoin('users', 'users.id', '=', 'rtn_rcpt_hdrs.salesman_id');
            }
            if(strcmp($field, 'areas') == 0)
            {
                $rtnRcptHdrs->leftJoin('areas', 'areas.id', '=', 'delivery_points.area_id');
            }
        }
        
        if($pageSize > 0)
        {
            return $rtnRcptHdrs
                ->paginate($pageSize);
        }
        else
        {
            return $rtnRcptHdrs
                ->paginate(PHP_INT_MAX);
        }
    }

    static public function findByPk($hdrId) 
	{
        $rtnRcptHdr = RtnRcptHdr::where('id', $hdrId)
            ->first();

        return $rtnRcptHdr;
    }

    static public function createDocTxnFlow($procType, $toRtnRcptHdrId, $docTxnFlowData) 
	{
        $docTxnFlowModel = new DocTxnFlow;
        $docTxnFlowModel->proc_type = $procType;
        $docTxnFlowModel->to_doc_hdr_type = \App\RtnRcptHdr::class;
        $docTxnFlowModel->to_doc_hdr_id = $toRtnRcptHdrId;
        $docTxnFlowModel->save();
    }

    static public function createProcess($procType, $docNoId, $hdrData, $dtlDataList, $docTxnFlowDataList) 
	{
        $rtnRcptHdr = DB::transaction
        (
            function() use ($procType, $docNoId, $hdrData, $dtlDataList, $docTxnFlowDataList)
            {
                $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

                $hdrModel = new RtnRcptHdr;
                $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
                $hdrModel->doc_code = $newCode;
                $hdrModel->proc_type = $procType;
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                $txnHdrModelHash = array();
                foreach($dtlDataList as $lineNo => $dtlData)
                {
                    $dtlModel = new RtnRcptDtl;
                    $dtlModel = RepositoryUtils::dataToModel($dtlModel, $dtlData);
                    $dtlModel->hdr_id = $hdrModel->id;
                    $dtlModel->save();

                    //create a carbon copy
                    $cDtlModel = new RtnRcptCDtl;
                    $cDtlModel = RepositoryUtils::dataToModel($cDtlModel, $dtlData);
                    $cDtlModel->hdr_id = $hdrModel->id;
                    $cDtlModel->save();
                }

                //process docTxnFlowDataList
                foreach($docTxnFlowDataList as $docTxnFlowData)
                {
                    $frDocHdrModel = $docTxnFlowData['fr_doc_hdr_type']::where('id', $docTxnFlowData['fr_doc_hdr_id'])
                    ->lockForUpdate()
                    ->first();
                    if($frDocHdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                    {
                        $exc = new ApiException(__('RtnRcpt.fr_doc_is_not_complete', ['docType'=>$docTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$docTxnFlowData['fr_doc_hdr_code']]));
                        $exc->addData($docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_code']);
                        throw $exc;
                    }

                    //verify the frDoc is not closed for this procType
                    $tmpDocTxnFlow = DocTxnFlowRepository::findByProcTypeAndFrHdrId($procType, $docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_id']);
                    if(!empty($tmpDocTxnFlow))
                    {
                        $exc = new ApiException(__('RtnRcpt.fr_doc_is_closed', ['docType'=>$docTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$docTxnFlowData['fr_doc_hdr_code']]));
                        $exc->addData($docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_code']);
                        throw $exc;
                    }
                    
                    $docTxnFlowModel = new DocTxnFlow;
                    $docTxnFlowModel = RepositoryUtils::dataToModel($docTxnFlowModel, $docTxnFlowData);
                    $docTxnFlowModel->proc_type = $procType;
                    $docTxnFlowModel->to_doc_hdr_type = \App\RtnRcptHdr::class;
                    $docTxnFlowModel->to_doc_hdr_id = $hdrModel->id;
                    $docTxnFlowModel->save();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $rtnRcptHdr;
    }

    static public function txnFindByPk($hdrId) 
	{
        $model = DB::transaction
        (
            function() use ($hdrId)
            {
                $rtnRcptHdr = RtnRcptHdr::where('id', $hdrId)
                    ->first();
                return $rtnRcptHdr;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function commitToComplete($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = RtnRcptHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();
                    
                if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('RtnRcpt.doc_status_is_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\RtnRcptHdr::class, $hdrModel->id);
                    throw $exc;
                }
                if($hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('RtnRcpt.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\RtnRcptHdr::class, $hdrModel->id);
                    throw $exc;
                }

                $dtlModels = RtnRcptDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->orderBy('line_no')
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('RtnRcpt.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\RtnRcptHdr::class, $hdrModel->id);
                    throw $exc;
                }
        
                //1) update docStatus to COMPLETE
                $hdrModel->doc_status = DocStatus::$MAP['COMPLETE'];
                $hdrModel->save();

                return $hdrModel;              
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitToWip($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = RtnRcptHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('RtnRcpt.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\RtnRcptHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                $dtlModels = RtnRcptDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->orderBy('line_no')
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('RtnRcpt.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\RtnRcptHdr::class, $hdrModel->id);
                    throw $exc;
                }

                //1) update docStatus to WIP
                $hdrModel->doc_status = DocStatus::$MAP['WIP'];
                $hdrModel->save();
                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertCompleteToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = RtnRcptHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                {
                    $exc = new ApiException(__('RtnRcpt.doc_status_is_not_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\RtnRcptHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertWipToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = RtnRcptHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['WIP'])
                {
                    $exc = new ApiException(__('RtnRcpt.doc_status_is_not_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\RtnRcptHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function updateDetails($rtnRcptHdrData, $rtnRcptDtlArray, $delRtnRcptDtlArray, $inbOrdHdrData, $inbOrdDtlArray, $delInbOrdDtlArray) 
	{
        $delRtnRcptDtlIds = array();
        foreach($delRtnRcptDtlArray as $delRtnRcptDtlData)
        {
            $delRtnRcptDtlIds[] = $delRtnRcptDtlData['id'];
        }

        $delInbOrdDtlIds = array();
        foreach($delInbOrdDtlArray as $delInbOrdDtlData)
        {
            $delInbOrdDtlIds[] = $delInbOrdDtlData['id'];
        }
        
        $result = DB::transaction
        (
            function() use ($rtnRcptHdrData, $rtnRcptDtlArray, $delRtnRcptDtlIds, $inbOrdHdrData, $inbOrdDtlArray, $delInbOrdDtlIds)
            {
                //update inbOrdHdr
                $inbOrdHdrIdHashByUuid = array();
                $inbOrdDtlIdHashByUuid = array();
                if(!empty($inbOrdHdrData))
                {
                    $inbOrdHdrModel = InbOrdHdr::lockForUpdate()->find($inbOrdHdrData['id']);
                    if($inbOrdHdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                    {
                        //only DRAFT or above can transition to COMPLETE
                        $exc = new ApiException(__('InbOrd.doc_status_is_complete', ['docCode'=>$inbOrdHdrModel->doc_code]));
                        $exc->addData(\App\InbOrdHdr::class, $inbOrdHdrModel->id);
                        throw $exc;
                    }
                    $inbOrdHdrModel = RepositoryUtils::dataToModel($inbOrdHdrModel, $inbOrdHdrData);
                    $inbOrdHdrModel->save();

                    //update inbOrdDtl array
                    foreach($inbOrdDtlArray as $inbOrdDtlData)
                    {
                        $uuid = '';
                        $inbOrdDtlModel = null;
                        if (strpos($inbOrdDtlData['id'], 'NEW') !== false) 
                        {
                            $uuid = $inbOrdDtlData['id'];
                            unset($inbOrdDtlData['id']);
                            $inbOrdDtlModel = new InbOrdDtl;
                        }
                        else
                        {
                            $inbOrdDtlModel = InbOrdDtl::lockForUpdate()->find($inbOrdDtlModel['id']);
                        }
                        
                        $inbOrdDtlModel = RepositoryUtils::dataToModel($inbOrdDtlModel, $inbOrdDtlData);
                        $inbOrdDtlModel->hdr_id = $inbOrdHdrModel->id;
                        $inbOrdDtlModel->save();

                        if(!empty($uuid))
                        {
                            $inbOrdHdrIdHashByUuid[$uuid] = $inbOrdDtlModel->hdr_id;
                            $inbOrdDtlIdHashByUuid[$uuid] = $inbOrdDtlModel->id;
                        }
                    }

                    if(!empty($delInbOrdDtlIds))
                    {
                        InbOrdDtl::destroy($delInbOrdDtlIds);
                    }
                }

                //update rtnRcptHdr
                $rtnRcptHdrModel = RtnRcptHdr::lockForUpdate()->find($rtnRcptHdrData['id']);
                if($rtnRcptHdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('RtnRcpt.doc_status_is_complete', ['docCode'=>$rtnRcptHdrModel->doc_code]));
                    $exc->addData(\App\RtnRcptHdr::class, $rtnRcptHdrModel->id);
                    throw $exc;
                }
                $rtnRcptHdrModel = RepositoryUtils::dataToModel($rtnRcptHdrModel, $rtnRcptHdrData);
                $rtnRcptHdrModel->save();

                //update rtnRcptDtl array
                $rtnRcptDtlModels = array();
                foreach($rtnRcptDtlArray as $rtnRcptDtlData)
                {
                    $rtnRcptDtlModel = null;
                    if (strpos($rtnRcptDtlData['id'], 'NEW') !== false) 
                    {
                        $uuid = $rtnRcptDtlData['id'];
                        $rtnRcptDtlModel = new RtnRcptDtl;
                        if(array_key_exists($uuid, $inbOrdHdrIdHashByUuid))
                        {
                            $rtnRcptDtlModel->inb_ord_hdr_id = $inbOrdHdrIdHashByUuid[$uuid];
                        }
                        if(array_key_exists($uuid, $inbOrdDtlIdHashByUuid))
                        {
                            $rtnRcptDtlModel->inb_ord_dtl_id = $inbOrdDtlIdHashByUuid[$uuid];
                        }
                        unset($rtnRcptDtlData['id']);
                    }
                    else
                    {
                        $rtnRcptDtlModel = RtnRcptDtl::lockForUpdate()->find($rtnRcptDtlData['id']);
                    }
                    
                    $rtnRcptDtlModel = RepositoryUtils::dataToModel($rtnRcptDtlModel, $rtnRcptDtlData);
                    $rtnRcptDtlModel->hdr_id = $rtnRcptHdrModel->id;
                    $rtnRcptDtlModel->save();
                    $rtnRcptDtlModels[] = $rtnRcptDtlModel;
                }

                if(!empty($delRtnRcptDtlIds))
                {
                    RtnRcptDtl::destroy($delRtnRcptDtlIds);
                }

                return array(
                    'hdrModel' => $rtnRcptHdrModel,
                    'dtlModels' => $rtnRcptDtlModels
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function findAllNotExistRtnRcptSync01Txn($divisionId, $sorts, $filters = array(), $pageSize = 20) 
	{
        //$qStatuses = array(DocStatus::$MAP['COMPLETE']);
        $leftJoinHash = array();
        $rtnRcptHdrs = RtnRcptHdr::select('rtn_rcpt_hdrs.*')
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('doc_txn_flows')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['RTN_RCPT_SYNC_01'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\RtnRcptHdr::class)
                    ->whereRaw('doc_txn_flows.fr_doc_hdr_id = rtn_rcpt_hdrs.id')
                    ->where('doc_txn_flows.is_closed', 1);
            })
            ->where('doc_status', DocStatus::$MAP['COMPLETE'])
            ->where('division_id', $divisionId);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $rtnRcptHdrs->where('doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'salesman_username') == 0)
            {
                $leftJoinHash['users'] = 'users';
                $rtnRcptHdrs->where('users.username', 'LIKE', '%'.$filter['value'].'%');
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $rtnRcptHdrs->orderBy('doc_code', $sort['order']);
            }
            if(strcmp($sort['field'], 'delivery_point_area_code') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $rtnRcptHdrs->orderBy('delivery_points.area_code', $sort['order']);
            }
            if(strcmp($sort['field'], 'salesman_username') == 0)
            {
                $leftJoinHash['users'] = 'users';
                $rtnRcptHdrs->orderBy('users.username', $sort['order']);
            }
            if(strcmp($sort['field'], 'delivery_point_code') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $rtnRcptHdrs->orderBy('delivery_points.code', $sort['order']);
            }         
            if(strcmp($sort['field'], 'doc_date') == 0)
            {
                $rtnRcptHdrs->orderBy('doc_date', $sort['order']);
            }
            if(strcmp($sort['field'], 'net_amt') == 0)
            {
                $rtnRcptHdrs->orderBy('net_amt', $sort['order']);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'users') == 0)
            {
                $rtnRcptHdrs->leftJoin('users', 'users.id', '=', 'rtn_rcpt_hdrs.salesman_id');
            }
            if(strcmp($field, 'delivery_points') == 0)
            {
                $rtnRcptHdrs->leftJoin('delivery_points', 'delivery_points.id', '=', 'rtn_rcpt_hdrs.delivery_point_id');
            }
        }
        
        if($pageSize > 0)
        {
            return $rtnRcptHdrs
            ->paginate($pageSize);
        }
        else
        {
            return $rtnRcptHdrs
            ->paginate(PHP_INT_MAX);
        }
    }

    static public function revertDraftToVoid($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = RtnRcptHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('RtnRcpt.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\RtnRcptHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to VOID
                $hdrModel->doc_status = DocStatus::$MAP['VOID'];
                $hdrModel->save();

                $frDocTxnFlows = $hdrModel->frDocTxnFlows;
                foreach($frDocTxnFlows as $frDocTxnFlow)
                {
                    //move to voidTxn
                    $txnVoidModel = new DocTxnVoid;
                    $txnVoidModel->id = $frDocTxnFlow->id;
                    $txnVoidModel->proc_type = $frDocTxnFlow->proc_type;
                    $txnVoidModel->fr_doc_hdr_type = $frDocTxnFlow->fr_doc_hdr_type;
                    $txnVoidModel->fr_doc_hdr_id = $frDocTxnFlow->fr_doc_hdr_id;
                    $txnVoidModel->fr_doc_hdr_code = $frDocTxnFlow->fr_doc_hdr_code;
                    $txnVoidModel->to_doc_hdr_type = $frDocTxnFlow->to_doc_hdr_type;
                    $txnVoidModel->to_doc_hdr_id = $frDocTxnFlow->to_doc_hdr_id;
                    $txnVoidModel->is_closed = $frDocTxnFlow->is_closed;
                    $txnVoidModel->save();
        
                    //delete the txn
                    $frDocTxnFlow->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function createHeader($procType, $docNoId, $hdrData) 
	{
        $hdrModel = DB::transaction
        (
            function() use ($procType, $docNoId, $hdrData)
            {
                $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

                $hdrModel = new RtnRcptHdr;
                $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
                $hdrModel->doc_code = $newCode;
                $hdrModel->proc_type = $procType;
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];

                $hdrModel->gross_amt = 0;
                $hdrModel->gross_local_amt = 0;
                $hdrModel->disc_amt = 0;
                $hdrModel->disc_local_amt = 0;
                $hdrModel->tax_amt = 0;
                $hdrModel->tax_local_amt = 0;
                $hdrModel->is_round_adj = 0;
                $hdrModel->round_adj_amt = 0;
                $hdrModel->round_adj_local_amt = 0;
                $hdrModel->net_amt = 0;
                $hdrModel->net_local_amt = 0;
                
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }
}