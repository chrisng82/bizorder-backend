<?php

namespace App\Repositories;

use App\ItemGroup01;
use App\Item;
use App\Services\Utils\ApiException;
use App\Services\Utils\RepositoryUtils;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use JD\Cloudder\Facades\Cloudder;

class ItemGroup01Repository
{
    static public function createModel($data)
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = new ItemGroup01;
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return $model;
            },
            5 //reattempt times
        );
        return $model;
    }

    static public function updateModel($data)
	{
        $result = DB::transaction
        (
            function() use ($data)
            {
                //update Item
                $model = ItemGroup01::lockForUpdate()->find($data['id']);
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return array(
                    'model' => $model
                );
            },
            5 //reattempt times
        );
        return $result;
    }

    static public function findByPk($id)
    {
        return ItemGroup01::where('id', $id)->first();
    }

    static public function findByCode($code)
	{
        $group = ItemGroup01::where('code', $code)
        ->first();
        return $group;
    }

    static public function findByCodeAndDivision($code, $divisionId)
	{
        $group = ItemGroup01::select('item_group01s.*')
                ->leftJoin('item_group01_divisions', 'item_group01s.id', '=', 'item_group01_divisions.item_group01_id')
                ->where('item_group01_divisions.division_id', $divisionId)
                ->where('code', $code)
                ->first();
        return $group;
    }

    static public function select2($search, $filters, $pageSize = 10)
	{
        $itemGroup01s = ItemGroup01::select('item_group01s.*')
        ->where(function($q) use($search) {
            $q->orWhere('desc_01', 'LIKE', '%' . $search . '%')
            ->orWhere('desc_02', 'LIKE', '%' . $search . '%')
            ->orWhere('code', 'LIKE', '%' . $search . '%');
        });
        if(count($filters) >= 1)
        {
            $itemGroup01s = $itemGroup01s->where($filters);
        }
        return $itemGroup01s
            ->orderBy('code', 'ASC')
            ->paginate($pageSize);
    }

    static public function select2ByDivision($divisionID, $search, $filters, $pageSize = 100)
	{
        $divisionItemGroups = DB::select( DB::raw("SELECT DISTINCT i.item_group_01_id FROM item_divisions id LEFT JOIN items i
            ON i.id = id.item_id
            WHERE id.division_id = :division_id"),
            array(
            'division_id' => $divisionID,
            ));


        //$array = json_decode(json_encode($divisionItemGroups), True);
        foreach ($divisionItemGroups as $value)
            $array[] = $value->item_group_01_id;

        $itemGroup01s = ItemGroup01::select('item_group01s.*')
        ->whereIn('id', $array)
        ->where(function($q) use($search) {
            $q->orWhere('desc_01', 'LIKE', '%' . $search . '%')
            ->orWhere('desc_02', 'LIKE', '%' . $search . '%')
            ->orWhere('code', 'LIKE', '%' . $search . '%');
        });

        if(count($filters) >= 1)
        {
            $itemGroup01s = $itemGroup01s->where($filters);
        }

        return $itemGroup01s
            ->orderBy('code', 'ASC')
            ->paginate($pageSize);
    }

    static public function findAll($sorts, $filters = array(), $pageSize = 20)
	{
        $item_group01s = ItemGroup01::select('item_group01s.*', 'item_group01_divisions.sequence')
			->distinct()
			->leftJoin('item_group01_divisions', 'item_group01s.id', '=', 'item_group01_divisions.item_group01_id')
            ->whereNotNull('item_group01s.id');

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'division_id') == 0)
            {
                $item_group01s->where('item_group01_divisions.division_id', '=', $filter['value']);
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'code') == 0)
            {
                $item_group01s->orderBy('item_group01s.code', $sort['order']);
            }
            elseif(strcmp($sort['field'], 'desc_01') == 0)
            {
                $item_group01s->orderBy('item_group01s.desc_01', $sort['order']);
            }
            elseif(strcmp($sort['field'], 'desc_02') == 0)
            {
                $item_group01s->orderBy('item_group01s.desc_02', $sort['order']);
            }
            elseif(strcmp($sort['field'], 'sequence') == 0)
            {
                $item_group01s->orderBy('item_group01_divisions.sequence', $sort['order']);
            }
        }

        if($pageSize > 0)
        {
            return $item_group01s
                ->paginate($pageSize);
        }
        else
        {
            return $item_group01s
                ->paginate(PHP_INT_MAX);
        }
    }

    static public function savePhoto($data)
	{
        $group = DB::transaction
        (
            function() use ($data)
            {
				$group = ItemGroup01::find($data['id']);
				$group->image_desc_01 = $data['desc_01'];
				$group->image_desc_02 = $data['desc_02'];
				list($width, $height) = getimagesize($data['filename']);

                Cloudder::upload($data['filename'], null);
                $public_id = Cloudder::getPublicId();
                $image_url= Cloudder::show($public_id, ["width" => $width, "height"=>$height]);

				//save to uploads directory
                $image = $data['blob'];
                $image->move(public_path("uploads"), $group->desc_01);

                $group->image_path = $image_url;
                $group->image_desc_02 = $public_id;
				$group->save();

                return $group;
            },
            5 //reattempt times
        );
        return $group;
    }

    static public function deletePhoto($id)
	{
        $group = DB::transaction
        (
            function() use ($id)
            {
				$group = ItemGroup01::find($id);

				$public_id = $group->image_desc_02;
                if(!empty($public_id))
                {
                    Cloudder::destroyImage($public_id, array());

					$group->image_desc_01 = '';
					$group->image_desc_02 = '';
					$group->image_path = '';
					$group->save();
                }

				return $group;
            },
            5 //reattempt times
        );

        return $group;
    }
}
