<?php

namespace App\Repositories;

use App\Services\Utils\RepositoryUtils;
use App\DocTxnFlow;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\DB;

class DocTxnFlowRepository 
{
    static public function findAllByFrHdrTypeAndToHdrId($frHdrType, $toHdrType, $toHdrId) 
	{
        $docTxnFlows = DocTxnFlow::where(array(
            'fr_doc_hdr_type' => $frHdrType,
            'to_doc_hdr_type' => $toHdrType,
            'to_doc_hdr_id' => $toHdrId
            ))
            ->get();
        return $docTxnFlows;
    }

    static public function findAllByFrHdrTypeAndToHdrIds($frHdrType, $toHdrType, $hdrIds) 
	{
        $docTxnFlows = DocTxnFlow::where('fr_doc_hdr_type', $frHdrType)
            ->where('to_doc_hdr_type', $toHdrType)
            ->whereIn('to_doc_hdr_id', $hdrIds)
            ->get();
        return $docTxnFlows;
    }

    static public function findByProcTypeAndFrHdrId($procType, $frHdrType, $frHdrId, $isClosed = 1) 
	{
        $docTxnFlow = DocTxnFlow::where(array(
            'proc_type' => $procType,
            'fr_doc_hdr_type' => $frHdrType,
            'fr_doc_hdr_id' => $frHdrId,
            'is_closed' => $isClosed
            ))
            ->first();
            //->toSql();
            //dd($docTxnFlow);
        return $docTxnFlow;
    }

    static public function txnFindByProcTypeAndFrHdrId($procType, $frHdrType, $frHdrId, $isClosed = 1) 
	{
        $docTxnFlow = DB::transaction
        (
            function() use ($procType, $frHdrType, $frHdrId, $isClosed)
            {
                $docTxnFlow = DocTxnFlow::where(array(
                    'proc_type' => $procType,
                    'fr_doc_hdr_type' => $frHdrType,
                    'fr_doc_hdr_id' => $frHdrId,
                    'is_closed' => $isClosed
                    ))
                    ->first();
                    //->toSql();
                    //dd($docTxnFlow);
                return $docTxnFlow;
            }, 
            5 //reattempt times
        );
        return $docTxnFlow;
    }

    static public function createProcess($procType, $docTxnFlowDataList) 
	{
        $docTxnFlows = DB::transaction
        (
            function() use ($procType, $docTxnFlowDataList)
            {
                //process docTxnFlowDataList
                $models = array();
                foreach($docTxnFlowDataList as $docTxnFlowData)
                {
                    //verify the frDoc is not closed for this procType
                    $tmpDocTxnFlow = self::findByProcTypeAndFrHdrId($procType, $docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_id']);
                    if(!empty($tmpDocTxnFlow))
                    {
                        $exc = new ApiException(__('SlsInv.fr_doc_is_closed', ['docType'=>$docTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$docTxnFlowData['fr_doc_hdr_code']]));
                        $exc->addData($docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_code']);
                        throw $exc;
                    }
                    
                    $docTxnFlowModel = new DocTxnFlow;
                    $docTxnFlowModel = RepositoryUtils::dataToModel($docTxnFlowModel, $docTxnFlowData);
                    $docTxnFlowModel->proc_type = $procType;
                    $docTxnFlowModel->to_doc_hdr_type = '';
                    $docTxnFlowModel->to_doc_hdr_id = 0;
                    $docTxnFlowModel->save();
                    $models[] = $docTxnFlowModel;
                }

                return $models;
            }, 
            5 //reattempt times
        );
        return $docTxnFlows;
    }

    static public function findAllByFrHdrId($frHdrType, $frHdrId) 
	{
        $docTxnFlows = DocTxnFlow::where(array(
            'fr_doc_hdr_type' => $frHdrType,
            'fr_doc_hdr_id' => $frHdrId
            ))
            ->orderBy('id')
            ->get();
            //->toSql();
            //dd($docTxnFlow);
        return $docTxnFlows;
    }

    static public function findAllByToHdrId($toHdrType, $toHdrId) 
	{
        $docTxnFlows = DocTxnFlow::where(array(
            'to_doc_hdr_type' => $toHdrType,
            'to_doc_hdr_id' => $toHdrId
            ))
            ->orderBy('id')
            ->get();
            //->toSql();
            //dd($docTxnFlow);
        return $docTxnFlows;
    }
}