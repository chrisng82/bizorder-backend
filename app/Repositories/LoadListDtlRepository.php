<?php

namespace App\Repositories;

use App\LoadListHdr;
use App\LoadListDtl;
use App\LoadListCDtl;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\DB;

class LoadListDtlRepository 
{
    static public function findAllByOutbOrdHdrId($outbOrdHdrId) 
	{
        $loadListDtls = LoadListDtl::where('outb_ord_hdr_id', $outbOrdHdrId)
        ->get();
        return $loadListDtls;
    }

    static public function findAllByOutbOrdDtlId($outbOrdHdrId, $outbOrdDtlId) 
	{
        $loadListDtls = LoadListDtl::where('outb_ord_hdr_id', $outbOrdHdrId)
        ->where('outb_ord_dtl_id', $outbOrdDtlId)
        ->get();
        return $loadListDtls;
    }
}