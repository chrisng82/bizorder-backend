<?php

namespace App\Repositories;

use Spatie\Permission\Models\Role;
use App\Services\Utils\ApiException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class RoleRepository
{
    static public function findAll($sorts, $filters = array(), $pageSize = 20) 
	{
        $roles = Role::select('roles.*');

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'name') == 0)
            {
                $roles->where('roles.name', 'LIKE', '%'.$filter['value'].'%');
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'name') == 0)
            {
                $roles->orderBy('roles.name', $sort['order']);
            }
        }
        
        if($pageSize > 0)
        {
            return $roles
                ->paginate($pageSize);
        }
        else
        {
            return $roles
                ->paginate(PHP_INT_MAX);
        }
    }

    static public function syncRoleExcel01($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = Role::findOrCreate($data['name'], 'web');

                return $model;
            }
        );
        return $model;
    }

    static public function select2($search, $filters, $pageSize = 10) 
	{
        $roles = Role::where(function($q) use($search) {
            $q->orWhere('name', 'LIKE', '%' . $search . '%');
        });
        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'user_id') == 0)
            {
                $operator = 'NOT IN';
                if(isset($filter['operator']))
                {
                    $operator = $filter['operator'];
                }
                if(strcmp($operator, 'NOT IN') == 0)
                {  
                    $roles->whereRaw('roles.id NOT IN(SELECT role_id FROM model_has_roles WHERE model_type = ? AND model_id = ?)', array('App\User', $filter['value']));
                }
                elseif(strcmp($operator, 'IN') == 0)
                {
                    $roles->whereRaw('roles.id IN(SELECT role_id FROM model_has_roles WHERE model_type = ? AND model_id = ?)', array('App\User', $filter['value']));
                }                
            }
        }
        return $roles
            ->orderBy('name', 'ASC')
            ->paginate($pageSize);
    }

    static public function findByPk($id) 
	{
        $model = Role::where('id', $id)
            ->first();

        return $model;
    }
}