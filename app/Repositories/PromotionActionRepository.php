<?php

namespace App\Repositories;

use App\Models\Promotion;
use App\Models\PromotionAction;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Services\Utils\RepositoryUtils;

class PromotionActionRepository 
{
    static public function findAllByPromotionId($promotionId) 
	{
        $actions = PromotionAction::where('promotion_id', $promotionId)
            ->orderBy('id', 'ASC')
            ->get();

        return $actions;
    }

    static public function findByPromotionIdAndType($promotionId, $actionType) 
	{
        $action = PromotionAction::where('promotion_id', $promotionId)
            ->where('type', $actionType)
            ->first();

        return $action;
    }

    static public function createModel($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = new PromotionAction;
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return $model;
            }, 
            5 //reattempt times
        );
        return $model;
    }
    
    static public function updateModel($data) 
	{
        $result = DB::transaction
        (
            function() use ($data)
            {
                //update Item
                $model = PromotionAction::lockForUpdate()->find($data['id']);
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return array(
                    'model' => $model
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function deleteModel($id) 
	{
        PromotionAction::destroy($id);
    }
}