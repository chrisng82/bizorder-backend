<?php

namespace App\Repositories;

use App\Company;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Services\Utils\RepositoryUtils;

class CompanyRepository 
{
    static public function findAll($sorts, $filters = array(), $pageSize = 20) 
	{
        $users = Company::select('companies.*');

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'code') == 0)
            {
                $users->where('companies.code', 'LIKE', '%'.$filter['value'].'%');
            }
            elseif(strcmp($filter['field'], 'name_01') == 0)
            {
                $users->where('companies.name_01', $filter['value']);
            }
            elseif(strcmp($filter['field'], 'name_02') == 0)
            {
                $users->where('companies.name_02', $filter['value']);
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'code') == 0)
            {
                $users->orderBy('code', $sort['order']);
            }
            elseif(strcmp($sort['field'], 'name_01') == 0)
            {
                $users->orderBy('name_01', $sort['order']);
            }
            elseif(strcmp($sort['field'], 'name_02') == 0)
            {
                $users->orderBy('name_02', $sort['order']);
            }
        }
        
        if($pageSize > 0)
        {
            return $users
                ->paginate($pageSize);
        }
        else
        {
            return $users
                ->paginate(PHP_INT_MAX);
        }
    }

    static public function select2($search, $filters, $pageSize = 10) 
	{
        $companies = Company::where(function($q) use($search) {
            $q->where('code', 'LIKE', '%' . $search . '%')
            ->orWhere('name_01', 'LIKE', '%' . $search . '%');
        });
        if(count($filters) >= 1)
        {
            $companies = $companies->where($filters);
        }
        return $companies
            ->orderBy('code', 'ASC')
            ->paginate($pageSize);
    }

    static public function findTop() 
	{
        $model = Company::first();

        return $model;
    }

    static public function findByPk($id) 
	{
        $model = Company::where('id', $id)
            ->first();

        return $model;
    }

    static public function findByCode($code) 
	{
        $model = Company::where('code', $code)
            ->first();

        return $model;
    }
    
    static public function createModel($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = new Company;
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return $model;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function updateModel($data) 
	{
        $result = DB::transaction
        (
            function() use ($data)
            {
                //update Item
                $model = Company::lockForUpdate()->find($data['id']);
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return array(
                    'model' => $model
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }
}