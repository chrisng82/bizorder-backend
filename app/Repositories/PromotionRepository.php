<?php

namespace App\Repositories;

use App\Item;
use App\Models\Promotion;
use App\Models\PromotionVariant;
use App\Models\PromotionRule;
use App\Models\PromotionAction;
use App\Repositories\ItemRepository;
use App\Repositories\UomRepository;
use App\Repositories\DebtorRepository;
use App\Services\Env\PromotionType;
use App\Services\Env\PromotionActionType;
use App\Services\Env\PromotionRuleType;
use App\Services\Env\ResStatus;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Services\Utils\RepositoryUtils;

class PromotionRepository 
{
    static public function createModel($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = new Promotion;
                $model = RepositoryUtils::dataToModel($model, $data);
                $promotion = Promotion::where('division_id', $model->division_id)->orderBy('sequence', 'desc')->first();
                if ($promotion)
                {
                    $model->sequence = $promotion->sequence + 1;
                }
                else
                {
                    $model->sequence = 1;
                }
                $model->save();

                return $model;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function createVariantModel($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = new PromotionVariant;
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return $model;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function deleteModel($id)
    {
        DB::beginTransaction();

        try {
            $promotion = Promotion::find($id);
            Promotion::destroy($id);
            PromotionAction::where('promotion_id', $id)->delete();
            PromotionRule::where('promotion_id', $id)->delete();
            PromotionVariant::where('promotion_id', $id)->delete();
            DB::commit();

            return $promotion;
        } catch(\Exception $e) {
            DB::rollback();
            throw new Exception($e);
        }
    }

    static public function deleteVariantModel($id) 
	{
        PromotionVariant::destroy($id);
    }

    static public function findAll($divisionId, $sorts, $filters = array(), $pageSize = 20, $type=null) 
	{
        $promotions = Promotion::select('promotions.*');
        $promotions->where('promotions.division_id', $divisionId);

        if ($type)
        {
            if (is_array($type)) {
                $promotions->whereIn('promotions.type', $type);
            }
            else {
                $promotions->where('promotions.type', $type);
            }
        }

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'code') == 0)
            {
                $promotions->where('promotions.code', 'LIKE', '%'.$filter['value'].'%');
            }
            elseif(strcmp($filter['field'], 'desc_01') == 0)
            {
                $promotions->where('promotions.desc_01', $filter['value']);
            }
            elseif(strcmp($filter['field'], 'desc_02') == 0)
            {
                $promotions->where('promotions.desc_02', $filter['value']);
            }
            elseif(strcmp($filter['field'], 'type') == 0)
            {
                if(strpos($filter['value'], ',') !== false) {
                    $promotions->whereIn('promotions.type', explode(',', $filter['value']));
                }
                else {
                    $promotions->where('promotions.type', $filter['value']);
                }
            }
            elseif(strcmp($filter['field'], 'status') == 0)
            {
                if(strpos($filter['value'], ',') !== false) {
                    $promotions->whereIn('promotions.status', explode(',', $filter['value']));
                }
                else if($filter['value'] > 0)
                {
                    $promotions->where('promotions.status', $filter['value']);
                }
            }
            elseif(strcmp($filter['field'], 'validity') == 0)
            {
                if ($filter['value'] == 'ongoing') {
                    $promotions->whereDate('valid_from', '<=', date('Y-m-d'))
                        ->whereDate('valid_to', '>=', date('Y-m-d'));
                }
                elseif ($filter['value'] == 'upcoming') {
                    $promotions->whereDate('valid_from', '>', date('Y-m-d'));
                }
                elseif ($filter['value'] == 'expired') {
                    $promotions->whereDate('valid_to', '<', date('Y-m-d'));
                }
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'code') == 0)
            {
                $promotions->orderBy('code', $sort['order']);
            }
            elseif(strcmp($sort['field'], 'desc_01') == 0)
            {
                $promotions->orderBy('desc_01', $sort['order']);
            }
            elseif(strcmp($sort['field'], 'desc_02') == 0)
            {
                $promotions->orderBy('desc_02', $sort['order']);
            }
            elseif(strcmp($sort['field'], 'sequence') == 0)
            {
                $promotions->orderBy('sequence', $sort['order']);
            }
        }
        
        if($pageSize > 0)
        {
            return $promotions
                ->paginate($pageSize);
        }
        else
        {
            return $promotions
                ->paginate(PHP_INT_MAX);
        }
    }

    static public function findAllActiveByItem($itemId) 
	{
        $result = array();
        $promotions = Promotion::select('promotions.*')
            ->active()
            ->join('promotion_actions', 'promotions.id', '=', 'promotion_actions.promotion_id')
            ->whereHas('variants', function ($q) use ($itemId) {
                $q->where('item_id', $itemId);
            });
        $result = $promotions->orderBy('valid_from', 'ASC')->orderBy('code', 'ASC')->get();

        if ($result->isEmpty()) {
            $item = ItemRepository::findByPk($itemId);
            //item group 01
            if ($item->item_group_01_id > 0) {
                $promotions = Promotion::active()->whereHas('variants', function ($q) use ($item) {
                    $q->where('item_group_01_id', $item->item_group_01_id);
                });
                $result = $promotions->orderBy('valid_from', 'ASC')->orderBy('code', 'ASC')->get();
            }
            if ($result->isEmpty() && $item->item_group_02_id > 0) {
                $promotions = Promotion::active()->whereHas('variants', function ($q) use ($item) {
                    $q->where('item_group_02_id', $item->item_group_02_id);
                });
                $result = $promotions->orderBy('valid_from', 'ASC')->orderBy('code', 'ASC')->get();
            }
            if ($result->isEmpty() && $item->item_group_03_id > 0) {
                $promotions = Promotion::active()->whereHas('variants', function ($q) use ($item) {
                    $q->where('item_group_03_id', $item->item_group_03_id);
                });
                $result = $promotions->orderBy('valid_from', 'ASC')->orderBy('code', 'ASC')->get();
            }
        }
        
        return $result;
    }

    static public function findAllByItem($itemId) 
	{
        $result = array();
        $promotions = Promotion::whereHas('variants', function ($q) use ($itemId) {
            $q->where('item_id', $itemId);
        });
        $result = $promotions->orderBy('valid_from', 'ASC')->orderBy('code', 'ASC')->get();

        if ($result->isEmpty()) {
            $item = ItemRepository::findByPk($itemId);
            //item group 01
            if ($item->item_group_01_id > 0) {
                $promotions = Promotion::whereHas('variants', function ($q) use ($item) {
                    $q->where('item_group_01_id', $item->item_group_01_id);
                });
                $result = $promotions->orderBy('valid_from', 'ASC')->orderBy('code', 'ASC')->get();
            }
            if ($result->isEmpty() && $item->item_group_02_id > 0) {
                $promotions = Promotion::whereHas('variants', function ($q) use ($item) {
                    $q->where('item_group_02_id', $item->item_group_02_id);
                });
                $result = $promotions->orderBy('valid_from', 'ASC')->orderBy('code', 'ASC')->get();
            }
            if ($result->isEmpty() && $item->item_group_03_id > 0) {
                $promotions = Promotion::whereHas('variants', function ($q) use ($item) {
                    $q->where('item_group_03_id', $item->item_group_03_id);
                });
                $result = $promotions->orderBy('valid_from', 'ASC')->orderBy('code', 'ASC')->get();
            }
        }
        
        return $result;
    }

    static public function findByCode($code) 
	{
        $model = Promotion::where('code', $code)
            ->first();

        return $model;
    }

    static public function findByPk($id) 
	{
        $model = Promotion::where('id', $id)
            ->first();

        return $model;
    }

    static public function findItemsByPromotion($promotion_id)
    {
        $item_types = PromotionVariant::where('promotion_variants.promotion_id', $promotion_id)
                        ->select('variant_type', 'item_id', 'item_group_01_id', 'item_group_02_id', 'item_group_03_id')->get();

        $item_list_array = [];
        $item_array = [];
        $item_array['title'] = 'ITEMS';
        $item_array['expanded'] = false;
        $item_group01_array = [];
        $item_group02_array = [];
        $item_group03_array = [];
        $has_items = false;

        foreach($item_types as $type) {

            if($type->variant_type == 'item') {

                $item = ItemRepository::findByPkWithPhotoAndUom($type->item_id);

                if($item) {
                    if(!$has_items) {
                        $has_items = true;
                    }
                    
                    $item_array['items'][] = $item;
                }

            } else if($type->variant_type == 'item_group01') {

                $has_item_group01 = false;
                $item_group01_item_array = [];

                $item_group01s = Item::join('item_group01s', 'items.item_group_01_id', 'item_group01s.id')
                                ->where('item_group_01_id', $type->item_group_01_id)
                                ->select('items.id', 'item_group01s.code')->get();

                foreach($item_group01s as $item_group01) {
                    $item = ItemRepository::findByPkWithPhotoAndUom($item_group01->id);

                    if($item) {
                        if(!$has_item_group01) {
                            $item_group01_item_array['expanded'] = false;
                            $item_group01_item_array['title'] = $item_group01->code;
                            $has_item_group01 = true;
                        }

                        $item_group01_item_array['items'][] = $item;
                    }
                }

                $item_group01_array[] = $item_group01_item_array;

            } else if($type->variant_type == 'item_group02') {

                $has_item_group02 = false;
                $item_group02_item_array = [];

                $item_group02s = Item::join('item_group02s', 'items.item_group_02_id', 'item_group02s.id')
                                ->where('item_group_02_id', $type->item_group_02_id)
                                ->select('items.id', 'item_group02s.code')->get();

                foreach($item_group02s as $item_group02) {
                    $item = ItemRepository::findByPkWithPhotoAndUom($item_group02->id);

                    if($item) {
                        if(!$has_item_group02) {
                            $item_group02_item_array['expanded'] = false;
                            $item_group02_item_array['title'] = $item_group02->code;
                            $has_item_group02 = true;
                        }
                        $item_group02_item_array['items'][] = $item;
                    }
                }

                $item_group02_array[] = $item_group02_item_array;

            } else if($type->variant_type == 'item_group03') {

                $has_item_group03 = false;
                $item_group03_item_array = [];

                $item_group03s = Item::join('item_group03s', 'items.item_group_03_id', 'item_group03s.id')
                                ->where('item_group_03_id', $type->item_group_03_id)
                                ->select('items.id', 'item_group03s.code')->get();

                foreach($item_group03s as $item_group03) {
                    $item = ItemRepository::findByPkWithPhotoAndUom($item_group03->id);
                    
                    if($item) {
                        if(!$has_item_group03) {
                            $item_group03_item_array['expanded'] = false;
                            $item_group03_item_array['title'] = $item_group03->code;
                            $has_item_group03 = true;
                        }
                        $item_group03_item_array['items'][] = $item;
                    }
                }

                $item_group03_array[] = $item_group03_item_array;
                
            }
        }

        if($has_items) {
            $item_list_array[] = $item_array;
        }
        foreach($item_group01_array as $group) {
            $item_list_array[] = $group;
        }
        foreach($item_group02_array as $group) {
            $item_list_array[] = $group;
        }
        foreach($item_group03_array as $group) {
            $item_list_array[] = $group;
        }

        return $item_list_array;
    }

    static public function findTop() 
	{
        $model = Promotion::first();

        return $model;
    }

    static public function reorderPromo($curPromoId, $prevPromoId, $curDivisionId)
    {
        $data = DB::transaction
        (
            function() use ($curPromoId, $prevPromoId, $curDivisionId)
            {
                $data = Promotion::where('sequence', $prevPromoId)
                ->where('division_id', $curDivisionId)->first();
                $data->sequence = 0;
                $data->save();

                if($prevPromoId > $curPromoId) {
                    for($i = $prevPromoId - 1; $i >= $curPromoId; $i--) {
                        $data = Promotion::where('sequence', '=', $i)
                        ->where('division_id', '=', $curDivisionId)->first();
                        $data->sequence = $i + 1;
                        $data->save();
                    }
                } else {
                    for($i = $prevPromoId + 1; $i <= $curPromoId; $i++) {
                        $data = Promotion::where('sequence', '=', $i)
                        ->where('division_id', '=', $curDivisionId)->first();
                        $data->sequence = $i - 1;
                        $data->save();
                    }
                }

                $data = Promotion::where('sequence', '=', 0)
                ->where('division_id', '=', $curDivisionId)->first();
                $data->sequence = $curPromoId;
                $data->save();

                return $data;
            },
            5
        );

        return $data;
    }

    static public function select2($search, $filters, $pageSize = 10) 
	{
        $promotions = Promotion::where(function($q) use($search) {
            $q->where('code', 'LIKE', '%' . $search . '%')
            ->orWhere('desc_01', 'LIKE', '%' . $search . '%');
        });
        if(count($filters) >= 1)
        {
            $promotions = $promotions->where($filters);
        }
        return $promotions
            ->orderBy('code', 'ASC')
            ->paginate($pageSize);
    }

    static public function syncPromoSync01($code, $promotionDataArray, $divisionId, $syncSettingHdrId) 
	{
        $division = DivisionRepository::findByPk($divisionId);
        if (empty($division)) {
            throw new \Exception('Division not found.');
        }

        if (count($promotionDataArray) == 0)
        {
            return null;
        }

        $promotionModel = DB::transaction
        (
            function() use ($code, $promotionDataArray, $divisionId, $syncSettingHdrId)
            {
                $promotionModel = Promotion::select('promotions.*')
                    ->where('promotions.division_id', $divisionId)
                    ->where('code', $code)
                    ->first();
                
                if (!$promotionModel) {
                    $promotionModel = new Promotion;
                    $promotionModel->code = $code;
                }

                $data = $promotionDataArray[0];

                $promotionModel->name = $data['pg_desc'];
                $promotionModel->desc_01 = $data['pg_desc'];
                $promotionModel->desc_02 = '';
                $promotionModel->type = PromotionType::$MAP['disc_fixed_price'];
                $promotionModel->division_id = $divisionId;
                $promotionModel->valid_from = '2010-01-01 00:00:00';
                $promotionModel->valid_to = '2038-01-19 00:00:00';
                $promotionModel->status = ResStatus::$MAP['ACTIVE'];
                $promotionModel->position = 0;

                $variants = array();
                $rules = array();

                if($promotionModel->id > 0)
                {
                    //update record
                    $promotionModel->save();

                    //delete all previous variants and rules
                    PromotionVariant::where('promotion_id', $promotionModel->id)->delete();
                    PromotionRule::where('promotion_id', $promotionModel->id)->delete();
                }
                else
                {
                    //create record
                    $promotionModel->save();
                }
                
                //process promotion variant
                foreach ($promotionDataArray as $promotionData)
                {
                    $itemCode = $promotionData['item_code'];
                    $uomCode = $promotionData['uom_code'];
                    $price = $promotionData['unit_price'];
                    
                    $itemModel = ItemRepository::findByCodeAndDivision($itemCode, $divisionId);
                    $uomModel = UomRepository::findByCode($uomCode);
                    if (empty($itemModel) || empty($uomModel)) {
                        continue;
                    }
                    $itemUomModel = ItemUomRepository::findByItemIdAndUomId($itemModel->id, $uomModel->id);
                    if (empty($itemUomModel) || $itemUomModel->uom_rate > 1) {
                        //only save base uom item price
                        continue;
                    }

                    $data = array();
                    $data['variant_type'] = 'item';
                    $data['promotion_id'] = $promotionModel->id;
                    $data['item_id'] = $itemModel->id;
                    $data['disc_fixed_price'] = $price;
                    $variantModel = PromotionRepository::createVariantModel($data);
                    $variants[] = $variantModel;
                }

                //process promotion rule
                $promotionData = $promotionDataArray[0];
                if (!empty($promotionData['cust_code']) && $promotionData['cust_code'] != null) {
                    $custCodeArray = explode(',', $promotionData['cust_code']);
                    $debtorIdArray = array();
                    foreach ($custCodeArray as $custCode)
                    {
                        $debtorModel = DebtorRepository::findByCodeAndDivision($custCode, $divisionId);
                        if (!empty($debtorModel)) {
                            $debtorIdArray[] = $debtorModel->id;
                        }
                    }
                    $data = array();
                    $config = ['debtor_id'=>implode(',', $debtorIdArray)];
                    $data['config'] = $config; 
                    $data['type'] = PromotionRuleType::$MAP['debtor'];
                    $data['promotion_id'] = $promotionModel->id;
                    $ruleModel = PromotionRuleRepository::createModel($data);
                    $rules[] = $ruleModel;
                }
            }, 
            5 //reattempt times
        );
        return $promotionModel;
    }

    static public function updateModel($data) 
	{
        $result = DB::transaction
        (
            function() use ($data)
            {
                //update Item
                $model = Promotion::lockForUpdate()->find($data['id']);
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return array(
                    'model' => $model
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }
    
    static public function updateVariantModel($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = PromotionVariant::lockForUpdate()->find($data['id']);
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return $model;
            }, 
            5 //reattempt times
        );
        return $model;
    }
}