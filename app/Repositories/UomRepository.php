<?php

namespace App\Repositories;

use App\Uom;
use App\Services\Utils\ApiException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class UomRepository
{
    static public function findByCode($code) 
	{
        $uom = Uom::where('code', $code)
        ->first();
        return $uom;
    }

    static public function findAll($sorts, $filters = array(), $pageSize = 20) 
	{
        $uoms = Uom::select('uoms.*');

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'code') == 0)
            {
                $uoms->orderBy('code', $sort['order']);
            }
        }
        if($pageSize > 0)
        {
            return $uoms
            ->paginate($pageSize);
        }
        else
        {
            return $uoms
            ->paginate(PHP_INT_MAX);
        }
    }

    static public function syncSlsOrdSync01($division, $data) 
	{
        $model = DB::transaction
        (
            function() use ($division, $data)
            {
                $attributes = array('code'=>$data['uom_code']);
                $values = array('desc_01'=>$data['uom_code']);
                $model = Uom::firstOrCreate($attributes, $values);

                return $model;
            }
        );
        return $model;
    }

    static public function select2($search, $filters, $pageSize = 10) 
	{
        $uoms = Uom::select('uoms.*')
        ->where(function($q) use($search) {
            $q->orWhere('uoms.code', 'LIKE', '%' . $search . '%');
        });
        foreach($filters as $filter)
        {
            
        }
        return $uoms
            ->orderBy('uoms.code', 'ASC')
            ->paginate($pageSize);
    }
}