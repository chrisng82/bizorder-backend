<?php

namespace App\Repositories;

use App\SyncSettingHdr;
use App\SyncSettingDtl;
use App\Services\Utils\ApiException;
use App\Services\Utils\RepositoryUtils;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class SyncSettingHdrRepository
{
    static public function findByProcTypeAndSiteFlowId($procType, $siteFlowId) 
	{
        $syncSettingHdr = SyncSettingHdr::where('proc_type', $procType)
        ->where('site_flow_id', $siteFlowId)->first();
        return $syncSettingHdr;
    }

    static public function findByProcTypeAndDivisionId($procType, $divisionId) 
	{
        $syncSettingHdr = SyncSettingHdr::where('proc_type', $procType)
        ->where('division_id', $divisionId)->first();
        return $syncSettingHdr;
    }
    
    static public function findByPk($id) 
	{
        $model = SyncSettingHdr::where('id', $id)
            ->first();

        return $model;
    }

    static public function createHeader($data)
    {
        $hdrModel = new SyncSettingHdr;
        $hdrModel = RepositoryUtils::dataToModel($hdrModel, $data);
        $hdrModel->save();

        return $hdrModel;
    }

    static public function updateModel($data, $detailData) 
	{
        $result = DB::transaction
        (
            function() use ($data, $detailData)
            {
                //update Item
                $model = SyncSettingHdr::lockForUpdate()->find($data['id']);
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                foreach ($detailData as $field => $value)
                {
                    $dtlModel = SyncSettingDtl::where('hdr_id', $model->id)
                        ->where('field', $field)->first();
                    
                    if ($dtlModel) {
                        $dtlModel->value = $value;
                    }
                    else {
                        $dtlModel = new SyncSettingDtl;
                        $dtlModel->hdr_id = $model->id;
                        $dtlModel->field = $field;
                        $dtlModel->value = $value;
                    }
                    $dtlModel->save();
                }

                return array(
                    'model' => $model
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }
}