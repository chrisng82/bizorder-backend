<?php

namespace App\Repositories;

use App\SMobileSchema;

class SMobileSchemaRepository 
{
    static public function findAllByMobileApp($mobileApp) 
	{
        $sMobileSchemas = SMobileSchema::where('mobile_app', $mobileApp)
            ->orderBy('seq_no')
            ->get();

        return $sMobileSchemas;
    }

    static public function findByMobileAppAndTableName($mobileApp, $tableName) 
	{
        $sMobileSchema = SMobileSchema::where('mobile_app', $mobileApp)
            ->where('table_name', $tableName)
            ->first();

        return $sMobileSchema;
    }
}