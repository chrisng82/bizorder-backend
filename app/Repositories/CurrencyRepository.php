<?php

namespace App\Repositories;

use App\Currency;
use App\Services\Env\ItemType;
use App\Services\Env\ResStatus;
use App\Services\Env\RetrievalMethod;
use App\Services\Env\ScanMode;
use App\Services\Env\StorageClass;
use Illuminate\Support\Facades\DB;

class CurrencyRepository 
{
    static public function findRandom() 
	{
        $model = Currency::orderByRaw('RAND()')
            ->first();

        return $model;
    }

    static public function findAll($sorts, $filters = array(), $pageSize = 20) 
	{
        $currencies = Currency::select('currencies.*');

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'code') == 0)
            {
                $currencies->orderBy('code', $sort['order']);
            }
        }
        if($pageSize > 0)
        {
            return $currencies
            ->paginate($pageSize);
        }
        else
        {
            return $currencies
            ->paginate(PHP_INT_MAX);
        }
    }

    static public function findByCode($code) 
	{
        $currency = Currency::where('code', $code)
            ->first();

        return $currency;
    }

    static public function syncSlsOrdSync01($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $attributes = array('code'=>$data['currency_code']);
                $values = array(
                    'symbol'=>$data['currency_code'],
                    'currency_rate'=>$data['currency_rate'],
                );
                $model = Currency::firstOrCreate($attributes, $values);

                return $model;
            }
        );
        return $model;
    }

    static public function findByPk($id) 
	{
        $currency = Currency::where('id', $id)
            ->first();

        return $currency;
    }

    static public function select2($search, $filters, $pageSize = 10) 
	{
        $currencies = Currency::where(function($q) use($search) {
            $q->where('code', 'LIKE', '%' . $search . '%')
            ->orWhere('symbol', 'LIKE', '%' . $search . '%');
        });
        foreach($filters as $filter)
        {
            
        }
        return $currencies
            ->orderBy('code', 'ASC')
            ->paginate($pageSize);
    }
}