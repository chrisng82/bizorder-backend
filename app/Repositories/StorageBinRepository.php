<?php

namespace App\Repositories;

use App\QuantBal;
use App\StorageBin;
use App\StorageType;
use App\Services\Env\BinType;
use App\Services\Utils\RepositoryUtils;
use Illuminate\Support\Facades\DB;

class StorageBinRepository 
{
    static public function findByPk($id) 
	{
        $storageBin = StorageBin::where('id', $id)
        ->first();

        return $storageBin;
    }

    static public function findBySiteIdAndCode($siteId, $code) 
	{
        $storageBin = StorageBin::where('site_id', $siteId)
        ->where('code', $code)
        ->first();

        return $storageBin;
    }

    static public function findBySiteIdAndAttributes($siteId, $row, $bay, $level, $bin) 
	{
        $storageBin = StorageBin::select('storage_bins.*')
        ->leftJoin('storage_rows', 'storage_rows.id', '=', 'storage_bins.storage_row_id')
        ->leftJoin('storage_bays', 'storage_bays.id', '=', 'storage_bins.storage_bay_id')
        ->where('storage_bins.site_id', $siteId)
        ->where('storage_rows.code', $row) //A18
        ->where('storage_bays.code', 'LIKE', '%'.$bay) //1,2,...,18
        ->where('storage_bins.level', $level)
        ->where('storage_bins.code', 'LIKE', '%'.$bin) //A, B, C
        ->first();

        return $storageBin;
    }

    static public function findAllByAttributes($siteId, $storageRowIds, $storageBayIds, $levels) 
	{
        $storageBins = StorageBin::where('site_id', $siteId)
        ->when(!empty($storageRowIds), function($query) use ($storageRowIds) {
            return $query->whereIn('storage_row_id', $storageRowIds);
        })
        ->when(!empty($storageBayIds), function($query) use ($storageBayIds) {
            return $query->whereIn('storage_bay_id', $storageBayIds);
        })
        ->when(!empty($levels), function($query) use ($levels) {
            return $query->whereIn('level', $levels);
        })
        ->orderBy('code','ASC')
        ->get();

        return $storageBins;
    }

    static public function findAllByIds($ids) 
	{
        $storageBins = StorageBin::whereIn('id', $ids)
        ->orderBy('code','ASC')
        ->get();

        return $storageBins;
    }

    static public function findAllByBinTypes($siteId, $binTypes, $locationId) 
	{
        $storageBins = StorageBin::select('storage_bins.*')
        ->whereIn('storage_type_id', function ($query) use ($binTypes) {
            $query->select('id')
                ->from('storage_types')
                ->whereIn('bin_type', $binTypes);
        })
        ->where('storage_bins.site_id', $siteId)
        ->where('storage_bins.location_id', $locationId)
        ->get();

        return $storageBins;
    }

    static public function findPickFaceByStorageBayId($storageBayId) 
	{
        $storageBins = StorageBin::select('storage_bins.*')
        ->leftJoin('storage_types', 'storage_types.id', '=', 'storage_bins.storage_type_id')
        ->where('storage_bins.storage_bay_id', $storageBayId)
        ->where(function($q) {
            $q->orWhere('storage_types.bin_type', BinType::$MAP['CASE_STORAGE'])
            ->orWhere('storage_types.bin_type', BinType::$MAP['BROKEN_CASE_STORAGE']);
        })
        ->get();

        return $storageBins;
    }

    static public function findAllByStrategy($siteId, $locationIds, $storageTypeIds, $storageRowIds, $storageBayIds, $storageBinIds, $defaultBaySequence) 
	{
        $storageBins = StorageBin::where('site_id', $siteId)
        ->when(!empty($locationIds), function($query) use ($locationIds) {
            return $query->whereIn('location_id', $locationIds);
        })
        ->when(!empty($storageTypeIds), function($query) use ($storageTypeIds) {
            return $query->whereIn('storage_type_id', $storageTypeIds);
        })
        ->when(!empty($storageRowIds), function($query) use ($storageRowIds) {
            return $query->whereIn('storage_row_id', $storageRowIds);
        })
        ->when(!empty($storageBayIds), function($query) use ($storageBayIds) {
            return $query->whereIn('storage_bay_id', $storageBayIds);
        })
        ->when(!empty($storageBinIds), function($query) use ($storageBinIds) {
            return $query->whereIn('id', $storageBinIds);
        })
        ->when($defaultBaySequence > 0, function($query) use ($defaultBaySequence) {
            return $query->orderByRaw('ABS(CAST(bay_sequence AS SIGNED) - '.$defaultBaySequence.')');
        })
        ->get();

        return $storageBins;
    }

    static public function filterByStrategy($allStorageBins, $siteId, $locationIds, $storageTypeIds, $storageRowIds, $storageBayIds, $storageBinIds, $defaultBaySequence) 
	{
        $storageBins = array();
        foreach($allStorageBins as $storageBin)
        {
            if($storageBin->site_id != $siteId)
            {
                continue;
            }
            if(!empty($locationIds)
            && !in_array($storageBin->location_id, $locationIds))
            {
                continue;
            }
            if(!empty($storageTypeIds)
            && !in_array($storageBin->storage_type_id, $storageTypeIds))
            {
                continue;
            }
            if(!empty($storageRowIds)
            && !in_array($storageBin->storage_row_id, $storageRowIds))
            {
                continue;
            }
            if(!empty($storageBayIds)
            && !in_array($storageBin->storage_bay_id, $storageBayIds))
            {
                continue;
            }
            if(!empty($storageBinIds)
            && !in_array($storageBin->id, $storageBinIds))
            {
                continue;
            }

            $storageBin->bay_sequence_distance = 0;
            if($defaultBaySequence > 0)
            {
                $storageBin->bay_sequence_distance = abs($storageBin->bay_sequence - $defaultBaySequence);
            }

            $storageBins[] = $storageBin;
        }

        //sort the storageBins by bay_sequence_distance ASC, level ASC, code ASC
        usort($storageBins, function ($a, $b) {
            if($a->bay_sequence_distance == $b->bay_sequence_distance)
            {
                if($a->level == $b->level)
                {
                    return ($a->code < $b->code) ? -1 : 1;
                }
                return ($a->level < $b->level) ? -1 : 1;
            }
			return ($a->bay_sequence_distance < $b->bay_sequence_distance) ? -1 : 1;
        });

        return $storageBins;
    }

    static public function findAll($sorts, $filters = array(), $pageSize = 20) 
	{
        $leftJoinHash = array();
        $storageBins = StorageBin::select('storage_bins.*')
            //last_count_date
            ->selectRaw('IFNULL((
                SELECT MAX(cycle_count_hdrs.doc_date) 
                FROM cycle_count_dtls 
                LEFT JOIN cycle_count_hdrs ON cycle_count_dtls.hdr_id = cycle_count_hdrs.id 
                WHERE cycle_count_dtls.storage_bin_id = storage_bins.id AND cycle_count_hdrs.doc_status >= 50
                ), "1970-01-01") AS last_count_date');

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'code') == 0)
            {
                $storageBins->where('storage_bins.code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'storage_row_code') == 0)
            {
                $leftJoinHash['storage_rows'] = 'storage_rows';
                $storageBins->where('storage_rows.code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'storage_bay_code') == 0)
            {
                $leftJoinHash['storage_bays'] = 'storage_bays';
                $storageBins->where('storage_bays.code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'level') == 0)
            {
                $storageBins->where('level', 'LIKE', '%'.$filter['value'].'%');
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'code') == 0)
            {
                $storageBins->orderBy('storage_bins.code', $sort['order']);
            }            
            if(strcmp($sort['field'], 'storage_row_code') == 0)
            {
                $leftJoinHash['storage_rows'] = 'storage_rows';
                $storageBins->orderBy('storage_rows.code', $sort['order']);
            }
            if(strcmp($sort['field'], 'storage_bay_code') == 0)
            {
                $leftJoinHash['storage_bays'] = 'storage_bays';
                $storageBins->orderBy('storage_bays.code', $sort['order']);
            }
            if(strcmp($sort['field'], 'level') == 0)
            {
                $storageBins->orderBy('level', $sort['order']);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'storage_rows') == 0)
            {
                $storageBins->leftJoin('storage_rows', 'storage_rows.id', '=', 'storage_bins.storage_row_id');
            }
            if(strcmp($field, 'storage_bays') == 0)
            {
                $storageBins->leftJoin('storage_bays', 'storage_bays.id', '=', 'storage_bins.storage_bay_id');
            }
        }

        if($pageSize > 0)
        {
            return $storageBins
            ->paginate($pageSize);
        }
        else
        {
            return $storageBins
            ->paginate(PHP_INT_MAX);
        }
    }

    static public function select2($search, $filters, $pageSize = 10) 
	{
        $storageBins = StorageBin::select('storage_bins.*')
        ->where(function($q) use($search) {
            $q->where('storage_bins.code', 'LIKE', '%' . $search . '%')
            ->orWhere('storage_bins.desc_01', 'LIKE', '%' . $search . '%');
        })
        ->leftJoin('storage_types', 'storage_types.id', '=', 'storage_bins.storage_type_id');
        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'bin_types') == 0)
            {
                $storageBins->whereIn('storage_types.bin_type', $filter['value']);
            }
            if(strcmp($filter['field'], 'bin_type') == 0)
            {
                $storageBins->where('storage_types.bin_type', $filter['value']);
            }
        }
        return $storageBins
            ->orderBy('storage_bins.code', 'ASC')
            ->paginate($pageSize);
    }

    static public function findTop($filters) 
	{
        $storageBins = StorageBin::select('storage_bins.*')
        ->leftJoin('storage_types', 'storage_types.id', '=', 'storage_bins.storage_type_id');

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'bin_types') == 0)
            {
                $storageBins->whereIn('storage_types.bin_type', $filter['value']);
            }
            if(strcmp($filter['field'], 'bin_type') == 0)
            {
                $storageBins->where('storage_types.bin_type', $filter['value']);
            }
        }

        return $storageBins->first();
    }

    static public function queryByRowLevel($siteId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $storageBins = StorageBin::select('storage_bins.storage_row_id AS storage_row_id')
            ->selectRaw('storage_rows.code AS storage_row_code')
            ->selectRaw('storage_bins.level AS level')
            ->selectRaw('COUNT(DISTINCT storage_bins.storage_bay_id) AS storage_bay_count')
            ->leftJoin('storage_rows', 'storage_rows.id', '=', 'storage_bins.storage_row_id')
            ->where('storage_bins.site_id', $siteId)
            ->groupBy('storage_row_id', 'level')
            ->orderBy('storage_rows.code', 'ASC')
            ->orderBy('level', 'DESC');

        if($pageSize > 0)
        {
            return $storageBins
            ->paginate($pageSize);
        }
        else
        {
            return $storageBins
            ->paginate(PHP_INT_MAX);
        }
    }

    static public function findAllByRowLevel($storageRowId, $level) 
	{
        $storageBins = StorageBin::select('storage_bins.*')
            ->selectRaw('storage_bays.code AS storage_bay_code')
            ->leftJoin('storage_bays', 'storage_bays.id', '=', 'storage_bins.storage_bay_id')
            ->where('storage_bins.storage_row_id', $storageRowId)
            ->where('storage_bins.level', $level)
            ->orderBy('storage_bays.code', 'ASC')
            ->orderBy('storage_bins.code', 'ASC')
            ->get();

        return $storageBins;
    }

    static public function findAllByStorageBayId($storageBayId) 
	{
        $storageBins = StorageBin::select('storage_bins.*')
        ->where('storage_bins.storage_bay_id', $storageBayId)
        ->get();

        return $storageBins;
    }

    static public function findAllByStorageRowId($storageRowId) 
	{
        $storageBins = StorageBin::select('storage_bins.*')
        ->where('storage_bins.storage_row_id', $storageRowId)
        ->get();

        return $storageBins;
    }

    static public function plainCreate($data) 
	{
        $model = new StorageBin();
        $model = RepositoryUtils::dataToModel($model, $data);
        $model->save();

        $quantBals = QuantBal::select('quant_bals.*')
        ->where('storage_bin_id', $model->id)
        ->get();
        foreach($quantBals as $quantBal)
        {
            $quantBal->storage_row_id = $model->storage_row_id;
            $quantBal->floor = $model->floor;
            $quantBal->level = $model->level;
            $quantBal->bay_sequence = $model->bay_sequence;
        }

        return $model;
    }

    static public function plainUpdate($id, $data) 
	{
        $model = StorageBin::find($id);
        if(!empty($model))
        {
            $model = RepositoryUtils::dataToModel($model, $data);
            $model->save();

            $quantBals = QuantBal::select('quant_bals.*')
            ->where('storage_bin_id', $model->id)
            ->get();
            foreach($quantBals as $quantBal)
            {
                $quantBal->location_id = $model->location_id;
                $quantBal->storage_type_id = $model->storage_type_id;
                $quantBal->storage_row_id = $model->storage_row_id;
                $quantBal->storage_bay_id = $model->storage_bay_id;
                $quantBal->level = $model->level;
                $quantBal->bay_sequence = $model->bay_sequence;
                $quantBal->save();
            }
        }

        return $model;
    }
}