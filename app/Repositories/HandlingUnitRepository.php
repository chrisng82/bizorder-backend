<?php

namespace App\Repositories;

use App\HandlingUnit;
use App\Services\Env\ResStatus;
use App\Services\Env\HandlingType;
use Illuminate\Support\Facades\DB;

class HandlingUnitRepository 
{
    static public function findAllBySiteFlowId($siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $handlingUnits = HandlingUnit::select('handling_units.*')
        ->where('id', '>', 100)
        ->where('site_flow_id', $siteFlowId);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'barcode') == 0)
            {
                $handlingUnits->where('id', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'ref_code') == 0)
            {
                $handlingUnits->where('ref_code_01', 'LIKE', '%'.$filter['value'].'%');
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'barcode') == 0)
            {
                $handlingUnits->orderBy('id', $sort['order']);
            }
            elseif(strcmp($sort['field'], 'print_count') == 0)
            {
                $handlingUnits->selectRaw('(SELECT COUNT(print_res_txns.id) FROM print_res_txns WHERE res_type = "'.HandlingUnit::class.'" AND res_id = handling_units.id) AS print_count');
                $handlingUnits->orderBy('print_count', $sort['order']);
            }    
            elseif(strcmp($sort['field'], 'first_printed_at') == 0)
            {
                $handlingUnits->selectRaw('(SELECT MIN(print_res_txns.created_at) FROM print_res_txns WHERE res_type = "'.HandlingUnit::class.'" AND res_id = handling_units.id) AS first_printed_at');
                $handlingUnits->orderBy('first_printed_at', $sort['order']);
            }  
            elseif(strcmp($sort['field'], 'last_printed_at') == 0)
            {
                $handlingUnits->selectRaw('(SELECT MAX(print_res_txns.created_at) FROM print_res_txns WHERE res_type = "'.HandlingUnit::class.'" AND res_id = handling_units.id) AS last_printed_at');
                $handlingUnits->orderBy('last_printed_at', $sort['order']);
            }
            elseif(strcmp($sort['field'], 'created_at') == 0)
            {
                $handlingUnits->orderBy('id', $sort['order']);
            }
        }

        return $handlingUnits
        ->paginate($pageSize);
    }

    static public function findAllByIds($ids) 
	{
        $handlingUnits = HandlingUnit::whereIn('id', $ids)
            ->get();

        return $handlingUnits;
    }

    static public function findByPk($id) 
	{
        $handlingUnit = HandlingUnit::where('id', $id)
            ->first();

        return $handlingUnit;
    }

    static public function createHandlingUnitList01($procType, $siteFlowId, $noOfPallets) 
	{
        $handlingUnits = DB::transaction
        (
            function() use ($procType, $siteFlowId, $noOfPallets)
            {
                $handlingUnits = array();
                for($a = 0; $a < $noOfPallets; $a++)
                {
                    $newHandlingUnit = new HandlingUnit;
                    $newHandlingUnit->site_flow_id = $siteFlowId;
                    $newHandlingUnit->handling_type = HandlingType::$MAP['PALLET'];
                    $newHandlingUnit->save();

                    $handlingUnits[] = $newHandlingUnit;
                }

                return $handlingUnits;
            }, 
            5 //reattempt times
        );
        return $handlingUnits;
    }

    static public function firstOrCreate($id, $siteFlowId, $handlingType) 
	{
        $handlingUnit = HandlingUnit::where('id', $id)
            ->first();
        if(empty($handlingUnit))
        {
            $handlingUnit = new HandlingUnit();
            $handlingUnit->id = $id;
            $handlingUnit->ref_code_01 = '';
            $handlingUnit->ref_code_02 = '';
            $handlingUnit->site_flow_id = $siteFlowId;
            $handlingUnit->handling_type = $handlingType;
            $handlingUnit->save();
        }
        return $handlingUnit;
    }

    static public function firstOrCreateByRefCode01($refCode01, $siteFlowId, $storageBinCode, $handlingType) 
	{
        $handlingUnit = HandlingUnit::firstOrCreate(
            array(
                'ref_code_01' => $refCode01,
                'ref_code_02' => $storageBinCode
            ),
            array(
                'site_flow_id' => $siteFlowId,
                'handling_type' => $handlingType,
            )
        );
        return $handlingUnit;
    }

    static public function select2($search, $filters, $pageSize = 10) 
	{
        $search = trim($search);
        $handlingUnits = HandlingUnit::select('handling_units.*')
        ->where(function($q) use($search) {
            $q->orWhere('handling_units.id', '=', $search)
            ->orWhere('handling_units.ref_code_01', 'LIKE', '%' . $search . '%');
        });
        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'storage_bin_id') == 0)
            {
                $handlingUnits->whereExists(function ($query) use ($filter) {
                    $query->select(DB::raw(1))
                        ->from('quant_bals')
                        ->whereRaw('handling_unit_id = handling_units.id')
                        ->where('storage_bin_id', $filter['value'])
                        ->where('balance_unit_qty', '<>', 0);
                });
            }
            if(strcmp($filter['field'], 'site_flow_id') == 0)
            {
                $handlingUnits->where('site_flow_id', $filter['value']);
            }
            if(strcmp($filter['field'], 'handling_type') == 0)
            {
                $handlingUnits->where('handling_type', $filter['value']);
            }
            if(strcmp($filter['field'], 'status') == 0)
            {
                if($filter['value'] == ResStatus::$MAP['NEW'])
                {
                    //new handling unit will not associated with any quantBal
                    $handlingUnits->whereNotExists(function ($query) {
                        $query->select(DB::raw(1))
                            ->from('quant_bals')
                            ->whereRaw('quant_bals.handling_unit_id = handling_units.id')
                            ->where('quant_bals.balance_unit_qty', '<>', 0);
                    });
                }
            }
        }
        return $handlingUnits
            ->orderBy('id', 'ASC')
            ->paginate($pageSize);
    }

    static public function findTop($filters) 
	{
        $model = HandlingUnit::select('handling_units.*');

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'storage_bin_id') == 0)
            {
                $handlingUnits->whereExists(function ($query) use ($filter) {
                    $query->select(DB::raw(1))
                        ->from('quant_bals')
                        ->where('handling_unit_id', 'handling_units.id')
                        ->where('storage_bin_id', $filter['value']);
                });
            }
        }

        return $model->first();
    }

    static public function create($siteFlowId, $handlingType) 
	{
        $handlingUnit = new HandlingUnit();
        $handlingUnit->ref_code_01 = '';
        $handlingUnit->ref_code_02 = '';
        $handlingUnit->site_flow_id = $siteFlowId;
        $handlingUnit->handling_type = $handlingType;
        $handlingUnit->save();
        return $handlingUnit;
    }
}