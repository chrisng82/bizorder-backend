<?php

namespace App\Repositories;

use App\StorageRow;
use App\Services\Utils\RepositoryUtils;
use Illuminate\Support\Facades\DB;

class StorageRowRepository 
{
    static public function findBySiteIdAndCode($siteId, $code) 
	{
        $storageRow = StorageRow::where('site_id', $siteId)
        ->where('code', $code)
        ->first();

        return $storageRow;
    }

    static public function findAllBySiteId($siteId, $sorts = array(), $filters = array(), $pageSize = 20) 
	{
        $storageRows = StorageRow::where('site_id', $siteId)
        ->orderBy('aisle_sequence', 'ASC')
        ->orderBy('aisle_position', 'ASC');

        if($pageSize > 0)
        {
            return $storageRows
            ->paginate($pageSize);
        }
        else
        {
            return $storageRows
            ->paginate(PHP_INT_MAX);
        }
    }

    static public function select2($search, $filters, $pageSize = 10) 
	{
        $storageRows = StorageRow::select('storage_rows.*')
        ->where(function($q) use($search) {
            $q->orWhere('desc_01', 'LIKE', '%' . $search . '%')
            ->orWhere('desc_02', 'LIKE', '%' . $search . '%')
            ->orWhere('code', 'LIKE', '%' . $search . '%');
        });
        if(count($filters) >= 1)
        {
            $storageRows = $storageRows->where($filters);
        }
        return $storageRows
            ->orderBy('code', 'ASC')
            ->paginate($pageSize);
    }

    static public function create($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = new StorageRow();
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return $model;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function plainUpdate($id, $data) 
	{
        $model = StorageRow::find($id);
        if(!empty($model))
        {
            $model = RepositoryUtils::dataToModel($model, $data);
            $model->save();
        }

        return $model;
    }
}