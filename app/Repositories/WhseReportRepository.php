<?php

namespace App\Repositories;

use App\CountAdjDtl;
use App\CycleCountDtl;
use App\QuantBal;
use App\QuantBalTxn;
use App\QuantBalRsvdTxn;
use App\Services\Env\ResType;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class WhseReportRepository 
{
    static public function stockBalance($siteFlowId, $sorts, $criteria = array(), $columns = array(), $pageSize = 20) 
	{
        $today = date('Y-m-d');
        $backdate = $today;
        if(array_key_exists('date', $criteria))
        {
            $backdate = $criteria['date'];
        }

        $pickingUnitQtySql = '';
        $quantBalTxnWhereSql = '';
        $groupByHash = array();
        $leftJoinHash = array();
        $quantBals = QuantBal::select('quant_bals.*');

        if(strcmp($backdate, $today) == 0)
        {
            $quantBals->selectRaw('SUM(quant_bals.balance_unit_qty) AS balance_unit_qty');

            /*
            if(!empty($pickingUnitQtySql))
            {
                $quantBals->whereRaw(
                    'quant_bals.balance_unit_qty <> 0 
                    OR @pickingUnitQty <> 0');
            }
            else
            {
                $quantBals->whereRaw('quant_bals.balance_unit_qty <> 0');
            }
            */
        }
        else
        {
            $quantBals->selectRaw(
                'SUM(quant_bals.balance_unit_qty) - 
                (
                    SELECT IFNULL(SUM(quant_bal_txns.sign * quant_bal_txns.unit_qty), 0) 
                    FROM quant_bal_txns
                    WHERE quant_bal_txns.doc_date > "'.$backdate.'" 
                    AND quant_bal_txns.quant_bal_id = quant_bals.id
                ) AS balance_unit_qty'
            );
        }

        foreach($columns as $column)
        {
            if(strcmp($column, 'storage_bin_code') == 0)
            {
                $groupByHash['storage_bin_id'] = 'storage_bin_id';
                $leftJoinHash['storage_bins'] = 'storage_bins';
                $quantBals->selectRaw('storage_bins.code AS storage_bin_code');
            }
            elseif(strcmp($column, 'storage_bay_code') == 0)
            {
                $groupByHash['storage_bay_id'] = 'storage_bay_id';
                $leftJoinHash['storage_bays'] = 'storage_bays';
                $quantBals->selectRaw('storage_bays.code AS storage_bay_code');
            }
            elseif(strcmp($column, 'storage_row_code') == 0)
            {
                $groupByHash['storage_row_id'] = 'storage_row_id';
                $leftJoinHash['storage_rows'] = 'storage_rows';
                $quantBals->selectRaw('storage_rows.code AS storage_row_code');
            }
            elseif(strcmp($column, 'location_code') == 0)
            {
                $groupByHash['location_id'] = 'location_id';
                $leftJoinHash['locations'] = 'locations';
                $quantBals->selectRaw('locations.code AS location_code');
            }
            elseif(strcmp($column, 'item_code') == 0)
            {
                $groupByHash['item_id'] = 'item_id';
                $leftJoinHash['items'] = 'items';
                $quantBals->selectRaw('items.code AS item_code');
            }
            elseif(strcmp($column, 'item_desc_01') == 0)
            {
                $groupByHash['item_id'] = 'item_id';
                $leftJoinHash['items'] = 'items';
                $quantBals->selectRaw('items.desc_01 AS item_desc_01');
            }
            elseif(strcmp($column, 'item_desc_02') == 0)
            {
                $groupByHash['item_id'] = 'item_id';
                $leftJoinHash['items'] = 'items';
                $quantBals->selectRaw('items.desc_02 AS item_desc_02');
            }
            elseif(strcmp($column, 'batch_serial_no') == 0)
            {
                $groupByHash['item_batch_id'] = 'item_batch_id';
                $leftJoinHash['item_batches'] = 'item_batches';
                $quantBals->selectRaw('item_batches.batch_serial_no AS batch_serial_no');
            }
            elseif(strcmp($column, 'expiry_date') == 0)
            {
                $groupByHash['item_batch_id'] = 'item_batch_id';
                $leftJoinHash['item_batches'] = 'item_batches';
                $quantBals->selectRaw('item_batches.expiry_date AS expiry_date');
            }
            elseif(strcmp($column, 'receipt_date') == 0)
            {
                $groupByHash['item_batch_id'] = 'item_batch_id';
                $leftJoinHash['item_batches'] = 'item_batches';
                $quantBals->selectRaw('item_batches.receipt_date AS receipt_date');
            }
            elseif(strcmp($column, 'handling_unit') == 0)
            {
                $groupByHash['handling_unit_id'] = 'handling_unit_id';
                $quantBals->selectRaw('IF(quant_bals.handling_unit_id = 0, "", CONCAT("'.ResType::$MAP['HANDLING_UNIT'].'", LPAD(quant_bals.handling_unit_id, 11, "0"))) AS handling_unit');
            }
            elseif(strcmp($column, 'picking_unit_qty') == 0)
            {
                $sql 
                = '@pickingUnitQty := IFNULL((SELECT SUM(quant_bal_rsvd_txns.sign * quant_bal_rsvd_txns.unit_qty) AS unit_qty 
                FROM quant_bal_rsvd_txns
                WHERE quant_bal_rsvd_txns.quant_bal_id = quant_bals.id), 0) AS picking_unit_qty';
                $quantBals->selectRaw($sql);
            }            
        }

        foreach($criteria as $field => $value)
        {
            if(strcmp($field, 'item_ids') == 0)
            {
                $quantBals->whereIn('quant_bals.item_id', $value);
            }
            if(strcmp($field, 'item_group_01_ids') == 0)
            {
                $leftJoinHash['items'] = 'items';
                $quantBals->whereIn('items.item_group_01_id', $value);
            }
            if(strcmp($field, 'item_group_02_ids') == 0)
            {
                $leftJoinHash['items'] = 'items';
                $quantBals->whereIn('items.item_group_02_id', $value);
            }
            if(strcmp($field, 'item_group_03_ids') == 0)
            {
                $leftJoinHash['items'] = 'items';
                $quantBals->whereIn('items.item_group_03_id', $value);
            }
            if(strcmp($field, 'storage_bin_ids') == 0)
            {
                $quantBals->whereIn('quant_bals.storage_bin_id', $value);
            }
            if(strcmp($field, 'storage_row_ids') == 0)
            {
                $quantBals->whereIn('quant_bals.storage_row_id', $value);
            }
            if(strcmp($field, 'storage_bay_ids') == 0)
            {
                $quantBals->whereIn('quant_bals.storage_bay_id', $value);
            }
            if(strcmp($field, 'location_ids') == 0)
            {
                $quantBals->whereIn('quant_bals.location_id', $value);
            }
            if(strcmp($field, 'expiry_days') == 0)
            {
                if($value > 0)
                {
                    $leftJoinHash['item_batches'] = 'item_batches';
                    $quantBals->whereRaw('item_batches.expiry_date <= (DATE(NOW()) + INTERVAL +'.$value.' DAY)');
                }
            }
        }

        foreach($sorts as $field => $order)
        {
            if(strcmp($field, 'item_code') == 0)
            {
                $leftJoinHash['items'] = 'items';
                $quantBals->orderBy('items.code', $order);
            }            
            if(strcmp($field, 'storage_row_code') == 0)
            {
                $leftJoinHash['storage_rows'] = 'storage_rows';
                $quantBals->orderBy('storage_rows.code', $order);
            }
            if(strcmp($field, 'storage_bay_code') == 0)
            {
                $leftJoinHash['storage_bays'] = 'storage_bays';
                $quantBals->orderBy('storage_bays.code', $order);
            }
            if(strcmp($field, 'expiry_date') == 0)
            {
                $leftJoinHash['item_batches'] = 'item_batches';
                $quantBals->orderBy('item_batches.expiry_date', $order);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'storage_bins') == 0)
            {
                $quantBals->leftJoin('storage_bins', 'storage_bins.id', '=', 'quant_bals.storage_bin_id');
            }
            if(strcmp($field, 'storage_bays') == 0)
            {
                $quantBals->leftJoin('storage_bays', 'storage_bays.id', '=', 'quant_bals.storage_bay_id');
            }
            if(strcmp($field, 'storage_rows') == 0)
            {
                $quantBals->leftJoin('storage_rows', 'storage_rows.id', '=', 'quant_bals.storage_row_id');
            }
            if(strcmp($field, 'locations') == 0)
            {
                $quantBals->leftJoin('locations', 'locations.id', '=', 'quant_bals.location_id');
            }
            if(strcmp($field, 'items') == 0)
            {
                $quantBals->leftJoin('items', 'items.id', '=', 'quant_bals.item_id');
            }
            if(strcmp($field, 'item_batches') == 0)
            {
                $quantBals->leftJoin('item_batches', 'item_batches.id', '=', 'quant_bals.item_batch_id');
            }
        }

        $quantBals->groupBy('quant_bals.id');

        $dbResults = $quantBals->get();
        //process the group by here, can not do in sql because the quant_bal_txns subquery
        //must be group by quant_bal_id

        //process the item group by
        $groupBys = array();
        if(array_key_exists('handling_unit_id', $groupByHash))
        {
            $groupBys[] = 'handling_unit_id';
        }
        if(array_key_exists('item_batch_id', $groupByHash))
        {
            $groupBys[] = 'item_batch_id';
        }
        if(array_key_exists('item_id', $groupByHash))
        {
            $groupBys[] = 'item_id';
        }

        //process the storage bin group by
        if(array_key_exists('storage_bin_id', $groupByHash))
        {
            $groupBys[] = 'storage_bin_id';
        }
        if(array_key_exists('storage_bay_id', $groupByHash))
        {
            $groupBys[] = 'storage_bay_id';
        }
        if(array_key_exists('storage_row_id', $groupByHash))
        {
            $groupBys[] = 'storage_row_id';
        }
        if(array_key_exists('location_id', $groupByHash))
        {
            $groupBys[] = 'location_id';
        }

        $dbResults = $dbResults->groupBy($groupBys);
        $dataArray = self::summariseStockBalance($dbResults);
        $isShowZeroBalance = false;
        if(array_key_exists('is_show_zero_balance', $criteria))
        {
            $isShowZeroBalance = $criteria['is_show_zero_balance'];
        }
        if($isShowZeroBalance == false)
        {
            $dataArray = self::removeZeroStockBalance($dataArray);
        }
        //here convert the normal array to eloquent collection, so can use load relationship
        $results = Collection::make($dataArray);

        //LengthAwarePaginator is required for UI datatable
        return new LengthAwarePaginator($results, count($results), PHP_INT_MAX, 1);
    }

    static protected function summariseStockBalance($collection)
    {
        //recursive function to iterate the collection after group by
        //summarise the group balance_unit_qty
        $results = array();
        $quantBal = null;
        foreach($collection as $key => $value)
        {
            if($value instanceof \App\QuantBal)
            {
                if(empty($quantBal))
                {
                    $quantBal = $value;
                }
                else
                {
                    //sum up the balance_unit_qty
                    $quantBal->balance_unit_qty = bcadd(
                        $quantBal->balance_unit_qty,
                        $value->balance_unit_qty,
                        10
                    );
                    //sum up the picking_unit_qty
                    $quantBal->picking_unit_qty = bcadd(
                        $quantBal->picking_unit_qty,
                        $value->picking_unit_qty,
                        10
                    );
                }
            }
            else
            {                
                $tmpResults = self::summariseStockBalance($value);
                $results = array_merge($results, $tmpResults);
            }            
        }
        if(empty($quantBal))
        {
            return $results;
        }
        else
        {
            return array($quantBal);
        }        
    }

    static protected function removeZeroStockBalance($dataArray)
    {
        //recursive function to iterate the collection after group by
        //summarise the group balance_unit_qty
        $results = array();
        foreach($dataArray as $data)
        {
            if(bccomp($data->balance_unit_qty, 0, 10) == 0
            && bccomp($data->picking_unit_qty, 0, 10) == 0)
            {
                continue;
            }        
            
            $results[] = $data;
        }
        return $results;       
    }

    static public function cycleCountAnalysis($siteFlowId, $sorts, $criteria = array(), $columns = array(), $pageSize = 20) 
	{
        $groupByHash = array();
        $leftJoinHash = array();
        $cycleCountDtls = CycleCountDtl::select('cycle_count_dtls.*')
            ->selectRaw('cycle_count_hdrs.ref_code_01 AS ref_code_01')
            ->selectRaw('SUM(cycle_count_dtls.qty * cycle_count_dtls.uom_rate) AS balance_unit_qty')
            ->whereRaw('(cycle_count_dtls.item_id > 0)')
            ->whereRaw('cycle_count_hdrs.doc_status = 100');

        $cycleCountDtls->leftJoin('cycle_count_hdrs', 'cycle_count_hdrs.id', '=', 'cycle_count_dtls.hdr_id');
        foreach($columns as $column)
        {
            if(strcmp($column, 'doc_code') == 0)
            {
                $cycleCountDtls->selectRaw('cycle_count_hdrs.doc_code AS doc_code');
            }
            elseif(strcmp($column, 'doc_date') == 0)
            {
                $cycleCountDtls->selectRaw('cycle_count_hdrs.doc_date AS doc_date');
            }
            elseif(strcmp($column, 'storage_bin_code') == 0)
            {
                $groupByHash['storage_bin_id'] = 'storage_bin_id';
                $leftJoinHash['storage_bins'] = 'storage_bins';
                $cycleCountDtls->selectRaw('storage_bins.code AS storage_bin_code');
            }
            elseif(strcmp($column, 'storage_bay_code') == 0)
            {
                $groupByHash['storage_bay_id'] = 'storage_bay_id';
                $leftJoinHash['storage_bays'] = 'storage_bays';
                $cycleCountDtls->selectRaw('storage_bays.code AS storage_bay_code');
            }
            elseif(strcmp($column, 'storage_row_code') == 0)
            {
                $groupByHash['storage_row_id'] = 'storage_row_id';
                $leftJoinHash['storage_rows'] = 'storage_rows';
                $cycleCountDtls->selectRaw('storage_rows.code AS storage_row_code');
            }
            elseif(strcmp($column, 'location_code') == 0)
            {
                $groupByHash['location_id'] = 'location_id';
                $leftJoinHash['locations'] = 'locations';
                $cycleCountDtls->selectRaw('locations.code AS location_code');
            }
            elseif(strcmp($column, 'item_code') == 0)
            {
                $groupByHash['item_id'] = 'item_id';
                $leftJoinHash['items'] = 'items';
                $cycleCountDtls->selectRaw('items.code AS item_code');
            }
            elseif(strcmp($column, 'item_desc_01') == 0)
            {
                $groupByHash['item_id'] = 'item_id';
                $leftJoinHash['items'] = 'items';
                $cycleCountDtls->selectRaw('items.desc_01 AS item_desc_01');
            }
            elseif(strcmp($column, 'item_desc_02') == 0)
            {
                $groupByHash['item_id'] = 'item_id';
                $leftJoinHash['items'] = 'items';
                $cycleCountDtls->selectRaw('items.desc_02 AS item_desc_02');
            }
            elseif(strcmp($column, 'batch_serial_no') == 0)
            {
                $groupByHash['item_batch_id'] = 'item_batch_id';
                $leftJoinHash['item_batches'] = 'item_batches';
                $cycleCountDtls->selectRaw('item_batches.batch_serial_no AS batch_serial_no');
            }
            elseif(strcmp($column, 'expiry_date') == 0)
            {
                $groupByHash['item_batch_id'] = 'item_batch_id';
                $leftJoinHash['item_batches'] = 'item_batches';
                $cycleCountDtls->selectRaw('item_batches.expiry_date AS expiry_date');
            }
            elseif(strcmp($column, 'receipt_date') == 0)
            {
                $groupByHash['item_batch_id'] = 'item_batch_id';
                $leftJoinHash['item_batches'] = 'item_batches';
                $cycleCountDtls->selectRaw('item_batches.receipt_date AS receipt_date');
            }
            elseif(strcmp($column, 'handling_unit') == 0)
            {
                $groupByHash['handling_unit_id'] = 'handling_unit_id';
                $cycleCountDtls->selectRaw('IF(cycle_count_dtls.handling_unit_id = 0, "", CONCAT("'.ResType::$MAP['HANDLING_UNIT'].'", LPAD(cycle_count_dtls.handling_unit_id, 11, "0"))) AS handling_unit');
            }
        }

        foreach($criteria as $field => $value)
        {
            if(strcmp($field, 'start_date') == 0)
            {
                $cycleCountDtls->where('cycle_count_hdrs.doc_date', '>=', $value);
            }
            if(strcmp($field, 'end_date') == 0)
            {
                $cycleCountDtls->where('cycle_count_hdrs.doc_date', '<=', $value);
            }
            if(strcmp($field, 'item_ids') == 0)
            {
                $cycleCountDtls->whereIn('cycle_count_dtls.item_id', $value);
            }
            if(strcmp($field, 'item_group_01_ids') == 0)
            {
                $leftJoinHash['items'] = 'items';
                $cycleCountDtls->whereIn('items.item_group_01_id', $value);
            }
            if(strcmp($field, 'item_group_02_ids') == 0)
            {
                $leftJoinHash['items'] = 'items';
                $cycleCountDtls->whereIn('items.item_group_02_id', $value);
            }
            if(strcmp($field, 'item_group_03_ids') == 0)
            {
                $leftJoinHash['items'] = 'items';
                $cycleCountDtls->whereIn('items.item_group_03_id', $value);
            }
            if(strcmp($field, 'storage_bin_ids') == 0)
            {
                $cycleCountDtls->whereIn('cycle_count_dtls.storage_bin_id', $value);
            }
            if(strcmp($field, 'storage_row_ids') == 0)
            {
                $leftJoinHash['storage_bins'] = 'storage_bins';
                $cycleCountDtls->whereIn('storage_bins.storage_row_id', $value);
            }
            if(strcmp($field, 'storage_bay_ids') == 0)
            {
                $leftJoinHash['storage_bins'] = 'storage_bins';
                $cycleCountDtls->whereIn('storage_bins.storage_bay_id', $value);
            }
            if(strcmp($field, 'location_ids') == 0)
            {
                $leftJoinHash['storage_bins'] = 'storage_bins';
                $cycleCountDtls->whereIn('storage_bins.location_id', $value);
            }
        }

        foreach($sorts as $field => $order)
        {
            if(strcmp($field, 'doc_code') == 0)
            {
                $cycleCountDtls->orderBy('cycle_count_hdrs.doc_code', $order);
            }
            if(strcmp($field, 'ref_code_01') == 0)
            {
                $cycleCountDtls->orderBy('cycle_count_hdrs.ref_code_01', $order);
            }
            if(strcmp($field, 'doc_date') == 0)
            {
                $cycleCountDtls->orderBy('cycle_count_hdrs.doc_date', $order);
            }
            if(strcmp($field, 'item_code') == 0)
            {
                $leftJoinHash['items'] = 'items';
                $cycleCountDtls->orderBy('items.code', $order);
            }            
            if(strcmp($field, 'storage_row_code') == 0)
            {
                $leftJoinHash['storage_rows'] = 'storage_rows';
                $cycleCountDtls->orderBy('storage_rows.code', $order);
            }
            if(strcmp($field, 'storage_bay_code') == 0)
            {
                $leftJoinHash['storage_bays'] = 'storage_bays';
                $cycleCountDtls->orderBy('storage_bays.code', $order);
            }
            if(strcmp($field, 'expiry_date') == 0)
            {
                $leftJoinHash['item_batches'] = 'item_batches';
                $cycleCountDtls->orderBy('item_batches.expiry_date', $order);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'storage_bins') == 0)
            {
                $cycleCountDtls->leftJoin('storage_bins', 'storage_bins.id', '=', 'cycle_count_dtls.storage_bin_id');
            }
            if(strcmp($field, 'storage_bays') == 0)
            {
                $cycleCountDtls->leftJoin('storage_bays', 'storage_bays.id', '=', 'storage_bins.storage_bay_id');
            }
            if(strcmp($field, 'storage_rows') == 0)
            {
                $cycleCountDtls->leftJoin('storage_rows', 'storage_rows.id', '=', 'storage_bins.storage_row_id');
            }
            if(strcmp($field, 'locations') == 0)
            {
                $cycleCountDtls->leftJoin('locations', 'locations.id', '=', 'storage_bins.location_id');
            }
            if(strcmp($field, 'items') == 0)
            {
                $cycleCountDtls->leftJoin('items', 'items.id', '=', 'cycle_count_dtls.item_id');
            }
            if(strcmp($field, 'item_batches') == 0)
            {
                $cycleCountDtls->leftJoin('item_batches', 'item_batches.id', '=', 'cycle_count_dtls.item_batch_id');
            }
        }

        //process the item group by
        if(array_key_exists('handling_unit_id', $groupByHash))
        {
            $cycleCountDtls->groupBy('cycle_count_dtls.handling_unit_id');
        }
        if(array_key_exists('item_batch_id', $groupByHash))
        {
            $cycleCountDtls->groupBy('cycle_count_dtls.item_batch_id');
        }
        if(array_key_exists('item_id', $groupByHash))
        {
            $cycleCountDtls->groupBy('cycle_count_dtls.item_id');
        }

        //process the storage bin group by
        if(array_key_exists('storage_bin_id', $groupByHash))
        {
            $cycleCountDtls->groupBy('cycle_count_dtls.storage_bin_id');
        }
        if(array_key_exists('storage_bay_id', $groupByHash))
        {
            $cycleCountDtls->groupBy('cycle_count_dtls.storage_bay_id');
        }
        if(array_key_exists('storage_row_id', $groupByHash))
        {
            $cycleCountDtls->groupBy('cycle_count_dtls.storage_row_id');
        }
        if(array_key_exists('location_id', $groupByHash))
        {
            $cycleCountDtls->groupBy('cycle_count_dtls.location_id');
        }

        if($pageSize > 0)
        {
            return $cycleCountDtls
            ->paginate($pageSize);
        }
        else
        {
            return $cycleCountDtls
            ->paginate(PHP_INT_MAX);
        }
    }

    static public function stockCard($siteFlowId, $sorts, $criteria = array(), $columns = array(), $pageSize = 20) 
	{
        //query the quant_bals first
        $groupByHash = array();
        $leftJoinHash = array();
        $quantBalQuery = QuantBal::select('quant_bals.*')
            ->selectRaw('SUM(quant_bals.balance_unit_qty) AS balance_unit_qty')
            ->selectRaw('GROUP_CONCAT(quant_bals.id) AS quant_bal_ids');

        foreach($columns as $column)
        {
            if(strcmp($column, 'storage_bin_code') == 0)
            {
                $groupByHash['storage_bin_id'] = 'storage_bin_id';
                $leftJoinHash['storage_bins'] = 'storage_bins';
                $quantBalQuery->selectRaw('storage_bins.code AS storage_bin_code');
            }
            elseif(strcmp($column, 'storage_bay_code') == 0)
            {
                $groupByHash['storage_bay_id'] = 'storage_bay_id';
                $leftJoinHash['storage_bays'] = 'storage_bays';
                $quantBalQuery->selectRaw('storage_bays.code AS storage_bay_code');
            }
            elseif(strcmp($column, 'storage_row_code') == 0)
            {
                $groupByHash['storage_row_id'] = 'storage_row_id';
                $leftJoinHash['storage_rows'] = 'storage_rows';
                $quantBalQuery->selectRaw('storage_rows.code AS storage_row_code');
            }
            elseif(strcmp($column, 'location_code') == 0)
            {
                $groupByHash['location_id'] = 'location_id';
                $leftJoinHash['locations'] = 'locations';
                $quantBalQuery->selectRaw('locations.code AS location_code');
            }
            elseif(strcmp($column, 'item_code') == 0)
            {
                $groupByHash['item_id'] = 'item_id';
                $leftJoinHash['items'] = 'items';
                $quantBalQuery->selectRaw('items.code AS item_code');
            }
            elseif(strcmp($column, 'item_desc_01') == 0)
            {
                $groupByHash['item_id'] = 'item_id';
                $leftJoinHash['items'] = 'items';
                $quantBalQuery->selectRaw('items.desc_01 AS item_desc_01');
            }
            elseif(strcmp($column, 'item_desc_02') == 0)
            {
                $groupByHash['item_id'] = 'item_id';
                $leftJoinHash['items'] = 'items';
                $quantBalQuery->selectRaw('items.desc_02 AS item_desc_02');
            }
            elseif(strcmp($column, 'batch_serial_no') == 0)
            {
                $groupByHash['item_batch_id'] = 'item_batch_id';
                $leftJoinHash['item_batches'] = 'item_batches';
                $quantBalQuery->selectRaw('item_batches.batch_serial_no AS batch_serial_no');
            }
            elseif(strcmp($column, 'expiry_date') == 0)
            {
                $groupByHash['item_batch_id'] = 'item_batch_id';
                $leftJoinHash['item_batches'] = 'item_batches';
                $quantBalQuery->selectRaw('item_batches.expiry_date AS expiry_date');
            }
            elseif(strcmp($column, 'receipt_date') == 0)
            {
                $groupByHash['item_batch_id'] = 'item_batch_id';
                $leftJoinHash['item_batches'] = 'item_batches';
                $quantBalQuery->selectRaw('item_batches.receipt_date AS receipt_date');
            }
            elseif(strcmp($column, 'handling_unit') == 0)
            {
                $groupByHash['handling_unit_id'] = 'handling_unit_id';
                $quantBalQuery->selectRaw('IF(quant_bals.handling_unit_id = 0, "", CONCAT("'.ResType::$MAP['HANDLING_UNIT'].'", LPAD(quant_bals.handling_unit_id, 11, "0"))) AS handling_unit');
            }
            elseif(strcmp($column, 'picking_unit_qty') == 0)
            {
                $sql = 'IFNULL((SELECT SUM(quant_bal_rsvd_txns.sign * quant_bal_rsvd_txns.unit_qty) AS unit_qty 
                FROM quant_bal_rsvd_txns
                WHERE quant_bal_rsvd_txns.quant_bal_id = quant_bals.id), 0) AS picking_unit_qty';
                $quantBalQuery->selectRaw($sql);
            }            
        }
        
        foreach($criteria as $field => $value)
        {
            if(strcmp($field, 'item_ids') == 0)
            {
                $quantBalQuery->whereIn('quant_bals.item_id', $value);
            }
            if(strcmp($field, 'item_group_01_ids') == 0)
            {
                $leftJoinHash['items'] = 'items';
                $quantBalQuery->whereIn('items.item_group_01_id', $value);
            }
            if(strcmp($field, 'item_group_02_ids') == 0)
            {
                $leftJoinHash['items'] = 'items';
                $quantBalQuery->whereIn('items.item_group_02_id', $value);
            }
            if(strcmp($field, 'item_group_03_ids') == 0)
            {
                $leftJoinHash['items'] = 'items';
                $quantBalQuery->whereIn('items.item_group_03_id', $value);
            }
            if(strcmp($field, 'storage_bin_ids') == 0)
            {
                $quantBalQuery->whereIn('quant_bals.storage_bin_id', $value);
            }
            if(strcmp($field, 'storage_row_ids') == 0)
            {
                $quantBalQuery->whereIn('quant_bals.storage_row_id', $value);
            }
            if(strcmp($field, 'storage_bay_ids') == 0)
            {
                $quantBalQuery->whereIn('quant_bals.storage_bay_id', $value);
            }
            if(strcmp($field, 'location_ids') == 0)
            {
                $quantBalQuery->whereIn('quant_bals.location_id', $value);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'storage_bins') == 0)
            {
                $quantBalQuery->leftJoin('storage_bins', 'storage_bins.id', '=', 'quant_bals.storage_bin_id');
            }
            if(strcmp($field, 'storage_bays') == 0)
            {
                $quantBalQuery->leftJoin('storage_bays', 'storage_bays.id', '=', 'quant_bals.storage_bay_id');
            }
            if(strcmp($field, 'storage_rows') == 0)
            {
                $quantBalQuery->leftJoin('storage_rows', 'storage_rows.id', '=', 'quant_bals.storage_row_id');
            }
            if(strcmp($field, 'locations') == 0)
            {
                $quantBalQuery->leftJoin('locations', 'locations.id', '=', 'quant_bals.location_id');
            }
            if(strcmp($field, 'items') == 0)
            {
                $quantBalQuery->leftJoin('items', 'items.id', '=', 'quant_bals.item_id');
            }
            if(strcmp($field, 'item_batches') == 0)
            {
                $quantBalQuery->leftJoin('item_batches', 'item_batches.id', '=', 'quant_bals.item_batch_id');
            }
        }

        //process the item group by
        if(array_key_exists('handling_unit_id', $groupByHash))
        {
            $quantBalQuery->groupBy('quant_bals.handling_unit_id');
        }
        if(array_key_exists('item_batch_id', $groupByHash))
        {
            $quantBalQuery->groupBy('quant_bals.item_batch_id');
        }
        if(array_key_exists('item_id', $groupByHash))
        {
            $quantBalQuery->groupBy('quant_bals.item_id');
        }

        //process the storage bin group by
        if(array_key_exists('storage_bin_id', $groupByHash))
        {
            $quantBalQuery->groupBy('quant_bals.storage_bin_id');
        }
        if(array_key_exists('storage_bay_id', $groupByHash))
        {
            $quantBalQuery->groupBy('quant_bals.storage_bay_id');
        }
        if(array_key_exists('storage_row_id', $groupByHash))
        {
            $quantBalQuery->groupBy('quant_bals.storage_row_id');
        }
        if(array_key_exists('location_id', $groupByHash))
        {
            $quantBalQuery->groupBy('quant_bals.location_id');
        }

        $allQuantBalTxnQuery = null;

        $quantBals = $quantBalQuery->get();

        foreach($quantBals as $quantBal)
        {
            $strIds = $quantBal->quant_bal_ids;
            $ids = explode(',',$strIds);

            $quantBalTxnQuery = QuantBalTxn::select('quant_bal_txns.*')
            ->selectRaw($quantBal->balance_unit_qty.' AS cur_balance_unit_qty')
            ->selectRaw(min($ids).' AS hash_id')
            ->whereIn('quant_bal_txns.quant_bal_id', $ids)
            ->leftJoin('quant_bals', 'quant_bals.id', '=', 'quant_bal_txns.quant_bal_id');

            foreach($columns as $column)
            {
                if(strcmp($column, 'storage_bin_code') == 0)
                {
                    $leftJoinHash['storage_bins'] = 'storage_bins';
                    $quantBalTxnQuery->selectRaw('storage_bins.code AS storage_bin_code');
                }
                elseif(strcmp($column, 'storage_bay_code') == 0)
                {
                    $leftJoinHash['storage_bays'] = 'storage_bays';
                    $quantBalTxnQuery->selectRaw('storage_bays.code AS storage_bay_code');
                }
                elseif(strcmp($column, 'storage_row_code') == 0)
                {
                    $leftJoinHash['storage_rows'] = 'storage_rows';
                    $quantBalTxnQuery->selectRaw('storage_rows.code AS storage_row_code');
                }
                elseif(strcmp($column, 'location_code') == 0)
                {
                    $leftJoinHash['locations'] = 'locations';
                    $quantBalTxnQuery->selectRaw('locations.code AS location_code');
                }
                elseif(strcmp($column, 'item_code') == 0)
                {
                    $leftJoinHash['items'] = 'items';
                    $quantBalTxnQuery->selectRaw('items.code AS item_code');
                }
                elseif(strcmp($column, 'item_desc_01') == 0)
                {
                    $leftJoinHash['items'] = 'items';
                    $quantBalTxnQuery->selectRaw('items.desc_01 AS item_desc_01');
                }
                elseif(strcmp($column, 'item_desc_02') == 0)
                {
                    $leftJoinHash['items'] = 'items';
                    $quantBalTxnQuery->selectRaw('items.desc_02 AS item_desc_02');
                }
                elseif(strcmp($column, 'batch_serial_no') == 0)
                {
                    $leftJoinHash['item_batches'] = 'item_batches';
                    $quantBalTxnQuery->selectRaw('item_batches.batch_serial_no AS batch_serial_no');
                }
                elseif(strcmp($column, 'expiry_date') == 0)
                {
                    $leftJoinHash['item_batches'] = 'item_batches';
                    $quantBalTxnQuery->selectRaw('item_batches.expiry_date AS expiry_date');
                }
                elseif(strcmp($column, 'receipt_date') == 0)
                {
                    $leftJoinHash['item_batches'] = 'item_batches';
                    $quantBalTxnQuery->selectRaw('item_batches.receipt_date AS receipt_date');
                }
                elseif(strcmp($column, 'handling_unit') == 0)
                {
                    $groupByHash['handling_unit_id'] = 'handling_unit_id';
                    $quantBalTxnQuery->selectRaw('IF(quant_bal_txns.handling_unit_id = 0, "", CONCAT("'.ResType::$MAP['HANDLING_UNIT'].'", LPAD(quant_bal_txns.handling_unit_id, 11, "0"))) AS handling_unit');
                }          
            }

            foreach($criteria as $field => $value)
            {
                if(strcmp($field, 'start_date') == 0)
                {
                    $startDate = $value;
                    $quantBalTxnQuery->where('quant_bal_txns.doc_date', '>=', $value);
                }
            }

            foreach($leftJoinHash as $field)
            {
                if(strcmp($field, 'storage_bins') == 0)
                {
                    $quantBalTxnQuery->leftJoin('storage_bins', 'storage_bins.id', '=', 'quant_bal_txns.storage_bin_id');
                }
                if(strcmp($field, 'storage_bays') == 0)
                {
                    $quantBalTxnQuery->leftJoin('storage_bays', 'storage_bays.id', '=', 'quant_bals.storage_bay_id');
                }
                if(strcmp($field, 'storage_rows') == 0)
                {
                    $quantBalTxnQuery->leftJoin('storage_rows', 'storage_rows.id', '=', 'quant_bals.storage_row_id');
                }
                if(strcmp($field, 'locations') == 0)
                {
                    $quantBalTxnQuery->leftJoin('locations', 'locations.id', '=', 'quant_bals.location_id');
                }
                if(strcmp($field, 'items') == 0)
                {
                    $quantBalTxnQuery->leftJoin('items', 'items.id', '=', 'quant_bal_txns.item_id');
                }
                if(strcmp($field, 'item_batches') == 0)
                {
                    $quantBalTxnQuery->leftJoin('item_batches', 'item_batches.id', '=', 'quant_bal_txns.item_batch_id');
                }
            }

            if(empty($allQuantBalTxnQuery))
            {
                $allQuantBalTxnQuery = $quantBalTxnQuery;
            }
            else
            {
                $allQuantBalTxnQuery->union($quantBalTxnQuery);
            }            
        }
        if(empty($allQuantBalTxnQuery))
        {
            $allQuantBalTxnQuery = QuantBalTxn::select('quant_bal_txns.*')
            ->where('quant_bal_txns.id', '=', -1);
        }
        else
        {
            $allQuantBalTxnQuery
            ->orderBy('doc_date', 'ASC')
            ->orderBy('posted_at', 'ASC');
        }

        if($pageSize > 0)
        {
            return $allQuantBalTxnQuery
            ->paginate($pageSize);
        }
        else
        {
            return $allQuantBalTxnQuery
            ->paginate(PHP_INT_MAX);
        }
    }

    static public function countAdjAnalysis($siteFlowId, $sorts, $criteria = array(), $columns = array(), $pageSize = 20) 
	{
        $groupByHash = array();
        $leftJoinHash = array();
        $countAdjDtls = CountAdjDtl::select('count_adj_dtls.*')
            ->selectRaw('count_adj_hdrs.ref_code_01 AS ref_code_01')
            ->selectRaw('SUM(count_adj_dtls.sign * count_adj_dtls.qty * count_adj_dtls.uom_rate) AS balance_unit_qty')
            ->whereRaw('(count_adj_dtls.item_id > 0)')
            ->whereRaw('count_adj_hdrs.doc_status = 100');

        $countAdjDtls->leftJoin('count_adj_hdrs', 'count_adj_hdrs.id', '=', 'count_adj_dtls.hdr_id');
        foreach($columns as $column)
        {
            if(strcmp($column, 'doc_code') == 0)
            {
                $countAdjDtls->selectRaw('count_adj_hdrs.doc_code AS doc_code');
            }
            elseif(strcmp($column, 'doc_date') == 0)
            {
                $countAdjDtls->selectRaw('count_adj_hdrs.doc_date AS doc_date');
            }
            elseif(strcmp($column, 'storage_bin_code') == 0)
            {
                $groupByHash['storage_bin_id'] = 'storage_bin_id';
                $leftJoinHash['storage_bins'] = 'storage_bins';
                $countAdjDtls->selectRaw('storage_bins.code AS storage_bin_code');
            }
            elseif(strcmp($column, 'storage_bay_code') == 0)
            {
                $groupByHash['storage_bay_id'] = 'storage_bay_id';
                $leftJoinHash['storage_bays'] = 'storage_bays';
                $countAdjDtls->selectRaw('storage_bays.code AS storage_bay_code');
            }
            elseif(strcmp($column, 'storage_row_code') == 0)
            {
                $groupByHash['storage_row_id'] = 'storage_row_id';
                $leftJoinHash['storage_rows'] = 'storage_rows';
                $countAdjDtls->selectRaw('storage_rows.code AS storage_row_code');
            }
            elseif(strcmp($column, 'location_code') == 0)
            {
                $groupByHash['location_id'] = 'location_id';
                $leftJoinHash['locations'] = 'locations';
                $countAdjDtls->selectRaw('locations.code AS location_code');
            }
            elseif(strcmp($column, 'item_code') == 0)
            {
                $groupByHash['item_id'] = 'item_id';
                $leftJoinHash['items'] = 'items';
                $countAdjDtls->selectRaw('items.code AS item_code');
            }
            elseif(strcmp($column, 'item_desc_01') == 0)
            {
                $groupByHash['item_id'] = 'item_id';
                $leftJoinHash['items'] = 'items';
                $countAdjDtls->selectRaw('items.desc_01 AS item_desc_01');
            }
            elseif(strcmp($column, 'item_desc_02') == 0)
            {
                $groupByHash['item_id'] = 'item_id';
                $leftJoinHash['items'] = 'items';
                $countAdjDtls->selectRaw('items.desc_02 AS item_desc_02');
            }
            elseif(strcmp($column, 'batch_serial_no') == 0)
            {
                $groupByHash['item_batch_id'] = 'item_batch_id';
                $leftJoinHash['item_batches'] = 'item_batches';
                $countAdjDtls->selectRaw('item_batches.batch_serial_no AS batch_serial_no');
            }
            elseif(strcmp($column, 'expiry_date') == 0)
            {
                $groupByHash['item_batch_id'] = 'item_batch_id';
                $leftJoinHash['item_batches'] = 'item_batches';
                $countAdjDtls->selectRaw('item_batches.expiry_date AS expiry_date');
            }
            elseif(strcmp($column, 'receipt_date') == 0)
            {
                $groupByHash['item_batch_id'] = 'item_batch_id';
                $leftJoinHash['item_batches'] = 'item_batches';
                $countAdjDtls->selectRaw('item_batches.receipt_date AS receipt_date');
            }
            elseif(strcmp($column, 'handling_unit') == 0)
            {
                $groupByHash['handling_unit_id'] = 'handling_unit_id';
                $countAdjDtls->selectRaw('IF(count_adj_dtls.handling_unit_id = 0, "", CONCAT("'.ResType::$MAP['HANDLING_UNIT'].'", LPAD(count_adj_dtls.handling_unit_id, 11, "0"))) AS handling_unit');
            }
        }

        foreach($criteria as $field => $value)
        {
            if(strcmp($field, 'start_date') == 0)
            {
                $countAdjDtls->where('count_adj_hdrs.doc_date', '>=', $value);
            }
            if(strcmp($field, 'end_date') == 0)
            {
                $countAdjDtls->where('count_adj_hdrs.doc_date', '<=', $value);
            }
            if(strcmp($field, 'item_ids') == 0)
            {
                $countAdjDtls->whereIn('count_adj_dtls.item_id', $value);
            }
            if(strcmp($field, 'item_group_01_ids') == 0)
            {
                $leftJoinHash['items'] = 'items';
                $countAdjDtls->whereIn('items.item_group_01_id', $value);
            }
            if(strcmp($field, 'item_group_02_ids') == 0)
            {
                $leftJoinHash['items'] = 'items';
                $countAdjDtls->whereIn('items.item_group_02_id', $value);
            }
            if(strcmp($field, 'item_group_03_ids') == 0)
            {
                $leftJoinHash['items'] = 'items';
                $countAdjDtls->whereIn('items.item_group_03_id', $value);
            }
            if(strcmp($field, 'storage_bin_ids') == 0)
            {
                $countAdjDtls->whereIn('count_adj_dtls.storage_bin_id', $value);
            }
            if(strcmp($field, 'storage_row_ids') == 0)
            {
                $leftJoinHash['storage_bins'] = 'storage_bins';
                $countAdjDtls->whereIn('storage_bins.storage_row_id', $value);
            }
            if(strcmp($field, 'storage_bay_ids') == 0)
            {
                $leftJoinHash['storage_bins'] = 'storage_bins';
                $countAdjDtls->whereIn('storage_bins.storage_bay_id', $value);
            }
            if(strcmp($field, 'location_ids') == 0)
            {
                $leftJoinHash['storage_bins'] = 'storage_bins';
                $countAdjDtls->whereIn('storage_bins.location_id', $value);
            }
        }

        foreach($sorts as $field => $order)
        {
            if(strcmp($field, 'doc_code') == 0)
            {
                $countAdjDtls->orderBy('count_adj_hdrs.doc_code', $order);
            }
            if(strcmp($field, 'ref_code_01') == 0)
            {
                $countAdjDtls->orderBy('count_adj_hdrs.ref_code_01', $order);
            }
            if(strcmp($field, 'doc_date') == 0)
            {
                $countAdjDtls->orderBy('count_adj_hdrs.doc_date', $order);
            }
            if(strcmp($field, 'item_code') == 0)
            {
                $leftJoinHash['items'] = 'items';
                $countAdjDtls->orderBy('items.code', $order);
            }            
            if(strcmp($field, 'storage_row_code') == 0)
            {
                $leftJoinHash['storage_rows'] = 'storage_rows';
                $countAdjDtls->orderBy('storage_rows.code', $order);
            }
            if(strcmp($field, 'storage_bay_code') == 0)
            {
                $leftJoinHash['storage_bays'] = 'storage_bays';
                $countAdjDtls->orderBy('storage_bays.code', $order);
            }
            if(strcmp($field, 'expiry_date') == 0)
            {
                $leftJoinHash['item_batches'] = 'item_batches';
                $countAdjDtls->orderBy('item_batches.expiry_date', $order);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'storage_bins') == 0)
            {
                $countAdjDtls->leftJoin('storage_bins', 'storage_bins.id', '=', 'count_adj_dtls.storage_bin_id');
            }
            if(strcmp($field, 'storage_bays') == 0)
            {
                $countAdjDtls->leftJoin('storage_bays', 'storage_bays.id', '=', 'storage_bins.storage_bay_id');
            }
            if(strcmp($field, 'storage_rows') == 0)
            {
                $countAdjDtls->leftJoin('storage_rows', 'storage_rows.id', '=', 'storage_bins.storage_row_id');
            }
            if(strcmp($field, 'locations') == 0)
            {
                $countAdjDtls->leftJoin('locations', 'locations.id', '=', 'storage_bins.location_id');
            }
            if(strcmp($field, 'items') == 0)
            {
                $countAdjDtls->leftJoin('items', 'items.id', '=', 'count_adj_dtls.item_id');
            }
            if(strcmp($field, 'item_batches') == 0)
            {
                $countAdjDtls->leftJoin('item_batches', 'item_batches.id', '=', 'count_adj_dtls.item_batch_id');
            }
        }

        //process the item group by
        if(array_key_exists('handling_unit_id', $groupByHash))
        {
            $countAdjDtls->groupBy('count_adj_dtls.handling_unit_id');
        }
        if(array_key_exists('item_batch_id', $groupByHash))
        {
            $countAdjDtls->groupBy('count_adj_dtls.item_batch_id');
        }
        if(array_key_exists('item_id', $groupByHash))
        {
            $countAdjDtls->groupBy('count_adj_dtls.item_id');
        }

        //process the storage bin group by
        if(array_key_exists('storage_bin_id', $groupByHash))
        {
            $countAdjDtls->groupBy('count_adj_dtls.storage_bin_id');
        }
        if(array_key_exists('storage_bay_id', $groupByHash))
        {
            $countAdjDtls->groupBy('count_adj_dtls.storage_bay_id');
        }
        if(array_key_exists('storage_row_id', $groupByHash))
        {
            $countAdjDtls->groupBy('count_adj_dtls.storage_row_id');
        }
        if(array_key_exists('location_id', $groupByHash))
        {
            $countAdjDtls->groupBy('count_adj_dtls.location_id');
        }

        if($pageSize > 0)
        {
            return $countAdjDtls
            ->paginate($pageSize);
        }
        else
        {
            return $countAdjDtls
            ->paginate(PHP_INT_MAX);
        }
    }

    static public function reservedStock($siteFlowId, $sorts, $criteria = array(), $columns = array(), $pageSize = 20) 
	{
        $groupByHash = array();
        $leftJoinHash = array();
        $quantBalRsvTxns = QuantBalRsvdTxn::select('quant_bal_rsvd_txns.*');

        $quantBalRsvTxns->selectRaw('SUM(quant_bal_rsvd_txns.sign * quant_bal_rsvd_txns.unit_qty) AS balance_unit_qty');
        $quantBalRsvTxns->leftJoin('quant_bals', 'quant_bals.id', '=', 'quant_bal_rsvd_txns.quant_bal_id');

        foreach($columns as $column)
        {
            if(strcmp($column, 'storage_bin_code') == 0)
            {
                $groupByHash['storage_bin_id'] = 'storage_bin_id';
                $leftJoinHash['storage_bins'] = 'storage_bins';
                $quantBalRsvTxns->selectRaw('storage_bins.code AS storage_bin_code');
            }
            elseif(strcmp($column, 'storage_bay_code') == 0)
            {
                $groupByHash['storage_bay_id'] = 'storage_bay_id';
                $leftJoinHash['storage_bays'] = 'storage_bays';
                $quantBalRsvTxns->selectRaw('storage_bays.code AS storage_bay_code');
            }
            elseif(strcmp($column, 'storage_row_code') == 0)
            {
                $groupByHash['storage_row_id'] = 'storage_row_id';
                $leftJoinHash['storage_rows'] = 'storage_rows';
                $quantBalRsvTxns->selectRaw('storage_rows.code AS storage_row_code');
            }
            elseif(strcmp($column, 'location_code') == 0)
            {
                $groupByHash['location_id'] = 'location_id';
                $leftJoinHash['locations'] = 'locations';
                $quantBalRsvTxns->selectRaw('locations.code AS location_code');
            }
            elseif(strcmp($column, 'item_code') == 0)
            {
                $groupByHash['item_id'] = 'item_id';
                $leftJoinHash['items'] = 'items';
                $quantBalRsvTxns->selectRaw('items.code AS item_code');
            }
            elseif(strcmp($column, 'item_desc_01') == 0)
            {
                $groupByHash['item_id'] = 'item_id';
                $leftJoinHash['items'] = 'items';
                $quantBalRsvTxns->selectRaw('items.desc_01 AS item_desc_01');
            }
            elseif(strcmp($column, 'item_desc_02') == 0)
            {
                $groupByHash['item_id'] = 'item_id';
                $leftJoinHash['items'] = 'items';
                $quantBalRsvTxns->selectRaw('items.desc_02 AS item_desc_02');
            }
            elseif(strcmp($column, 'batch_serial_no') == 0)
            {
                $groupByHash['item_batch_id'] = 'item_batch_id';
                $leftJoinHash['item_batches'] = 'item_batches';
                $quantBalRsvTxns->selectRaw('item_batches.batch_serial_no AS batch_serial_no');
            }
            elseif(strcmp($column, 'expiry_date') == 0)
            {
                $groupByHash['item_batch_id'] = 'item_batch_id';
                $leftJoinHash['item_batches'] = 'item_batches';
                $quantBalRsvTxns->selectRaw('item_batches.expiry_date AS expiry_date');
            }
            elseif(strcmp($column, 'receipt_date') == 0)
            {
                $groupByHash['item_batch_id'] = 'item_batch_id';
                $leftJoinHash['item_batches'] = 'item_batches';
                $quantBalRsvTxns->selectRaw('item_batches.receipt_date AS receipt_date');
            }
            elseif(strcmp($column, 'handling_unit') == 0)
            {
                $groupByHash['handling_unit_id'] = 'handling_unit_id';
                $quantBalRsvTxns->selectRaw('IF(quant_bal_rsvd_txns.handling_unit_id = 0, "", CONCAT("'.ResType::$MAP['HANDLING_UNIT'].'", LPAD(quant_bals.handling_unit_id, 11, "0"))) AS handling_unit');
            }        
        }

        foreach($criteria as $field => $value)
        {
            if(strcmp($field, 'item_ids') == 0)
            {
                $quantBalRsvTxns->whereIn('quant_bal_rsvd_txns.item_id', $value);
            }
            if(strcmp($field, 'item_group_01_ids') == 0)
            {
                $leftJoinHash['items'] = 'items';
                $quantBalRsvTxns->whereIn('items.item_group_01_id', $value);
            }
            if(strcmp($field, 'item_group_02_ids') == 0)
            {
                $leftJoinHash['items'] = 'items';
                $quantBalRsvTxns->whereIn('items.item_group_02_id', $value);
            }
            if(strcmp($field, 'item_group_03_ids') == 0)
            {
                $leftJoinHash['items'] = 'items';
                $quantBalRsvTxns->whereIn('items.item_group_03_id', $value);
            }
            if(strcmp($field, 'storage_bin_ids') == 0)
            {
                $quantBalRsvTxns->whereIn('quant_bal_rsvd_txns.storage_bin_id', $value);
            }
            if(strcmp($field, 'storage_row_ids') == 0)
            {
                $quantBalRsvTxns->whereIn('quant_bal_rsvd_txns.storage_row_id', $value);
            }
            if(strcmp($field, 'storage_bay_ids') == 0)
            {
                $quantBalRsvTxns->whereIn('quant_bal_rsvd_txns.storage_bay_id', $value);
            }
        }

        foreach($sorts as $field => $order)
        {
            if(strcmp($field, 'item_code') == 0)
            {
                $leftJoinHash['items'] = 'items';
                $quantBalRsvTxns->orderBy('items.code', $order);
            }            
            if(strcmp($field, 'storage_row_code') == 0)
            {
                $leftJoinHash['storage_rows'] = 'storage_rows';
                $quantBalRsvTxns->orderBy('storage_rows.code', $order);
            }
            if(strcmp($field, 'storage_bay_code') == 0)
            {
                $leftJoinHash['storage_bays'] = 'storage_bays';
                $quantBalRsvTxns->orderBy('storage_bays.code', $order);
            }
            if(strcmp($field, 'expiry_date') == 0)
            {
                $leftJoinHash['item_batches'] = 'item_batches';
                $quantBalRsvTxns->orderBy('item_batches.expiry_date', $order);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'storage_bins') == 0)
            {
                $quantBalRsvTxns->leftJoin('storage_bins', 'storage_bins.id', '=', 'quant_bal_rsvd_txns.storage_bin_id');
            }
            if(strcmp($field, 'storage_bays') == 0)
            {
                $quantBalRsvTxns->leftJoin('storage_bays', 'storage_bays.id', '=', 'quant_bal_rsvd_txns.storage_bay_id');
            }
            if(strcmp($field, 'storage_rows') == 0)
            {
                $quantBalRsvTxns->leftJoin('storage_rows', 'storage_rows.id', '=', 'quant_bal_rsvd_txns.storage_row_id');
            }
            if(strcmp($field, 'items') == 0)
            {
                $quantBalRsvTxns->leftJoin('items', 'items.id', '=', 'quant_bal_rsvd_txns.item_id');
            }
            if(strcmp($field, 'item_batches') == 0)
            {
                $quantBalRsvTxns->leftJoin('item_batches', 'item_batches.id', '=', 'quant_bal_rsvd_txns.item_batch_id');
            }
        }

        $quantBalRsvTxns->groupBy('quant_bal_rsvd_txns.id');

        /*
        //process the item group by
        if(array_key_exists('handling_unit_id', $groupByHash))
        {
            $quantBalRsvTxns->groupBy('quant_bal_rsvd_txns.handling_unit_id');
        }
        if(array_key_exists('item_batch_id', $groupByHash))
        {
            $quantBalRsvTxns->groupBy('quant_bal_rsvd_txns.item_batch_id');
        }
        if(array_key_exists('item_id', $groupByHash))
        {
            $quantBalRsvTxns->groupBy('quant_bal_rsvd_txns.item_id');
        }

        //process the storage bin group by
        if(array_key_exists('storage_bin_id', $groupByHash))
        {
            $quantBalRsvTxns->groupBy('quant_bal_rsvd_txns.storage_bin_id');
        }
        if(array_key_exists('storage_bay_id', $groupByHash))
        {
            $quantBalRsvTxns->groupBy('quant_bal_rsvd_txns.storage_bay_id');
        }
        if(array_key_exists('storage_row_id', $groupByHash))
        {
            $quantBalRsvTxns->groupBy('quant_bal_rsvd_txns.storage_row_id');
        }
        */

        if($pageSize > 0)
        {
            return $quantBalRsvTxns
            ->paginate($pageSize);
        }
        else
        {
            return $quantBalRsvTxns
            ->paginate(PHP_INT_MAX);
        }
    }
}