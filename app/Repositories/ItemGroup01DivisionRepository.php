<?php

namespace App\Repositories;

use App\ItemGroup01Division;
use App\Services\Utils\RepositoryUtils;
use Illuminate\Support\Facades\DB;

class ItemGroup01DivisionRepository {
    static public function findByPk($id) {
        return ItemGroup01Division::find($id);
    }

    static public function createModel($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = new ItemGroup01Division;
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return $model;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function updateModel($data) 
	{
        $result = DB::transaction
        (
            function() use ($data)
            {
                //update Item
                $model = ItemGroup01Division::lockForUpdate()->find($data['id']);
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return array(
                    'model' => $model
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function reorderBrand($curBrandId, $prevBrandId, $curDivisionId)
    {
        $data = DB::transaction
        (
            function() use ($curBrandId, $prevBrandId, $curDivisionId)
            {
                $data = ItemGroup01Division::where('sequence', '=', $prevBrandId)
                ->where('division_id', '=', $curDivisionId)->first();
                $data->sequence = 0;
                $data->save();

                if($prevBrandId > $curBrandId) {
                    for($i = $prevBrandId - 1; $i >= $curBrandId; $i--) {
                        $data = ItemGroup01Division::where('sequence', '=', $i)
                        ->where('division_id', '=', $curDivisionId)->first();
                        $data->sequence = $i + 1;
                        $data->save();
                    }
                } else {
                    for($i = $prevBrandId + 1; $i <= $curBrandId; $i++) {
                        $data = ItemGroup01Division::where('sequence', '=', $i)
                        ->where('division_id', '=', $curDivisionId)->first();
                        $data->sequence = $i - 1;
                        $data->save();
                    }
                }

                $data = ItemGroup01Division::where('sequence', '=', 0)
                ->where('division_id', '=', $curDivisionId)->first();
                $data->sequence = $curBrandId;
                $data->save();

                return $data;
            },
            5
        );

        return $data;
    }
}