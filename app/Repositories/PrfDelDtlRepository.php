<?php

namespace App\Repositories;

use App\PrfDelDtl;
use App\Services\Env\ProcType;
use Illuminate\Support\Facades\DB;

class PrfDelDtlRepository 
{
    static public function findAllByHdrId($hdrId) 
	{
        $prfDelDtls = PrfDelDtl::where('hdr_id', $hdrId)
            ->orderBy('line_no')
            ->get();

        return $prfDelDtls;
    }
}