<?php

namespace App\Repositories;

use App\MobileHistory;
use App\SMobileSchema;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\DB;

class MobileHistoryRepository 
{
    static public function findAllByMobileProfileIdAndTableName($mobileProfileId, $tableName) 
	{
        $mobileHistories = MobileHistory::where('mobile_profile_id', $mobileProfileId)
            ->where('table_name', $tableName)
            ->get();

        return $mobileHistories;
    }

    static public function updateHistory($mobileProfileId, $tableName, $dtlArray) 
	{
        $affectedRows = DB::transaction
        (
            function() use ($mobileProfileId, $tableName, $dtlArray)
            {
                foreach($dtlArray as $data)
                {
                    $recordId = $data['record_id'];
                    $operation = $data['operation'];
                    $mobileHistory = MobileHistory::where('mobile_profile_id', $mobileProfileId)
                    ->where('table_name', $tableName)
                    ->where('record_id', $recordId)
                    ->lockForUpdate()
                    ->first();
                    if(strcmp($operation, 'INSERT') == 0)
                    {
                        if(!empty($mobileHistory))
                        {
                            //this history already in table, need to throw exception
                            $exc = new ApiException(__('Mobile.mobile_history_duplicate', ['mobileProfileId'=>$mobileProfileId, 'tableName'=>$tableName, 'recordId'=>$recordId]));
                            $exc->addData(\App\MobileHistory::class, $recordId);
                            throw $exc;
                        }

                        $mobileHistory = new MobileHistory;
                        $mobileHistory->mobile_profile_id = $mobileProfileId;
                        $mobileHistory->table_name = $tableName;
                        $mobileHistory->record_id = $recordId;
                        $mobileHistory->save();
                    }
                    elseif(strcmp($operation, 'UPDATE') == 0)
                    {
                        if(empty($mobileHistory))
                        {
                            //this history already in table, need to throw exception
                            $exc = new ApiException(__('Mobile.mobile_history_not_found', ['mobileProfileId'=>$mobileProfileId, 'tableName'=>$tableName, 'recordId'=>$recordId]));
                            $exc->addData(\App\MobileHistory::class, $recordId);
                            throw $exc;
                        }
                        //update the updated_on time
                        $mobileHistory->touch();
                    }
                    elseif(strcmp($operation, 'DELETE') == 0)
                    {
                        $mobileHistory->delete();
                    }
                    else
                    {
                        //this history already in table, need to throw exception
                        $exc = new ApiException(__('Mobile.operation_invalid', ['mobileProfileId'=>$mobileProfileId, 'tableName'=>$tableName, 'recordId'=>$recordId, 'operation'=>$operation]));
                        $exc->addData(\App\MobileHistory::class, $recordId);
                        throw $exc;
                    }
                }

                return count($dtlArray);
            }, 
            5 //reattempt times
        );
        return $affectedRows;
    }

    static public function resetHistory($mobileProfileId, $tableName) 
	{
        $affectedRows = DB::transaction
        (
            function() use ($mobileProfileId, $tableName)
            {
                $affectedRows = MobileHistory::where('mobile_profile_id', $mobileProfileId)
                    ->where('table_name', $tableName)
                    ->delete();

                return $affectedRows;
            }, 
            5 //reattempt times
        );
        return $affectedRows;
    }

    static public function resetAllHistory($mobileProfileId) 
	{
        $affectedRows = DB::transaction
        (
            function() use ($mobileProfileId)
            {
                $affectedRows = MobileHistory::where('mobile_profile_id', $mobileProfileId)
                    ->delete();

                return $affectedRows;
            }, 
            5 //reattempt times
        );
        return $affectedRows;
    }
}