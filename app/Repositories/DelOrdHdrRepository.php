<?php

namespace App\Repositories;

use App\DelOrdHdr;
use App\DelOrdDtl;
use App\DocTxnFlow;
use App\Services\Env\DocStatus;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\DB;

class DelOrdHdrRepository 
{
    static public function findByPk($hdrId) 
	{
        $delOrdHdr = DelOrdHdr::where('id', $hdrId)
            ->first();

        return $delOrdHdr;
    }

    static public function txnFindByPk($hdrId) 
	{
        $model = DB::transaction
        (
            function() use ($hdrId)
            {
                $delOrdHdr = DelOrdHdr::where('id', $hdrId)
                    ->first();
                return $delOrdHdr;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function commitToComplete($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = DelOrdHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();
                    
                if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('DelOrd.doc_status_is_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\DelOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                if($hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('DelOrd.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\DelOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }

                $dtlModels = DelOrdDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->orderBy('line_no')
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('DelOrd.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\DelOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
        
                //1) update docStatus to COMPLETE
                $hdrModel->doc_status = DocStatus::$MAP['COMPLETE'];
                $hdrModel->save();

                return $hdrModel;              
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitToWip($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = DelOrdHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('DelOrd.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\DelOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                $dtlModels = DelOrdDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->orderBy('line_no')
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('DelOrd.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\DelOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }

                //1) update docStatus to WIP
                $hdrModel->doc_status = DocStatus::$MAP['WIP'];
                $hdrModel->save();
                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertCompleteToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = DelOrdHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                {
                    $exc = new ApiException(__('DelOrd.doc_status_is_not_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\DelOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertWipToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = DelOrdHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['WIP'])
                {
                    $exc = new ApiException(__('DelOrd.doc_status_is_not_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\DelOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function updateDetails($delOrdHdrData, $delOrdDtlArray, $delDelOrdDtlArray, $outbOrdHdrData, $outbOrdDtlArray, $delOutbOrdDtlArray) 
	{
        $delDelOrdDtlIds = array();
        foreach($delDelOrdDtlArray as $delDelOrdDtlData)
        {
            $delDelOrdDtlIds[] = $delDelOrdDtlData['id'];
        }

        $delOutbOrdDtlIds = array();
        foreach($delOutbOrdDtlArray as $delOutbOrdDtlData)
        {
            $delOutbOrdDtlIds[] = $delOutbOrdDtlData['id'];
        }
        
        $result = DB::transaction
        (
            function() use ($delOrdHdrData, $delOrdDtlArray, $delDelOrdDtlIds, $outbOrdHdrData, $outbOrdDtlArray, $delOutbOrdDtlIds)
            {
                //update outbOrdHdr
                $outbOrdHdrIdHashByUuid = array();
                $outbOrdDtlIdHashByUuid = array();
                if(!empty($outbOrdHdrData))
                {
                    $outbOrdHdrModel = OutbOrdHdr::lockForUpdate()->find($outbOrdHdrData['id']);
                    if($outbOrdHdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                    {
                        //only DRAFT or above can transition to COMPLETE
                        $exc = new ApiException(__('OutbOrd.doc_status_is_complete', ['docCode'=>$outbOrdHdrModel->doc_code]));
                        $exc->addData(\App\OutbOrdHdr::class, $outbOrdHdrModel->id);
                        throw $exc;
                    }
                    $outbOrdHdrModel = RepositoryUtils::dataToModel($outbOrdHdrModel, $outbOrdHdrData);
                    $outbOrdHdrModel->save();

                    //update outbOrdDtl array
                    foreach($outbOrdDtlArray as $outbOrdDtlData)
                    {
                        $uuid = '';
                        $outbOrdDtlModel = null;
                        if (strpos($outbOrdDtlData['id'], 'NEW') !== false) 
                        {
                            $uuid = $outbOrdDtlData['id'];
                            unset($outbOrdDtlData['id']);
                            $outbOrdDtlModel = new OutbOrdDtl;
                        }
                        else
                        {
                            $outbOrdDtlModel = OutbOrdDtl::lockForUpdate()->find($outbOrdDtlModel['id']);
                        }
                        
                        $outbOrdDtlModel = RepositoryUtils::dataToModel($outbOrdDtlModel, $outbOrdDtlData);
                        $outbOrdDtlModel->hdr_id = $outbOrdHdrModel->id;
                        $outbOrdDtlModel->save();

                        if(!empty($uuid))
                        {
                            $outbOrdHdrIdHashByUuid[$uuid] = $outbOrdDtlModel->hdr_id;
                            $outbOrdDtlIdHashByUuid[$uuid] = $outbOrdDtlModel->id;
                        }
                    }

                    if(!empty($delOutbOrdDtlIds))
                    {
                        OutbOrdDtl::destroy($delOutbOrdDtlIds);
                    }
                }

                //update delOrdHdr
                $delOrdHdrModel = DelOrdHdr::lockForUpdate()->find($delOrdHdrData['id']);
                if($delOrdHdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('DelOrd.doc_status_is_complete', ['docCode'=>$delOrdHdrModel->doc_code]));
                    $exc->addData(\App\DelOrdHdr::class, $delOrdHdrModel->id);
                    throw $exc;
                }
                $delOrdHdrModel = RepositoryUtils::dataToModel($delOrdHdrModel, $delOrdHdrData);
                $delOrdHdrModel->save();

                //update delOrdDtl array
                $delOrdDtlModels = array();
                foreach($delOrdDtlArray as $delOrdDtlData)
                {
                    $delOrdDtlModel = null;
                    if (strpos($delOrdDtlData['id'], 'NEW') !== false) 
                    {
                        $uuid = $delOrdDtlData['id'];
                        $delOrdDtlModel = new DelOrdDtl;
                        if(array_key_exists($uuid, $outbOrdHdrIdHashByUuid))
                        {
                            $delOrdDtlModel->outb_ord_hdr_id = $outbOrdHdrIdHashByUuid[$uuid];
                        }
                        if(array_key_exists($uuid, $outbOrdDtlIdHashByUuid))
                        {
                            $delOrdDtlModel->outb_ord_dtl_id = $outbOrdDtlIdHashByUuid[$uuid];
                        }
                        unset($delOrdDtlData['id']);
                    }
                    else
                    {
                        $delOrdDtlModel = DelOrdDtl::lockForUpdate()->find($delOrdDtlData['id']);
                    }
                    
                    $delOrdDtlModel = RepositoryUtils::dataToModel($delOrdDtlModel, $delOrdDtlData);
                    $delOrdDtlModel->hdr_id = $delOrdHdrModel->id;
                    $delOrdDtlModel->save();
                    $delOrdDtlModels[] = $delOrdDtlModel;
                }

                if(!empty($delDelOrdDtlIds))
                {
                    DelOrdDtl::destroy($delDelOrdDtlIds);
                }

                return array(
                    'hdrModel' => $delOrdHdrModel,
                    'dtlModels' => $delOrdDtlModels
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function findAll($divisionId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $leftJoinHash = array();
        $delOrdHdrs = DelOrdHdr::select('del_ord_hdrs.*')
            ->where('division_id', $divisionId);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $delOrdHdrs->where('del_ord_hdrs.doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $delOrdHdrs->where(function($q) use($filter) {
                    $q->where('delivery_points.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('delivery_points.company_name_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'salesman') == 0)
            {
                $leftJoinHash['users'] = 'users';
                $delOrdHdrs->where('users.username', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point_area') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $leftJoinHash['areas'] = 'areas';
                $delOrdHdrs->where(function($q) use($filter) {
                    $q->where('areas.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('areas.desc_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'status') == 0)
            {
                if($filter['value'] > 0)
                {
                    $delOrdHdrs->where('del_ord_hdrs.doc_status', $filter['value']);
                }
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $delOrdHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'delivery_points') == 0)
            {
                $delOrdHdrs->leftJoin('delivery_points', 'delivery_points.id', '=', 'del_ord_hdrs.delivery_point_id');
            }
            if(strcmp($field, 'users') == 0)
            {
                $delOrdHdrs->leftJoin('users', 'users.id', '=', 'del_ord_hdrs.salesman_id');
            }
            if(strcmp($field, 'areas') == 0)
            {
                $delOrdHdrs->leftJoin('areas', 'areas.id', '=', 'delivery_points.area_id');
            }
        }
        
        if($pageSize > 0)
        {
            return $delOrdHdrs
                ->paginate($pageSize);
        }
        else
        {
            return $delOrdHdrs
                ->paginate(PHP_INT_MAX);
        }
    }
}