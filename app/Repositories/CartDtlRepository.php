<?php

namespace App\Repositories;

use App\CartDtl;
use App\CartDtlPromotion;
use App\Models\PromotionRule;
use App\Models\Promotion;
use App\CartHdr;
use App\Debtor;
use App\Item;
use App\Services\CartService;
use App\Services\Env\ResStatus;
use App\Services\Env\DocStatus;
use App\Services\Env\PromotionRuleType;
use App\Services\Env\PromotionType;
use App\Repositories\PromotionActionRepository;
use Illuminate\Support\Facades\DB;

class CartDtlRepository 
{
    static public function findByPk($dtlId) 
	{
        $cartDtl = CartDtl::where('id', $dtlId)
            ->first();

        return $cartDtl;
    }

    static public function findAllByHdrId($hdrId) 
	{
        $cartDtls = CartDtl::where('hdr_id', $hdrId)
            ->orderBy('line_no', 'ASC')
            ->get();

        return $cartDtls;
    }

    static public function findAllByHdrIds($hdrIds) 
	{
        $cartDtls = CartDtl::whereIn('hdr_id', $hdrIds)
            ->get();

        return $cartDtls;
    }

    static public function findAllByIds($ids) 
	{
        $cartDtls = CartDtl::whereIn('id', $ids)
            ->get();

        return $cartDtls;
    }

    static public function findTopByHdrId($hdrId)
    {
        $cartDtl = CartDtl::where('hdr_id', $hdrId)
            ->orderBy('line_no')
            ->first();

        return $cartDtl;
    }

    static public function findAllByItemId($itemId) 
	{
        $cartDtls = CartDtl::where('item_id', $itemId)
            ->get();

        return $cartDtls;
    }

    static public function getTotalItemQty($hdrId, $itemId) 
    {
        $totalQty = DB::table('cart_dtls')
            ->where('hdr_id', $hdrId)
            ->where('item_id', $itemId)
            ->sum(DB::raw('qty * uom_rate'));
        return $totalQty;
    }

    static public function getTotalItemArrayQty($hdrId, $itemIds) 
    {
        $totalQty = DB::table('cart_dtls')
            ->where('hdr_id', $hdrId)
            ->whereIn('item_id', $itemIds)
            ->sum(DB::raw('qty * uom_rate'));
        return $totalQty;
    }

    static public function getTotalMainItemArrayQty($hdrId, $itemIds) 
    {
        $totalQty = DB::table('cart_dtls')
            ->where('hdr_id', $hdrId)
            ->whereIn('item_id', $itemIds)
            ->where('net_amt', '>', 0.00)
            ->sum(DB::raw('qty * uom_rate'));
        return $totalQty;
    }

    static public function disableCartDtlByPromotionId($id)
    {
        DB::beginTransaction();
        try {
            $cart_dtl = CartDtl::find($id);
            $cart_dtl->status = ResStatus::$MAP['CONFLICT'];
            $cart_dtl->save();

            DB::commit();
            return $cart_dtl;
        } catch (\Exception $e) {
            DB::rollback();
            throw new \Exception($e);
        }
    }

    // static public function checkForPromotionChanges($cart_dtls, $division_id, $debtor_id)
    // {
    //     DB::beginTransaction();
    //     try {
    //         //Initialize max perc disc and max fixed disc
    //         $max_perc = ['id' => 0, 'disc' => 0];
    //         $max_fixed = ['id' => 0, 'disc' => 0];

    //         //Get all promotions by division ID and type
    //         $promotions = Promotion::whereIn('type', [PromotionType::$MAP['disc_percent'], PromotionType::$MAP['disc_fixed_price']])
    //                                 ->where('division_id', $division_id)
    //                                 ->get();


    //         foreach($cart_dtls as $cart_dtl) {
    //             $item_id = $cart_dtl->item->id;
    //             $entitled_promotions = [];

    //             //Loop for all promotions to filter
    //             foreach($promotions as $promotion) {
    //                 $rules = $promotion->rules;
    //                 $variants = $promotion->variants;
    //                 $item_id_array = [];
    //                 $debtors = [];
    //                 $group_01s = [];
    //                 $debtor_exists = false;

    //                 //Get Rules
    //                 $rules = $promotion->rules;
    //                 foreach($rules as $rule) {
    //                     if($rule->type == PromotionRuleType::$MAP['debtor']) {
    //                         $debtors = explode(',', $rule->config['debtor_id']);
    //                     } else if($rule->type == PromotionRuleType::$MAP['debtor_group']) {
    //                         $group_01s = explode(',', $rule->config['debtor_group_01_id']);
    //                     } 
    //                 }

    //                 //Checks if debtor is entitled
    //                 $debtor_exists = CartDtlRepository::checkDebtorEligibility($promotion, $debtor_id, $debtors, $group_01s);

    //                 //Get Item Variants
    //                 $variants = $promotion->variants;
    //                 foreach($variants as $variant) {
    //                     if($variant->item_id > 0) {
    //                         $item_id_array[] = $variant->item_id;
    //                     } else if($variant->type === 'item_group01') {
    //                         $item_array = Item::where('item_group_01_id', $variant->item_group_01_id)->select('id')->get();
    //                         foreach($item_array as $item) {
    //                             $item_id_array[] = $item->id;
    //                         }
    //                     } else if($variant->type === 'item_group02') {
    //                         $item_array = Item::where('item_group_02_id', $variant->item_group_02_id)->select('id')->get();
    //                         foreach($item_array as $item) {
    //                             $item_id_array[] = $item->id;
    //                         }
    //                     } else if($variant->type === 'item_group03') {
    //                         $item_array = Item::where('item_group_03_id', $variant->item_group_03_id)->select('id')->get();
    //                         foreach($item_array as $item) {
    //                             $item_id_array[] = $item->id;
    //                         }
    //                     }
    //                 }

    //                 //If entitled, push to array
    //                 if($debtor_exists && in_array($item_id, $item_id_array)) {
    //                     $entitled_promotions[] = $promotion;
    //                 }
    //             }

    //             //Loop entitled promo list to find max disc per type
    //             foreach($entitled_promotions as $entitled) {
    //                 $action = $entitled->actions[0];
    //                 if($entitled->type == PromotionType::$MAP['disc_fixed_price']) {
    //                     if($action->config['disc_fixed_price'] > $max_fixed['disc']) {
    //                         $max_fixed['id'] = $entitled->id;
    //                         $max_fixed['disc'] = $action->config['disc_fixed_price'];
    //                     }
    //                 } else if($entitled->type == PromotionType::$MAP['disc_percent']) {
    //                     if($action->config['disc_perc_01'] > $max_fixed['disc']) {
    //                         $max_fixed['id'] = $entitled->id;
    //                         $max_fixed['disc'] = $action->config['disc_perc_01'];
    //                     }
    //                 }
    //             }

    //             //Apply the max perc and fixed disc
    //             $dtl_promotions = $cart_dtl->promotions;
    //             foreach($dtl_promotions as $dtl_promotion) {
                    
    //             }
    //         }

    //         DB::commit();
    //     } catch (\Exception $e) {
    //         DB::rollback();
    //         throw new \Exception($e);
    //     }
    // }

    // static public function updateCartDtlPriceByPromotionId($promotion, $update_type = null)
    // {
    //     $rules = $promotion->rules;

    //     DB::beginTransaction();
    //     try {
    //         $cart_dtls = CartDtl::join('cart_dtl_promotions', 'cart_dtls.id','=', 'cart_dtl_promotions.cart_dtl_id')
    //                     ->where('cart_dtl_promotions.promotion_id', $promotion->id)
    //                     ->select('cart_dtls.hdr_id', 'cart_dtls.id')
    //                     ->orderBy('cart_dtls.hdr_id', 'asc')
    //                     ->get();

    //         //Get Rules
    //         $debtors = [];
    //         $group_01s = [];
    //         foreach($rules as $rule) {
    //             if($rule->type == PromotionRuleType::$MAP['debtor']) {
    //                 $debtors = explode(',', $rule->config['debtor_id']);
    //             } else if($rule->type == PromotionRuleType::$MAP['debtor_group']) {
    //                 $group_01s = explode(',', $rule->config['debtor_group_01_id']);
    //             } 
    //         }

    //         //Change cart_dtl status to conflict if hdr_id is not within min buy_qty
    //         foreach($cart_dtls as $dtl) {
    //             $conflict = true;
    //             $debtor_exists = false;
    
    //             if($update_type == 'rule') {
    //                 $debtor_exists = CartDtlRepository::checkDebtorEligibility($promotion, $dtl, $debtors, $group_01s);
    //             }
    
    //             //If debtor/group exists or no rules exists, check if promotion is applied
    //             if($debtor_exists) {
    //                 $conflict = false;
    //             }

    //             if($conflict) {
    //                 $cart_dtl = CartDtl::find($dtl->id);
    //                 if($cart_dtl->status == ResStatus::$MAP['ACTIVE']) {
    //                     $cart_dtl->status = ResStatus::$MAP['CONFLICT'];
    //                     $cart_dtl->save();
    //                 }
    //             } 
    //         }

    //         DB::commit();
    //     } catch (\Exception $e) {
    //         DB::rollback();
    //         throw new \Exception($e);
    //     }
    // }

    // static public function checkDebtorEligibility($promotion, $debtor_id, $debtors, $group_01s)
    // {
    //     $debtor_exists = false;
        
    //     if($promotion->type == PromotionType::$MAP['disc_percent']) {
    //         $debtor = Debtor::find($debtor_id);
    //         if(!empty($debtors)) {
    //             if(in_array($debtor_id, $debtors)) {
    //                 $debtor_exists = true;
    //             } else if(!empty($group_01s)) {
    //                 if(in_array($debtor->debtor_category_01_id, $group_01s)) {
    //                     $debtor_exists = true;
    //                 }
    //             }
    //         } else if(!empty($group_01s)) {
    //             if(in_array($debtor->debtor_category_01_id, $group_01s)) {
    //                 $debtor_exists = true;
    //             }
    //         } else {
    //             $debtor_exists = true;
    //         }
    //     }

    //     return $debtor_exists;
    // }
}