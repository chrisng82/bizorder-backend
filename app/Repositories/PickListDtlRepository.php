<?php

namespace App\Repositories;

use App\Services\Utils\RepositoryUtils;
use App\PickListDtl;
use App\Services\Env\ProcType;
use App\Services\Env\DocStatus;
use Illuminate\Support\Facades\DB;

class PickListDtlRepository 
{
    static public function findAllByHdrId($hdrId) 
	{
        $pickListDtls = PickListDtl::where('hdr_id', $hdrId)
            ->orderBy('whse_job_type')
            ->orderBy('line_no')
            ->get();

        return $pickListDtls;
    }

    static public function findAllByHdrIds($hdrIds) 
	{
        $pickListDtls = PickListDtl::whereIn('hdr_id', $hdrIds)
            ->get();

        return $pickListDtls;
    }

    static public function updateWhseJobType($dtlId, $whseJobType) 
	{
        $dtlModel = DB::transaction
        (
            function() use ($dtlId, $whseJobType)
            {
                $pickListDtl = PickListDtl::where('id', $dtlId)
                    ->lockForUpdate()
                    ->first();
                
                $pickListDtl->whse_job_type = $whseJobType;
                $pickListDtl->save();
                
                return $pickListDtl;
            }, 
            5 //reattempt times
        );

        return $dtlModel;
    }
    
    static public function create($dtlData) 
	{
        $dtlModel = new PickListDtl;
        $dtlModel->hdr_id = $dtlData['hdr_id'];
		$dtlModel->company_id = $dtlData['company_id'];
		$dtlModel->item_id = $dtlData['item_id'];
		$dtlModel->desc_01 = $dtlData['desc_01'];
		$dtlModel->desc_02 = $dtlData['desc_02'];
		$dtlModel->uom_id = $dtlData['uom_id'];
		$dtlModel->uom_rate = $dtlData['uom_rate'];
        $dtlModel->qty = $dtlData['qty'];
        $dtlModel->storage_bin_id = $dtlData['storage_bin_id'];
		$dtlModel->quant_bal_id = $dtlData['quant_bal_id'];
		$dtlModel->to_storage_bin_id = $dtlData['to_storage_bin_id'];
		$dtlModel->to_handling_unit_id = $dtlData['to_handling_unit_id'];
		$dtlModel->whse_job_type = $dtlData['whse_job_type'];
		$dtlModel->whse_job_code = $dtlData['whse_job_code'];
		$dtlModel->save();
        return $dtlModel;
    }

    static public function createPickFaceReplenishment($dtlData) 
	{
        $dtlModel = DB::transaction
        (
            function() use ($dtlData)
            {
                $dtlModel = PickListDtl::where('hdr_id', $dtlData['hdr_id'])
                ->where('quant_bal_id', $dtlData['quant_bal_id'])
                ->where('whse_job_type', $dtlData['whse_job_type'])
                ->lockForUpdate()
                ->first();
                if(empty($dtlModel))
                {
                    $dtlModel = new PickListDtl;
                    $dtlModel->hdr_id = $dtlData['hdr_id'];
                    $dtlModel->company_id = $dtlData['company_id'];
                    $dtlModel->item_id = $dtlData['item_id'];
                    $dtlModel->desc_01 = $dtlData['desc_01'];
                    $dtlModel->desc_02 = $dtlData['desc_02'];
                    $dtlModel->uom_id = $dtlData['uom_id'];
                    $dtlModel->uom_rate = $dtlData['uom_rate'];
                    $dtlModel->qty = $dtlData['qty'];
                    $dtlModel->storage_bin_id = $dtlData['storage_bin_id'];
                    $dtlModel->quant_bal_id = $dtlData['quant_bal_id'];
                    $dtlModel->to_storage_bin_id = $dtlData['to_storage_bin_id'];
                    $dtlModel->to_handling_unit_id = $dtlData['to_handling_unit_id'];
                    $dtlModel->whse_job_type = $dtlData['whse_job_type'];
                    $dtlModel->whse_job_code = $dtlData['whse_job_code'];
                    $dtlModel->save();
                }
                return $dtlModel;
            }, 
            5 //reattempt times
        );

        return $dtlModel;
    }

    static public function breakUp($pickListDtlId, $oldDtlData, $newDtlData) 
	{
        $result = DB::transaction
        (
            function() use ($pickListDtlId, $oldDtlData, $newDtlData)
            {
                $oldPickListDtl = PickListDtl::where('id', $pickListDtlId)
                    ->lockForUpdate()
                    ->first();
                
                $oldPickListDtl = RepositoryUtils::dataToModel($oldPickListDtl, $oldDtlData);
                $oldPickListDtl->save();

                $newPickListDtl = $oldPickListDtl->replicate();
                $newPickListDtl = RepositoryUtils::dataToModel($newPickListDtl, $newDtlData);
                $newPickListDtl->save();

                return array(
                    'old_pick_list_dtl' => $oldPickListDtl,
                    'new_pick_list_dtl' => $newPickListDtl,
                );
            }, 
            5 //reattempt times
        );

        return $result;
    }

    static public function queryShipmentDetails($hdrId, $sortfield = 'item_group_01_code', $sortOrder = 'ASC') 
	{
        $pickListDtls = PickListDtl::select(
                'pick_list_dtls.hdr_id AS hdr_id',
                'pick_list_dtls.id AS id',
                'pick_list_dtls.whse_job_type AS whse_job_type',
                'pick_list_dtls.whse_job_code AS whse_job_code',
                'items.id AS item_id',
                'items.code AS item_code',
                'items.ref_code_01 AS item_ref_code_01',
                'items.desc_01 AS item_desc_01',
                'items.desc_02 AS item_desc_02',
                'items.storage_class AS storage_class',
                'items.item_group_01_code AS item_group_01_code',
                'items.item_group_02_code AS item_group_02_code',
                'items.item_group_03_code AS item_group_03_code',
                'items.item_group_04_code AS item_group_04_code',
                'items.item_group_05_code AS item_group_05_code',
                'items.s_storage_class AS s_storage_class',
                'items.s_item_group_01_code AS s_item_group_01_code',
                'items.s_item_group_02_code AS s_item_group_02_code',
                'items.s_item_group_03_code AS s_item_group_03_code',
                'items.s_item_group_04_code AS s_item_group_04_code',
                'items.s_item_group_05_code AS s_item_group_05_code',
                'item_batches.batch_serial_no AS batch_serial_no',
                'item_batches.expiry_date AS expiry_date',
                'item_batches.receipt_date AS receipt_date',
                'storage_bins.code AS storage_bin_code',
                'storage_rows.code AS storage_row_code',
                'storage_bays.code AS storage_bay_code',
                'storage_bays.bay_sequence AS item_bay_sequence'
            )
            //unit_qty
            ->selectRaw('(pick_list_dtls.qty * pick_list_dtls.uom_rate) AS unit_qty')
            //case_qty
            ->selectRaw('(pick_list_dtls.qty * pick_list_dtls.uom_rate / items.case_uom_rate) AS case_qty')
            //gross_weight, kg
            ->selectRaw('(pick_list_dtls.qty * pick_list_dtls.uom_rate / items.case_uom_rate * items.case_gross_weight) AS gross_weight')
            //cubic_meter, meter
            ->selectRaw('(pick_list_dtls.qty * pick_list_dtls.uom_rate / items.case_uom_rate * items.case_ext_length * items.case_ext_width * items.case_ext_height / 1000000000) AS cubic_meter')
            ->leftJoin('items', 'items.id', '=', 'pick_list_dtls.item_id')
            ->leftJoin('quant_bals', 'quant_bals.id', '=', 'pick_list_dtls.quant_bal_id')
            ->leftJoin('item_batches', 'item_batches.id', '=', 'quant_bals.item_batch_id')
            ->leftJoin('storage_bins', 'storage_bins.id', '=', 'quant_bals.storage_bin_id')
            ->leftJoin('storage_rows', 'storage_rows.id', '=', 'quant_bals.storage_row_id')
            ->leftJoin('storage_bays', 'storage_bays.id', '=', 'quant_bals.storage_bay_id')
            ->where('hdr_id', $hdrId)
            ->orderBy('item_bay_sequence', 'ASC')
            ->when(in_array($sortfield, array('item_code', 'item_ref_code_01', 'item_desc_01', 'item_desc_02')) === FALSE, function ($query) use($sortfield, $sortOrder) {
                return $query->orderBy($sortfield, $sortOrder);
            })
            ->orderBy('item_code', $sortOrder)
            ->get();
        return $pickListDtls;
    }

    static public function findWIPByQuantBalIdAndWhseJobType($quantBalId, $whseJobType) 
	{
        $pickListDtl = PickListDtl::select('pick_list_dtls.*')
            ->leftJoin('pick_list_hdrs', 'pick_list_hdrs.id', '=', 'pick_list_dtls.hdr_id')
            ->where('pick_list_hdrs.doc_status', DocStatus::$MAP['WIP'])
            ->where('quant_bal_id', $quantBalId)
            ->where('whse_job_type', $whseJobType)
            ->first();

        return $pickListDtl;
    }

    static public function findAllByQuantBalIdAndWhseJobType($quantBalId, $whseJobType) 
	{
        $pickListDtls = PickListDtl::select('pick_list_dtls.*')
            ->selectRaw('pick_list_hdrs.doc_code AS doc_code')
            ->leftJoin('pick_list_hdrs', 'pick_list_hdrs.id', '=', 'pick_list_dtls.hdr_id')
            //->where('pick_list_hdrs.doc_status', DocStatus::$MAP['WIP'])
            ->where('quant_bal_id', $quantBalId)
            ->where('whse_job_type', $whseJobType)
            ->get();

        return $pickListDtls;
    }
}