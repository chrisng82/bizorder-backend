<?php

namespace App\Repositories;

use App\SyncSettingDtl;
use App\Services\Utils\ApiException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class SyncSettingDtlRepository
{
    static public function findByHdrIdAndField($hdrId, $field) 
	{
        $syncSettingDtl = SyncSettingDtl::where('hdr_id', $hdrId)
        ->where('field', $field)
        ->first();
        return $syncSettingDtl;
    }
}