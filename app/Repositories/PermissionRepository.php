<?php

namespace App\Repositories;

use Spatie\Permission\Models\Permission;
use App\Services\Utils\ApiException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PermissionRepository
{
    static public function findAll($sorts, $filters = array(), $pageSize = 20) 
	{
        $permissions = Permission::select('permissions.*');

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'name') == 0)
            {
                $permissions->where('permissions.name', 'LIKE', '%'.$filter['value'].'%');
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'name') == 0)
            {
                $permissions->orderBy('permissions.name', $sort['order']);
            }
        }
        
        if($pageSize > 0)
        {
            return $permissions
                ->paginate($pageSize);
        }
        else
        {
            return $permissions
                ->paginate(PHP_INT_MAX);
        }
    }

    static public function refresh($dataList) 
	{
        $models = array();
        foreach($dataList as $data)
        {
            $model = Permission::findOrCreate($data['name'], 'web');

            $models[] = $model;
        }

        return $models;
    }

    static public function syncRoleExcel01($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = Permission::findOrCreate($data['name'], 'web');

                return $model;
            }
        );
        return $model;
    }

    static public function select2($search, $filters, $pageSize = 10) 
	{
        $permissions = Permission::where(function($q) use($search) {
            $q->orWhere('name', 'LIKE', '%' . $search . '%');
        });
        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'role_id') == 0)
            {
                $operator = 'NOT IN';
                if(isset($filter['operator']))
                {
                    $operator = $filter['operator'];
                }
                if(strcmp($operator, 'NOT IN') == 0)
                {  
                    $permissions->whereRaw('permissions.id NOT IN(SELECT permission_id FROM role_has_permissions WHERE role_id = ?)', array($filter['value']));
                }
                elseif(strcmp($operator, 'IN') == 0)
                {
                    $permissions->whereRaw('permissions.id IN(SELECT permission_id FROM role_has_permissions WHERE role_id = ?)', array($filter['value']));
                }                
            }
        }
        return $permissions
            ->orderBy('name', 'ASC')
            ->paginate($pageSize);
    }

    static public function findByPk($id) 
	{
        $model = Permission::where('id', $id)
            ->first();

        return $model;
    }
}