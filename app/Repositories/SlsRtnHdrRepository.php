<?php

namespace App\Repositories;

use App\SlsRtnDtl;
use App\SlsRtnHdr;
use App\DocTxnFlow;
use App\InbOrdHdr;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SlsRtnHdrRepository 
{
    static public function findAllNotExistInbOrd02Txn($divisionId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $outbOrd02TxnFlows = DocTxnFlow::select('doc_txn_flows.fr_doc_hdr_id')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['INB_ORD_02'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\SlsRtnHdr::class)
                    ->distinct();

        //$qStatuses = array(DocStatus::$MAP['DRAFT'], DocStatus::$MAP['WIP'], DocStatus::$MAP['COMPLETE']);
        $leftJoinHash = array();
        $slsRtnHdrs = SlsRtnHdr::select('sls_rtn_hdrs.*')
            ->leftJoinSub($outbOrd02TxnFlows, 'inb_ord_02_txn_flows', function ($join) {
                $join->on('sls_rtn_hdrs.id', '=', 'inb_ord_02_txn_flows.fr_doc_hdr_id');
            })
            ->whereNull('inb_ord_02_txn_flows.fr_doc_hdr_id')
            ->where('division_id', $divisionId);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $slsRtnHdrs->where('doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'salesman') == 0)
            {
                $leftJoinHash['users'] = 'users';
                $slsRtnHdrs->where('users.username', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $slsRtnHdrs->where(function($q) use($filter) {
                    $q->where('delivery_points.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('delivery_points.company_name_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'delivery_point_area') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $leftJoinHash['areas'] = 'areas';
                $slsRtnHdrs->where(function($q) use($filter) {
                    $q->where('areas.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('areas.desc_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $slsRtnHdrs->orderBy('doc_code', $sort['order']);
            }
            if(strcmp($sort['field'], 'delivery_point_area_code') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $slsRtnHdrs->orderBy('delivery_points.area_code', $sort['order']);
            }
            if(strcmp($sort['field'], 'salesman_username') == 0)
            {
                $leftJoinHash['users'] = 'users';
                $slsRtnHdrs->orderBy('users.username', $sort['order']);
            }
            if(strcmp($sort['field'], 'delivery_point_code') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $slsRtnHdrs->orderBy('delivery_points.code', $sort['order']);
            }         
            if(strcmp($sort['field'], 'doc_date') == 0)
            {
                $slsRtnHdrs->orderBy('doc_date', $sort['order']);
            }
            if(strcmp($sort['field'], 'net_amt') == 0)
            {
                $slsRtnHdrs->orderBy('net_amt', $sort['order']);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'users') == 0)
            {
                $slsRtnHdrs->leftJoin('users', 'users.id', '=', 'sls_rtn_hdrs.salesman_id');
            }
            if(strcmp($field, 'delivery_points') == 0)
            {
                $slsRtnHdrs->leftJoin('delivery_points', 'delivery_points.id', '=', 'sls_rtn_hdrs.delivery_point_id');
            }
            if(strcmp($field, 'areas') == 0)
            {
                $slsRtnHdrs->leftJoin('areas', 'areas.id', '=', 'delivery_points.area_id');
            }
        }

        return $slsRtnHdrs
        ->paginate($pageSize);
    }

    static public function findAllByHdrIds($hdrIds)
    {
        $slsRtnHdrs = SlsRtnHdr::whereIn('id', $hdrIds)
            ->get();

        return $slsRtnHdrs;
    }

    static public function syncSlsRtnSync01($company, $division, $data) 
	{
        $siteFlow = $division->siteFlow;

        $slsRtnHdrModel = DB::transaction
        (
            function() use ($siteFlow, $company, $division, $data)
            {
                $isDirty = false;
                $model = SlsRtnHdr::firstOrCreate(['doc_code' => $data['doc_code']]);
                //get this sales return docFlowTxn
                $inbOrdHdrId = 0;
                $toDocTxnFlows = $model->toDocTxnFlows;
                foreach($toDocTxnFlows as $toDocTxnFlow)
                {
                    if(strcmp($toDocTxnFlow->to_doc_hdr_type, \App\InbOrdHdr::class) != 0)
                    {
                        //skip if this is not InbOrdHdr
                        continue;
                    }
                    $inbOrdHdrId = $toDocTxnFlow->to_doc_hdr_id;
                }

                //check if this return oledi have inb_ord_hdr_id
                if($inbOrdHdrId > 0)
                {
                    /*
                    //if already transfer to outOrd, then no need to do any sync
                    //but update the est_del_date, desc_01, desc_02
                    //$model->est_del_date = $data['userdate_01'];
                    $model->desc_01 = $data['remark_01'];
                    $model->desc_02 = $data['remark_02'];
                    $model->save();
                    
                    $inbOrdHdr = InbOrdHdr::find($model->inb_ord_hdr_id);
                    //$inbOrdHdr->est_del_date = $model->est_del_date;
                    $inbOrdHdr->desc_01 = $model->desc_01;
                    $inbOrdHdr->desc_02 = $model->desc_02;
                    $inbOrdHdr->save();
                    */
                    return $model;
                }
                
                $model->proc_type = ProcType::$MAP['SLS_RTN_SYNC_01'];
                if($model->doc_status == DocStatus::$MAP['NULL'])
                {
                    //initialize with DRAFT status
                    $model->doc_status = DocStatus::$MAP['DRAFT'];
                }             
                $model->ref_code_01 = $data['ref_code'];
                $model->doc_date = $data['doc_date'];
                //$model->est_del_date = $data['userdate_01'];
                $model->est_del_date = '1970-00-00';
                $model->desc_01 = $data['remark_01'];
                $model->desc_02 = $data['remark_02'];

                $model->division_id = $division->id;
                $model->site_flow_id = $division->site_flow_id;
                $model->company_id = $company->id;

                //currency
                $model->currency_id = 1;
                $model->currency_rate = $data['currency_rate'];
                $currency = CurrencyRepository::syncSlsOrdSync01($data);
                if(!empty($currency))
                {
                    $model->currency_id = $currency->id;
                }
                
                //creditTerm
                $model->credit_term_id = 0;
                $creditTerm = CreditTermRepository::syncSlsOrdSync01($data);
                if(!empty($creditTerm))
                {
                    $model->credit_term_id = $creditTerm->id;
                }                

                //salesman
                $salesman = UserRepository::syncSlsOrdSync01($division, $data);
                $model->salesman_id = $salesman->id;

                //delivery point                
                $deliveryPoint = DeliveryPointRepository::syncSlsOrdSync01($division, $data);
                $model->debtor_id = $deliveryPoint->debtor_id;
                $model->delivery_point_id = $deliveryPoint->id;

                $model->gross_amt = $data['order_amt'];
                $model->gross_local_amt = $data['order_local_amt'];
                $model->disc_amt = $data['disc_amt'];
                $model->disc_local_amt = $data['disc_local_amt'];
                $model->tax_amt = $data['tax_amt'];
                $model->tax_local_amt = $data['tax_local_amt'];
                $model->is_round_adj = 0;
                $model->round_adj_amt = 0;
                $model->round_adj_local_amt = 0;
                $model->net_amt = $data['net_amt'];
                $model->net_local_amt = $data['net_local_amt'];

                $slsRtnDtls = SlsRtnDtl::where('hdr_id', $model->id)
                                ->get();
                $slsRtnDtlsHashByLineNo = array();
                $updatedSlsRtnDtls = array();
                $deletedSlsRtnDtls = array();
                foreach($slsRtnDtls as $slsRtnDtl)
                {
                    if(array_key_exists($slsRtnDtl->line_no, $slsRtnDtlsHashByLineNo))
                    {
                        $deletedSlsRtnDtls[] = $slsRtnDtl;
                    }
                    else
                    {
                        $slsRtnDtlsHashByLineNo[$slsRtnDtl->line_no] = $slsRtnDtl;
                    }
                }

                $details = $data['details'];
                $delDate = $data['doc_date'];
                foreach($details as $detail)
                {
                    $delDate = $detail['del_date'];

                    $slsRtnDtl = null;
                    if(array_key_exists($detail['line_no'], $slsRtnDtlsHashByLineNo))
                    {
                        $slsRtnDtl = $slsRtnDtlsHashByLineNo[$detail['line_no']];
                        unset($slsRtnDtlsHashByLineNo[$detail['line_no']]);
                    }
                    else
                    {
                        $slsRtnDtl = new SlsRtnDtl();
                        $slsRtnDtl->hdr_id = $model->id;
                    }

                    $slsRtnDtl->line_no = $detail['line_no'];

                    $location = LocationRepository::syncSlsRtnSync01($siteFlow->site_id, $detail);
                    if(empty($location))
                    {
                        $exc = new ApiException(__('SlsRtn.location_not_found', ['code'=>$detail['location_code']]));
                        //$exc->addData(\App\Location::class, $hdrModel->id);
                        throw $exc;
                    }
                    $slsRtnDtl->location_id = $location->id;

                    $uom = UomRepository::syncSlsOrdSync01($division, $detail);
                    
                    $item = ItemRepository::syncSlsOrdSync01($division, $uom, $detail);
                    $slsRtnDtl->item_id = $item->id;
                    $slsRtnDtl->desc_01 = $detail['description'];
                    $slsRtnDtl->desc_02 = '';
                    $slsRtnDtl->desc_03 = $detail['remark'];
                    $slsRtnDtl->desc_04 = '';

                    $reason01 = Reason01Repository::syncSlsRtnSync01($detail);
                    $slsRtnDtl->reason_01_id = $reason01->id;

                    $slsRtnDtl->ref_doc_dtl_01_type = '';
                    $slsRtnDtl->ref_doc_dtl_01_id = 0;
                    //refer to field
                    $slsRtnDtl->ref_doc_dtl_01_code = $detail['userfield_01'];
                    
                    $slsRtnDtl->uom_id = $uom->id;
                    $slsRtnDtl->uom_rate = $detail['uom_rate'];
                    if(bccomp($detail['list_price'], $detail['price'], 2) > 0)
                    {
                        $slsRtnDtl->sale_price = $detail['list_price'];
                        $slsRtnDtl->price_disc = bcsub($detail['list_price'], $detail['price'], 2);
                    }
                    else
                    {
                        $slsRtnDtl->sale_price = $detail['price'];
                        $slsRtnDtl->price_disc = 0;
                    }
                    $slsRtnDtl->qty = $detail['qty'];
                    $slsRtnDtl->dtl_disc_perc_01 = $detail['disc_percent_01'] >= 0 ? $detail['disc_percent_01'] : 0; //for efichain disc amt
                    $slsRtnDtl->dtl_disc_val_01 = 0;
                    if(bccomp($detail['disc_percent_01'], -1) == 0)
                    {
                        $slsRtnDtl->dtl_disc_val_01 = $detail['usernumber_01'];
                    }
                    $slsRtnDtl->dtl_disc_perc_02 = $detail['disc_percent_02'];
                    $slsRtnDtl->dtl_disc_perc_03 = $detail['disc_percent_03'];
                    $slsRtnDtl->dtl_disc_perc_04 = $detail['disc_percent_04'];
                    $slsRtnDtl->dtl_disc_amt = $detail['disc_amt'];
                    $slsRtnDtl->dtl_disc_local_amt = $detail['disc_local_amt'];
                    $slsRtnDtl->hdr_disc_perc_01 = $detail['disc_percent_05'];
                    $slsRtnDtl->hdr_disc_perc_02 = $detail['disc_percent_06'];
                    $slsRtnDtl->hdr_disc_perc_03 = $detail['disc_percent_07'];
                    $slsRtnDtl->hdr_disc_perc_04 = $detail['disc_percent_08'];
                    $slsRtnDtl->dtl_tax_perc_01 = $detail['tax_percent_01'];
                    $slsRtnDtl->dtl_tax_perc_02 = $detail['tax_percent_02'];
                    $slsRtnDtl->dtl_tax_perc_03 = $detail['tax_percent_03'];
                    $slsRtnDtl->dtl_tax_perc_04 = $detail['tax_percent_04'];
                    $slsRtnDtl->dtl_tax_amt_01 = $detail['tax_amt'];
                    $slsRtnDtl->dtl_tax_local_amt_01 = $detail['tax_local_amt'];
                    $slsRtnDtl->gross_amt = $detail['order_amt'];
                    $slsRtnDtl->gross_local_amt = $detail['order_local_amt'];
                    $slsRtnDtl->net_amt = $detail['net_amt'];
                    $slsRtnDtl->net_local_amt = $detail['net_local_amt'];

                    if($slsRtnDtl->isDirty() == true)
                    {
                        $isDirty = $slsRtnDtl->isDirty();
                    }

                    $updatedSlsRtnDtls[] = $slsRtnDtl;
                }
                $model->est_del_date = $delDate;

                if($model->isDirty() == true)
                {
                    $isDirty = $model->isDirty();
                }
                if(count($slsRtnDtlsHashByLineNo) > 0)
                {
                    $isDirty = true;
                }
                if(count($deletedSlsRtnDtls) > 0)
                {
                    $isDirty = true;
                }

                if($isDirty == true)
                {
                    if($model->doc_status >= DocStatus::$MAP['WIP'])
                    {
                        //return to draft, so can update the details
                        if($model->doc_status == DocStatus::$MAP['COMPLETE'])
                        {
                            $tmpModel = self::revertCompleteToDraft($model->id);
                            $model->doc_status = $tmpModel->doc_status;
                        }
                        elseif($model->doc_status == DocStatus::$MAP['WIP'])
                        {
                            $tmpModel = self::revertWipToDraft($model->id);
                            $model->doc_status = $tmpModel->doc_status;
                        }
                    }
                    
                    foreach($updatedSlsRtnDtls as $updatedSlsRtnDtl)
                    {
                        $updatedSlsRtnDtl->save();
                    }
                    foreach($slsRtnDtlsHashByLineNo as $lineNo => $slsRtnDtl)
                    {
                        $slsRtnDtl->delete();
                    }
                    foreach($deletedSlsRtnDtls as $slsRtnDtl)
                    {
                        $slsRtnDtl->delete();
                    }

                    $model->save();
                }

                if($data['status'] == 3)
                {
                    if($model->doc_status == DocStatus::$MAP['VOID'])
                    {
                        $model = self::commitVoidToDraft($model->id);
                    }

                    if($model->doc_status < DocStatus::$MAP['COMPLETE'])
                    {
                        $model = self::commitToComplete($model->id, true);
                    }
                }
                else
                {
                    if($model->doc_status == DocStatus::$MAP['COMPLETE'])
                    {
                        $model = self::revertCompleteToDraft($model->id);
                    }
                    if($model->doc_status == DocStatus::$MAP['DRAFT'])
                    {
                        $model = self::commitToWip($model->id);
                    }
                }
                
                return $model;
            }, 
            5 //reattempt times
        );
        return $slsRtnHdrModel;
    }

    static public function commitToWip($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = SlsRtnHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('SlsRtn.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsRtnHdr::class, $hdrModel->id);
                    throw $exc;
                }

                $dtlModels = SlsRtnDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->orderBy('line_no')
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('SlsRtn.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsRtnHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to WIP
                $hdrModel->doc_status = DocStatus::$MAP['WIP'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitToComplete($hdrId, $isNonZeroBal)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId, $isNonZeroBal)
            {
                $hdrModel = SlsRtnHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();
                    
                if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('SlsRtn.doc_status_is_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsRtnHdr::class, $hdrModel->id);
                    throw $exc;
                }
                if($hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('SlsRtn.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsRtnHdr::class, $hdrModel->id);
                    throw $exc;
                }
        
                //1) update docStatus to COMPLETE
                $hdrModel->doc_status = DocStatus::$MAP['COMPLETE'];
                $hdrModel->save();

                $dtlModels = SlsRtnDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('SlsRtn.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsRtnHdr::class, $hdrModel->id);
                    throw $exc;
                }
                $dtlModels->load('item');
                foreach($dtlModels as $dtlModel)
                {                    
                    $item = $dtlModel->item;
                    if(empty($item))
                    {
                        //only DRAFT or above can transition to COMPLETE
                        $exc = new ApiException(__('SlsRtn.item_is_required', ['lineNo'=>$dtlModel->line_no]));
                        $exc->addData(\App\SlsRtnDtl::class, $dtlModel->id);
                        throw $exc;
                    }
                }  
                return $hdrModel;              
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertCompleteToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = SlsRtnHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                {
                    $exc = new ApiException(__('SlsRtn.doc_status_is_not_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsRtnHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertWipToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = SlsRtnHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['WIP'])
                {
                    $exc = new ApiException(__('SlsRtn.doc_status_is_not_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsRtnHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertDraftToVoid($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = SlsRtnHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('SlsRtn.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsRtnHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to VOID
                $hdrModel->doc_status = DocStatus::$MAP['VOID'];
                $hdrModel->save();

                $frDocTxnFlows = $hdrModel->frDocTxnFlows;
                foreach($frDocTxnFlows as $frDocTxnFlow)
                {
                    //move to voidTxn
                    $txnVoidModel = new DocTxnVoid;
                    $txnVoidModel->id = $frDocTxnFlow->id;
                    $txnVoidModel->proc_type = $frDocTxnFlow->proc_type;
                    $txnVoidModel->fr_doc_hdr_type = $frDocTxnFlow->fr_doc_hdr_type;
                    $txnVoidModel->fr_doc_hdr_id = $frDocTxnFlow->fr_doc_hdr_id;
                    $txnVoidModel->fr_doc_hdr_code = $frDocTxnFlow->fr_doc_hdr_code;
                    $txnVoidModel->to_doc_hdr_type = $frDocTxnFlow->to_doc_hdr_type;
                    $txnVoidModel->to_doc_hdr_id = $frDocTxnFlow->to_doc_hdr_id;
                    $txnVoidModel->is_closed = $frDocTxnFlow->is_closed;
                    $txnVoidModel->save();
        
                    //delete the txn
                    $frDocTxnFlow->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitVoidToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = SlsRtnHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['VOID'])
                {
                    //only VOID can transition to DRAFT
                    $exc = new ApiException(__('SlsRtn.doc_status_is_not_void', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsRtnHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                $frDocTxnVoids = $hdrModel->frDocTxnVoids;
                foreach($frDocTxnVoids as $frDocTxnVoid)
                {
                    //verify first
                    $docTxnFlow = DocTxnFlow::where(array(
                        'proc_type' => $frDocTxnVoid->proc_type,
                        'fr_doc_hdr_type' => $frDocTxnVoid->fr_doc_hdr_type,
                        'fr_doc_hdr_id' => $frDocTxnVoid->fr_doc_hdr_id,
                        'to_doc_hdr_type' => $frDocTxnVoid->to_doc_hdr_type
                        ))
                        ->first();
                    if(!empty($docTxnFlow))
                    {
                        $exc = new ApiException(__('SlsRtn.fr_doc_is_closed', ['docCode'=>$frDocTxnVoid->fr_doc_hdr_code]));
                        $exc->addData(\App\SlsRtnHdr::class, $hdrModel->id);
                        throw $exc;
                    }

                    //move to txnFlow
                    $txnFlowModel = new DocTxnFlow;
                    $txnFlowModel->id = $frDocTxnVoid->id;
                    $txnFlowModel->proc_type = $frDocTxnVoid->proc_type;
                    $txnFlowModel->fr_doc_hdr_type = $frDocTxnVoid->fr_doc_hdr_type;
                    $txnFlowModel->fr_doc_hdr_id = $frDocTxnVoid->fr_doc_hdr_id;
                    $txnFlowModel->fr_doc_hdr_code = $frDocTxnVoid->fr_doc_hdr_code;
                    $txnFlowModel->to_doc_hdr_type = $frDocTxnVoid->to_doc_hdr_type;
                    $txnFlowModel->to_doc_hdr_id = $frDocTxnVoid->to_doc_hdr_id;
                    $txnFlowModel->is_closed = $frDocTxnVoid->is_closed;
                    $txnFlowModel->save();
        
                    //delete the txnVoid
                    $frDocTxnVoid->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function txnFindByPk($hdrId) 
	{
        $model = DB::transaction
        (
            function() use ($hdrId)
            {
                $slsRtnHdr = SlsRtnHdr::where('id', $hdrId)
                    ->first();
                return $slsRtnHdr;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function createHeader($procType, $docNoId, $hdrData) 
	{
        $hdrModel = DB::transaction
        (
            function() use ($procType, $docNoId, $hdrData)
            {
                $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

                $hdrModel = new SlsRtnHdr;
                $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
                $hdrModel->doc_code = $newCode;
                $hdrModel->proc_type = $procType;
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];

                $hdrModel->gross_amt = 0;
                $hdrModel->gross_local_amt = 0;
                $hdrModel->disc_amt = 0;
                $hdrModel->disc_local_amt = 0;
                $hdrModel->tax_amt = 0;
                $hdrModel->tax_local_amt = 0;
                $hdrModel->is_round_adj = 0;
                $hdrModel->round_adj_amt = 0;
                $hdrModel->round_adj_local_amt = 0;
                $hdrModel->net_amt = 0;
                $hdrModel->net_local_amt = 0;
                
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function findByPk($hdrId) 
	{
        $slsRtnHdr = SlsRtnHdr::where('id', $hdrId)
            ->first();

        return $slsRtnHdr;
    }

    static public function updateDetails($slsRtnHdrData, $slsRtnDtlArray, $delSlsRtnDtlArray, $inbOrdHdrData, $inbOrdDtlArray, $delInbOrdDtlArray) 
	{
        $delSlsRtnDtlIds = array();
        foreach($delSlsRtnDtlArray as $delSlsRtnDtlData)
        {
            $delSlsRtnDtlIds[] = $delSlsRtnDtlData['id'];
        }

        $delInbOrdDtlIds = array();
        foreach($delInbOrdDtlArray as $delInbOrdDtlData)
        {
            $delInbOrdDtlIds[] = $delInbOrdDtlData['id'];
        }
        
        $result = DB::transaction
        (
            function() use ($slsRtnHdrData, $slsRtnDtlArray, $delSlsRtnDtlIds, $inbOrdHdrData, $inbOrdDtlArray, $delInbOrdDtlIds)
            {
                //update inbOrdHdr
                $inbOrdHdrIdHashByUuid = array();
                $inbOrdDtlIdHashByUuid = array();
                if(!empty($inbOrdHdrData))
                {
                    $inbOrdHdrModel = InbOrdHdr::lockForUpdate()->find($inbOrdHdrData['id']);
                    if($inbOrdHdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                    {
                        //only DRAFT or above can transition to COMPLETE
                        $exc = new ApiException(__('InbOrd.doc_status_is_complete', ['docCode'=>$inbOrdHdrModel->doc_code]));
                        $exc->addData(\App\InbOrdHdr::class, $inbOrdHdrModel->id);
                        throw $exc;
                    }
                    $inbOrdHdrModel = RepositoryUtils::dataToModel($inbOrdHdrModel, $inbOrdHdrData);
                    $inbOrdHdrModel->save();

                    //update inbOrdDtl array
                    foreach($inbOrdDtlArray as $inbOrdDtlData)
                    {
                        $uuid = '';
                        $inbOrdDtlModel = null;
                        if (strpos($inbOrdDtlData['id'], 'NEW') !== false) 
                        {
                            $uuid = $inbOrdDtlData['id'];
                            unset($inbOrdDtlData['id']);
                            $inbOrdDtlModel = new InbOrdDtl;
                        }
                        else
                        {
                            $inbOrdDtlModel = InbOrdDtl::lockForUpdate()->find($inbOrdDtlModel['id']);
                        }
                        
                        $inbOrdDtlModel = RepositoryUtils::dataToModel($inbOrdDtlModel, $inbOrdDtlData);
                        $inbOrdDtlModel->hdr_id = $inbOrdHdrModel->id;
                        $inbOrdDtlModel->save();

                        if(!empty($uuid))
                        {
                            $inbOrdHdrIdHashByUuid[$uuid] = $inbOrdDtlModel->hdr_id;
                            $inbOrdDtlIdHashByUuid[$uuid] = $inbOrdDtlModel->id;
                        }
                    }

                    if(!empty($delInbOrdDtlIds))
                    {
                        InbOrdDtl::destroy($delInbOrdDtlIds);
                    }
                }

                //update slsRtnHdr
                $slsRtnHdrModel = SlsRtnHdr::lockForUpdate()->find($slsRtnHdrData['id']);
                if($slsRtnHdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('SlsRtn.doc_status_is_complete', ['docCode'=>$slsRtnHdrModel->doc_code]));
                    $exc->addData(\App\SlsRtnHdr::class, $slsRtnHdrModel->id);
                    throw $exc;
                }
                $slsRtnHdrModel = RepositoryUtils::dataToModel($slsRtnHdrModel, $slsRtnHdrData);
                $slsRtnHdrModel->save();

                //update slsRtnDtl array
                $slsRtnDtlModels = array();
                foreach($slsRtnDtlArray as $slsRtnDtlData)
                {
                    $slsRtnDtlModel = null;
                    if (strpos($slsRtnDtlData['id'], 'NEW') !== false) 
                    {
                        $uuid = $slsRtnDtlData['id'];
                        $slsRtnDtlModel = new SlsRtnDtl;
                        if(array_key_exists($uuid, $inbOrdHdrIdHashByUuid))
                        {
                            $slsRtnDtlModel->inb_ord_hdr_id = $inbOrdHdrIdHashByUuid[$uuid];
                        }
                        if(array_key_exists($uuid, $inbOrdDtlIdHashByUuid))
                        {
                            $slsRtnDtlModel->inb_ord_dtl_id = $inbOrdDtlIdHashByUuid[$uuid];
                        }
                        unset($slsRtnDtlData['id']);
                    }
                    else
                    {
                        $slsRtnDtlModel = SlsRtnDtl::lockForUpdate()->find($slsRtnDtlData['id']);
                    }
                    
                    $slsRtnDtlModel = RepositoryUtils::dataToModel($slsRtnDtlModel, $slsRtnDtlData);
                    $slsRtnDtlModel->hdr_id = $slsRtnHdrModel->id;
                    $slsRtnDtlModel->save();
                    $slsRtnDtlModels[] = $slsRtnDtlModel;
                }

                if(!empty($delSlsRtnDtlIds))
                {
                    SlsRtnDtl::destroy($delSlsRtnDtlIds);
                }

                return array(
                    'document_header' => $slsRtnHdrModel,
                    'document_details' => $slsRtnDtlModels
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function findAll($divisionId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $leftJoinHash = array();
        $slsRtnHdrs = SlsRtnHdr::select('sls_rtn_hdrs.*')
            ->where('division_id', $divisionId);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $slsRtnHdrs->where('sls_rtn_hdrs.doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $slsRtnHdrs->where(function($q) use($filter) {
                    $q->where('delivery_points.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('delivery_points.company_name_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'salesman') == 0)
            {
                $leftJoinHash['users'] = 'users';
                $slsRtnHdrs->where('users.username', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point_area') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $leftJoinHash['areas'] = 'areas';
                $slsRtnHdrs->where(function($q) use($filter) {
                    $q->where('areas.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('areas.desc_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'status') == 0)
            {
                if($filter['value'] > 0)
                {
                    $slsRtnHdrs->where('sls_rtn_hdrs.doc_status', $filter['value']);
                }
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $slsRtnHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'delivery_points') == 0)
            {
                $slsInvHdrs->leftJoin('delivery_points', 'delivery_points.id', '=', 'sls_rtn_hdrs.delivery_point_id');
            }
            if(strcmp($field, 'users') == 0)
            {
                $slsInvHdrs->leftJoin('users', 'users.id', '=', 'sls_rtn_hdrs.salesman_id');
            }
            if(strcmp($field, 'areas') == 0)
            {
                $slsInvHdrs->leftJoin('areas', 'areas.id', '=', 'delivery_points.area_id');
            }
        }
        
        if($pageSize > 0)
        {
            return $slsRtnHdrs
                ->paginate($pageSize);
        }
        else
        {
            return $slsRtnHdrs
                ->paginate(PHP_INT_MAX);
        }
    }
}