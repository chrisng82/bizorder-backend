<?php

namespace App\Repositories;

use App\PutAwayInbOrd;
use Illuminate\Support\Facades\DB;

class PutAwayInbOrdRepository 
{
    static public function findAllByInbOrdHdrId($hdrId) 
	{
        $putAwayInbOrds = PutAwayInbOrd::where('inb_ord_hdr_id', $hdrId)
            ->get();

        return $putAwayInbOrds;
    }

    static public function findAllByHdrIds($hdrIds) 
	{
        $putAwayInbOrds = PutAwayInbOrd::whereIn('hdr_id', $hdrIds)
            ->get();

        return $putAwayInbOrds;
    }
}