<?php

namespace App\Repositories;

use App\Services\Utils\RepositoryUtils;
use App\AdvShipHdr;
use App\AdvShipDtl;
use App\InbOrdHdr;
use App\Services\Env\ProcType;
use App\Services\Env\DocStatus;
use App\Repositories\CurrencyRepository;
use App\Repositories\CreditTermRepository;
use App\Repositories\UserRepository;
use App\Repositories\BizPartnerRepository;
use App\Repositories\UomRepository;
use App\Repositories\ItemRepository;
use App\Repositories\LocationRepository;
use Illuminate\Support\Facades\DB;
use App\Services\Utils\ApiException;

class AdvShipHdrRepository 
{
    static public function findAllByHdrIds($hdrIds)
    {
        $advShipHdrs = AdvShipHdr::whereIn('id', $hdrIds)
            ->get();

        return $advShipHdrs;
    }

    static public function findAllNotExistInbOrd01Txn($divisionId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $qStatuses = array(DocStatus::$MAP['DRAFT'], DocStatus::$MAP['WIP'], DocStatus::$MAP['COMPLETE']);
        $advShipHdrs = AdvShipHdr::select('adv_ship_hdrs.*')
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('doc_txn_flows')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['INB_ORD_01'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\AdvShipHdr::class)
                    ->whereRaw('doc_txn_flows.fr_doc_hdr_id = adv_ship_hdrs.id')
                    ->where('doc_txn_flows.is_closed', 1);
            })
            ->where('division_id', $divisionId);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $advShipHdrs->where('adv_ship_hdrs.doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $advShipHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        return $advShipHdrs
        ->paginate($pageSize);
    }

    static public function createHeader($procType, $docNoId, $hdrData) 
	{
        $hdrModel = DB::transaction
        (
            function() use ($procType, $docNoId, $hdrData)
            {
                $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

                $hdrModel = new AdvShipHdr;
                $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
                $hdrModel->doc_code = $newCode;
                $hdrModel->proc_type = $procType;
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];

                $hdrModel->gross_amt = 0;
                $hdrModel->gross_local_amt = 0;
                $hdrModel->disc_amt = 0;
                $hdrModel->disc_local_amt = 0;
                $hdrModel->tax_amt = 0;
                $hdrModel->tax_local_amt = 0;
                $hdrModel->is_round_adj = 0;
                $hdrModel->round_adj_amt = 0;
                $hdrModel->round_adj_local_amt = 0;
                $hdrModel->net_amt = 0;
                $hdrModel->net_local_amt = 0;
                
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function findByPk($id) 
	{
        $model = AdvShipHdr::where('id', $id)
            ->first();

        return $model;
    }

    static public function txnFindByPk($hdrId) 
	{
        $model = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdr = AdvShipHdr::where('id', $hdrId)
                    ->first();
                return $hdr;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function updateDetails($advShipHdrData, $advShipDtlArray, $delAdvShipDtlArray, $inbOrdHdrData = array(), $inbOrdDtlArray = array(), $delInbOrdDtlArray = array()) 
	{
        $delAdvShipDtlIds = array();
        foreach($delAdvShipDtlArray as $delAdvShipDtlData)
        {
            $delAdvShipDtlIds[] = $delAdvShipDtlData['id'];
        }

        $delInbOrdDtlIds = array();
        foreach($delInbOrdDtlArray as $delInbOrdDtlData)
        {
            $delInbOrdDtlIds[] = $delInbOrdDtlData['id'];
        }
        
        $result = DB::transaction
        (
            function() use ($advShipHdrData, $advShipDtlArray, $delAdvShipDtlIds, $inbOrdHdrData, $inbOrdDtlArray, $delInbOrdDtlIds)
            {
                //update inbOrdHdr
                $inbOrdHdrIdHashByUuid = array();
                $inbOrdDtlIdHashByUuid = array();
                if(!empty($inbOrdHdrData))
                {
                    $inbOrdHdrModel = InbOrdHdr::lockForUpdate()->find($inbOrdHdrData['id']);
                    if($inbOrdHdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                    {
                        //only DRAFT or above can transition to COMPLETE
                        $exc = new ApiException(__('inbOrd.doc_status_is_complete', ['docCode'=>$inbOrdHdrModel->doc_code]));
                        $exc->addData(\App\InbOrdHdr::class, $inbOrdHdrModel->id);
                        throw $exc;
                    }
                    $inbOrdHdrModel = RepositoryUtils::dataToModel($inbOrdHdrModel, $inbOrdHdrData);
                    $inbOrdHdrModel->save();

                    //update inbOrdDtl array
                    foreach($inbOrdDtlArray as $inbOrdDtlData)
                    {
                        $uuid = '';
                        $inbOrdDtlModel = null;
                        if (strpos($inbOrdDtlData['id'], 'NEW') !== false) 
                        {
                            $uuid = $inbOrdDtlData['id'];
                            unset($inbOrdDtlData['id']);
                            $inbOrdDtlModel = new InbOrdDtl;
                        }
                        else
                        {
                            $inbOrdDtlModel = InbOrdDtl::lockForUpdate()->find($inbOrdDtlModel['id']);
                        }
                        
                        $inbOrdDtlModel = RepositoryUtils::dataToModel($inbOrdDtlModel, $inbOrdDtlData);
                        $inbOrdDtlModel->hdr_id = $inbOrdHdrModel->id;
                        $inbOrdDtlModel->save();

                        if(!empty($uuid))
                        {
                            $inbOrdHdrIdHashByUuid[$uuid] = $inbOrdDtlModel->hdr_id;
                            $inbOrdDtlIdHashByUuid[$uuid] = $inbOrdDtlModel->id;
                        }
                    }

                    if(!empty($delInbOrdDtlIds))
                    {
                        InbOrdDtl::destroy($delInbOrdDtlIds);
                    }
                }

                //update advShipHdr
                $advShipHdrModel = AdvShipHdr::lockForUpdate()->find($advShipHdrData['id']);
                if($advShipHdrModel->doc_status >= DocStatus::$MAP['WIP'])
                {
                    //WIP and COMPLETE status can not be updated
                    $exc = new ApiException(__('AdvShip.doc_status_is_wip_or_complete', ['docCode'=>$advShipHdrModel->doc_code]));
                    $exc->addData(\App\AdvShipHdr::class, $advShipHdrModel->id);
                    throw $exc;
                }
                $advShipHdrModel = RepositoryUtils::dataToModel($advShipHdrModel, $advShipHdrData);
                $advShipHdrModel->save();

                //update advShipDtl array
                $advShipDtlModels = array();
                foreach($advShipDtlArray as $advShipDtlData)
                {
                    $advShipDtlModel = null;
                    if (strpos($advShipDtlData['id'], 'NEW') !== false) 
                    {
                        $uuid = $advShipDtlData['id'];
                        $advShipDtlModel = new AdvShipDtl;
                        if(array_key_exists($uuid, $inbOrdHdrIdHashByUuid))
                        {
                            $advShipDtlModel->inb_ord_hdr_id = $inbOrdHdrIdHashByUuid[$uuid];
                        }
                        if(array_key_exists($uuid, $inbOrdDtlIdHashByUuid))
                        {
                            $advShipDtlModel->inb_ord_dtl_id = $inbOrdDtlIdHashByUuid[$uuid];
                        }
                        unset($advShipDtlData['id']);
                    }
                    else
                    {
                        $advShipDtlModel = AdvShipDtl::lockForUpdate()->find($advShipDtlData['id']);
                    }
                    
                    $advShipDtlModel = RepositoryUtils::dataToModel($advShipDtlModel, $advShipDtlData);
                    $advShipDtlModel->hdr_id = $advShipHdrModel->id;
                    $advShipDtlModel->save();
                    $advShipDtlModels[] = $advShipDtlModel;
                }

                if(!empty($delAdvShipDtlIds))
                {
                    AdvShipDtl::destroy($delAdvShipDtlIds);
                }

                return array(
                    'hdrModel' => $advShipHdrModel,
                    'dtlModels' => $advShipDtlModels
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function commitToWip($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = AdvShipHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('AdvShip.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\AdvShipHdr::class, $hdrModel->id);
                    throw $exc;
                }

                $dtlModels = AdvShipDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->orderBy('line_no')
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('AdvShip.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\AdvShipHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to WIP
                $hdrModel->doc_status = DocStatus::$MAP['WIP'];
                $hdrModel->save();
                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitToComplete($hdrId, $isNonZeroBal)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId, $isNonZeroBal)
            {
                $hdrModel = AdvShipHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();
                    
                if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('AdvShip.doc_status_is_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\AdvShipHdr::class, $hdrModel->id);
                    throw $exc;
                }
                if($hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('AdvShip.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\AdvShipHdr::class, $hdrModel->id);
                    throw $exc;
                }

                $dtlModels = AdvShipDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->orderBy('line_no')
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('AdvShip.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\AdvShipHdr::class, $hdrModel->id);
                    throw $exc;
                }
        
                //1) update docStatus to COMPLETE
                $hdrModel->doc_status = DocStatus::$MAP['COMPLETE'];
                $hdrModel->save();

                return $hdrModel;              
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertCompleteToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = AdvShipHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                {
                    $exc = new ApiException(__('AdvShip.doc_status_is_not_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\AdvShipHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertWipToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = AdvShipHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['WIP'])
                {
                    $exc = new ApiException(__('AdvShip.doc_status_is_not_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\AdvShipHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function syncAdvShipSync01($company, $division, $data) 
	{
        $siteFlow = $division->siteFlow;
        
        $advShipHdrModel = DB::transaction
        (
            function() use ($siteFlow, $company, $division, $data)
            {
                $isDirty = false;
                $model = AdvShipHdr::firstOrCreate(['doc_code' => $data['doc_code']]);
                //check if this order oledi have inb_ord_hdr_id
                if($model->inb_ord_hdr_id > 0)
                {
                    //if already transfer to outOrd, then no need to do any sync
                    //but update the est_del_date, desc_01, desc_02
                    $model->est_del_date = $data['userdate_01'];
                    $model->desc_01 = $data['remark_01'];
                    $model->desc_02 = $data['remark_02'];
                    $model->ref_code_01 = $data['userfield_03']; //Ref No
                    $model->ref_code_02 = $data['ref_code']; //Invoice No
                    $model->ref_code_03 = $data['userfield_02']; //P/O No
                    $model->ref_code_04 = $data['userfield_04']; //Container No
                    $model->ref_code_05 = $data['remark_01']; //Seal No
                    $model->ref_code_06 = $data['remark_02']; //Vessel
                    $model->save();
                    
                    $inbOrdHdr = InbOrdHdr::find($model->inb_ord_hdr_id);
                    $inbOrdHdr->est_del_date = $model->est_del_date;
                    $inbOrdHdr->desc_01 = $model->desc_01;
                    $inbOrdHdr->desc_02 = $model->desc_02;
                    $inbOrdHdr->save();
                    return $model;
                }
                
                $model->proc_type = ProcType::$MAP['ADV_SHIP_SYNC_01'];
                if($model->doc_status == DocStatus::$MAP['NULL'])
                {
                    //initialize with DRAFT status
                    $model->doc_status = DocStatus::$MAP['DRAFT'];
                }   
                $model->doc_date = $data['doc_date'];
                $model->est_del_date = $data['userdate_01'];
                $model->desc_01 = $data['remark_01'];
                $model->desc_02 = $data['remark_02'];
                $model->ref_code_01 = $data['userfield_03']; //Ref No
                $model->ref_code_02 = $data['ref_code']; //Invoice No
                $model->ref_code_03 = $data['userfield_02']; //P/O No
                $model->ref_code_04 = $data['userfield_04']; //Container No
                $model->ref_code_05 = $data['remark_01']; //Seal No
                $model->ref_code_06 = $data['remark_02']; //Vessel

                $model->division_id = $division->id;
                $model->site_flow_id = $division->site_flow_id;
                $model->company_id = $company->id;

                //currency
                $model->currency_id = 1;
                $model->currency_rate = $data['currency_rate'];
                $currency = CurrencyRepository::syncSlsOrdSync01($data);
                if(!empty($currency))
                {
                    $model->currency_id = $currency->id;
                }

                //creditTerm
                $model->credit_term_id = 0;
                $creditTerm = CreditTermRepository::syncSlsOrdSync01($data);
                if(!empty($creditTerm))
                {
                    $model->credit_term_id = $creditTerm->id;
                }

                //purchaser
                $purchaser = UserRepository::syncAdvShipSync01($division, $data);
                $model->purchaser_id = $purchaser->id;

                //biz partner                
                $bizPartner = BizPartnerRepository::syncAdvShipSync01($division, $data);
                $model->biz_partner_id = $bizPartner->id;

                $model->gross_amt = $data['order_amt'];
                $model->gross_local_amt = $data['order_local_amt'];
                $model->disc_amt = $data['disc_amt'];
                $model->disc_local_amt = $data['disc_local_amt'];
                $model->tax_amt = $data['tax_amt'];
                $model->tax_local_amt = $data['tax_local_amt'];
                $model->is_round_adj = 0;
                $model->round_adj_amt = 0;
                $model->round_adj_local_amt = 0;
                $model->net_amt = $data['net_amt'];
                $model->net_local_amt = $data['net_local_amt'];

                $advShipDtls = AdvShipDtl::where('hdr_id', $model->id)
                                ->get();
                $advShipDtlsHashByLineNo = array();
                $updatedAdvShipDtls = array();
                $deletedAdvShipDtls = array();
                foreach($advShipDtls as $advShipDtl)
                {
                    if(array_key_exists($advShipDtl->line_no, $advShipDtlsHashByLineNo))
                    {
                        $deletedAdvShipDtls[] = $advShipDtl;
                    }
                    else
                    {
                        $advShipDtlsHashByLineNo[$advShipDtl->line_no] = $advShipDtl;
                    }
                }

                $details = $data['details'];
                $delDate = $data['doc_date'];
                foreach($details as $detail)
                {
                    $delDate = $detail['del_date'];

                    $advShipDtl = null;
                    if(array_key_exists($detail['line_no'], $advShipDtlsHashByLineNo))
                    {
                        $advShipDtl = $advShipDtlsHashByLineNo[$detail['line_no']];
                        unset($advShipDtlsHashByLineNo[$detail['line_no']]);
                    }
                    else
                    {
                        $advShipDtl = new AdvShipDtl();
                        $advShipDtl->hdr_id = $model->id;
                    }

                    $advShipDtl->line_no = $detail['line_no'];

                    $location = LocationRepository::syncSlsOrdSync01($siteFlow->site_id, $detail);
                    $advShipDtl->location_id = $location->id;

                    $uom = UomRepository::syncSlsOrdSync01($division, $detail);
                    
                    $item = ItemRepository::syncSlsOrdSync01($division, $uom, $detail);
                    $advShipDtl->item_id = $item->id;
                    $advShipDtl->desc_01 = $detail['description'];
                    $advShipDtl->desc_02 = '';
                    $advShipDtl->uom_id = $uom->id;
                    $advShipDtl->uom_rate = $detail['uom_rate'];
                    $advShipDtl->sale_price = $detail['price'];
                    $advShipDtl->price_disc = 0;
                    if(bccomp($detail['list_price'], $detail['price']) > 0)
                    {
                        $advShipDtl->sale_price = $detail['list_price'];
                        $advShipDtl->price_disc = bcsub($detail['list_price'], $detail['price'], 10);
                    }
                    $advShipDtl->qty = $detail['qty'];
                    $advShipDtl->dtl_disc_perc_01 = $detail['disc_percent_01'];
                    $advShipDtl->dtl_disc_perc_02 = $detail['disc_percent_02'];
                    $advShipDtl->dtl_disc_perc_03 = $detail['disc_percent_03'];
                    $advShipDtl->dtl_disc_perc_04 = $detail['disc_percent_04'];
                    $advShipDtl->dtl_disc_amt = $detail['disc_amt'];
                    $advShipDtl->dtl_disc_local_amt = $detail['disc_local_amt'];
                    $advShipDtl->hdr_disc_perc_01 = $detail['disc_percent_05'];
                    $advShipDtl->hdr_disc_perc_02 = $detail['disc_percent_06'];
                    $advShipDtl->hdr_disc_perc_03 = $detail['disc_percent_07'];
                    $advShipDtl->hdr_disc_perc_04 = $detail['disc_percent_08'];
                    $advShipDtl->dtl_tax_perc_01 = $detail['tax_percent_01'];
                    $advShipDtl->dtl_tax_perc_02 = $detail['tax_percent_02'];
                    $advShipDtl->dtl_tax_perc_03 = $detail['tax_percent_03'];
                    $advShipDtl->dtl_tax_perc_04 = $detail['tax_percent_04'];
                    $advShipDtl->dtl_tax_amt_01 = $detail['tax_amt'];
                    $advShipDtl->dtl_tax_local_amt_01 = $detail['tax_local_amt'];
                    $advShipDtl->gross_amt = $detail['order_amt'];
                    $advShipDtl->gross_local_amt = $detail['order_local_amt'];
                    $advShipDtl->net_amt = $detail['net_amt'];
                    $advShipDtl->net_local_amt = $detail['net_local_amt'];
                    
                    if($advShipDtl->isDirty() == true)
                    {
                        $isDirty = $advShipDtl->isDirty();
                    }

                    $updatedAdvShipDtls[] = $advShipDtl;
                }
                $model->est_del_date = $delDate;

                if($model->isDirty() == true)
                {
                    $isDirty = $model->isDirty();
                }
                if(count($advShipDtlsHashByLineNo) > 0)
                {
                    $isDirty = true;
                }
                if(count($deletedAdvShipDtls) > 0)
                {
                    $isDirty = true;
                }

                if($isDirty == true)
                {
                    if($model->doc_status >= DocStatus::$MAP['WIP'])
                    {
                        //return to draft, so can update the details
                        if($model->doc_status == DocStatus::$MAP['COMPLETE'])
                        {
                            $tmpModel = self::revertCompleteToDraft($model->id);
                            $model->doc_status = $tmpModel->doc_status;
                        }
                        elseif($model->doc_status == DocStatus::$MAP['WIP'])
                        {
                            $tmpModel = self::revertWipToDraft($model->id);
                            $model->doc_status = $tmpModel->doc_status;
                        }
                    }

                    foreach($updatedAdvShipDtls as $updatedAdvShipDtl)
                    {
                        $updatedAdvShipDtl->save();
                    }
                    foreach($advShipDtlsHashByLineNo as $lineNo => $advShipDtl)
                    {
                        $advShipDtl->delete();
                    }
                    foreach($deletedAdvShipDtls as $advShipDtl)
                    {
                        $advShipDtl->delete();
                    }

                    $model->save();
                }

                if($data['status'] == 3)
                {
                    if($model->doc_status == DocStatus::$MAP['VOID'])
                    {
                        $model = self::commitVoidToDraft($model->id);
                    }

                    if($model->doc_status < DocStatus::$MAP['COMPLETE'])
                    {
                        $model = self::commitToComplete($model->id, true);
                    }
                }
                else
                {
                    if($model->doc_status == DocStatus::$MAP['COMPLETE'])
                    {
                        $model = self::revertCompleteToDraft($model->id);
                    }
                    if($model->doc_status == DocStatus::$MAP['DRAFT'])
                    {
                        $model = self::commitToWip($model->id);
                    }
                }
                    
                return $model;
            }, 
            5 //reattempt times
        );
        return $advShipHdrModel;
    }

    static public function findAll($divisionId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $leftJoinHash = array();
        $advShipHdrs = AdvShipHdr::select('adv_ship_hdrs.*')
            ->where('division_id', $divisionId);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $advShipHdrs->where('adv_ship_hdrs.doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $advShipHdrs->where(function($q) use($filter) {
                    $q->where('delivery_points.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('delivery_points.company_name_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'purchaser') == 0)
            {
                $leftJoinHash['users'] = 'users';
                $advShipHdrs->where('users.username', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point_area') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $leftJoinHash['areas'] = 'areas';
                $advShipHdrs->where(function($q) use($filter) {
                    $q->where('areas.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('areas.desc_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'status') == 0)
            {
                if($filter['value'] > 0)
                {
                    $advShipHdrs->where('adv_ship_hdrs.doc_status', $filter['value']);
                }
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $advShipHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'delivery_points') == 0)
            {
                $advShipHdrs->leftJoin('delivery_points', 'delivery_points.id', '=', 'adv_ship_hdrs.delivery_point_id');
            }
            if(strcmp($field, 'users') == 0)
            {
                $advShipHdrs->leftJoin('users', 'users.id', '=', 'adv_ship_hdrs.purchaser_id');
            }
            if(strcmp($field, 'areas') == 0)
            {
                $advShipHdrs->leftJoin('areas', 'areas.id', '=', 'delivery_points.area_id');
            }
        }
        
        if($pageSize > 0)
        {
            return $advShipHdrs
                ->paginate($pageSize);
        }
        else
        {
            return $advShipHdrs
                ->paginate(PHP_INT_MAX);
        }
    }
}