<?php

namespace App\Repositories;

use App\UserDivision;
use App\Services\Utils\ApiException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class UserDivisionRepository
{
    static public function findAllByUserId($userId) 
	{
        $models = UserDivision::select('user_divisions.*')
            ->where('user_id', $userId)
            ->get();
        return $models;
    }
}