<?php

namespace App\Repositories;

use App\ItemPurPrice;

class ItemPurPriceRepository 
{
    static public function findByItemIdAndUomId($docDate, $priceGroupId, $currencyId, $itemId, $uomId) 
	{
        $itemPurPrice = ItemPurPrice::where(array(           
            'item_id' => $itemId,
            'uom_id' => $uomId,
            'price_group_id' => $priceGroupId, 
            'currency_id' => $currencyId
            ))
            ->whereDate('valid_from', '<=', $docDate)
            ->whereDate('valid_to', '>=', $docDate)
            ->first();

        return $itemPurPrice;
    }
}