<?php

namespace App\Repositories;

use App\Device;
use App\Services\Utils\RepositoryUtils;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class DeviceRepository 
{
    static public function findByToken($fcmToken) 
	{
        $device = Device::where('fcm_token', $fcmToken)
            ->first();

        return $device;
    }

    static public function findTokensByUserId($receiver_ids) 
	{
        $tokens = Device::join('users', 'devices.user_id', '=', 'users.id')
            ->where('users.debtor_id', '=', $receiver_ids)
            ->select('fcm_token')->get()->toArray();

        return $tokens;
    }

    static public function register($fcmToken)
    {
        $model = DB::transaction
        (
            function() use ($fcmToken)
            {
                $model = new Device;
                $model->user_id = Auth::user()->id;
                $model->fcm_token = $fcmToken;
                $model->active = true;
                $model->save();

                return $model;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function unregister(Device $device)
    {
        $model = DB::transaction
        (
            function() use ($device)
            {
                $device->delete();
            }, 
            5 //reattempt times
        );
        return $model;
    }
}