<?php

namespace App\Repositories;

use App\Models\Promotion;
use App\Models\PromotionVariant;
use App\Repositories\ItemRepository;
use App\Repositories\ItemUomRepository;
use App\Repositories\ItemSalePriceRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Services\Utils\RepositoryUtils;
use App\Services\Env\PromotionType;

class PromotionVariantRepository 
{
    static public function findAllByPromotionId($promotionId) 
	{
        $variants = PromotionVariant::where('promotion_id', $promotionId)
            ->orderBy('id', 'ASC')
            ->get();

        return $variants;
    }

    static public function findAllMainByPromotionId($promotionId) 
	{
        $variants = PromotionVariant::where('promotion_id', $promotionId)
            ->where('is_main', 1)
            ->orderBy('id', 'ASC')
            ->get();

        return $variants;
    }

    static public function findAllAddOnByPromotionId($promotionId) 
	{
        $variants = PromotionVariant::where('promotion_id', $promotionId)
            ->where('is_add_on', 1)
            ->orderBy('id', 'ASC')
            ->get();

        return $variants;
    }

    static public function findEnabledByItem($promotionId, $itemId) 
	{
        $promotionVariant = PromotionVariant::enabled()
            ->where('promotion_id', $promotionId)
            ->where('item_id', $itemId)->first();

        return $promotionVariant;
    }
    
    static public function findEnabledByItemAndGroup($promotionId, $itemId) 
	{
        $promotionVariant = PromotionVariant::enabled()
            ->where('promotion_id', $promotionId)
            ->where('item_id', $itemId)->first();

        if (empty($promotionVariant)) {
            $item = ItemRepository::findByPk($itemId);
            //item group 01
            if (!empty($item->item_group_01_id)) {
                $promotionVariant = PromotionVariant::enabled()
                    ->where('promotion_id', $promotionId)
                    ->where('item_group_01_id', $item->item_group_01_id)->first();
            }
            if (empty($promotionVariant) && $item->item_group_02_id > 0) {
                $promotionVariant = PromotionVariant::enabled()
                    ->where('promotion_id', $promotionId)
                    ->where('item_group_02_id', $item->item_group_02_id)->first();
            }
            if (empty($promotionVariant) && $item->item_group_03_id > 0) {
                $promotionVariant = PromotionVariant::enabled()
                    ->where('promotion_id', $promotionId)
                    ->where('item_group_03_id', $item->item_group_03_id)->first();
            }
        }
        
        return $promotionVariant;
    }

    static public function findEnabledMainByItem($promotionId, $itemId) 
	{
        $promotionVariant = PromotionVariant::enabled()
            ->where('promotion_id', $promotionId)
            ->where('is_main', 1)
            ->where('item_id', $itemId)->first();

        return $promotionVariant;
    }

    static public function findAllEnabled($promotionId) 
	{
        $promotionVariants = PromotionVariant::enabled()
            ->where('promotion_id', $promotionId)
            ->get();

        return $promotionVariants;
    }

    static public function findAllEnabledMain($promotionId) 
	{
        $promotionVariants = PromotionVariant::enabled()
            ->where('promotion_id', $promotionId)
            ->where('is_main', 1)
            ->get();

        return $promotionVariants;
    }

    static public function findAllEnabledAddOn($promotionId) 
	{
        $promotionVariants = PromotionVariant::enabled()
            ->where('promotion_id', $promotionId)
            ->where('is_add_on', 1)
            ->get();

        return $promotionVariants;
    }

    //Updates all the new price in item variants by promotion id after changing the discount fixed price in headerform section
    static public function updateAllVariantByPromotionId($promotionId, $disc)
    {
        $promotion = Promotion::find($promotionId);
        $variants = PromotionVariant::where('promotion_id', $promotionId)->select('id')->get();

        foreach($variants as $variant) {
            $singleVariant = PromotionVariant::find($variant->id);
            if($promotion->type == PromotionType::$MAP['disc_percent']) {
                $singleVariant->disc_perc_01 = $disc;
                $singleVariant->save();
                //TODO: cater for disc_perc_02 - 04
            } else if($promotion->type == PromotionType::$MAP['disc_fixed_price']) {
                $itemUom = ItemUomRepository::findSmallestByItemId($singleVariant->item_id);
                if(!empty($itemUom)) {
                    $itemSalePrice = ItemSalePriceRepository::findByItemIdAndUomId(now(), 0, 1, $singleVariant->item_id, $itemUom->uom_id);
                    if(!empty($itemSalePrice))
                    {
                        $new_price = $itemSalePrice->sale_price - $disc;
                        $singleVariant->disc_fixed_price = $new_price;
                        $singleVariant->save();
                    }
                }
            }
        }
    }
}