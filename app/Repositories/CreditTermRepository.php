<?php

namespace App\Repositories;

use App\CreditTerm;
use App\Services\Env\CreditTermType;
use Illuminate\Support\Facades\DB;

class CreditTermRepository 
{
    static public function findRandom() 
	{
        $model = CreditTerm::orderByRaw('RAND()')
            ->first();

        return $model;
    }

    static public function findAll($sorts, $filters = array(), $pageSize = 20) 
	{
        $creditTerms = CreditTerm::select('credit_terms.*');

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'code') == 0)
            {
                $creditTerms->orderBy('code', $sort['order']);
            }
        }
        if($pageSize > 0)
        {
            return $creditTerms
            ->paginate($pageSize);
        }
        else
        {
            return $creditTerms
            ->paginate(PHP_INT_MAX);
        }
    }

    static public function findByCode($code) 
	{
        $creditTerm = CreditTerm::where('code', $code)
            ->first();

        return $creditTerm;
    }

    static public function syncSlsOrdSync01($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $attributes = array('code'=>$data['term_code']);
                $values = array(
                    'credit_term_type'=>CreditTermType::$MAP['DUE_IN_NO_OF_DAYS'],
                    'desc_01'=>$data['term_description'],
                    'desc_02'=>'',
                    'period_01'=>$data['term_day'],
                    'period_02'=>0
                );
                $model = CreditTerm::firstOrCreate($attributes, $values);

                return $model;
            }
        );
        return $model;
    }

    static public function findByPk($id) 
	{
        $creditTerm = CreditTerm::where('id', $id)
            ->first();

        return $creditTerm;
    }

    static public function select2($search, $filters, $pageSize = 10) 
	{
        $creditTerms = CreditTerm::where(function($q) use($search) {
            $q->where('code', 'LIKE', '%' . $search . '%')
            ->orWhere('desc_01', 'LIKE', '%' . $search . '%');
        });
        foreach($filters as $filter)
        {
            
        }
        return $creditTerms
            ->orderBy('code', 'ASC')
            ->paginate($pageSize);
    }
}