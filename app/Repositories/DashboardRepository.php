<?php

namespace App\Repositories;

use App\Dashboard;
use App\SlsOrdHdr;
use App\Services\Env\ResStatus;
use App\Services\Env\RetrievalMethod;
use App\Services\Env\ScanMode;
use App\Services\Env\StorageClass;
use App\Services\Utils\RepositoryUtils;
use Carbon\Carbon;
use DateTime;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use stdClass;

class DashboardRepository
{
    static public function getWeek($week)
    {
        Log::error($week);
        try {
            $model = SlsOrdHdr::select('doc_date')
                ->selectRaw("SUM(net_local_amt) as net_local_amt")
                ->orderBy('doc_date', 'ASC')
                ->groupBy('doc_date')
                ->where(DB::raw("WEEKOFYEAR(doc_date)"), (int)self::extractWeek($week))
                ->take(7)
                ->get();

            $weekArray = array(
                "0" => "Monday",
                "1" => "Tuesday",
                "2" => "Wednesday",
                "3" => "Thursday",
                "4" => "Friday",
                "5" => "Saturday",
                "6" => "Sunday",
            );
            $shortenWeekArray = array(
                "0" => "Mon",
                "1" => "Tue",
                "2" => "Wed",
                "3" => "Thu",
                "4" => "Fri",
                "5" => "Sat",
                "6" => "Sun"
            );

            if (!empty($model)) {
                $newModelArray = [];

                $j = 0;

                for ($i = 0; $i < 7; $i++) {
                    if ($weekArray[$i] == date('l', strtotime($model[$j]->doc_date))) {
                        $newModel = new stdClass();
                        $newModel->doc_date = $shortenWeekArray[$i];
                        $newModel->sales = (float)$model[$j]->net_local_amt;
                        $newModelArray[$i] = $newModel;
                        unset($newModel);
                        if ($j < count($model) - 1)
                            $j++;
                    } else {
                        $newModel = new stdClass();
                        $newModel->doc_date = $shortenWeekArray[$i];
                        $newModel->sales = 0.0;
                        $newModelArray[$i] = $newModel;
                        unset($newModel);
                    }
                }
                return $newModelArray;
            }
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    static public function getMonth($month)
    {
        try {
            $dateDetail = explode("-", self::extractMonth($month));

            $model = SlsOrdHdr::select('doc_date')
                ->selectRaw("SUM(net_local_amt) as net_local_amt")
                ->orderBy('doc_date', 'ASC')
                ->groupBy('doc_date')
                ->whereYear('doc_date', '=', (int)$dateDetail[0])
                ->whereMonth('doc_date', '=', (int)$dateDetail[1])
                ->get();

            if (!empty($model)) {
                $tmpMonth = date('m', strtotime($model[0]->doc_date));
                $tmpYear = date('y', strtotime($model[0]->doc_date));
                $dayInMonth = cal_days_in_month(CAL_GREGORIAN, $tmpMonth, $tmpYear);
                $newModelArray = [];
                for ($i = 0; $i < $dayInMonth; $i++) {
                    $dayString = $i < 9 ? "0" . ($i + 1) : ($i + 1);
                    foreach ($model as $key => $value) {
                        if ($dayString == date('d', strtotime($value->doc_date))) {
                            $newModel = new stdClass();
                            $newModel->doc_date = (string)($i + 1);
                            $newModel->sales = (float)$value->net_local_amt;
                            $newModelArray[$i] = $newModel;
                            unset($newModel);
                            break;
                        } else {
                            $newModel = new stdClass();
                            $newModel->doc_date = (string)($i + 1);
                            $newModel->sales = 0.0;
                            $newModelArray[$i] = $newModel;
                            unset($newModel);
                        }
                    }
                }
                return $newModelArray;
            }
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    static public function getYear($year)
    {
        try {
            $model = SlsOrdHdr::select(DB::raw('MONTH(doc_date) as month'))
                ->selectRaw("SUM(net_local_amt) as net_local_amt")
                ->orderBy('month', 'ASC')
                ->groupBy('month')
                ->whereYear('doc_date', '=', (int)self::extractYear($year))
                ->get();

            $monthArray = array(
                "1" => "Jan",
                "2" => "Feb",
                "3" => "Mar",
                "4" => "Apr",
                "5" => "May",
                "6" => "Jun",
                "7" => "Jul",
                "8" => "Aug",
                "9" => "Sep",
                "10" => "Oct",
                "11" => "Nov",
                "12" => "Dec"
            );

            if (!empty($model)) {
                $newModelArray = [];
                for ($i = 0; $i < 12; $i++) {
                    $month = $i + 1;
                    foreach ($model as $key => $value) {
                        if ($month == $value->month) {
                            $newModel = new stdClass();
                            $newModel->doc_date = $monthArray[$value->month];
                            $newModel->sales = (float)$value->net_local_amt;
                            $newModelArray[$i] = $newModel;
                            unset($newModel);
                            break;
                        } else {
                            $newModel = new stdClass();
                            $newModel->doc_date = $monthArray[$month];
                            $newModel->sales = 0.0;
                            $newModelArray[$i] = $newModel;
                            unset($newModel);
                        }
                    }
                }
                return $newModelArray;
            }
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    static public function summaryReport()
    {
        try {
            $now = Carbon::now();

            $model1 = SlsOrdHdr::selectRaw("SUM(net_local_amt) as sales")
                ->whereYear('doc_date', '=', (int)$now->year)
                ->whereMonth('doc_date', '=', (int)$now->month)
                ->first();

            $model2 = SlsOrdHdr::selectRaw("SUM(net_local_amt) as sales")
                ->whereYear('doc_date', '=', (int)$now->year)
                ->first();

            $model3 = SlsOrdHdr::selectRaw("SUM(net_local_amt) as sales")
                ->whereYear('doc_date', '=', (int)$now->year)
                ->whereMonth('doc_date', '=', (int)$now->month)
                ->where('doc_status', '=', 100)
                ->first();

            $model4 = SlsOrdHdr::selectRaw("SUM(net_local_amt) as sales")
                ->whereYear('doc_date', '=', (int)$now->year)
                ->whereMonth('doc_date', '=', (int)$now->month)
                ->where('doc_status', '=', 50)
                ->first();

            $model = new stdClass();
            $model->monthlySales = empty($model1->sales) ? 0 : (double)$model1->sales;
            $model->yearlySales = empty($model2->sales) ? 0 : (double)$model2->sales;
            $model->totalOrder = empty($model3->sales) ? 0 : (double)$model3->sales;
            $model->outstandingOrder = empty($model4->sales) ? 0 : (double)$model4->sales;

            return $model;
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    static function updateModel($setting)
    {
        try {
            $chartOneConfig = new stdClass();
            $chartTwoConfig = new stdClass();
            $chartThreeConfig = new stdClass();
            $newModel = new stdClass();

            $chartOneConfig = $setting['chartOneConfig'];
            $chartTwoConfig = $setting['chartTwoConfig'];
            $chartThreeConfig = $setting['chartThreeConfig'];
            $chartFourConfig = $setting['chartFourConfig'];
            $chartFiveConfig = $setting['chartFiveConfig'];
            $chartSixConfig = $setting['chartSixConfig'];

            $newModel->id = $setting['dashboardId'];
            $newModel->user_id = $setting['userId'];
            $newModel->chart_1_chart_type = $chartOneConfig['chart'];
            $newModel->chart_1_sort_by = $chartOneConfig['sortBy'];
            $newModel->chart_2_chart_type = $chartTwoConfig['chart'];
            $newModel->chart_2_sort_by = $chartTwoConfig['sortBy'];
            $newModel->chart_3_chart_type = $chartThreeConfig['chart'];
            $newModel->chart_3_sort_by = $chartThreeConfig['sortBy'];
            $newModel->chart_4_chart_type = $chartFourConfig['chart'];
            $newModel->chart_4_sort_by = $chartFourConfig['sortBy'];
            $newModel->chart_5_chart_type = $chartFiveConfig['chart'];
            $newModel->chart_5_sort_by = $chartFiveConfig['sortBy'];
            $newModel->chart_6_chart_type = $chartSixConfig['chart'];
            $newModel->chart_6_sort_by = $chartSixConfig['sortBy'];

            $result = DB::transaction(
                function () use ($newModel) {
                    //update Item
                    $model = Dashboard::lockForUpdate()->find($newModel->id);
                    $model = RepositoryUtils::dataToModel($model, $newModel);
                    $model->save();

                    return array(
                        'model' => $model
                    );
                },
                5 //reattempt times
            );
            return $result;
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    static function createModel($setting)
    {
        try {
            $chartOneConfig = new stdClass();
            $chartTwoConfig = new stdClass();
            $chartThreeConfig = new stdClass();
            $newModel = new stdClass();

            $chartOneConfig = $setting['chartOneConfig'];
            $chartTwoConfig = $setting['chartTwoConfig'];
            $chartThreeConfig = $setting['chartThreeConfig'];
            $chartFourConfig = $setting['chartFourConfig'];
            $chartFiveConfig = $setting['chartFiveConfig'];
            $chartSixConfig = $setting['chartSixConfig'];

            $newModel->user_id = $setting['userId'];
            $newModel->chart_1_chart_type = $chartOneConfig['chart'];
            $newModel->chart_1_sort_by = $chartOneConfig['sortBy'];
            $newModel->chart_2_chart_type = $chartTwoConfig['chart'];
            $newModel->chart_2_sort_by = $chartTwoConfig['sortBy'];
            $newModel->chart_3_chart_type = $chartThreeConfig['chart'];
            $newModel->chart_3_sort_by = $chartThreeConfig['sortBy'];
            $newModel->chart_4_chart_type = $chartFourConfig['chart'];
            $newModel->chart_4_sort_by = $chartFourConfig['sortBy'];
            $newModel->chart_5_chart_type = $chartFiveConfig['chart'];
            $newModel->chart_5_sort_by = $chartFiveConfig['sortBy'];
            $newModel->chart_6_chart_type = $chartSixConfig['chart'];
            $newModel->chart_6_sort_by = $chartSixConfig['sortBy'];

            $result = DB::transaction(
                function () use ($newModel) {
                    //update Item
                    $model = new Dashboard;
                    $model = RepositoryUtils::dataToModel($model, $newModel);
                    $model->save();

                    return $model;
                },
                5 //reattempt times
            );
            return $result;
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    static function initId($userId)
    {
        try {
            $model = Dashboard::select('id')
                ->where("user_id", (int)$userId)
                ->first();

            if (empty($model)) {
                $model = new stdClass();
                $model->id = -1;
            }

            return $model;
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    static function retrieveModel($id)
    {
        try {
            $result = Dashboard::all()
                ->where("id", $id)
                ->first();

            return $result;
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    static function extractWeek($date)
    {
        $week = date('W', strtotime($date));

        return $week;
    }

    static function extractMonth($date)
    {
        $year = date('Y', strtotime($date));
        $month = date('m', strtotime($date));

        return $year . '-' . $month;
    }

    static function extractYear($date)
    {
        $year = date('Y', strtotime($date));

        return $year;
    }
}
