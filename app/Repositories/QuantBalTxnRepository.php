<?php

namespace App\Repositories;

use App\QuantBalTxn;

class QuantBalTxnRepository 
{
    static public function findAllByDocDtlId($docHdrType, $docHdrId, $docDtlId, $sign) 
	{
        $quantBalTxns = QuantBalTxn::where('doc_hdr_type', $docHdrType)
            ->where('doc_hdr_id', $docHdrId)
            ->where('doc_dtl_id', $docDtlId)
            ->when($sign != 0, function($query) use ($sign) {
                return $query->where('sign', $sign);
            })
            ->get();
        return $quantBalTxns;
    }

    static public function findAllByDocHdrId($docHdrType, $docHdrId, $sign) 
	{
        $quantBalTxns = QuantBalTxn::where('doc_hdr_type', $docHdrType)
            ->where('doc_hdr_id', $docHdrId)
            ->when($sign != 0, function($query) use ($sign) {
                return $query->where('sign', $sign);
            })
            ->get();
        return $quantBalTxns;
    }

    static public function findByDocDtlId($docHdrType, $docHdrId, $docDtlId, $sign) 
	{
        $quantBalTxn = QuantBalTxn::where('doc_hdr_type', $docHdrType)
            ->where('doc_hdr_id', $docHdrId)
            ->where('doc_dtl_id', $docDtlId)
            ->when($sign != 0, function($query) use ($sign) {
                return $query->where('sign', $sign);
            })
            ->first();
        return $quantBalTxn;
    }
}