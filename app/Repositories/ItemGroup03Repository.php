<?php

namespace App\Repositories;

use App\ItemGroup03;
use App\Services\Utils\ApiException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use JD\Cloudder\Facades\Cloudder;

class ItemGroup03Repository
{
    static public function findByCode($code)
	{
        $group = ItemGroup03::where('code', $code)
        ->first();
        return $group;
    }

    static public function select2($search, $filters, $pageSize = 10)
	{
        $itemGroup03s = ItemGroup03::select('item_group03s.*')
        ->where(function($q) use($search) {
            $q->orWhere('desc_01', 'LIKE', '%' . $search . '%')
            ->orWhere('desc_02', 'LIKE', '%' . $search . '%')
            ->orWhere('code', 'LIKE', '%' . $search . '%');
        });
        if(count($filters) >= 1)
        {
            $itemGroup03s = $itemGroup03s->where($filters);
        }
        return $itemGroup03s
            ->orderBy('code', 'ASC')
            ->paginate($pageSize);
    }

    static public function select2ByDivision($divisionID, $search, $filters, $pageSize = 100)
	{
        $divisionItemGroups = DB::select( DB::raw("SELECT DISTINCT i.item_group_03_id FROM item_divisions id LEFT JOIN items i
            ON i.id = id.item_id
            WHERE id.division_id = :division_id"),
            array(
            'division_id' => $divisionID,
            ));

        //$array = json_decode(json_encode($divisionItemGroups), True);
        foreach ($divisionItemGroups as $value)
            $array[] = $value->item_group_03_id;

        $itemGroup03s = ItemGroup03::select('item_group03s.*')
        ->whereIn('id', $array)
        ->where(function($q) use($search) {
            $q->orWhere('desc_01', 'LIKE', '%' . $search . '%')
            ->orWhere('desc_02', 'LIKE', '%' . $search . '%')
            ->orWhere('code', 'LIKE', '%' . $search . '%');
        });

        if(count($filters) >= 1)
        {
            $itemGroup03s = $itemGroup03s->where($filters);
        }

        return $itemGroup03s
            ->orderBy('code', 'ASC')
            ->paginate($pageSize);
    }

    static public function findByPk($id)
    {
        return ItemGroup03::where('id', $id)->first();
    }

    static public function findAll($sorts, $filters = array(), $pageSize = 20)
	{
        $users = ItemGroup03::select('item_group03s.*');

        foreach($filters as $filter)
        {
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'code') == 0)
            {
                $users->orderBy('code', $sort['order']);
            }
            elseif(strcmp($sort['field'], 'desc_01') == 0)
            {
                $users->orderBy('desc_01', $sort['order']);
            }
            elseif(strcmp($sort['field'], 'desc_02') == 0)
            {
                $users->orderBy('desc_02', $sort['order']);
            }
        }

        if($pageSize > 0)
        {
            return $users
                ->paginate($pageSize);
        }
        else
        {
            return $users
                ->paginate(PHP_INT_MAX);
        }
    }

    static public function savePhoto($data)
	{
        $group = DB::transaction
        (
            function() use ($data)
            {
				$group = ItemGroup03::find($data['id']);
				$group->image_desc_01 = $data['desc_01'];
				$group->image_desc_02 = $data['desc_02'];
				list($width, $height) = getimagesize($data['filename']);

                Cloudder::upload($data['filename'], null);
                $public_id = Cloudder::getPublicId();
                $image_url= Cloudder::show($public_id, ["width" => $width, "height"=>$height]);

				//save to uploads directory
                $image = $data['blob'];
                $image->move(public_path("uploads"), $group->desc_01);

                $group->image_path = $image_url;
                $group->image_desc_02 = $public_id;
				$group->save();

                return $group;
            },
            5 //reattempt times
        );
        return $group;
    }

    static public function deletePhoto($id)
	{
        $group = DB::transaction
        (
            function() use ($id)
            {
				$group = ItemGroup03::find($id);

				$public_id = $group->image_desc_02;
                if(!empty($public_id))
                {
                    Cloudder::destroyImage($public_id, array());

					$group->image_desc_01 = '';
					$group->image_desc_02 = '';
					$group->image_path = '';
					$group->save();
                }

				return $group;
            },
            5 //reattempt times
        );

        return $group;
    }
}
