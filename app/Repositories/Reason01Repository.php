<?php

namespace App\Repositories;

use App\Reason01;
use App\Services\Utils\ApiException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class Reason01Repository
{
    static public function syncSlsRtnSync01($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $attributes = array('code'=>$data['reason_code']);
                $values = array('desc_01'=>$data['reason_description']);
                $model = Reason01::firstOrCreate($attributes, $values);

                return $model;
            }
        );
        return $model;
    }
}