<?php

namespace App\Repositories;

use App\DebtorDivision;
use App\Services\Utils\RepositoryUtils;
use Illuminate\Support\Facades\DB;

class DebtorDivisionRepository 
{
    static public function createModel($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = new DebtorDivision;
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return $model;
            }, 
            5 //reattempt times
        );
        return $model;
    }
}