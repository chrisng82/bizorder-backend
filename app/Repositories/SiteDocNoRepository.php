<?php

namespace App\Repositories;

use App\SiteDocNo;

class SiteDocNoRepository 
{
    static public function findSiteDocNo($siteId, $docType) 
	{
        $siteDocNo = SiteDocNo::where(array(
            'site_id' => $siteId,
            'doc_no_type' => $docType
        ))
        ->orderBy('seq_no')
        ->first();

        return $siteDocNo;
    }
}