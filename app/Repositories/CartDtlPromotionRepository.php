<?php

namespace App\Repositories;

use App\CartDtl;
use App\CartDtlPromotion;
use App\Models\Promotion;
use App\Services\Utils\RepositoryUtils;
use Illuminate\Support\Facades\DB;

class CartDtlPromotionRepository 
{
    static public function findByPk($dtlId) 
	{
        $cartDtlPromo = CartDtlPromotion::where('id', $dtlId)
            ->first();

        return $cartDtlPromo;
    }

    static public function findAllByDtlId($dtlId) 
	{
        $cartDtlPromos = CartDtlPromotion::where('cart_dtl_id', $dtlId)
            ->get();

        return $cartDtlPromos;
    }

    static public function findAllByDtlIds($dtlIds) 
	{
        $cartDtlPromos = CartDtlPromotion::whereIn('cart_dtl_id', $dtlIds)
            ->get();

        return $cartDtlPromos;
    }

    static public function findAllByIds($ids) 
	{
        $cartDtlPromos = CartDtlPromotion::whereIn('id', $ids)
            ->get();

        return $cartDtlPromos;
    }

    static public function findAllByPromotionId($promotionId) 
	{
        $cartDtlPromos = CartDtlPromotion::where('promotion_id', $promotionId)
            ->get();

        return $cartDtlPromos;
    }

    static public function createModel($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = new CartDtlPromotion;
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return $model;
            }, 
            5 //reattempt times
        );
        return $model;
    }
    
    static public function deleteAllByDtlIds($dtlIds) 
	{
        CartDtlPromotion::whereIn('cart_dtl_id', $dtlIds)->delete();
    }

    static public function deleteByPromotionId($promotionId)
    {
        DB::beginTransaction();
        try {
            $cart_dtl_array = [];
            $cart_dtl_promotion = CartDtlPromotion::where('promotion_id', $promotionId)->get();
            foreach($cart_dtl_promotion as $dtl) {
                if(in_array($dtl->cart_dtl_id, $cart_dtl_array)) {
                    $cart_dtl_array[] = $dtl->cart_dtl_id;
                }
    
                CartDtlPromotion::destroy($dtl->id);
            }
            DB::commit();
            return $cart_dtl_array;
        } catch (\Exception $e) {
            DB::rollback();
            throw new \Exception($e);
        }
    }

    static public function deleteByPromotionIdAndRule($promotiondId, $rule)
    {
        
    }
}