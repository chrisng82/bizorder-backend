<?php

namespace App\Repositories;

use App\Item;
use App\ItemBatch;
use App\Uom;
use App\QuantBal;
use App\PickListHdr;
use App\PickListDtl;
use App\PutAwayHdr;
use App\PutAwayDtl;
use App\QuantBalTxn;
use App\QuantBalRsvdTxn;
use App\QuantBalVoid;
use App\StorageBin;
use App\QuantBalAdvTxn;
use App\QuantBalConflict;
use App\Services\Env\RetrievalMethod;
use App\Services\Env\BinType;
use App\Services\Env\WhseJobType;
use App\Services\Env\LocationType;
use App\Services\Env\DocStatus;
use App\Services\Utils\ApiException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class QuantBalRepository 
{
    static public function findRandomStorage() 
	{
        $model = QuantBal::whereRaw('balance_unit_qty > 0')
            ->orderByRaw('RAND()')
            ->first();

        return $model;
    }

    static protected function convertRetrievalMethodToSortParams($retrievalMethod)
    {
        //default to be FEFO
        $sortField = 'item_batches.expiry_date';
        $sortType = 'ASC';
        if($retrievalMethod == RetrievalMethod::$MAP['FIRST_EXPIRED_FIRST_OUT']) 
        {
            $sortField = 'item_batches.expiry_date';
            $sortType = 'ASC';
        }
        elseif($retrievalMethod == RetrievalMethod::$MAP['LAST_EXPIRED_FIRST_OUT'])
        {
            $sortField = 'item_batches.expiry_date';
            $sortType = 'DESC';
        }
        elseif($retrievalMethod == RetrievalMethod::$MAP['FIRST_IN_FIRST_OUT'])
        {
            $sortField = 'item_batches.receipt_date';
            $sortType = 'ASC';
        }
        elseif($retrievalMethod == RetrievalMethod::$MAP['LAST_IN_FIRST_OUT'])
        {
            $sortField = 'item_batches.receipt_date';
            $sortType = 'DESC';
        }

        return array(
            'sort_field' => $sortField,
            'sort_type' => $sortType
        );
    }

    static public function queryStorageBinCapacity($storageBinId) 
	{
        $cubicMeter = 0;
        $grossWeight = 0;

        $quantBals = QuantBal::selectRaw('IFNULL(SUM(quant_bals.balance_unit_qty / items.case_uom_rate * case_ext_length * case_ext_width * case_ext_height / 1000000000), 0) AS cubic_meter, IFNULL(SUM(quant_bals.balance_unit_qty / items.case_uom_rate * case_gross_weight), 0) AS gross_weight')
            ->leftJoin('items', 'items.id', '=', 'quant_bals.item_id')    
            ->where('storage_bin_id', $storageBinId)
            ->whereRaw('balance_unit_qty > 0')
            ->get();
        foreach($quantBals as $quantBal)
        {
            $cubicMeter = $quantBal->cubic_meter;
            $grossWeight = $quantBal->gross_weight;
        }

        return array(
            'cubic_meter' => $cubicMeter,
            'gross_weight' => $grossWeight
        );
    }

    static public function queryHandlingUnitLatestExpiryDate($storageBinId, $handlingUnitId) 
	{
        $expiryDate = date('Y-m-d', 0);
        $quantBals = QuantBal::selectRaw('IFNULL(MAX(item_batches.expiry_date), CURDATE()) AS expiry_date')
            ->leftJoin('item_batches', 'item_batches.id', '=', 'quant_bals.item_batch_id')    
            ->where('storage_bin_id', $storageBinId)
            ->where('handling_unit_id', $handlingUnitId)
            ->whereRaw('balance_unit_qty > 0')
            ->get();

        foreach($quantBals as $quantBal)
        {
            $expiryDate = $quantBal->expiry_date;
        }

        return $expiryDate;
    }

    static public function queryHandlingUnitEarliestExpiryDate($storageBinId, $handlingUnitId) 
	{
        $expiryDate = date('Y-m-d');
        $quantBals = QuantBal::selectRaw('IFNULL(MIN(item_batches.expiry_date), CURDATE()) AS expiry_date')
            ->leftJoin('item_batches', 'item_batches.id', '=', 'quant_bals.item_batch_id')    
            ->where('storage_bin_id', $storageBinId)
            ->where('handling_unit_id', $handlingUnitId)
            ->whereRaw('balance_unit_qty > 0')
            ->get();

        foreach($quantBals as $quantBal)
        {
            $expiryDate = $quantBal->expiry_date;
        }

        return $expiryDate;
    }

    static public function queryHandlingUnitCapacity($storageBinId, $handlingUnitId) 
	{
        $cubicMeter = 0;
        $grossWeight = 0;

        $quantBals = QuantBal::selectRaw('IFNULL(SUM(quant_bals.balance_unit_qty / items.case_uom_rate * case_ext_length * case_ext_width * case_ext_height / 1000000000), 0) AS cubic_meter, IFNULL(SUM(quant_bals.balance_unit_qty / items.case_uom_rate * case_gross_weight), 0) AS gross_weight')
            ->leftJoin('items', 'items.id', '=', 'quant_bals.item_id')    
            ->where('storage_bin_id', $storageBinId)
            ->where('handling_unit_id', $handlingUnitId)
            ->whereRaw('balance_unit_qty > 0')
            ->get();
        foreach($quantBals as $quantBal)
        {
            $cubicMeter = $quantBal->cubic_meter;
            $grossWeight = $quantBal->gross_weight;
        }

        return array(
            'cubic_meter' => $cubicMeter,
            'gross_weight' => $grossWeight
        );
    }

    static public function queryAvailUnitQty($siteId, $binTypes, $locationId, $companyId, $itemId, $pickingCriterias = array()) 
	{
        $balanceUnitQty = 0;
        $reservedUnitQty = 0;
        $qtyScale = 0;
        
        $query = QuantBal::selectRaw('IFNULL(SUM(balance_unit_qty),0) as balance_unit_qty')
            ->leftJoin('storage_bins', 'storage_bins.id', '=', 'quant_bals.storage_bin_id')
            ->whereIn('storage_bins.storage_type_id', function ($query) use ($binTypes) {
                $query->select('id')
                    ->from('storage_types')
                    ->whereIn('bin_type', $binTypes);
            })
            ->where('storage_bins.site_id', $siteId)
            ->where('storage_bins.location_id', $locationId)
            ->where(array(
                'quant_bals.company_id' => $companyId,
                'quant_bals.item_id' => $itemId
            ));
        
        $leftJoinHash = array();
        foreach($pickingCriterias as $pickingCriteria)
        {
            if(strcmp($pickingCriteria->field, 'min_expiry_month') == 0)
            {
                $leftJoinHash['item_batches'] = 'item_batches';
                $query->whereRaw('(12 * (YEAR(item_batches.expiry_date) - YEAR(CURDATE())) + (MONTH(item_batches.expiry_date) - MONTH(CURDATE())) >= '.$pickingCriteria->value.')');
            }
        }
        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'item_batches') == 0)
            {
                $query->leftJoin('item_batches', 'item_batches.id', '=', 'quant_bals.item_batch_id');
            }
        }

        $quantBals = $query->get();
        foreach($quantBals as $quantBal)
        {
            $balanceUnitQty = $quantBal->balance_unit_qty;
        }

        $query = QuantBalRsvdTxn::selectRaw('IFNULL(SUM(unit_qty),0) as unit_qty, qty_scale')
            ->leftJoin('storage_bins', 'storage_bins.id', '=', 'quant_bal_rsvd_txns.storage_bin_id')
            ->whereIn('storage_bins.storage_type_id', function ($query) use ($binTypes) {
                $query->select('id')
                    ->from('storage_types')
                    ->whereIn('bin_type', $binTypes);
            })
            ->where('storage_bins.site_id', $siteId)
            ->where('storage_bins.location_id', $locationId)
            ->where(array(
                'quant_bal_rsvd_txns.company_id' => $companyId,
                'quant_bal_rsvd_txns.item_id' => $itemId
            ));
        $leftJoinHash = array();
        foreach($pickingCriterias as $pickingCriteria)
        {
            if(strcmp($pickingCriteria->field, 'min_expiry_month') == 0)
            {
                $leftJoinHash['item_batches'] = 'item_batches';
                $today = date('y-m-d');
                $query->whereRaw('(12 * (YEAR(item_batches.expiry_date) - YEAR(CURDATE())) + (MONTH(item_batches.expiry_date) - MONTH(CURDATE())) >= '.$pickingCriteria->value.')');
            }
        }
        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'item_batches') == 0)
            {
                $query->leftJoin('item_batches', 'item_batches.id', '=', 'quant_bal_rsvd_txns.item_batch_id');
            }
        }

        $quantBalRsvdTxns = $query->get();
        foreach($quantBalRsvdTxns as $quantBalRsvdTxn)
        {
            $reservedUnitQty = $quantBalRsvdTxn->unit_qty;
            $qtyScale = $quantBalRsvdTxn->qty_scale;
        }

        return bcsub($balanceUnitQty, $reservedUnitQty, $qtyScale);
    }

    static public function findByPk($id) 
	{
        $quantBal = QuantBal::where('id', $id)
            ->first();

        return $quantBal;
    }

    static public function findAllActive($siteId, $companyId, $itemId, $storageBinIds = array(), $retrievalMethod = 0) 
	{
        $sortParams = QuantBalRepository::convertRetrievalMethodToSortParams($retrievalMethod);
        $sortField = $sortParams['sort_field'];
        $sortType = $sortParams['sort_type'];

        $quantBals = QuantBal::select('quant_bals.*')
            ->when($retrievalMethod > 0, function ($query) {
                return $query->leftJoin('item_batches', 'item_batches.id', '=', 'quant_bals.item_batch_id');
            })            
            ->where(array(
                'quant_bals.site_id' => $siteId,
                'quant_bals.company_id' => $companyId,
                'quant_bals.item_id' => $itemId
            ))
            ->whereRaw('quant_bals.balance_unit_qty > 0')
            ->when(!empty($storageBinIds), function ($query) use ($storageBinIds) {
                return $query->whereIn('quant_bals.storage_bin_id', $storageBinIds);
            })
            ->when($retrievalMethod > 0, function ($query) use ($sortField, $sortType) {
                return $query->orderBy($sortField, $sortType);
            })
            ->get();

        return $quantBals;
    }

    static public function findAllActiveByPickingCriteria($rqdUnitQty, $siteId, $binTypes, $locationId, $companyId, $itemId, $retrievalMethod = 0, $pickingCriterias = array()) 
	{
        $sortParams = QuantBalRepository::convertRetrievalMethodToSortParams($retrievalMethod);
        $sortField = $sortParams['sort_field'];
        $sortType = $sortParams['sort_type'];

        $query = QuantBal::select('quant_bals.*')
            ->leftJoin('storage_bins', 'storage_bins.id', '=', 'quant_bals.storage_bin_id')
            ->whereIn('storage_bins.storage_type_id', function ($query) use ($binTypes) {
                $query->select('id')
                    ->from('storage_types')
                    ->whereIn('bin_type', $binTypes);
            })
            ->where('storage_bins.site_id', $siteId)
            ->where('storage_bins.location_id', $locationId) 
            ->where(array(
                'quant_bals.site_id' => $siteId,
                'quant_bals.company_id' => $companyId,
                'quant_bals.item_id' => $itemId
            ))
            ->whereRaw('quant_bals.balance_unit_qty > 0')
            ->when($retrievalMethod > 0, function ($q) use ($sortField, $sortType) {
                return $q->orderBy($sortField, $sortType);
            })
            //tis order by is to make sure the pallet size is closed to request size
            //so if pick face replenishment, no need have too many remaining at pick face
            ->orderByRaw('( ('.$rqdUnitQty.' - quant_bals.balance_unit_qty) * -1 ) ASC')
            ->orderBy('quant_bals.level', 'DESC');
            
        $leftJoinHash = array();
        if($retrievalMethod > 0) 
        {
            $leftJoinHash['item_batches'] = 'item_batches';
        }
        foreach($pickingCriterias as $pickingCriteria)
        {
            if(strcmp($pickingCriteria->field, 'min_expiry_month') == 0)
            {
                $leftJoinHash['item_batches'] = 'item_batches';
                $today = date('y-m-d');
                $query->whereRaw('(12 * (YEAR(item_batches.expiry_date) - YEAR(CURDATE())) + (MONTH(item_batches.expiry_date) - MONTH(CURDATE())) >= '.$pickingCriteria->value.')');
            }
        }
        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'item_batches') == 0)
            {
                $query->leftJoin('item_batches', 'item_batches.id', '=', 'quant_bals.item_batch_id');
            }
        }

        return $query->get();
    }

    static public function findOtherQuantsInPallet($thisQuantBalId, $handlingUnitId, $storageBinId)
    {
        $quantBals = QuantBal::where(array(
                'quant_bals.handling_unit_id' => $handlingUnitId,
                'quant_bals.storage_bin_id' => $storageBinId
            ))
            ->whereRaw('quant_bals.id <> '.$thisQuantBalId)
            ->whereRaw('quant_bals.balance_unit_qty > 0')
            ->get();
        return $quantBals;
    }

    static public function findAllByHandlingUnitId($handlingUnitId)
    {
        $quantBals = QuantBal::where(array(
                'quant_bals.handling_unit_id' => $handlingUnitId
            ))
            ->get();
        return $quantBals;
    }

    static public function findAllActiveByStorageBinId($storageBinId)
    {
        $quantBals = QuantBal::select('quant_bals.*')
            ->where(array(
                'quant_bals.storage_bin_id' => $storageBinId
            ))
            ->where('balance_unit_qty', '<>', 0)
            ->get();
        return $quantBals;
    }

    static public function findAllByStorageBinId($storageBinId, $backdate = '')
    {
        if(empty($backdate))
        {
            $backdate = date('Y-m-d');
        }

        $query = QuantBal::select('quant_bals.*')
        ->where(array(
            'quant_bals.storage_bin_id' => $storageBinId
        ));

        $today = date('Y-m-d');

        if(strcmp($backdate, $today) != 0)
        {
            $query->selectRaw(
                'SUM(quant_bals.balance_unit_qty) - 
                (
                    SELECT IFNULL(SUM(quant_bal_txns.sign * quant_bal_txns.unit_qty), 0) 
                    FROM quant_bal_txns
                    WHERE quant_bal_txns.doc_date > "'.$backdate.'" 
                    AND quant_bal_txns.quant_bal_id = quant_bals.id
                ) AS balance_unit_qty'
            )
            ->groupBy('quant_bals.id');
        }
        
        $models = $query->get();
        return $models;
    }

    static public function findAllActiveByStorageBinIdAndItemId($storageBinId, $itemId)
    {
        $quantBals = QuantBal::where(array(
                'quant_bals.storage_bin_id' => $storageBinId,
                'quant_bals.item_id' => $itemId,
            ))
            ->where('balance_unit_qty', '<>', 0)
            ->get();
        return $quantBals;
    }

    static public function findAllActiveByStorageBinIdAndHandlingUnitIdAndItemId($storageBinId, $handlingUnitId, $itemId)
    {
        $quantBals = QuantBal::where(array(
                'quant_bals.storage_bin_id' => $storageBinId,
                'quant_bals.handling_unit_id' => $handlingUnitId,
                'quant_bals.item_id' => $itemId,
            ))
            ->where('balance_unit_qty', '<>', 0)
            ->get();
        return $quantBals;
    }

    static public function findAllActiveByStorageBinIdAndCompanyId($storageBinId, $companyId)
    {
        $quantBals = QuantBal::select('quant_bals.*')
            ->where(array(
                'quant_bals.storage_bin_id' => $storageBinId,
                'quant_bals.company_id' => $companyId
            ))
            ->where('balance_unit_qty', '<>', 0)
            ->get();

        return $quantBals;
    }

    static public function findAllByStorageBinIdAndCompanyId($storageBinId, $companyId, $backdate = '')
    {
        if(empty($backdate))
        {
            $backdate = date('Y-m-d');
        }

        $query = QuantBal::select('quant_bals.*')
        ->where(array(
            'quant_bals.storage_bin_id' => $storageBinId,
            'quant_bals.company_id' => $companyId
        ))
        ->where('balance_unit_qty', '<>', 0);

        $today = date('Y-m-d');

        if(strcmp($backdate, $today) != 0)
        {
            $query->selectRaw(
                'SUM(quant_bals.balance_unit_qty) - 
                (
                    SELECT IFNULL(SUM(quant_bal_txns.sign * quant_bal_txns.unit_qty), 0) 
                    FROM quant_bal_txns
                    WHERE quant_bal_txns.doc_date > "'.$backdate.'" 
                    AND quant_bal_txns.quant_bal_id = quant_bals.id
                ) AS balance_unit_qty'
            )
            ->groupBy('quant_bals.id');
        }

        return $query->get();
    }

    static public function findByHandlingUnitId($handlingUnitId)
    {
        $quantBal = QuantBal::where(array(
                'quant_bals.handling_unit_id' => $handlingUnitId
            ))
            ->first();
        return $quantBal;
    }

    static public function findActiveByStorageBinIdAndHandlingUnitId($storageBinId, $handlingUnitId)
    {
        $quantBal = QuantBal::where(array(
                'quant_bals.storage_bin_id' => $storageBinId,
                'quant_bals.handling_unit_id' => $handlingUnitId
            ))
            ->whereRaw('quant_bals.balance_unit_qty > 0')
            ->first();
        return $quantBal;
    }

    static public function pickListDtlReserve($whseJobType, $pickListDtlId, $pickListHdr, $item, $quantBalId, $pickUnitQty) 
	{
        $isSuccess = DB::transaction
        (
            function() use ($whseJobType, $pickListDtlId, $pickListHdr, $item, $quantBalId, $pickUnitQty)
            {
                $pickListDtl = PickListDtl::where('id', $pickListDtlId)
                    ->lockForUpdate()
                    ->first();

                if($pickListDtl->quantBalId > 0)
                {
                    //this pickListDtl already assigned quantBal
                    return true;
                }

                //use lock to block racing condition, make sure the availUnitQty is accurate
                $quantBal = QuantBal::where('id', $quantBalId)
                    ->lockForUpdate()
                    ->first();

                $reservedUnitQty = 0;
                $quantBalRsvdTxn = QuantBalRsvdTxn::selectRaw('SUM(sign * unit_qty) as unit_qty')
                    ->where('quant_bal_id', $quantBal->id)
                    ->first();
                if(!empty($quantBalRsvdTxn))
                {
                    $reservedUnitQty = $quantBalRsvdTxn->unit_qty;
                }
                $availUnitQty = bcsub($quantBal->balance_unit_qty, $reservedUnitQty, $item->qty_scale);
                if(bccomp($pickUnitQty, $availUnitQty, $item->qty_scale) > 0)
                {
                    return false;
                }

                if($whseJobType == WhseJobType::$MAP['PICK_FULL_PALLET'])
                {
                    //if full pallet, need to make sure this pallet do not have other reservation
                    if(bccomp($reservedUnitQty, 0, $item->qty_scale) > 0)
                    {
                        return false;
                    }
                    //if full pallet, then is move with handlingUnit
                    $pickListDtl->to_handling_unit_id = $quantBal->handling_unit_id;
                }
                else
                {
                    $pickListDtl->to_handling_unit_id = 0;
                }
                $pickListDtl->storage_bin_id = $quantBal->storage_bin_id;
                $pickListDtl->quant_bal_id = $quantBal->id;
                $pickListDtl->whse_job_type = $whseJobType;
                $pickListDtl->save();

                $rsvdTxnModel = new QuantBalRsvdTxn;
                $rsvdTxnModel->posted_at = Carbon::now();
                $rsvdTxnModel->doc_date = $pickListHdr->doc_date;
                $rsvdTxnModel->sign =  1; //1 for add reservedQty
                $rsvdTxnModel->uom_id = $pickListDtl->uom_id;
                $rsvdTxnModel->uom_rate = $pickListDtl->uom_rate;
                $rsvdTxnModel->qty_scale = $item->qty_scale;
                $rsvdTxnModel->unit_qty = bcmul($pickListDtl->qty, $pickListDtl->uom_rate, $item->qty_scale);
                $rsvdTxnModel->doc_hdr_type = \App\PickListHdr::class;
                $rsvdTxnModel->doc_hdr_id = $pickListHdr->id;
                $rsvdTxnModel->doc_dtl_id = $pickListDtl->id;
                $rsvdTxnModel->quant_bal_id = $quantBal->id;
                $rsvdTxnModel->storage_bin_id = $quantBal->storage_bin_id;
                $rsvdTxnModel->site_id = $quantBal->site_id;
                $rsvdTxnModel->company_id = $quantBal->company_id;
                $rsvdTxnModel->item_id = $quantBal->item_id;
                $rsvdTxnModel->item_batch_id = $quantBal->item_batch_id;
                $rsvdTxnModel->handling_unit_id = $quantBal->handling_unit_id;
                $rsvdTxnModel->save();
                return true;
            }, 
            5 //reattempt times
        );
        return $isSuccess;
    }

    static public function putAwayDtlAssign($siteId, $whseJobType, $putAwayDtlId, $putAwayHdr, $item, $toHandlingUnit, $toStorageBinId, $huMaxCount) 
	{
        $isSuccess = DB::transaction
        (
            function() use ($siteId, $whseJobType, $putAwayDtlId, $putAwayHdr, $item, $toHandlingUnit, $toStorageBinId, $huMaxCount)
            {
                $putAwayDtl = PutAwayDtl::where('id', $putAwayDtlId)
                    ->lockForUpdate()
                    ->first();

                if($putAwayDtl->to_storage_bin_id > 0)
                {
                    //this putAwayDtl already assigned storage bin
                    return true;
                }

                //lock the to StorageBin, to block the racing condition
                $toStorageBin = StorageBin::where('id', $toStorageBinId)
                    ->lockForUpdate()
                    ->first();

                if($huMaxCount > 0)
                {
                    //if huMaxCount is set, then need to do pallet count checking
                    $binHandlingUnitCount = 0;
                    $huQuantBal = QuantBal::selectRaw('COUNT(DISTINCT handling_unit_id) as handling_unit_count')
                        ->where('storage_bin_id', $toStorageBin->id)
                        ->whereRaw('handling_unit_id > 0')
                        ->whereRaw('balance_unit_qty > 0')
                        ->groupBy('storage_bin_id')
                        ->first();
                    if(!empty($huQuantBal))
                    {
                        $binHandlingUnitCount = $huQuantBal->handling_unit_count;
                    }
                    if(bccomp($binHandlingUnitCount, $huMaxCount, 0) >= 0)
                    {
                        //this bin if the capacity is full
                        return false;
                    }

                    $advHandlingUnitCount = 0;
                    $huQuantBalAdvTxn = QuantBalAdvTxn::selectRaw('COUNT(DISTINCT handling_unit_id) as handling_unit_count')
                        ->where('storage_bin_id', $toStorageBin->id)
                        ->whereRaw('handling_unit_id > 0')
                        ->whereRaw('unit_qty > 0')
                        ->groupBy('storage_bin_id')
                        ->first();
                    if(!empty($huQuantBalAdvTxn))
                    {
                        $advHandlingUnitCount = $huQuantBalAdvTxn->handling_unit_count;
                    }

                    $storageBinHuTtlCount = bcadd($binHandlingUnitCount, $advHandlingUnitCount, 0);
                    if(bccomp($storageBinHuTtlCount, $huMaxCount, 0) >= 0)
                    {
                        //this bin if the capacity is full
                        return false;
                    }
                }

                $frQuantBal = QuantBal::where('id', $putAwayDtl->quant_bal_id)
                    ->lockForUpdate()
                    ->first();

                $putAwayDtl->to_handling_unit_id = $toHandlingUnit->id;
                $putAwayDtl->to_storage_bin_id = $toStorageBin->id;
                $putAwayDtl->whse_job_type = $whseJobType;
                $putAwayDtl->save();

                //check the to_storage_bin_id got similar quant or not
                $toQuantBal = QuantBal::where(array(
                    'handling_unit_id'=>$putAwayDtl->to_handling_unit_id,
                    'storage_bin_id'=>$putAwayDtl->to_storage_bin_id,
                    'company_id'=>$putAwayDtl->company_id,
                    'item_id'=>$frQuantBal->item_id,
                    'item_batch_id'=>$frQuantBal->item_batch_id
                    ))
                ->lockForUpdate()
                ->first();
                if(empty($toQuantBal))
                {
                    $toQuantBal = new QuantBal;
                    $toQuantBal->storage_bin_id = $putAwayDtl->to_storage_bin_id;
                    $toQuantBal->site_id = $siteId;
                    $toQuantBal->company_id = $putAwayDtl->company_id;
                    $toQuantBal->item_id = $frQuantBal->item_id;
                    $toQuantBal->item_batch_id = $frQuantBal->item_batch_id;
                    $toQuantBal->handling_unit_id = $putAwayDtl->to_handling_unit_id;
                    $toQuantBal->balance_unit_qty = 0;
                    $toQuantBal->location_id = $toStorageBin->location_id;
                    $toQuantBal->storage_type_id = $toStorageBin->storage_type_id;
                    $toQuantBal->storage_row_id = $toStorageBin->storage_row_id;
                    $toQuantBal->storage_bay_id = $toStorageBin->storage_bay_id;
                    $toQuantBal->level = $toStorageBin->level;
                    $toQuantBal->bay_sequence = $toStorageBin->bay_sequence;
                    $toQuantBal->save();
                }

                $advTxnModel = new QuantBalAdvTxn;
                $advTxnModel->posted_at = Carbon::now();
                $advTxnModel->doc_date = $putAwayHdr->doc_date;
                $advTxnModel->sign = 1; //1 for add reservedQty
                $advTxnModel->uom_id = $putAwayDtl->uom_id;
                $advTxnModel->uom_rate = $putAwayDtl->uom_rate;
                $advTxnModel->qty_scale = $item->qty_scale;
                $advTxnModel->unit_qty = bcmul($putAwayDtl->qty, $putAwayDtl->uom_rate, $item->qty_scale);
                $advTxnModel->doc_hdr_type = \App\PutAwayHdr::class;
                $advTxnModel->doc_hdr_id = $putAwayHdr->id;
                $advTxnModel->doc_dtl_id = $putAwayDtl->id;
                $advTxnModel->quant_bal_id = $toQuantBal->id;
                $advTxnModel->storage_bin_id = $toQuantBal->storage_bin_id;
                $advTxnModel->site_id = $siteId;
                $advTxnModel->company_id = $putAwayDtl->company_id;
                $advTxnModel->item_id = $toQuantBal->item_id;
                $advTxnModel->item_batch_id = $toQuantBal->item_batch_id;
                $advTxnModel->handling_unit_id = $toQuantBal->handling_unit_id;
                $advTxnModel->save();

                return true;
            }, 
            5 //reattempt times
        );
        return $isSuccess;
    }

    static public function revertStockTransaction($docHdrType, $docHdrId)
	{
        $quantBalTxns = QuantBalTxn::where(array(
                'doc_hdr_type'=>$docHdrType,
                'doc_hdr_id'=>$docHdrId
            ))
            ->lockForUpdate()
            ->get();
        foreach($quantBalTxns as $quantBalTxn)
        {
            $quantBal = QuantBal::where(
                'id', $quantBalTxn->quant_bal_id
                )
                ->lockForUpdate()
                ->first();

            $latestCCData = CycleCountHdrRepository::queryLatestCompleteByStorageBinIds(array($quantBal->storage_bin_id));
            $isLocked = false;
            if(strcmp($docHdrType, \App\CountAdjHdr::class) == 0)
            {
                if(strcmp($quantBalTxn->doc_date, $latestCCData['doc_date']) < 0)
                {
                    $isLocked = true;
                }
            }
            else
            {
                if(strcmp($quantBalTxn->doc_date, $latestCCData['doc_date']) <= 0)
                {
                    $isLocked = true;
                }
            }
            
            if($isLocked == true)
            {
                $storageBinCodes = '';
                $tmpStorageBin = $quantBal->storageBin;
                if(!empty($tmpStorageBin))
                {
                    $storageBinCodes = $tmpStorageBin->code;
                }
    
                $exc = new ApiException(__('QuantBal.storage_bins_is_locked', [
                    'docCode'=>$latestCCData['doc_code'],
                    'docDate'=>$latestCCData['doc_date'],
                    'storageBinCodes'=>$storageBinCodes
                ]));
                //$exc->addData(\App\QuantBal::class, $frQuantBal->id);
                throw $exc;
            }

            //reverse the quantBalTxn on quantBal
            $quantBal->balance_unit_qty = bcsub($quantBal->balance_unit_qty, bcmul($quantBalTxn->sign, $quantBalTxn->unit_qty, $quantBalTxn->qty_scale), $quantBalTxn->qty_scale);
            $quantBal->save();

            //move to voidTxn
            $balVoidModel = new QuantBalVoid;
            $balVoidModel->id = $quantBalTxn->id;
            $balVoidModel->voided_at = Carbon::now();
            $balVoidModel->posted_at = $quantBalTxn->posted_at;
            $balVoidModel->doc_date = $quantBalTxn->doc_date;
            $balVoidModel->sign = $quantBalTxn->sign;
            $balVoidModel->uom_id = $quantBalTxn->uom_id;
            $balVoidModel->uom_rate = $quantBalTxn->uom_rate;
            $balVoidModel->qty_scale = $quantBalTxn->qty_scale;
            $balVoidModel->unit_qty = $quantBalTxn->unit_qty;
            $balVoidModel->doc_hdr_type = $quantBalTxn->doc_hdr_type;
            $balVoidModel->doc_hdr_id = $quantBalTxn->doc_hdr_id;
            $balVoidModel->doc_dtl_id = $quantBalTxn->doc_dtl_id;
            $balVoidModel->quant_bal_id = $quantBalTxn->quant_bal_id;
            $balVoidModel->storage_bin_id = $quantBalTxn->storage_bin_id;
            $balVoidModel->site_id = $quantBalTxn->site_id;
            $balVoidModel->company_id = $quantBalTxn->company_id;
            $balVoidModel->item_id = $quantBalTxn->item_id;
            $balVoidModel->item_batch_id = $quantBalTxn->item_batch_id;
            $balVoidModel->handling_unit_id = $quantBalTxn->handling_unit_id;
            $balVoidModel->save();

            //delete the txn
            $quantBalTxn->delete();
        }
    }

    static public function commitHandlingUnitTransfer($frStorageBinId, $frHandlingUnitId, $toStorageBinId, $toHandlingUnitId, $docDate, $docHdrType, $docHdrId, $docDtlId, $dtlModels = array())
	{
        $toStorageBin = StorageBin::where('id', $toStorageBinId)
            ->lockForUpdate()
            ->first();

        $frQuantBals = QuantBal::where('storage_bin_id', $frStorageBinId)
            ->where('handling_unit_id', $frHandlingUnitId)
            ->lockForUpdate()
            ->get();

        $latestCCData = CycleCountHdrRepository::queryLatestCompleteByStorageBinIds(array($toStorageBinId, $frStorageBinId));
        if(strcmp($docDate, $latestCCData['doc_date']) <= 0)
        {
            $storageBinCodes = $toStorageBin->code;
            $tmpFrStorageBin = StorageBin::where('id', $frStorageBinId)
            ->first();
            if(!empty($tmpFrStorageBin))
            {
                $storageBinCodes.= ', '.$tmpFrStorageBin->code;
            }

            $exc = new ApiException(__('QuantBal.storage_bins_is_locked', [
                'docCode'=>$latestCCData['doc_code'],
                'docDate'=>$latestCCData['doc_date'],
                'storageBinCodes'=>$storageBinCodes
            ]));
            //$exc->addData(\App\QuantBal::class, $frQuantBal->id);
            throw $exc;
        }
        
        foreach($frQuantBals as $frQuantBal)
        {
            $item = $frQuantBal->item;
            //here store the original id and balanceUnitQty of frQuantBal
            //because if frStorageBinId is different from frQuantBal storageBinId
            //need to create a new frQuantBal with exact attributes (0 balance) but at different storageBin
            $frQuantBalId = $frQuantBal->id;
            $frQuantBalUnitQty = $frQuantBal->balance_unit_qty;
            //here need to block replenish more than 1 times
            if(bccomp($frQuantBalUnitQty, 0) < 0)
            {
                $frQuantBalUnitQty = 0;
            }

            if($frStorageBinId > 0
            && $frStorageBinId != $frQuantBal->storage_bin_id)
            {
                //if frStorageBinId is not 0, and is different from frQuantBal storageBinId
                //need to create a new frQuantBal with exact attributes (0 balance) but at different storageBin
                
                //lock frStorageBin
                $frStorageBin = StorageBin::where('id', $frStorageBinId)
                    ->lockForUpdate()
                    ->first();

                $newFrQuantBal = QuantBal::where(array(
                    'handling_unit_id'=>$frQuantBal->handling_unit_id,
                    'storage_bin_id'=>$frStorageBin->id,
                    'company_id'=>$frQuantBal->company_id,
                    'item_id'=>$frQuantBal->item_id,
                    'item_batch_id'=>$frQuantBal->item_batch_id
                    ))
                    ->lockForUpdate()
                    ->first();
                if(empty($newFrQuantBal))
                {
                    //create a new frQuantBal
                    $newFrQuantBal = $frQuantBal->replicate();
                    $newFrQuantBal->storage_bin_id = $frStorageBin->id;
                    $newFrQuantBal->balance_unit_qty = 0;
                    $newFrQuantBal->location_id = $frStorageBin->location_id;
                    $newFrQuantBal->storage_type_id = $frStorageBin->storage_type_id;
                    $newFrQuantBal->storage_row_id = $frStorageBin->storage_row_id;
                    $newFrQuantBal->storage_bay_id = $frStorageBin->storage_bay_id;
                    $newFrQuantBal->level = $frStorageBin->level;
                    $newFrQuantBal->bay_sequence = $frStorageBin->bay_sequence;
                    $newFrQuantBal->save();
                }

                $newQuantBalConflict = new QuantBalConflict;
                $newQuantBalConflict->doc_hdr_type = $docHdrType;
                $newQuantBalConflict->doc_hdr_id = $docHdrId;
                $newQuantBalConflict->doc_dtl_id = $docDtlId;
                $newQuantBalConflict->old_storage_bin_id = $frQuantBal->storage_bin_id;
                $newQuantBalConflict->old_quant_bal_id = $frQuantBal->id;
                $newQuantBalConflict->new_storage_bin_id = $newFrQuantBal->storage_bin_id;
                $newQuantBalConflict->new_quant_bal_id = $newFrQuantBal->id;
                $newQuantBalConflict->resolved_by = 0;
                $newQuantBalConflict->save();

                //assign newFrQuantBal to $frQuantBal
                $frQuantBal = $newFrQuantBal;
            }

            //2) add frQuantBalTxn
            $frQuantBalTxn = new QuantBalTxn;
            $frQuantBalTxn->posted_at = Carbon::now();
            $frQuantBalTxn->doc_date = $docDate;
            $frQuantBalTxn->sign = -1;
            $frQuantBalTxn->uom_id = $item->unit_uom_id;
            $frQuantBalTxn->uom_rate = 1;
            $frQuantBalTxn->qty_scale = $item->qty_scale;
            $frQuantBalTxn->unit_qty = $frQuantBalUnitQty;
            $frQuantBalTxn->doc_hdr_type = $docHdrType;
            $frQuantBalTxn->doc_hdr_id = $docHdrId;
            $frQuantBalTxn->doc_dtl_id = $docDtlId;
            $frQuantBalTxn->quant_bal_id = $frQuantBal->id;
            $frQuantBalTxn->storage_bin_id = $frQuantBal->storage_bin_id;
            $frQuantBalTxn->site_id = $frQuantBal->site_id;
            $frQuantBalTxn->company_id = $frQuantBal->company_id;
            $frQuantBalTxn->item_id = $frQuantBal->item_id;
            $frQuantBalTxn->item_batch_id = $frQuantBal->item_batch_id;
            $frQuantBalTxn->handling_unit_id = $frQuantBal->handling_unit_id;
            $frQuantBalTxn->save();

            //3) increase the toQuantBal stock
            //check the to_storage_bin_id got similar quant or not
            $toQuantBal = QuantBal::where(array(
                'handling_unit_id'=>$toHandlingUnitId,
                'storage_bin_id'=>$toStorageBinId,
                'company_id'=>$frQuantBal->company_id,
                'item_id'=>$frQuantBal->item_id,
                'item_batch_id'=>$frQuantBal->item_batch_id
                ))
                ->lockForUpdate()
                ->first();
            if(empty($toQuantBal))
            {
                $toQuantBal = new QuantBal;
                $toQuantBal->storage_bin_id = $toStorageBinId;
                $toQuantBal->site_id = $frQuantBal->site_id;
                $toQuantBal->company_id = $frQuantBal->company_id;
                $toQuantBal->item_id = $frQuantBal->item_id;
                $toQuantBal->item_batch_id = $frQuantBal->item_batch_id;
                $toQuantBal->handling_unit_id = $toHandlingUnitId;
                $toQuantBal->balance_unit_qty = $frQuantBalUnitQty;
                $toQuantBal->location_id = $toStorageBin->location_id;
                $toQuantBal->storage_type_id = $toStorageBin->storage_type_id;
                $toQuantBal->storage_row_id = $toStorageBin->storage_row_id;
                $toQuantBal->storage_bay_id = $toStorageBin->storage_bay_id;
                $toQuantBal->level = $toStorageBin->level;
                $toQuantBal->bay_sequence = $toStorageBin->bay_sequence;
                $toQuantBal->save();
            }
            else
            {
                $toQuantBal->balance_unit_qty = bcadd($toQuantBal->balance_unit_qty, $frQuantBalUnitQty, $item->qty_scale);
                $toQuantBal->save();
            }

            //4) add toQuantBalTxn
            //increase the to_storage_bin_id quant stock;
            $toQuantBalTxn = new QuantBalTxn;
            $toQuantBalTxn->posted_at = Carbon::now();
            $toQuantBalTxn->doc_date = $docDate;
            $toQuantBalTxn->sign = 1;
            $toQuantBalTxn->uom_id = $item->unit_uom_id;
            $toQuantBalTxn->uom_rate = 1;
            $toQuantBalTxn->qty_scale = $item->qty_scale;
            $toQuantBalTxn->unit_qty = $frQuantBalUnitQty;
            $toQuantBalTxn->doc_hdr_type = $docHdrType;
            $toQuantBalTxn->doc_hdr_id = $docHdrId;
            $toQuantBalTxn->doc_dtl_id = $docDtlId;
            $toQuantBalTxn->quant_bal_id = $toQuantBal->id;
            $toQuantBalTxn->storage_bin_id = $toQuantBal->storage_bin_id;
            $toQuantBalTxn->site_id = $toQuantBal->site_id;
            $toQuantBalTxn->company_id = $toQuantBal->company_id;
            $toQuantBalTxn->item_id = $toQuantBal->item_id;
            $toQuantBalTxn->item_batch_id = $toQuantBal->item_batch_id;
            $toQuantBalTxn->handling_unit_id = $toHandlingUnitId;
            $toQuantBalTxn->save();

            //1) deduct the frQuantBal stock, set balanceUnitQty = 0
            $frQuantBal->balance_unit_qty = bcsub($frQuantBal->balance_unit_qty, $frQuantBalUnitQty, 10);
            $frQuantBal->save();

            $tmpPickListDtls = PickListDtl::where('quant_bal_id', $frQuantBal->id)
                ->where('whse_job_type', WhseJobType::$MAP['WAITING_REPLENISHMENT'])
                ->lockForUpdate()
                ->get();
            foreach($tmpPickListDtls as $tmpPickListDtl)
            {
                $tmpPickListDtl->storage_bin_id = $toQuantBal->storage_bin_id;
                $tmpPickListDtl->quant_bal_id = $toQuantBal->id;
                $tmpPickListDtl->save();
                
                $tmpQuantBalRsvdTxn = QuantBalRsvdTxn::where('doc_hdr_type', \App\PickListHdr::class)
                    ->where('doc_hdr_id', $tmpPickListDtl->hdr_id)
                    ->where('doc_dtl_id', $tmpPickListDtl->id)
                    ->lockForUpdate()
                    ->first();
                if(!empty($tmpQuantBalRsvdTxn))
                {
                    $tmpQuantBalRsvdTxn->storage_bin_id = $toQuantBal->storage_bin_id;
                    $tmpQuantBalRsvdTxn->quant_bal_id = $toQuantBal->id;
                    $tmpQuantBalRsvdTxn->save();
                }
            }

            /*
            //5) change those reserved document of this frQuantBal, and change to toQuantBal
            $tmpQuantBalRsvdTxns = QuantBalRsvdTxn::where('quant_bal_id', $frQuantBal->id)
                ->lockForUpdate()
                ->get();
            foreach($tmpQuantBalRsvdTxns as $tmpQuantBalRsvdTxn)
            {
                if($tmpQuantBalRsvdTxn->doc_hdr_type == \App\PickListHdr::class)
                {
                    $tmpPickListDtl = PickListDtl::where('id', $tmpQuantBalRsvdTxn->doc_dtl_id)
                        ->lockForUpdate()
                        ->first();
                    if($tmpPickListDtl->whse_job_type == WhseJobType::$MAP['WAITING_REPLENISHMENT'])
                    {
                        $tmpPickListDtl->storage_bin_id = $toQuantBal->storage_bin_id;
                        $tmpPickListDtl->quant_bal_id = $toQuantBal->id;
                        $tmpPickListDtl->save();

                        $tmpQuantBalRsvdTxn->storage_bin_id = $toQuantBal->storage_bin_id;
                        $tmpQuantBalRsvdTxn->quant_bal_id = $toQuantBal->id;
                        $tmpQuantBalRsvdTxn->save();
                    }
                }
            }
            */

            /*
            //5) update this document details that is waiting replenishment for this quantBal
            foreach($dtlModels as $dtlModel)
            {
                if($dtlModel->quant_bal_id == $frQuantBal->id
                && $dtlModel->whse_job_type == WhseJobType::$MAP['WAITING_REPLENISHMENT'])
                {
                    //Log::error($dtlModel->hdr_id.' '.$dtlModel->id.' old Q['.$dtlModel->quant_bal_id.'] new Q['.$toQuantBal->id.']');
                    //this is waiting replenishment detail line wait for replenish quantBal
                    //and this waiting replenishment didnt update the quantBalId
                    //it still pointing to replenish quantBal, so need to update it to quantBal
                    //that this replenish quantBal move to
                    $dtlModel->storage_bin_id = $toQuantBal->storage_bin_id;
                    $dtlModel->quant_bal_id = $toQuantBal->id;
                    $dtlModel->save();

                    $quantBalRsvdTxn = QuantBalRsvdTxn::select('quant_bal_rsvd_txns.*')
                        ->where('doc_hdr_type', get_class($dtlModel))
                        ->where('doc_hdr_id', $dtlModel->hdr_id)
                        ->where('doc_dtl_id', $dtlModel->id)
                        ->lockForUpdate()
                        ->first();
                    if(!empty($quantBalRsvdTxn))
                    {
                        $quantBalRsvdTxn->storage_bin_id = $toQuantBal->storage_bin_id;
                        $quantBalRsvdTxn->quant_bal_id = $toQuantBal->id;
                        $quantBalRsvdTxn->save();
                    }
                }
            }
            */
        }
    }

    static public function commitStockTransfer(
		$frStorageBinId,
        $isNonZeroBal, $item, 
        $frQuantBalId, $toStorageBinId, $toHandlingUnitId, 
        $docDate, $docHdrType, $docHdrId, $docDtlId, 
        $uomId, $uomRate, $qty) 
	{
        $toStorageBin = StorageBin::where('id', $toStorageBinId)
            ->lockForUpdate()
            ->first();

        //must be within transaction
        $frQuantBal = QuantBal::where('id', $frQuantBalId)
            ->lockForUpdate()
            ->first();

        $latestCCData = CycleCountHdrRepository::queryLatestCompleteByStorageBinIds(array($toStorageBinId, $frQuantBal->storage_bin_id));
        if(strcmp($docDate, $latestCCData['doc_date']) <= 0)
        {
            $storageBinCodes = $toStorageBin->code;
            $tmpFrStorageBin = $frQuantBal->storageBin;
            if(!empty($tmpFrStorageBin))
            {
                $storageBinCodes.= ', '.$tmpFrStorageBin->code;
            }

            $exc = new ApiException(__('QuantBal.storage_bins_is_locked', [
                'docCode'=>$latestCCData['doc_code'],
                'docDate'=>$latestCCData['doc_date'],
                'storageBinCodes'=>$storageBinCodes
            ]));
            //$exc->addData(\App\QuantBal::class, $frQuantBal->id);
            throw $exc;
        }

        $frStorageBin = null;
        if($frStorageBinId > 0
        && $frStorageBinId != $frQuantBal->storage_bin_id)
        {
            //if frStorageBinId is not 0, and is different from frQuantBal storageBinId
            //need to create a new frQuantBal with exact attributes (0 balance) but at different storageBin
            //lock frStorageBin
            $frStorageBin = StorageBin::where('id', $frStorageBinId)
                ->lockForUpdate()
                ->first();

            $newFrQuantBal = QuantBal::where(array(
                'handling_unit_id'=>$frQuantBal->handling_unit_id,
                'storage_bin_id'=>$frStorageBin->id,
                'company_id'=>$frQuantBal->company_id,
                'item_id'=>$frQuantBal->item_id,
                'item_batch_id'=>$frQuantBal->item_batch_id
                ))
                ->lockForUpdate()
                ->first();
            
            if(empty($newFrQuantBal))
            {
                //create a new frQuantBal
                $newFrQuantBal = $frQuantBal->replicate();
                $newFrQuantBal->storage_bin_id = $frStorageBin->id;
                $newFrQuantBal->balance_unit_qty = 0;
                $newFrQuantBal->location_id = $frStorageBin->location_id;
                $newFrQuantBal->storage_type_id = $frStorageBin->storage_type_id;
                $newFrQuantBal->storage_row_id = $frStorageBin->storage_row_id;
                $newFrQuantBal->storage_bay_id = $frStorageBin->storage_bay_id;
                $newFrQuantBal->level = $frStorageBin->level;
                $newFrQuantBal->bay_sequence = $frStorageBin->bay_sequence;
                $newFrQuantBal->save();
            }

            $newQuantBalConflict = new QuantBalConflict;
            $newQuantBalConflict->doc_hdr_type = $docHdrType;
            $newQuantBalConflict->doc_hdr_id = $docHdrId;
            $newQuantBalConflict->doc_dtl_id = $docDtlId;
            $newQuantBalConflict->old_storage_bin_id = $frQuantBal->storage_bin_id;
            $newQuantBalConflict->old_quant_bal_id = $frQuantBal->id;
            $newQuantBalConflict->new_storage_bin_id = $newFrQuantBal->storage_bin_id;
            $newQuantBalConflict->new_quant_bal_id = $newFrQuantBal->id;
            $newQuantBalConflict->resolved_by = 0;
            $newQuantBalConflict->save();

            //assign newFrQuantBal to $frQuantBal
            $frQuantBal = $newFrQuantBal;
        }

        $unitQty = bcmul($qty, $uomRate, $item->qty_scale);
        //1) deduct the frQuantBal stock
        $frQuantBal->balance_unit_qty = bcsub($frQuantBal->balance_unit_qty, $unitQty, $item->qty_scale);
        $frQuantBal->save();
        if($isNonZeroBal === true
        && bccomp($frQuantBal->balance_unit_qty, 0, $item->qty_scale) < 0)
        {
            if(empty($frStorageBin))
            {
                $frStorageBin = $frQuantBal->storageBin;
            }
            $exc = new ApiException(__('QuantBal.balance_is_not_enough', [
                'dtlId'=>$docDtlId,
                'storageBinCode'=>$frStorageBin->code,
                'itemCode'=>$item->code,
                'itemDesc01'=>$item->desc_01,
                'afBalanceUnitQty'=>$frQuantBal->balance_unit_qty,
                'reqUnitQty'=>$unitQty,
                ]));
            $exc->addData(\App\QuantBal::class, $frQuantBal->id);
            throw $exc;
        }

        //2) add frQuantBalTxn
        $frQuantBalTxn = new QuantBalTxn;
        $frQuantBalTxn->posted_at = Carbon::now();
        $frQuantBalTxn->doc_date = $docDate;
        $frQuantBalTxn->sign = -1;
        $frQuantBalTxn->uom_id = $uomId;
        $frQuantBalTxn->uom_rate = $uomRate;
        $frQuantBalTxn->qty_scale = $item->qty_scale;
        $frQuantBalTxn->unit_qty = $unitQty;
        $frQuantBalTxn->doc_hdr_type = $docHdrType;
        $frQuantBalTxn->doc_hdr_id = $docHdrId;
        $frQuantBalTxn->doc_dtl_id = $docDtlId;
        $frQuantBalTxn->quant_bal_id = $frQuantBal->id;
        $frQuantBalTxn->storage_bin_id = $frQuantBal->storage_bin_id;
        $frQuantBalTxn->site_id = $frQuantBal->site_id;
        $frQuantBalTxn->company_id = $frQuantBal->company_id;
        $frQuantBalTxn->item_id = $frQuantBal->item_id;
        $frQuantBalTxn->item_batch_id = $frQuantBal->item_batch_id;
        $frQuantBalTxn->handling_unit_id = $frQuantBal->handling_unit_id;
        $frQuantBalTxn->save();

        //3) increase the toQuantBal stock
        //check the to_storage_bin_id got similar quant or not
        $toQuantBal = QuantBal::where(array(
            'handling_unit_id'=>$toHandlingUnitId,
            'storage_bin_id'=>$toStorageBinId,
            'company_id'=>$frQuantBal->company_id,
            'item_id'=>$frQuantBal->item_id,
            'item_batch_id'=>$frQuantBal->item_batch_id
            ))
            ->lockForUpdate()
            ->first();
        if(empty($toQuantBal))
        {
            $toQuantBal = new QuantBal;
            $toQuantBal->storage_bin_id = $toStorageBinId;
            $toQuantBal->site_id = $frQuantBal->site_id;
            $toQuantBal->company_id = $frQuantBal->company_id;
            $toQuantBal->item_id = $frQuantBal->item_id;
            $toQuantBal->item_batch_id = $frQuantBal->item_batch_id;
            $toQuantBal->handling_unit_id = $toHandlingUnitId;
            $toQuantBal->balance_unit_qty = $unitQty;
            $toQuantBal->location_id = $toStorageBin->location_id;
            $toQuantBal->storage_type_id = $toStorageBin->storage_type_id;
            $toQuantBal->storage_row_id = $toStorageBin->storage_row_id;
            $toQuantBal->storage_bay_id = $toStorageBin->storage_bay_id;
            $toQuantBal->level = $toStorageBin->level;
            $toQuantBal->bay_sequence = $toStorageBin->bay_sequence;
            $toQuantBal->save();
        }
        else
        {
            $toQuantBal->balance_unit_qty = bcadd($toQuantBal->balance_unit_qty, $unitQty, $item->qty_scale);
            $toQuantBal->save();
        }

        //4) add toQuantBalTxn
        //increase the to_storage_bin_id quant stock
        $toQuantBalTxn = new QuantBalTxn;
        $toQuantBalTxn->posted_at = Carbon::now();
        $toQuantBalTxn->doc_date = $docDate;
        $toQuantBalTxn->sign = 1;
        $toQuantBalTxn->uom_id = $uomId;
        $toQuantBalTxn->uom_rate = $uomRate;
        $toQuantBalTxn->qty_scale = $item->qty_scale;
        $toQuantBalTxn->unit_qty = $unitQty;
        $toQuantBalTxn->doc_hdr_type = $docHdrType;
        $toQuantBalTxn->doc_hdr_id = $docHdrId;
        $toQuantBalTxn->doc_dtl_id = $docDtlId;
        $toQuantBalTxn->quant_bal_id = $toQuantBal->id;
        $toQuantBalTxn->storage_bin_id = $toQuantBal->storage_bin_id;
        $toQuantBalTxn->site_id = $toQuantBal->site_id;
        $toQuantBalTxn->company_id = $toQuantBal->company_id;
        $toQuantBalTxn->item_id = $toQuantBal->item_id;
        $toQuantBalTxn->item_batch_id = $toQuantBal->item_batch_id;
        $toQuantBalTxn->handling_unit_id = $toHandlingUnitId;
        $toQuantBalTxn->save();
    }

    static public function commitStockIssue($isNonZeroBal, $item, $frQuantBalId, $docDate, $docHdrType, $docHdrId, $docDtlId, $uomId, $uomRate, $qty) 
	{
        //must be within transaction
        $frQuantBal = QuantBal::where('id', $frQuantBalId)
            ->lockForUpdate()
            ->first();

        $frStorageBin = StorageBin::where('id', $frQuantBal->storage_bin_id)
            ->lockForUpdate()
            ->first();

        $latestCCData = CycleCountHdrRepository::queryLatestCompleteByStorageBinIds(array($frQuantBal->storage_bin_id));
        if(strcmp($docDate, $latestCCData['doc_date']) < 0)
        {
            $storageBinCodes = '';
            $tmpFrStorageBin = $frQuantBal->storageBin;
            if(!empty($tmpFrStorageBin))
            {
                $storageBinCodes = $tmpFrStorageBin->code;
            }

            $exc = new ApiException(__('QuantBal.storage_bins_is_locked', [
                'docCode'=>$latestCCData['doc_code'],
                'docDate'=>$latestCCData['doc_date'],
                'storageBinCodes'=>$storageBinCodes
            ]));
            //$exc->addData(\App\QuantBal::class, $frQuantBal->id);
            throw $exc;
        }

        $unitQty = bcmul($qty, $uomRate, $item->qty_scale);
        //1) deduct the frQuantBal stock
        $frQuantBal->balance_unit_qty = bcsub($frQuantBal->balance_unit_qty, $unitQty, $item->qty_scale);
        $frQuantBal->save();
        if($isNonZeroBal === true
        && bccomp($frQuantBal->balance_unit_qty, 0, $item->qty_scale) < 0)
        {
            $exc = new ApiException(__('QuantBal.balance_is_not_enough', [
                'dtlId'=>$docDtlId,
                'storageBinCode'=>$frStorageBin->code,
                'itemCode'=>$item->code,
                'itemDesc01'=>$item->desc_01,
                'afBalanceUnitQty'=>$frQuantBal->balance_unit_qty,
                'reqUnitQty'=>$unitQty,
                ]));
            $exc->addData(\App\QuantBal::class, $frQuantBal->id);
            throw $exc;
        }

        //2) add frQuantBalTxn
        $frQuantBalTxn = new QuantBalTxn;
        $frQuantBalTxn->posted_at = Carbon::now();
        $frQuantBalTxn->doc_date = $docDate;
        $frQuantBalTxn->sign = -1;
        $frQuantBalTxn->uom_id = $uomId;
        $frQuantBalTxn->uom_rate = $uomRate;
        $frQuantBalTxn->qty_scale = $item->qty_scale;
        $frQuantBalTxn->unit_qty = $unitQty;
        $frQuantBalTxn->doc_hdr_type = $docHdrType;
        $frQuantBalTxn->doc_hdr_id = $docHdrId;
        $frQuantBalTxn->doc_dtl_id = $docDtlId;
        $frQuantBalTxn->quant_bal_id = $frQuantBal->id;
        $frQuantBalTxn->storage_bin_id = $frQuantBal->storage_bin_id;
        $frQuantBalTxn->site_id = $frQuantBal->site_id;
        $frQuantBalTxn->company_id = $frQuantBal->company_id;
        $frQuantBalTxn->item_id = $frQuantBal->item_id;
        $frQuantBalTxn->item_batch_id = $frQuantBal->item_batch_id;
        $frQuantBalTxn->handling_unit_id = $toHandlingUnitId;
        $frQuantBalTxn->save();
    }

    static public function commitStockReceipt(
        $siteId, $itemId, $docDate, $docHdrType, 
        $docHdrId, $docDtlId, $companyId, 
        $uomId, $uomRate, $qty, 
        $batchSerialNo, $expiryDate, $receiptDate, 
        $toStorageBinId, $toHandlingUnitId) 
	{
        $item = Item::where('id', $itemId)
            ->lockForUpdate()
            ->first();

        $toStorageBin = StorageBin::where('id', $toStorageBinId)
            ->lockForUpdate()
            ->first();

        $latestCCData = CycleCountHdrRepository::queryLatestCompleteByStorageBinIds(array($toStorageBinId));
        if(strcmp($docDate, $latestCCData['doc_date']) <= 0)
        {
            $storageBinCodes = $toStorageBin->code;

            $exc = new ApiException(__('QuantBal.storage_bins_is_locked', [
                'docCode'=>$latestCCData['doc_code'],
                'docDate'=>$latestCCData['doc_date'],
                'storageBinCodes'=>$storageBinCodes
            ]));
            //$exc->addData(\App\QuantBal::class, $frQuantBal->id);
            throw $exc;
        }

        $unitQty = bcmul($qty, $uomRate, $item->qty_scale);
        if(bccomp($unitQty, 0, $item->qty_scale) == 0)
        {
            //if unitQty is 0, then just return
            return;
        }

        //get the itemBatchId
        $toItemBatch = ItemBatchRepository::firstOrCreate(
            $item, 
            $batchSerialNo, 
            $expiryDate, 
            $receiptDate
        );

        //3) increase the toQuantBal stock
        //check the to_storage_bin_id got similar quant or not
        $toQuantBal = QuantBal::where(array(
            'handling_unit_id'=>$toHandlingUnitId,
            'storage_bin_id'=>$toStorageBinId,
            'company_id'=>$companyId,
            'item_id'=>$item->id,
            'item_batch_id'=>$toItemBatch->id
            ))
            ->lockForUpdate()
            ->first();
        if(empty($toQuantBal))
        {
            $toQuantBal = new QuantBal;
            $toQuantBal->storage_bin_id = $toStorageBinId;
            $toQuantBal->site_id = $siteId;
            $toQuantBal->company_id = $companyId;
            $toQuantBal->item_id = $item->id;
            $toQuantBal->item_batch_id = $toItemBatch->id;
            $toQuantBal->handling_unit_id = $toHandlingUnitId;
            $toQuantBal->balance_unit_qty = $unitQty;
            $toQuantBal->location_id = $toStorageBin->location_id;
            $toQuantBal->storage_type_id = $toStorageBin->storage_type_id;
            $toQuantBal->storage_row_id = $toStorageBin->storage_row_id;
            $toQuantBal->storage_bay_id = $toStorageBin->storage_bay_id;
            $toQuantBal->level = $toStorageBin->level;
            $toQuantBal->bay_sequence = $toStorageBin->bay_sequence;
            $toQuantBal->save();
        }
        else
        {
            $toQuantBal->balance_unit_qty = bcadd($toQuantBal->balance_unit_qty, $unitQty, $item->qty_scale);
            $toQuantBal->save();
        }

        //4) add toQuantBalTxn
        //increase the to_storage_bin_id quant stock
        $toQuantBalTxn = new QuantBalTxn;
        $toQuantBalTxn->posted_at = Carbon::now();
        $toQuantBalTxn->doc_date = $docDate;
        $toQuantBalTxn->sign = 1;
        $toQuantBalTxn->uom_id = $uomId;
        $toQuantBalTxn->uom_rate = $uomRate;
        $toQuantBalTxn->qty_scale = $item->qty_scale;
        $toQuantBalTxn->unit_qty = $unitQty;
        $toQuantBalTxn->doc_hdr_type = $docHdrType;
        $toQuantBalTxn->doc_hdr_id = $docHdrId;
        $toQuantBalTxn->doc_dtl_id = $docDtlId;
        $toQuantBalTxn->quant_bal_id = $toQuantBal->id;
        $toQuantBalTxn->storage_bin_id = $toQuantBal->storage_bin_id;
        $toQuantBalTxn->site_id = $toQuantBal->site_id;
        $toQuantBalTxn->company_id = $toQuantBal->company_id;
        $toQuantBalTxn->item_id = $toQuantBal->item_id;
        $toQuantBalTxn->item_batch_id = $toQuantBal->item_batch_id;
        $toQuantBalTxn->handling_unit_id = $toHandlingUnitId;
        $toQuantBalTxn->save();
    }

    static public function commitStockAdjustment(
        $siteId, $itemId, $docDate, $docHdrType, 
        $docHdrId, $docDtlId, $companyId, 
        $uomId, $uomRate, $sign, $qty, 
        $batchSerialNo, $expiryDate, $receiptDate, 
        $storageBinId, $oriHandlingUnitId) 
	{
        $item = Item::where('id', $itemId)
            ->lockForUpdate()
            ->first();

        $storageBin = StorageBin::where('id', $storageBinId)
            ->lockForUpdate()
            ->first();

        //DB::connection()->enableQueryLog();
        $latestCCData = CycleCountHdrRepository::queryLatestCompleteByStorageBinIds(array($storageBinId));
        //Log::error(DB::getQueryLog());
        if(strcmp($docDate, $latestCCData['doc_date']) < 0)
        {
            $storageBinCodes = $storageBin->code;

            $exc = new ApiException(__('QuantBal.storage_bins_is_locked', [
                'docCode'=>$latestCCData['doc_code'],
                'docDate'=>$latestCCData['doc_date'],
                'storageBinCodes'=>$storageBinCodes
            ]));
            //$exc->addData(\App\QuantBal::class, $frQuantBal->id);
            throw $exc;
        }
        /*
        //if bin type is pick face, handlingUnitId is 0
        $handlingUnitId = 0;
        $storageType = $storageBin->storageType;
        if($storageType->isPallet() == true)
        {
            $handlingUnitId = $oriHandlingUnitId;
        }
        */
        $handlingUnitId = $oriHandlingUnitId;

        $absUnitQty = bcmul($qty, $uomRate, $item->qty_scale);
        $unitQty = bcmul($sign, $absUnitQty, $item->qty_scale);

        //get the itemBatchId
        $itemBatch = ItemBatchRepository::firstOrCreate(
            $item, 
            $batchSerialNo, 
            $expiryDate, 
            $receiptDate
        );

        //3) increase the quantBal stock
        //check the to_storage_bin_id got similar quant or not
        $quantBal = QuantBal::where(array(
            'handling_unit_id'=>$handlingUnitId,
            'storage_bin_id'=>$storageBinId,
            'company_id'=>$companyId,
            'item_id'=>$item->id,
            'item_batch_id'=>$itemBatch->id
            ))
            ->lockForUpdate()
            ->first();
        if(empty($quantBal))
        {
            $quantBal = new QuantBal;
            $quantBal->storage_bin_id = $storageBinId;
            $quantBal->site_id = $siteId;
            $quantBal->company_id = $companyId;
            $quantBal->item_id = $item->id;
            $quantBal->item_batch_id = $itemBatch->id;
            $quantBal->handling_unit_id = $handlingUnitId;
            $quantBal->balance_unit_qty = $unitQty;
            $quantBal->location_id = $storageBin->location_id;
            $quantBal->storage_type_id = $storageBin->storage_type_id;
            $quantBal->storage_row_id = $storageBin->storage_row_id;
            $quantBal->storage_bay_id = $storageBin->storage_bay_id;
            $quantBal->level = $storageBin->level;
            $quantBal->bay_sequence = $storageBin->bay_sequence;
            $quantBal->save();
        }
        else
        {
            $quantBal->balance_unit_qty = bcadd($quantBal->balance_unit_qty, $unitQty, $item->qty_scale);
            $quantBal->save();
        }

        //4) add quantBalTxn
        //increase the to_storage_bin_id quant stock
        $quantBalTxn = new QuantBalTxn;
        $quantBalTxn->posted_at = Carbon::now();
        $quantBalTxn->doc_date = $docDate;
        $quantBalTxn->sign = $sign;
        $quantBalTxn->uom_id = $uomId;
        $quantBalTxn->uom_rate = $uomRate;
        $quantBalTxn->qty_scale = $item->qty_scale;
        $quantBalTxn->unit_qty = $absUnitQty;
        $quantBalTxn->doc_hdr_type = $docHdrType;
        $quantBalTxn->doc_hdr_id = $docHdrId;
        $quantBalTxn->doc_dtl_id = $docDtlId;
        $quantBalTxn->quant_bal_id = $quantBal->id;
        $quantBalTxn->storage_bin_id = $quantBal->storage_bin_id;
        $quantBalTxn->site_id = $quantBal->site_id;
        $quantBalTxn->company_id = $quantBal->company_id;
        $quantBalTxn->item_id = $quantBal->item_id;
        $quantBalTxn->item_batch_id = $quantBal->item_batch_id;
        $quantBalTxn->handling_unit_id = $handlingUnitId;
        $quantBalTxn->save();
    }

    static public function queryHuCountBySiteId($siteId)
	{
        $storageBinData = array();
        $quantBals = QuantBal::selectRaw('storage_bin_id, COUNT(DISTINCT handling_unit_id) as handling_unit_count')
            ->where('site_id', $siteId)
            ->whereRaw('balance_unit_qty > 0')
            ->whereRaw('handling_unit_id > 0')
            ->groupBy('storage_bin_id')
            ->get();
        foreach($quantBals as $quantBal)
        {
            $data = array();
            $data['storage_bin_id'] = $quantBal->storage_bin_id;
            $data['handling_unit_count'] = $quantBal->handling_unit_count;

            $storageBinData[] = $data;
        }
        return $storageBinData;
    }

    static public function queryHuCountByStorageBinId($storageBinId)
	{
        $data = DB::transaction
        (
            function() use ($storageBinId)
            {
                $quantBal = QuantBal::selectRaw('storage_bin_id, COUNT(DISTINCT handling_unit_id) as handling_unit_count')
                ->where('storage_bin_id', $storageBinId)
                ->whereRaw('balance_unit_qty > 0')
                ->whereRaw('handling_unit_id > 0')
                ->groupBy('storage_bin_id')
                ->first();
                $data = array(
                    'storage_bin_id' => $storageBinId,
                    'handling_unit_count' => 0
                );
                if(!empty($quantBal))
                {
                    $data['storage_bin_id'] = $quantBal->storage_bin_id;
                    $data['handling_unit_count'] = $quantBal->handling_unit_count;
                }
                return $data;
            }, 
            5 //reattempt times
        );
        return $data;
    }

    static public function queryByAttributes(
        $storageBinId, $handlingUnitId,
        $companyId, $itemId, $itemBatchId,
        $batchSerialNo, $expiryDate, $receiptDate) 
	{
        if($itemBatchId > 0)
        {
            $itemBatch = ItemBatch::where(array(
                'item_id'=>$itemId,
                'expiry_date'=>$expiryDate,
                'batch_serial_no'=>$batchSerialNo,
                'receipt_date'=>$receiptDate
                ))
                ->first();
            if(!empty($itemBatch))
            {
                $itemBatchId = $itemBatch->id;
            }
        }

        $quantBal = QuantBal::where(array(
            'handling_unit_id'=>$handlingUnitId,
            'storage_bin_id'=>$storageBinId,
            'company_id'=>$companyId,
            'item_id'=>$itemId,
            'item_batch_id'=>$itemBatchId
            ))
            ->first();
        return $quantBal;
    }

    static public function verifyTxn()
    {
        $invalidRows = array();

        $page = 1;
        $limit = 100;
        while(true)
        {
            $offset = ($page - 1) * $limit;
            $quantBals = QuantBal::offset($offset)
            ->limit($limit)
            ->get();
            foreach($quantBals as $quantBal)
            {
                $ttlTxnUnitQty = 0;
                $quantBalTxns = QuantBalTxn::where('quant_bal_id', $quantBal->id)
                ->get();
                foreach($quantBalTxns as $quantBalTxn)
                {
                    $ttlUnitQty = bcmul($quantBalTxn->sign, $quantBalTxn->unit_qty, 10);
                    $ttlTxnUnitQty = bcadd($ttlTxnUnitQty, $ttlUnitQty, 10);
                }

                if(bccomp($quantBal->balance_unit_qty, $ttlTxnUnitQty, 10) != 0)
                {
                    $item = $quantBal->item;
                    $storageBin = $quantBal->storageBin;
                    $invalidRows[] = $item->code.' Item['.$item->desc_01.'] Bin['.$storageBin->code.'] QuantBal['.$quantBal->id.'] balance['.$quantBal->balance_unit_qty.'] not tally with txn['.$ttlTxnUnitQty.']';
                }
            }

            if(count($quantBals) == 0)
            {
                break;
            }

            $page++;
        }        

        return $invalidRows;
    }

    static public function findAllActiveByStorageRowId($storageRowId)
    {
        $quantBals = QuantBal::where(array(
                'quant_bals.storage_row_id' => $storageRowId
            ))
            ->where('balance_unit_qty', '<>', 0)
            ->get();
        return $quantBals;
    }

    static public function queryAvailUnitQtyBySiteIdAndLocationId($companyId, $itemId, $siteId, $siteFlowId, $locationId, $pickingCriterias = array()) 
	{
        $balanceUnitQty = 0;
        $reservedUnitQty = 0;
        $qtyScale = 0;
        
        $query = QuantBal::selectRaw('IFNULL(SUM(balance_unit_qty),0) as balance_unit_qty')
            ->where(array(
                'quant_bals.company_id' => $companyId,
                'quant_bals.item_id' => $itemId,
                'quant_bals.site_id' => $siteId,
                'quant_bals.location_id' => $locationId
            ));

        $leftJoinHash = array();
        foreach($pickingCriterias as $pickingCriteria)
        {
            if(strcmp($pickingCriteria->field, 'min_expiry_month') == 0)
            {
                $leftJoinHash['item_batches'] = 'item_batches';
                $query->whereRaw('(12 * (YEAR(item_batches.expiry_date) - YEAR(CURDATE())) + (MONTH(item_batches.expiry_date) - MONTH(CURDATE())) >= '.$pickingCriteria->value.')');
            }
        }
        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'item_batches') == 0)
            {
                $query->leftJoin('item_batches', 'item_batches.id', '=', 'quant_bals.item_batch_id');
            }
        }

        $quantBals = $query->get();
        foreach($quantBals as $quantBal)
        {
            $balanceUnitQty = $quantBal->balance_unit_qty;
        }

        /*
        $query = QuantBalRsvdTxn::selectRaw('IFNULL(SUM(unit_qty),0) as unit_qty, qty_scale')
            ->leftJoin('storage_bins', 'storage_bins.id', '=', 'quant_bal_rsvd_txns.storage_bin_id')    
            ->where(array(
                'quant_bal_rsvd_txns.company_id' => $companyId,
                'quant_bal_rsvd_txns.item_id' => $itemId,
                'storage_bins.location_id' => $locationId
            ));

        $leftJoinHash = array();
        foreach($pickingCriterias as $pickingCriteria)
        {
            if(strcmp($pickingCriteria->field, 'min_expiry_month') == 0)
            {
                $leftJoinHash['item_batches'] = 'item_batches';
                $today = date('y-m-d');
                $query->whereRaw('(12 * (YEAR(item_batches.expiry_date) - YEAR(CURDATE())) + (MONTH(item_batches.expiry_date) - MONTH(CURDATE())) >= '.$pickingCriteria->value.')');
            }
        }
        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'item_batches') == 0)
            {
                $query->leftJoin('item_batches', 'item_batches.id', '=', 'quant_bal_rsvd_txns.item_batch_id');
            }
        }
        $quantBalRsvdTxns = $query->get();
        foreach($quantBalRsvdTxns as $quantBalRsvdTxn)
        {
            $reservedUnitQty = $quantBalRsvdTxn->unit_qty;
            $qtyScale = $quantBalRsvdTxn->qty_scale;
        }
        */

        //DB::connection()->enableQueryLog();
        $qStatuses = array(DocStatus::$MAP['DRAFT'], DocStatus::$MAP['WIP']); 
        $pickListDtls = PickListDtl::selectRaw('IFNULL(SUM(qty * uom_rate),0) as unit_qty')
            ->leftJoin('pick_list_hdrs', 'pick_list_hdrs.id', '=', 'pick_list_dtls.hdr_id')
            ->where(array(
                'pick_list_dtls.company_id' => $companyId,
                'pick_list_dtls.item_id' => $itemId
            ))
            ->where('pick_list_dtls.whse_job_type', '<>', 3)
            ->where('pick_list_hdrs.site_flow_id', $siteFlowId) 
            ->where('pick_list_hdrs.location_id', $locationId)            
            ->whereIn('pick_list_hdrs.doc_status', $qStatuses)
            ->get();
        foreach($pickListDtls as $pickListDtl)
        {
            $reservedUnitQty = $pickListDtl->unit_qty;
        }
        //Log::error(DB::getQueryLog());
        
        return bcsub($balanceUnitQty, $reservedUnitQty, 10);
    }

    static public function findAllActiveByHandlingUnitId($handlingUnitId)
    {
        $quantBals = QuantBal::where(array(
                'quant_bals.handling_unit_id' => $handlingUnitId
            ))
            ->where('balance_unit_qty', '<>', 0)
            ->get();
        return $quantBals;
    }

    static public function findAllActiveByHandlingUnitIdAndStorageBinId($handlingUnitId, $storageBinId)
    {
        $quantBals = QuantBal::where(array(
                'quant_bals.handling_unit_id' => $handlingUnitId,                
                'quant_bals.storage_bin_id' => $storageBinId
            ))
            ->where('balance_unit_qty', '<>', 0)
            ->get();
        return $quantBals;
    }

    static public function queryBalanceUnitQtyBySiteId($siteId) 
	{
        $quantBals = QuantBal::selectRaw('companies.code AS company_code')
            ->selectRaw('items.code AS item_code')
            ->selectRaw('items.desc_01 AS item_desc_01')
            ->selectRaw('items.desc_02 AS item_desc_02')
            ->selectRaw('locations.code AS location_code')
            ->selectRaw('locations.location_type AS location_type')
            ->selectRaw('IFNULL(SUM(balance_unit_qty),0) as balance_unit_qty')
            ->leftJoin('companies', 'companies.id', '=', 'quant_bals.company_id')
            ->leftJoin('items', 'items.id', '=', 'quant_bals.item_id')
            ->leftJoin('storage_bins', 'storage_bins.id', '=', 'quant_bals.storage_bin_id')
            ->leftJoin('locations', 'locations.id', '=', 'storage_bins.location_id')
            ->where('quant_bals.site_id', $siteId)
            ->where('locations.location_type', LocationType::$MAP['GOOD_STOCK'])
            ->groupBy(
                'quant_bals.company_id',
                'quant_bals.item_id',
                'storage_bins.location_id'
            )
            ->get();

        return $quantBals;
    }

    static public function findAllActiveByStorageRowIdAndLevel($storageRowId, $level)
    {
        $quantBals = QuantBal::where('quant_bals.storage_row_id', $storageRowId)
            ->where('quant_bals.level', $level)
            ->where('balance_unit_qty', '<>', 0)
            ->get();
        return $quantBals;
    }

    static public function firstOrCreate($siteId, $companyId, $handlingUnitId, $item, $itemBatch, $storageBin) 
	{
        $quantBal = QuantBal::where(array(
            'handling_unit_id'=>$handlingUnitId,
            'storage_bin_id'=>$storageBin->id,
            'company_id'=>$companyId,
            'item_id'=>$item->id,
            'item_batch_id'=>$itemBatch->id
            ))
            ->lockForUpdate()
            ->first();
        if(empty($quantBal))
        {
            $quantBal = new QuantBal;
            $quantBal->storage_bin_id = $storageBin->id;
            $quantBal->site_id = $siteId;
            $quantBal->company_id = $companyId;
            $quantBal->item_id = $item->id;
            $quantBal->item_batch_id = $itemBatch->id;
            $quantBal->handling_unit_id = $handlingUnitId;
            $quantBal->balance_unit_qty = 0;
            $quantBal->location_id = $storageBin->location_id;
            $quantBal->storage_type_id = $storageBin->storage_type_id;
            $quantBal->storage_row_id = $storageBin->storage_row_id;
            $quantBal->storage_bay_id = $storageBin->storage_bay_id;
            $quantBal->level = $storageBin->level;
            $quantBal->bay_sequence = $storageBin->bay_sequence;
            $quantBal->save();
        }
        return $quantBal;
    }

    static public function select2($search, $filters, $pageSize = 10) 
	{
        $search = trim($search);
        $quantBals = QuantBal::select('quant_bals.*')
        ->leftJoin('items', 'items.id', '=', 'quant_bals.item_id')
        ->leftJoin('item_batches', 'item_batches.id', '=', 'quant_bals.item_batch_id')
        ->where(function($q) use($search) {
            $q->orWhere('quant_bals.handling_unit_id', 'LIKE', '%' . $search . '%')
            ->orWhere('items.code', 'LIKE', '%' . $search . '%')
            ->orWhere('items.desc_01', 'LIKE', '%' . $search . '%')
            ->orWhere('items.desc_02', 'LIKE', '%' . $search . '%')
            ->orWhere('item_batches.expiry_date', 'LIKE', '%' . $search . '%')
            ->orWhere('item_batches.batch_serial_no', 'LIKE', '%' . $search . '%');
        });
        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'company_id') == 0)
            {
                $operator = '=';
                if(isset($filter['operator']))
                {
                    $operator = $filter['operator'];
                }
                $quantBals->where('quant_bals.company_id', $operator, $filter['value']);
            }
            if(strcmp($filter['field'], 'handling_unit_id') == 0)
            {
                $operator = '=';
                if(isset($filter['operator']))
                {
                    $operator = $filter['operator'];
                }
                $quantBals->where('quant_bals.handling_unit_id', $operator, $filter['value']);
            }
            if(strcmp($filter['field'], 'storage_bin_id') == 0)
            {
                $operator = '=';
                if(isset($filter['operator']))
                {
                    $operator = $filter['operator'];
                }
                $quantBals->where('quant_bals.storage_bin_id', $operator, $filter['value']);
            }
            if(strcmp($filter['field'], 'item_id') == 0)
            {
                $operator = '=';
                if(isset($filter['operator']))
                {
                    $operator = $filter['operator'];
                }
                $quantBals->where('quant_bals.item_id', $operator, $filter['value']);
            }
        }
        return $quantBals
            ->orderBy('item_batches.expiry_date', 'DESC')
            ->orderBy('handling_unit_id', 'DESC')
            ->orderBy('balance_unit_qty', 'DESC')
            ->paginate($pageSize);
    }

    static public function findTop($filters) 
	{
        $model = QuantBal::select('quant_bals.*');

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'storage_bin_id') == 0)
            {
                $model->where('quant_bals.storage_bin_id', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'item_id') == 0)
            {
                $model->where('quant_bals.item_id', 'LIKE', '%'.$filter['value'].'%');
            }
        }

        return $model
            ->orderBy('handling_unit_id', 'DESC')
            ->orderBy('balance_unit_qty', 'DESC')
            ->first();
    }

    static public function select2GroupByHandlingUnitId($search, $filters, $pageSize = 10) 
	{
        $quantBals = QuantBal::select('quant_bals.*')
        ->leftJoin('item_batches', 'item_batches.id', '=', 'quant_bals.item_batch_id')
        ->where('handling_unit_id', '>', 0)
        ->where(function($q) use($search) {
            $q->orWhere('quant_bals.handling_unit_id', 'LIKE', '%' . $search . '%')
            ->orWhere('item_batches.expiry_date', 'LIKE', '%' . $search . '%')
            ->orWhere('item_batches.batch_serial_no', 'LIKE', '%' . $search . '%');
        })
        ->groupBy('handling_unit_id');
        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'storage_bin_id') == 0)
            {
                $quantBals->where('quant_bals.storage_bin_id', '=', $filter['value']);
            }
            if(strcmp($filter['field'], 'item_id') == 0)
            {
                $quantBals->where('quant_bals.item_id', '=', $filter['value']);
            }
        }
        return $quantBals
            ->orderBy('balance_unit_qty', 'DESC')
            ->paginate($pageSize);
    }

    static public function findTopGroupByHandlingUnitId($filters) 
	{
        $model = QuantBal::select('quant_bals.*')
            ->where('handling_unit_id', '>', 0)
            ->groupBy('handling_unit_id');

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'storage_bin_id') == 0)
            {
                $model->where('quant_bals.storage_bin_id', '=', $filter['value']);
            }
            if(strcmp($filter['field'], 'item_id') == 0)
            {
                $model->where('quant_bals.item_id', '=', $filter['value']);
            }
        }

        return $model
            ->orderBy('balance_unit_qty', 'DESC')
            ->first();
    }

    static public function findAllByHandlingUnitIdAndStorageBinId($handlingUnitId, $storageBinId)
    {
        $quantBals = QuantBal::where(array(
                'quant_bals.handling_unit_id' => $handlingUnitId,                
                'quant_bals.storage_bin_id' => $storageBinId
            ))
            ->get();
        return $quantBals;
    }

    static public function findAllActiveByPickFaceAndPallet($siteId, $companyId, $itemId) 
	{
        $binTypes = array(
            BinType::$MAP['BULK_STORAGE'],
            BinType::$MAP['PALLET_STORAGE'],
            BinType::$MAP['BROKEN_PALLET_STORAGE'],
            BinType::$MAP['CASE_STORAGE'],
            BinType::$MAP['BROKEN_CASE_STORAGE']
        );
        $quantBals = QuantBal::select('quant_bals.*')
            ->leftJoin('storage_types', 'storage_types.id', '=', 'quant_bals.storage_type_id')        
            ->where(array(
                'quant_bals.site_id' => $siteId,
                'quant_bals.company_id' => $companyId,
                'quant_bals.item_id' => $itemId
            ))
            ->whereRaw('quant_bals.balance_unit_qty > 0')
            ->whereIn('storage_types.bin_type', $binTypes)
            ->get();

        return $quantBals;
    }

    static public function txnFindByKey($companyId, $handlingUnitId, $itemId, $itemBatchId, $storageBinId, $backdate = '') 
	{
        if(empty($backdate))
        {
            $backdate = date('Y-m-d');
        }

        $model = DB::transaction
        (
            function() use ($companyId, $handlingUnitId, $itemId, $itemBatchId, $storageBinId, $backdate)
            {
                $today = date('Y-m-d');

                $query = QuantBal::select('quant_bals.*')
                ->where(array(
                    'handling_unit_id'=>$handlingUnitId,
                    'storage_bin_id'=>$storageBinId,
                    'company_id'=>$companyId,
                    'item_id'=>$itemId,
                    'item_batch_id'=>$itemBatchId
                ));

                if(strcmp($backdate, $today) != 0)
                {
                    $query->selectRaw(
                        'SUM(quant_bals.balance_unit_qty) - 
                        (
                            SELECT IFNULL(SUM(quant_bal_txns.sign * quant_bal_txns.unit_qty), 0) 
                            FROM quant_bal_txns
                            WHERE quant_bal_txns.doc_date > "'.$backdate.'" 
                            AND quant_bal_txns.quant_bal_id = quant_bals.id
                        ) AS balance_unit_qty'
                    )
                    ->groupBy('quant_bals.id');;
                }

                $model = $query->first();
                if(empty($model))
                {
                    $model = new QuantBal;
                    $model->storage_bin_id = $storageBinId;
                    //$model->site_id = 0;
                    $model->company_id = $companyId;
                    $model->item_id = $itemId;
                    $model->item_batch_id = $itemBatchId;
                    $model->handling_unit_id = $handlingUnitId;
                    $model->balance_unit_qty = 0;
                    //$model->location_id = 0;
                    //$model->storage_type_id = 0;
                    //$model->storage_row_id = 0;
                    //$model->storage_bay_id = 0;
                    //$model->level = 0;
                    //$model->bay_sequence = 0;
                }
                return $model;
            }, 
            5 //reattempt times
        );
        return $model;
    }
}