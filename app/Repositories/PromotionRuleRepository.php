<?php

namespace App\Repositories;

use App\Models\Promotion;
use App\Models\PromotionRule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Services\Utils\RepositoryUtils;

class PromotionRuleRepository 
{
    static public function findAllByPromotionId($promotionId) 
	{
        $models = PromotionRule::where('promotion_id', $promotionId)
            ->orderBy('id', 'ASC')
            ->get();

        return $models;
    }

    static public function createModel($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = new PromotionRule;
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return $model;
            }, 
            5 //reattempt times
        );
        return $model;
    }
    
    static public function updateModel($data) 
	{
        $result = DB::transaction
        (
            function() use ($data)
            {
                //update Item
                $model = PromotionRule::lockForUpdate()->find($data['id']);
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return array(
                    'model' => $model
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function deleteModel($id) 
	{
        $rule = PromotionRule::find($id);
        $rule->delete();
        return $rule;
    }
}