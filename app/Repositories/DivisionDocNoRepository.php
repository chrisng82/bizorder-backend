<?php

namespace App\Repositories;

use App\DivisionDocNo;

class DivisionDocNoRepository 
{
    static public function findDivisionDocNo($divisionId, $docType, $creditTermType = 0) 
	{
        $divisionDocNo = null;
        $divisionDocNos = DivisionDocNo::where(array(
            'division_id' => $divisionId,
            'doc_no_type' => $docType
        ))
        ->orderBy('seq_no')
        ->get();
        //match credit term type first
        if($creditTermType > 0)
        {
            foreach($divisionDocNos as $tmpDivisionDocNo)
            {
                if($tmpDivisionDocNo->credit_term_type == $creditTermType)
                {
                    $divisionDocNo = $tmpDivisionDocNo;
                    break;
                }
            }
        }
        //match 0 credit term type at last if not found
        if(empty($divisionDocNo))
        {
            foreach($divisionDocNos as $tmpDivisionDocNo)
            {
                $divisionDocNo = $tmpDivisionDocNo;
                break;
            }
        }

        return $divisionDocNo;
    }
}