<?php

namespace App\Repositories;

use App\PrintDocSetting;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\DB;

class PrintDocSettingRepository
{
    static public function findByDocHdrTypeAndSiteFlowId($docHdrType, $siteFlowId) 
	{
        $printDocSetting = PrintDocSetting::where('doc_hdr_type', $docHdrType)
        ->where('site_flow_id', $siteFlowId)
        ->orderBy('seq_no')
        ->first();

        return $printDocSetting;
    }

    static public function findByDocHdrTypeAndDivisionId($docHdrType, $divisionId) 
	{
        $printDocSetting = PrintDocSetting::where('doc_hdr_type', $docHdrType)
        ->where('division_id', $divisionId)
        ->orderBy('seq_no')
        ->first();

        return $printDocSetting;
    }
}