<?php

namespace App\Repositories;

use App\WhseJobItem;
use Illuminate\Support\Facades\DB;

class WhseJobItemRepository 
{
    static public function findAllByHdrId($hdrId) 
	{
        $whseJobItems = WhseJobItem::where('hdr_id', $hdrId)
            ->get();

        return $whseJobItems;
    }
}