<?php

namespace App\Repositories;

use App\Services\Utils\RepositoryUtils;
use App\InbOrdHdr;
use App\InbOrdDtl;
use App\InbOrdCDtl;
use App\DocTxnFlow;
use App\DocTxnVoid;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use Illuminate\Support\Facades\DB;

class InbOrdHdrRepository 
{
    static public function txnFindByPk($hdrId) 
	{
        $model = DB::transaction
        (
            function() use ($hdrId)
            {
                $inbOrdHdr = InbOrdHdr::where('id', $hdrId)
                    ->first();
                return $inbOrdHdr;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function findAllByHdrIds($hdrIds)
    {
        $inbOrdHdrs = InbOrdHdr::whereIn('id', $hdrIds)
            ->get();

        return $inbOrdHdrs;
    }

    static public function findAllNotExistGdsRcpt01Txn($siteFlowId, $docStatus, $sorts, $filters = array(), $pageSize = 20) 
	{
        $gdsRcpt01TxnFlows = DocTxnFlow::select('doc_txn_flows.fr_doc_hdr_id')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['GDS_RCPT_01'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\InbOrdHdr::class)
                    ->where('doc_txn_flows.is_closed', 1)
                    ->distinct();

        $inbOrdHdrs = InbOrdHdr::select('inb_ord_hdrs.*')
            ->leftJoinSub($gdsRcpt01TxnFlows, 'gds_rcpt_01_txn_flows', function ($join) {
                $join->on('inb_ord_hdrs.id', '=', 'gds_rcpt_01_txn_flows.fr_doc_hdr_id');
            })
            ->whereNull('gds_rcpt_01_txn_flows.fr_doc_hdr_id')
            ->where('site_flow_id', $siteFlowId)
            ->where('doc_status', $docStatus)
            ->where('proc_type', ProcType::$MAP['INB_ORD_01']);
        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $inbOrdHdrs->where(function($q) use($filter) {
                    $q->orWhere('inb_ord_hdrs.doc_code', 'LIKE', '%' . $filter['value'] . '%')
                    ->orWhere('inb_ord_hdrs.adv_ship_hdr_code', 'LIKE', '%' . $filter['value'] . '%')
                    ->orWhere('inb_ord_hdrs.ref_code_01', 'LIKE', '%' . $filter['value'] . '%')
                    ->orWhere('inb_ord_hdrs.ref_code_02', 'LIKE', '%' . $filter['value'] . '%')
                    ->orWhere('inb_ord_hdrs.ref_code_03', 'LIKE', '%' . $filter['value'] . '%')
                    ->orWhere('inb_ord_hdrs.ref_code_04', 'LIKE', '%' . $filter['value'] . '%')
                    ->orWhere('inb_ord_hdrs.ref_code_05', 'LIKE', '%' . $filter['value'] . '%')
                    ->orWhere('inb_ord_hdrs.ref_code_06', 'LIKE', '%' . $filter['value'] . '%')
                    ->orWhere('inb_ord_hdrs.ref_code_07', 'LIKE', '%' . $filter['value'] . '%')
                    ->orWhere('inb_ord_hdrs.ref_code_08', 'LIKE', '%' . $filter['value'] . '%');
                });
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $inbOrdHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        return $inbOrdHdrs
            ->paginate($pageSize);
    }

    static public function createHeader($procType, $docNoId, $hdrData) 
	{
        $hdrModel = DB::transaction
        (
            function() use ($procType, $docNoId, $hdrData)
            {
                $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

                $hdrModel = new InbOrdHdr;
                $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
                $hdrModel->doc_code = $newCode;
                $hdrModel->proc_type = $procType;
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];

                $hdrModel->gross_amt = 0;
                $hdrModel->gross_local_amt = 0;
                $hdrModel->disc_amt = 0;
                $hdrModel->disc_local_amt = 0;
                $hdrModel->tax_amt = 0;
                $hdrModel->tax_local_amt = 0;
                $hdrModel->is_round_adj = 0;
                $hdrModel->round_adj_amt = 0;
                $hdrModel->round_adj_local_amt = 0;
                $hdrModel->net_amt = 0;
                $hdrModel->net_local_amt = 0;
                
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function updateDetails($inbOrdHdrData, $inbOrdDtlArray, $delInbOrdDtlArray) 
	{
        foreach($delInbOrdDtlArray as $delInbOrdDtlData)
        {
            $delInbOrdDtlIds[] = $delInbOrdDtlData['id'];
        }

        //delete detail id
        $delInbOrdDtlIds = array();
        foreach($delInbOrdDtlArray as $delInbOrdDtlData)
        {
            $delInbOrdDtlIds[] = $delInbOrdDtlData['id'];
        }
        
        $result = DB::transaction
        (
            function() use ($inbOrdHdrData, $inbOrdDtlArray, $delInbOrdDtlIds)
            {
                $inbOrdHdrIdHashByUuid = array();
                $inbOrdDtlIdHashByUuid = array();
                //update inbOrdHdr
                $inbOrdHdrModel = InbOrdHdr::lockForUpdate()->find($inbOrdHdrData['id']);
                if($inbOrdHdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    $exc = new ApiException(__('InbOrd.doc_status_is_complete', ['docCode'=>$inbOrdHdrModel->doc_code]));
                    $exc->addData(\App\InbOrdHdr::class, $inbOrdHdrModel->id);
                    throw $exc;
                }
                $inbOrdHdrModel = RepositoryUtils::dataToModel($inbOrdHdrModel, $inbOrdHdrData);
                $inbOrdHdrModel->save();

                //update inbOrdDtl array
                $inbOrdDtlModels = array();
                foreach($inbOrdDtlArray as $inbOrdDtlData)
                {
                    $inbOrdDtlModel = null;
                    if (strpos($inbOrdDtlData['id'], 'NEW') !== false) 
                    {
                        $uuid = $inbOrdDtlData['id'];
                        $inbOrdDtlModel = new InbOrdDtl;
                        if(array_key_exists($uuid, $inbOrdHdrIdHashByUuid))
                        {
                            $inbOrdDtlModel->outb_ord_hdr_id = $inbOrdHdrIdHashByUuid[$uuid];
                        }
                        if(array_key_exists($uuid, $inbOrdDtlIdHashByUuid))
                        {
                            $inbOrdDtlModel->outb_ord_dtl_id = $inbOrdDtlIdHashByUuid[$uuid];
                        }
                        unset($inbOrdDtlData['id']);
                    }
                    else
                    {
                        $inbOrdDtlModel = InbOrdDtl::lockForUpdate()->find($inbOrdDtlData['id']);
                    }
                    
                    $inbOrdDtlModel = RepositoryUtils::dataToModel($inbOrdDtlModel, $inbOrdDtlData);
                    $inbOrdDtlModel->hdr_id = $inbOrdHdrModel->id;
                    $inbOrdDtlModel->save();
                    $inbOrdDtlModels[] = $inbOrdDtlModel;
                }

                if(!empty($delInbOrdDtlIds))
                {
                    InbOrdDtl::destroy($delInbOrdDtlIds);
                }

                return array(
                    'hdrModel' => $inbOrdHdrModel,
                    'dtlModels' => $inbOrdDtlModels
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function commitToComplete($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = InbOrdHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();
                    
                if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('InbOrd.doc_status_is_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\InbOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                if($hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('InbOrd.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\InbOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }

                $dtlModels = InbOrdDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->orderBy('line_no')
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('InbOrd.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\InbOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
        
                //1) update docStatus to COMPLETE
                $hdrModel->doc_status = DocStatus::$MAP['COMPLETE'];
                $hdrModel->save();

                return $hdrModel;              
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function findByPk($hdrId) 
	{
        $inbOrdHdr = InbOrdHdr::where('id', $hdrId)
            ->first();

        return $inbOrdHdr;
    }

    static public function createProcess($procType, $docNoId, $hdrData, $dtlDataHashByDtlId, $frDocHdrModel, $frDocDtlModelHashByDtlId, $docTxnFlowDataList) 
	{
        if(empty($dtlDataHashByDtlId))
        {
            $exc = new ApiException(__('InbOrd.item_details_not_found', ['docCode'=>$hdrData['docCode']]));
            //$exc->addData(\App\InbOrdHdr::class, $hdrData['docCode']);
            throw $exc;
        }
        $hdrModel = DB::transaction
        (
            function() use ($procType, $docNoId, $hdrData, $dtlDataHashByDtlId, $frDocHdrModel, $frDocDtlModelHashByDtlId, $docTxnFlowDataList)
            {
                $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

                $hdrModel = new InbOrdHdr;
                $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
                $hdrModel->doc_code = $newCode;
                $hdrModel->proc_type = $procType;
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                //set the frDocHdr to point to this inbOrd
                $frDocHdrModel->inb_ord_hdr_id = $hdrModel->id;
                $frDocHdrModel->save();

                foreach($dtlDataHashByDtlId as $dtlId => $dtlData)
                {
                    $dtlModel = new InbOrdDtl;
                    $dtlModel = RepositoryUtils::dataToModel($dtlModel, $dtlData);
                    $dtlModel->hdr_id = $hdrModel->id;
                    $dtlModel->save();

                    //create a carbon copy
                    $cDtlModel = new InbOrdCDtl;
                    $cDtlModel = RepositoryUtils::dataToModel($cDtlModel, $dtlData);
                    $cDtlModel->hdr_id = $hdrModel->id;
                    $cDtlModel->save();

                    //set the frDocDtl to point to this inbOrd
                    $frDocDtlModel = $frDocDtlModelHashByDtlId[$dtlId];
                    $frDocDtlModel->inb_ord_hdr_id = $hdrModel->id;
                    $frDocDtlModel->inb_ord_dtl_id = $dtlModel->id;
                    $frDocDtlModel->save();
                }

                //process docTxnFlowDataList
                foreach($docTxnFlowDataList as $docTxnFlowData)
                {
                    $frDocHdrModel = $docTxnFlowData['fr_doc_hdr_type']::where('id', $docTxnFlowData['fr_doc_hdr_id'])
                    ->lockForUpdate()
                    ->first();
                    if($frDocHdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                    {
                        $exc = new ApiException(__('InbOrd.fr_doc_is_not_complete', ['docType'=>$docTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$docTxnFlowData['fr_doc_hdr_code']]));
                        $exc->addData($docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_code']);
                        throw $exc;
                    }

                    //verify the frDoc is not closed for this procType
                    $tmpDocTxnFlow = DocTxnFlowRepository::findByProcTypeAndFrHdrId($procType, $docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_id'], 1);
                    if(!empty($tmpDocTxnFlow))
                    {
                        $exc = new ApiException(__('InbOrd.fr_doc_is_closed', ['docType'=>$docTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$docTxnFlowData['fr_doc_hdr_code']]));
                        $exc->addData($docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_code']);
                        throw $exc;
                    }

                    $docTxnFlowModel = new DocTxnFlow;
                    $docTxnFlowModel = RepositoryUtils::dataToModel($docTxnFlowModel, $docTxnFlowData);
                    $docTxnFlowModel->proc_type = $procType;
                    $docTxnFlowModel->to_doc_hdr_type = \App\InbOrdHdr::class;
                    $docTxnFlowModel->to_doc_hdr_id = $hdrModel->id;
                    $docTxnFlowModel->save();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function findAllNotExistPurInv01Txn($divisionId, $docStatus, $sorts, $filters = array(), $pageSize = 20) 
	{
        $inbOrdHdrs = InbOrdHdr::select('inb_ord_hdrs.*')
            ->whereNotExists(function ($query) {
            $query->select(DB::raw(1))
                ->from('doc_txn_flows')
                ->where('doc_txn_flows.proc_type', ProcType::$MAP['PUR_INV_01'])
                ->where('doc_txn_flows.fr_doc_hdr_type', \App\InbOrdHdr::class)
                ->whereRaw('doc_txn_flows.fr_doc_hdr_id = inb_ord_hdrs.id')
                ->where('doc_txn_flows.is_closed', 1);
            })
            ->where('division_id', $divisionId)
            ->where('doc_status', $docStatus);
        foreach($filters as $key => $value)
        {
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $inbOrdHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        return $inbOrdHdrs
            ->paginate($pageSize);
    }

    static public function findAllNotExistGdsRcpt02Txn($divisionIds, $siteFlowId, $docStatus, $sorts, $filters = array(), $pageSize = 20) 
	{
        $leftJoinHash = array();
        $gdsRcpt02TxnFlows = DocTxnFlow::select('doc_txn_flows.fr_doc_hdr_id')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['GDS_RCPT_02'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\InbOrdHdr::class)
                    ->where('doc_txn_flows.is_closed', 1)
                    ->distinct();

        $inbOrdHdrs = InbOrdHdr::select('inb_ord_hdrs.*')
            ->leftJoinSub($gdsRcpt02TxnFlows, 'gds_rcpt_02_txn_flows', function ($join) {
                $join->on('inb_ord_hdrs.id', '=', 'gds_rcpt_02_txn_flows.fr_doc_hdr_id');
            })
            ->whereIn('division_id', $divisionIds)
            ->whereNull('gds_rcpt_02_txn_flows.fr_doc_hdr_id')
            ->where('site_flow_id', $siteFlowId)
            ->where('doc_status', $docStatus)
            ->where('proc_type', ProcType::$MAP['INB_ORD_02']);
        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $inbOrdHdrs->where(function($q) use($filter) {
                    $q->where('inb_ord_hdrs.doc_code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('inb_ord_hdrs.sls_rtn_hdr_code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('inb_ord_hdrs.ref_code_01', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('inb_ord_hdrs.ref_code_02', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('inb_ord_hdrs.ref_code_03', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('inb_ord_hdrs.ref_code_04', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'salesman') == 0)
            {
                $leftJoinHash['users'] = 'users';
                $inbOrdHdrs->where('users.username', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $inbOrdHdrs->where(function($q) use($filter) {
                    $q->where('delivery_points.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('delivery_points.company_name_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $inbOrdHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'users') == 0)
            {
                $inbOrdHdrs->leftJoin('users', 'users.id', '=', 'inb_ord_hdrs.salesman_id');
            }
            if(strcmp($field, 'delivery_points') == 0)
            {
                $inbOrdHdrs->leftJoin('delivery_points', 'delivery_points.id', '=', 'inb_ord_hdrs.delivery_point_id');
            }
            if(strcmp($field, 'areas') == 0)
            {
                $inbOrdHdrs->leftJoin('areas', 'areas.id', '=', 'delivery_points.area_id');
            }
        }

        return $inbOrdHdrs
            ->paginate($pageSize);
    }

    static public function findAllByGdsRcptHdrIdAndItemId($gdsRcptHdrId, $itemId) 
	{
        $inbOrdHdrs = InbOrdHdr::select('inb_ord_hdrs.*')
            ->leftJoin('gds_rcpt_inb_ords', 'inb_ord_hdrs.id', '=', 'gds_rcpt_inb_ords.inb_ord_hdr_id')
            ->where('gds_rcpt_inb_ords.hdr_id', $gdsRcptHdrId)
            ->whereExists(function ($query) use ($itemId) {
                $query->select(DB::raw(1))
                    ->from('inb_ord_dtls')
                    ->whereRaw('inb_ord_dtls.hdr_id = inb_ord_hdrs.id')
                    ->where('inb_ord_dtls.item_id', $itemId);
            })
            ->get();

        return $inbOrdHdrs;
    }

    static public function revertDraftToVoid($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = InbOrdHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('InbOrd.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\InbOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to VOID
                $hdrModel->doc_status = DocStatus::$MAP['VOID'];
                $hdrModel->save();

                $frDocTxnFlows = $hdrModel->frDocTxnFlows;
                foreach($frDocTxnFlows as $frDocTxnFlow)
                {
                    //move to voidTxn
                    $txnVoidModel = new DocTxnVoid;
                    $txnVoidModel->id = $frDocTxnFlow->id;
                    $txnVoidModel->proc_type = $frDocTxnFlow->proc_type;
                    $txnVoidModel->fr_doc_hdr_type = $frDocTxnFlow->fr_doc_hdr_type;
                    $txnVoidModel->fr_doc_hdr_id = $frDocTxnFlow->fr_doc_hdr_id;
                    $txnVoidModel->fr_doc_hdr_code = $frDocTxnFlow->fr_doc_hdr_code;
                    $txnVoidModel->to_doc_hdr_type = $frDocTxnFlow->to_doc_hdr_type;
                    $txnVoidModel->to_doc_hdr_id = $frDocTxnFlow->to_doc_hdr_id;
                    $txnVoidModel->is_closed = $frDocTxnFlow->is_closed;
                    $txnVoidModel->save();
        
                    //delete the txn
                    $frDocTxnFlow->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertCompleteToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = InbOrdHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                {
                    $exc = new ApiException(__('InbOrd.doc_status_is_not_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\InbOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertWipToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = InbOrdHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['WIP'])
                {
                    $exc = new ApiException(__('InbOrd.doc_status_is_not_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\InbOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitVoidToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = InbOrdHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['VOID'])
                {
                    //only VOID can transition to DRAFT
                    $exc = new ApiException(__('InbOrd.doc_status_is_not_void', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\InbOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                $frDocTxnVoids = $hdrModel->frDocTxnVoids;
                foreach($frDocTxnVoids as $frDocTxnVoid)
                {
                    //verify first
                    $docTxnFlow = DocTxnFlow::where(array(
                        'proc_type' => $frDocTxnVoid->proc_type,
                        'fr_doc_hdr_type' => $frDocTxnVoid->fr_doc_hdr_type,
                        'fr_doc_hdr_id' => $frDocTxnVoid->fr_doc_hdr_id,
                        'to_doc_hdr_type' => $frDocTxnVoid->to_doc_hdr_type
                        ))
                        ->first();
                    if(!empty($docTxnFlow))
                    {
                        $exc = new ApiException(__('InbOrd.fr_doc_is_closed', ['docCode'=>$frDocTxnVoid->fr_doc_hdr_code]));
                        $exc->addData(\App\InbOrdHdr::class, $hdrModel->id);
                        throw $exc;
                    }

                    //move to txnFlow
                    $txnFlowModel = new DocTxnFlow;
                    $txnFlowModel->id = $frDocTxnVoid->id;
                    $txnFlowModel->proc_type = $frDocTxnVoid->proc_type;
                    $txnFlowModel->fr_doc_hdr_type = $frDocTxnVoid->fr_doc_hdr_type;
                    $txnFlowModel->fr_doc_hdr_id = $frDocTxnVoid->fr_doc_hdr_id;
                    $txnFlowModel->fr_doc_hdr_code = $frDocTxnVoid->fr_doc_hdr_code;
                    $txnFlowModel->to_doc_hdr_type = $frDocTxnVoid->to_doc_hdr_type;
                    $txnFlowModel->to_doc_hdr_id = $frDocTxnVoid->to_doc_hdr_id;
                    $txnFlowModel->is_closed = $frDocTxnVoid->is_closed;
                    $txnFlowModel->save();
        
                    //delete the txnVoid
                    $frDocTxnVoid->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitToWip($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = InbOrdHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('InbOrd.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\InbOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }

                $dtlModels = InbOrdDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->orderBy('line_no')
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('InbOrd.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\InbOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to WIP
                $hdrModel->doc_status = DocStatus::$MAP['WIP'];
                $hdrModel->save();
                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function findAll($divisionId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $leftJoinHash = array();
        $inbOrdHdrs = InbOrdHdr::select('inb_ord_hdrs.*')
            ->where('division_id', $divisionId);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $inbOrdHdrs->where('inb_ord_hdrs.doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $inbOrdHdrs->where(function($q) use($filter) {
                    $q->where('delivery_points.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('delivery_points.company_name_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'purchaser') == 0)
            {
                $leftJoinHash['users'] = 'users';
                $inbOrdHdrs->where('users.username', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point_area') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $leftJoinHash['areas'] = 'areas';
                $inbOrdHdrs->where(function($q) use($filter) {
                    $q->where('areas.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('areas.desc_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'status') == 0)
            {
                if($filter['value'] > 0)
                {
                    $inbOrdHdrs->where('inb_ord_hdrs.doc_status', $filter['value']);
                }
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $inbOrdHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'delivery_points') == 0)
            {
                $inbOrdHdrs->leftJoin('delivery_points', 'delivery_points.id', '=', 'inb_ord_hdrs.delivery_point_id');
            }
            if(strcmp($field, 'users') == 0)
            {
                $inbOrdHdrs->leftJoin('users', 'users.id', '=', 'inb_ord_hdrs.purchaser_id');
            }
            if(strcmp($field, 'areas') == 0)
            {
                $inbOrdHdrs->leftJoin('areas', 'areas.id', '=', 'delivery_points.area_id');
            }
        }
        
        if($pageSize > 0)
        {
            return $inbOrdHdrs
                ->paginate($pageSize);
        }
        else
        {
            return $inbOrdHdrs
                ->paginate(PHP_INT_MAX);
        }
    }
}