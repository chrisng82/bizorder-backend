<?php

namespace App\Repositories;

use App\AdvShipDtl;

class AdvShipDtlRepository 
{
    static public function findAllByHdrId($hdrId)
    {
        $advShipDtls = AdvShipDtl::where('hdr_id', $hdrId)
            ->orderBy('line_no')
            ->get();

        return $advShipDtls;
    }

    static public function findTopByHdrId($hdrId)
    {
        $advShipDtl = AdvShipDtl::where('hdr_id', $hdrId)
            ->orderBy('line_no')
            ->first();

        return $advShipDtl;
    }

    static public function queryShipmentDetailsGroupByItemId($hdrId, $sortfield = 'item_group_01_code', $sortOrder = 'ASC') 
	{
        $advShipDtls = AdvShipDtl::select(
                'adv_ship_dtls.hdr_id AS hdr_id',
                'items.id AS item_id',
                'items.code AS item_code',
                'items.ref_code_01 AS item_ref_code_01',
                'items.desc_01 AS item_desc_01',
                'items.desc_02 AS item_desc_02',
                'items.storage_class AS storage_class',
                'items.item_group_01_code AS item_group_01_code',
                'items.item_group_02_code AS item_group_02_code',
                'items.item_group_03_code AS item_group_03_code',
                'items.item_group_04_code AS item_group_04_code',
                'items.item_group_05_code AS item_group_05_code',
                'items.s_storage_class AS s_storage_class',
                'items.s_item_group_01_code AS s_item_group_01_code',
                'items.s_item_group_02_code AS s_item_group_02_code',
                'items.s_item_group_03_code AS s_item_group_03_code',
                'items.s_item_group_04_code AS s_item_group_04_code',
                'items.s_item_group_05_code AS s_item_group_05_code'
            )
            //case_qty
            ->selectRaw('SUM(adv_ship_dtls.qty * adv_ship_dtls.uom_rate) AS unit_qty')
            //case_qty
            ->selectRaw('SUM(adv_ship_dtls.qty * adv_ship_dtls.uom_rate / items.case_uom_rate) AS case_qty')
            //gross_weight, kg
            ->selectRaw('SUM(adv_ship_dtls.qty * adv_ship_dtls.uom_rate / items.case_uom_rate * items.case_gross_weight) AS gross_weight')
            //cubic_meter, meter
            ->selectRaw('SUM(adv_ship_dtls.qty * adv_ship_dtls.uom_rate / items.case_uom_rate * items.case_ext_length * items.case_ext_width * items.case_ext_height / 1000000000) AS cubic_meter')
            ->leftJoin('items', 'items.id', '=', 'adv_ship_dtls.item_id')
            ->where('hdr_id', $hdrId)
            ->groupBy(
                'item_code',
                'item_ref_code_01',
                'item_desc_01',
                'item_desc_02',
                'storage_class',
                'item_group_01_code',
                'item_group_02_code',
                'item_group_03_code',
                'item_group_04_code',
                'item_group_05_code',
                's_storage_class',
                's_item_group_01_code',
                's_item_group_02_code',
                's_item_group_03_code',
                's_item_group_04_code',
                's_item_group_05_code'
            )
            ->when(in_array($sortfield, array('item_code', 'item_ref_code_01', 'item_desc_01', 'item_desc_02')) === FALSE, function ($query) use($sortfield, $sortOrder) {
                return $query->orderBy($sortfield, $sortOrder);
            })
            ->orderBy('item_code', $sortOrder)
            ->get();
        return $advShipDtls;
    }
}