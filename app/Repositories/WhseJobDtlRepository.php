<?php

namespace App\Repositories;

use App\WhseJobDtl;
use App\Services\Env\ProcType;
use Illuminate\Support\Facades\DB;

class WhseJobDtlRepository 
{
    static public function findAllByHdrId($hdrId) 
	{
        $whseJobDtls = WhseJobDtl::select('whse_job_dtls.*')
            ->where('hdr_id', $hdrId)
            ->orderBy('line_no')
            ->get();

        return $whseJobDtls;
    }

    static public function findAllByHdrIds($hdrIds) 
	{
        $whseJobDtls = WhseJobDtl::select('whse_job_dtls.*')
            ->whereIn('hdr_id', $hdrIds)
            ->get();

        return $whseJobDtls;
    }

    static public function findTopByHdrId($hdrId) 
	{
        $whseJobDtl = WhseJobDtl::select('whse_job_dtls.*')
            ->where('hdr_id', $hdrId)
            ->orderBy('line_no')
            ->first();

        return $whseJobDtl;
    }

    static public function findAllByQuantBalIdAndWhseJobType($quantBalId, $whseJobType) 
	{
        $whseJobDtls = WhseJobDtl::select('whse_job_dtls.*')
            ->selectRaw('whse_job_hdrs.doc_code AS doc_code')
            ->leftJoin('whse_job_hdrs', 'whse_job_hdrs.id', '=', 'whse_job_dtls.hdr_id')
            ->where('quant_bal_id', $quantBalId)
            ->where('whse_job_type', $whseJobType)
            //->whereIn('whse_job_hdrs.doc_status', DocStatus::$MAP['WIP'])
            ->get();

        return $whseJobDtls;
    }
}