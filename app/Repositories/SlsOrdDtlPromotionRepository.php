<?php

namespace App\Repositories;

use App\SlsOrdDtl;
use App\SlsOrdDtlPromotion;
use App\Models\Promotion;
use App\Services\Utils\RepositoryUtils;
use Illuminate\Support\Facades\DB;

class SlsOrdDtlPromotionRepository 
{
    static public function findByPk($dtlId) 
	{
        $slsOrdDtlPromo = SlsOrdDtlPromotion::where('id', $dtlId)
            ->first();

        return $slsOrdDtlPromo;
    }

    static public function findAllByDtlId($dtlId) 
	{
        $slsOrdDtlPromos = SlsOrdDtlPromotion::where('sls_ord_dtl_id', $dtlId)
            ->get();

        return $slsOrdDtlPromos;
    }

    static public function findAllByDtlIds($dtlIds) 
	{
        $slsOrdDtlPromos = SlsOrdDtlPromotion::whereIn('sls_ord_dtl_id', $dtlIds)
            ->get();

        return $slsOrdDtlPromos;
    }

    static public function findAllByIds($ids) 
	{
        $slsOrdDtlPromos = SlsOrdDtlPromotion::whereIn('id', $ids)
            ->get();

        return $slsOrdDtlPromos;
    }

    static public function findAllByPromotionId($promotionId) 
	{
        $slsOrdDtlPromos = SlsOrdDtlPromotion::where('promotion_id', $promotionId)
            ->get();

        return $slsOrdDtlPromos;
    }

    static public function createModel($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = new SlsOrdDtlPromotion;
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return $model;
            }, 
            5 //reattempt times
        );
        return $model;
    }
    
    static public function deleteAllByDtlIds($dtlIds) 
	{
        SlsOrdDtlPromotion::whereIn('sls_ord_dtl_id', $dtlIds)->delete();
    }

}