<?php

namespace App\Repositories;

use App\ItemDivision;
use App\Services\Utils\RepositoryUtils;
use Illuminate\Support\Facades\DB;

class ItemDivisionRepository 
{
    static public function createModel($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = new ItemDivision;
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return $model;
            }, 
            5 //reattempt times
        );
        return $model;
    }
}