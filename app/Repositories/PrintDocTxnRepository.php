<?php

namespace App\Repositories;

use App\DocTxnFlow;
use App\PrintDocTxn;
use App\Services\Utils\RepositoryUtils;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class PrintDocTxnRepository
{
    static public function createProcess($printDocTxnDataArray, $toDocTxnFlowArray = array()) 
	{
        $printedAt = DB::transaction
        (
            function() use ($printDocTxnDataArray, $toDocTxnFlowArray)
            {
                $printedAt = Carbon::createFromTimestamp(0);
                foreach($printDocTxnDataArray as $printDocTxnData)
                {
                    $printDocTxn = new PrintDocTxn;
                    $printDocTxn->doc_hdr_type = $printDocTxnData['doc_hdr_type'];
                    $printDocTxn->doc_hdr_id = $printDocTxnData['doc_hdr_id'];
                    $printDocTxn->user_id = $printDocTxnData['user_id'];
                    $printDocTxn->save();

                    $printedAt = $printDocTxn->created_at;
                }

                foreach($toDocTxnFlowArray as $toDocTxnFlowData)
                {
                    $docTxnFlowModel = new DocTxnFlow;
                    $docTxnFlowModel = RepositoryUtils::dataToModel($docTxnFlowModel, $toDocTxnFlowData);
                    $docTxnFlowModel->save();
                }

                return $printedAt;
            }, 
            5 //reattempt times
        );
        return $printedAt;
    }

    static public function queryPrintDocTxn($docHdrType, $docHdrId) 
	{
        $cubicMeter = 0;
        $grossWeight = 0;

        $printDocTxn = PrintDocTxn::selectRaw('COUNT(id) AS print_count')
            ->selectRaw('IFNULL(MIN(updated_at), "1970-01-01 00:00:00") AS first_printed_at')
            ->selectRaw('IFNULL(MAX(updated_at), "1970-01-01 00:00:00") AS last_printed_at')
            ->where('doc_hdr_type', $docHdrType)
            ->where('doc_hdr_id', $docHdrId)
            ->first();
        return $printDocTxn;
    }
}