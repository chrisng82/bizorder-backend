<?php

namespace App\Repositories;

use App\Area;
use App\Division;
use App\Debtor;
use App\DebtorDivision;
use App\DebtorGroup01;
use App\DebtorGroup02;
use App\DebtorGroup03;
use App\Services\Env\ResStatus;
use App\Services\Utils\RepositoryUtils;
use Illuminate\Support\Facades\DB;

class DebtorRepository 
{
    static public function findByPk($id) 
	{
        $debtor = Debtor::where('id', $id)
            ->first();

        return $debtor;
    }

    static public function findByCode($code) 
	{
        $debtor = Debtor::where('code', $code)
            ->first();

        return $debtor;
    }
    
    static public function findByCodeAndDivision($code, $divisionId) 
	{
        $debtor = Debtor::select('debtors.*')
                ->leftJoin('debtor_divisions', 'debtors.id', '=', 'debtor_divisions.debtor_id')
                ->where('debtor_divisions.division_id', $divisionId)
                ->where('code', $code)
                ->first();
        return $debtor;
    }

    static public function findAll($sorts, $filters = array(), $pageSize = 20, $divisionId = 0,$status) 
	{
        error_log($status);
        $debtors = Debtor::select('debtors.*');
        if($status == 'Not_Deleted'){
            $debtors->where('debtors.status', '!=', '818');
        }
        else{
            $debtors->where('debtors.status', '=', '818');
        }
        if ($divisionId > 0) {
            $debtors->leftJoin('debtor_divisions', 'debtor_divisions.debtor_id', '=', 'debtors.id');
            $debtors->where('debtor_divisions.division_id', '=', $divisionId);
        }
        
        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'debtor_category_01_id') == 0)
            {
                if (is_array($filter['value'])) 
                {                
                    $debtors->whereIn('debtors.debtor_category_01_id', $filter['value']);
                }
                else
                {
                    $debtors->where('debtors.debtor_category_01_id', $filter['value']);
                }
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'code') == 0)
            {
                $debtors->orderBy('code', $sort['order']);
            }
        }
        if($pageSize > 0)
        {
            return $debtors
            ->paginate($pageSize);
        }
        else
        {
            return $debtors
            ->paginate(PHP_INT_MAX);
        }
    }

    static public function select2($search, $filters, $pageSize = 10) 
	{
        $debtors = Debtor::select('debtors.*')
        ->where(function($q) use($search) {
            $q->orWhere('company_name_01', 'LIKE', '%' . $search . '%')
            ->orWhere('company_name_02', 'LIKE', '%' . $search . '%')
            ->orWhere('code', 'LIKE', '%' . $search . '%');
        });
        if(count($filters) >= 1)
        {
            foreach ($filters as $filter)
            {
                if(strcmp($filter['field'], 'division_id') == 0)
                {
                    $debtors->leftJoin('debtor_divisions', 'debtor_divisions.debtor_id', '=', 'debtors.id');
                    $debtors->where('debtor_divisions.division_id', '=', $filter['value']);
                }
                else if(strcmp($filter['field'], 'id') == 0)
                {
                    if(is_array($filter['value'])) {
                        $debtors->whereIn('debtors.id', $filter['value']);
                    } else {
                        $debtors->where('debtors.id', $filter['value']);
                    }
                }
                else if(strcmp($filter['field'], 'debtor_category_01_id') == 0)
                {
                    if(is_array($filter['value'])) {
                        $debtors->orWhere(function($q) use($filter) {
                            $q->whereIn('debtors.debtor_category_01_id', $filter['value']);
                        });
                    } else {
                        $debtors->orWhere('debtors.debtor_category_01_id', $filter['value']);
                    }
                }
                else
                {
                    $debtors = $debtors->where($filter);
                }
            }
        }
        return $debtors
            ->orderBy('code', 'ASC')
            ->paginate($pageSize);
    }
    
    static public function updateDivisions($debtorId, $debtorDivisionArray, $delDebtorDivisionArray) 
	{
        $delDebtorDivisionIds = array();
        foreach($delDebtorDivisionArray as $delDebtorDivisionData)
        {
            $delModel = DebtorDivision::where('debtor_id', $debtorId)
                        ->where('division_id', $delDebtorDivisionData['division_id'])->first();
            $delDebtorDivisionIds[] = $delModel->id;
        }
        
        $result = DB::transaction
        (
            function() use ($debtorId, $debtorDivisionArray, $delDebtorDivisionIds)
            {
                //update debtorDivision array
                $debtorDivisionModels = array();
                foreach($debtorDivisionArray as $debtorDivisionData)
                {
                    $debtorDivisionModel = null;
                    if (strpos($debtorDivisionData['id'], 'NEW') !== false) 
                    {
                        $uuid = $debtorDivisionData['id'];
                        $debtorDivisionModel = new DebtorDivision;
                        unset($debtorDivisionData['id']);
                    }
                    else
                    {
                        $debtorDivisionModel = DebtorDivision::lockForUpdate()->find($debtorDivisionData['id']);
                    }
                    
                    $debtorDivisionModel = RepositoryUtils::dataToModel($debtorDivisionModel, $debtorDivisionData);
                    $debtorDivisionModel->debtor_id = $debtorId;
                    $debtorDivisionModel->save();
                    $debtorDivisionModels[] = $debtorDivisionModel;
                }

                if(!empty($delDebtorDivisionIds))
                {
                    DebtorDivision::destroy($delDebtorDivisionIds);
                }

                return array(
                    'debtorDivisionModels' => $debtorDivisionModels
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function updateHeader($data) 
	{
        $result = DB::transaction
        (
            function() use ($data)
            {
                //update Debtor
                $model = Debtor::lockForUpdate()->find($data['id']);
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return array(
                    'model' => $model
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function createHeader($data)
    {
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = new Debtor;
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return $model;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function deleteHeader($hdrId)
    {
        $result = DB::transaction
        (
            function() use ($hdrId)
            {
                $delModel = Debtor::find($hdrId);
                Debtor::destroy($hdrId);

                return array(
                    'model' => $delModel
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function syncDebtorSync01($data, $divisionId, $syncSettingHdrId) 
	{
        $division = DivisionRepository::findByPk($divisionId);
        if (empty($division)) {
            throw new \Exception('Division not found.');
        }

        $debtorModel = DB::transaction
        (
            function() use ($data, $divisionId, $syncSettingHdrId)
            {
                // $debtorModel = Debtor::firstOrNew(['code' => $data['code']]);
                $debtorModel = Debtor::leftJoin('debtor_divisions', 'debtors.id', '=', 'debtor_divisions.debtor_id')
                    ->where('debtor_divisions.division_id', $divisionId)
                    ->where('code', $data['code'])
                    ->first();
                
                if (!$debtorModel) {
                    $debtorModel = new Debtor;
                    $debtorModel->code = $data['code'];
                }

                $debtorModel->ref_code_01 = $data['ref_code'];
                $debtorModel->company_name_01 = $data['company_name'];
                if($debtorModel->id > 0)
                {
                    //update record
                    if($data['is_active'] == 1)
                    {
                        $debtorCategoryMapping = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdrId, 'debtor_category_mapping');

                        if (!empty($debtorCategoryMapping)) {
                            if ($debtorCategoryMapping->value == 'customer_category') {
                                if (isset($data['customer_category_code']) && !empty($data['customer_category_code'])) {
                                    $attributes = array('code'=>$data['customer_category_code']);
                                    $values = array('desc_01'=>$data['customer_category_description']);
                                    $debtorGroup01Model = DebtorGroup01::firstOrCreate($attributes, $values);
                                    $debtorModel->debtor_category_01_id = $debtorGroup01Model->id;
                                }
                            }
                            else if ($debtorCategoryMapping->value == 'customer_category_01') {
                                //process the customer_category1_code
                                if (isset($data['customer_category1_code']) && !empty($data['customer_category1_code'])) {
                                    $attributes = array('code'=>$data['customer_category1_code']);
                                    $values = array('desc_01'=>$data['customer_category1_description']);
                                    $debtorGroup01Model = DebtorGroup02::firstOrCreate($attributes, $values);
                                    $debtorModel->debtor_category_01_id = $debtorGroup01Model->id;
                                }
                            }
                            else if ($debtorCategoryMapping->value == 'customer_category_02') {
                                //process the customer_category2_code
                                if (isset($data['customer_category2_code']) && !empty($data['customer_category2_code'])) {
                                    $attributes = array('code'=>$data['customer_category2_code']);
                                    $values = array('desc_01'=>$data['customer_category2_description']);
                                    $debtorGroup01Model = DebtorGroup03::firstOrCreate($attributes, $values);
                                    $debtorModel->debtor_category_01_id = $debtorGroup01Model->id;
                                }
                            }
                            else {
                                $debtorModel->debtor_category_01_id = 0;
                            }
                        }
                        else {
                            $debtorModel->debtor_category_01_id = 0;
                        }

                        //insert record
                        //process the customer_category_code
                        // if (isset($data['customer_category_code']) && !empty($data['customer_category_code'])) {
                        //     $attributes = array('code'=>$data['customer_category_code']);
                        //     $values = array('desc_01'=>$data['customer_category_description']);
                        //     $debtorGroup01Model = DebtorGroup01::firstOrCreate($attributes, $values);
                        //     $debtorModel->debtor_category_01_id = $debtorGroup01Model->id;
                        // }

                        // //process the customer_category1_code
                        // if (isset($data['customer_category1_code']) && !empty($data['customer_category1_code'])) {
                        //     $attributes = array('code'=>$data['customer_category1_code']);
                        //     $values = array('desc_01'=>$data['customer_category1_description']);
                        //     $debtorGroup02Model = DebtorGroup02::firstOrCreate($attributes, $values);
                        //     $debtorModel->debtor_category_02_id = $debtorGroup02Model->id;
                        // }

                        // //process the customer_category2_code
                        // if (isset($data['customer_category2_code']) && !empty($data['customer_category2_code'])) {
                        //     $attributes = array('code'=>$data['customer_category2_code']);
                        //     $values = array('desc_01'=>$data['customer_category2_description']);
                        //     $debtorGroup03Model = DebtorGroup03::firstOrCreate($attributes, $values);
                        //     $debtorModel->debtor_category_03_id = $debtorGroup03Model->id;
                        // }

                        //process area
                        $attributes = array('code'=>$data['area_code']);
                        $values = array('desc_01'=>$data['area_description']);
                        $areaModel = Area::firstOrCreate($attributes, $values);
                        $debtorModel->area_id = $areaModel->id;
                        $debtorModel->area_code = $areaModel->code;

                        $debtorModel->address_01 = $data['address_01'];
                        $debtorModel->address_02 = $data['address_02'];
                        $debtorModel->address_03 = $data['address_03'];
                        $debtorModel->address_04 = $data['address_04'];
                        $debtorModel->attention = $data['attention'];
                        $debtorModel->phone_01 = $data['phone_01'];
                        $debtorModel->phone_02 = $data['phone_02'];
                        $debtorModel->fax_01 = $data['fax_01'];
                        $debtorModel->fax_02 = $data['fax_02'];
                        $debtorModel->email_01 = $data['email'];
                        $debtorModel->email_02 = '';
                        $debtorModel->save();

                        if(count($data['details']) > 0)
                        {
                            foreach($data['details'] as $division)
                            {
                                $divisionDB = Division::where('code', $division['division_code'])
                                    ->first();
                                if(!empty($divisionDB))
                                {
                                    $debtorDivisions = DebtorDivision::where('debtor_id', $debtorModel->id)
                                        ->where('division_id', $divisionDB->id)
                                        ->get();
                                    if(count($debtorDivisions) == 0)
                                    {
                                        $debtorDivision = new DebtorDivision;
                                        $debtorDivision->debtor_id = $debtorModel->id;
                                        $debtorDivision->division_id = $divisionDB->id;
                                        $debtorDivision->save();
                                    }
                                }
                            }
                        }
                        else
                        {
                            $debtorModel->status = ResStatus::$MAP['INACTIVE'];
                            $debtorModel->save();
                        }

                    }
                }
                else
                {
                    if(count($data['details']) > 0)
                    {
                        //insert record
                        
                        $debtorCategoryMapping = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdrId, 'debtor_category_mapping');

                        if (!empty($debtorCategoryMapping)) {
                            if ($debtorCategoryMapping->value == 'customer_category') {
                                if (isset($data['customer_category_code']) && !empty($data['customer_category_code'])) {
                                    $attributes = array('code'=>$data['customer_category_code']);
                                    $values = array('desc_01'=>$data['customer_category_description']);
                                    $debtorGroup01Model = DebtorGroup01::firstOrCreate($attributes, $values);
                                    $debtorModel->debtor_category_01_id = $debtorGroup01Model->id;
                                }
                            }
                            else if ($debtorCategoryMapping->value == 'customer_category_01') {
                                //process the customer_category1_code
                                if (isset($data['customer_category1_code']) && !empty($data['customer_category1_code'])) {
                                    $attributes = array('code'=>$data['customer_category1_code']);
                                    $values = array('desc_01'=>$data['customer_category1_description']);
                                    $debtorGroup01Model = DebtorGroup02::firstOrCreate($attributes, $values);
                                    $debtorModel->debtor_category_01_id = $debtorGroup01Model->id;
                                }
                            }
                            else if ($debtorCategoryMapping->value == 'customer_category_02') {
                                //process the customer_category2_code
                                if (isset($data['customer_category2_code']) && !empty($data['customer_category2_code'])) {
                                    $attributes = array('code'=>$data['customer_category2_code']);
                                    $values = array('desc_01'=>$data['customer_category2_description']);
                                    $debtorGroup01Model = DebtorGroup03::firstOrCreate($attributes, $values);
                                    $debtorModel->debtor_category_01_id = $debtorGroup01Model->id;
                                }
                            }
                            else {
                                $debtorModel->debtor_category_01_id = 0;
                            }
                        }
                        else {
                            $debtorModel->debtor_category_01_id = 0;
                        }
                        
                        //process the customer_category_code
                        // if (isset($data['customer_category_code']) && !empty($data['customer_category_code'])) {
                        //     $attributes = array('code'=>$data['customer_category_code']);
                        //     $values = array('desc_01'=>$data['customer_category_description']);
                        //     $debtorGroup01Model = DebtorGroup01::firstOrCreate($attributes, $values);
                        //     $debtorModel->debtor_category_01_id = $debtorGroup01Model->id;
                        // }

                        // //process the customer_category1_code
                        // if (isset($data['customer_category1_code']) && !empty($data['customer_category1_code'])) {
                        //     $attributes = array('code'=>$data['customer_category1_code']);
                        //     $values = array('desc_01'=>$data['customer_category1_description']);
                        //     $debtorGroup02Model = DebtorGroup02::firstOrCreate($attributes, $values);
                        //     $debtorModel->debtor_category_02_id = $debtorGroup02Model->id;
                        // }

                        // //process the customer_category2_code
                        // if (isset($data['customer_category2_code']) && !empty($data['customer_category2_code'])) {
                        //     $attributes = array('code'=>$data['customer_category2_code']);
                        //     $values = array('desc_01'=>$data['customer_category2_description']);
                        //     $debtorGroup03Model = DebtorGroup03::firstOrCreate($attributes, $values);
                        //     $debtorModel->debtor_category_03_id = $debtorGroup03Model->id;
                        // }

                        //process area
                        if (isset($data['area_code']) && !empty($data['area_code'])) {
                            $attributes = array('code'=>$data['area_code']);
                            $values = array('desc_01'=>$data['area_description']);
                            $areaModel = Area::firstOrCreate($attributes, $values);
                            $debtorModel->area_id = $areaModel->id;
                            $debtorModel->area_code = $areaModel->code;
                        }

                        $debtorModel->address_01 = $data['address_01'];
                        $debtorModel->address_02 = $data['address_02'];
                        $debtorModel->address_03 = $data['address_03'];
                        $debtorModel->address_04 = $data['address_04'];
                        $debtorModel->attention = $data['attention'];
                        $debtorModel->phone_01 = $data['phone_01'];
                        $debtorModel->phone_02 = $data['phone_02'];
                        $debtorModel->fax_01 = $data['fax_01'];
                        $debtorModel->fax_02 = $data['fax_02'];
                        $debtorModel->email_01 = $data['email'];
                        $debtorModel->email_02 = '';

                        $debtorModel->unit_no = '';
                        $debtorModel->building_name = '';
                        $debtorModel->street_name = '';
                        $debtorModel->district_01 = '';
                        $debtorModel->district_02 = '';
                        $debtorModel->postcode = '';
                        $debtorModel->state_name = '';
                        $debtorModel->country_name = '';
                        
                        $debtorModel->status = $data['is_active'] == 1 ? ResStatus::$MAP['ACTIVE'] : ResStatus::$MAP['INACTIVE'];

                        $debtorModel->save();

                        foreach($data['details'] as $division)
                        {
                            $divisionDB = Division::where('code', $division['division_code'])
                                ->first();
                            if(!empty($divisionDB))
                            {
                                $debtorDivision = new DebtorDivision;
                                $debtorDivision->debtor_id = $debtorModel->id;
                                $debtorDivision->division_id = $divisionDB->id;
                                $debtorDivision->save();
                            }
                        }
                    }
                }
            }, 
            5 //reattempt times
        );
        return $debtorModel;
    }
}