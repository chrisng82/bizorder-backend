<?php

namespace App\Repositories;

use App\Uom;
use App\Item;
use App\ItemUom;
use App\ItemGroup01;
use App\ItemGroup02;
use App\ItemGroup03;
use App\Division;
use App\ItemDivision;
use App\ItemSalePrice;
use App\Repositories\ItemGroup01Repository;
use App\Repositories\ItemGroup01DivisionRepository;
use App\Services\Env\ItemType;
use App\Services\Env\ResStatus;
use App\Services\Env\RetrievalMethod;
use App\Services\Env\ScanMode;
use App\Services\Env\StorageClass;
use App\Services\Utils\RepositoryUtils;
use Illuminate\Support\Facades\DB;

class ItemRepository 
{
    static public function findRandomByType($itemType) 
	{
        $model = Item::where('status', ResStatus::$MAP['ACTIVE'])
            ->where('item_type', $itemType)
            ->orderByRaw('RAND()')
            ->first();

        return $model;
    }

    static public function findByPk($id) 
	{
        $item = Item::where('id', $id)
            ->first();

        return $item;
    }

    static public function findByPkWithPhoto($id) 
	{
        $item = Item::leftJoin('item_photos', 'items.id', '=', 'item_photos.item_id')
            ->where('items.id', $id)
            ->select('items.*', 'item_photos.path')
            ->first();

        return $item;
    }

    static public function findByPkWithPhotoAndUom($id)
    {
        $item = Item::find($id);
        $item->itemPhotos;
        $item->itemUoms;
        $item->unitUom;
        $item->caseUom;
        $item->itemPrices;

        return $item;
    }

    static public function findByCode($code) 
	{
        throw new Exception('please use ItemRepository::findByCodeAndDivision');
        // $item = Item::where('code', $code)
        //     ->first();

        // return $item;
    }
    
    static public function findByCodeAndDivision($code, $divisionId) 
	{
        $item = Item::select('items.*')
                ->leftJoin('item_divisions', 'items.id', '=', 'item_divisions.item_id')
                ->where('item_divisions.division_id', $divisionId)
                ->where('code', $code)
                ->first();
        return $item;
    }
    
    static public function findAllWithDeleteFilterStatus($sorts, $filters = array(), $pageSize = 20, $divisionId = 0,$status) 
	{
        $items = Item::join('item_uoms', 'items.id', '=', 'item_uoms.item_id')->select('items.*')->distinct();
        if($status == 'Not_Deleted'){
            $items->where('items.status', '!=', '818');
        }
        else{
            $items->where('items.status', '=', '818');
        }
        
        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'code') == 0)
            {
                $items->where('items.code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'ref_code_01') == 0)
            {
                $items->where('items.ref_code_01', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'desc_01') == 0)
            {
                $items->where('items.desc_01', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'desc_02') == 0)
            {
                $items->where('items.desc_02', 'LIKE', '%'.$filter['value'].'%');
            }
			if(strcmp($filter['field'], 'keyword') == 0)
            {
                $filter['value'] = explode(' ', $filter['value']);
                foreach($filter['value'] as $value) {
                    $items->where(function($query) use($value) {
                        $query->where('items.code', 'LIKE', '%'.$value.'%')
                        ->orWhere('items.desc_01', 'LIKE', '%'.$value.'%')
                        ->orWhere('items.desc_02', 'LIKE', '%'.$value.'%')
                        ->orWhere('item_uoms.barcode', 'LIKE', '%'.$value.'%');
                    });
				}
            }
            if(strcmp($filter['field'], 'division_id') == 0)
            {
                $divisionId = $divisionId > 0 ? $divisionId : $filter['value'];
            }
            if(strcmp($filter['field'], 'id') == 0)
            {
                if (is_array($filter['value'])) 
                {
                    $items->whereIn('items.id', $filter['value']);
                }
                else
                {
                    $items->where('items.id', '=', $filter['value']);
                }
            }
            if(strcmp($filter['field'], 'item_group_01_id') == 0)
            {
                if (is_array($filter['value'])) 
                {
                    $items->whereIn('item_group_01_id', $filter['value']);
                }
                else
                {
                    $items->where('item_group_01_id', '=', $filter['value']);
                }
            }
            if(strcmp($filter['field'], 'item_group_02_id') == 0)
            {
                if (is_array($filter['value'])) 
                {
                    $items->whereIn('item_group_02_id', $filter['value']);
                }
                else
                {
                    $items->where('item_group_02_id', '=', $filter['value']);
                }
            }
            if(strcmp($filter['field'], 'item_group_03_id') == 0)
            {
                if (is_array($filter['value'])) 
                {
                    $items->whereIn('items.item_group_03_id', $filter['value']);
                }
                else
                {
                    $items->where('items.item_group_03_id', '=', $filter['value']);
                }
            }
            if(strcmp($filter['field'], 'item_group_04_id') == 0)
            {
                if (is_array($filter['value'])) 
                {
                    $items->whereIn('items.item_group_04_id', $filter['value']);
                }
                else
                {
                    $items->where('items.item_group_04_id', '=', $filter['value']);
                }
            }
            if(strcmp($filter['field'], 'item_group_05_id') == 0)
            {
                if (is_array($filter['value'])) 
                {
                    $items->whereIn('items.item_group_05_id', $filter['value']);
                }
                else
                {
                    $items->where('items.item_group_05_id', '=', $filter['value']);
                }
            }
            if(strcmp($filter['field'], 'status') == 0)
            {
                $items->where('items.status', '=', $filter['value']);
            }
            if(strcmp($filter['field'], 'str_status') == 0)
            {
                if (strcmp(strtoupper($filter['value']), 'ACTIVE') == 0)
                {
                    $items->where('items.status', '=', ResStatus::$MAP['ACTIVE']);
                }
                if (strcmp(strtoupper($filter['value']), 'INACTIVE') == 0)
                {
                    $items->where('items.status', '=', ResStatus::$MAP['INACTIVE']);
                }
            }
            if(strcmp($filter['field'], 'item_group_01_code') == 0)
            {
                $items->leftJoin('item_group01s', 'item_group01s.id', '=', 'items.item_group_01_id');
                $items->where('item_group01s.code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'item_group_02_code') == 0)
            {
                $items->leftJoin('item_group02s', 'item_group02s.id', '=', 'items.item_group_02_id');
                $items->where('item_group02s.code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'item_group_03_code') == 0)
            {
                $items->leftJoin('item_group03s', 'item_group03s.id', '=', 'items.item_group_03_id');
                $items->where('item_group03s.code', 'LIKE', '%'.$filter['value'].'%');
            }
        }
		
        if ($divisionId > 0) {
            $items->leftJoin('item_divisions', 'item_divisions.item_id', '=', 'items.id');
            $items->where('item_divisions.division_id', '=', $divisionId);
        }
        
        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'code') == 0)
            {
                $items->orderBy('items.code', $sort['order']);
            }
            if(strcmp($sort['field'], 'desc_01') == 0)
            {
                $items->orderBy('items.desc_01', $sort['order']);
            }
            if(strcmp($sort['field'], 'desc_02') == 0)
            {
                $items->orderBy('items.desc_02', $sort['order']);
            }
        }
        if($pageSize > 0)
        {
            return $items
            ->paginate($pageSize);
        }
        else
        {
            return $items
            ->paginate(PHP_INT_MAX);
        }
    }


    static public function findAll($sorts, $filters = array(), $pageSize = 20, $divisionId = 0) 
	{
        $items = Item::join('item_uoms', 'items.id', '=', 'item_uoms.item_id')->select('items.*')->distinct();

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'code') == 0)
            {
                $items->where('items.code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'ref_code_01') == 0)
            {
                $items->where('items.ref_code_01', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'desc_01') == 0)
            {
                $items->where('items.desc_01', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'desc_02') == 0)
            {
                $items->where('items.desc_02', 'LIKE', '%'.$filter['value'].'%');
            }
			if(strcmp($filter['field'], 'keyword') == 0)
            {
                $filter['value'] = explode(' ', $filter['value']);
                foreach($filter['value'] as $value) {
                    $items->where(function($query) use($value) {
                        $query->where('items.code', 'LIKE', '%'.$value.'%')
                        ->orWhere('items.desc_01', 'LIKE', '%'.$value.'%')
                        ->orWhere('items.desc_02', 'LIKE', '%'.$value.'%')
                        ->orWhere('item_uoms.barcode', 'LIKE', '%'.$value.'%');
                    });
				}
            }
            if(strcmp($filter['field'], 'division_id') == 0)
            {
                $divisionId = $divisionId > 0 ? $divisionId : $filter['value'];
            }
            if(strcmp($filter['field'], 'id') == 0)
            {
                if (is_array($filter['value'])) 
                {
                    $items->whereIn('items.id', $filter['value']);
                }
                else
                {
                    $items->where('items.id', '=', $filter['value']);
                }
            }
            if(strcmp($filter['field'], 'item_group_01_id') == 0)
            {
                if (is_array($filter['value'])) 
                {
                    $items->whereIn('item_group_01_id', $filter['value']);
                }
                else
                {
                    $items->where('item_group_01_id', '=', $filter['value']);
                }
            }
            if(strcmp($filter['field'], 'item_group_02_id') == 0)
            {
                if (is_array($filter['value'])) 
                {
                    $items->whereIn('item_group_02_id', $filter['value']);
                }
                else
                {
                    $items->where('item_group_02_id', '=', $filter['value']);
                }
            }
            if(strcmp($filter['field'], 'item_group_03_id') == 0)
            {
                if (is_array($filter['value'])) 
                {
                    $items->whereIn('items.item_group_03_id', $filter['value']);
                }
                else
                {
                    $items->where('items.item_group_03_id', '=', $filter['value']);
                }
            }
            if(strcmp($filter['field'], 'item_group_04_id') == 0)
            {
                if (is_array($filter['value'])) 
                {
                    $items->whereIn('items.item_group_04_id', $filter['value']);
                }
                else
                {
                    $items->where('items.item_group_04_id', '=', $filter['value']);
                }
            }
            if(strcmp($filter['field'], 'item_group_05_id') == 0)
            {
                if (is_array($filter['value'])) 
                {
                    $items->whereIn('items.item_group_05_id', $filter['value']);
                }
                else
                {
                    $items->where('items.item_group_05_id', '=', $filter['value']);
                }
            }
            if(strcmp($filter['field'], 'status') == 0)
            {
                $items->where('items.status', '=', $filter['value']);
            }
            if(strcmp($filter['field'], 'str_status') == 0)
            {
                if (strcmp(strtoupper($filter['value']), 'ACTIVE') == 0)
                {
                    $items->where('items.status', '=', ResStatus::$MAP['ACTIVE']);
                }
                if (strcmp(strtoupper($filter['value']), 'INACTIVE') == 0)
                {
                    $items->where('items.status', '=', ResStatus::$MAP['INACTIVE']);
                }
            }
            if(strcmp($filter['field'], 'item_group_01_code') == 0)
            {
                $items->leftJoin('item_group01s', 'item_group01s.id', '=', 'items.item_group_01_id');
                $items->where('item_group01s.code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'item_group_02_code') == 0)
            {
                $items->leftJoin('item_group02s', 'item_group02s.id', '=', 'items.item_group_02_id');
                $items->where('item_group02s.code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'item_group_03_code') == 0)
            {
                $items->leftJoin('item_group03s', 'item_group03s.id', '=', 'items.item_group_03_id');
                $items->where('item_group03s.code', 'LIKE', '%'.$filter['value'].'%');
            }
        }
		
        if ($divisionId > 0) {
            $items->leftJoin('item_divisions', 'item_divisions.item_id', '=', 'items.id');
            $items->where('item_divisions.division_id', '=', $divisionId);
        }
        
        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'code') == 0)
            {
                $items->orderBy('items.code', $sort['order']);
            }
            if(strcmp($sort['field'], 'desc_01') == 0)
            {
                $items->orderBy('items.desc_01', $sort['order']);
            }
            if(strcmp($sort['field'], 'desc_02') == 0)
            {
                $items->orderBy('items.desc_02', $sort['order']);
            }
        }
        if($pageSize > 0)
        {
            return $items
            ->paginate($pageSize);
        }
        else
        {
            return $items
            ->paginate(PHP_INT_MAX);
        }
    }


    static public function syncItemSync01($data, $divisionId, $syncSettingHdrId) 
	{
        $division = DivisionRepository::findByPk($divisionId);
        if (empty($division)) {
            throw new \Exception('Division not found.');
        }

        $itemModel = DB::transaction
        (
            function() use ($data, $divisionId, $syncSettingHdrId)
            {
                // $itemModel = Item::firstOrNew(['code' => $data['code']]);
                $itemModel = Item::select('items.*')
                    ->leftJoin('item_divisions', 'items.id', '=', 'item_divisions.item_id')
                    ->where('item_divisions.division_id', $divisionId)
                    ->where('code', $data['code'])
                    ->first();
                
                if (!$itemModel) {
                    $itemModel = new Item;
                    $itemModel->code = $data['code'];
                }

                $itemModel->ref_code_01 = $data['ref_code'];
                $itemModel->desc_01 = $data['description'];
                $itemModel->desc_02 = $data['packing'];
                if($itemModel->id > 0)
                {
                    //update record
                    if(strcmp($data['status'], 'a') == 0)
                    {
                        $itemModel = self::updateItemAttributes($itemModel, $data, $syncSettingHdrId, $divisionId);
                        $itemModel = self::updateItemUomAttributes($itemModel, $data);
                        $itemUomModels = $itemModel->itemUomModels;
                        unset($itemModel->itemUomModels);

                        $itemModel->save();
                        foreach($itemUomModels as $itemUomModel)
                        {
                            $itemUomModel->item_id = $itemModel->id;
                            $itemUomModel->save();
                        }

                        if(count($data['divisions']) > 0)
                        {
                            foreach($data['divisions'] as $division)
                            {
                                $divisionDB = Division::where('code', $division['division_code'])
                                    ->first();
                                if(!empty($divisionDB))
                                {
                                    $itemDivisions = ItemDivision::where('item_id', $itemModel->id)
                                        ->where('division_id', $divisionDB->id)
                                        ->get();
                                    if(count($itemDivisions) == 0)
                                    {
                                        $itemDivision = new ItemDivision;
                                        $itemDivision->item_id = $itemModel->id;
                                        $itemDivision->division_id = $divisionDB->id;
                                        $itemDivision->save();
                                    }
                                }
                            }
                        }
                        else
                        {
                            $itemModel->status = ResStatus::$MAP['INACTIVE'];
                            $itemModel->save();
                        }

                        if(count($data['prices']) > 0)
                        {
                            foreach($data['prices'] as $itemPriceData)
                            {
                                //process the uom_code
                                $attributes = array('code'=>$itemPriceData['uom_code']);
                                $values = array('desc_01'=>$itemPriceData['uom_description']);
                                $uomModel = Uom::firstOrCreate($attributes, $values);

                                $itemPriceModel = ItemSalePrice::where('item_id', $itemModel->id)
                                    ->where('uom_id', $uomModel->id)
                                    ->whereDate('valid_from', $itemPriceData['valid_from'])
                                    ->whereDate('valid_to', $itemPriceData['valid_to'])
                                    ->first();
                                if ($itemPriceModel)
                                {
                                    if(bccomp($itemPriceModel->sale_price, $itemPriceData['unit_price'], 8) != 0)
                                    {
                                        //update
                                        $itemPriceModel->sale_price = $itemPriceData['unit_price'];
                                        $itemPriceModel->save();
                                    }
                                }
                                else
                                {
                                    //create
                                    $itemPriceModel = new ItemSalePrice();
                                    $itemPriceModel->item_id = $itemModel->id;
                                    $itemPriceModel->uom_id = $uomModel->id;
                                    $itemPriceModel->sale_price = $itemPriceData['unit_price'];
                                    $itemPriceModel->valid_from = $itemPriceData['valid_from'];
                                    $itemPriceModel->valid_to = $itemPriceData['valid_to'];
                                    $itemPriceModel->currency_id = 1;
                                    $itemPriceModel->save();
                                }
                            }
                        }
                    }
                    else if(strcmp($data['status'], 'i') == 0 || strcmp($data['status'], 'b') == 0) 
                    {
                        $itemModel->status = ResStatus::$MAP['INACTIVE'];
                        $itemModel->save();
                    }
                }
                else
                {
                    if(strcmp($data['status'], 'a') == 0
                    && count($data['divisions']) > 0)
                    {
                        $itemModel = self::updateItemAttributes($itemModel, $data, $syncSettingHdrId, $divisionId);
                        $itemModel = self::updateItemUomAttributes($itemModel, $data);
                        $itemUomModels = $itemModel->itemUomModels;
                        unset($itemModel->itemUomModels);

                        $itemModel->save();
                        foreach($itemUomModels as $itemUomModel)
                        {
                            $itemUomModel->item_id = $itemModel->id;
                            $itemUomModel->save();
                        }
                        
                        foreach($data['divisions'] as $division)
                        {
                            $divisionDB = Division::where('code', $division['division_code'])
                                ->first();
                            if(!empty($divisionDB))
                            {
                                $itemDivision = new ItemDivision;
                                $itemDivision->item_id = $itemModel->id;
                                $itemDivision->division_id = $divisionDB->id;
                                $itemDivision->save();
                            }
                        }

                        //process item price
                        foreach($data['prices'] as $itemPriceData)
                        {
                            //process the uom_code
                            $attributes = array('code'=>$itemPriceData['uom_code']);
                            $values = array('desc_01'=>$itemPriceData['uom_description']);
                            $uomModel = Uom::firstOrCreate($attributes, $values);

                            $itemPriceModel = new ItemSalePrice();
                            $itemPriceModel->item_id = $itemModel->id;
                            $itemPriceModel->uom_id = $uomModel->id;
                            $itemPriceModel->sale_price = $itemPriceData['unit_price'];
                            $itemPriceModel->valid_from = $itemPriceData['valid_from'];
                            $itemPriceModel->valid_to = $itemPriceData['valid_to'];
                            $itemPriceModel->currency_id = 1;
                            $itemPriceModel->save();
                        }
                    }
                }
            }, 
            5 //reattempt times
        );
        return $itemModel;
    }

    static private function updateItemAttributes($itemModel, $data, $syncSettingHdrId, $divisionId)
    {
        $itemBrandMapping = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdrId, 'item_brand_mapping');
        $itemManufaturerMapping = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdrId, 'item_manufacturer_mapping');
        $itemCategoryMapping = SyncSettingDtlRepository::findByHdrIdAndField($syncSettingHdrId, 'item_category_mapping');

        if (!empty($itemBrandMapping)) {
            $item_group_01_code = '';
            $item_group_01_desc = '';
            if ($itemBrandMapping->value == 'item_group') {
                $item_group_01_code = $data['item_group_code'];
                $item_group_01_desc = $data['item_group_description'];
            }
            else if ($itemBrandMapping->value == 'item_group_01') {
                $item_group_01_code = $data['item_group1_code'];
                $item_group_01_desc = $data['item_group1_description'];
            }
            else if ($itemBrandMapping->value == 'item_group_02') {
                $item_group_01_code = $data['item_group2_code'];
                $item_group_01_desc = $data['item_group2_description'];
            }

            if (empty($item_group_01_code))
            {
                $itemModel->item_group_01_id = 0;
            }
            else
            {
                //process the item_group_code
                $itemGroup01Model = ItemGroup01Repository::findByCodeAndDivision($item_group_01_code, $divisionId);
                if (empty($itemGroup01Model)) {
                    $itemGroup01Data = array();
                    $itemGroup01Data['code'] = $item_group_01_code;
                    $itemGroup01Data['desc_01'] = $item_group_01_desc;
                    $itemGroup01Model = ItemGroup01Repository::createModel($itemGroup01Data);
                    //create item group by division
                    $itemGroup01DivisionData = array();
                    $itemGroup01DivisionData['item_group01_id'] = $itemGroup01Model->id;
                    $itemGroup01DivisionData['division_id'] = $divisionId;
                    $itemGroup01DivisionModel = ItemGroup01DivisionRepository::createModel($itemGroup01DivisionData);
                }
                $itemModel->item_group_01_id = $itemGroup01Model->id;
            }
        }
        else {
            $itemModel->item_group_01_id = 0;
        }
        
        if (!empty($itemCategoryMapping)) {
            if ($itemCategoryMapping->value == 'item_group') {
                //process the item_group_code
                $attributes = array('code'=>$data['item_group_code']);
                $values = array('desc_01'=>$data['item_group_description']);
                $itemGroup02Model = ItemGroup02::firstOrCreate($attributes, $values);
                $itemModel->item_group_02_id = $itemGroup02Model->id;
            }
            else if ($itemCategoryMapping->value == 'item_group_01') {
                //process the item_group1_code
                $attributes = array('code'=>$data['item_group1_code']);
                $values = array('desc_01'=>$data['item_group1_description']);
                $itemGroup02Model = ItemGroup02::firstOrCreate($attributes, $values);
                $itemModel->item_group_02_id = $itemGroup02Model->id;
            }
            else if ($itemCategoryMapping->value == 'item_group_02') {
                //process the item_group2_code
                $attributes = array('code'=>$data['item_group2_code']);
                $values = array('desc_01'=>$data['item_group2_description']);
                $itemGroup02Model = ItemGroup02::firstOrCreate($attributes, $values);
                $itemModel->item_group_02_id = $itemGroup02Model->id;
            }
            else {
                $itemModel->item_group_02_id = 0;
            }
        }
        else {
            $itemModel->item_group_02_id = 0;
        }
        
        if (!empty($itemManufaturerMapping)) {
            if ($itemManufaturerMapping->value == 'item_group') {
                //process the item_group_code
                $attributes = array('code'=>$data['item_group_code']);
                $values = array('desc_01'=>$data['item_group_description']);
                $itemGroup03Model = ItemGroup03::firstOrCreate($attributes, $values);
                $itemModel->item_group_03_id = $itemGroup03Model->id;
            }
            else if ($itemManufaturerMapping->value == 'item_group_01') {
                //process the item_group1_code
                $attributes = array('code'=>$data['item_group1_code']);
                $values = array('desc_01'=>$data['item_group1_description']);
                $itemGroup03Model = ItemGroup03::firstOrCreate($attributes, $values);
                $itemModel->item_group_03_id = $itemGroup03Model->id;
            }
            else if ($itemManufaturerMapping->value == 'item_group_02') {
                //process the item_group2_code
                $attributes = array('code'=>$data['item_group2_code']);
                $values = array('desc_01'=>$data['item_group2_description']);
                $itemGroup03Model = ItemGroup03::firstOrCreate($attributes, $values);
                $itemModel->item_group_03_id = $itemGroup03Model->id;
            }
            else {
                $itemModel->item_group_03_id = 0;
            }
        }
        else {
            $itemModel->item_group_03_id = 0;
        }

        // //insert record
        // //process the item_group_code
        // $attributes = array('code'=>$data['item_group_code']);
        // $values = array('desc_01'=>$data['item_group_description']);
        // $itemGroup01Model = ItemGroup01::firstOrCreate($attributes, $values);

        // //process the item_group1_code
        // $attributes = array('code'=>$data['item_group1_code']);
        // $values = array('desc_01'=>$data['item_group1_description']);
        // $itemGroup02Model = ItemGroup02::firstOrCreate($attributes, $values);

        // //process the item_group2_code
        // $attributes = array('code'=>$data['item_group2_code']);
        // $values = array('desc_01'=>$data['item_group2_description']);
        // $itemGroup03Model = ItemGroup03::firstOrCreate($attributes, $values);

        //default value for EfiChain
        $itemModel->storage_class = StorageClass::$MAP['AMBIENT_01'];
        
        $itemModel->qty_scale = 0;
        $itemModel->item_type = strcmp($data['type'], 's') == 0 ? ItemType::$MAP['NON_STOCK_ITEM'] : ItemType::$MAP['STOCK_ITEM'];
        $itemModel->scan_mode = ScanMode::$MAP['ENTER_QTY'];
        $itemModel->retrieval_method = RetrievalMethod::$MAP['FIRST_EXPIRED_FIRST_OUT'];

        $itemModel->status = strcmp($data['status'], 'a') == 0 ? ResStatus::$MAP['ACTIVE'] : ResStatus::$MAP['INACTIVE'];
        
        return $itemModel;
    }
    
    static private function updateItemUomAttributes($itemModel, $data)
    {
        $unitItemUom = null;
        $largestItemUom = null;
        $itemUomModels = array();
        foreach($data['details'] as $itemUomData)
        {
            //process the uom_code
            $attributes = array('code'=>$itemUomData['uom_code']);
            $values = array('desc_01'=>$itemUomData['uom_description']);
            $uomModel = Uom::firstOrCreate($attributes, $values);

            if ($itemModel->id > 0) {
                $itemUomModel = ItemUomRepository::findByItemIdAndUomId($itemModel->id, $uomModel->id);
                if (!$itemUomModel) {
                    $itemUomModel = new ItemUom();
                    $itemUomModel->item_id = $itemModel->id;
                    $itemUomModel->uom_id = $uomModel->id;
                }
            }
            else {
                $itemUomModel = new ItemUom();
                $itemUomModel->uom_id = $uomModel->id;
            }

            $itemUomModel->barcode = $itemUomData['uom_barcode'];
            $itemUomModel->uom_rate = $itemUomData['uom_rate'];

            if(bccomp($itemUomModel->uom_rate, 1, 8) == 0)
            {
                $unitItemUom = $itemUomModel;
                if(empty($itemUomModel->barcode)
                && !empty($data['barcode']))
                {
                    $itemUomModel->barcode = $data['barcode'];
                }
            } 
            elseif(empty($largestItemUom) || bccomp($itemUomModel->uom_rate, $largestItemUom->uom_rate, 8) > 0)
            {
                $largestItemUom = $itemUomModel;
            }
            $itemUomModels[] = $itemUomModel;
        }

        $itemModel->unit_uom_id = 0;
        if(!empty($unitItemUom))
        {
            $itemModel->unit_uom_id = $unitItemUom->uom_id;
        }
        $itemModel->case_uom_id = 0;
        $itemModel->case_uom_rate = 35000;
        if(!empty($largestItemUom))
        {
            $itemModel->case_uom_id = $largestItemUom->uom_id;
            $itemModel->case_uom_rate = $largestItemUom->uom_rate;
        }
        $itemModel->pallet_uom_rate = 0;

        $itemModel->case_ext_length = bcmul($data['length'], 10);
        $itemModel->case_ext_width = bcmul($data['width'], 10);
        $itemModel->case_ext_height = bcmul($data['height'], 10);
        $itemModel->case_gross_weight = 0;
        if(!empty($largestItemUom))
        {
            $ctnGram = bcmul($data['gross_weight'], $largestItemUom->uom_rate, 8);
            $itemModel->case_gross_weight = bcdiv($ctnGram, 1000, 8);
        }

        $itemModel->itemUomModels = $itemUomModels;
        return $itemModel;
    }

    static public function select2($search, $filters, $pageSize = 10) 
	{
        $items = Item::select('items.*')
        ->where(function($q) use($search) {
            $q->orWhere('desc_01', 'LIKE', '%' . $search . '%')
            ->orWhere('desc_02', 'LIKE', '%' . $search . '%')
            ->orWhere('code', 'LIKE', '%' . $search . '%');
        });
        if(count($filters) >= 1)
        {
            foreach ($filters as $filter)
            {
                if(strcmp($filter['field'], 'division_id') == 0)
                {
                    $items->leftJoin('item_divisions', 'item_divisions.item_id', '=', 'items.id');
                    $items->where('item_divisions.division_id', '=', $filter['value']);
                }
                else
                {
                    $items = $items->where($filter);
                }
            }
        }
        return $items
            ->orderBy('code', 'ASC')
            ->paginate($pageSize);
    }

    static public function findTop() 
	{
        $model = Item::first();

        return $model;
    }

    static public function syncSlsOrdSync01($division, $uom, $data) 
	{
        $model = DB::transaction
        (
            function() use ($division, $uom, $data)
            {
                //check item
                $item = Item::where('code', $data['item_code'])
                    ->first();
                if(empty($item))
                {
                    $attributes = array('code'=>$data['group_code']);
                    $values = array('desc_01'=>$data['group_code']);
                    $itemGroup01 = ItemGroup01::firstOrCreate($attributes, $values);

                    //process the item_group1_code
                    $attributes = array('code'=>$data['group1_code']);
                    $values = array('desc_01'=>$data['group1_code']);
                    $itemGroup02 = ItemGroup02::firstOrCreate($attributes, $values);

                    //process the item_group2_code
                    $attributes = array('code'=>$data['group2_code']);
                    $values = array('desc_01'=>$data['group2_code']);
                    $itemGroup03 = ItemGroup03::firstOrCreate($attributes, $values);

                    $item = new Item;
                    $item->code = $data['item_code'];
                    $item->ref_code_01 = '';
                    $item->desc_01 = $data['item_description'];
                    $item->desc_02 = '';
                    $item->storage_class = StorageClass::$MAP['AMBIENT_01'];
                    $item->item_group_01_id = $itemGroup01->id;
                    $item->item_group_02_id = $itemGroup02->id;
                    $item->item_group_03_id = $itemGroup03->id;
                    $item->item_group_04_id = 0;
                    $item->item_group_05_id = 0;
                    $item->item_group_01_code = $itemGroup01->code;
                    $item->item_group_02_code = $itemGroup02->code;
                    $item->item_group_03_code = $itemGroup03->code;
                    $item->item_group_04_code = '';
                    $item->item_group_05_code = '';
                    $item->s_storage_class = StorageClass::$MAP['AMBIENT_01'];
                    $item->s_item_group_01_id = $itemGroup01->id;
                    $item->s_item_group_02_id = $itemGroup02->id;
                    $item->s_item_group_03_id = $itemGroup03->id;
                    $item->s_item_group_04_id = 0;
                    $item->s_item_group_05_id = 0;
                    $item->s_item_group_01_code = $itemGroup01->code;
                    $item->s_item_group_02_code = $itemGroup02->code;
                    $item->s_item_group_03_code = $itemGroup03->code;
                    $item->s_item_group_04_code = '';
                    $item->s_item_group_05_code = '';
                    $item->qty_scale = 0;
                    $item->item_type = ItemType::$MAP['STOCK_ITEM'];
                    $item->scan_mode = ScanMode::$MAP['ENTER_QTY'];
                    $item->retrieval_method = RetrievalMethod::$MAP['FIRST_EXPIRED_FIRST_OUT'];
                    $item->unit_uom_id = Uom::$UNIT;
                    $item->case_uom_id = Uom::$CASE;
                    $item->case_uom_rate = 35000;
                    $item->pallet_uom_rate = 0;
                    $item->case_ext_length = 320;
                    $item->case_ext_width = 230;
                    $item->case_ext_height = 230;
                    $item->case_gross_weight = 5.904;
                    $item->cases_per_pallet_length = 1;
                    $item->cases_per_pallet_width = 1;
                    $item->no_of_layers = 1;
                    $item->is_length_allowed_vertical = 0;
                    $item->is_width_allowed_vertical = 0;
                    $item->is_height_allowed_vertical = 1;
                    $item->status = ResStatus::$MAP['ACTIVE'];
                    $item->save();

                    $itemUom = new ItemUom;
                    $itemUom->item_id = $item->id;
                    $itemUom->uom_id = $uom->id;
                    $itemUom->barcode = '';
                    $itemUom->uom_rate = $data['uom_rate'];
                    $itemUom->save();
                }

                return $item;
            }
        );
        return $model;
    }

    static public function createModel($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = new Item;
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return $model;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function updateModel($data) 
	{
        $result = DB::transaction
        (
            function() use ($data)
            {
                //update Item
                $model = Item::lockForUpdate()->find($data['id']);
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return array(
                    'model' => $model
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

	static public function findItemGroup01($search, $filters, $sorts, $pageSize = 20) {
        $items = Item::select('item_group01s.*', 'item_group01_divisions.sequence')
			->distinct()
            ->leftJoin('item_group01s', 'item_group01s.id', '=', 'items.item_group_01_id')
			->leftJoin('item_group01_divisions', 'item_group01_divisions.item_group01_id', '=', 'item_group01s.id')
            ->leftJoin('item_group02s', 'item_group02s.id', '=', 'items.item_group_02_id');
			
        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'item_group_02_id') == 0)
            {
                $items->where('item_group02s.id', '=', $filter['value']);
            }
            if(strcmp($filter['field'], 'division_id') == 0)
            {
                $items->where('item_group01_divisions.division_id', '=', $filter['value']);
            }
            
        }

        $items->whereNotNull('item_group02s.id')
        ->where('items.status', '=', 100);

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'sequence') == 0)
            {
                $items->orderBy('item_group01_divisions.sequence', $sort['order']);
            }
        }

		return $items->paginate($pageSize);
	}

	static public function findItemGroup02($search, $filters, $pageSize = 20) {
        $items = Item::select('item_group02s.*')
			->distinct()
			->leftJoin('item_divisions', 'item_divisions.item_id', '=', 'items.id')
            ->leftJoin('item_group02s', 'item_group02s.id', '=', 'items.item_group_02_id')
            ->leftJoin('item_group01s', 'item_group01s.id', '=', 'items.item_group_01_id')
			->whereNotNull('item_group02s.id')
            ->where('items.status', '=', 100);
        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'division_id') == 0)
            {
                $items->where('item_divisions.division_id', '=', $filter['value']);
            }
            if(strcmp($filter['field'], 'item_group_01_id') == 0)
            {
                $items->where('item_group01s.id', '=', $filter['value']);
            }
		}			
		return $items->paginate($pageSize);
	}

	static public function findItemGroup03($search, $filters, $pageSize = 20) {
        $items = Item::select('item_group03s.*')
			->distinct()
			->leftJoin('item_divisions', 'item_divisions.item_id', '=', 'items.id')
			->leftJoin('item_group03s', 'item_group03s.id', '=', 'items.item_group_03_id')
            ->whereNotNull('item_group03s.id')            
            ->where('items.status', '=', 100);
        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'division_id') == 0)
            {
                $items->where('item_divisions.division_id', '=', $filter['value']);
            }
		}			
		return $items->paginate($pageSize);
	}
}