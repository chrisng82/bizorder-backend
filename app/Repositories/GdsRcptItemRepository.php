<?php

namespace App\Repositories;

use App\GdsRcptItem;
use Illuminate\Support\Facades\DB;

class GdsRcptItemRepository 
{
    static public function findAllByHdrId($hdrId) 
	{
        $gdsRcptItems = GdsRcptItem::where('hdr_id', $hdrId)
            ->get();

        return $gdsRcptItems;
    }

    static public function findAllByHdrIds($hdrIds) 
	{
        $gdsRcptItems = GdsRcptItem::whereIn('hdr_id', $hdrIds)
            ->get();

        return $gdsRcptItems;
    }
}