<?php

namespace App\Repositories;

use App\PickListHdr;
use App\QuantBalRsvdTxn;
use Illuminate\Support\Carbon;

class QuantBalRsvdTxnRepository 
{
    static public function queryReservedUnitQty($quantBalId) 
	{
        $reservedUnitQty = 0;
        
        $quantBalRsvdTxn = QuantBalRsvdTxn::selectRaw('SUM(sign * unit_qty) as unit_qty')
            ->where('quant_bal_id', $quantBalId)
            ->first();
        if(!empty($quantBalRsvdTxn))
        {
            $reservedUnitQty = $quantBalRsvdTxn->unit_qty;
        }

        return $reservedUnitQty;
    }

    static public function queryEarliestTimestamp($docHdrType, $docHdrId) 
	{
        $timestamp = Carbon::create(1970, 1, 1, 0, 0, 0, 'UTC');
        $quantBalRsvdTxn = QuantBalRsvdTxn::where('doc_hdr_type', $docHdrType)
            ->where('doc_hdr_id', $docHdrId)
            ->orderBy('created_at', 'ASC')
            ->first();
        if(!empty($quantBalRsvdTxn))
        {
            $timestamp = $quantBalRsvdTxn->created_at;
        }
        return $timestamp;
    }

    static public function queryReservedCapacity($storageBinId) 
	{
        $cubicMeter = 0;
        $grossWeight = 0;

        $quantBalRsvdTxns = QuantBalRsvdTxn::selectRaw('IFNULL(SUM(quant_bal_rsvd_txns.unit_qty / items.case_uom_rate * case_ext_length * case_ext_width * case_ext_height / 1000000000), 0) AS cubic_meter')
            ->selectRaw('IFNULL(SUM(quant_bal_rsvd_txns.unit_qty / items.case_uom_rate * case_gross_weight), 0) AS gross_weight')
            ->leftJoin('items', 'items.id', '=', 'quant_bal_rsvd_txns.item_id')    
            ->where('storage_bin_id', $storageBinId)
            ->get();
        foreach($quantBalRsvdTxns as $quantBalRsvdTxn)
        {
            $cubicMeter = $quantBalRsvdTxn->cubic_meter;
            $grossWeight = $quantBalRsvdTxn->gross_weight;
        }

        return array(
            'cubic_meter' => $cubicMeter,
            'gross_weight' => $grossWeight
        );
    }

    static public function queryDocsByStorageBinIds($storageBinIds) 
	{
        $docHdrDatas = array();
        $quantBalRsvdTxns = QuantBalRsvdTxn::select('doc_hdr_type', 'doc_hdr_id')
        ->whereIn('storage_bin_id', $storageBinIds)
        ->distinct()
        ->get();
        foreach($quantBalRsvdTxns as $quantBalRsvdTxn)
        {
            $docHdr = $quantBalRsvdTxn->docHdr;
            if(!empty($docHdr))
            {
                $data = array();
                $data['doc_code'] = $docHdr->doc_code;

                $docHdrDatas[] = $data;
            }
        }
        return $docHdrDatas;
    }
}