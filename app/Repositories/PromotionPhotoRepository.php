<?php

namespace App\Repositories;

use App\Models\Promotion;
use App\Services\Env\ResType;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use JD\Cloudder\Facades\Cloudder;

class PromotionPhotoRepository 
{
    static public function savePromotionPhoto($data) 
	{
        // $itemPhotoModel = DB::transaction
        // (
        //     function() use ($data)
        //     {
                $promotionPhotoModel = Promotion::find($data['promotionId']);
                
                if($promotionPhotoModel)
                {
                    //Replace
                    $promotionPhotoModel->photo_desc_01 = $data['photo_desc_01'];
                    $promotionPhotoModel->photo_desc_02 = $data['photo_desc_02'];
                    if (isset($data['old_filename']) && !empty($data['old_filename']))
                    {
                        $promotionPhotoModel->old_filename = $data['old_filename'];
                        list($width, $height) = getimagesize($data['old_filename']);
                        $promotionPhotoModel->old_width = $width;
                        $promotionPhotoModel->old_height = $height;
                    }
                    else if (isset($data['blob']) && !empty($data['blob']))
                    {
                        $temp = tempnam(sys_get_temp_dir(), 'TMP_');
                        file_put_contents($temp, $data['blob']);
                        $promotionPhotoModel->old_filename = $temp;
                        list($width, $height) = getimagesize($temp);
                        $promotionPhotoModel->old_width = $width;
                        $promotionPhotoModel->old_height = $height;
                        unset($data['blob']);
                    }
                    else
                    {
                        throw new Exception('no image to save');
                    }

                    $promotionPhotoModel->asset_url = '';
                    
                    $promotionPhotoModel->save();
                }
                else 
                {
                    throw new Exception('promotion does not exist');
                }

                // $filename = ResType::$MAP['ITEM_PHOTO'].'_'.$promotionPhotoModel->id.'.'.$imageExt;
                // $path = Storage::put('photos/'.$filename, $data['blob']);
                
                Cloudder::upload($promotionPhotoModel->old_filename, null);
                $public_id = Cloudder::getPublicId();
                $image_url= Cloudder::show($public_id, ["width" => $promotionPhotoModel->old_width, "height"=>$promotionPhotoModel->old_height]);
                //save to uploads directory
                // $image = $data['blob'];
                // $image->move(public_path("uploads"), $promotionPhotoModel->desc_01);

                $promotionPhotoModel->asset_url = $image_url;
                $promotionPhotoModel->photo_desc_02 = $public_id;
                $promotionPhotoModel->save();

        //         return $promotionPhotoModel;
        //     }, 
        //     5 //reattempt times
        // );
        return $promotionPhotoModel;
    }

    static public function deletePromotionPhotos($promotionId) 
	{
        $promotion = Promotion::find($promotionId);
        $public_ids = array();
        $public_ids[] = $promotion->photo_desc_02;

        $result = DB::transaction
        (
            function() use ($promotion, $public_ids)
            {
                if($promotion !== false)
                {
                    $promotion->asset_url = null;
                    $promotion->photo_desc_01 = null;
                    $promotion->photo_desc_02 = null;
                    $promotion->old_filename = null;
                    $promotion->old_width = null;
                    $promotion->old_height = null;
                    $promotion->save();
                }

                if($promotion !== false)
                {
                    Cloudder::destroyImages($public_ids, array());
                }
            }, 
            5 //reattempt times
        );

        return $promotion->toArray();
    }
}