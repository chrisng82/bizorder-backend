<?php

namespace App\Repositories;

use App\PackListDtl;
use Illuminate\Support\Facades\DB;

class PackListDtlRepository 
{
    static public function findAllByHdrId($hdrId) 
	{
        $packListDtls = PackListDtl::where('hdr_id', $hdrId)
            ->orderBy('line_no')
            ->get();

        return $packListDtls;
    }

    static public function findAllByHdrIds($hdrIds = array()) 
	{
        $packListDtls = PackListDtl::whereIn('hdr_id', $hdrIds)
            ->orderBy('item_id', 'ASC')
            ->get();

        return $packListDtls;
    }

    static public function queryShipmentDetails($hdrId, $sortfield = 'item_group_01_code', $sortOrder = 'ASC') 
	{
        $packListDtls = PackListDtl::select(
                'pack_list_dtls.hdr_id AS hdr_id',
                'pack_list_dtls.id AS id',
                'pack_list_dtls.whse_job_type AS whse_job_type',
                'pack_list_dtls.whse_job_code AS whse_job_code',
                'items.id AS item_id',
                'items.code AS item_code',
                'items.ref_code_01 AS item_ref_code_01',
                'items.desc_01 AS item_desc_01',
                'items.desc_02 AS item_desc_02',
                'items.storage_class AS storage_class',
                'items.item_group_01_code AS item_group_01_code',
                'items.item_group_02_code AS item_group_02_code',
                'items.item_group_03_code AS item_group_03_code',
                'items.item_group_04_code AS item_group_04_code',
                'items.item_group_05_code AS item_group_05_code',
                'items.s_storage_class AS s_storage_class',
                'items.s_item_group_01_code AS s_item_group_01_code',
                'items.s_item_group_02_code AS s_item_group_02_code',
                'items.s_item_group_03_code AS s_item_group_03_code',
                'items.s_item_group_04_code AS s_item_group_04_code',
                'items.s_item_group_05_code AS s_item_group_05_code',
                'item_batches.batch_serial_no AS batch_serial_no',
                'item_batches.expiry_date AS expiry_date',
                'item_batches.receipt_date AS receipt_date',
                'storage_bins.code AS storage_bin_code',
                'storage_rows.code AS storage_row_code',
                'storage_bays.code AS storage_bay_code',
                'quant_bals.bay_sequence AS item_bay_sequence'
            )
            //unit_qty
            ->selectRaw('SUM(pack_list_dtls.qty * pack_list_dtls.uom_rate) AS unit_qty')
            //case_qty
            ->selectRaw('SUM(pack_list_dtls.qty * pack_list_dtls.uom_rate / items.case_uom_rate) AS case_qty')
            //gross_weight, kg
            ->selectRaw('SUM(pack_list_dtls.qty * pack_list_dtls.uom_rate / items.case_uom_rate * items.case_gross_weight) AS gross_weight')
            //cubic_meter, meter
            ->selectRaw('SUM(pack_list_dtls.qty * pack_list_dtls.uom_rate / items.case_uom_rate * items.case_ext_length * items.case_ext_width * items.case_ext_height / 1000000000) AS cubic_meter')
            ->leftJoin('items', 'items.id', '=', 'pack_list_dtls.item_id')
            ->leftJoin('quant_bals', 'quant_bals.id', '=', 'pack_list_dtls.quant_bal_id')
            ->leftJoin('item_batches', 'item_batches.id', '=', 'quant_bals.item_batch_id')
            ->leftJoin('storage_bins', 'storage_bins.id', '=', 'quant_bals.storage_bin_id')
            ->leftJoin('storage_rows', 'storage_rows.id', '=', 'quant_bals.storage_row_id')
            ->leftJoin('storage_bays', 'storage_bays.id', '=', 'quant_bals.storage_bay_id')
            ->where('hdr_id', $hdrId)
            ->groupBy(
                'item_code'
            )
            ->orderBy('item_bay_sequence', 'ASC')
            ->when(in_array($sortfield, array('item_code', 'item_ref_code_01', 'item_desc_01', 'item_desc_02')) === FALSE, function ($query) use($sortfield, $sortOrder) {
                return $query->orderBy($sortfield, $sortOrder);
            })
            ->orderBy('item_code', $sortOrder)
            ->get();
        return $packListDtls;
    }
}