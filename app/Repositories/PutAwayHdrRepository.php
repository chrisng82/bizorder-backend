<?php

namespace App\Repositories;

use App\Services\Utils\RepositoryUtils;
use App\PutAwayHdr;
use App\PutAwayDtl;
use App\PutAwayCDtl;
use App\PutAwayInbOrd;
use App\InbOrdHdr;
use App\DocTxnFlow;
use App\DocTxnVoid;
use App\QuantBalAdvTxn;
use App\QuantBalTxn;
use App\Repositories\DocNoRepository;
use App\Repositories\QuantBalRepository;
use App\Repositories\PutAwayDtlRepository;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Services\Env\WhseJobType;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\DB;

class PutAwayHdrRepository 
{
    static public function txnFindByPk($hdrId) 
	{
        $model = DB::transaction
        (
            function() use ($hdrId)
            {
                $putAwayHdr = PutAwayHdr::where('id', $hdrId)
                    ->first();
                return $putAwayHdr;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function commitToWip($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = PutAwayHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('PutAway.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\PutAwayHdr::class, $hdrModel->id);
                    throw $exc;
                }

                $dtlModels = PutAwayDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->orderBy('line_no')
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('PutAway.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\PutAwayHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to WIP
                $hdrModel->doc_status = DocStatus::$MAP['WIP'];
                $hdrModel->save();
                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitToComplete($hdrId, $isNonZeroBal)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId, $isNonZeroBal)
            {
                $hdrModel = PutAwayHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('PutAway.doc_status_is_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\PutAwayHdr::class, $hdrModel->id);
                    throw $exc;
                }
                if($hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('PutAway.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\PutAwayHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to COMPLETE
                $hdrModel->doc_status = DocStatus::$MAP['COMPLETE'];
                $hdrModel->save();

                //2) delete all the adv txn
                $quantBalAdvTxns = QuantBalAdvTxn::where(array(
                        'doc_hdr_type' => \App\PutAwayHdr::class,
                        'doc_hdr_id' => $hdrId
                    ))
                    ->lockForUpdate()
                    ->get();
                
                DB::table('quant_bal_adv_txns')
                    ->where(array(
                        'doc_hdr_type' => \App\PutAwayHdr::class,
                        'doc_hdr_id' => $hdrId
                    ))
                    ->delete();

                $dtlModels = PutAwayDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('PutAway.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\PutAwayHdr::class, $hdrModel->id);
                    throw $exc;
                }
                $dtlModels->load('item');
                foreach($dtlModels as $dtlModel)
                {
                    $item = $dtlModel->item;

                    if($dtlModel->to_storage_bin_id == 0)
                    {
                        //only DRAFT or above can transition to COMPLETE
                        $exc = new ApiException(__('PutAway.to_storage_bin_is_required', ['lineNo'=>$dtlModel->line_no]));
                        $exc->addData(\App\PutAwayDtl::class, $dtlModel->id);
                        throw $exc;
                    }

                    //this is stock transfer
                    QuantBalRepository::commitStockTransfer(
                        0, $isNonZeroBal, $item, $dtlModel->quant_bal_id, 
                        $dtlModel->to_storage_bin_id, $dtlModel->to_handling_unit_id, 
                        $hdrModel->doc_date, \App\PutAwayHdr::class, $hdrModel->id, 
                        $dtlModel->id, $dtlModel->uom_id, $dtlModel->uom_rate, $dtlModel->qty);
                }
                return $hdrModel;                
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertCompleteToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = PutAwayHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                {
                    $exc = new ApiException(__('PutAway.doc_status_is_not_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\PutAwayHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                QuantBalRepository::revertStockTransaction(\App\PutAwayHdr::class, $hdrModel->id);
                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertWipToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = PutAwayHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['WIP'])
                {
                    $exc = new ApiException(__('PutAway.doc_status_is_not_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\PutAwayHdr::class, $hdrModel->id);
                    throw $exc;
                }

                //1) verify the toWhseJobs, make sure it is not WIP(download to mobile)
                $docTxnFlows = DocTxnFlow::where(array(
                    'fr_doc_hdr_type' => get_class($hdrModel),
                    'fr_doc_hdr_id' => $hdrModel->id,
                    'to_doc_hdr_type' => \App\WhseJobHdr::class
                    ))
                    ->get();
                foreach($docTxnFlows as $docTxnFlow)
                {
                    $toWhseJobHdr = \App\WhseJobHdr::where('id', $docTxnFlow->to_doc_hdr_id)
                        ->lockForUpdate()
                        ->first();
                    if($toWhseJobHdr->doc_status == DocStatus::$MAP['DRAFT']
                    || $toWhseJobHdr->doc_status == DocStatus::$MAP['WIP'])
                    {
                        $exc = new ApiException(__('WhseJob.job_status_is_draft_or_wip', ['docCode'=>$toWhseJobHdr->doc_code]));
                        $exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
                        throw $exc;
                    }
                }
                
                //2) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                //revert the putAwayDtls back to original
                $dtlModels = PutAwayDtl::where('hdr_id', $hdrModel->id)
                ->lockForUpdate()
                ->get();
                //$dtlModels->load('item');
                foreach($dtlModels as $dtlModel)
                {
                    $dtlModel->to_storage_bin_id = 0;
                    $dtlModel->whse_job_type = WhseJobType::$MAP['NULL'];
                    $dtlModel->save();
                }

                //2) delete all the adv txn
                $quantBalAdvTxns = QuantBalAdvTxn::where(array(
                    'doc_hdr_type' => \App\PutAwayHdr::class,
                    'doc_hdr_id' => $hdrId
                ))
                ->lockForUpdate()
                ->get();

                DB::table('quant_bal_adv_txns')
                    ->where(array(
                        'doc_hdr_type' => \App\PutAwayHdr::class,
                        'doc_hdr_id' => $hdrId
                    ))
                    ->delete();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function createProcess($procType, $docNoId, $hdrData, $dtlDataList, $inbOrdHdrIds, $docTxnFlowDataList) 
	{
        $putAwayHdr = DB::transaction
        (
            function() use ($procType, $docNoId, $hdrData, $dtlDataList, $inbOrdHdrIds, $docTxnFlowDataList)
            {
                $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

                $hdrModel = new PutAwayHdr;
                $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
                $hdrModel->doc_code = $newCode;
                $hdrModel->proc_type = $procType;
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();
                
                foreach($dtlDataList as $dtlData)
                {
                    $dtlModel = new PutAwayDtl;
                    $dtlModel = RepositoryUtils::dataToModel($dtlModel, $dtlData);
                    $dtlModel->hdr_id = $hdrModel->id;
                    $dtlModel->save();

                    //create a carbon copy
                    $cDtlModel = new PutAwayCDtl;
                    $cDtlModel = RepositoryUtils::dataToModel($cDtlModel, $dtlData);
                    $cDtlModel->hdr_id = $hdrModel->id;
                    $cDtlModel->save();
                }

                foreach($inbOrdHdrIds as $inbOrdHdrId)
                {
                    $putAwayInbOrdModel =  new PutAwayInbOrd;
                    $putAwayInbOrdModel->hdr_id = $hdrModel->id;
                    $putAwayInbOrdModel->inb_ord_hdr_id = $inbOrdHdrId;
                    $putAwayInbOrdModel->save();
                }

                //process docTxnFlowDataList
                foreach($docTxnFlowDataList as $docTxnFlowData)
                {
                    $frDocHdrModel = $docTxnFlowData['fr_doc_hdr_type']::where('id', $docTxnFlowData['fr_doc_hdr_id'])
                    ->lockForUpdate()
                    ->first();
                    if($frDocHdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                    {
                        $exc = new ApiException(__('PutAway.fr_doc_is_not_complete', ['docType'=>$docTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$docTxnFlowData['fr_doc_hdr_code']]));
                        $exc->addData($docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_code']);
                        throw $exc;
                    }

                    //verify the frDoc is not closed for this procType
                    $tmpDocTxnFlow = DocTxnFlowRepository::findByProcTypeAndFrHdrId($procType, $docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_id']);
                    if(!empty($tmpDocTxnFlow))
                    {
                        $exc = new ApiException(__('PutAway.fr_doc_is_closed', ['docType'=>$docTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$docTxnFlowData['fr_doc_hdr_code']]));
                        $exc->addData($docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_id']);
                        throw $exc;
                    }
                    
                    $docTxnFlowModel = new DocTxnFlow;
                    $docTxnFlowModel = RepositoryUtils::dataToModel($docTxnFlowModel, $docTxnFlowData);
                    $docTxnFlowModel->proc_type = $procType;
                    $docTxnFlowModel->to_doc_hdr_type = \App\PutAwayHdr::class;
                    $docTxnFlowModel->to_doc_hdr_id = $hdrModel->id;
                    $docTxnFlowModel->save();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $putAwayHdr;
    }

    static public function verifyTxn()
    {
        $invalidRows = array();

        $page = 1;
        $limit = 100;
        while(true)
        {
            $offset = ($page - 1) * $limit;
            $putAwayHdrs = PutAwayHdr::where('doc_status', DocStatus::$MAP['COMPLETE'])
            ->offset($offset)
            ->limit($limit)
            ->get();
            foreach($putAwayHdrs as $putAwayHdr)
            {
                $quantBalTxnsHashByDtlId = array();

                $quantBalTxns = QuantBalTxn::where('doc_hdr_type', PutAwayHdr::class)
                ->where('doc_hdr_id', $putAwayHdr->id)
                ->get();
                foreach($quantBalTxns as $quantBalTxn)
                {
                    $tmpQuantBalTxns = array();
                    if(array_key_exists($quantBalTxn->doc_dtl_id, $quantBalTxnsHashByDtlId))
                    {
                        $tmpQuantBalTxns = $quantBalTxnsHashByDtlId[$quantBalTxn->doc_dtl_id];
                    }

                    $tmpQuantBalTxns[] = $quantBalTxn;
                    $quantBalTxnsHashByDtlId[$quantBalTxn->doc_dtl_id] = $tmpQuantBalTxns;                    
                }

                $putAwayDtls = PutAwayDtl::where('hdr_id', $putAwayHdr->id)
                ->get();
                foreach($putAwayDtls as $putAwayDtl)
                {
                    $ttlTxnUnitQty = 0;
                    if(array_key_exists($putAwayDtl->id, $quantBalTxnsHashByDtlId))
                    {
                        $quantBalTxns = $quantBalTxnsHashByDtlId[$putAwayDtl->id];
                        foreach($quantBalTxns as $quantBalTxn)
                        {
                            $ttlUnitQty = bcmul($quantBalTxn->sign, $quantBalTxn->unit_qty, 10);
                            $ttlTxnUnitQty = bcadd($ttlTxnUnitQty, $ttlUnitQty, 10);
                        }

                        $ttlDtlUnitQty = bcmul($putAwayDtl->qty, $putAwayDtl->uom_rate, 10);
                        if(bccomp(0, $ttlTxnUnitQty, 10) != 0)
                        {
                            $invalidRows[] = 'PutAwayDtl ['.$putAwayDtl->id.'] qty['.$ttlDtlUnitQty.'] not tally with txn['.$ttlTxnUnitQty.']';
                        }
                    }
                    else
                    {
                        $invalidRows[] = 'PutAwayDtl ['.$putAwayDtl->id.'] qty['.$ttlDtlUnitQty.'] not tally with txn[NULL]';
                    }
                }
            }

            if(count($putAwayHdrs) == 0)
            {
                break;
            }

            $page++;
        }        

        return $invalidRows;
    }

    static public function findAllNotExistWhseJob1501Txn($siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $qStatuses = array(DocStatus::$MAP['DRAFT'], DocStatus::$MAP['WIP']); 
        $putAwayHdrs = PutAwayHdr::select('put_away_hdrs.*')
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('doc_txn_flows')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['WHSE_JOB_15_01'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\PutAwayHdr::class)
                    ->whereRaw('doc_txn_flows.fr_doc_hdr_id = put_away_hdrs.id')
                    ->where('doc_txn_flows.is_closed', 1);
            })
            ->where('site_flow_id', $siteFlowId)
            ->whereIn('doc_status', $qStatuses);
        
        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $putAwayHdrs->orderBy('doc_code', $sort['order']);
            }
        }
        return $putAwayHdrs
            ->paginate($pageSize);
    }

    static public function findAllByHdrIds($hdrIds) 
	{
        $putAwayHdrs = PutAwayHdr::whereIn('id', $hdrIds)
            ->get();

        return $putAwayHdrs;
    }

    static public function updateDetails($putAwayHdrData, $putAwayDtlArray, $delPutAwayDtlArray) 
	{
        $delPutAwayDtlIds = array();
        foreach($delPutAwayDtlArray as $delPutAwayDtlData)
        {
            $delPutAwayDtlIds[] = $delPutAwayDtlData['id'];
        }
        
        $result = DB::transaction
        (
            function() use ($putAwayHdrData, $putAwayDtlArray, $delPutAwayDtlIds)
            {
                //update putAwayHdr
                $putAwayHdrModel = PutAwayHdr::lockForUpdate()->find($putAwayHdrData['id']);
                if($putAwayHdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //WIP and COMPLETE status can not be updated
                    $exc = new ApiException(__('PutAway.doc_status_is_complete', ['docCode'=>$putAwayHdrModel->doc_code]));
                    $exc->addData(\App\PutAwayHdr::class, $putAwayHdrModel->id);
                    throw $exc;
                }
                $putAwayHdrModel = RepositoryUtils::dataToModel($putAwayHdrModel, $putAwayHdrData);
                $putAwayHdrModel->save();

                //update putAwayDtl array
                $putAwayDtlModels = array();
                foreach($putAwayDtlArray as $putAwayDtlData)
                {
                    $putAwayDtlModel = null;
                    if (strpos($putAwayDtlData['id'], 'NEW') !== false) 
                    {
                        $uuid = $putAwayDtlData['id'];
                        $putAwayDtlModel = new PutAwayDtl;
                        unset($putAwayDtlData['id']);
                    }
                    else
                    {
                        $putAwayDtlModel = PutAwayDtl::lockForUpdate()->find($putAwayDtlData['id']);
                    }
                    
                    $putAwayDtlModel = RepositoryUtils::dataToModel($putAwayDtlModel, $putAwayDtlData);
                    $putAwayDtlModel->hdr_id = $putAwayHdrModel->id;
                    $putAwayDtlModel->save();
                    $putAwayDtlModels[] = $putAwayDtlModel;
                }

                if(!empty($delPutAwayDtlIds))
                {
                    PutAwayDtl::destroy($delPutAwayDtlIds);
                }

                return array(
                    'hdrModel' => $putAwayHdrModel,
                    'dtlModels' => $putAwayDtlModels
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function findByPk($hdrId) 
	{
        $putAwayHdr = PutAwayHdr::where('id', $hdrId)
            ->first();
        return $putAwayHdr;
    }

    static public function findAll($siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $putAwayHdrs = PutAwayHdr::select('put_away_hdrs.*')
            ->where('site_flow_id', $siteFlowId);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $putAwayHdrs->where('put_away_hdrs.doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $putAwayHdrs->orderBy('doc_code', $sort['order']);
            }
        }
        
        return $putAwayHdrs
            ->paginate($pageSize);
    }

    static public function revertDraftToVoid($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = PutAwayHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('PutAway.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\PutAwayHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to VOID
                $hdrModel->doc_status = DocStatus::$MAP['VOID'];
                $hdrModel->save();

                $frDocTxnFlows = $hdrModel->frDocTxnFlows;
                foreach($frDocTxnFlows as $frDocTxnFlow)
                {
                    //move to voidTxn
                    $txnVoidModel = new DocTxnVoid;
                    $txnVoidModel->id = $frDocTxnFlow->id;
                    $txnVoidModel->proc_type = $frDocTxnFlow->proc_type;
                    $txnVoidModel->fr_doc_hdr_type = $frDocTxnFlow->fr_doc_hdr_type;
                    $txnVoidModel->fr_doc_hdr_id = $frDocTxnFlow->fr_doc_hdr_id;
                    $txnVoidModel->fr_doc_hdr_code = $frDocTxnFlow->fr_doc_hdr_code;
                    $txnVoidModel->to_doc_hdr_type = $frDocTxnFlow->to_doc_hdr_type;
                    $txnVoidModel->to_doc_hdr_id = $frDocTxnFlow->to_doc_hdr_id;
                    $txnVoidModel->is_closed = $frDocTxnFlow->is_closed;
                    $txnVoidModel->save();
        
                    //delete the txn
                    $frDocTxnFlow->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitVoidToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = PutAwayHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['VOID'])
                {
                    //only VOID can transition to DRAFT
                    $exc = new ApiException(__('PutAway.doc_status_is_not_void', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\PutAwayHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                $frDocTxnVoids = $hdrModel->frDocTxnVoids;
                foreach($frDocTxnVoids as $frDocTxnVoid)
                {
                    //verify first
                    $docTxnFlow = DocTxnFlow::where(array(
                        'proc_type' => $frDocTxnVoid->proc_type,
                        'fr_doc_hdr_type' => $frDocTxnVoid->fr_doc_hdr_type,
                        'fr_doc_hdr_id' => $frDocTxnVoid->fr_doc_hdr_id,
                        'to_doc_hdr_type' => $frDocTxnVoid->to_doc_hdr_type
                        ))
                        ->first();
                    if(!empty($docTxnFlow))
                    {
                        $exc = new ApiException(__('PutAway.fr_doc_is_closed', ['docCode'=>$frDocTxnVoid->fr_doc_hdr_code]));
                        $exc->addData(\App\PutAwayHdr::class, $hdrModel->id);
                        throw $exc;
                    }

                    //move to txnFlow
                    $txnFlowModel = new DocTxnFlow;
                    $txnFlowModel->id = $frDocTxnVoid->id;
                    $txnFlowModel->proc_type = $frDocTxnVoid->proc_type;
                    $txnFlowModel->fr_doc_hdr_type = $frDocTxnVoid->fr_doc_hdr_type;
                    $txnFlowModel->fr_doc_hdr_id = $frDocTxnVoid->fr_doc_hdr_id;
                    $txnFlowModel->fr_doc_hdr_code = $frDocTxnVoid->fr_doc_hdr_code;
                    $txnFlowModel->to_doc_hdr_type = $frDocTxnVoid->to_doc_hdr_type;
                    $txnFlowModel->to_doc_hdr_id = $frDocTxnVoid->to_doc_hdr_id;
                    $txnFlowModel->is_closed = $frDocTxnVoid->is_closed;
                    $txnFlowModel->save();
        
                    //delete the txnVoid
                    $frDocTxnVoid->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }
}