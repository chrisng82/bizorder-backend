<?php

namespace App\Repositories;

use App\Area;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class AreaRepository 
{
    static public function select2($search, $filters, $pageSize = 10) 
	{
        $areas = Area::where(function($q) use($search) {
            $q->where('code', 'LIKE', '%' . $search . '%')
            ->orWhere('desc_01', 'LIKE', '%' . $search . '%');
        });
        if(count($filters) >= 1)
        {
            $areas = $areas->where($filters);
        }
        return $areas
            ->orderBy('code', 'ASC')
            ->paginate($pageSize);
    }

    static public function findTop() 
	{
        $model = Area::first();

        return $model;
    }

    static public function findByPk($id) 
	{
        $model = Area::where('id', $id)
            ->first();

        return $model;
    }

    static public function findByCode($code) 
	{
        $model = Area::where('code', $code)
            ->first();

        return $model;
    }
}