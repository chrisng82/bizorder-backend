<?php

namespace App\Repositories;

use App\CycleCountDtl;
use App\CycleCountHdr;

class CycleCountDtlRepository 
{
    static public function findAllByHdrId($hdrId) 
	{
        $cycleCountDtls = CycleCountDtl::where('hdr_id', $hdrId)
            ->orderBy('line_no')
            ->get();

        return $cycleCountDtls;
    }

    static public function findAllByHdrIds($hdrIds) 
	{
        $cycleCountDtls = CycleCountDtl::whereIn('hdr_id', $hdrIds)
            ->get();

        return $cycleCountDtls;
    }

    static public function findAllByIds($ids) 
	{
        $cycleCountDtls = CycleCountDtl::whereIn('id', $ids)
            ->get();

        return $cycleCountDtls;
    }

    static public function findAllByFrCycleCountDtlId($id) 
	{
        $cycleCountDtls = CycleCountDtl::where('fr_cycle_count_dtl_id', $id)
            ->get();

        return $cycleCountDtls;
    }

    static public function findByFrCycleCountDtlId($id) 
	{
        $cycleCountDtl = CycleCountDtl::where('fr_cycle_count_dtl_id', $id)
            ->first();

        return $cycleCountDtl;
    }

    static public function findByPk($id) 
	{
        $cycleCountDtl = CycleCountDtl::where('id', $id)
            ->first();

        return $cycleCountDtl;
    }
}