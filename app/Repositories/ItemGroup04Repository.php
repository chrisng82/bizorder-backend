<?php

namespace App\Repositories;

use App\ItemGroup04;
use App\Services\Utils\ApiException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ItemGroup04Repository
{
    static public function findByCode($code) 
	{
        $group = ItemGroup04::where('code', $code)
        ->first();
        return $group;
    }
}