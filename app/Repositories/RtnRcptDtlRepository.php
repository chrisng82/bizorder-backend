<?php

namespace App\Repositories;

use App\RtnRcptHdr;
use App\RtnRcptDtl;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use Illuminate\Support\Facades\DB;

class RtnRcptDtlRepository 
{
    static public function findAllByHdrId($hdrId) 
	{
        $rtnRcptDtls = RtnRcptDtl::where('hdr_id', $hdrId)
            ->orderBy('line_no')
            ->get();

        return $rtnRcptDtls;
    }

    static public function findTopByHdrId($hdrId)
    {
        $rtnRcptDtl = RtnRcptDtl::where('hdr_id', $hdrId)
            ->orderBy('line_no')
            ->first();

        return $rtnRcptDtl;
    }
}