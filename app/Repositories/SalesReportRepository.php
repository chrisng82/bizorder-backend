<?php

namespace App\Repositories;

use App\OutbOrdDtl;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Services\Env\ResType;
use Illuminate\Support\Facades\DB;

class SalesReportRepository 
{
    static public function outbOrdAnalysis($siteFlowId, $sorts, $criteria = array(), $columns = array(), $pageSize = 20) 
	{
        $groupByHash = array();
        $leftJoinHash = array();
        $outbOrdDtls = OutbOrdDtl::select('outb_ord_dtls.*')
            ->selectRaw('SUM(outb_ord_dtls.net_amt) AS net_amt')
            ->selectRaw('SUM(outb_ord_dtls.qty * outb_ord_dtls.uom_rate / items.case_uom_rate) AS case_qty')
            ->selectRaw('SUM(outb_ord_dtls.qty * outb_ord_dtls.uom_rate / items.case_uom_rate * items.case_gross_weight) AS gross_weight')
            ->selectRaw('SUM(outb_ord_dtls.qty * outb_ord_dtls.uom_rate / items.case_uom_rate * items.case_ext_length * items.case_ext_width * items.case_ext_height / 1000000000) AS cubic_meter');

        $outbOrdDtls->leftJoin('items', 'items.id', '=', 'outb_ord_dtls.item_id');
        $outbOrdDtls->leftJoin('outb_ord_hdrs', 'outb_ord_hdrs.id', '=', 'outb_ord_dtls.hdr_id');
        
        foreach($columns as $column)
        {
            if(strcmp($column, 'division_code') == 0)
            {
                $leftJoinHash['divisions'] = 'divisions';
                $outbOrdDtls->selectRaw('divisions.code AS division_code');
            }
            elseif(strcmp($column, 'doc_status') == 0)
            {
                $groupByHash['outb_ord_hdr_id'] = 'outb_ord_hdr_id';
                $outbOrdDtls->selectRaw('outb_ord_hdrs.doc_status AS doc_status');
            }
            elseif(strcmp($column, 'doc_date') == 0)
            {
                $groupByHash['outb_ord_hdr_id'] = 'outb_ord_hdr_id';
                $outbOrdDtls->selectRaw('outb_ord_hdrs.doc_date AS doc_date');
            }
            elseif(strcmp($column, 'doc_code') == 0)
            {
                $groupByHash['outb_ord_hdr_id'] = 'outb_ord_hdr_id';
                $outbOrdDtls->selectRaw('outb_ord_hdrs.doc_code AS doc_code');
            }
            elseif(strcmp($column, 'ref_code_01') == 0)
            {
                $groupByHash['outb_ord_hdr_id'] = 'outb_ord_hdr_id';
                $outbOrdDtls->selectRaw('outb_ord_hdrs.ref_code_01 AS ref_code_01');
            }
            elseif(strcmp($column, 'hdr_desc_01') == 0)
            {
                $groupByHash['outb_ord_hdr_id'] = 'outb_ord_hdr_id';
                $outbOrdDtls->selectRaw('outb_ord_hdrs.desc_01 AS hdr_desc_01');
            }
            elseif(strcmp($column, 'hdr_desc_02') == 0)
            {
                $groupByHash['outb_ord_hdr_id'] = 'outb_ord_hdr_id';
                $outbOrdDtls->selectRaw('outb_ord_hdrs.desc_02 AS hdr_desc_02');
            }
            elseif(strcmp($column, 'hdr_created_at') == 0)
            {
                $groupByHash['outb_ord_hdr_id'] = 'outb_ord_hdr_id';
                $outbOrdDtls->selectRaw('outb_ord_hdrs.created_at AS hdr_created_at');
            }
            elseif(strcmp($column, 'sls_ord_hdr_code') == 0)
            {
                $groupByHash['outb_ord_hdr_id'] = 'outb_ord_hdr_id';
                $outbOrdDtls->selectRaw('outb_ord_hdrs.sls_ord_hdr_code AS sls_ord_hdr_code');
            }
            elseif(strcmp($column, 'sls_ord_doc_date') == 0)
            {
                $groupByHash['outb_ord_hdr_id'] = 'outb_ord_hdr_id';
                $leftJoinHash['sls_ord_hdrs'] = 'sls_ord_hdrs';
                $outbOrdDtls->selectRaw('sls_ord_hdrs.doc_date AS sls_ord_doc_date');
            }
            elseif(strcmp($column, 'sls_inv_hdr_code') == 0)
            {
                $groupByHash['outb_ord_hdr_id'] = 'outb_ord_hdr_id';
                $outbOrdDtls->selectRaw('outb_ord_hdrs.sls_inv_hdr_code AS sls_inv_hdr_code');
            }
            elseif(strcmp($column, 'sls_inv_doc_date') == 0)
            {
                $groupByHash['outb_ord_hdr_id'] = 'outb_ord_hdr_id';
                $leftJoinHash['sls_inv_hdrs'] = 'sls_inv_hdrs';
                $outbOrdDtls->selectRaw('sls_inv_hdrs.doc_date AS sls_inv_doc_date');
            }
            elseif(strcmp($column, 'pick_list_hdr_code') == 0)
            {
                $groupByHash['outb_ord_hdr_id'] = 'outb_ord_hdr_id';
                $leftJoinHash['pick_list_hdrs'] = 'pick_list_hdrs';
                $outbOrdDtls->selectRaw('pick_list_hdrs.doc_code AS pick_list_hdr_code');
            }
            elseif(strcmp($column, 'pick_list_doc_date') == 0)
            {
                $groupByHash['outb_ord_hdr_id'] = 'outb_ord_hdr_id';
                $leftJoinHash['pick_list_hdrs'] = 'pick_list_hdrs';
                $outbOrdDtls->selectRaw('pick_list_hdrs.doc_date AS pick_list_doc_date');
            }
            elseif(strcmp($column, 'item_code') == 0)
            {
                $groupByHash['item_id'] = 'item_id';
                $outbOrdDtls->selectRaw('items.code AS item_code');
            }
            elseif(strcmp($column, 'delivery_point_code') == 0)
            {
                $groupByHash['delivery_point_id'] = 'delivery_point_id';
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $outbOrdDtls->selectRaw('delivery_points.code AS delivery_point_code');
            }
            elseif(strcmp($column, 'delivery_point_area_code') == 0)
            {
                $groupByHash['delivery_point_id'] = 'delivery_point_id';
                $leftJoinHash['delivery_points'] = 'delivery_points';                
                $outbOrdDtls->selectRaw('delivery_points.area_code AS delivery_point_area_code');
            }
            elseif(strcmp($column, 'delivery_point_area_desc_01') == 0)
            {
                $groupByHash['delivery_point_id'] = 'delivery_point_id';
                $leftJoinHash['areas'] = 'areas';
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $outbOrdDtls->selectRaw('areas.desc_01 AS delivery_point_area_desc_01');
            }
            elseif(strcmp($column, 'delivery_point_company_name_01') == 0)
            {
                $groupByHash['delivery_point_id'] = 'delivery_point_id';
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $outbOrdDtls->selectRaw('delivery_points.company_name_01 AS delivery_point_company_name_01');
            }
            elseif(strcmp($column, 'delivery_point_company_name_02') == 0)
            {
                $groupByHash['delivery_point_id'] = 'delivery_point_id';
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $outbOrdDtls->selectRaw('delivery_points.company_name_02 AS delivery_point_company_name_02');
            }
            elseif(strcmp($column, 'delivery_point_unit_no') == 0)
            {
                $groupByHash['delivery_point_id'] = 'delivery_point_id';
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $outbOrdDtls->selectRaw('delivery_points.unit_no AS delivery_point_unit_no');
            }
            elseif(strcmp($column, 'delivery_point_building_name') == 0)
            {
                $groupByHash['delivery_point_id'] = 'delivery_point_id';
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $outbOrdDtls->selectRaw('delivery_points.building_name AS delivery_point_building_name');
            }
            elseif(strcmp($column, 'delivery_point_street_name') == 0)
            {
                $groupByHash['delivery_point_id'] = 'delivery_point_id';
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $outbOrdDtls->selectRaw('delivery_points.street_name AS delivery_point_street_name');
            }
            elseif(strcmp($column, 'delivery_point_district_01') == 0)
            {
                $groupByHash['delivery_point_id'] = 'delivery_point_id';
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $outbOrdDtls->selectRaw('delivery_points.district_01 AS delivery_point_district_01');
            }
            elseif(strcmp($column, 'delivery_point_district_02') == 0)
            {
                $groupByHash['delivery_point_id'] = 'delivery_point_id';
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $outbOrdDtls->selectRaw('delivery_points.district_02 AS delivery_point_district_02');
            }
            elseif(strcmp($column, 'delivery_point_postcode') == 0)
            {
                $groupByHash['delivery_point_id'] = 'delivery_point_id';
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $outbOrdDtls->selectRaw('delivery_points.postcode AS delivery_point_postcode');
            }
            elseif(strcmp($column, 'delivery_point_state_name') == 0)
            {
                $groupByHash['delivery_point_id'] = 'delivery_point_id';
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $outbOrdDtls->selectRaw('delivery_points.state_name AS delivery_point_state_name');
            }
            elseif(strcmp($column, 'delivery_point_country_name') == 0)
            {
                $groupByHash['delivery_point_id'] = 'delivery_point_id';
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $outbOrdDtls->selectRaw('delivery_points.country_name AS delivery_point_country_name');
            }
            elseif(strcmp($column, 'delivery_point_attention') == 0)
            {
                $groupByHash['delivery_point_id'] = 'delivery_point_id';
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $outbOrdDtls->selectRaw('delivery_points.attention AS delivery_point_attention');
            }
            elseif(strcmp($column, 'delivery_point_phone_01') == 0)
            {
                $groupByHash['delivery_point_id'] = 'delivery_point_id';
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $outbOrdDtls->selectRaw('delivery_points.phone_01 AS delivery_point_phone_01');
            }
            elseif(strcmp($column, 'delivery_point_phone_02') == 0)
            {
                $groupByHash['delivery_point_id'] = 'delivery_point_id';
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $outbOrdDtls->selectRaw('delivery_points.phone_02 AS delivery_point_phone_02');
            }
            elseif(strcmp($column, 'delivery_point_fax_01') == 0)
            {
                $groupByHash['delivery_point_id'] = 'delivery_point_id';
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $outbOrdDtls->selectRaw('delivery_points.fax_01 AS delivery_point_fax_01');
            }
            elseif(strcmp($column, 'delivery_point_fax_02') == 0)
            {
                $groupByHash['delivery_point_id'] = 'delivery_point_id';
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $outbOrdDtls->selectRaw('delivery_points.fax_02 AS delivery_point_fax_02');
            }
            elseif(strcmp($column, 'item_desc_01') == 0)
            {
                $groupByHash['item_id'] = 'item_id';
                $outbOrdDtls->selectRaw('items.desc_01 AS item_desc_01');
            }
            elseif(strcmp($column, 'item_desc_02') == 0)
            {
                $groupByHash['item_id'] = 'item_id';
                $outbOrdDtls->selectRaw('items.desc_02 AS item_desc_02');
            }
        }

        $outbOrdDtls->where(function($q) {
            $q->where('outb_ord_hdrs.doc_status', DocStatus::$MAP['COMPLETE'])
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('doc_txn_flows')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['PICK_LIST_01'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\OutbOrdHdr::class)
                    ->whereRaw('doc_txn_flows.fr_doc_hdr_id = outb_ord_hdrs.id')
                    ->where('doc_txn_flows.is_closed', 1);
            });
        })
        ->orWhere(function($q) use($criteria) {
            foreach($criteria as $field => $value)
            {
                if(strcmp($field, 'start_date') == 0)
                {
                    $q->where('outb_ord_hdrs.doc_date', '>=', $value);
                }
                if(strcmp($field, 'end_date') == 0)
                {
                    $q->where('outb_ord_hdrs.doc_date', '<=', $value);
                }
                if(strcmp($field, 'division_ids') == 0)
                {
                    $q->whereIn('outb_ord_hdrs.division_id', $value);
                }
                if(strcmp($field, 'delivery_point_ids') == 0)
                {
                    $q->whereIn('outb_ord_hdrs.delivery_point_id', $value);
                }
                if(strcmp($field, 'item_ids') == 0)
                {
                    $q->whereIn('outb_ord_dtls.item_id', $value);
                }
                if(strcmp($field, 'item_group_01_ids') == 0)
                {
                    $q->whereIn('items.item_group_01_id', $value);
                }
                if(strcmp($field, 'item_group_02_ids') == 0)
                {
                    $q->whereIn('items.item_group_02_id', $value);
                }
                if(strcmp($field, 'item_group_03_ids') == 0)
                {
                    $q->whereIn('items.item_group_03_id', $value);
                }
            }
        });

        

        foreach($sorts as $field => $order)
        {
                   
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'divisions') == 0)
            {
                $outbOrdDtls->leftJoin('divisions', 'divisions.id', '=', 'outb_ord_hdrs.division_id');
            }
            if(strcmp($field, 'delivery_points') == 0)
            {
                $outbOrdDtls->leftJoin('delivery_points', 'delivery_points.id', '=', 'outb_ord_hdrs.delivery_point_id');
            }
            if(strcmp($field, 'areas') == 0)
            {
                $outbOrdDtls->leftJoin('areas', 'areas.id', '=', 'delivery_points.area_id');
            }
            if(strcmp($field, 'sls_ord_hdrs') == 0)
            {
                $outbOrdDtls->leftJoin('sls_ord_hdrs', 'sls_ord_hdrs.id', '=', 'outb_ord_hdrs.sls_ord_hdr_id');
            }
            if(strcmp($field, 'sls_inv_hdrs') == 0)
            {
                $outbOrdDtls->leftJoin('sls_inv_hdrs', 'sls_inv_hdrs.id', '=', 'outb_ord_hdrs.sls_inv_hdr_id');
            }
            if(strcmp($field, 'pick_list_hdrs') == 0)
            {
                $outbOrdDtls->leftJoin('pick_list_outb_ords', 'pick_list_outb_ords.outb_ord_hdr_id', '=', 'outb_ord_hdrs.id');
                $outbOrdDtls->leftJoin('pick_list_hdrs', 'pick_list_outb_ords.hdr_id', '=', 'pick_list_hdrs.id');
            }
            
        }

        if(array_key_exists('outb_ord_hdr_id', $groupByHash))
        {
            $outbOrdDtls->groupBy('outb_ord_dtls.hdr_id');
        }
        //process the item group by
        if(array_key_exists('item_id', $groupByHash))
        {
            $outbOrdDtls->groupBy('outb_ord_dtls.item_id');
        }

        //process the storage bin group by

        if($pageSize > 0)
        {
            return $outbOrdDtls
            ->paginate($pageSize);
        }
        else
        {
            return $outbOrdDtls
            ->paginate(PHP_INT_MAX);
        }
    }
}