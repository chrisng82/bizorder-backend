<?php

namespace App\Repositories;

use App\WhseTxnFlow;

class WhseTxnFlowRepository 
{
    static public function findByPk($siteFlowId, $procType) 
	{
        $whseTxnFlow = WhseTxnFlow::where(array(
            'site_flow_id' => $siteFlowId,
            'proc_type' => $procType
            ))
            ->first();

        return $whseTxnFlow;
    }

    static public function findAllBySiteFlowIdAndTxnFlowType($siteFlowId, $txnFlowType) 
	{
        $whseTxnFlows = WhseTxnFlow::where(array(
            'site_flow_id' => $siteFlowId,
            'txn_flow_type' => $txnFlowType
            ))
            ->orderBy('step')
            ->get();

        return $whseTxnFlows;
    }
}