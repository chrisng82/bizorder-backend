<?php

namespace App\Repositories;

use App\PickFaceStrategy;
use App\Services\Utils\RepositoryUtils;

class PickFaceStrategyRepository 
{
    static public function findAllByItemAttr($siteId, $itemId, $storageClass, $itemGroup01Id, 
    $itemGroup02Id, $itemGroup03Id, $itemGroup04Id, $itemGroup05Id) 
	{
        $pickFaceStrategies = PickFaceStrategy::where('site_id', $siteId)
            ->where(function ($query) use ($itemId, $storageClass, $itemGroup01Id, $itemGroup02Id, $itemGroup03Id, $itemGroup04Id, $itemGroup05Id) {
                $query->where('item_id', $itemId)
                    ->orWhere('storage_class', $storageClass)
                    ->orWhere('item_group_01_id', $itemGroup01Id)
                    ->orWhere('item_group_02_id', $itemGroup02Id)
                    ->orWhere('item_group_03_id', $itemGroup03Id)
                    ->orWhere('item_group_04_id', $itemGroup04Id)
                    ->orWhere('item_group_05_id', $itemGroup05Id);
            })
            ->orderBy('item_id', 'DESC')
            ->orderBy('storage_class', 'DESC')
            ->orderBy('item_group_01_id', 'DESC')
            ->orderBy('item_group_02_id', 'DESC')
            ->orderBy('item_group_03_id', 'DESC')
            ->orderBy('item_group_04_id', 'DESC')
            ->orderBy('item_group_05_id', 'DESC')
            ->get();
        return $pickFaceStrategies;
    }

    static public function findAll($sorts, $filters = array(), $pageSize = 20) 
	{
        $leftJoinHash = array();
        $pickFaceStrategies = PickFaceStrategy::select('pick_face_strategies.*');

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'site_id') == 0)
            {
                $pickFaceStrategies->where('pick_face_strategies.site_id', $filter['value']);
            }
            if(strcmp($filter['field'], 'line_no') == 0)
            {
                $pickFaceStrategies->where('pick_face_strategies.line_no', '>=', $filter['value']);
            }
            if(strcmp($filter['field'], 'item_code') == 0)
            {
                $leftJoinHash['items'] = 'items';
                $pickFaceStrategies->where('items.code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'item_desc') == 0)
            {
                $leftJoinHash['items'] = 'items';
                $pickFaceStrategies->where('items.desc_01', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'storage_bin_code') == 0)
            {
                $leftJoinHash['storage_bins'] = 'storage_bins';
                $pickFaceStrategies->where('storage_bins.code', 'LIKE', '%'.$filter['value'].'%');
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'line_no') == 0)
            {
                $pickFaceStrategies->orderBy('pick_face_strategies.line_no', $sort['order']);
            } 
            if(strcmp($sort['field'], 'item_code') == 0)
            {
                $leftJoinHash['items'] = 'items';
                $pickFaceStrategies->orderBy('items.code', $sort['order']);
            }            
            if(strcmp($sort['field'], 'storage_bin_code') == 0)
            {
                $leftJoinHash['storage_bins'] = 'storage_bins';
                $pickFaceStrategies->orderBy('storage_bins.code', $sort['order']);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'items') == 0)
            {
                $pickFaceStrategies->leftJoin('items', 'items.id', '=', 'pick_face_strategies.item_id');
            }
            if(strcmp($field, 'storage_bins') == 0)
            {
                $pickFaceStrategies->leftJoin('storage_bins', 'storage_bins.id', '=', 'pick_face_strategies.storage_bin_id');
            }
        }

        if($pageSize > 0)
        {
            return $pickFaceStrategies
            ->paginate($pageSize);
        }
        else
        {
            return $pickFaceStrategies
            ->paginate(PHP_INT_MAX);
        }
    }

    static public function plainCreate($data) 
	{
        $model = new PickFaceStrategy();
        $model = RepositoryUtils::dataToModel($model, $data);
        $model->save();

        return $model;
    }

    static public function plainUpdate($id, $data) 
	{
        $model = PickFaceStrategy::find($id);
        if(!empty($model))
        {
            $model = RepositoryUtils::dataToModel($model, $data);
            $model->save();
        }

        return $model;
    }

    static public function findBySiteIdAndLineNo($siteId, $lineNo) 
	{
        $model = PickFaceStrategy::where('site_id', $siteId)
        ->where('line_no', $lineNo)
        ->first();

        return $model;
    }

    static public function findAllByItemId($siteId, $itemId) 
	{
        $pickFaceStrategies = PickFaceStrategy::where('site_id', $siteId)
            ->where('item_id', $itemId)
            ->orderBy('line_no', 'ASC')
            ->get();
        return $pickFaceStrategies;
    }
}