<?php

namespace App\Repositories;

use App\ItemGroup05;
use App\Services\Utils\ApiException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ItemGroup05Repository
{
    static public function findByCode($code) 
	{
        $group = ItemGroup05::where('code', $code)
        ->first();
        return $group;
    }
}