<?php

namespace App\Repositories;

use App\Division;
use App\Services\Utils\RepositoryUtils;
use Illuminate\Support\Facades\DB;

class DivisionRepository 
{
    static public function findAll($sorts, $filters = array(), $pageSize = 20) 
	{
        $users = Division::select('divisions.*');

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'code') == 0)
            {
                $users->where('divisions.code', 'LIKE', '%'.$filter['value'].'%');
            }
            elseif(strcmp($filter['field'], 'name_01') == 0)
            {
                $users->where('divisions.name_01', $filter['value']);
            }
            elseif(strcmp($filter['field'], 'name_02') == 0)
            {
                $users->where('divisions.name_02', $filter['value']);
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'code') == 0)
            {
                $users->orderBy('code', $sort['order']);
            }
            elseif(strcmp($sort['field'], 'name_01') == 0)
            {
                $users->orderBy('name_01', $sort['order']);
            }
            elseif(strcmp($sort['field'], 'name_02') == 0)
            {
                $users->orderBy('name_02', $sort['order']);
            }
        }
        
        if($pageSize > 0)
        {
            return $users
                ->paginate($pageSize);
        }
        else
        {
            return $users
                ->paginate(PHP_INT_MAX);
        }
    }

    static public function findAllByUserId($userId) 
	{
        $divisions = Division::select('divisions.*')
        ->whereExists(function ($query) use($userId) {
            $query->select(DB::raw(1))
                ->from('user_divisions')
                ->where('user_id', $userId)
                ->whereRaw('division_id = divisions.id');
        })
        ->get();

        return $divisions;
    }

    static public function findTopByUserId($userId) 
	{
        $division = Division::select('divisions.*')
        ->whereExists(function ($query) use($userId) {
            $query->select(DB::raw(1))
                ->from('user_divisions')
                ->where('user_id', $userId);
        })
        ->first();

        return $division;
    }

    static public function findByPk($id) 
	{
        $division = Division::where('id', $id)
            ->first();

        return $division;
    }

    static public function select2($divisionIds, $search, $filters, $pageSize = 10) 
	{
        $divisions = Division::select('divisions.*')
        ->where(function($q) use($search) {
            $q->orWhere('name_01', 'LIKE', '%' . $search . '%')
            ->orWhere('name_02', 'LIKE', '%' . $search . '%')
            ->orWhere('code', 'LIKE', '%' . $search . '%');
        });

        $isNotInUserId = false;
        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'user_id') == 0)
            {
                $operator = 'NOT IN';
                if(isset($filter['operator']))
                {
                    $operator = $filter['operator'];
                }
                if(strcmp($operator, 'NOT IN') == 0)
                {  
                    $isNotInUserId = true;
                    $divisions->whereRaw('divisions.id NOT IN(SELECT division_id FROM user_divisions WHERE user_id = ?)', array($filter['value']));
                }              
            }
        }
        if($isNotInUserId === false)
        {
            $divisions->whereIn('id', $divisionIds);
        }

        return $divisions
            ->orderBy('code', 'ASC')
            ->paginate($pageSize);
    }
    
    static public function createModel($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = new Division;
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return $model;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function updateModel($data) 
	{
        $result = DB::transaction
        (
            function() use ($data)
            {
                //update Item
                $model = Division::lockForUpdate()->find($data['id']);
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return array(
                    'model' => $model
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }
}