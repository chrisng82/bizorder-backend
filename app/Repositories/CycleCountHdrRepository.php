<?php

namespace App\Repositories;

use App\Services\Utils\RepositoryUtils;
use App\CycleCountDtl;
use App\CycleCountHdr;
use App\CycleCountCDtl;
use App\DocTxnFlow;
use App\DocTxnVoid;
use App\Services\Env\ProcType;
use App\Services\Env\DocStatus;
use App\Services\Env\PhysicalCountStatus;
use Illuminate\Support\Facades\DB;
use App\Services\Utils\ApiException;

class CycleCountHdrRepository 
{
    static public function findByPk($hdrId) 
	{
        $cycleCountHdr = CycleCountHdr::where('id', $hdrId)
            ->first();

        return $cycleCountHdr;
    }

    static public function txnFindByPk($hdrId) 
	{
        $model = DB::transaction
        (
            function() use ($hdrId)
            {
                $cycleCountHdr = CycleCountHdr::where('id', $hdrId)
                    ->first();
                return $cycleCountHdr;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function findLatestByStorageBinId($storageBinId, $numOfRecords = 10) 
	{
        $cycleCountHdrs = CycleCountHdr::select(
                'cycle_count_hdrs.*'
            )
            ->whereExists(function ($query) use ($storageBinId) {
                $query->select(DB::raw(1))
                    ->from('cycle_count_dtls')
                    ->where('cycle_count_dtls.storage_bin_id', $storageBinId)
                    ->whereRaw('cycle_count_dtls.hdr_id = cycle_count_hdrs.id');
            })
            ->where('doc_status', '>=', DocStatus::$MAP['WIP'])
            ->orderBy('doc_date', 'DESC')
            ->limit($numOfRecords)
            ->get();
        return $cycleCountHdrs;
    }

    static public function commitToWip($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = CycleCountHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('CycleCount.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CycleCountHdr::class, $hdrModel->id);
                    throw $exc;
                }

                $dtlModels = CycleCountDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->orderBy('line_no')
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('CycleCount.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CycleCountHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to WIP
                $hdrModel->doc_status = DocStatus::$MAP['WIP'];
                $hdrModel->save();

                /*
                $cycleCountDtls = CycleCountDtl::where('hdr_id', $hdrModel->id)
                    ->lockForUpdate()
                    ->get();
                foreach($cycleCountDtls as $cycleCountDtl)
                {
                    if(empty($company))
                    {
                        //only DRAFT or above can transition to COMPLETE
                        $exc = new ApiException(__('CycleCount.company_is_required', ['lineNo'=>$cycleCountDtl->line_no]));
                        $exc->addData(\App\CycleCountDtl::class, $cycleCountDtl->id);
                        throw $exc;
                    }
                }
                */
                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitToComplete($hdrId, $isNonZeroBal)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId, $isNonZeroBal)
            {
                $hdrModel = CycleCountHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();
                    
                if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('CycleCount.doc_status_is_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CycleCountHdr::class, $hdrModel->id);
                    throw $exc;
                }
                if($hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('CycleCount.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CycleCountHdr::class, $hdrModel->id);
                    throw $exc;
                }

                $siteFlow = $hdrModel->siteFlow;
        
                //1) update docStatus to COMPLETE
                $hdrModel->doc_status = DocStatus::$MAP['COMPLETE'];
                $hdrModel->save();

                $dtlModels = CycleCountDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('CycleCount.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CycleCountHdr::class, $hdrModel->id);
                    throw $exc;
                }
                $dtlModels->load('item');
                foreach($dtlModels as $dtlModel)
                {    
                    $item = $dtlModel->item; 
                    /*                    
                    if(empty($item))
                    {
                        //only DRAFT or above can transition to COMPLETE
                        $exc = new ApiException(__('CycleCount.item_is_required', ['lineNo'=>$dtlModel->line_no]));
                        $exc->addData(\App\CycleCountDtl::class, $dtlModel->id);
                        throw $exc;
                    }
                    */
                    
                    if($dtlModel->storage_bin_id == 0)
                    {
                        //only DRAFT or above can transition to COMPLETE
                        $exc = new ApiException(__('CycleCount.storage_bin_is_required', ['lineNo'=>$dtlModel->line_no]));
                        $exc->addData(\App\CycleCountDtl::class, $dtlModel->id);
                        throw $exc;
                    }

                    if($dtlModel->item_id > 0)
                    {
                        if($dtlModel->item_batch_id > 0)
                        {
                            //here temporarily code to solve the wrong item_batch_id, cause by bug
                            //so verify the item batch here
                            $isreset = false;
                            $dtlItemBatch = ItemBatchRepository::findByPk($dtlModel->item_batch_id);
                            if(empty($dtlItemBatch))
                            {
                                $isreset = true;
                            }
                            else
                            {
                                if($dtlItemBatch->item_id != $dtlModel->item_id)
                                {
                                    $isreset = true;
                                }
                                if(strcmp($dtlItemBatch->expiry_date, $dtlModel->expiry_date) != 0)
                                {
                                    $isreset = true;
                                }
                            }
                            if($isreset)
                            {
                                $dtlModel->item_batch_id = 0;
                            }
                        }

                        if($dtlModel->item_batch_id == 0)
                        {
                            $itemBatch = ItemBatchRepository::firstOrCreate(
                                $item, 
                                $dtlModel->batch_serial_no, 
                                $dtlModel->expiry_date, 
                                $dtlModel->receipt_date
                            );
                            $dtlModel->item_batch_id = $itemBatch->id;
                        }
                    }
                    $dtlModel->save();
                }  
                return $hdrModel;              
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertCompleteToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = CycleCountHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                {
                    $exc = new ApiException(__('CycleCount.doc_status_is_not_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CycleCountHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertWipToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = CycleCountHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['WIP'])
                {
                    $exc = new ApiException(__('CycleCount.doc_status_is_not_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CycleCountHdr::class, $hdrModel->id);
                    throw $exc;
                }

                //1) verify the toDocHdr, make sure it is not WIP(download to mobile)
                $docTxnFlows = DocTxnFlow::where(array(
                    'fr_doc_hdr_type' => get_class($hdrModel),
                    'fr_doc_hdr_id' => $hdrModel->id,
                    'to_doc_hdr_type' => \App\CountAdjHdr::class
                    ))
                    ->get();
                foreach($docTxnFlows as $docTxnFlow)
                {
                    $toDocHdr = \App\CountAdjHdr::where('id', $docTxnFlow->to_doc_hdr_id)
                        ->lockForUpdate()
                        ->first();
                    if($toDocHdr->doc_status == DocStatus::$MAP['DRAFT']
                    || $toDocHdr->doc_status == DocStatus::$MAP['WIP'])
                    {
                        $exc = new ApiException(__('CountAdj.job_status_is_draft_or_wip', ['docCode'=>$toDocHdr->doc_code]));
                        $exc->addData(\App\CountAdjHdr::class, $hdrModel->id);
                        throw $exc;
                    }
                }
                
                //2) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function createProcess($procType, $docNoId, $hdrData, $dtlDataList, $oriCycleCountDtlDataList = array()) 
	{
        $cycleCountHdr = DB::transaction
        (
            function() use ($procType, $docNoId, $hdrData, $dtlDataList, $oriCycleCountDtlDataList)
            {
                $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

                $hdrModel = new CycleCountHdr;
                $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
                $hdrModel->doc_code = $newCode;
                $hdrModel->proc_type = $procType;
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                foreach($dtlDataList as $dtlData)
                {
                    $dtlModel = new CycleCountDtl;
                    $dtlModel = RepositoryUtils::dataToModel($dtlModel, $dtlData);
                    $dtlModel->hdr_id = $hdrModel->id;
                    $dtlModel->save();

                    //create a carbon copy
                    $cDtlModel = new CycleCountCDtl;
                    $cDtlModel = RepositoryUtils::dataToModel($cDtlModel, $dtlData);
                    $cDtlModel->hdr_id = $hdrModel->id;
                    $cDtlModel->save();
                }

                foreach($oriCycleCountDtlDataList as $oriCycleCountDtlData)
                {
                    $oriCycleCountDtl = CycleCountDtl::lockForUpdate()->find($oriCycleCountDtlData['id']);
                    $oriCycleCountDtl->to_cycle_count_hdr_id = $hdrModel->id;
                    //change physical_count_status to RECOUNT
                    if($oriCycleCountDtl->physical_count_status == PhysicalCountStatus::$MAP['MARK_RECOUNT'])
                    {
                        $oriCycleCountDtl->physical_count_status = PhysicalCountStatus::$MAP['RECOUNT'];
                    }
                    $oriCycleCountDtl->save();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $cycleCountHdr;
    }

    static public function findAllNotExistWhseJob1601Txn($siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $qStatuses = array(DocStatus::$MAP['DRAFT'], DocStatus::$MAP['WIP']); 
        $cycleCountHdrs = CycleCountHdr::select('cycle_count_hdrs.*')
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('doc_txn_flows')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['WHSE_JOB_16_01'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\CycleCountHdr::class)
                    ->whereRaw('doc_txn_flows.fr_doc_hdr_id = cycle_count_hdrs.id')
                    ->where('doc_txn_flows.is_closed', 1);
            })
            ->where('site_flow_id', $siteFlowId)
            ->whereIn('doc_status', $qStatuses);

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $cycleCountHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        return $cycleCountHdrs
            ->paginate($pageSize);
    }

    static public function findAllNotExistCountAdj01Txn($siteFlowId, $qStatuses, $sorts, $filters = array(), $pageSize = 20) 
	{ 
        $cycleCountHdrs = CycleCountHdr::select('cycle_count_hdrs.*')
            ->whereNotExists(function ($query) {
            $query->select(DB::raw(1))
                ->from('doc_txn_flows')
                ->where('doc_txn_flows.proc_type', ProcType::$MAP['COUNT_ADJ_01'])
                ->where('doc_txn_flows.fr_doc_hdr_type', \App\CycleCountHdr::class)
                ->whereRaw('doc_txn_flows.fr_doc_hdr_id = cycle_count_hdrs.id')
                ->where('doc_txn_flows.is_closed', 1);
            })
            ->where('site_flow_id', $siteFlowId)
            ->whereIn('doc_status', $qStatuses);
        foreach($filters as $key => $value)
        {
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $cycleCountHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        return $cycleCountHdrs
            ->paginate($pageSize);
    }

    static public function findAllByHdrIds($hdrIds)
    {
        $cycleCountHdrs = CycleCountHdr::whereIn('id', $hdrIds)
            ->orderBy('doc_code', 'ASC')
            ->get();

        return $cycleCountHdrs;
    }

    static public function revertDraftToVoid($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = CycleCountHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('CycleCount.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CycleCountHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to VOID
                $hdrModel->doc_status = DocStatus::$MAP['VOID'];
                $hdrModel->save();

                $frDocTxnFlows = $hdrModel->frDocTxnFlows;
                foreach($frDocTxnFlows as $frDocTxnFlow)
                {
                    //move to voidTxn
                    $txnVoidModel = new DocTxnVoid;
                    $txnVoidModel->id = $frDocTxnFlow->id;
                    $txnVoidModel->proc_type = $frDocTxnFlow->proc_type;
                    $txnVoidModel->fr_doc_hdr_type = $frDocTxnFlow->fr_doc_hdr_type;
                    $txnVoidModel->fr_doc_hdr_id = $frDocTxnFlow->fr_doc_hdr_id;
                    $txnVoidModel->fr_doc_hdr_code = $frDocTxnFlow->fr_doc_hdr_code;
                    $txnVoidModel->to_doc_hdr_type = $frDocTxnFlow->to_doc_hdr_type;
                    $txnVoidModel->to_doc_hdr_id = $frDocTxnFlow->to_doc_hdr_id;
                    $txnVoidModel->is_closed = $frDocTxnFlow->is_closed;
                    $txnVoidModel->save();
        
                    //delete the txn
                    $frDocTxnFlow->delete();
                }

                //find the parent cycleCountDtl
                //update the physical_count_status to MARK_RECOUNT, if >= 50
                $parentDtlModels = CycleCountDtl::where('to_cycle_count_hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->get();
                foreach($parentDtlModels as $parentDtlModel)
                {
                    if($parentDtlModel->physical_count_status == PhysicalCountStatus::$MAP['RECOUNT'])
                    {
                        $parentDtlModel->physical_count_status = PhysicalCountStatus::$MAP['MARK_RECOUNT'];
                        $parentDtlModel->save();
                    }
                }                

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitVoidToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = CycleCountHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['VOID'])
                {
                    //only VOID can transition to DRAFT
                    $exc = new ApiException(__('CycleCount.doc_status_is_not_void', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CycleCountHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                $frDocTxnVoids = $hdrModel->frDocTxnVoids;
                foreach($frDocTxnVoids as $frDocTxnVoid)
                {
                    //verify first
                    $docTxnFlow = DocTxnFlow::where(array(
                        'proc_type' => $frDocTxnVoid->proc_type,
                        'fr_doc_hdr_type' => $frDocTxnVoid->fr_doc_hdr_type,
                        'fr_doc_hdr_id' => $frDocTxnVoid->fr_doc_hdr_id,
                        'to_doc_hdr_type' => $frDocTxnVoid->to_doc_hdr_type
                        ))
                        ->first();
                    if(!empty($docTxnFlow))
                    {
                        $exc = new ApiException(__('CycleCount.fr_doc_is_closed', ['docCode'=>$frDocTxnVoid->fr_doc_hdr_code]));
                        $exc->addData(\App\CycleCountHdr::class, $hdrModel->id);
                        throw $exc;
                    }

                    //move to txnFlow
                    $txnFlowModel = new DocTxnFlow;
                    $txnFlowModel->id = $frDocTxnVoid->id;
                    $txnFlowModel->proc_type = $frDocTxnVoid->proc_type;
                    $txnFlowModel->fr_doc_hdr_type = $frDocTxnVoid->fr_doc_hdr_type;
                    $txnFlowModel->fr_doc_hdr_id = $frDocTxnVoid->fr_doc_hdr_id;
                    $txnFlowModel->fr_doc_hdr_code = $frDocTxnVoid->fr_doc_hdr_code;
                    $txnFlowModel->to_doc_hdr_type = $frDocTxnVoid->to_doc_hdr_type;
                    $txnFlowModel->to_doc_hdr_id = $frDocTxnVoid->to_doc_hdr_id;
                    $txnFlowModel->is_closed = $frDocTxnVoid->is_closed;
                    $txnFlowModel->save();
        
                    //delete the txnVoid
                    $frDocTxnVoid->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function updateDetails($cycleCountHdrData, $cycleCountDtlArray, $delCycleCountDtlArray) 
	{
        $delCycleCountDtlIds = array();
        foreach($delCycleCountDtlArray as $delCycleCountDtlData)
        {
            $delCycleCountDtlIds[] = $delCycleCountDtlData['id'];
        }
        
        $result = DB::transaction
        (
            function() use ($cycleCountHdrData, $cycleCountDtlArray, $delCycleCountDtlIds)
            {
                //update cycleCountHdr
                $cycleCountHdrModel = CycleCountHdr::lockForUpdate()->find($cycleCountHdrData['id']);
                if($cycleCountHdrModel->doc_status >= DocStatus::$MAP['WIP'])
                {
                    //WIP and COMPLETE status can not be updated
                    $exc = new ApiException(__('CycleCount.doc_status_is_wip_or_complete', ['docCode'=>$cycleCountHdrModel->doc_code]));
                    $exc->addData(\App\CycleCountHdr::class, $cycleCountHdrModel->id);
                    throw $exc;
                }
                $cycleCountHdrModel = RepositoryUtils::dataToModel($cycleCountHdrModel, $cycleCountHdrData);
                $cycleCountHdrModel->save();

                //update cycleCountDtl array
                $cycleCountDtlModels = array();
                foreach($cycleCountDtlArray as $cycleCountDtlData)
                {
                    $cycleCountDtlModel = null;
                    if (strpos($cycleCountDtlData['id'], 'NEW') !== false) 
                    {
                        $uuid = $cycleCountDtlData['id'];
                        $cycleCountDtlModel = new CycleCountDtl;
                        unset($cycleCountDtlData['id']);
                    }
                    else
                    {
                        $cycleCountDtlModel = CycleCountDtl::lockForUpdate()->find($cycleCountDtlData['id']);
                    }
                    
                    $cycleCountDtlModel = RepositoryUtils::dataToModel($cycleCountDtlModel, $cycleCountDtlData);
                    $cycleCountDtlModel->hdr_id = $cycleCountHdrModel->id;
                    $cycleCountDtlModel->save();
                    $cycleCountDtlModels[] = $cycleCountDtlModel;
                }

                if(!empty($delCycleCountDtlIds))
                {
                    CycleCountDtl::destroy($delCycleCountDtlIds);
                }

                return array(
                    'hdrModel' => $cycleCountHdrModel,
                    'dtlModels' => $cycleCountDtlModels
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function findAllDraftAndWIP($siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $qStatuses = array(DocStatus::$MAP['DRAFT'], DocStatus::$MAP['WIP']); 
        $cycleCountHdrs = CycleCountHdr::where('site_flow_id', $siteFlowId)
            ->whereIn('doc_status', $qStatuses);

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $cycleCountHdrs->orderBy('doc_code', $sort['order']);
            }
        }
        return $cycleCountHdrs
            ->paginate($pageSize);
    }

    static public function createHeader($procType, $docNoId, $hdrData) 
	{
        $hdrModel = DB::transaction
        (
            function() use ($procType, $docNoId, $hdrData)
            {
                $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

                $hdrModel = new CycleCountHdr;
                $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
                $hdrModel->doc_code = $newCode;
                $hdrModel->proc_type = $procType;
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function findAllExistWhseJob1601TxnAndWIP($sorts, $filters = array(), $pageSize = 20) 
	{
        $whseJob1601TxnFlows = DocTxnFlow::select('doc_txn_flows.fr_doc_hdr_id')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['WHSE_JOB_16_01'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\CycleCountHdr::class)
                    ->distinct();

        $qStatuses = array(DocStatus::$MAP['WIP']); 
        $cycleCountHdrs = CycleCountHdr::select('cycle_count_hdrs.*')
            ->leftJoinSub($whseJob1601TxnFlows, 'whse_job_16_01_txn_flows', function ($join) {
                $join->on('cycle_count_hdrs.id', '=', 'whse_job_16_01_txn_flows.fr_doc_hdr_id');
            })
            ->whereNotNull('whse_job_16_01_txn_flows.fr_doc_hdr_id')
            ->whereIn('doc_status', $qStatuses);

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $cycleCountHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $cycleCountHdrs->where('doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
        }
        
        return $cycleCountHdrs
            ->paginate($pageSize);
    }

    static public function updateRecountDetails($data) 
	{
        $result = DB::transaction
        (
            function() use ($data)
            {
                $cycleCountDtlModels = array();
                //update cycleCountDtl
                foreach($data as $id => $physicalCountStatus)
                {
                    $cycleCountDtlModel = CycleCountDtl::lockForUpdate()->find($id);
                    $cycleCountHdrModel = $cycleCountDtlModel->cycleCountHdr;
                    if($cycleCountHdrModel->doc_status < DocStatus::$MAP['WIP'])
                    {
                        //only WIP or COMPLETE can be updated
                        $exc = new ApiException(__('CycleCount.doc_status_is_not_wip', ['docCode'=>$cycleCountHdrModel->doc_code]));
                        $exc->addData(\App\CycleCountHdr::class, $cycleCountHdrModel->id);
                        throw $exc;
                    }

                    if($cycleCountDtlModel->physical_count_status <= 10)
                    {
                        //physical_count_status is already confirmed, can not update
                        $exc = new ApiException(__('CycleCount.physical_count_status_update_failed', [
                            'lineNo'=>$cycleCountDtlModel->line_no,
                            'physical_count_status'=>PhysicalCountStatus::$MAP[$cycleCountDtlModel->physical_count_status]
                            ]));
                        //$exc->addData(\App\CycleCountDtl::class, $cycleCountDtlModel->id);
                        throw $exc;
                    }

                    $cycleCountDtlModel->physical_count_status = $physicalCountStatus;
                    $cycleCountDtlModel->save();

                    $cycleCountDtlModels[] = $cycleCountDtlModel;
                }

                return array(
                    //'hdrModel' => $cycleCountHdrModel,
                    'dtlModels' => $cycleCountDtlModels
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function findAll($siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $cycleCountHdrs = CycleCountHdr::select('cycle_count_hdrs.*')
            ->where('site_flow_id', $siteFlowId);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $cycleCountHdrs->where('cycle_count_hdrs.doc_code', 'LIKE', '%'.$filter['value'].'%');
            }

            if(strcmp($filter['field'], 'str_doc_status') == 0)
            {
                if(array_key_exists(strtoupper($filter['value']), DocStatus::$MAP))
                {
                    $docStatus = DocStatus::$MAP[strtoupper($filter['value'])];
                    $cycleCountHdrs->where('cycle_count_hdrs.doc_status', $docStatus);
                }                
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $cycleCountHdrs->orderBy('doc_code', $sort['order']);
            }
        }
        
        if($pageSize > 0)
        {
            return $cycleCountHdrs
                ->paginate($pageSize);
        }
        else
        {
            return $cycleCountHdrs
                ->paginate(PHP_INT_MAX);
        }
    }

    static public function queryLatestCompleteByStorageBinIds($storageBinIds)
	{
        $latestDocCode = '';
        $latestDate = '1970-01-01';
        $cycleCountDtl = CycleCountDtl::selectRaw('MAX(doc_code) AS doc_code, MAX(doc_date) AS doc_date')
        ->leftJoin('cycle_count_hdrs', 'cycle_count_hdrs.id', '=', 'cycle_count_dtls.hdr_id')
        ->where('doc_status', DocStatus::$MAP['COMPLETE'])
        ->whereIn('storage_bin_id', $storageBinIds)
        ->first();
        if(!empty($cycleCountDtl))
        {
            $latestDocCode = $cycleCountDtl->doc_code;
            $latestDate = $cycleCountDtl->doc_date;
        }
        return array(
            'doc_code' => $latestDocCode,
            'doc_date' => $latestDate
        );
    }
}