<?php

namespace App\Repositories;

use App\Location;
use App\Services\Env\LocationType;
use App\Services\Utils\RepositoryUtils;
use Illuminate\Support\Facades\DB;

class LocationRepository 
{
    static public function findBySiteIdAndCode($siteId, $code) 
	{
        $location = Location::where('site_id', $siteId)
        ->where('code', $code)
        ->first();

        return $location;
    }

    static public function findAllByGoodStock($siteId) 
	{
        $locations = Location::where('site_id', $siteId)
        ->where('location_type', LocationType::$MAP['GOOD_STOCK'])
        ->get();

        return $locations;
    }

    static public function select2($search, $filters, $pageSize = 10) 
	{
        $locations = Location::select('locations.*')
        ->where(function($q) use($search) {
            $q->orWhere('desc_01', 'LIKE', '%' . $search . '%')
            ->orWhere('desc_02', 'LIKE', '%' . $search . '%')
            ->orWhere('code', 'LIKE', '%' . $search . '%');
        });
        if(count($filters) >= 1)
        {
            $locations = $locations->where($filters);
        }
        return $locations
            ->orderBy('code', 'ASC')
            ->paginate($pageSize);
    }

    static public function findAllBySiteIdAndLocationType($siteId, $locationType) 
	{
        $locations = Location::where('site_id', $siteId)
        ->where('location_type', $locationType)
        ->get();

        return $locations;
    }

    static public function syncSlsOrdSync01($siteId, $data) 
	{
        $location = Location::where('site_id', $siteId)
        ->where('code', $data['location_code'])
        ->first();
        if(empty($location))
        {
            //just get 1 of the default good location
            $location = Location::where('site_id', $siteId)
            ->where('location_type', LocationType::$MAP['GOOD_STOCK'])
            ->first();
        }
        return $location;
    }

    static public function syncSlsRtnSync01($siteId, $data) 
	{
        $location = Location::where('site_id', $siteId)
        ->where('code', $data['location_code'])
        ->first();
        
        return $location;
    }

    static public function plainCreate($data) 
	{
        $model = new Location();
        $model = RepositoryUtils::dataToModel($model, $data);
        $model->save();

        return $model;
    }
}