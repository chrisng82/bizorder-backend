<?php

namespace App\Repositories;

use App\Services\Utils\RepositoryUtils;
use Illuminate\Support\Facades\DB;

use App\Models\Message;

class NotificationRepository 
{
    static public function createNotification($data) {
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = new Message;
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return $model;
            }, 
            5 //reattempt times
        );
        return $model;
    }
}