<?php

namespace App\Repositories;

use App\OutbOrdHdr;
use App\CartHdr;
use App\CartDtl;
use App\DocTxnFlow;
use App\DocNo;
use App\Debtor;
use App\Models\Promotion;
use App\Services\Env\ProcType;
use App\Services\Env\DocStatus;
use App\Services\Env\ItemType;
use App\Services\Env\PromotionRuleType;
use App\Services\Env\ResStatus;
use App\Services\Env\RetrievalMethod;
use App\Services\Env\ScanMode;
use App\Services\Env\StorageClass;
use App\Services\Utils\RepositoryUtils;
use App\Repositories\DeliveryRepository;
use App\Repositories\CurrencyRepository;
use App\Repositories\CreditTermRepository;
use App\Repositories\UserRepository;
use App\Repositories\DeliveryPointRepository;
use App\Repositories\ItemRepository;
use App\Repositories\LocationRepository;
use App\Repositories\SlsOrdHdrRepository;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Libs\Promotion\VariantItemChecker;
use App\Libs\Promotion\DebtorChecker;
use App\Libs\Promotion\DebtorGroupChecker;

class CartHdrRepository 
{
    static public function commitToComplete($hdrId, $isNonZeroBal)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId, $isNonZeroBal)
            {
                $hdrModel = CartHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();
                    
                if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('SlsOrd.doc_status_is_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CartHdr::class, $hdrModel->id);
                    throw $exc;
                }
                if($hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('SlsOrd.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CartHdr::class, $hdrModel->id);
                    throw $exc;
                }
        
                //1) update docStatus to COMPLETE
                $hdrModel->doc_status = DocStatus::$MAP['COMPLETE'];
                $hdrModel->save();

                $dtlModels = CartDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('SlsOrd.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CartHdr::class, $hdrModel->id);
                    throw $exc;
                }
                $dtlModels->load('item');
                foreach($dtlModels as $dtlModel)
                {                    
                    $item = $dtlModel->item;
                    if(empty($item))
                    {
                        //only DRAFT or above can transition to COMPLETE
                        $exc = new ApiException(__('SlsOrd.item_is_required', ['lineNo'=>$dtlModel->line_no]));
                        $exc->addData(\App\CartDtl::class, $dtlModel->id);
                        throw $exc;
                    }
                }  
                return $hdrModel;              
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitToWip($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = CartHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('SlsOrd.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CartHdr::class, $hdrModel->id);
                    throw $exc;
                }

                $dtlModels = CartDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->orderBy('line_no')
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('SlsOrd.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CartHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to WIP
                $hdrModel->doc_status = DocStatus::$MAP['WIP'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitVoidToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = CartHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['VOID'])
                {
                    //only VOID can transition to DRAFT
                    $exc = new ApiException(__('SlsOrd.doc_status_is_not_void', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CartHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                $frDocTxnVoids = $hdrModel->frDocTxnVoids;
                foreach($frDocTxnVoids as $frDocTxnVoid)
                {
                    //verify first
                    $docTxnFlow = DocTxnFlow::where(array(
                        'proc_type' => $frDocTxnVoid->proc_type,
                        'fr_doc_hdr_type' => $frDocTxnVoid->fr_doc_hdr_type,
                        'fr_doc_hdr_id' => $frDocTxnVoid->fr_doc_hdr_id,
                        'to_doc_hdr_type' => $frDocTxnVoid->to_doc_hdr_type
                        ))
                        ->first();
                    if(!empty($docTxnFlow))
                    {
                        $exc = new ApiException(__('SlsOrd.fr_doc_is_closed', ['docCode'=>$frDocTxnVoid->fr_doc_hdr_code]));
                        $exc->addData(\App\CartHdr::class, $hdrModel->id);
                        throw $exc;
                    }

                    //move to txnFlow
                    $txnFlowModel = new DocTxnFlow;
                    $txnFlowModel->id = $frDocTxnVoid->id;
                    $txnFlowModel->proc_type = $frDocTxnVoid->proc_type;
                    $txnFlowModel->fr_doc_hdr_type = $frDocTxnVoid->fr_doc_hdr_type;
                    $txnFlowModel->fr_doc_hdr_id = $frDocTxnVoid->fr_doc_hdr_id;
                    $txnFlowModel->fr_doc_hdr_code = $frDocTxnVoid->fr_doc_hdr_code;
                    $txnFlowModel->to_doc_hdr_type = $frDocTxnVoid->to_doc_hdr_type;
                    $txnFlowModel->to_doc_hdr_id = $frDocTxnVoid->to_doc_hdr_id;
                    $txnFlowModel->is_closed = $frDocTxnVoid->is_closed;
                    $txnFlowModel->save();
        
                    //delete the txnVoid
                    $frDocTxnVoid->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function createHeader($docNoId, $hdrData) 
	{
        $hdrModel = DB::transaction
        (
            function() use ($docNoId, $hdrData)
            {
                $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

                $hdrModel = new CartHdr;
                $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
                $hdrModel->doc_code = $newCode;
                //$hdrModel->proc_type = $procType;
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];

                $hdrModel->gross_amt = 0;
                $hdrModel->gross_local_amt = 0;
                $hdrModel->disc_amt = 0;
                $hdrModel->disc_local_amt = 0;
                $hdrModel->tax_amt = 0;
                $hdrModel->tax_local_amt = 0;
                $hdrModel->is_round_adj = 0;
                $hdrModel->round_adj_amt = 0;
                $hdrModel->round_adj_local_amt = 0;
                $hdrModel->net_amt = 0;
                $hdrModel->net_local_amt = 0;
                
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }
    
    static public function createProcess($procType, $toDocTxnFlowData) {

        $docTxnFlowModel = new DocTxnFlow;
        $docTxnFlowModel = RepositoryUtils::dataToModel($docTxnFlowModel, $toDocTxnFlowData);
        $docTxnFlowModel->save();

        return $docTxnFlowModel;
    }

    static public function findAll($divisionId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $leftJoinHash = array();
        $cartHdrs = CartHdr::select('cart_hdrs.*')
            ->where('division_id', $divisionId);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $cartHdrs->where('cart_hdrs.doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $cartHdrs->where(function($q) use($filter) {
                    $q->where('delivery_points.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('delivery_points.company_name_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'salesman') == 0)
            {
                $leftJoinHash['users'] = 'users';
                $cartHdrs->where('users.username', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point_area') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $leftJoinHash['areas'] = 'areas';
                $cartHdrs->where(function($q) use($filter) {
                    $q->where('areas.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('areas.desc_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'status') == 0)
            {
                if($filter['value'] > 0)
                {
                    $cartHdrs->where('cart_hdrs.doc_status', $filter['value']);
                }
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $cartHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'delivery_points') == 0)
            {
                $cartHdrs->leftJoin('delivery_points', 'delivery_points.id', '=', 'cart_hdrs.delivery_point_id');
            }
            if(strcmp($field, 'users') == 0)
            {
                $cartHdrs->leftJoin('users', 'users.id', '=', 'cart_hdrs.salesman_id');
            }
            if(strcmp($field, 'areas') == 0)
            {
                $cartHdrs->leftJoin('areas', 'areas.id', '=', 'delivery_points.area_id');
            }
        }
        
        if($pageSize > 0)
        {
            return $cartHdrs
                ->paginate($pageSize);
        }
        else
        {
            return $cartHdrs
                ->paginate(PHP_INT_MAX);
        }
    }

    static public function findAllByHdrIds($hdrIds)
    {
        $cartHdrs = CartHdr::whereIn('id', $hdrIds)
            ->get();

        return $cartHdrs;
    }

    static public function findAllNotExistOutbOrd01Txn($divisionId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $qStatuses = array(DocStatus::$MAP['WIP'],DocStatus::$MAP['COMPLETE']);
        $outbOrd01TxnFlows = DocTxnFlow::select('doc_txn_flows.fr_doc_hdr_id')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['OUTB_ORD_01'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\CartHdr::class)
                    ->distinct();

        //$qStatuses = array(DocStatus::$MAP['DRAFT'], DocStatus::$MAP['WIP'], DocStatus::$MAP['COMPLETE']);
        $leftJoinHash = array();
        $cartHdrs = CartHdr::select('cart_hdrs.*')
            ->leftJoinSub($outbOrd01TxnFlows, 'outb_ord_01_txn_flows', function ($join) {
                $join->on('cart_hdrs.id', '=', 'outb_ord_01_txn_flows.fr_doc_hdr_id');
            })
            ->whereNull('outb_ord_01_txn_flows.fr_doc_hdr_id')
            ->where('division_id', $divisionId)
            ->whereIn('doc_status', $qStatuses);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $cartHdrs->where('doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'salesman') == 0)
            {
                $leftJoinHash['users'] = 'users';
                $cartHdrs->where('users.username', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $cartHdrs->where(function($q) use($filter) {
                    $q->where('delivery_points.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('delivery_points.company_name_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'delivery_point_area') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $leftJoinHash['areas'] = 'areas';
                $cartHdrs->where(function($q) use($filter) {
                    $q->where('areas.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('areas.desc_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $cartHdrs->orderBy('doc_code', $sort['order']);
            }
            if(strcmp($sort['field'], 'delivery_point_area_code') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $cartHdrs->orderBy('delivery_points.area_code', $sort['order']);
            }
            if(strcmp($sort['field'], 'salesman_username') == 0)
            {
                $leftJoinHash['users'] = 'users';
                $cartHdrs->orderBy('users.username', $sort['order']);
            }
            if(strcmp($sort['field'], 'delivery_point_code') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $cartHdrs->orderBy('delivery_points.code', $sort['order']);
            }         
            if(strcmp($sort['field'], 'doc_date') == 0)
            {
                $cartHdrs->orderBy('doc_date', $sort['order']);
            }
            if(strcmp($sort['field'], 'net_amt') == 0)
            {
                $cartHdrs->orderBy('net_amt', $sort['order']);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'users') == 0)
            {
                $cartHdrs->leftJoin('users', 'users.id', '=', 'cart_hdrs.salesman_id');
            }
            if(strcmp($field, 'delivery_points') == 0)
            {
                $cartHdrs->leftJoin('delivery_points', 'delivery_points.id', '=', 'cart_hdrs.delivery_point_id');
            }
            if(strcmp($field, 'areas') == 0)
            {
                $cartHdrs->leftJoin('areas', 'areas.id', '=', 'delivery_points.area_id');
            }
        }

        return $cartHdrs
        ->paginate($pageSize);
    }

    static public function findByPk($hdrId) 
	{
        $cartHdr = CartHdr::where('id', $hdrId)
            ->first();

        return $cartHdr;
    }

    static public function findOrCreateDraftHeader($divisionId, $userId, $debtorId) 
	{
        $division = DivisionRepository::findByPk($divisionId);
        if (empty($division)) {
            throw new \Exception('Division not found.');
        }

        $cartHdr = CartHdr::where('division_id', $divisionId)
            ->where('user_id', $userId)
            ->where('doc_status', DocStatus::$MAP['DRAFT'])
            ->first();

        $debtor = Debtor::where('id', $debtorId)->first();
        if (empty($debtor)) {
            throw new \Exception('Debtor not found.');
        }

        if (empty($cartHdr)) {
            $docNo = DocNo::where('doc_type', 'App\CartHdr')->first();
            if (empty($docNo)) {
                throw new \Exception('Document number has not been setup.');
            }

            $hdrData = array();
            $hdrData['division_id'] = $divisionId;
    
            $hdrData['company_id'] = $division->company_id;
            $hdrData['debtor_id'] = $debtorId;
            $hdrData['credit_term_id'] = 1;
            $hdrData['salesman_id'] = $userId;
            $hdrData['currency_id'] = 1;
            $hdrData['currency_rate'] = 1.000;
            $hdrData['user_id'] = $userId;
            $hdrData['doc_date'] = date('Y-m-d');
            $hdrData['est_del_date'] = date('Y-m-d');
            $cartHdr = CartHdrRepository::createHeader($docNo->id, $hdrData);
        }

        $cartHdr->load('cartDtls', 'cartDtls.item', 'cartDtls.uom', 'cartDtls.item.itemPrices', 
                'cartDtls.item.itemPhotos', 'cartDtls.promotions');

        $data = ['debtor_id'=>$debtorId, 'item_id' => 0, 'variant_type' => 0];

        $cartDtls = $cartHdr->cartDtls;
        $conflicts = []; //Used for identifying which promotion produces the conflicts
        $variant_flag = false; //false - no conflict, true - conflict

        foreach($cartHdr->cartDtls as $dtl) {
            $promotions = [];
            //Loop through current cart_dtl's promotions
            foreach($dtl->promotions as $key => $promotion) {
                //Assign item_id for checking
                $data['item_id'] = $dtl->item_id;

                //Get current promo info
                $promo = Promotion::where('id', $promotion->promotion_id)->first();
                //Assign latest promo desc
                $promotion->desc_01 = $promo->desc_01;
            }
        }
        
        //to load also item, price and uom information

        return array(
            'cartHdr' => $cartHdr
        );
    }

    static public function revertCompleteToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = CartHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                {
                    $exc = new ApiException(__('SlsOrd.doc_status_is_not_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CartHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertDraftToVoid($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = CartHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('SlsOrd.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CartHdr::class, $hdrModel->id);
                    throw $exc;
                }
                if($hdrModel->outb_ord_hdr_id > 0)
                {
                    //sales order can not be voided if got associated outbound order
                    $outbOrdHdrModel = OutbOrdHdr::where('id', $hdrModel->outb_ord_hdr_id)
                    ->lockForUpdate()
                    ->first();
                    if(!empty($outbOrdHdrModel))
                    {
                        $exc = new ApiException(__('SlsOrd.outbound_order_is_not_void', ['docCode'=>$outbOrdHdrModel->doc_code]));
                        $exc->addData(\App\CartHdr::class, $hdrModel->id);
                        throw $exc;
                    }
                }
                
                //1) update docStatus to VOID
                $hdrModel->doc_status = DocStatus::$MAP['VOID'];
                $hdrModel->save();

                $frDocTxnFlows = $hdrModel->frDocTxnFlows;
                foreach($frDocTxnFlows as $frDocTxnFlow)
                {
                    //move to voidTxn
                    $txnVoidModel = new DocTxnVoid;
                    $txnVoidModel->id = $frDocTxnFlow->id;
                    $txnVoidModel->proc_type = $frDocTxnFlow->proc_type;
                    $txnVoidModel->fr_doc_hdr_type = $frDocTxnFlow->fr_doc_hdr_type;
                    $txnVoidModel->fr_doc_hdr_id = $frDocTxnFlow->fr_doc_hdr_id;
                    $txnVoidModel->fr_doc_hdr_code = $frDocTxnFlow->fr_doc_hdr_code;
                    $txnVoidModel->to_doc_hdr_type = $frDocTxnFlow->to_doc_hdr_type;
                    $txnVoidModel->to_doc_hdr_id = $frDocTxnFlow->to_doc_hdr_id;
                    $txnVoidModel->is_closed = $frDocTxnFlow->is_closed;
                    $txnVoidModel->save();
        
                    //delete the txn
                    $frDocTxnFlow->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertWipToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = CartHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['WIP'])
                {
                    $exc = new ApiException(__('SlsOrd.doc_status_is_not_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CartHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function syncSlsOrdSync01($company, $division, $data) 
	{
        $siteFlow = $division->siteFlow;

        $cartHdrModel = DB::transaction
        (
            function() use ($siteFlow, $company, $division, $data)
            {
                $isDirty = false;
                $model = CartHdr::firstOrCreate(['doc_code' => $data['doc_code']]);
                //get this sales order docFlowTxn
                $outbOrdHdrId = 0;
                $toDocTxnFlows = $model->toDocTxnFlows;
                foreach($toDocTxnFlows as $toDocTxnFlow)
                {
                    if(strcmp($toDocTxnFlow->to_doc_hdr_type, \App\OutbOrdHdr::class) != 0)
                    {
                        //skip if this is not outbOrdHdr
                        continue;
                    }
                    $outbOrdHdrId = $toDocTxnFlow->to_doc_hdr_id;
                }

                //check if this order oledi have outb_ord_hdr_id
                if($outbOrdHdrId > 0)
                {
                    //if already transfer to outOrd, then no need to do any sync
                    //but update the est_del_date, desc_01, desc_02
                    /*
                    $model->est_del_date = $data['userdate_01'];
                    $model->desc_01 = $data['remark_01'];
                    $model->desc_02 = $data['remark_02'];
                    $model->save();
                    
                    $outbOrdHdr = OutbOrdHdr::find($model->outb_ord_hdr_id);
                    $outbOrdHdr->est_del_date = $model->est_del_date;
                    $outbOrdHdr->desc_01 = $model->desc_01;
                    $outbOrdHdr->desc_02 = $model->desc_02;
                    $outbOrdHdr->save();
                    */
                    return $model;
                }
                
                $model->proc_type = ProcType::$MAP['SLS_ORD_SYNC_01'];
                if($model->doc_status == DocStatus::$MAP['NULL'])
                {
                    //initialize with DRAFT status
                    $model->doc_status = DocStatus::$MAP['DRAFT'];
                }             
                $model->ref_code_01 = $data['ref_code'];
                $model->doc_date = $data['doc_date'];
                $model->est_del_date = $data['userdate_01'];
                $model->desc_01 = $data['remark_01'];
                $model->desc_02 = $data['remark_02'];

                $model->division_id = $division->id;
                $model->site_flow_id = $division->site_flow_id;
                $model->company_id = $company->id;

                //currency
                $model->currency_id = 1;
                $model->currency_rate = $data['currency_rate'];
                $currency = CurrencyRepository::syncSlsOrdSync01($data);
                if(!empty($currency))
                {
                    $model->currency_id = $currency->id;
                }
                
                //creditTerm
                $model->credit_term_id = 0;
                if(strcmp($data['sales_type'], 'c') == 0)
                {
                    //if sales order sales_type = 0, always set to CASH term
                    //so will generate another invoice code
                    $model->credit_term_id = 1;
                }
                else
                {
                    $creditTerm = CreditTermRepository::syncSlsOrdSync01($data);
                    if(!empty($creditTerm))
                    {
                        $model->credit_term_id = $creditTerm->id;
                    }
                }

                //salesman
                $salesman = UserRepository::syncSlsOrdSync01($division, $data);
                $model->salesman_id = $salesman->id;

                //delivery point                
                $deliveryPoint = DeliveryPointRepository::syncSlsOrdSync01($division, $data);
                $model->debtor_id = $deliveryPoint->debtor_id;
                $model->delivery_point_id = $deliveryPoint->id;

                $model->gross_amt = $data['order_amt'];
                $model->gross_local_amt = $data['order_local_amt'];
                $model->disc_amt = $data['disc_amt'];
                $model->disc_local_amt = $data['disc_local_amt'];
                $model->tax_amt = $data['tax_amt'];
                $model->tax_local_amt = $data['tax_local_amt'];
                $model->is_round_adj = 0;
                $model->round_adj_amt = 0;
                $model->round_adj_local_amt = 0;
                $model->net_amt = $data['net_amt'];
                $model->net_local_amt = $data['net_local_amt'];

                $cartDtls = CartDtl::where('hdr_id', $model->id)
                                ->get();
                $cartDtlsHashByLineNo = array();
                $updatedCartDtls = array();
                $deletedCartDtls = array();
                foreach($cartDtls as $cartDtl)
                {
                    if(array_key_exists($cartDtl->line_no, $cartDtlsHashByLineNo))
                    {
                        $deletedCartDtls[] = $cartDtl;
                    }
                    else
                    {
                        $cartDtlsHashByLineNo[$cartDtl->line_no] = $cartDtl;
                    }
                }

                $details = $data['details'];
                $delDate = $data['doc_date'];
                foreach($details as $detail)
                {
                    $delDate = $detail['del_date'];

                    $cartDtl = null;
                    if(array_key_exists($detail['line_no'], $cartDtlsHashByLineNo))
                    {
                        $cartDtl = $cartDtlsHashByLineNo[$detail['line_no']];
                        unset($cartDtlsHashByLineNo[$detail['line_no']]);
                    }
                    else
                    {
                        $cartDtl = new CartDtl();
                        $cartDtl->hdr_id = $model->id;
                    }

                    $cartDtl->line_no = $detail['line_no'];

                    $location = LocationRepository::syncSlsOrdSync01($siteFlow->site_id, $detail);
                    $cartDtl->location_id = $location->id;

                    $uom = UomRepository::syncSlsOrdSync01($division, $detail);
                    
                    $item = ItemRepository::syncSlsOrdSync01($division, $uom, $detail);
                    $cartDtl->item_id = $item->id;
                    $cartDtl->desc_01 = $detail['description'];
                    $cartDtl->desc_02 = '';
                    $cartDtl->uom_id = $uom->id;
                    $cartDtl->uom_rate = $detail['uom_rate'];
                    if(bccomp($detail['list_price'], $detail['price'], 2) > 0)
                    {
                        $cartDtl->sale_price = $detail['list_price'];
                        $cartDtl->price_disc = bcsub($detail['list_price'], $detail['price'], 2);
                    }
                    else
                    {
                        $cartDtl->sale_price = $detail['price'];
                        $cartDtl->price_disc = 0;
                    }
                    $cartDtl->qty = $detail['qty'];
                    $cartDtl->dtl_disc_perc_01 = $detail['disc_percent_01'] >= 0 ? $detail['disc_percent_01'] : 0; //for efichain disc amt
                    $cartDtl->dtl_disc_val_01 = 0;
                    if(bccomp($detail['disc_percent_01'], -1) == 0)
                    {
                        $cartDtl->dtl_disc_val_01 = $detail['usernumber_01'];
                    }
                    $cartDtl->dtl_disc_perc_02 = $detail['disc_percent_02'];
                    $cartDtl->dtl_disc_perc_03 = $detail['disc_percent_03'];
                    $cartDtl->dtl_disc_perc_04 = $detail['disc_percent_04'];
                    $cartDtl->dtl_disc_amt = $detail['disc_amt'];
                    $cartDtl->dtl_disc_local_amt = $detail['disc_local_amt'];
                    $cartDtl->hdr_disc_perc_01 = $detail['disc_percent_05'];
                    $cartDtl->hdr_disc_perc_02 = $detail['disc_percent_06'];
                    $cartDtl->hdr_disc_perc_03 = $detail['disc_percent_07'];
                    $cartDtl->hdr_disc_perc_04 = $detail['disc_percent_08'];
                    $cartDtl->dtl_tax_perc_01 = $detail['tax_percent_01'];
                    $cartDtl->dtl_tax_perc_02 = $detail['tax_percent_02'];
                    $cartDtl->dtl_tax_perc_03 = $detail['tax_percent_03'];
                    $cartDtl->dtl_tax_perc_04 = $detail['tax_percent_04'];
                    $cartDtl->dtl_tax_amt_01 = $detail['tax_amt'];
                    $cartDtl->dtl_tax_local_amt_01 = $detail['tax_local_amt'];
                    $cartDtl->gross_amt = $detail['order_amt'];
                    $cartDtl->gross_local_amt = $detail['order_local_amt'];
                    $cartDtl->net_amt = $detail['net_amt'];
                    $cartDtl->net_local_amt = $detail['net_local_amt'];

                    if($cartDtl->isDirty() == true)
                    {
                        $isDirty = $cartDtl->isDirty();
                    }

                    $updatedCartDtls[] = $cartDtl;
                }
                $model->est_del_date = $delDate;

                if($model->isDirty() == true)
                {
                    $isDirty = $model->isDirty();
                }
                if(count($cartDtlsHashByLineNo) > 0)
                {
                    $isDirty = true;
                }
                if(count($deletedCartDtls) > 0)
                {
                    $isDirty = true;
                }

                if($isDirty == true)
                {
                    //will check for models dirty, if is dirty, then only will revert the model to DRAFT
                    if($model->doc_status >= DocStatus::$MAP['WIP'])
                    {
                        //return to draft, so can update the details
                        if($model->doc_status == DocStatus::$MAP['COMPLETE'])
                        {
                            $tmpModel = self::revertCompleteToDraft($model->id);
                            $model->doc_status = $tmpModel->doc_status;
                        }
                        elseif($model->doc_status == DocStatus::$MAP['WIP'])
                        {
                            $tmpModel = self::revertWipToDraft($model->id);
                            $model->doc_status = $tmpModel->doc_status;
                        }
                    }

                    foreach($updatedCartDtls as $updatedCartDtl)
                    {
                        $updatedCartDtl->save();
                    }
                    foreach($cartDtlsHashByLineNo as $lineNo => $cartDtl)
                    {
                        $cartDtl->delete();
                    }
                    foreach($deletedCartDtls as $cartDtl)
                    {
                        $cartDtl->delete();
                    }
                    $model->save();
                }

                if($data['status'] == 3)
                {
                    //set to COMPLETE
                    if($model->doc_status == DocStatus::$MAP['VOID'])
                    {
                        $model = self::commitVoidToDraft($model->id);
                    }

                    if($model->doc_status < DocStatus::$MAP['COMPLETE'])
                    {
                        $model = self::commitToComplete($model->id, true);
                    }
                }
                else
                {
                    //set to WIP
                    if($model->doc_status == DocStatus::$MAP['COMPLETE'])
                    {
                        $model = self::revertCompleteToDraft($model->id);
                    }
                    if($model->doc_status == DocStatus::$MAP['DRAFT'])
                    {
                        $model = self::commitToWip($model->id);
                    }
                }
                
                return $model;
            }, 
            5 //reattempt times
        );
        return $cartHdrModel;
    }

    static public function txnFindByPk($hdrId) 
	{
        $model = DB::transaction
        (
            function() use ($hdrId)
            {
                $cartHdr = CartHdr::where('id', $hdrId)
                    ->first();
                return $cartHdr;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function transferToSlsOdrHdr($hdrData) {
        
        $docNo = DocNo::where('doc_type', 'App\SlsOrdHdr')->first();
        if (empty($docNo)) {
            throw new \Exception('Document number has not been setup.');
        }

        //process data before transfer to sales order
        //set order date as today
        $hdrData['doc_date'] = date('Y-m-d');
        $hdrData['est_del_date'] = date('Y-m-d');
        $hdrData['salesman_id'] = $hdrData['user_id'];
        unset($hdrData['user_id']);

        return SlsOrdHdrRepository::createHeader(ProcType::$MAP['CART_ORD_01'], $docNo->id, $hdrData);
        // $slsOrdHdrData = $slsOrdHdr->toArray();
        // $slsOrdDtlArray = array();
        // foreach($cartDtlArray as $cartDtlData)
        // {
        //     $cartDtlData['id'] = 'NEW';
        //     $slsOrdDtlArray[] = $cartDtlData;
        // }
        // $result = SlsOrdHdrRepository::updateDetails($slsOrdHdrData, $slsOrdDtlArray);
        // return $result;
    }

    static public function updateDetails($cartHdrData, $cartDtlArray, $delCartDtlArray) 
	{
        $delCartDtlIds = array();
        foreach($delCartDtlArray as $delCartDtlData)
        {
            $delCartDtlIds[] = $delCartDtlData['id'];
        }

        // $delOutbOrdDtlIds = array();
        // foreach($delOutbOrdDtlArray as $delOutbOrdDtlData)
        // {
        //     $delOutbOrdDtlIds[] = $delOutbOrdDtlData['id'];
        // }
        
        $result = DB::transaction
        (
            function() use ($cartHdrData, $cartDtlArray, $delCartDtlIds)
            {
                // //update outbOrdHdr
                // $outbOrdHdrIdHashByUuid = array();
                // $outbOrdDtlIdHashByUuid = array();
                // if(!empty($outbOrdHdrData))
                // {
                //     $outbOrdHdrModel = OutbOrdHdr::lockForUpdate()->find($outbOrdHdrData['id']);
                //     if($outbOrdHdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                //     {
                //         //only DRAFT or above can transition to COMPLETE
                //         $exc = new ApiException(__('OutbOrd.doc_status_is_complete', ['docCode'=>$outbOrdHdrModel->doc_code]));
                //         $exc->addData(\App\OutbOrdHdr::class, $outbOrdHdrModel->id);
                //         throw $exc;
                //     }
                //     $outbOrdHdrModel = RepositoryUtils::dataToModel($outbOrdHdrModel, $outbOrdHdrData);
                //     $outbOrdHdrModel->save();

                //     //update outbOrdDtl array
                //     foreach($outbOrdDtlArray as $outbOrdDtlData)
                //     {
                //         $uuid = '';
                //         $outbOrdDtlModel = null;
                //         if (strpos($outbOrdDtlData['id'], 'NEW') !== false) 
                //         {
                //             $uuid = $outbOrdDtlData['id'];
                //             unset($outbOrdDtlData['id']);
                //             $outbOrdDtlModel = new OutbOrdDtl;
                //         }
                //         else
                //         {
                //             $outbOrdDtlModel = OutbOrdDtl::lockForUpdate()->find($outbOrdDtlModel['id']);
                //         }
                        
                //         $outbOrdDtlModel = RepositoryUtils::dataToModel($outbOrdDtlModel, $outbOrdDtlData);
                //         $outbOrdDtlModel->hdr_id = $outbOrdHdrModel->id;
                //         $outbOrdDtlModel->save();

                //         if(!empty($uuid))
                //         {
                //             $outbOrdHdrIdHashByUuid[$uuid] = $outbOrdDtlModel->hdr_id;
                //             $outbOrdDtlIdHashByUuid[$uuid] = $outbOrdDtlModel->id;
                //         }
                //     }

                //     if(!empty($delOutbOrdDtlIds))
                //     {
                //         OutbOrdDtl::destroy($delOutbOrdDtlIds);
                //     }
                // }

                //update cartHdr
                $cartHdrModel = CartHdr::lockForUpdate()->find($cartHdrData['id']);
                if($cartHdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('SlsOrd.doc_status_is_complete', ['docCode'=>$cartHdrModel->doc_code]));
                    $exc->addData(\App\CartHdr::class, $cartHdrModel->id);
                    throw $exc;
                }
                $cartHdrModel = RepositoryUtils::dataToModel($cartHdrModel, $cartHdrData);
                $cartHdrModel->save();

                //update cartDtl array
                $cartDtlModels = array();
                foreach($cartDtlArray as $cartDtlData)
                {
                    $cartDtlModel = null;
                    if (strpos($cartDtlData['id'], 'NEW') !== false) 
                    {
                        $uuid = $cartDtlData['id'];
                        $cartDtlModel = new CartDtl;
                        // if(array_key_exists($uuid, $outbOrdHdrIdHashByUuid))
                        // {
                        //     $cartDtlModel->outb_ord_hdr_id = $outbOrdHdrIdHashByUuid[$uuid];
                        // }
                        // if(array_key_exists($uuid, $outbOrdDtlIdHashByUuid))
                        // {
                        //     $cartDtlModel->outb_ord_dtl_id = $outbOrdDtlIdHashByUuid[$uuid];
                        // }
                        unset($cartDtlData['id']);
                    }
                    else
                    {
                        $cartDtlModel = CartDtl::lockForUpdate()->find($cartDtlData['id']);
                    }
                    
                    $cartDtlModel = RepositoryUtils::dataToModel($cartDtlModel, $cartDtlData);
                    $cartDtlModel->hdr_id = $cartHdrModel->id;
                    $cartDtlModel->save();
                    $cartDtlModels[] = $cartDtlModel;
                }

                if(!empty($delCartDtlIds))
                {
                    CartDtl::destroy($delCartDtlIds);
                    CartDtlPromotionRepository::deleteAllByDtlIds($delCartDtlIds);
                }

                return array(
                    'hdrModel' => $cartHdrModel,
                    'dtlModels' => $cartDtlModels
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    // static public function checkSlsOrdWithInverze($company, $division, $data) 
	// {
    //     $siteFlow = $division->siteFlow;

    //     $cartHdrModel = DB::transaction
    //     (
    //         function() use ($siteFlow, $company, $division, $data)
    //         {
    //             $isDirty = false;
    //             $model = CartHdr::where('doc_code', $data['doc_code'])->first();
    //             //get this sales order docFlowTxn
    //             $outbOrdHdrId = 0;
    //             if(!empty($model))
    //             {
    //                 $toDocTxnFlows = $model->toDocTxnFlows;
    //                 foreach($toDocTxnFlows as $toDocTxnFlow)
    //                 {
    //                     if(strcmp($toDocTxnFlow->to_doc_hdr_type, \App\OutbOrdHdr::class) != 0)
    //                     {
    //                         //skip if this is not outbOrdHdr
    //                         continue;
    //                     }
    //                     $outbOrdHdrId = $toDocTxnFlow->to_doc_hdr_id;
    //                 }
    //             }
    //             else
    //             {
    //                 //Log::error($data['doc_code'].' not found');
    //             }

    //             //check if this order oledi have outb_ord_hdr_id
    //             if($outbOrdHdrId > 0)
    //             {
    //                 $outbOrdHdrModel = OutbOrdHdr::find($outbOrdHdrId);

    //                 if(bccomp($data['net_amt'], $outbOrdHdrModel->net_amt, 5) != 0)
    //                 {
    //                     Log::error(
    //                         $outbOrdHdrModel->sls_ord_hdr_code.' '.
    //                         $outbOrdHdrModel->sls_inv_hdr_code.' '.
    //                         ' wrong amt '.
    //                         $outbOrdHdrModel->net_amt.
    //                         ' correct amt '.
    //                         $data['net_amt']
    //                     );
    //                 }
    //             }
                
    //             return $model;
    //         }, 
    //         5 //reattempt times
    //     );
    //     return $cartHdrModel;
    // }
}