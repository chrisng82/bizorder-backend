<?php

namespace App\Repositories;

use App\Services\Utils\RepositoryUtils;
use App\GdsRcptHdr;
use App\GdsRcptDtl;
use App\GdsRcptCDtl;
use App\GdsRcptItem;
use App\AdvShipHdr;
use App\DocTxnFlow;
use App\DocTxnVoid;
use App\HandlingUnit;
use App\GdsRcptInbOrd;
use App\QuantBalTxn;
use App\Item;
use App\ItemUom;
use App\Uom;
use App\Repositories\DocNoRepository;
use App\Repositories\HandlingUnitRepository;
use App\Services\Env\ResStatus;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Services\Env\HandlingType;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\DB;

class GdsRcptHdrRepository 
{
    static public function txnFindByPk($hdrId) 
	{
        $model = DB::transaction
        (
            function() use ($hdrId)
            {
                $gdsRcptHdr = GdsRcptHdr::where('id', $hdrId)
                    ->first();
                return $gdsRcptHdr;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function findAllByHdrIds($hdrIds)
    {
        $gdsRcptHdrs = GdsRcptHdr::whereIn('id', $hdrIds)
            ->get();

        return $gdsRcptHdrs;
    }

    static public function commitToWip($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = GdsRcptHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('GdsRcpt.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
                    throw $exc;
                }

                $dtlModels = GdsRcptDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->orderBy('line_no')
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('GdsRcpt.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to WIP
                $hdrModel->doc_status = DocStatus::$MAP['WIP'];
                $hdrModel->save();

                $dtlModels = GdsRcptDtl::where('hdr_id', $hdrModel->id)
                    ->lockForUpdate()
                    ->get();
                $dtlModels->load('company');
                foreach($dtlModels as $dtlModel)
                {
                    $company = $dtlModel->company;

                    if(empty($company))
                    {
                        //only DRAFT or above can transition to COMPLETE
                        $exc = new ApiException(__('GdsRcpt.company_is_required', ['lineNo'=>$dtlModel->line_no]));
                        $exc->addData(\App\GdsRcptDtl::class, $dtlModel->id);
                        throw $exc;
                    }
                }
                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitToComplete($hdrId, $isNonZeroBal)
    {
        $gdsRcptItemHash = array();
        //query the gdsRcptItems
        $gdsRcptItems = GdsRcptItemRepository::findAllByHdrId($hdrId);
        foreach($gdsRcptItems as $gdsRcptItem)
        {
            $gdsRcptItemHash[$gdsRcptItem->item_id] = $gdsRcptItem;
        }

        $hdrModel = DB::transaction
        (
            function() use ($hdrId, $isNonZeroBal, $gdsRcptItemHash)
            {
                $hdrModel = GdsRcptHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();
                    
                if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('GdsRcpt.doc_status_is_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
                    throw $exc;
                }
                if($hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('GdsRcpt.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
                    throw $exc;
                }

                $siteFlow = $hdrModel->siteFlow;
        
                //1) update docStatus to COMPLETE
                $hdrModel->doc_status = DocStatus::$MAP['COMPLETE'];
                $hdrModel->save();

                $dtlModels = GdsRcptDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('GdsRcpt.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
                    throw $exc;
                }
                foreach($dtlModels as $dtlModel)
                {                    
                    if(bccomp($dtlModel->qty, 0, 5) == 0)
                    {
                        continue;
                    }
                    //lock the item also, for status update and itemUom update
                    $item = Item::where('id', $dtlModel->item_id)
                        ->lockForUpdate()
                        ->first();
                    if(empty($item))
                    {
                        //only DRAFT or above can transition to COMPLETE
                        $exc = new ApiException(__('GdsRcpt.item_is_required', ['lineNo'=>$dtlModel->line_no]));
                        $exc->addData(\App\GdsRcptDtl::class, $dtlModel->id);
                        throw $exc;
                    }

                    if($dtlModel->company_id == 0)
                    {
                        //only DRAFT or above can transition to COMPLETE
                        $exc = new ApiException(__('GdsRcpt.company_is_required', ['lineNo'=>$dtlModel->line_no]));
                        $exc->addData(\App\GdsRcptDtl::class, $dtlModel->id);
                        throw $exc;
                    }       
                    
                    if($dtlModel->to_storage_bin_id == 0)
                    {
                        //only DRAFT or above can transition to COMPLETE
                        $exc = new ApiException(__('GdsRcpt.to_storage_bin_is_required', ['lineNo'=>$dtlModel->line_no]));
                        $exc->addData(\App\GdsRcptDtl::class, $dtlModel->id);
                        throw $exc;
                    }

                    if(strcmp($dtlModel->expiry_date, '0000-00-00') == 0)
                    {
                        //expiry date is required
                        $exc = new ApiException(__('GdsRcpt.expiry_date_is_required', ['lineNo'=>$dtlModel->line_no]));
                        $exc->addData(\App\GdsRcptDtl::class, $dtlModel->id);
                        throw $exc;
                    }

                    if(array_key_exists($item->id, $gdsRcptItemHash))
                    {
                        $gdsRcptItem = $gdsRcptItemHash[$item->id];

                        //update item status to WIP
                        //update the item uom based on gdsRcptItem
                        if($item->status == ResStatus::$MAP['NEW'])
                        {
                            //update the dtlModel if uom_rate is different
                            if($dtlModel->uom_id == Uom::$UNIT)
                            {
                                if(bccomp($dtlModel->uom_rate, 1, 5) != 0)
                                {
                                    $dtlModel->uom_rate = 1;
                                    $dtlModel->save();
                                }
                            }
                            elseif($dtlModel->uom_id == Uom::$CASE)
                            {
                                if(bccomp($dtlModel->uom_rate, $gdsRcptItem->case_uom_rate, 5) != 0)
                                {
                                    $dtlModel->uom_rate = $gdsRcptItem->case_uom_rate;
                                    $dtlModel->save();
                                }
                            }
                            else
                            {
                                //new item UOM must be UNIT or CASE
                                $exc = new ApiException(__('GdsRcpt.new_item_uom_must_be_unit_or_case', ['lineNo'=>$dtlModel->line_no]));
                                $exc->addData(\App\GdsRcptDtl::class, $dtlModel->id);
                                throw $exc;
                            }

                            $item->status = ResStatus::$MAP['WIP'];
                            $item->case_uom_rate = $gdsRcptItem->case_uom_rate;
                            $item->save();

                            $itemUoms = ItemUom::where('item_id', $item->id)
                                ->lockForUpdate()
                                ->get();
                            foreach($itemUoms as $itemUom)
                            {
                                if($itemUom->uom_id == Uom::$UNIT)
                                {
                                    $itemUom->barcode = $gdsRcptItem->unit_barcode;
                                    $itemUom->uom_rate = 1;
                                    $itemUom->save();
                                }
                                elseif($itemUom->uom_id == Uom::$CASE)
                                {
                                    $itemUom->barcode = $gdsRcptItem->case_barcode;
                                    $itemUom->uom_rate = $gdsRcptItem->case_uom_rate;
                                    $itemUom->save();
                                }
                            }
                        }
                    }

                    //this is stock receipt
                    $receiptDate = $hdrModel->doc_date;
                    if(strtotime($dtlModel->receipt_date) > 0)
                    {
                        $receiptDate = $dtlModel->receipt_date;
                    }
                    QuantBalRepository::commitStockReceipt(
                        $siteFlow->site_id, $item->id, $hdrModel->doc_date, \App\GdsRcptHdr::class, 
                        $hdrModel->id, $dtlModel->id, $dtlModel->company_id, 
                        $dtlModel->uom_id, $dtlModel->uom_rate, $dtlModel->qty, 
                        $dtlModel->batch_serial_no, $dtlModel->expiry_date, $receiptDate, 
                        $dtlModel->to_storage_bin_id, $dtlModel->to_handling_unit_id);
                }  
                return $hdrModel;              
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertCompleteToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = GdsRcptHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                {
                    $exc = new ApiException(__('GdsRcpt.doc_status_is_not_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                QuantBalRepository::revertStockTransaction(\App\GdsRcptHdr::class, $hdrModel->id);
                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertWipToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = GdsRcptHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['WIP'])
                {
                    $exc = new ApiException(__('GdsRcpt.doc_status_is_not_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
                    throw $exc;
                }

                //1) verify the toWhseJobs, make sure it is not WIP(download to mobile)
                $docTxnFlows = DocTxnFlow::where(array(
                    'fr_doc_hdr_type' => get_class($hdrModel),
                    'fr_doc_hdr_id' => $hdrModel->id,
                    'to_doc_hdr_type' => \App\WhseJobHdr::class
                    ))
                    ->get();
                foreach($docTxnFlows as $docTxnFlow)
                {
                    $toWhseJobHdr = \App\WhseJobHdr::where('id', $docTxnFlow->to_doc_hdr_id)
                        ->lockForUpdate()
                        ->first();
                    if($toWhseJobHdr->doc_status == DocStatus::$MAP['DRAFT']
                    || $toWhseJobHdr->doc_status == DocStatus::$MAP['WIP'])
                    {
                        $exc = new ApiException(__('WhseJob.job_status_is_draft_or_wip', ['docCode'=>$toWhseJobHdr->doc_code]));
                        $exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
                        throw $exc;
                    }
                }
                
                //2) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function findAllNotExistPutAway01Txn($siteFlowId, $docStatus, $sorts, $filters = array(), $pageSize = 20) 
	{
        $gdsRcptHdrs = GdsRcptHdr::select('gds_rcpt_hdrs.*')
            ->whereNotExists(function ($query) {
            $query->select(DB::raw(1))
                ->from('doc_txn_flows')
                ->where('doc_txn_flows.proc_type', ProcType::$MAP['PUT_AWAY_01'])
                ->where('doc_txn_flows.fr_doc_hdr_type', \App\GdsRcptHdr::class)
                ->whereRaw('doc_txn_flows.fr_doc_hdr_id = gds_rcpt_hdrs.id')
                ->where('doc_txn_flows.is_closed', 1);
            })
            ->where('site_flow_id', $siteFlowId)
            ->where('doc_status', $docStatus)
            ->where('proc_type', ProcType::$MAP['GDS_RCPT_01']);
        foreach($filters as $key => $value)
        {
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $gdsRcptHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        return $gdsRcptHdrs
            ->paginate($pageSize);
    }

    static public function createProcess($procType, $docNoId, $hdrData, $dtlDataListHashByHandlingUnitId, $inbOrdHdrIds, $docTxnFlowDataList, $itemDataList) 
	{
        $gdsRcptHdr = DB::transaction
        (
            function() use ($procType, $docNoId, $hdrData, $dtlDataListHashByHandlingUnitId, $inbOrdHdrIds, $docTxnFlowDataList, $itemDataList)
            {
                $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

                $hdrModel = new GdsRcptHdr;
                $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
                $hdrModel->doc_code = $newCode;
                $hdrModel->proc_type = $procType;
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                $txnHdrModelHash = array();
                foreach($dtlDataListHashByHandlingUnitId as $hashHandlingUnitId => $dtlDataList)
                {
                    $newHandlingUnitId = 0;
                    if($hashHandlingUnitId > 0)
                    {
                        //if $hashHandlingUnitId > 0, need to create a new HandlingUnit, and assign to model dtl
                        $newHandlingUnit = HandlingUnitRepository::create($hdrModel->site_flow_id, HandlingType::$MAP['PALLET']);
                        $newHandlingUnitId = $newHandlingUnit->id;
                    }
                    foreach($dtlDataList as $dtlData)
                    {
                        $dtlModel = new GdsRcptDtl;
                        $dtlModel = RepositoryUtils::dataToModel($dtlModel, $dtlData);
                        $dtlModel->hdr_id = $hdrModel->id;
                        $dtlModel->to_handling_unit_id = $newHandlingUnitId;
                        $dtlModel->save();

                        //create a carbon copy
                        $cDtlModel = new GdsRcptCDtl;
                        $cDtlModel = RepositoryUtils::dataToModel($cDtlModel, $dtlData);
                        $cDtlModel->hdr_id = $hdrModel->id;
                        $cDtlModel->to_handling_unit_id = $newHandlingUnitId;
                        $cDtlModel->save();
                    }
                }

                foreach($inbOrdHdrIds as $inbOrdHdrId)
                {
                    $gdsRcptInbOrdModel =  new GdsRcptInbOrd;
                    $gdsRcptInbOrdModel->hdr_id = $hdrModel->id;
                    $gdsRcptInbOrdModel->inb_ord_hdr_id = $inbOrdHdrId;
                    $gdsRcptInbOrdModel->save();
                }

                //process docTxnFlowDataList
                foreach($docTxnFlowDataList as $docTxnFlowData)
                {
                    $frDocHdrModel = $docTxnFlowData['fr_doc_hdr_type']::where('id', $docTxnFlowData['fr_doc_hdr_id'])
                    ->lockForUpdate()
                    ->first();
                    if($frDocHdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                    {
                        $exc = new ApiException(__('GdsRcpt.fr_doc_is_not_complete', ['docType'=>$docTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$docTxnFlowData['fr_doc_hdr_code']]));
                        $exc->addData($docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_code']);
                        throw $exc;
                    }

                    //verify the frDoc is not closed for this procType
                    $tmpDocTxnFlow = DocTxnFlowRepository::findByProcTypeAndFrHdrId($procType, $docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_id']);
                    if(!empty($tmpDocTxnFlow))
                    {
                        $exc = new ApiException(__('GdsRcpt.fr_doc_is_closed', ['docType'=>$docTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$docTxnFlowData['fr_doc_hdr_code']]));
                        $exc->addData($docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_id']);
                        throw $exc;
                    }
                    
                    $docTxnFlowModel = new DocTxnFlow;
                    $docTxnFlowModel = RepositoryUtils::dataToModel($docTxnFlowModel, $docTxnFlowData);
                    $docTxnFlowModel->proc_type = $procType;
                    $docTxnFlowModel->to_doc_hdr_type = \App\GdsRcptHdr::class;
                    $docTxnFlowModel->to_doc_hdr_id = $hdrModel->id;
                    $docTxnFlowModel->save();
                }

                //process gdsRcptItem
                foreach($itemDataList as $itemData)
                {
                    $gdsRcptItemModel = new GdsRcptItem;
                    $gdsRcptItemModel = RepositoryUtils::dataToModel($gdsRcptItemModel, $itemData);
                    $gdsRcptItemModel->hdr_id = $hdrModel->id;
                    $gdsRcptItemModel->save();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $gdsRcptHdr;
    }

    static public function findAllNotExistWhseJob1401Txn($siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $qStatuses = array(DocStatus::$MAP['DRAFT'], DocStatus::$MAP['WIP']); 
        $gdsRcptHdrs = GdsRcptHdr::select('gds_rcpt_hdrs.*')
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('doc_txn_flows')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['WHSE_JOB_14_01'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\GdsRcptHdr::class)
                    ->whereRaw('doc_txn_flows.fr_doc_hdr_id = gds_rcpt_hdrs.id')
                    ->where('doc_txn_flows.is_closed', 1);
            })
            ->where('proc_type', ProcType::$MAP['GDS_RCPT_01'])
            ->where('site_flow_id', $siteFlowId)
            ->whereIn('doc_status', $qStatuses);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $gdsRcptHdrs->where('gds_rcpt_hdrs.doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $gdsRcptHdrs->orderBy('doc_code', $sort['order']);
            }
        }
        
        return $gdsRcptHdrs
            ->paginate($pageSize);
    }

    static public function verifyTxn()
    {
        $invalidRows = array();

        $page = 1;
        $limit = 100;
        while(true)
        {
            $offset = ($page - 1) * $limit;
            $gdsRcptHdrs = GdsRcptHdr::where('doc_status', DocStatus::$MAP['COMPLETE'])
            ->offset($offset)
            ->limit($limit)
            ->get();
            foreach($gdsRcptHdrs as $gdsRcptHdr)
            {
                $quantBalTxnsHashByDtlId = array();

                $quantBalTxns = QuantBalTxn::where('doc_hdr_type', GdsRcptHdr::class)
                ->where('doc_hdr_id', $gdsRcptHdr->id)
                ->get();
                foreach($quantBalTxns as $quantBalTxn)
                {
                    $tmpQuantBalTxns = array();
                    if(array_key_exists($quantBalTxn->doc_dtl_id, $quantBalTxnsHashByDtlId))
                    {
                        $tmpQuantBalTxns = $quantBalTxnsHashByDtlId[$quantBalTxn->doc_dtl_id];
                    }

                    $tmpQuantBalTxns[] = $quantBalTxn;
                    $quantBalTxnsHashByDtlId[$quantBalTxn->doc_dtl_id] = $tmpQuantBalTxns;                    
                }

                $gdsRcptDtls = GdsRcptDtl::where('hdr_id', $gdsRcptHdr->id)
                ->get();
                foreach($gdsRcptDtls as $gdsRcptDtl)
                {
                    $ttlTxnUnitQty = 0;
                    if(array_key_exists($gdsRcptDtl->id, $quantBalTxnsHashByDtlId))
                    {
                        $quantBalTxns = $quantBalTxnsHashByDtlId[$gdsRcptDtl->id];
                        foreach($quantBalTxns as $quantBalTxn)
                        {
                            $ttlUnitQty = bcmul($quantBalTxn->sign, $quantBalTxn->unit_qty, 10);
                            $ttlTxnUnitQty = bcadd($ttlTxnUnitQty, $ttlUnitQty, 10);
                        }

                        $ttlDtlUnitQty = bcmul($gdsRcptDtl->qty, $gdsRcptDtl->uom_rate, 10);
                        if(bccomp($ttlDtlUnitQty, $ttlTxnUnitQty, 10) != 0)
                        {
                            $invalidRows[] = 'GdsRcptDtl ['.$gdsRcptDtl->id.'] qty['.$ttlDtlUnitQty.'] not tally with txn['.$ttlTxnUnitQty.']';
                        }
                    }
                    else
                    {
                        $ttlDtlUnitQty = bcmul($gdsRcptDtl->qty, $gdsRcptDtl->uom_rate, 10);
                        if(bccomp($ttlDtlUnitQty, 0, 10) != 0)
                        {
                            $invalidRows[] = 'GdsRcptDtl ['.$gdsRcptDtl->id.'] qty['.$ttlDtlUnitQty.'] not tally with txn[NULL]';
                        }
                    }
                }
            }

            if(count($gdsRcptHdrs) == 0)
            {
                break;
            }

            $page++;
        }        

        return $invalidRows;
    }

    static public function findByPk($hdrId) 
	{
        $gdsRcptHdr = GdsRcptHdr::where('id', $hdrId)
            ->first();
        return $gdsRcptHdr;
    }

    static public function updateDetails($gdsRcptHdrData, $gdsRcptDtlArray, $delGdsRcptDtlArray) 
	{
        $delGdsRcptDtlIds = array();
        foreach($delGdsRcptDtlArray as $delGdsRcptDtlData)
        {
            $delGdsRcptDtlIds[] = $delGdsRcptDtlData['id'];
        }
        
        $result = DB::transaction
        (
            function() use ($gdsRcptHdrData, $gdsRcptDtlArray, $delGdsRcptDtlIds)
            {
                //update gdsRcptHdr
                $gdsRcptHdrModel = GdsRcptHdr::lockForUpdate()->find($gdsRcptHdrData['id']);
                if($gdsRcptHdrModel->doc_status >= DocStatus::$MAP['WIP'])
                {
                    //WIP and COMPLETE status can not be updated
                    $exc = new ApiException(__('GdsRcpt.doc_status_is_wip_or_complete', ['docCode'=>$gdsRcptHdrModel->doc_code]));
                    $exc->addData(\App\GdsRcptHdr::class, $gdsRcptHdrModel->id);
                    throw $exc;
                }
                $gdsRcptHdrModel = RepositoryUtils::dataToModel($gdsRcptHdrModel, $gdsRcptHdrData);
                $gdsRcptHdrModel->save();

                //update gdsRcptDtl array
                $gdsRcptDtlModels = array();
                foreach($gdsRcptDtlArray as $gdsRcptDtlData)
                {
                    $gdsRcptDtlModel = null;
                    if (strpos($gdsRcptDtlData['id'], 'NEW') !== false) 
                    {
                        $uuid = $gdsRcptDtlData['id'];
                        $gdsRcptDtlModel = new GdsRcptDtl;
                        unset($gdsRcptDtlData['id']);
                    }
                    else
                    {
                        $gdsRcptDtlModel = GdsRcptDtl::lockForUpdate()->find($gdsRcptDtlData['id']);
                    }
                    
                    $gdsRcptDtlModel = RepositoryUtils::dataToModel($gdsRcptDtlModel, $gdsRcptDtlData);
                    $gdsRcptDtlModel->hdr_id = $gdsRcptHdrModel->id;
                    $gdsRcptDtlModel->save();
                    $gdsRcptDtlModels[] = $gdsRcptDtlModel;
                }

                if(!empty($delGdsRcptDtlIds))
                {
                    GdsRcptDtl::destroy($delGdsRcptDtlIds);
                }

                return array(
                    'hdrModel' => $gdsRcptHdrModel,
                    'dtlModels' => $gdsRcptDtlModels
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function findAll($siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $gdsRcptHdrs = GdsRcptHdr::select('gds_rcpt_hdrs.*')
            ->where('site_flow_id', $siteFlowId);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $gdsRcptHdrs->where('gds_rcpt_hdrs.doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $gdsRcptHdrs->orderBy('doc_code', $sort['order']);
            }
        }
        
        return $gdsRcptHdrs
            ->paginate($pageSize);
    }

    static public function revertDraftToVoid($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = GdsRcptHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('GdsRcpt.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to VOID
                $hdrModel->doc_status = DocStatus::$MAP['VOID'];
                $hdrModel->save();

                $frDocTxnFlows = $hdrModel->frDocTxnFlows;
                foreach($frDocTxnFlows as $frDocTxnFlow)
                {
                    //move to voidTxn
                    $txnVoidModel = new DocTxnVoid;
                    $txnVoidModel->id = $frDocTxnFlow->id;
                    $txnVoidModel->proc_type = $frDocTxnFlow->proc_type;
                    $txnVoidModel->fr_doc_hdr_type = $frDocTxnFlow->fr_doc_hdr_type;
                    $txnVoidModel->fr_doc_hdr_id = $frDocTxnFlow->fr_doc_hdr_id;
                    $txnVoidModel->fr_doc_hdr_code = $frDocTxnFlow->fr_doc_hdr_code;
                    $txnVoidModel->to_doc_hdr_type = $frDocTxnFlow->to_doc_hdr_type;
                    $txnVoidModel->to_doc_hdr_id = $frDocTxnFlow->to_doc_hdr_id;
                    $txnVoidModel->is_closed = $frDocTxnFlow->is_closed;
                    $txnVoidModel->save();
        
                    //delete the txn
                    $frDocTxnFlow->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitVoidToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = GdsRcptHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['VOID'])
                {
                    //only VOID can transition to DRAFT
                    $exc = new ApiException(__('GdsRcpt.doc_status_is_not_void', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                $frDocTxnVoids = $hdrModel->frDocTxnVoids;
                foreach($frDocTxnVoids as $frDocTxnVoid)
                {
                    //verify first
                    $docTxnFlow = DocTxnFlow::where(array(
                        'proc_type' => $frDocTxnVoid->proc_type,
                        'fr_doc_hdr_type' => $frDocTxnVoid->fr_doc_hdr_type,
                        'fr_doc_hdr_id' => $frDocTxnVoid->fr_doc_hdr_id,
                        'to_doc_hdr_type' => $frDocTxnVoid->to_doc_hdr_type
                        ))
                        ->first();
                    if(!empty($docTxnFlow))
                    {
                        $exc = new ApiException(__('GdsRcpt.fr_doc_is_closed', ['docCode'=>$frDocTxnVoid->fr_doc_hdr_code]));
                        $exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
                        throw $exc;
                    }

                    //move to txnFlow
                    $txnFlowModel = new DocTxnFlow;
                    $txnFlowModel->id = $frDocTxnVoid->id;
                    $txnFlowModel->proc_type = $frDocTxnVoid->proc_type;
                    $txnFlowModel->fr_doc_hdr_type = $frDocTxnVoid->fr_doc_hdr_type;
                    $txnFlowModel->fr_doc_hdr_id = $frDocTxnVoid->fr_doc_hdr_id;
                    $txnFlowModel->fr_doc_hdr_code = $frDocTxnVoid->fr_doc_hdr_code;
                    $txnFlowModel->to_doc_hdr_type = $frDocTxnVoid->to_doc_hdr_type;
                    $txnFlowModel->to_doc_hdr_id = $frDocTxnVoid->to_doc_hdr_id;
                    $txnFlowModel->is_closed = $frDocTxnVoid->is_closed;
                    $txnFlowModel->save();
        
                    //delete the txnVoid
                    $frDocTxnVoid->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function findAllDraftAndWIP($divisionIds, $siteFlowId, $procType, $sorts, $filters = array(), $pageSize = 20) 
	{
        $divGdsRcptInbOrds = GdsRcptInbOrd::select('gds_rcpt_inb_ords.hdr_id')
                    ->leftJoin('inb_ord_hdrs', 'inb_ord_hdrs.id', '=', 'gds_rcpt_inb_ords.inb_ord_hdr_id')
                    ->whereIn('inb_ord_hdrs.division_id', $divisionIds)
                    ->distinct();

        $qStatuses = array(DocStatus::$MAP['DRAFT'], DocStatus::$MAP['WIP']); 
        $gdsRcptHdrs = GdsRcptHdr::where('site_flow_id', $siteFlowId)
            ->leftJoinSub($divGdsRcptInbOrds, 'div_gds_rcpt_inb_ords', function ($join) {
                $join->on('gds_rcpt_hdrs.id', '=', 'div_gds_rcpt_inb_ords.hdr_id');
            })
            ->whereNotNull('div_gds_rcpt_inb_ords.hdr_id')
            ->where('proc_type', $procType)
            ->whereIn('doc_status', $qStatuses);

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $gdsRcptHdrs->orderBy('doc_code', $sort['order']);
            }
        }
        return $gdsRcptHdrs
            ->paginate($pageSize);
    }

    static public function findAllNotExistInvDoc02Txn($divisionIds, $procType, $sorts, $filters = array(), $pageSize = 20) 
	{
        $invDoc02TxnFlows = DocTxnFlow::select('doc_txn_flows.fr_doc_hdr_id')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['INV_DOC_02'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\GdsRcptHdr::class)
                    ->distinct();
        $divGdsRcptInbOrds = GdsRcptInbOrd::select('gds_rcpt_inb_ords.hdr_id')
                    ->leftJoin('inb_ord_hdrs', 'inb_ord_hdrs.id', '=', 'gds_rcpt_inb_ords.inb_ord_hdr_id')
                    ->whereIn('inb_ord_hdrs.division_id', $divisionIds)
                    ->distinct();

        $leftJoinHash = array();
        $qStatuses = array(DocStatus::$MAP['COMPLETE']);
        $gdsRcptHdrs = GdsRcptHdr::select('gds_rcpt_hdrs.*')
            ->leftJoinSub($invDoc02TxnFlows, 'inv_doc_02_txn_flows', function ($join) {
                $join->on('gds_rcpt_hdrs.id', '=', 'inv_doc_02_txn_flows.fr_doc_hdr_id');
            })
            ->whereNull('inv_doc_02_txn_flows.fr_doc_hdr_id')
            ->leftJoinSub($divGdsRcptInbOrds, 'div_gds_rcpt_inb_ords', function ($join) {
                $join->on('gds_rcpt_hdrs.id', '=', 'div_gds_rcpt_inb_ords.hdr_id');
            })
            ->whereNotNull('div_gds_rcpt_inb_ords.hdr_id')
            ->where('proc_type', $procType)
            ->whereIn('doc_status', $qStatuses);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $gdsRcptHdrs->where('doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $gdsRcptHdrs->orderBy('doc_code', $sort['order']);
            }
        }
        return $gdsRcptHdrs
            ->paginate($pageSize);
    }
}