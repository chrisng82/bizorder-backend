<?php

namespace App\Repositories;

use App\PrintResTxn;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class PrintResTxnRepository
{
    static public function create($printResTxnDataArray) 
	{
        $printedAt = DB::transaction
        (
            function() use ($printResTxnDataArray)
            {
                $printedAt = Carbon::createFromTimestamp(0);
                foreach($printResTxnDataArray as $printResTxnData)
                {
                    $printResTxn = new PrintResTxn;
                    $printResTxn->res_type = $printResTxnData['res_type'];
                    $printResTxn->res_id = $printResTxnData['res_id'];
                    $printResTxn->user_id = $printResTxnData['user_id'];
                    $printResTxn->save();

                    $printedAt = $printResTxn->created_at;
                }

                return $printedAt;
            }, 
            5 //reattempt times
        );
        return $printedAt;
    }

    static public function queryPrintResTxn($resType, $resId) 
	{
        $cubicMeter = 0;
        $grossWeight = 0;

        $printResTxn = PrintResTxn::selectRaw('COUNT(id) AS print_count')
            ->selectRaw('IFNULL(MIN(updated_at), "1970-01-01 00:00:00") AS first_printed_at')
            ->selectRaw('IFNULL(MAX(updated_at), "1970-01-01 00:00:00") AS last_printed_at')
            ->where('res_type', $resType)
            ->where('res_id', $resId)
            ->first();
        return $printResTxn;
    }
}