<?php

namespace App\Repositories;

use App\User;
use App\UserDivision;
use App\Debtor;
use App\Services\Env\ResStatus;
use App\Services\Utils\ApiException;
use App\Services\Utils\RepositoryUtils;
use App\Repositories\DebtorRepository;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class UserRepository
{
    static public function findRandom()
	{
        $model = User::orderByRaw('RAND()')
            ->first();

        return $model;
    }

    static public function select2($search, $filters, $pageSize = 10)
	{
        $users = User::where(function($q) use($search) {
            $q->where('username', 'LIKE', '%' . $search . '%');
        });
        if(count($filters) >= 1)
        {
            $users = $users->where($filters);
        }
        return $users
            ->orderBy('username', 'ASC')
            ->paginate($pageSize);
    }

    static public function findTop()
	{
        $model = User::first();

        return $model;
    }

    static public function findByPk($id)
	{
        $model = User::where('id', $id)
            ->first();

        return $model;
    }

    static public function findByDebtorId($id)
	{
        $model = User::where('debtor_id', $id)
            ->first();

        return $model;
    }

    static public function findWithDevicesByDebtorId($id)
	{
        $model = User::join('devices', 'devices.user_id', 'users.id')->where('debtor_id', $id)
            ->first();

        return $model;
    }

    static public function syncUserSync03($division, $data)
	{
        $model = DB::transaction
        (
            function() use ($division, $data)
            {
                $model = User::where('username', $data['username'])
                    ->first();

                $debtor = DebtorRepository::findByCodeAndDivision($data['debtor_code'], $division->id);
                if(empty($debtor))
                {
                    throw new ApiException('Debtor '.$data['debtor_code'].' not found in division '.$division->code.'.');
                }

                if(empty($model))
                {
                    $model = new User;
                    $model->username = $data['username'];
                }
                $model->debtor_id = $debtor->id;
                $model->email = $data['email'];
                $model->first_name = $data['first_name'];
                $model->last_name = $data['last_name'];
                $model->login_type = $data['login_type'];
                $model->password = bcrypt('1234');
                $model->status = ResStatus::$MAP['ACTIVE'];
                $model->timezone = 'Asia/Singapore';
                $model->save();

                //check user division
                $userDivision = UserDivision::where('user_id', $model->id)
                    ->where('division_id', $division->id)
                    ->first();
                if(empty($userDivision))
                {
                    //START userDivision
                    $userDivision = new UserDivision;
                    $userDivision->user_id = $model->id;
                    $userDivision->division_id = $division->id;
                    $userDivision->ref_code_01 = '';
                    $userDivision->ref_code_02 = '';
                    $userDivision->save();
                    //END add userDivision
                }

                return $model;
            }
        );
        return $model;
    }

    static public function syncSlsOrdSync01($division, $data)
	{
        $model = DB::transaction
        (
            function() use ($division, $data)
            {
                $model = User::where('username', $data['salesman_code'])
                    ->first();
                if(empty($model))
                {
                    $model = new User;
                    $model->username = $data['salesman_code'];
                    $model->email = $data['salesman_email'];
                    $model->password = bcrypt('1234');
                    $model->status = ResStatus::$MAP['INACTIVE'];
                    $model->timezone = 'Asia/Singapore';
                    $model->save();
                }

                //check user division
                $userDivision = UserDivision::where('user_id', $model->id)
                    ->where('division_id', $division->id)
                    ->first();
                if(empty($userDivision))
                {
                    //START userDivision
                    $userDivision = new UserDivision;
                    $userDivision->user_id = $model->id;
                    $userDivision->division_id = $division->id;
                    $userDivision->ref_code_01 = '';
                    $userDivision->ref_code_02 = '';
                    $userDivision->save();
                    //END add userDivision
                }

                return $model;
            }
        );
        return $model;
    }

    static public function syncAdvShipSync01($division, $data)
	{
        $model = DB::transaction
        (
            function() use ($division, $data)
            {
                $model = User::where('username', $data['purchaser_code'])
                    ->first();
                if(empty($model))
                {
                    $model = new User;
                    $model->username = $data['purchaser_code'];
                    $model->email = $data['purchaser_email'];
                    $model->password = bcrypt('1234');
                    $model->status = ResStatus::$MAP['INACTIVE'];
                    $model->timezone = 'Asia/Singapore';
                    $model->save();
                }

                //check user division
                $userDivision = UserDivision::where('user_id', $model->id)
                    ->where('division_id', $division->id)
                    ->first();
                if(empty($userDivision))
                {
                    //START userDivision
                    $userDivision = new UserDivision;
                    $userDivision->user_id = $model->id;
                    $userDivision->division_id = $division->id;
                    $userDivision->ref_code_01 = '';
                    $userDivision->ref_code_02 = '';
                    $userDivision->save();
                    //END add userDivision
                }

                return $model;
            }
        );
        return $model;
    }

    static public function findAll($sorts, $filters = array(), $pageSize = 20)
	{
        $users = User::select('users.*');

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'username') == 0)
            {
                $users->where('users.username', 'LIKE', '%'.$filter['value'].'%');
            }
            elseif(strcmp($filter['field'], 'status') == 0)
            {
                $users->where('users.status', $filter['value']);
            }
            elseif(strcmp($filter['field'], 'login_type') == 0)
            {
                $users->where('users.login_type', $filter['value']);
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'username') == 0)
            {
                $users->orderBy('username', $sort['order']);
            }
            elseif(strcmp($sort['field'], 'last_login') == 0)
            {
                $users->orderBy('last_login', $sort['order']);
            }
            elseif(strcmp($sort['field'], 'password_changed_at') == 0)
            {
                $users->orderBy('password_changed_at', $sort['order']);
            }
        }

        if($pageSize > 0)
        {
            return $users
                ->paginate($pageSize);
        }
        else
        {
            return $users
                ->paginate(PHP_INT_MAX);
        }
    }

    static public function createModel($data)
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = new User;
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return $model;
            },
            5 //reattempt times
        );
        return $model;
    }

    static public function updateModel($data)
	{
        $result = DB::transaction
        (
            function() use ($data)
            {
                //update User
                if(array_key_exists('password', $data))
                {
                    unset($data['password']);
                }
                $model = User::lockForUpdate()->find($data['id']);
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return array(
                    'model' => $model
                );
            },
            5 //reattempt times
        );
        return $result;
    }

    static public function txnFindByPk($id)
	{
        $model = DB::transaction
        (
            function() use ($id)
            {
                $model = User::where('id', $id)
                    ->first();
                return $model;
            },
            5 //reattempt times
        );
        return $model;
    }

    static public function changePassword($id, $newPassword)
	{
        $model = DB::transaction
        (
            function() use ($id, $newPassword)
            {
                $model = User::lockForUpdate()->find($id);

                $model->password = bcrypt($newPassword);
                $model->password_changed_at = Carbon::now()->toDateTimeString();
                $model->save();

                return $model;
            },
            5 //reattempt times
        );
        return $model;
    }

    static public function updateDivisions($userId, $userDivisionArray, $delUserDivisionArray)
	{
        $delUserDivisionIds = array();
        foreach($delUserDivisionArray as $delUserDivisionData)
        {
            $delModel = UserDivision::where('user_id', $userId)
                        ->where('division_id', $delUserDivisionData['division_id'])->first();
            $delUserDivisionIds[] = $delModel->id;
        }

        $result = DB::transaction
        (
            function() use ($userId, $userDivisionArray, $delUserDivisionIds)
            {
                //update userDivision array
                $userDivisionModels = array();
                foreach($userDivisionArray as $userDivisionData)
                {
                    $userDivisionModel = null;
                    if (strpos($userDivisionData['id'], 'NEW') !== false)
                    {
                        $uuid = $userDivisionData['id'];
                        $userDivisionModel = new UserDivision;
                        unset($userDivisionData['id']);
                    }
                    else
                    {
                        $userDivisionModel = UserDivision::lockForUpdate()->find($userDivisionData['id']);
                    }

                    $userDivisionModel = RepositoryUtils::dataToModel($userDivisionModel, $userDivisionData);
                    $userDivisionModel->user_id = $userId;
                    $userDivisionModel->save();
                    $userDivisionModels[] = $userDivisionModel;
                }

                if(!empty($delUserDivisionIds))
                {
                    UserDivision::destroy($delUserDivisionIds);
                }

                return array(
                    'userDivisionModels' => $userDivisionModels
                );
            },
            5 //reattempt times
        );
        return $result;
    }
}
