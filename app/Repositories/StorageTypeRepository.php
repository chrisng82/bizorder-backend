<?php

namespace App\Repositories;

use App\Services\Env\BinType;
use App\StorageType;
use App\Services\Utils\RepositoryUtils;
use Illuminate\Support\Facades\DB;

class StorageTypeRepository 
{
    static public function findBySiteIdAndCode($siteId, $code) 
	{
        $storageType = StorageType::where('site_id', $siteId)
        ->where('code', $code)
        ->first();

        return $storageType;
    }

    static public function findAll($sorts, $filters = array(), $pageSize = 20) 
	{
        $storageTypes = StorageType::select('storage_types.*');
        foreach($filters as $key => $value)
        {
        }
        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'code') == 0)
            {
                $storageTypes->orderBy('code', $sort['order']);
            }
        }
        if($pageSize > 0)
        {
            return $storageTypes
            ->paginate($pageSize);
        }
        else
        {
            return $storageTypes
            ->paginate(PHP_INT_MAX);
        }
    }

    static public function findPalletConfiguration($siteId) 
	{
        $result = array();
        $result['pallet_min_load_length'] = 0;
        $result['pallet_min_load_width'] = 0;
        $result['pallet_min_load_height'] = 0;
        $result['pallet_min_load_weight'] = 0;
        $result['pallet_max_load_length'] = 0;
        $result['pallet_max_load_width'] = 0;
        $result['pallet_max_load_height'] = 0;
        $result['pallet_max_load_weight'] = 0;

        $binTypes = array();
        $binTypes[] = BinType::$MAP['BULK_STORAGE'];
        $binTypes[] = BinType::$MAP['PALLET_STORAGE'];
        $binTypes[] = BinType::$MAP['BROKEN_PALLET_STORAGE'];

        $storageTypes = StorageType::whereIn('bin_type', $binTypes)
        ->whereRaw('EXISTS(
            SELECT * FROM storage_bins
            WHERE storage_bins.site_id = ? AND storage_types.id = storage_bins.storage_type_id
        )', array($siteId))
        ->get();
        
        if(count($storageTypes) > 0)
        {
            $result['pallet_min_load_length'] = PHP_INT_MAX;
            $result['pallet_min_load_width'] = PHP_INT_MAX;
            $result['pallet_min_load_height'] = PHP_INT_MAX;
            $result['pallet_min_load_weight'] = PHP_INT_MAX;
            $result['pallet_max_load_length'] = 0;
            $result['pallet_max_load_width'] = 0;
            $result['pallet_max_load_height'] = 0;
            $result['pallet_max_load_weight'] = 0;
        }
        foreach($storageTypes as $storageType)
        {
            if(bccomp($result['pallet_min_load_length'], $storageType->hu_min_load_length, 5) > 0)
            {
                $result['pallet_min_load_length'] = $storageType->hu_min_load_length;
            }
            if(bccomp($result['pallet_min_load_width'], $storageType->hu_min_load_width, 5) > 0)
            {
                $result['pallet_min_load_width'] = $storageType->hu_min_load_width;
            }
            if(bccomp($result['pallet_min_load_height'], $storageType->hu_min_load_height, 5) > 0)
            {
                $result['pallet_min_load_height'] = $storageType->hu_min_load_height;
            }
            if(bccomp($result['pallet_min_load_weight'], $storageType->hu_min_load_weight, 5) > 0)
            {
                $result['pallet_min_load_weight'] = $storageType->hu_min_load_weight;
            }

            if(bccomp($result['pallet_max_load_length'], $storageType->hu_max_load_length, 5) < 0)
            {
                $result['pallet_max_load_length'] = $storageType->hu_max_load_length;
            }
            if(bccomp($result['pallet_max_load_width'], $storageType->hu_max_load_width, 5) < 0)
            {
                $result['pallet_max_load_width'] = $storageType->hu_max_load_width;
            }
            if(bccomp($result['pallet_max_load_height'], $storageType->hu_max_load_height, 5) < 0)
            {
                $result['pallet_max_load_height'] = $storageType->hu_max_load_height;
            }
            if(bccomp($result['pallet_max_load_weight'], $storageType->hu_max_load_weight, 5) < 0)
            {
                $result['pallet_max_load_weight'] = $storageType->hu_max_load_weight;
            }
        }

        return $result;
    }

    static public function plainCreate($data) 
	{
        $model = new StorageType();
        $model = RepositoryUtils::dataToModel($model, $data);
        $model->save();

        return $model;
    }
}