<?php

namespace App\Repositories;

use App\Services\Utils\RepositoryUtils;
use App\Uom;
use App\WhseJobItem;
use App\DocTxnFlow;
use App\DocTxnVoid;
use App\WhseJobHdr;
use App\WhseJobDtl;
use App\WhseJobCDtl;
use App\PickListHdr;
use App\PickListDtl;
use App\PackListHdr;
use App\PackListDtl;
use App\LoadListHdr;
use App\LoadListDtl;
use App\GdsRcptHdr;
use App\GdsRcptDtl;
use App\PutAwayHdr;
use App\PutAwayDtl;
use App\Services\Env\ProcType;
use App\Services\Env\DocStatus;
use App\Services\Env\WhseJobType;
use App\Services\Env\HandlingType;
use App\Services\Utils\ApiException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class WhseJobHdrRepository 
{
    static public function findByPk($hdrId) 
	{
        $whseJobHdr = WhseJobHdr::where('id', $hdrId)
            ->first();

        return $whseJobHdr;
    }

    static public function txnFindByPk($hdrId) 
	{
        $model = DB::transaction
        (
            function() use ($hdrId)
            {
                $whseJobHdr = WhseJobHdr::where('id', $hdrId)
                    ->first();
                return $whseJobHdr;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function createProcess($procType, $docNoId, $hdrData, $dtlDataList, $docTxnFlowDataList, $itemDataList = array()) 
	{
        if(empty($dtlDataList)
        && $procType != ProcType::$MAP['BIN_TRF_02']
        && $procType != ProcType::$MAP['WHSE_JOB_17_02'])
        {
            $exc = new ApiException(__('WhseJob.item_details_not_found', []));
            //$exc->addData(\App\PickListHdr::class, $hdrData->doc_code);
            throw $exc;
        }
        $whseJobHdr = DB::transaction
        (
            function() use ($procType, $docNoId, $hdrData, $dtlDataList, $docTxnFlowDataList, $itemDataList)
            {
                $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

                $hdrModel = new WhseJobHdr;
                $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
                $hdrModel->doc_code = $newCode;
                $hdrModel->proc_type = $procType;
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                foreach($dtlDataList as $lineNo => $dtlData)
                {
                    $dtlModel = new WhseJobDtl;
                    $dtlModel = RepositoryUtils::dataToModel($dtlModel, $dtlData);
                    $dtlModel->hdr_id = $hdrModel->id;
                    $dtlModel->save();

                    //create a carbon copy
                    $cDtlModel = new WhseJobCDtl;
                    $cDtlModel = RepositoryUtils::dataToModel($cDtlModel, $dtlData);
                    $cDtlModel->hdr_id = $hdrModel->id;
                    $cDtlModel->save();
                }

                //process docTxnFlowDataList
                foreach($docTxnFlowDataList as $docTxnFlowData)
                {
                    $frDocHdrModel = $docTxnFlowData['fr_doc_hdr_type']::where('id', $docTxnFlowData['fr_doc_hdr_id'])
                    ->lockForUpdate()
                    ->first();
                    if($frDocHdrModel->doc_status != DocStatus::$MAP['WIP'])
                    {
                        $exc = new ApiException(__('WhseJob.fr_doc_is_not_wip', ['docType'=>$docTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$docTxnFlowData['fr_doc_hdr_code']]));
                        $exc->addData($docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_code']);
                        throw $exc;
                    }
                    
                    //verify the frDoc is not closed for this procType
                    $tmpDocTxnFlow = DocTxnFlowRepository::findByProcTypeAndFrHdrId($procType, $docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_id'], 1);
                    if(!empty($tmpDocTxnFlow))
                    {
                        $exc = new ApiException(__('WhseJob.fr_doc_is_closed', ['docType'=>$docTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$docTxnFlowData['fr_doc_hdr_code']]));
                        $exc->addData($docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_code']);
                        throw $exc;
                    }

                    $docTxnFlowModel = new DocTxnFlow;
                    $docTxnFlowModel = RepositoryUtils::dataToModel($docTxnFlowModel, $docTxnFlowData);
                    $docTxnFlowModel->proc_type = $procType;
                    $docTxnFlowModel->to_doc_hdr_type = \App\WhseJobHdr::class;
                    $docTxnFlowModel->to_doc_hdr_id = $hdrModel->id;
                    $docTxnFlowModel->save();
                }

                //process whseJobItem
                foreach($itemDataList as $itemData)
                {
                    $whseJobItemModel = new WhseJobItem;
                    $whseJobItemModel = RepositoryUtils::dataToModel($whseJobItemModel, $itemData);
                    $whseJobItemModel->hdr_id = $hdrModel->id;
                    $whseJobItemModel->save();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $whseJobHdr;
    }

    static public function commitToWip($hdrId, $mobileProfileId = 0, 
        $worker01Id = 0, $worker02Id = 0, $worker03Id = 0, $worker04Id = 0, $worker05Id = 0,
        $equipment01Id = 0, $equipment02Id = 0, $equipment03Id = 0, $equipment04Id = 0, $equipment05Id = 0)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId, $mobileProfileId, 
            $worker01Id, $worker02Id, $worker03Id, $worker04Id, $worker05Id,
            $equipment01Id, $equipment02Id, $equipment03Id, $equipment04Id, $equipment05Id)
            {
                $hdrModel = WhseJobHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($mobileProfileId == 0)
                {
                    $exc = new ApiException(__('WhseJob.mobile_profile_not_assigned', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\WhseJobHdr::class, $hdrModel->id);
                    throw $exc;
                }

                if($worker01Id == 0)
                {
                    $exc = new ApiException(__('WhseJob.worker_not_assigned', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\WhseJobHdr::class, $hdrModel->id);
                    throw $exc;
                }

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('WhseJob.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\WhseJobHdr::class, $hdrModel->id);
                    throw $exc;
                }

                if($hdrModel->proc_type != ProcType::$MAP['BIN_TRF_02'])
                {
                    $dtlModels = WhseJobDtl::where('hdr_id', $hdrId)
                        ->lockForUpdate()
                        ->orderBy('line_no')
                        ->get();
                    if(count($dtlModels) == 0)
                    {
                        $exc = new ApiException(__('WhseJob.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                        $exc->addData(\App\WhseJobHdr::class, $hdrModel->id);
                        throw $exc;
                    }
                }
                
                //1) update docStatus to WIP
                $hdrModel->doc_status = DocStatus::$MAP['WIP'];
                $hdrModel->mobile_profile_id = $mobileProfileId;
                $hdrModel->worker_01_id = $worker01Id;
                $hdrModel->worker_02_id = $worker02Id;
                $hdrModel->worker_03_id = $worker03Id;
                $hdrModel->worker_04_id = $worker04Id;
                $hdrModel->worker_05_id = $worker05Id;
                $hdrModel->equipment_01_id = $equipment01Id;
                $hdrModel->equipment_02_id = $equipment02Id;
                $hdrModel->equipment_03_id = $equipment03Id;
                $hdrModel->equipment_04_id = $equipment04Id;
                $hdrModel->equipment_05_id = $equipment05Id;
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitToComplete($hdrId, $isForced = true)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId, $isForced)
            {
                $hdrModel = WhseJobHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($isForced == false)
                {
                    if($hdrModel->doc_status == DocStatus::$MAP['WIP']
                    && $hdrModel->mobile_profile_id > 0)
                    {
                        $exc = new ApiException(__('WhseJob.already_being_processed', ['docCode'=>$hdrModel->doc_code, 'mobileProfileId'=>$hdrModel->mobile_profile_id]));
                        $exc->addData(\App\WhseJobHdr::class, $hdrModel->id);
                        throw $exc;
                    }
                }
                if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('WhseJob.doc_status_is_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\WhseJobHdr::class, $hdrModel->id);
                    throw $exc;
                }
                if($hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('WhseJob.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\WhseJobHdr::class, $hdrModel->id);
                    throw $exc;
                }

                //1) update docStatus to COMPLETE
                $hdrModel->doc_status = DocStatus::$MAP['COMPLETE'];
                $hdrModel->save();

                //2) update the associated document details, according to whseJobDtls
                $updatedDocDtlIdHash = array();
                $dtlModels = WhseJobDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('WhseJob.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\WhseJobHdr::class, $hdrModel->id);
                    throw $exc;
                }
                $dtlModels->load('docDtl', 'docHdr');
                foreach($dtlModels as $dtlModel)
                {
                    $frDocHdr = $dtlModel->docHdr;
                    if($dtlModel->doc_dtl_id > 0)
                    {
                        $frDocDtl = $dtlModel->docDtl;
                        if(empty($frDocDtl))
                        {
                            //missing fr doc details
                            $exc = new ApiException(__('WhseJob.fr_doc_hdr_not_found', ['docCode'=>$frDocHdr->doc_code]));
                            $exc->addData($dtlModel->doc_hdr_type, $frDocHdr->doc_code);
                            throw $exc;
                        }

                        if(array_key_exists($dtlModel->doc_dtl_id, $updatedDocDtlIdHash))
                        {
                            //special flow, the whseJob have m dtlModels to 1 docDtl
                            //this case happen when pickListDtl ask to pick at 1 quantBal,
                            //but the quantBal is not enough stock in reality, but near by quantBal have 
                            //so picker enter additional dtlModel to fulfill the pickListDtl
                            $frDocDtl = $frDocDtl->replicate();
                            //this is copy from the last document detail
                            //hdr_id, company_id must follow the last document detail
                            //replicate will copy the company_id
                            if(strcmp($dtlModel->doc_hdr_type, \App\CycleCountHdr::class) == 0)
                            {
                                //item_batch_id must be reset
                                $frDocDtl->item_batch_id = 0;
                            }
                            $frDocDtl->desc_01 = '';
                            $frDocDtl->desc_02 = '';
                        }

                        $updatedDocDtlIdHash[$dtlModel->doc_dtl_id] = $dtlModel->doc_dtl_id;
                    }
                    else
                    {
                        //this is for mobile cycle count/mobile bin transfer
                        //the whse_job_dtl do not have associated details
                        if(strcmp($dtlModel->doc_hdr_type, \App\PickListHdr::class) == 0)
                        {
                            $frDocDtl = new \App\PickListDtl;
                        }
                        elseif(strcmp($dtlModel->doc_hdr_type, \App\PackListHdr::class) == 0)
                        {
                            $frDocDtl = new \App\PackListDtl;
                        }
                        elseif(strcmp($dtlModel->doc_hdr_type, \App\LoadListHdr::class) == 0)
                        {
                            $frDocDtl = new \App\LoadListDtl;
                        }
                        elseif(strcmp($dtlModel->doc_hdr_type, \App\GdsRcptHdr::class) == 0)
                        {
                            $frDocDtl = new \App\GdsRcptDtl;
                        }
                        elseif(strcmp($dtlModel->doc_hdr_type, \App\PutAwayHdr::class) == 0)
                        {
                            $frDocDtl = new \App\PutAwayDtl;
                        }
                        elseif(strcmp($dtlModel->doc_hdr_type, \App\CycleCountHdr::class) == 0)
                        {
                            $frDocDtl = new \App\CycleCountDtl;
                        }
                        elseif(strcmp($dtlModel->doc_hdr_type, \App\BinTrfHdr::class) == 0)
                        {
                            $frDocDtl = new \App\BinTrfDtl;
                        }
                        $frDocDtl->hdr_id = $frDocHdr->id;
                        $frDocDtl->desc_01 = '';
                        $frDocDtl->desc_02 = ''; 
                        $frDocDtl->whse_job_type = $dtlModel->whse_job_type;
                        //this is from web/mobile, that must specify the company_id
                        $frDocDtl->company_id = $dtlModel->company_id;
                    }

                    if(!empty($dtlModel->desc_01))
                    {
                        $frDocDtl->desc_01 = $dtlModel->desc_01;
                    }
                    if(!empty($dtlModel->desc_02))
                    {
                        $frDocDtl->desc_02 = $dtlModel->desc_02;
                    }
                    
                    $frDocDtl->desc_01 = $dtlModel->desc_01;
                    $frDocDtl->desc_02 = $dtlModel->desc_02;
                    $frDocDtl->item_id = $dtlModel->item_id;
                    $frDocDtl->uom_id = $dtlModel->uom_id;
                    $frDocDtl->uom_rate = $dtlModel->uom_rate;
                    $frDocDtl->qty = $dtlModel->qty;
                    if(strcmp($dtlModel->doc_hdr_type, \App\PickListHdr::class) == 0)
                    {
                        $frDocDtl->storage_bin_id = $dtlModel->storage_bin_id;
                        $frDocDtl->quant_bal_id = $dtlModel->quant_bal_id;
                        $frDocDtl->to_storage_bin_id = $dtlModel->to_storage_bin_id;
                        $frDocDtl->to_handling_unit_id = $dtlModel->to_handling_unit_id;   
                        /*
                        if($dtlModel->whse_job_type == WhseJobType::$MAP['PICK_FACE_REPLENISHMENT'])
                        {
                            //this pick list dtl is pick face replenishment, 
                            //so update the required dtl from storage_bin_id to to_storage_bin_id
                            $reqDtlModels = WhseJobDtl::where('quant_bal_id', $dtlModel->quant_bal_id)
                                ->where('req_whse_job_hdr_id', $dtlModel->hdr_id)
                                ->lockForUpdate()
                                ->get();
                            foreach($reqDtlModels as $reqDtlModel)
                            {
                                $reqDtlModel->storage_bin_id = $dtlModel->to_storage_bin_id;
                                $reqDtlModel->save();
                            }
                        }
                        */                      
                    }
                    if(strcmp($dtlModel->doc_hdr_type, \App\PutAwayHdr::class) == 0)
                    {
                        $frDocDtl->quant_bal_id = $dtlModel->quant_bal_id;
                        $frDocDtl->to_storage_bin_id = $dtlModel->to_storage_bin_id;
                        $frDocDtl->to_handling_unit_id = $dtlModel->to_handling_unit_id;
                    }
                    if(strcmp($dtlModel->doc_hdr_type, \App\GdsRcptHdr::class) == 0)
                    {
                        $frDocDtl->batch_serial_no = $dtlModel->batch_serial_no;
                        $frDocDtl->expiry_date = $dtlModel->expiry_date;
                        $frDocDtl->receipt_date = $dtlModel->receipt_date;
                        $frDocDtl->item_cond_01_id = $dtlModel->item_cond_01_id;
                        $frDocDtl->item_cond_02_id = $dtlModel->item_cond_02_id;
                        $frDocDtl->item_cond_03_id = $dtlModel->item_cond_03_id;
                        $frDocDtl->item_cond_04_id = $dtlModel->item_cond_04_id;
                        $frDocDtl->item_cond_05_id = $dtlModel->item_cond_05_id;
                        $frDocDtl->to_storage_bin_id = $dtlModel->to_storage_bin_id;
                        $frDocDtl->to_handling_unit_id = $dtlModel->to_handling_unit_id;
                    }  
                    if(strcmp($dtlModel->doc_hdr_type, \App\CycleCountHdr::class) == 0)
                    {
                        $frDocDtl->storage_bin_id = $dtlModel->storage_bin_id;
                        $frDocDtl->handling_unit_id = $dtlModel->handling_unit_id;
                        $frDocDtl->batch_serial_no = $dtlModel->batch_serial_no;
                        $frDocDtl->expiry_date = $dtlModel->expiry_date;
                        $frDocDtl->receipt_date = $dtlModel->receipt_date;

                        //will fill in the item_batch_id when whseJob is complete
                        //last time done when the cycleCount is complete
                        $item = $dtlModel->item;
                        if(!empty($item))
                        {
                            $itemBatch = ItemBatchRepository::firstOrCreate(
                                $item, 
                                $dtlModel->batch_serial_no, 
                                $dtlModel->expiry_date, 
                                $dtlModel->receipt_date
                            );
                            $frDocDtl->item_batch_id = $itemBatch->id;
                        }
                        //force the updated_at
                        $frDocDtl->desc_02 = $frDocDtl->desc_02.' ';
                    }
                    if(strcmp($dtlModel->doc_hdr_type, \App\BinTrfHdr::class) == 0)
                    {
                        $frDocDtl->handling_unit_id = $dtlModel->handling_unit_id;
                        $frDocDtl->quant_bal_id = $dtlModel->quant_bal_id;
                        $frDocDtl->storage_bin_id = $dtlModel->storage_bin_id;
                        $frDocDtl->batch_serial_no = $dtlModel->batch_serial_no;
                        $frDocDtl->expiry_date = $dtlModel->expiry_date;
                        $frDocDtl->receipt_date = $dtlModel->receipt_date;                        
                        $frDocDtl->to_storage_bin_id = $dtlModel->to_storage_bin_id;
                        $frDocDtl->to_handling_unit_id = $dtlModel->to_handling_unit_id;
                    }
                    $frDocDtl->save();

                    if($dtlModel->doc_dtl_id > 0)
                    {

                    }
                    else
                    {
                        //if this is to create new frDocDtl
                        $dtlModel->doc_dtl_type = get_class($frDocDtl);
                        $dtlModel->doc_dtl_id = $frDocDtl->id;
                        $dtlModel->save();
                    }
                }

                //3) update the associated gdsRcpt details, according to whseJobItems
                $whseJobItems = WhseJobItem::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->get();
                foreach($whseJobItems as $whseJobItem)
                {
                    $frDocItem = $whseJobItem->docItem;
                    if(empty($frDocItem))
                    {
                        $exc = new ApiException(__('WhseJob.fr_doc_item_not_found', ['itemCode'=>$frDocItem->code]));
                        $exc->addData($whseJobItem->doc_dtl_type, $frDocItem->code);
                        throw $exc;
                    }

                    $frDocItem->unit_barcode = $whseJobItem->unit_barcode;
                    $frDocItem->case_barcode = $whseJobItem->case_barcode;
                    $frDocItem->case_uom_rate = $whseJobItem->case_uom_rate;
                    $frDocItem->save();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );

        return $hdrModel;
    }

    static public function commitFrDocToComplete($frDocHdrType, $frDocHdrId)
    {
        if(strcmp($frDocHdrType, \App\PickListHdr::class) == 0)
        {
            //WHSE_JOB_03_01
            $pickListHdr = PickListHdrRepository::commitToComplete($frDocHdrId, true);
            return $pickListHdr;
        }
        elseif(strcmp($frDocHdrType, \App\PackListHdr::class) == 0)
        {
            //WHSE_JOB_05_01
            $packListHdr = PackListHdrRepository::commitToComplete($frDocHdrId, true);
            return $packListHdr;
        }
        elseif(strcmp($frDocHdrType, \App\LoadListHdr::class) == 0)
        {
            //WHSE_JOB_06_01
            $loadListHdr = LoadListHdrRepository::commitToComplete($frDocHdrId, true);
            return $loadListHdr;
        }
        elseif(strcmp($frDocHdrType, \App\GdsRcptHdr::class) == 0)
        {
            //WHSE_JOB_14_01
            $gdsRcptHdr = GdsRcptHdrRepository::commitToComplete($frDocHdrId, true);
            return $gdsRcptHdr;
        }
        elseif(strcmp($frDocHdrType, \App\PutAwayHdr::class) == 0)
        {
            //WHSE_JOB_15_01
            $putAwayHdr = PutAwayHdrRepository::commitToComplete($frDocHdrId, true);
            return $putAwayHdr;
        }
        elseif(strcmp($frDocHdrType, \App\CycleCountHdr::class) == 0)
        {
            //WHSE_JOB_16_01
            $cycleCountHdr = CycleCountHdrRepository::commitToComplete($frDocHdrId, true);
            return $cycleCountHdr;
        }
        elseif(strcmp($frDocHdrType, \App\BinTrfHdr::class) == 0)
        {
            $binTrfHdr = BinTrfHdrRepository::commitToComplete($frDocHdrId, true);
            return $binTrfHdr;
        }
    }

    static public function isAllWhseJobsComplete($frDocHdrType, $frDocHdrId)
    {
        $result = DB::transaction
        (
            function() use ($frDocHdrType, $frDocHdrId)
            {
                $isComplete = true;
                $whseJobDtls = WhseJobDtl::selectRaw('whse_job_dtls.*')
                    ->selectRaw('whse_job_hdrs.doc_status AS doc_status')
                    ->leftJoin('whse_job_hdrs', 'whse_job_hdrs.id', '=', 'whse_job_dtls.hdr_id')
                    ->where('doc_hdr_type', $frDocHdrType)                    
                    ->where('doc_hdr_id', $frDocHdrId)
                    ->where('whse_job_hdrs.doc_status', '>=', DocStatus::$MAP['DRAFT'])
                    ->get();
                foreach($whseJobDtls as $whseJobDtl)
                {
                    if($whseJobDtl->doc_status != DocStatus::$MAP['COMPLETE'])
                    {
                        $isComplete = false;
                    }
                }
                return $isComplete;
            }
        );

        return $result;
    }

    static public function revertCompleteToDraft($hdrId, $frDocHdrHash)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId, $frDocHdrHash)
            {
                $hdrModel = WhseJobHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                {
                    $exc = new ApiException(__('WhseJob.doc_status_is_not_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\WhseJobHdr::class, $hdrModel->id);
                    throw $exc;
                }

                //1) check if the associated document is not COMPLETE
                //if is COMPLETE, then can not set this whseJob to DRAFT from COMPLETE
                foreach($frDocHdrHash as $key => $value)
                {
                    $keyParts = explode('/',$key);
                    $frDocHdrType = $keyParts[0];
                    $frDocHdrId = $keyParts[1];
                    if(strcmp($frDocHdrType, \App\PickListHdr::class) == 0)
                    {
                        //WHSE_JOB_03_01
                        $pickListHdr = PickListHdr::where('id', $frDocHdrId)
                            ->first();
                        if($pickListHdr->doc_status == DocStatus::$MAP['COMPLETE'])
                        {
                            $exc = new ApiException(__('PickList.doc_status_is_complete', ['docCode'=>$pickListHdr->doc_code]));
                            $exc->addData(\App\PickListHdr::class, $pickListHdr->id);
                            throw $exc;
                        }
                    }
                    elseif(strcmp($frDocHdrType, \App\PackListHdr::class) == 0)
                    {
                        //WHSE_JOB_05_01
                        $packListHdr = PackListHdr::where('id', $frDocHdrId)
                            ->first();
                        if($packListHdr->doc_status == DocStatus::$MAP['COMPLETE'])
                        {
                            $exc = new ApiException(__('PackList.doc_status_is_complete', ['docCode'=>$packListHdr->doc_code]));
                            $exc->addData(\App\PackListHdr::class, $packListHdr->id);
                            throw $exc;
                        }
                    }
                    elseif(strcmp($frDocHdrType, \App\LoadListHdr::class) == 0)
                    {
                        //WHSE_JOB_06_01
                        $loadListHdr = LoadListHdr::where('id', $frDocHdrId)
                            ->first();
                        if($loadListHdr->doc_status == DocStatus::$MAP['COMPLETE'])
                        {
                            $exc = new ApiException(__('LoadList.doc_status_is_complete', ['docCode'=>$loadListHdr->doc_code]));
                            $exc->addData(\App\LoadListHdr::class, $loadListHdr->id);
                            throw $exc;
                        }
                    }
                    elseif(strcmp($frDocHdrType, \App\GdsRcptHdr::class) == 0)
                    {
                        //WHSE_JOB_14_01
                        $gdsRcptHdr = GdsRcptHdr::where('id', $frDocHdrId)
                            ->first();
                        if($gdsRcptHdr->doc_status == DocStatus::$MAP['COMPLETE'])
                        {
                            $exc = new ApiException(__('GdsRcptHdr.doc_status_is_complete', ['docCode'=>$gdsRcptHdr->doc_code]));
                            $exc->addData(\App\GdsRcptHdr::class, $gdsRcptHdr->id);
                            throw $exc;
                        }
                    }
                    elseif(strcmp($frDocHdrType, \App\PutAwayHdr::class) == 0)
                    {
                        //WHSE_JOB_15_01
                        $putAwayHdr = PutAwayHdr::where('id', $frDocHdrId)
                            ->first();
                        if($putAwayHdr->doc_status == DocStatus::$MAP['COMPLETE'])
                        {
                            $exc = new ApiException(__('PutAway.doc_status_is_complete', ['docCode'=>$putAwayHdr->doc_code]));
                            $exc->addData(\App\PutAwayHdr::class, $putAwayHdr->id);
                            throw $exc;
                        }
                    }
                    elseif(strcmp($frDocHdrType, \App\CycleCountHdr::class) == 0)
                    {
                        $cycleCountHdr = CycleCountHdr::where('id', $frDocHdrId)
                            ->first();
                        if($cycleCountHdr->doc_status == DocStatus::$MAP['COMPLETE'])
                        {
                            $exc = new ApiException(__('CycleCount.doc_status_is_complete', ['docCode'=>$cycleCountHdr->doc_code]));
                            $exc->addData(\App\CycleCountHdr::class, $cycleCountHdr->id);
                            throw $exc;
                        }
                    }
                }
                
                //2) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->mobile_profile_id = 0;
                $hdrModel->worker_01_id = 0;
                $hdrModel->worker_02_id = 0;
                $hdrModel->worker_03_id = 0;
                $hdrModel->worker_04_id = 0;
                $hdrModel->worker_05_id = 0;
                $hdrModel->equipment_01_id = 0;
                $hdrModel->equipment_02_id = 0;
                $hdrModel->equipment_03_id = 0;
                $hdrModel->equipment_04_id = 0;
                $hdrModel->equipment_05_id = 0;
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertWipToDraft($hdrId, $isForced = true)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId, $isForced)
            {
                $hdrModel = WhseJobHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['WIP'])
                {
                    $exc = new ApiException(__('WhseJob.doc_status_is_not_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\WhseJobHdr::class, $hdrModel->id);
                    throw $exc;
                }

                if($isForced == false)
                {
                    if($hdrModel->mobile_profile_id > 0)
                    {
                        $exc = new ApiException(__('WhseJob.already_being_processed', ['docCode'=>$hdrModel->doc_code, 'mobileProfileId'=>$hdrModel->mobile_profile_id]));
                        $exc->addData(\App\WhseJobHdr::class, $hdrModel->id);
                        throw $exc;
                    }
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->mobile_profile_id = 0;
                $hdrModel->worker_01_id = 0;
                $hdrModel->worker_02_id = 0;
                $hdrModel->worker_03_id = 0;
                $hdrModel->worker_04_id = 0;
                $hdrModel->worker_05_id = 0;
                $hdrModel->equipment_01_id = 0;
                $hdrModel->equipment_02_id = 0;
                $hdrModel->equipment_03_id = 0;
                $hdrModel->equipment_04_id = 0;
                $hdrModel->equipment_05_id = 0;
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function querySiteFlowLatestTimestamp($siteFlowId) 
	{
        $timestamp = Carbon::create(1970, 1, 1, 0, 0, 0, 'UTC');
        $whseJobHdr = WhseJobHdr::where('site_flow_id', $siteFlowId)
            ->orderBy('created_at', 'DESC')
            ->first();
        if(!empty($whseJobHdr))
        {
            $timestamp = $whseJobHdr->created_at;
        }
        return $timestamp;
    }

    static public function queryRecentPickFaceJob($quantBalId) 
	{
        $whseJobHdr = WhseJobHdr::select('whse_job_hdrs.*')
            ->whereExists(function ($query) use ($quantBalId) {
                $query->select(DB::raw(1))
                    ->from('whse_job_dtls')
                    ->where('hdr_id', 'whse_job_hdrs.id')
                    ->where('quant_bal_id', $quantBalId)
                    ->where('whse_job_type', WhseJobType::$MAP['PICK_FACE_REPLENISHMENT']);
            })
            ->where(function ($query) {
                $query->whereDate('doc_date', '=', date('Y-m-d'))
                    ->orWhere('doc_status', DocStatus::$MAP['DRAFT'])
                    ->orWhere('doc_status', DocStatus::$MAP['WIP']);
            })
            ->first();
        return $whseJobHdr;
    }

    static public function updateDetails($whseJobHdrData, $whseJobDtlArray, $delWhseJobDtlArray, $newItemHash) 
	{     
        $delWhseJobDtlIds = array();
        foreach($delWhseJobDtlArray as $delWhseJobDtlData)
        {
            $delWhseJobDtlIds[] = $delWhseJobDtlData['id'];
        }

        $result = DB::transaction
        (
            function() use ($whseJobHdrData, $whseJobDtlArray, $delWhseJobDtlIds, $newItemHash)
            {
                //update WhseJobHdr
                $whseJobHdrModel = WhseJobHdr::lockForUpdate()->find($whseJobHdrData['id']);
                if($whseJobHdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('WhseJob.doc_status_is_complete', ['docCode'=>$whseJobHdrModel->doc_code]));
                    $exc->addData(\App\WhseJobHdr::class, $whseJobHdrModel->id);
                    throw $exc;
                }
                $whseJobHdrModel = RepositoryUtils::dataToModel($whseJobHdrModel, $whseJobHdrData);
                $whseJobHdrModel->save();

                //get the parentDocHdr if is mobile bin transfer/mobile cycle count
                $parentDocHdrId = 0;
                $parentDocHdrType = '';
                if($whseJobHdrModel->proc_type == ProcType::$MAP['WHSE_JOB_17_02'])
                {
                    $docTxnFlows = DocTxnFlowRepository::findAllByFrHdrTypeAndToHdrId(\App\BinTrfHdr::class, get_class($whseJobHdrModel), $whseJobHdrModel->id);
                    foreach($docTxnFlows as $docTxnFlow)
                    {
                        $parentDocHdrId = $docTxnFlow->fr_doc_hdr_id;
                        $parentDocHdrType = $docTxnFlow->fr_doc_hdr_type;
                    }
                }
                elseif($whseJobHdrModel->proc_type == ProcType::$MAP['WHSE_JOB_03_01'])
                {
                    //pick list whse job
                    $docTxnFlows = DocTxnFlowRepository::findAllByFrHdrTypeAndToHdrId(\App\PickListHdr::class, get_class($whseJobHdrModel), $whseJobHdrModel->id);
                    foreach($docTxnFlows as $docTxnFlow)
                    {
                        $parentDocHdrId = $docTxnFlow->fr_doc_hdr_id;
                        $parentDocHdrType = $docTxnFlow->fr_doc_hdr_type;
                    }
                }
                elseif($whseJobHdrModel->proc_type == ProcType::$MAP['WHSE_JOB_05_01'])
                {
                    //pick list whse job
                    $docTxnFlows = DocTxnFlowRepository::findAllByFrHdrTypeAndToHdrId(\App\PackListHdr::class, get_class($whseJobHdrModel), $whseJobHdrModel->id);
                    foreach($docTxnFlows as $docTxnFlow)
                    {
                        $parentDocHdrId = $docTxnFlow->fr_doc_hdr_id;
                        $parentDocHdrType = $docTxnFlow->fr_doc_hdr_type;
                    }
                }
                elseif($whseJobHdrModel->proc_type == ProcType::$MAP['WHSE_JOB_06_01'])
                {
                    //pick list whse job
                    $docTxnFlows = DocTxnFlowRepository::findAllByFrHdrTypeAndToHdrId(\App\LoadListHdr::class, get_class($whseJobHdrModel), $whseJobHdrModel->id);
                    foreach($docTxnFlows as $docTxnFlow)
                    {
                        $parentDocHdrId = $docTxnFlow->fr_doc_hdr_id;
                        $parentDocHdrType = $docTxnFlow->fr_doc_hdr_type;
                    }
                }
                elseif($whseJobHdrModel->proc_type == ProcType::$MAP['WHSE_JOB_14_01'])
                {
                    //gds rcpt whse job
                    $docTxnFlows = DocTxnFlowRepository::findAllByFrHdrTypeAndToHdrId(\App\GdsRcptHdr::class, get_class($whseJobHdrModel), $whseJobHdrModel->id);
                    foreach($docTxnFlows as $docTxnFlow)
                    {
                        $parentDocHdrId = $docTxnFlow->fr_doc_hdr_id;
                        $parentDocHdrType = $docTxnFlow->fr_doc_hdr_type;
                    }
                }
                elseif($whseJobHdrModel->proc_type == ProcType::$MAP['WHSE_JOB_15_01'])
                {
                    //put away whse job
                    $docTxnFlows = DocTxnFlowRepository::findAllByFrHdrTypeAndToHdrId(\App\PutAwayHdr::class, get_class($whseJobHdrModel), $whseJobHdrModel->id);
                    foreach($docTxnFlows as $docTxnFlow)
                    {
                        $parentDocHdrId = $docTxnFlow->fr_doc_hdr_id;
                        $parentDocHdrType = $docTxnFlow->fr_doc_hdr_type;
                    }
                }
                elseif($whseJobHdrModel->proc_type == ProcType::$MAP['WHSE_JOB_16_01'])
                {
                    //cycle count 01
                    $docTxnFlows = DocTxnFlowRepository::findAllByFrHdrTypeAndToHdrId(\App\CycleCountHdr::class, get_class($whseJobHdrModel), $whseJobHdrModel->id);
                    foreach($docTxnFlows as $docTxnFlow)
                    {
                        $parentDocHdrId = $docTxnFlow->fr_doc_hdr_id;
                        $parentDocHdrType = $docTxnFlow->fr_doc_hdr_type;
                    }
                }

                //update WhseJobDtl array
                $whseJobDtlModels = array();
                foreach($whseJobDtlArray as $whseJobDtlData)
                {
                    //process the photos here, temporarily assigned to a variable
                    $dtlPhotos = array();
                    if(isset($whseJobDtlData['photos']))
                    {
                        $dtlPhotos = $whseJobDtlData['photos'];
                        //need to unset, so it will not assigned to model
                        unset($whseJobDtlData['photos']);
                    }

                    $whseJobDtlModel = null;
                    if (strpos($whseJobDtlData['id'], 'NEW') !== false
                    || empty($whseJobDtlData['id'])) 
                    {
                        if($parentDocHdrId > 0)
                        {
                            //mobile bin transfer
                            $uuid = $whseJobDtlData['id'];
                            $whseJobDtlModel = new WhseJobDtl;
                            $whseJobDtlModel = RepositoryUtils::dataToModel($whseJobDtlModel, $whseJobDtlData);
                            $whseJobDtlModel->doc_hdr_type = $parentDocHdrType;
                            $whseJobDtlModel->doc_hdr_id = $parentDocHdrId;
                        }               
                    }
                    else
                    {
                        $whseJobDtlModel = WhseJobDtl::lockForUpdate()->find($whseJobDtlData['id']);
                        $whseJobDtlModel = RepositoryUtils::dataToModel($whseJobDtlModel, $whseJobDtlData);
                    }

                    //verify the bin transfer job type
                    if($whseJobDtlModel->whse_job_type == WhseJobType::$MAP['BIN_TRF_PALLET_TO_BIN'])
                    {
                        //full pallet, make sure qty, uom_id, and uom_rate is correct
                        $whseJobDtlModel->qty = 1;
                        $whseJobDtlModel->uom_id = Uom::$PALLET;
                        $whseJobDtlModel->uom_rate = 0;
                        //make sure to_handling_unit_id is same with handling_unit_id
                        $whseJobDtlModel->to_handling_unit_id = $whseJobDtlModel->handling_unit_id;
                    }
                    elseif($whseJobDtlModel->whse_job_type == WhseJobType::$MAP['BIN_TRF_PALLET_TO_LOOSE'])
                    {
                        //full pallet, make sure qty, uom_id, and uom_rate is correct
                        $whseJobDtlModel->qty = 1;
                        $whseJobDtlModel->uom_id = Uom::$PALLET;
                        $whseJobDtlModel->uom_rate = 0;
                        //make sure to_handling_unit_id is 0
                        $whseJobDtlModel->to_handling_unit_id = 0;
                    }

                    if($whseJobDtlModel->uom_rate == 0
                    && $whseJobDtlModel->uom_id > Uom::$PALLET)
                    {
                        //update the uom_rate, if mobile not fill in
                        $itemUom = ItemUomRepository::findByItemIdAndUomId($whseJobDtlModel->item_id, $whseJobDtlModel->uom_id);
                        if(empty($itemUom))
                        {
                            $exc = new ApiException(__('WhseJob.item_uom_not_found', ['dtlId'=>$whseJobDtlModel->id, 'itemId'=>$whseJobDtlModel->item_id, 'uomId'=>$whseJobDtlModel->uom_id]));
                            $exc->addData(\App\WhseJobHdr::class, $whseJobHdrModel->id);
                            throw $exc;
                        }
                        else
                        {
                            $whseJobDtlModel->uom_rate = $itemUom->uom_rate;
                        }
                    }

                    if(strcmp($whseJobDtlModel->doc_hdr_type, \App\CycleCountHdr::class) == 0)
                    {
                        if($whseJobDtlModel->handling_unit_id >= 1
                        && $whseJobDtlModel->handling_unit_id <= 100
                        && $whseJobDtlModel->item_id > 0)
                        {
                            //itemId > 0, make sure this cycleCountDtl is not accidentally add Pallet
                            //if this is cycleCount, and handlingUnitId is 1, then assign a new handlingUnit to it
                            $handlingUnit = HandlingUnitRepository::firstOrCreate(
                                $whseJobDtlModel->handling_unit_id, 
                                $whseJobHdrModel->site_flow_id,
                                HandlingType::$MAP['PALLET']
                            );
                            $whseJobDtlModel->handling_unit_id = $handlingUnit->id;
                        }
                    }

                    if(strcmp($whseJobDtlModel->doc_hdr_type, \App\GdsRcptHdr::class) == 0
                    && array_key_exists($whseJobDtlModel->item_id, $newItemHash))
			        {
                        //this detail line is new item, and this is GdsRcpt
                        //need to update the detail line uom_rate, accordingly
                        $newItemData = $newItemHash[$whseJobDtlModel->item_id];

                        if($whseJobDtlModel->uom_id == Uom::$UNIT)
                        {
                            $whseJobDtlModel->uom_rate = 1;
                        }
                        elseif($whseJobDtlModel->uom_id == Uom::$CASE)
                        {
                            $whseJobDtlModel->uom_rate = $newItemData['case_uom_rate'];
                        }
                    }

                    $whseJobDtlModel->hdr_id = $whseJobHdrModel->id;
                    //verify the desc_01/desc_02, if empty, then assign item desc_01/desc_02
                    $itemDesc01 = '';
                    $itemDesc02 = '';
                    if(empty($whseJobDtlModel->desc_01)
                    || empty($whseJobDtlModel->desc_02))
                    {
                        $dtlItem = $whseJobDtlModel->item;
                        if(!empty($dtlItem))
                        {
                            $itemDesc01 = $dtlItem->desc_01;
                            $itemDesc02 = $dtlItem->desc_02;
                        }
                    }
                    if(empty($whseJobDtlModel->desc_01))
                    {
                        $whseJobDtlModel->desc_01 = $itemDesc01;
                    }
                    if(empty($whseJobDtlModel->desc_02))
                    {
                        $whseJobDtlModel->desc_02 = $itemDesc02;
                    }
                    $whseJobDtlModel->save();

                    //process the photos here
                    if(!empty($dtlPhotos))
                    {
                        foreach($dtlPhotos as $photoData)
                        {
                            $docPhotoModel = DocPhotoRepository::createWhseJobPhoto($whseJobDtlModel, $photoData);
                        }                        
                    }

                    $whseJobDtlModels[] = $whseJobDtlModel;
                }

                //delete
                if(!empty($delWhseJobDtlIds))
                {
                    WhseJobDtl::destroy($delWhseJobDtlIds);
                }

                $whseJobItemModels = array();
                foreach($newItemHash as $itemId => $newItemData)
                {
                    $whseJobItemModel = null;
                    if ($newItemData['id'] > 0) 
                    {
                        $whseJobItemModel = WhseJobItem::lockForUpdate()->find($newItemData['id']);
                    }
                    else
                    {
                        $whseJobItemModel = new WhseJobItem;
                    }
                    
                    $whseJobItemModel = RepositoryUtils::dataToModel($whseJobItemModel, $newItemData);
                    $whseJobItemModel->save();
                    $whseJobItemModels[] = $whseJobItemModel;
                }

                return array(
                    'hdrModel' => $whseJobHdrModel,
                    'dtlModels' => $whseJobDtlModels,
                    'itemModels' => $whseJobItemModels,
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function findAllDraftAndWIP($siteFlowId, $procType, $sorts, $filters = array(), $pageSize = 20) 
	{
        $qStatuses = array(DocStatus::$MAP['DRAFT'], DocStatus::$MAP['WIP']); 
        $whseJobHdrs = WhseJobHdr::where('site_flow_id', $siteFlowId)
            ->where('proc_type', $procType)
            ->whereIn('doc_status', $qStatuses);

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $whseJobHdrs->orderBy('doc_code', $sort['order']);
            }
        }
        return $whseJobHdrs
            ->paginate($pageSize);
    }

    static public function findAllByIds($hdrIds) 
	{
        $whseJobHdrs = WhseJobHdr::whereIn('id', $hdrIds)
            ->get();

        return $whseJobHdrs;
    }

    static public function revertDraftToVoid($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = WhseJobHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('WhseJob.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\WhseJobHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to VOID
                $hdrModel->doc_status = DocStatus::$MAP['VOID'];
                $hdrModel->save();

                $frDocTxnFlows = $hdrModel->frDocTxnFlows;
                foreach($frDocTxnFlows as $frDocTxnFlow)
                {
                    //move to voidTxn
                    $txnVoidModel = new DocTxnVoid;
                    $txnVoidModel->id = $frDocTxnFlow->id;
                    $txnVoidModel->proc_type = $frDocTxnFlow->proc_type;
                    $txnVoidModel->fr_doc_hdr_type = $frDocTxnFlow->fr_doc_hdr_type;
                    $txnVoidModel->fr_doc_hdr_id = $frDocTxnFlow->fr_doc_hdr_id;
                    $txnVoidModel->fr_doc_hdr_code = $frDocTxnFlow->fr_doc_hdr_code;
                    $txnVoidModel->to_doc_hdr_type = $frDocTxnFlow->to_doc_hdr_type;
                    $txnVoidModel->to_doc_hdr_id = $frDocTxnFlow->to_doc_hdr_id;
                    $txnVoidModel->is_closed = $frDocTxnFlow->is_closed;
                    $txnVoidModel->save();
        
                    //delete the txn
                    $frDocTxnFlow->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitVoidToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = WhseJobHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['VOID'])
                {
                    //only VOID can transition to DRAFT
                    $exc = new ApiException(__('WhseJob.doc_status_is_not_void', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\WhseJobHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                $frDocTxnVoids = $hdrModel->frDocTxnVoids;
                foreach($frDocTxnVoids as $frDocTxnVoid)
                {
                    //verify first
                    $docTxnFlow = DocTxnFlow::where(array(
                        'proc_type' => $frDocTxnVoid->proc_type,
                        'fr_doc_hdr_type' => $frDocTxnVoid->fr_doc_hdr_type,
                        'fr_doc_hdr_id' => $frDocTxnVoid->fr_doc_hdr_id,
                        'to_doc_hdr_type' => $frDocTxnVoid->to_doc_hdr_type
                        ))
                        ->first();
                    if(!empty($docTxnFlow))
                    {
                        $exc = new ApiException(__('WhseJob.fr_doc_is_closed', ['docCode'=>$frDocTxnVoid->fr_doc_hdr_code]));
                        $exc->addData(\App\WhseJobHdr::class, $hdrModel->id);
                        throw $exc;
                    }

                    //move to txnFlow
                    $txnFlowModel = new DocTxnFlow;
                    $txnFlowModel->id = $frDocTxnVoid->id;
                    $txnFlowModel->proc_type = $frDocTxnVoid->proc_type;
                    $txnFlowModel->fr_doc_hdr_type = $frDocTxnVoid->fr_doc_hdr_type;
                    $txnFlowModel->fr_doc_hdr_id = $frDocTxnVoid->fr_doc_hdr_id;
                    $txnFlowModel->fr_doc_hdr_code = $frDocTxnVoid->fr_doc_hdr_code;
                    $txnFlowModel->to_doc_hdr_type = $frDocTxnVoid->to_doc_hdr_type;
                    $txnFlowModel->to_doc_hdr_id = $frDocTxnVoid->to_doc_hdr_id;
                    $txnFlowModel->is_closed = $frDocTxnVoid->is_closed;
                    $txnFlowModel->save();
        
                    //delete the txnVoid
                    $frDocTxnVoid->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }
}