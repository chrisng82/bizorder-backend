<?php

namespace App\Repositories;

use App\BinTrfDtl;

class BinTrfDtlRepository 
{
    static public function findAllByHdrId($hdrId)
    {
        $binTrfDtls = BinTrfDtl::where('hdr_id', $hdrId)
            ->orderBy('line_no')
            ->get();

        return $binTrfDtls;
    }
}