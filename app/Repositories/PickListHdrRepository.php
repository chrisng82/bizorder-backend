<?php

namespace App\Repositories;

use App\Services\Utils\RepositoryUtils;
use App\Uom;
use App\QuantBal;
use App\PickListHdr;
use App\PickListDtl;
use App\PickListCDtl;
use App\PickListOutbOrd;
use App\OutbOrdHdr;
use App\DocTxnFlow;
use App\DocTxnVoid;
use App\QuantBalRsvdTxn;
use App\QuantBalTxn;
use App\Repositories\DocNoRepository;
use App\Repositories\QuantBalRepository;
use App\Repositories\DocTxnFlowRepository;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Services\Env\WhseJobType;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\DB;

class PickListHdrRepository 
{
    const BREAK_UP_LINE_NO = 35000;

    static public function findAll($siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $pickListHdrs = PickListHdr::select('pick_list_hdrs.*')
            ->where('site_flow_id', $siteFlowId);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $pickListHdrs->where('pick_list_hdrs.doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $pickListHdrs->orderBy('doc_code', $sort['order']);
            }
        }
        
        return $pickListHdrs
            ->paginate($pageSize);
    }

    static public function findAllByHdrIds($hdrIds) 
	{
        $pickListHdrs = PickListHdr::whereIn('id', $hdrIds)
            ->get();

        return $pickListHdrs;
    }

    static public function findByPk($hdrId) 
	{
        $pickListHdr = PickListHdr::where('id', $hdrId)
            ->first();

        return $pickListHdr;
    }

    static public function txnFindByPk($hdrId) 
	{
        $model = DB::transaction
        (
            function() use ($hdrId)
            {
                $pickListHdr = PickListHdr::where('id', $hdrId)
                    ->first();
                return $pickListHdr;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function commitToWip($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = PickListHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('PickList.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\PickListHdr::class, $hdrModel->id);
                    throw $exc;
                }

                $dtlModels = PickListDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->orderBy('line_no')
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('PickList.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\PickListHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to WIP
                $hdrModel->doc_status = DocStatus::$MAP['WIP'];
                $hdrModel->save();
                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitToComplete($hdrId, $isNonZeroBal)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId, $isNonZeroBal)
            {
                $hdrModel = PickListHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();
                    
                if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('PickList.doc_status_is_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\PickListHdr::class, $hdrModel->id);
                    throw $exc;
                }
                if($hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('PickList.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\PickListHdr::class, $hdrModel->id);
                    throw $exc;
                }
        
                //1) update docStatus to COMPLETE
                $hdrModel->doc_status = DocStatus::$MAP['COMPLETE'];
                $hdrModel->save();

                $isPickFaceReplenishment = false;
                //process pick face replenishment first
                $dtlModels = PickListDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->orderBy('line_no')
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('PickList.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\PickListHdr::class, $hdrModel->id);
                    throw $exc;
                }
                $dtlModels->load('item');
                foreach($dtlModels as $dtlModel)
                {
                    $item = $dtlModel->item;

                    if($dtlModel->to_storage_bin_id == 0)
                    {
                        //only DRAFT or above can transition to COMPLETE
                        $exc = new ApiException(__('PickList.to_storage_bin_is_required', ['lineNo'=>$dtlModel->line_no]));
                        $exc->addData(\App\PickListDtl::class, $dtlModel->id);
                        throw $exc;
                    }

                    //3) check this dtlModel is pick face replenishment or transfer stock
                    if($dtlModel->uom_id == Uom::$PALLET
                    && bccomp($dtlModel->uom_rate, 0, 5) == 0)
                    {
                        $isPickFaceReplenishment = true;
                        //this is pickFaceReplenishment
                        $pickQuantBal = QuantBal::where('id', $dtlModel->quant_bal_id)
                            ->first();

                        if($pickQuantBal->handling_unit_id == 0)
                        {
                            /*
                            //only DRAFT or above can transition to COMPLETE
                            $exc = new ApiException(__('PickList.handling_unit_is_required', ['id'=>$dtlModel->id]));
                            $exc->addData(\App\PickListDtl::class, $dtlModel->id);
                            throw $exc;
                            */
                        }
                        
                        QuantBalRepository::commitHandlingUnitTransfer(
                            $pickQuantBal->storage_bin_id, $pickQuantBal->handling_unit_id, 
                            $dtlModel->to_storage_bin_id, 0, 
                            $hdrModel->doc_date, \App\PickListHdr::class, $hdrModel->id, $dtlModel->id, $dtlModels);
                    }
                }
                
                //process normal stock transfer
                if($isPickFaceReplenishment === true) 
                {
                    //refresh the detail again
                    $dtlModels = PickListDtl::where('hdr_id', $hdrId)
                        ->lockForUpdate()
                        ->orderBy('line_no')
                        ->get();
                    $dtlModels->load('item');
                }

                foreach($dtlModels as $dtlModel)
                {
                    $item = $dtlModel->item;

                    if($dtlModel->to_storage_bin_id == 0)
                    {
                        //only DRAFT or above can transition to COMPLETE
                        $exc = new ApiException(__('PickList.to_storage_bin_is_required', ['lineNo'=>$dtlModel->line_no]));
                        $exc->addData(\App\PickListDtl::class, $dtlModel->id);
                        throw $exc;
                    }

                    //3) check this dtlModel is pick face replenishment or transfer stock
                    if($dtlModel->uom_id == Uom::$PALLET
                    && bccomp($dtlModel->uom_rate, 0, 5) == 0)
                    {
                        //these are pickface replenishment, and already processed by above code
                    }
                    else
                    {
                        if($dtlModel->quant_bal_id == 0)
                        {
                            //only DRAFT or above can transition to COMPLETE
                            $exc = new ApiException(__('PickList.quant_bal_is_required', ['lineNo'=>$dtlModel->line_no]));
                            $exc->addData(\App\PickListDtl::class, $dtlModel->id);
                            throw $exc;
                        }
                        
                        //this is stock transfer
                        QuantBalRepository::commitStockTransfer(
                            $dtlModel->storage_bin_id, $isNonZeroBal, $item, 
                            $dtlModel->quant_bal_id, $dtlModel->to_storage_bin_id, $dtlModel->to_handling_unit_id, 
                            $hdrModel->doc_date, \App\PickListHdr::class, $hdrModel->id, 
                            $dtlModel->id, $dtlModel->uom_id, $dtlModel->uom_rate, $dtlModel->qty);
                    }
                } 

                //lastly only delete all the rsvd txn
                $quantBalRsvdTxns = QuantBalRsvdTxn::where(array(
                    'doc_hdr_type' => \App\PickListHdr::class,
                    'doc_hdr_id' => $hdrId
                ))
                ->lockForUpdate()
                ->get();
            
                DB::table('quant_bal_rsvd_txns')
                ->where(array(
                    'doc_hdr_type' => \App\PickListHdr::class,
                    'doc_hdr_id' => $hdrId
                ))
                ->delete();
                return $hdrModel;              
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertCompleteToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = PickListHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                {
                    $exc = new ApiException(__('PickList.doc_status_is_not_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\PickListHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                QuantBalRepository::revertStockTransaction(\App\PickListHdr::class, $hdrModel->id);
                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertWipToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = PickListHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['WIP'])
                {
                    $exc = new ApiException(__('PickList.doc_status_is_not_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\PickListHdr::class, $hdrModel->id);
                    throw $exc;
                }

                //1) verify the toWhseJobs, make sure it is not WIP(download to mobile)
                $docTxnFlows = DocTxnFlow::where(array(
                    'fr_doc_hdr_type' => get_class($hdrModel),
                    'fr_doc_hdr_id' => $hdrModel->id,
                    'to_doc_hdr_type' => \App\WhseJobHdr::class
                    ))
                    ->get();
                foreach($docTxnFlows as $docTxnFlow)
                {
                    $toWhseJobHdr = \App\WhseJobHdr::where('id', $docTxnFlow->to_doc_hdr_id)
                        ->lockForUpdate()
                        ->first();
                    if($toWhseJobHdr->doc_status == DocStatus::$MAP['DRAFT']
                    || $toWhseJobHdr->doc_status == DocStatus::$MAP['WIP'])
                    {
                        $exc = new ApiException(__('WhseJob.job_status_is_draft_or_wip', ['docCode'=>$toWhseJobHdr->doc_code]));
                        $exc->addData(\App\GdsRcptHdr::class, $hdrModel->id);
                        throw $exc;
                    }
                }
                
                //2) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                //revert the pickListDtls back to original
                $pickListDtls = PickListDtl::where('hdr_id', $hdrId)
                ->lockForUpdate()
                ->orderBy('line_no')
                ->get();
                $pickListDtls->load('item');
                //sum back the algo generated pickListDtls qty to parent_dtl_id
                $pickListDtlsHashByParentDtlId = array();
                foreach($pickListDtls as $pickListDtl)
                {
                    if($pickListDtl->whse_job_type == WhseJobType::$MAP['PICK_FACE_REPLENISHMENT'])
                    {
                        continue;
                    }

                    $tmpPickListDtls = array();
                    if(array_key_exists($pickListDtl->parent_dtl_id, $pickListDtlsHashByParentDtlId))
                    {
                        $tmpPickListDtls = $pickListDtlsHashByParentDtlId[$pickListDtl->parent_dtl_id];
                    }
                    $tmpPickListDtls[] = $pickListDtl;

                    $pickListDtlsHashByParentDtlId[$pickListDtl->parent_dtl_id] = $tmpPickListDtls; 
                }
                //if detail parent_dtl_id = 0, then this is parent dtl
                $parentPickListDtls = array();
                if(array_key_exists(0, $pickListDtlsHashByParentDtlId))
                {
                    $parentPickListDtls = $pickListDtlsHashByParentDtlId[0];
                }
                foreach($parentPickListDtls as $parentPickListDtl)
                {
                    if(array_key_exists($parentPickListDtl->id, $pickListDtlsHashByParentDtlId))
                    {
                        $item = $parentPickListDtl->item;
                        $ttlUnitQty = bcmul($parentPickListDtl->uom_rate, $parentPickListDtl->qty, $item->qty_scale);
                        
                        $childPickListDtls = $pickListDtlsHashByParentDtlId[$parentPickListDtl->id];
                        foreach($childPickListDtls as $childPickListDtl)
                        {
                            $childUnitQty = bcmul($childPickListDtl->uom_rate, $childPickListDtl->qty, $item->qty_scale);
                            $ttlUnitQty = bcadd($ttlUnitQty, $childUnitQty, $item->qty_scale);
                            //delete the child pickListDtl
                            $childPickListDtl->delete();
                        }
                        
                        $parentPickListDtl->qty = $ttlUnitQty;
                        $parentPickListDtl->uom_id = $item->unit_uom_id;
                        $parentPickListDtl->uom_rate = 1;
                    }

                    $parentPickListDtl->storage_bin_id = 0;
                    $parentPickListDtl->quant_bal_id = 0;
                    $parentPickListDtl->whse_job_type = WhseJobType::$MAP['NULL'];
                    $parentPickListDtl->whse_job_code = '';
                    $parentPickListDtl->save();
                }

                //delete the PICK_FACE_REPLENISHMENT and algo generated pickListDtls
                foreach($pickListDtls as $pickListDtl)
                {
                    if($pickListDtl->whse_job_type == WhseJobType::$MAP['PICK_FACE_REPLENISHMENT'])
                    {
                        $pickListDtl->delete();
                    }
                }

                //2) delete all the rsvd txn
                $quantBalRsvdTxns = QuantBalRsvdTxn::where(array(
                    'doc_hdr_type' => \App\PickListHdr::class,
                    'doc_hdr_id' => $hdrId
                ))
                ->lockForUpdate()
                ->get();

                DB::table('quant_bal_rsvd_txns')
                ->where(array(
                    'doc_hdr_type' => \App\PickListHdr::class,
                    'doc_hdr_id' => $hdrId
                ))
                ->delete();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function createProcess($procType, $docNoId, $hdrData, $dtlDataList, $outbOrdHdrIds, $docTxnFlowDataList) 
	{
        if(empty($dtlDataList))
        {
            $exc = new ApiException(__('PickList.item_details_not_found', []));
            //$exc->addData(\App\PickListHdr::class, $hdrData->doc_code);
            throw $exc;
        }
        $pickListHdr = DB::transaction
        (
            function() use ($procType, $docNoId, $hdrData, $dtlDataList, $outbOrdHdrIds, $docTxnFlowDataList)
            {
                $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

                $hdrModel = new PickListHdr;
                $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
                $hdrModel->doc_code = $newCode;
                $hdrModel->proc_type = $procType;
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                foreach($dtlDataList as $lineNo => $dtlData)
                {
                    $dtlModel = new PickListDtl;
                    $dtlModel = RepositoryUtils::dataToModel($dtlModel, $dtlData);
                    $dtlModel->hdr_id = $hdrModel->id;
                    $dtlModel->save();

                    //create a carbon copy
                    $cDtlModel = new PickListCDtl;
                    $cDtlModel = RepositoryUtils::dataToModel($cDtlModel, $dtlData);
                    $cDtlModel->hdr_id = $hdrModel->id;
                    $cDtlModel->save();
                }

                foreach($outbOrdHdrIds as $outbOrdHdrId)
                {
                    $pickListOutbOrdModel =  new PickListOutbOrd;
                    $pickListOutbOrdModel->hdr_id = $hdrModel->id;
                    $pickListOutbOrdModel->outb_ord_hdr_id = $outbOrdHdrId;
                    $pickListOutbOrdModel->save();
                }

                //process docTxnFlowDataList
                foreach($docTxnFlowDataList as $docTxnFlowData)
                {
                    $frDocHdrModel = $docTxnFlowData['fr_doc_hdr_type']::where('id', $docTxnFlowData['fr_doc_hdr_id'])
                    ->lockForUpdate()
                    ->first();
                    if($frDocHdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                    {
                        $exc = new ApiException(__('PickList.fr_doc_is_not_complete', ['docType'=>$docTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$docTxnFlowData['fr_doc_hdr_code']]));
                        $exc->addData($docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_code']);
                        throw $exc;
                    }

                    //verify the frDoc is not closed for this procType
                    $tmpDocTxnFlow = DocTxnFlowRepository::findByProcTypeAndFrHdrId($procType, $docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_id'], 1);
                    if(!empty($tmpDocTxnFlow))
                    {
                        $exc = new ApiException(__('PickList.fr_doc_is_closed', ['docType'=>$docTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$docTxnFlowData['fr_doc_hdr_code']]));
                        $exc->addData($docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_code']);
                        throw $exc;
                    }

                    $docTxnFlowModel = new DocTxnFlow;
                    $docTxnFlowModel = RepositoryUtils::dataToModel($docTxnFlowModel, $docTxnFlowData);
                    $docTxnFlowModel->proc_type = $procType;
                    $docTxnFlowModel->to_doc_hdr_type = \App\PickListHdr::class;
                    $docTxnFlowModel->to_doc_hdr_id = $hdrModel->id;
                    $docTxnFlowModel->save();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $pickListHdr;
    }

    static public function findAllNotExistWhseJob0301Txn($divisionIds, $sorts, $filters = array(), $pageSize = 20) 
	{
        $qStatuses = array(DocStatus::$MAP['DRAFT'], DocStatus::$MAP['WIP']); 
        $pickListHdrs = PickListHdr::select('pick_list_hdrs.*')
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('doc_txn_flows')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['WHSE_JOB_03_01'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\PickListHdr::class)
                    ->whereRaw('doc_txn_flows.fr_doc_hdr_id = pick_list_hdrs.id')
                    ->where('doc_txn_flows.is_closed', 1);
            })
            ->whereExists(function ($q1) use($divisionIds) {
                $q1->select(DB::raw(1))
                    ->from('pick_list_outb_ords')
                    ->leftJoin('outb_ord_hdrs', 'outb_ord_hdrs.id', '=', 'pick_list_outb_ords.outb_ord_hdr_id')
                    
                    ->whereRaw('pick_list_outb_ords.hdr_id = pick_list_hdrs.id')
                    ->whereIn('outb_ord_hdrs.division_id', $divisionIds);
            })
            ->whereIn('doc_status', $qStatuses);

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $pickListHdrs->orderBy('doc_code', $sort['order']);
            }
        }
        
        return $pickListHdrs
            ->paginate($pageSize);
    }

    static public function findAllNotExistPackList01Txn($siteFlowId, $docStatus, $reqProcType = 0, $sorts, $filters = array(), $pageSize = 20) 
	{
        $pickListHdrs = PickListHdr::select('pick_list_hdrs.*')
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('doc_txn_flows')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['PACK_LIST_01'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\PickListHdr::class)
                    ->whereRaw('doc_txn_flows.fr_doc_hdr_id = pick_list_hdrs.id')
                    ->where('doc_txn_flows.is_closed', 1);
            })
            ->when($reqProcType > 0, function ($query) use ($reqProcType) {
                return $query->whereExists(function ($query) use ($reqProcType) {
                    $query->select(DB::raw(1))
                        ->from('doc_txn_flows')
                        ->where('doc_txn_flows.proc_type', $reqProcType)
                        ->where('doc_txn_flows.fr_doc_hdr_type', \App\PickListHdr::class)
                        ->whereRaw('doc_txn_flows.fr_doc_hdr_id = pick_list_hdrs.id')
                        ->where('doc_txn_flows.is_closed', 1);
                });
            })
            ->where('site_flow_id', $siteFlowId)
            ->where('doc_status', $docStatus);

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $pickListHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        return $pickListHdrs
            ->paginate($pageSize);
            //->toSql();
    }

    static public function findAllNotExistInvDoc01Txn($divisionIds, $sorts, $filters = array(), $pageSize = 20) 
	{
        $invDoc01TxnFlows = DocTxnFlow::select('doc_txn_flows.fr_doc_hdr_id')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['INV_DOC_01'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\PickListHdr::class)
                    ->distinct();
        $divPickListOutbOrds = PickListOutbOrd::select('pick_list_outb_ords.hdr_id')
                    ->leftJoin('outb_ord_hdrs', 'outb_ord_hdrs.id', '=', 'pick_list_outb_ords.outb_ord_hdr_id')
                    ->whereIn('outb_ord_hdrs.division_id', $divisionIds)
                    ->distinct();

        $leftJoinHash = array();
        $qStatuses = array(DocStatus::$MAP['WIP'], DocStatus::$MAP['COMPLETE']);
        $pickListHdrs = PickListHdr::select('pick_list_hdrs.*')
            ->leftJoinSub($invDoc01TxnFlows, 'inv_doc_01_txn_flows', function ($join) {
                $join->on('pick_list_hdrs.id', '=', 'inv_doc_01_txn_flows.fr_doc_hdr_id');
            })
            ->whereNull('inv_doc_01_txn_flows.fr_doc_hdr_id')
            ->leftJoinSub($divPickListOutbOrds, 'div_pick_list_outb_ords', function ($join) {
                $join->on('pick_list_hdrs.id', '=', 'div_pick_list_outb_ords.hdr_id');
            })
            ->whereNotNull('div_pick_list_outb_ords.hdr_id')
            ->whereIn('doc_status', $qStatuses);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $pickListHdrs->where('doc_code', 'LIKE', '%'.$filter['value'].'%');
            }

            if(strcmp($filter['field'], 'doc_status') == 0)
            {
                if(strpos('COMPLETE',$filter['value']) !== false)
                {
                    //complete
                    $pickListHdrs->where('doc_status', DocStatus::$MAP['COMPLETE']);
                }
                elseif(strpos('WIP',$filter['value']) !== false)
                {
                    //wip
                    $pickListHdrs->where('doc_status', DocStatus::$MAP['WIP']);
                }
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $pickListHdrs->orderBy('doc_code', $sort['order']);
            }
        }
        return $pickListHdrs
            ->paginate($pageSize);
    }

    static public function findAllExistInvDoc01TxnAndNotExistInvDoc0101($divisionIds, $sorts, $filters = array(), $pageSize = 20) 
	{
        $leftJoinHash = array();
        $qStatuses = array(DocStatus::$MAP['COMPLETE']);
        $pickListHdrs = PickListHdr::select('pick_list_hdrs.*')
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('doc_txn_flows')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['INV_DOC_01'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\PickListHdr::class)
                    ->whereRaw('doc_txn_flows.fr_doc_hdr_id = pick_list_hdrs.id')
                    ->whereRaw('doc_txn_flows.to_doc_hdr_id <> 0')
                    ->where('doc_txn_flows.is_closed', 1);
            })
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('doc_txn_flows')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['INV_DOC_01_01'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\PickListHdr::class)
                    ->whereRaw('doc_txn_flows.fr_doc_hdr_id = pick_list_hdrs.id')
                    ->where('doc_txn_flows.is_closed', 1);
            })
            ->whereExists(function ($q1) use($divisionIds) {
                $q1->select(DB::raw(1))
                    ->from('pick_list_outb_ords')
                    ->leftJoin('outb_ord_hdrs', 'outb_ord_hdrs.id', '=', 'pick_list_outb_ords.outb_ord_hdr_id')
                    
                    ->whereRaw('pick_list_outb_ords.hdr_id = pick_list_hdrs.id')
                    ->whereIn('outb_ord_hdrs.division_id', $divisionIds);
            })
            ->whereIn('doc_status', $qStatuses);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'delivery_point') == 0)
            {
                $pickListHdrs->whereExists(function ($q1) use($filter) {
                    $q1->select(DB::raw(1))
                        ->from('pick_list_outb_ords')
                        ->leftJoin('outb_ord_hdrs', 'outb_ord_hdrs.id', '=', 'pick_list_outb_ords.outb_ord_hdr_id')
                        ->leftJoin('delivery_points', 'delivery_points.id', '=', 'outb_ord_hdrs.delivery_point_id')
                        ->whereRaw('pick_list_outb_ords.hdr_id = pick_list_hdrs.id')
                        ->where(function($q2) use($filter) {
                            $q2->where('delivery_points.code', 'LIKE', '%'.$filter['value'].'%')
                            ->orWhere('delivery_points.company_name_01', 'LIKE', '%'.$filter['value'].'%');
                        });
                });
            }
            if(strcmp($filter['field'], 'salesman') == 0)
            {
                $pickListHdrs->whereExists(function ($q1) use($filter) {
                    $q1->select(DB::raw(1))
                        ->from('pick_list_outb_ords')
                        ->leftJoin('outb_ord_hdrs', 'outb_ord_hdrs.id', '=', 'pick_list_outb_ords.outb_ord_hdr_id')
                        ->leftJoin('users', 'users.id', '=', 'outb_ord_hdrs.salesman_id')
                        ->whereRaw('pick_list_outb_ords.hdr_id = pick_list_hdrs.id')
                        ->where(function($q2) use($filter) {
                            $q2->where('users.username', 'LIKE', '%'.$filter['value'].'%');
                        });
                });
            }
            if(strcmp($filter['field'], 'delivery_point_area') == 0)
            {
                $pickListHdrs->whereExists(function ($q1) use($filter) {
                    $q1->select(DB::raw(1))
                        ->from('pick_list_outb_ords')
                        ->leftJoin('outb_ord_hdrs', 'outb_ord_hdrs.id', '=', 'pick_list_outb_ords.outb_ord_hdr_id')
                        ->leftJoin('delivery_points', 'delivery_points.id', '=', 'outb_ord_hdrs.delivery_point_id')
                        ->whereRaw('pick_list_outb_ords.hdr_id = pick_list_hdrs.id')
                        ->where(function($q2) use($filter) {
                            $q2->where('delivery_points.area_code', 'LIKE', '%'.$filter['value'].'%');
                        });
                });
            }
            if(strcmp($filter['field'], 'outb_ord_hdr') == 0)
            {
                $pickListHdrs->whereExists(function ($q1) use($filter) {
                    $q1->select(DB::raw(1))
                        ->from('pick_list_outb_ords')
                        ->leftJoin('outb_ord_hdrs', 'outb_ord_hdrs.id', '=', 'pick_list_outb_ords.outb_ord_hdr_id')
                        ->whereRaw('pick_list_outb_ords.hdr_id = pick_list_hdrs.id')
                        ->where(function($q2) use($filter) {
                            $q2->where('outb_ord_hdrs.doc_code', 'LIKE', '%'.$filter['value'].'%');
                        });
                });
            }
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $pickListHdrs->where('doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $pickListHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        foreach($leftJoinHash as $field)
        {
            
        }
        
        return $pickListHdrs
            ->paginate($pageSize);
    }

    static public function findAllExistInvDoc01Txn($siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $leftJoinHash = array();
        $qStatuses = array(DocStatus::$MAP['COMPLETE']);
        $pickListHdrs = PickListHdr::select('pick_list_hdrs.*')
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('doc_txn_flows')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['INV_DOC_01'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\PickListHdr::class)
                    ->whereRaw('doc_txn_flows.fr_doc_hdr_id = pick_list_hdrs.id')
                    ->where('doc_txn_flows.is_closed', 1);
            })
            ->where('site_flow_id', $siteFlowId)
            ->whereIn('doc_status', $qStatuses);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'delivery_point') == 0)
            {
                $pickListHdrs->whereExists(function ($q1) use($filter) {
                    $q1->select(DB::raw(1))
                        ->from('pick_list_outb_ords')
                        ->leftJoin('outb_ord_hdrs', 'outb_ord_hdrs.id', '=', 'pick_list_outb_ords.outb_ord_hdr_id')
                        ->leftJoin('delivery_points', 'delivery_points.id', '=', 'outb_ord_hdrs.delivery_point_id')
                        ->whereRaw('pick_list_outb_ords.hdr_id = pick_list_hdrs.id')
                        ->where(function($q2) use($filter) {
                            $q2->where('delivery_points.code', 'LIKE', '%'.$filter['value'].'%')
                            ->orWhere('delivery_points.company_name_01', 'LIKE', '%'.$filter['value'].'%');
                        });
                });
            }
            if(strcmp($filter['field'], 'salesman') == 0)
            {
                $pickListHdrs->whereExists(function ($q1) use($filter) {
                    $q1->select(DB::raw(1))
                        ->from('pick_list_outb_ords')
                        ->leftJoin('outb_ord_hdrs', 'outb_ord_hdrs.id', '=', 'pick_list_outb_ords.outb_ord_hdr_id')
                        ->leftJoin('users', 'users.id', '=', 'outb_ord_hdrs.salesman_id')
                        ->whereRaw('pick_list_outb_ords.hdr_id = pick_list_hdrs.id')
                        ->where(function($q2) use($filter) {
                            $q2->where('users.username', 'LIKE', '%'.$filter['value'].'%');
                        });
                });
            }
            if(strcmp($filter['field'], 'delivery_point_area') == 0)
            {
                $pickListHdrs->whereExists(function ($q1) use($filter) {
                    $q1->select(DB::raw(1))
                        ->from('pick_list_outb_ords')
                        ->leftJoin('outb_ord_hdrs', 'outb_ord_hdrs.id', '=', 'pick_list_outb_ords.outb_ord_hdr_id')
                        ->leftJoin('delivery_points', 'delivery_points.id', '=', 'outb_ord_hdrs.delivery_point_id')
                        ->whereRaw('pick_list_outb_ords.hdr_id = pick_list_hdrs.id')
                        ->where(function($q2) use($filter) {
                            $q2->where('delivery_points.area_code', 'LIKE', '%'.$filter['value'].'%');
                        });
                });
            }
            if(strcmp($filter['field'], 'outb_ord_hdr') == 0)
            {
                $pickListHdrs->whereExists(function ($q1) use($filter) {
                    $q1->select(DB::raw(1))
                        ->from('pick_list_outb_ords')
                        ->leftJoin('outb_ord_hdrs', 'outb_ord_hdrs.id', '=', 'pick_list_outb_ords.outb_ord_hdr_id')
                        ->whereRaw('pick_list_outb_ords.hdr_id = pick_list_hdrs.id')
                        ->where(function($q2) use($filter) {
                            $q2->where('outb_ord_hdrs.doc_code', 'LIKE', '%'.$filter['value'].'%')                            
                            ->orWhere('outb_ord_hdrs.sls_ord_hdr_code', 'LIKE', '%'.$filter['value'].'%')                            
                            ->orWhere('outb_ord_hdrs.sls_inv_hdr_code', 'LIKE', '%'.$filter['value'].'%');
                        });
                });
            }
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $pickListHdrs->where('doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $pickListHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        foreach($leftJoinHash as $field)
        {
            
        }
        
        return $pickListHdrs
            ->paginate($pageSize);
    }

    static public function verifyTxn()
    {
        $invalidRows = array();

        $page = 1;
        $limit = 100;
        while(true)
        {
            $offset = ($page - 1) * $limit;
            $pickListHdrs = PickListHdr::where('doc_status', DocStatus::$MAP['COMPLETE'])
            ->offset($offset)
            ->limit($limit)
            ->get();
            foreach($pickListHdrs as $pickListHdr)
            {
                $quantBalTxnsHashByDtlId = array();

                $quantBalTxns = QuantBalTxn::where('doc_hdr_type', PickListHdr::class)
                ->where('doc_hdr_id', $pickListHdr->id)
                ->get();
                foreach($quantBalTxns as $quantBalTxn)
                {
                    $tmpQuantBalTxns = array();
                    if(array_key_exists($quantBalTxn->doc_dtl_id, $quantBalTxnsHashByDtlId))
                    {
                        $tmpQuantBalTxns = $quantBalTxnsHashByDtlId[$quantBalTxn->doc_dtl_id];
                    }

                    $tmpQuantBalTxns[] = $quantBalTxn;
                    $quantBalTxnsHashByDtlId[$quantBalTxn->doc_dtl_id] = $tmpQuantBalTxns;                    
                }

                $pickListDtls = PickListDtl::where('hdr_id', $pickListHdr->id)
                ->get();
                foreach($pickListDtls as $pickListDtl)
                {
                    $ttlTxnUnitQty = 0;
                    if(array_key_exists($pickListDtl->id, $quantBalTxnsHashByDtlId))
                    {
                        $quantBalTxns = $quantBalTxnsHashByDtlId[$pickListDtl->id];
                        foreach($quantBalTxns as $quantBalTxn)
                        {
                            $ttlUnitQty = bcmul($quantBalTxn->sign, $quantBalTxn->unit_qty, 10);
                            $ttlTxnUnitQty = bcadd($ttlTxnUnitQty, $ttlUnitQty, 10);
                        }

                        $ttlDtlUnitQty = bcmul($pickListDtl->qty, $pickListDtl->uom_rate, 10);
                        if(bccomp(0, $ttlTxnUnitQty, 10) != 0)
                        {
                            $invalidRows[] = 'PickListDtl ['.$pickListDtl->id.'] qty['.$ttlDtlUnitQty.'] not tally with txn['.$ttlTxnUnitQty.']';
                        }
                    }
                    else
                    {
                        $invalidRows[] = 'PickListDtl ['.$pickListDtl->id.'] qty['.$ttlDtlUnitQty.'] not tally with txn[NULL]';
                    }
                }
            }

            if(count($pickListHdrs) == 0)
            {
                break;
            }

            $page++;
        }        

        return $invalidRows;
    }

    static public function revertDraftToVoid($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = PickListHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('PickList.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\PickListHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to VOID
                $hdrModel->doc_status = DocStatus::$MAP['VOID'];
                $hdrModel->save();

                $frDocTxnFlows = $hdrModel->frDocTxnFlows;
                foreach($frDocTxnFlows as $frDocTxnFlow)
                {
                    //move to voidTxn
                    $txnVoidModel = new DocTxnVoid;
                    $txnVoidModel->id = $frDocTxnFlow->id;
                    $txnVoidModel->proc_type = $frDocTxnFlow->proc_type;
                    $txnVoidModel->fr_doc_hdr_type = $frDocTxnFlow->fr_doc_hdr_type;
                    $txnVoidModel->fr_doc_hdr_id = $frDocTxnFlow->fr_doc_hdr_id;
                    $txnVoidModel->fr_doc_hdr_code = $frDocTxnFlow->fr_doc_hdr_code;
                    $txnVoidModel->to_doc_hdr_type = $frDocTxnFlow->to_doc_hdr_type;
                    $txnVoidModel->to_doc_hdr_id = $frDocTxnFlow->to_doc_hdr_id;
                    $txnVoidModel->is_closed = $frDocTxnFlow->is_closed;
                    $txnVoidModel->save();
        
                    //delete the txn
                    $frDocTxnFlow->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitVoidToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = PickListHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['VOID'])
                {
                    //only VOID can transition to DRAFT
                    $exc = new ApiException(__('PickList.doc_status_is_not_void', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\PickListHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                $frDocTxnVoids = $hdrModel->frDocTxnVoids;
                foreach($frDocTxnVoids as $frDocTxnVoid)
                {
                    //verify first
                    $docTxnFlow = DocTxnFlow::where(array(
                        'proc_type' => $frDocTxnVoid->proc_type,
                        'fr_doc_hdr_type' => $frDocTxnVoid->fr_doc_hdr_type,
                        'fr_doc_hdr_id' => $frDocTxnVoid->fr_doc_hdr_id,
                        'to_doc_hdr_type' => $frDocTxnVoid->to_doc_hdr_type
                        ))
                        ->first();
                    if(!empty($docTxnFlow))
                    {
                        $exc = new ApiException(__('PickList.fr_doc_is_closed', ['docCode'=>$frDocTxnVoid->fr_doc_hdr_code]));
                        $exc->addData(\App\PickListHdr::class, $hdrModel->id);
                        throw $exc;
                    }

                    //move to txnFlow
                    $txnFlowModel = new DocTxnFlow;
                    $txnFlowModel->id = $frDocTxnVoid->id;
                    $txnFlowModel->proc_type = $frDocTxnVoid->proc_type;
                    $txnFlowModel->fr_doc_hdr_type = $frDocTxnVoid->fr_doc_hdr_type;
                    $txnFlowModel->fr_doc_hdr_id = $frDocTxnVoid->fr_doc_hdr_id;
                    $txnFlowModel->fr_doc_hdr_code = $frDocTxnVoid->fr_doc_hdr_code;
                    $txnFlowModel->to_doc_hdr_type = $frDocTxnVoid->to_doc_hdr_type;
                    $txnFlowModel->to_doc_hdr_id = $frDocTxnVoid->to_doc_hdr_id;
                    $txnFlowModel->is_closed = $frDocTxnVoid->is_closed;
                    $txnFlowModel->save();
        
                    //delete the txnVoid
                    $frDocTxnVoid->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function updateDetails($pickListHdrData, $pickListDtlArray, $delPickListDtlArray) 
	{
        $delPickListDtlIds = array();
        foreach($delPickListDtlArray as $delPickListDtlData)
        {
            $delPickListDtlIds[] = $delPickListDtlData['id'];
        }
        
        $result = DB::transaction
        (
            function() use ($pickListHdrData, $pickListDtlArray, $delPickListDtlIds)
            {
                //update pickListHdr
                $pickListHdrModel = PickListHdr::lockForUpdate()->find($pickListHdrData['id']);
                if($pickListHdrModel->doc_status >= DocStatus::$MAP['WIP'])
                {
                    //WIP and COMPLETE status can not be updated
                    $exc = new ApiException(__('PickList.doc_status_is_wip_or_complete', ['docCode'=>$pickListHdrModel->doc_code]));
                    $exc->addData(\App\PickListHdr::class, $pickListHdrModel->id);
                    throw $exc;
                }
                $pickListHdrModel = RepositoryUtils::dataToModel($pickListHdrModel, $pickListHdrData);
                $pickListHdrModel->save();

                //update pickListDtl array
                $pickListDtlModels = array();
                foreach($pickListDtlArray as $pickListDtlData)
                {
                    $pickListDtlModel = null;
                    if (strpos($pickListDtlData['id'], 'NEW') !== false) 
                    {
                        $uuid = $pickListDtlData['id'];
                        $pickListDtlModel = new PickListDtl;
                        unset($pickListDtlData['id']);
                    }
                    else
                    {
                        $pickListDtlModel = PickListDtl::lockForUpdate()->find($pickListDtlData['id']);
                    }
                    
                    $pickListDtlModel = RepositoryUtils::dataToModel($pickListDtlModel, $pickListDtlData);
                    $pickListDtlModel->hdr_id = $pickListHdrModel->id;
                    $pickListDtlModel->save();
                    $pickListDtlModels[] = $pickListDtlModel;
                }

                if(!empty($delPickListDtlIds))
                {
                    PickListDtl::destroy($delPickListDtlIds);
                }

                return array(
                    'hdrModel' => $pickListHdrModel,
                    'dtlModels' => $pickListDtlModels
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function findAllExistWhseJob0301TxnAndWIP($divisionIds, $sorts, $filters = array(), $pageSize = 20) 
	{
        $whseJob0301TxnFlows = DocTxnFlow::select('doc_txn_flows.fr_doc_hdr_id')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['WHSE_JOB_03_01'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\PickListHdr::class)
                    ->distinct();

        $divPickListOutbOrds = PickListOutbOrd::select('pick_list_outb_ords.hdr_id')
                    ->leftJoin('outb_ord_hdrs', 'outb_ord_hdrs.id', '=', 'pick_list_outb_ords.outb_ord_hdr_id')
                    ->whereIn('outb_ord_hdrs.division_id', $divisionIds)
                    ->distinct();

        $qStatuses = array(DocStatus::$MAP['WIP']); 
        $pickListHdrs = PickListHdr::select('pick_list_hdrs.*')
            ->leftJoinSub($whseJob0301TxnFlows, 'whse_job_03_01_txn_flows', function ($join) {
                $join->on('pick_list_hdrs.id', '=', 'whse_job_03_01_txn_flows.fr_doc_hdr_id');
            })
            ->whereNotNull('whse_job_03_01_txn_flows.fr_doc_hdr_id')
            ->leftJoinSub($divPickListOutbOrds, 'div_pick_list_outb_ords', function ($join) {
                $join->on('pick_list_hdrs.id', '=', 'div_pick_list_outb_ords.hdr_id');
            })
            ->whereNotNull('div_pick_list_outb_ords.hdr_id')
            ->whereIn('doc_status', $qStatuses);

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $pickListHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $pickListHdrs->where('doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
        }
        
        return $pickListHdrs
            ->paginate($pageSize);
    }
}