<?php

namespace App\Repositories;

use App\ItemSalePrice;
use App\Services\Utils\RepositoryUtils;
use Illuminate\Support\Facades\DB;

class ItemSalePriceRepository 
{
    static public function findByPk($id) 
	{
        $item = ItemSalePrice::where('id', $id)
            ->first();

        return $item;
    }

    static public function deleteByPK($id) 
	{
        $item = ItemSalePrice::find($id);
        $temp=$item;
        $item->delete();
        return $temp;
    }

    static public function createModel($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = new ItemSalePrice;
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return $model;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function updateModel($data) 
	{
        $result = DB::transaction
        (
            function() use ($data)
            {
                //update Item
                $model = ItemSalePrice::lockForUpdate()->find($data['id']);
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return array(
                    'model' => $model
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function findByItemIdAndUomId($docDate, $priceGroupId, $currencyId, $itemId, $uomId) 
	{
        $itemSalePrice = ItemSalePrice::where(array(
            'item_id' => $itemId,
            'uom_id' => $uomId,
            'price_group_id' => $priceGroupId,
            'currency_id' => $currencyId
            ))
            ->whereDate('valid_from', '<=', $docDate)
            ->whereDate('valid_to', '>=', $docDate)
            ->first();

        return $itemSalePrice;
    }
}