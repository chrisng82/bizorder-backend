<?php

namespace App\Repositories;

use App\BatchJobStatus;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class BatchJobStatusRepository 
{
    static public function firstOrCreate($procType, $userId) 
	{
        $attributes = array('proc_type'=>$procType, 'user_id'=>$userId);
        $values = array();
        $batchJobStatusModel = BatchJobStatus::firstOrCreate($attributes, $values);

        return $batchJobStatusModel;
    }

    static public function updateStatusNumber($id, $number) 
	{
        DB::table('batch_job_status')
            ->where('id', $id)
            ->update(array(
                'status_number' => $number,
                'updated_at' => Carbon::now(),
            ));

        DB::connection('mysql')->select('FLUSH TABLES batch_job_status');
    }

    static public function completeStatus($id) 
	{
        $batchJobStatusModel = BatchJobStatus::find($id);
        $batchJobStatusModel->status_number = 100;
        $batchJobStatusModel->save();

        DB::connection('mysql')->select('FLUSH TABLES batch_job_status');
        return $batchJobStatusModel;
    }

    static public function delete($id) 
	{
        BatchJobStatus::destroy($id);
    }
}