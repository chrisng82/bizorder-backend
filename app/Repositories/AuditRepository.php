<?php

namespace App\Repositories;

use App\Audit;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class AuditRepository 
{
    static public function findAllByAuditableTypeAndAuditableId($auditableType, $auditableId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $model = Audit::select('audits.*')
        ->where('auditable_type', $auditableType)
        ->where('auditable_id', $auditableId);

        $leftJoinHash = array();
        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'tags') == 0)
            {
                if(!empty($filter['value']))
                {
                    $model->where('audits.tags', 'LIKE', '%'.$filter['value'].'%');
                }
            }
            if(strcmp($filter['field'], 'user') == 0)
            {
                if(!empty($filter['value']))
                {
                    $leftJoinHash['users'] = 'users';
                    $model->where(function($q) use($filter) {
                        $q->where('users.username', 'LIKE', '%'.$filter['value'].'%')
                        ->orWhere('users.first_name', 'LIKE', '%'.$filter['value'].'%')
                        ->orWhere('users.last_name', 'LIKE', '%'.$filter['value'].'%');
                    });
                }                
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'id') == 0)
            {
                $model->orderBy('id', $sort['order']);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'users') == 0)
            {
                $model->leftJoin('users', 'users.id', '=', 'audits.user_id');
            }
        }

        if($pageSize > 0)
        {
            return $model
            ->paginate($pageSize);
        }
        else
        {
            return $model
            ->paginate(PHP_INT_MAX);
        }
    }

    static public function findAllByAuditableTypeAndTags($auditableType, $tags, $sorts, $filters = array(), $pageSize = 20) 
	{
        $model = Audit::select('audits.*')
        ->where('auditable_type', $auditableType)
        ->where('tags', $tags);

        $leftJoinHash = array();
        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'tags') == 0)
            {
                $model->where('audits.tags', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'user') == 0)
            {
                if(!empty($filter['value']))
                {
                    $leftJoinHash['users'] = 'users';
                    $model->where(function($q) use($filter) {
                        $q->where('users.username', 'LIKE', '%'.$filter['value'].'%')
                        ->orWhere('users.first_name', 'LIKE', '%'.$filter['value'].'%')
                        ->orWhere('users.last_name', 'LIKE', '%'.$filter['value'].'%');
                    });
                }                
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'id') == 0)
            {
                $model->orderBy('id', $sort['order']);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'users') == 0)
            {
                $model->leftJoin('users', 'users.id', '=', 'audits.user_id');
            }
        }

        if($pageSize > 0)
        {
            return $model
            ->paginate($pageSize);
        }
        else
        {
            return $model
            ->paginate(PHP_INT_MAX);
        }
    }

    static public function findAllByUserId($userId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $model = Audit::select('audits.*')
        ->where('user_id', $userId);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'tags') == 0)
            {
                $model->where('audits.tags', 'LIKE', '%'.$filter['value'].'%');
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'id') == 0)
            {
                $model->orderBy('id', $sort['order']);
            }
        }

        if($pageSize > 0)
        {
            return $model
            ->paginate($pageSize);
        }
        else
        {
            return $model
            ->paginate(PHP_INT_MAX);
        }
    }

    static public function findAllByauditableId($auditable_id, $sorts, $filters = array(), $pageSize = 20) 
	{
        $model = Audit::select('audits.*')
        ->where('auditable_id', $auditable_id);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'tags') == 0)
            {
                $model->where('audits.tags', 'LIKE', '%'.$filter['value'].'%');
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'id') == 0)
            {
                $model->orderBy('id', $sort['order']);
            }
        }

        if($pageSize > 0)
        {
            return $model
            ->paginate($pageSize);
        }
        else
        {
            return $model
            ->paginate(PHP_INT_MAX);
        }
    }
}