<?php

namespace App\Repositories;

use App\StorageStrategy;

class StorageStrategyRepository 
{
    static public function findAllByItemAttr($siteId, $itemId, $storageClass, 
    $itemGroup01Id, $itemGroup02Id, $itemGroup03Id, 
    $itemGroup04Id, $itemGroup05Id, 
    $itemCond01Id, $itemCond02Id, $itemCond03Id, 
    $itemCond04Id, $itemCond05Id
    ) 
	{
        $storageStrategies = StorageStrategy::where('site_id', $siteId)
            ->where(function ($query) use (
                $itemId, $storageClass, 
                $itemGroup01Id, $itemGroup02Id, $itemGroup03Id, 
                $itemGroup04Id, $itemGroup05Id,
                $itemCond01Id, $itemCond02Id, $itemCond03Id, 
                $itemCond04Id, $itemCond05Id
                ) {
                $query->where('item_id', $itemId)
                    ->orWhere('storage_class', $storageClass)
                    ->orWhere('item_group_01_id', $itemGroup01Id)
                    ->orWhere('item_group_02_id', $itemGroup02Id)
                    ->orWhere('item_group_03_id', $itemGroup03Id)
                    ->orWhere('item_group_04_id', $itemGroup04Id)
                    ->orWhere('item_group_05_id', $itemGroup05Id)
                    ->orWhere('item_cond_01_id', $itemCond01Id)
                    ->orWhere('item_cond_02_id', $itemCond02Id)
                    ->orWhere('item_cond_03_id', $itemCond03Id)
                    ->orWhere('item_cond_04_id', $itemCond04Id)
                    ->orWhere('item_cond_05_id', $itemCond05Id);
            })
            ->orderBy('line_no', 'ASC')
            ->get();
        return $storageStrategies;
    }
}