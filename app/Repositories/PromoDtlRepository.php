<?php

namespace App\Repositories;

use App\PromoDtl;
use App\Services\Env\DocStatus;

class PromoDtlRepository 
{
    static public function queryHdrIdByDocDtl($divisionId, $docDate,
    $debtorId, $custGroup01Id, $custGroup02Id, $custGroup03Id, $custGroup04Id, $custGroup05Id, 
    $itemId, $itemGroup01Id, $itemGroup02Id, $itemGroup03Id, $itemGroup04Id, $itemGroup05Id) 
	{
        $promoDtls = PromoDtl::where('doc_status', DocStatus::$MAP['COMPLETE'])
            ->whereDate('valid_from', '<=', $docDate)
            ->whereDate('valid_to', '>=', $docDate)
            ->where(function ($query) use ($itemId, $itemGroup01Id, $itemGroup02Id, $itemGroup03Id, $itemGroup04Id, $itemGroup05Id) {
                $query
                    ->when($itemId > 0, function ($query) use ($itemId) {
                        return $query->orWhere('item_id', $itemId);
                    })
                    ->when($itemGroup01Id > 0, function ($query) use ($itemGroup01Id) {
                        return $query->orWhere('group_01_id', $itemGroup01Id);
                    })
                    ->when($itemGroup02Id > 0, function ($query) use ($itemGroup02Id) {
                        return $query->orWhere('group_02_id', $itemGroup02Id);
                    })
                    ->when($itemGroup03Id > 0, function ($query) use ($itemGroup03Id) {
                        return $query->orWhere('group_03_id', $itemGroup03Id);
                    })
                    ->when($itemGroup04Id > 0, function ($query) use ($itemGroup04Id) {
                        return $query->orWhere('group_04_id', $itemGroup04Id);
                    })
                    ->when($itemGroup05Id > 0, function ($query) use ($itemGroup05Id) {
                        return $query->orWhere('group_05_id', $itemGroup05Id);
                    });
                })
            ->whereRaw('EXISTS(
                    SELECT * FROM promo_debtors pc 
                    WHERE pc.hdr_id = promo_dtls.hdr_id 
                    AND (
                        (debtor_id = ? AND debtor_id >0) 
                        OR (group_01_id = ? AND group_01_id > 0)
                        OR (group_02_id = ? AND group_02_id > 0)
                        OR (group_03_id = ? AND group_03_id > 0)
                        OR (group_04_id = ? AND group_04_id > 0)
                        OR (group_05_id = ? AND group_05_id > 0)
                    )
                )', array($debtorId, $custGroup01Id, $custGroup02Id, $custGroup03Id, $custGroup04Id, $custGroup05Id))
            ->whereRaw('EXISTS(
                    SELECT * FROM promo_divisions pd 
                    WHERE pd.hdr_id = promo_dtls.hdr_id AND division_id = ?
                )', array($divisionId))
            ->groupBy('hdr_id')
            ->get();
            //->toSql();
            //dd($promoDtls);
        return $promoDtls;
    }

    static public function findAllByHdrIds($hdrIds) 
	{
        $promoDtls = PromoDtl::whereIn('hdr_id', $hdrIds)
            ->get();

        return $promoDtls;
    }
}