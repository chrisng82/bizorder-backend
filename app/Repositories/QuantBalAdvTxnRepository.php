<?php

namespace App\Repositories;

use App\QuantBalAdvTxn;
use Illuminate\Support\Facades\DB;

class QuantBalAdvTxnRepository 
{
    static public function queryHuCountBySiteId($siteId) 
	{
        $storageBinData = array();
        $quantBalAdvTxns = QuantBalAdvTxn::selectRaw('storage_bin_id, COUNT(DISTINCT handling_unit_id) as handling_unit_count')
            ->where('site_id', $siteId)
            ->whereRaw('unit_qty > 0')
            ->whereRaw('handling_unit_id > 0')
            ->groupBy('storage_bin_id')
            ->get();
        foreach($quantBalAdvTxns as $quantBalAdvTxn)
        {
            $data = array();
            $data['storage_bin_id'] = $quantBalAdvTxn->storage_bin_id;
            $data['handling_unit_count'] = $quantBalAdvTxn->handling_unit_count;

            $storageBinData[] = $data;
        }
        return $storageBinData;
    }

    static public function queryHuCountByStorageBinId($storageBinId) 
	{
        $data = DB::transaction
        (
            function() use ($storageBinId)
            {
                $quantBalAdvTxn = QuantBalAdvTxn::selectRaw('storage_bin_id, COUNT(DISTINCT handling_unit_id) as handling_unit_count')
                ->where('storage_bin_id', $storageBinId)
                ->whereRaw('unit_qty > 0')
                ->whereRaw('handling_unit_id > 0')
                ->groupBy('storage_bin_id')
                ->first();
                $data = array(
                    'storage_bin_id' => $storageBinId,
                    'handling_unit_count' => 0
                );
                if(!empty($quantBalAdvTxn))
                {
                    $data['storage_bin_id'] = $quantBalAdvTxn->storage_bin_id;
                    $data['handling_unit_count'] = $quantBalAdvTxn->handling_unit_count;
                }

                return $data;
            }, 
            5 //reattempt times
        );
        return $data;
    }

    static public function queryDocsByStorageBinIds($storageBinIds) 
	{
        $docHdrDatas = array();
        $quantBalAdvTxns = QuantBalAdvTxn::select('doc_hdr_type', 'doc_hdr_id')
        ->whereIn('storage_bin_id', $storageBinIds)
        ->distinct()
        ->get();
        foreach($quantBalAdvTxns as $quantBalAdvTxn)
        {
            $docHdr = $quantBalAdvTxn->docHdr;
            if(!empty($docHdr))
            {
                $data = array();
                $data['doc_code'] = $docHdr->doc_code;

                $docHdrDatas[] = $data;
            }
        }
        return $docHdrDatas;
    }
}