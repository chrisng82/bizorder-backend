<?php

namespace App\Repositories;

use App\Services\Env\BizPartnerType;
use App\Services\Env\ResStatus;
use App\BizPartner;
use App\Creditor;
use App\Area;
use App\Repositories\DebtorRepository;
use Illuminate\Support\Facades\DB;

class BizPartnerRepository 
{
    static public function findRandomByType($bizPartnerType) 
	{
        $model = BizPartner::where('status', ResStatus::$MAP['ACTIVE'])
            ->where('biz_partner_type', $bizPartnerType)
            ->orderByRaw('RAND()')
            ->first();

        return $model;
    }

    static public function select2($search, $filters, $pageSize = 10) 
	{
        $bizPartners = BizPartner::where(function($q) use($search) {
            $q->where('code', 'LIKE', '%' . $search . '%')
            ->orWhere('company_name_01', 'LIKE', '%' . $search . '%');
        });
        if(count($filters) >= 1)
        {
            $bizPartners = $bizPartners->where($filters);
        }
        return $bizPartners
            ->orderBy('code', 'ASC')
            ->paginate($pageSize);
    }

    static public function findTop() 
	{
        $model = BizPartner::first();

        return $model;
    }

    static public function findByPk($id) 
	{
        $model = BizPartner::where('id', $id)
            ->first();

        return $model;
    }

    static public function syncAdvShipSync01($division, $data) 
	{
        $model = DB::transaction
        (
            function() use ($division, $data)
            {
                //check creditor
                $creditor = Creditor::where('code', $data['vendor_code'])
                    ->first();
                if(empty($creditor))
                {
                    $creditor = new Creditor;
                    $creditor->code = $data['vendor_code'];
                    $creditor->ref_code_01 = $data['vendor_ref_code'];
                    $creditor->company_name_01 = $data['vendor_name'];
                    $creditor->company_name_02 = $data['vendor_name_01'];
                    $creditor->cmpy_register_no = $data['vendor_registration_no'];
                    $creditor->tax_register_no = $data['vendor_gst_reg_no'];
                    $creditor->unit_no = '';
                    $creditor->building_name = $data['bill_address_01'];
                    $creditor->street_name = $data['bill_address_02'];
                    $creditor->district_01 = $data['bill_address_03'];
                    $creditor->district_02 = $data['bill_address_04'];
                    $creditor->postcode = $data['bill_postcode'];
                    $creditor->state_name = '';
                    $creditor->country_name = '';
                    $creditor->attention = $data['bill_attention'];
                    $creditor->phone_01 = $data['bill_phone_01'];
                    $creditor->phone_02 = $data['bill_phone_02'];
                    $creditor->fax_01 = $data['bill_fax_01'];
                    $creditor->fax_02 = $data['bill_fax_01'];
                    $creditor->email_01 = $data['bill_email'];
                    $creditor->email_02 = '';
                    $creditor->save();
                }
                else
                {
                    /*
                    $creditor->company_name_01 = $data['vendor_name'];
                    $creditor->company_name_02 = $data['vendor_name_01'];
                    $creditor->save();
                    */
                }

                //check BizPartner
                $bizPartner = BizPartner::where('code', $data['vendor_code'])
                    ->first();
                if(empty($bizPartner))
                {
                    $bizPartner = new BizPartner;
                    $bizPartner->biz_partner_type = BizPartnerType::$MAP['CREDITOR'];
                    $bizPartner->creditor_id = $creditor->id;
                    $bizPartner->price_group_id = 0;
                    $bizPartner->code = $data['vendor_code'];
                    $bizPartner->ref_code_01 = $data['vendor_ref_code'];
                    $bizPartner->company_name_01 = $data['vendor_name'];
                    $bizPartner->company_name_02 = $data['vendor_name_01'];
                    $bizPartner->unit_no = '';
                    $bizPartner->building_name = $data['del_address_01'];
                    $bizPartner->street_name = $data['del_address_02'];
                    $bizPartner->district_01 = $data['del_address_03'];
                    $bizPartner->district_02 = $data['del_address_04'];
                    $bizPartner->postcode = $data['del_postcode'];
                    $bizPartner->state_name = '';
                    $bizPartner->country_name = '';
                    $bizPartner->attention = $data['del_attention'];
                    $bizPartner->phone_01 = $data['del_phone_01'];
                    $bizPartner->phone_02 = $data['del_phone_02'];
                    $bizPartner->fax_01 = $data['del_fax_01'];
                    $bizPartner->fax_02 = $data['del_fax_01'];
                    $bizPartner->email_01 = '';
                    $bizPartner->email_02 = '';
                    $bizPartner->status = ResStatus::$MAP['ACTIVE'];
                    $bizPartner->save();
                }
                else
                {
                    /*
                    $bizPartner->company_name_01 = $data['vendor_name'];
                    $bizPartner->company_name_02 = $data['vendor_name_01'];
                    $bizPartner->save();
                    */
                }

                return $bizPartner;
            }
        );
        return $model;
    }
}