<?php

namespace App\Repositories;

use App\DocNo;
use App\DivisionDocNo;
use App\SiteDocNo;

class DocNoRepository 
{
    static public function generateNewCode($id, $docDate) 
	{
        //this function must run within transaction
        $newCode = '';
        $docNo = DocNo::where('id', $id)
            ->lockForUpdate()
            ->first();

        $newCode = $docNo->doc_prefix 
            . str_pad($docNo->running_no, $docNo->running_no_length, '0', STR_PAD_LEFT) 
            . $docNo->doc_suffix;

        $docNo->running_no = $docNo->running_no + 1;

        DocNo::disableAuditing();
        $docNo->save();
        DocNo::enableAuditing();
        
        return $newCode;
    }

    static public function findAllDivisionDocNo($divisionId, $docType, $docDate) 
	{
        $divisionDocNos = DivisionDocNo::where(array(
            'division_id' => $divisionId,
            'doc_no_type' => $docType
        ))
        ->orderBy('seq_no')
        ->get();
        $divisionDocNos->load('docNo');

        $docNos = array();
        foreach($divisionDocNos as $divisionDocNo)
        {
            $docNo = $divisionDocNo->docNo;

            $docNo->latest_code = $docNo->doc_prefix 
                . str_pad($docNo->running_no, $docNo->running_no_length, '0', STR_PAD_LEFT) 
                . $docNo->doc_suffix;
            $docNos[] = $docNo;
        }

        return $docNos;
    }

    static public function findAllSiteDocNo($siteId, $docType, $docDate) 
	{
        $siteDocNos = SiteDocNo::where(array(
            'site_id' => $siteId,
            'doc_no_type' => $docType
        ))
        ->orderBy('seq_no')
        ->get();
        $siteDocNos->load('docNo');

        $docNos = array();
        foreach($siteDocNos as $siteDocNo)
        {
            $docNo = $siteDocNo->docNo;

            $docNo->latest_code = $docNo->doc_prefix 
                . str_pad($docNo->running_no, $docNo->running_no_length, '0', STR_PAD_LEFT) 
                . $docNo->doc_suffix;
            $docNos[] = $docNo;
        }

        return $docNos;
    }
}