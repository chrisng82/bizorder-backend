<?php

namespace App\Repositories;

use App\DocPhoto;
use App\Services\Env\ResType;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class DocPhotoRepository 
{
    static public function createWhseJobPhoto($whseJobDtlModel, $photoData) 
	{
        $blob = base64_decode($photoData['blob']);
        $imageInfo = getimagesizefromstring($blob);
        $imageExt = image_type_to_extension($imageInfo[2]);

        if(!isset($data['old_filename']))
        {
            $data['old_filename'] = '';
        }
        if(!isset($data['old_width']))
        {
            $data['old_width'] = $imageInfo[0];
        }
        if(!isset($data['old_height']))
        {
            $data['old_height'] = $imageInfo[1];
        }
        
        $docPhotoModel = new DocPhoto;
        $docPhotoModel->whse_job_hdr_id = $whseJobDtlModel->hdr_id;
        $docPhotoModel->whse_job_dtl_id = $whseJobDtlModel->id;
        $docPhotoModel->doc_hdr_type = $whseJobDtlModel->doc_hdr_type;
        $docPhotoModel->doc_hdr_id = $whseJobDtlModel->doc_hdr_id;
        $docPhotoModel->doc_dtl_id = $whseJobDtlModel->doc_dtl_id;
        $docPhotoModel->desc_01 = empty($photoData['desc_01']) ? '' : $photoData['desc_01'];
        $docPhotoModel->desc_02 = empty($photoData['desc_02']) ? '' : $photoData['desc_02'];
        $docPhotoModel->old_filename = empty($photoData['old_filename']) ? '' : $photoData['old_filename'];
        $docPhotoModel->old_width = $photoData['old_width'];
        $docPhotoModel->old_height = $photoData['old_height'];
        $docPhotoModel->path = '';
        
        $docPhotoModel->save();

        $filename = ResType::$MAP['DOC_PHOTO'].'_'.$docPhotoModel->id.$imageExt;
        $path = Storage::put('photos/'.$filename, $blob);

        $docPhotoModel->path = env('APP_URL').Storage::url('photos/'.$filename);
        $docPhotoModel->save();
        return $docPhotoModel;
    }
}