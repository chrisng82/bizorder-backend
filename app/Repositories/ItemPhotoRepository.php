<?php

namespace App\Repositories;

use App\ItemPhoto;
use App\Services\Env\ResType;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use JD\Cloudder\Facades\Cloudder;

class ItemPhotoRepository 
{
    static public function findByItemId($itemId) 
	{
        $itemPhoto = ItemPhoto::where('item_id')
            ->where('desc_01', $desc01)
            ->first();

        return $itemPhoto;
    }

    static public function findByDesc01($itemId, $desc01) 
	{
        $itemPhoto = ItemPhoto::where('item_id', $itemId)
            ->where('desc_01', $desc01)
            ->first();

        return $itemPhoto;
    }

    static public function syncItemSync0101($data) 
	{
        $imageInfo = getimagesizefromstring($data['blob']);
        $imageExt = image_type_to_extension($imageInfo[2]);

        if(!isset($data['old_filename']))
        {
            $data['old_filename'] = '';
        }
        if(!isset($data['old_width']))
        {
            $data['old_width'] = $imageInfo[0];
        }
        if(!isset($data['old_height']))
        {
            $data['old_height'] = $imageInfo[1];
        }
        
        $itemPhotoModel = DB::transaction
        (
            function() use ($data, $imageInfo, $imageExt)
            {
                $itemPhotoModel = ItemPhoto::firstOrNew(['item_id' => $data['item_id'], 'desc_01' => $data['desc_01']]);
                if($itemPhotoModel->id > 0)
                {
                    //update record
                }
                else
                {
                    //insert record
                    //default value for EfiChain
                    $itemPhotoModel->desc_01 = $data['desc_01'];
                    $itemPhotoModel->desc_02 = $data['desc_02'];
                    $itemPhotoModel->old_filename = $data['old_filename'];
                    $itemPhotoModel->old_width = $data['old_width'];
                    $itemPhotoModel->old_height = $data['old_height'];
                    $itemPhotoModel->path = '';
                    
                    $itemPhotoModel->save();
                }

                $filename = ResType::$MAP['ITEM_PHOTO'].'_'.$itemPhotoModel->id.$imageExt;
                $path = Storage::put('photos/'.$filename, $data['blob']);

                $itemPhotoModel->path = env('APP_URL').Storage::url('photos/'.$filename);
                $itemPhotoModel->save();
            }, 
            5 //reattempt times
        );
        return $itemPhotoModel;
    }

    static public function saveItemPhoto($data) 
	{
        // $itemPhotoModel = DB::transaction
        // (
        //     function() use ($data)
        //     {
                $itemPhotoModel = ItemPhoto::firstOrNew(['item_id' => $data['item_id'], 'desc_01' => $data['desc_01']]);
                if($itemPhotoModel->id > 0)
                {
                    //update record
                    $itemPhotoModel->desc_02 = $data['desc_02'];
                    $itemPhotoModel->old_filename = $data['old_filename'];
                    
                    if (isset($data['old_filename']) && !empty($data['old_filename'])) {
                        list($width, $height) = getimagesize($data['old_filename']);
                        $itemPhotoModel->old_width = $width;
                        $itemPhotoModel->old_height = $height;
                    }
                    else if (isset($data['blob']) && !empty($data['blob']))
                    {
                        $temp = tempnam(sys_get_temp_dir(), 'TMP_');
                        file_put_contents($temp, $data['blob']);
                        $itemPhotoModel->old_filename = $temp;
                        list($width, $height) = getimagesize($temp);
                        $itemPhotoModel->old_width = $width;
                        $itemPhotoModel->old_height = $height;
                        unset($data['blob']);
                    }
                    else
                    {
                        throw new Exception('no image to save');
                    }
                    
                    $itemPhotoModel->path = '';
                    
                    $itemPhotoModel->save();
                }
                else
                {
                    //insert record
                    //default value for EfiChain
                    $itemPhotoModel->desc_01 = $data['desc_01'];
                    $itemPhotoModel->desc_02 = $data['desc_02'];
                    if (isset($data['old_filename']) && !empty($data['old_filename']))
                    {
                        $itemPhotoModel->old_filename = $data['old_filename'];
                        list($width, $height) = getimagesize($data['old_filename']);
                        $itemPhotoModel->old_width = $width;
                        $itemPhotoModel->old_height = $height;
                    }
                    else if (isset($data['blob']) && !empty($data['blob']))
                    {
                        $temp = tempnam(sys_get_temp_dir(), 'TMP_');
                        file_put_contents($temp, $data['blob']);
                        $itemPhotoModel->old_filename = $temp;
                        list($width, $height) = getimagesize($temp);
                        $itemPhotoModel->old_width = $width;
                        $itemPhotoModel->old_height = $height;
                        unset($data['blob']);
                    }
                    else
                    {
                        throw new Exception('no image to save');
                    }

                    $itemPhotoModel->path = '';
                    
                    $itemPhotoModel->save();
                }

                // $filename = ResType::$MAP['ITEM_PHOTO'].'_'.$itemPhotoModel->id.'.'.$imageExt;
                // $path = Storage::put('photos/'.$filename, $data['blob']);
                
                Cloudder::upload($itemPhotoModel->old_filename, null);
                $public_id = Cloudder::getPublicId();
                $image_url= Cloudder::show($public_id, ["width" => $itemPhotoModel->old_width, "height"=>$itemPhotoModel->old_height]);
                //save to uploads directory
                // $image = $data['blob'];
                // $image->move(public_path("uploads"), $itemPhotoModel->desc_01);

                $itemPhotoModel->path = $image_url;
                $itemPhotoModel->desc_02 = $public_id;
                $itemPhotoModel->save();

        //         return $itemPhotoModel;
        //     }, 
        //     5 //reattempt times
        // );
        return $itemPhotoModel;
    }

    static public function deleteItemPhotos($itemId, $delItemPhotoArray) 
	{
        $delItemPhotoModelArray = array();
        $delItemPhotoIds = array();
        $delItemPhotoPublicIds = array();
        foreach($delItemPhotoArray as $delItemPhotoData)
        {
            $itemPhotoModel = ItemPhoto::where('id',$delItemPhotoData['item_photo_id'])->first();
            $delItemPhotoModelArray[] = $itemPhotoModel;
            $delItemPhotoIds[] = $itemPhotoModel->id;
            $delItemPhotoPublicIds[] = $itemPhotoModel->desc_02;
        }
        
        $result = DB::transaction
        (
            function() use ($itemId, $delItemPhotoIds, $delItemPhotoPublicIds)
            {
                if(!empty($delItemPhotoIds))
                {
                    ItemPhoto::destroy($delItemPhotoIds);
                }

                if(!empty($delItemPhotoIds))
                {
                    Cloudder::destroyImages($delItemPhotoPublicIds, array());
                }
            }, 
            5 //reattempt times
        );

        return $delItemPhotoModelArray;
    }
}