<?php

namespace App\Repositories;

use App\Services\Utils\RepositoryUtils;
use App\DocTxnFlow;
use App\DocTxnVoid;
use App\CountAdjDtl;
use App\CountAdjHdr;
use App\CountAdjCDtl;
use App\CycleCountDtl;
use App\Services\Env\ProcType;
use App\Services\Env\DocStatus;
use App\Services\Env\PhysicalCountStatus;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\DB;

class CountAdjHdrRepository 
{
    static public function findByPk($hdrId) 
	{
        $countAdjHdr = CountAdjHdr::where('id', $hdrId)
            ->first();

        return $countAdjHdr;
    }

    static public function txnFindByPk($hdrId) 
	{
        $model = DB::transaction
        (
            function() use ($hdrId)
            {
                $countAdjHdr = CountAdjHdr::where('id', $hdrId)
                    ->first();
                return $countAdjHdr;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function commitToWip($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = CountAdjHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('CountAdj.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CountAdjHdr::class, $hdrModel->id);
                    throw $exc;
                }

                $dtlModels = CountAdjDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->orderBy('line_no')
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('CountAdj.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CountAdjHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to WIP
                $hdrModel->doc_status = DocStatus::$MAP['WIP'];
                $hdrModel->save();

                /*
                $countAdjDtls = CountAdjDtl::where('hdr_id', $hdrModel->id)
                    ->lockForUpdate()
                    ->get();
                $countAdjDtls->load('item');
                foreach($countAdjDtls as $countAdjDtl)
                {
                    
                }
                */
                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitToComplete($hdrId, $isNonZeroBal)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId, $isNonZeroBal)
            {
                $hdrModel = CountAdjHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();
                    
                if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('CountAdj.doc_status_is_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CountAdjHdr::class, $hdrModel->id);
                    throw $exc;
                }
                if($hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('CountAdj.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CountAdjHdr::class, $hdrModel->id);
                    throw $exc;
                }

                $siteFlow = $hdrModel->siteFlow;
        
                //1) update docStatus to COMPLETE
                $hdrModel->doc_status = DocStatus::$MAP['COMPLETE'];
                $hdrModel->save();

                $dtlModels = CountAdjDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('CountAdj.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CountAdjHdr::class, $hdrModel->id);
                    throw $exc;
                }
                $dtlModels->load('item');
                foreach($dtlModels as $dtlModel)
                {                    
                    $item = $dtlModel->item;
                    if(empty($item))
                    {
                        //only DRAFT or above can transition to COMPLETE
                        $exc = new ApiException(__('CountAdj.item_is_required', ['lineNo'=>$dtlModel->line_no]));
                        $exc->addData(\App\CountAdjDtl::class, $dtlModel->id);
                        throw $exc;
                    }  
                    
                    if($dtlModel->storage_bin_id == 0)
                    {
                        //only DRAFT or above can transition to COMPLETE
                        $exc = new ApiException(__('CountAdj.storage_bin_is_required', ['lineNo'=>$dtlModel->line_no]));
                        $exc->addData(\App\CountAdjDtl::class, $dtlModel->id);
                        throw $exc;
                    }

                    //this is stock transfer
                    QuantBalRepository::commitStockAdjustment(
                        $siteFlow->site_id, $item->id, $hdrModel->doc_date, \App\CountAdjHdr::class, 
                        $hdrModel->id, $dtlModel->id, $hdrModel->company_id, 
                        $dtlModel->uom_id, $dtlModel->uom_rate, $dtlModel->sign, $dtlModel->qty, 
                        $dtlModel->batch_serial_no, $dtlModel->expiry_date, $dtlModel->receipt_date, 
                        $dtlModel->storage_bin_id, $dtlModel->handling_unit_id);
                }  
                return $hdrModel;              
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertCompleteToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = CountAdjHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                {
                    $exc = new ApiException(__('CountAdj.doc_status_is_not_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CountAdjHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                QuantBalRepository::revertStockTransaction(\App\CountAdjHdr::class, $hdrModel->id);
                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertWipToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = CountAdjHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['WIP'])
                {
                    $exc = new ApiException(__('CountAdj.doc_status_is_not_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CountAdjHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function createProcess($procType, $docNoId, $hdrData, $dtlDataList, $docTxnFlowDataList, $oriCycleCountDtlDataList = array()) 
	{
        $countAdjHdr = DB::transaction
        (
            function() use ($procType, $docNoId, $hdrData, $dtlDataList, $docTxnFlowDataList, $oriCycleCountDtlDataList)
            {
                if(count($dtlDataList) == 0)
                {
                    $exc = new ApiException(__('CountAdj.nothing_to_be_adjusted', ['docCode'=>$docTxnFlowDataList[0]['fr_doc_hdr_code']]));
                    $exc->addData(\App\CycleCountHdr::class, $docTxnFlowDataList[0]['fr_doc_hdr_id']);
                    throw $exc;
                }
                
                $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

                $hdrModel = new CountAdjHdr;
                $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
                $hdrModel->doc_code = $newCode;
                $hdrModel->proc_type = $procType;
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                foreach($dtlDataList as $dtlData)
                {
                    $dtlModel = new CountAdjDtl;
                    $dtlModel = RepositoryUtils::dataToModel($dtlModel, $dtlData);
                    $dtlModel->hdr_id = $hdrModel->id;
                    $dtlModel->save();

                    //create a carbon copy
                    $cDtlModel = new CountAdjCDtl;
                    $cDtlModel = RepositoryUtils::dataToModel($cDtlModel, $dtlData);
                    $cDtlModel->hdr_id = $hdrModel->id;
                    $cDtlModel->save();
                }

                //process docTxnFlowDataList
                foreach($docTxnFlowDataList as $docTxnFlowData)
                {                    
                    $frDocHdrModel = $docTxnFlowData['fr_doc_hdr_type']::where('id', $docTxnFlowData['fr_doc_hdr_id'])
                    ->lockForUpdate()
                    ->first();
                    if($frDocHdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                    {
                        $exc = new ApiException(__('CountAdj.fr_doc_is_not_complete', ['docType'=>$docTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$docTxnFlowData['fr_doc_hdr_code']]));
                        $exc->addData($docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_code']);
                        throw $exc;
                    }

                    //verify the frDoc is not closed for this procType
                    $tmpDocTxnFlow = DocTxnFlowRepository::findByProcTypeAndFrHdrId($procType, $docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_id']);
                    if(!empty($tmpDocTxnFlow))
                    {
                        $exc = new ApiException(__('CountAdj.fr_doc_is_closed', ['docType'=>$docTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$docTxnFlowData['fr_doc_hdr_code']]));
                        $exc->addData($docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_id']);
                        throw $exc;
                    }
                    
                    $docTxnFlowModel = new DocTxnFlow;
                    $docTxnFlowModel = RepositoryUtils::dataToModel($docTxnFlowModel, $docTxnFlowData);
                    $docTxnFlowModel->proc_type = $procType;
                    $docTxnFlowModel->to_doc_hdr_type = \App\CountAdjHdr::class;
                    $docTxnFlowModel->to_doc_hdr_id = $hdrModel->id;
                    $docTxnFlowModel->save();
                }

                foreach($oriCycleCountDtlDataList as $oriCycleCountDtlData)
                {
                    $oriCycleCountDtl = CycleCountDtl::lockForUpdate()->find($oriCycleCountDtlData['id']);
                    if($oriCycleCountDtl->physical_count_status == PhysicalCountStatus::$MAP['COUNTING'])
                    {
                        $exc = new ApiException(__('CountAdj.physical_count_status_is_counting', ['lineNo'=>$oriCycleCountDtl->line_no]));
                        //$exc->addData(\App\CycleCountDtl::class, $cycleCountDtl->id);
                        throw $exc;
                    }
                    //change physical_count_status to COMPLETE
                    if($oriCycleCountDtl->physical_count_status == PhysicalCountStatus::$MAP['MARK_CONFIRMED'])
                    {
                        $oriCycleCountDtl->physical_count_status = PhysicalCountStatus::$MAP['CONFIRMED'];
                    }
                    elseif($oriCycleCountDtl->physical_count_status == PhysicalCountStatus::$MAP['MARK_DROPPED'])
                    {
                        $oriCycleCountDtl->physical_count_status = PhysicalCountStatus::$MAP['DROPPED'];
                    }
                    $oriCycleCountDtl->save();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $countAdjHdr;
    }

    static public function revertDraftToVoid($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = CountAdjHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('CountAdj.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CountAdjHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to VOID
                $hdrModel->doc_status = DocStatus::$MAP['VOID'];
                $hdrModel->save();

                $frDocTxnFlows = $hdrModel->frDocTxnFlows;
                foreach($frDocTxnFlows as $frDocTxnFlow)
                {
                    //move to voidTxn
                    $txnVoidModel = new DocTxnVoid;
                    $txnVoidModel->id = $frDocTxnFlow->id;
                    $txnVoidModel->proc_type = $frDocTxnFlow->proc_type;
                    $txnVoidModel->fr_doc_hdr_type = $frDocTxnFlow->fr_doc_hdr_type;
                    $txnVoidModel->fr_doc_hdr_id = $frDocTxnFlow->fr_doc_hdr_id;
                    $txnVoidModel->fr_doc_hdr_code = $frDocTxnFlow->fr_doc_hdr_code;
                    $txnVoidModel->to_doc_hdr_type = $frDocTxnFlow->to_doc_hdr_type;
                    $txnVoidModel->to_doc_hdr_id = $frDocTxnFlow->to_doc_hdr_id;
                    $txnVoidModel->is_closed = $frDocTxnFlow->is_closed;
                    $txnVoidModel->save();
        
                    //delete the txn
                    $frDocTxnFlow->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitVoidToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = CountAdjHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['VOID'])
                {
                    //only VOID can transition to DRAFT
                    $exc = new ApiException(__('CountAdj.doc_status_is_not_void', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\CountAdjHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                $frDocTxnVoids = $hdrModel->frDocTxnVoids;
                foreach($frDocTxnVoids as $frDocTxnVoid)
                {
                    //verify first
                    $docTxnFlow = DocTxnFlow::where(array(
                        'proc_type' => $frDocTxnVoid->proc_type,
                        'fr_doc_hdr_type' => $frDocTxnVoid->fr_doc_hdr_type,
                        'fr_doc_hdr_id' => $frDocTxnVoid->fr_doc_hdr_id,
                        'to_doc_hdr_type' => $frDocTxnVoid->to_doc_hdr_type
                        ))
                        ->first();
                    if(!empty($docTxnFlow))
                    {
                        $exc = new ApiException(__('CountAdj.fr_doc_is_closed', ['docCode'=>$frDocTxnVoid->fr_doc_hdr_code]));
                        $exc->addData(\App\CountAdjHdr::class, $hdrModel->id);
                        throw $exc;
                    }

                    //move to txnFlow
                    $txnFlowModel = new DocTxnFlow;
                    $txnFlowModel->id = $frDocTxnVoid->id;
                    $txnFlowModel->proc_type = $frDocTxnVoid->proc_type;
                    $txnFlowModel->fr_doc_hdr_type = $frDocTxnVoid->fr_doc_hdr_type;
                    $txnFlowModel->fr_doc_hdr_id = $frDocTxnVoid->fr_doc_hdr_id;
                    $txnFlowModel->fr_doc_hdr_code = $frDocTxnVoid->fr_doc_hdr_code;
                    $txnFlowModel->to_doc_hdr_type = $frDocTxnVoid->to_doc_hdr_type;
                    $txnFlowModel->to_doc_hdr_id = $frDocTxnVoid->to_doc_hdr_id;
                    $txnFlowModel->is_closed = $frDocTxnVoid->is_closed;
                    $txnFlowModel->save();
        
                    //delete the txnVoid
                    $frDocTxnVoid->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function findAll($siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $countAdjHdrs = CountAdjHdr::select('count_adj_hdrs.*')
            ->where('site_flow_id', $siteFlowId);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $countAdjHdrs->where('count_adj_hdrs.doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $countAdjHdrs->orderBy('doc_code', $sort['order']);
            }
        }
        
        if($pageSize > 0)
        {
            return $countAdjHdrs
                ->paginate($pageSize);
        }
        else
        {
            return $countAdjHdrs
                ->paginate(PHP_INT_MAX);
        }
    }

    static public function updateDetails($countAdjHdrData, $countAdjDtlArray, $delCountAdjDtlArray) 
	{
        $delCountAdjDtlIds = array();
        foreach($delCountAdjDtlArray as $delCountAdjDtlData)
        {
            $delCountAdjDtlIds[] = $delCountAdjDtlData['id'];
        }
        
        $result = DB::transaction
        (
            function() use ($countAdjHdrData, $countAdjDtlArray, $delCountAdjDtlIds)
            {
                //update countAdjHdr
                $countAdjHdrModel = CountAdjHdr::lockForUpdate()->find($countAdjHdrData['id']);
                if($countAdjHdrModel->doc_status >= DocStatus::$MAP['WIP'])
                {
                    //WIP and COMPLETE status can not be updated
                    $exc = new ApiException(__('CountAdj.doc_status_is_wip_or_complete', ['docCode'=>$countAdjHdrModel->doc_code]));
                    $exc->addData(\App\CountAdjHdr::class, $countAdjHdrModel->id);
                    throw $exc;
                }
                $countAdjHdrModel = RepositoryUtils::dataToModel($countAdjHdrModel, $countAdjHdrData);
                $countAdjHdrModel->save();

                //update countAdjDtl array
                $countAdjDtlModels = array();
                foreach($countAdjDtlArray as $countAdjDtlData)
                {
                    $countAdjDtlModel = null;
                    if (strpos($countAdjDtlData['id'], 'NEW') !== false) 
                    {
                        $uuid = $countAdjDtlData['id'];
                        $countAdjDtlModel = new CountAdjDtl;
                        unset($countAdjDtlData['id']);
                    }
                    else
                    {
                        $countAdjDtlModel = CountAdjDtl::lockForUpdate()->find($countAdjDtlData['id']);
                    }
                    
                    $countAdjDtlModel = RepositoryUtils::dataToModel($countAdjDtlModel, $countAdjDtlData);
                    $countAdjDtlModel->hdr_id = $countAdjHdrModel->id;
                    $countAdjDtlModel->save();
                    $countAdjDtlModels[] = $countAdjDtlModel;
                }

                if(!empty($delCountAdjDtlIds))
                {
                    CountAdjDtl::destroy($delCountAdjDtlIds);
                }

                return array(
                    'hdrModel' => $countAdjHdrModel,
                    'dtlModels' => $countAdjDtlModels
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function createHeader($procType, $docNoId, $hdrData) 
	{
        $hdrModel = DB::transaction
        (
            function() use ($procType, $docNoId, $hdrData)
            {
                $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

                $hdrModel = new CountAdjHdr;
                $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
                $hdrModel->doc_code = $newCode;
                $hdrModel->proc_type = $procType;
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }
}