<?php

namespace App\Repositories;

use App\Services\Utils\RepositoryUtils;
use App\OutbOrdHdr;
use App\OutbOrdCDtl;
use App\OutbOrdDtl;
use App\SlsOrdHdr;
use App\DocTxnFlow;
use App\DocTxnVoid;
use App\PickListOutbOrd;
use App\SiteFlow;
use App\Services\Env\ProcType;
use App\Services\Env\DocStatus;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\DB;

class OutbOrdHdrRepository 
{
    static public function findAll($divisionId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $leftJoinHash = array();
        $outbOrdHdrs = OutbOrdHdr::select('outb_ord_hdrs.*')
            ->where('division_id', $divisionId);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $outbOrdHdrs->where('outb_ord_hdrs.doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $outbOrdHdrs->where(function($q) use($filter) {
                    $q->where('delivery_points.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('delivery_points.company_name_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'salesman') == 0)
            {
                $leftJoinHash['users'] = 'users';
                $outbOrdHdrs->where('users.username', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point_area') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $leftJoinHash['areas'] = 'areas';
                $outbOrdHdrs->where(function($q) use($filter) {
                    $q->where('areas.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('areas.desc_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'status') == 0)
            {
                if($filter['value'] > 0)
                {
                    $outbOrdHdrs->where('outb_ord_hdrs.doc_status', $filter['value']);
                }
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $outbOrdHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'delivery_points') == 0)
            {
                $outbOrdHdrs->leftJoin('delivery_points', 'delivery_points.id', '=', 'outb_ord_hdrs.delivery_point_id');
            }
            if(strcmp($field, 'users') == 0)
            {
                $outbOrdHdrs->leftJoin('users', 'users.id', '=', 'outb_ord_hdrs.salesman_id');
            }
            if(strcmp($field, 'areas') == 0)
            {
                $outbOrdHdrs->leftJoin('areas', 'areas.id', '=', 'delivery_points.area_id');
            }
        }
        
        if($pageSize > 0)
        {
            return $outbOrdHdrs
                ->paginate($pageSize);
        }
        else
        {
            return $outbOrdHdrs
                ->paginate(PHP_INT_MAX);
        }
    }

    static public function txnFindByPk($hdrId) 
	{
        $model = DB::transaction
        (
            function() use ($hdrId)
            {
                $outbOrdHdr = OutbOrdHdr::where('id', $hdrId)
                    ->first();
                return $outbOrdHdr;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function queryRsvdUnitQtyBySiteFlowId($companyId, $itemId, $siteFlowId, $locationId) 
	{
        $rsvdUnitQty = 0;
        $outbOrdDtls = OutbOrdDtl::selectRaw('IFNULL(SUM(outb_ord_dtls.qty * outb_ord_dtls.uom_rate),0) as rsvd_unit_qty')
            ->leftJoin('outb_ord_hdrs', 'outb_ord_hdrs.id', '=', 'outb_ord_dtls.hdr_id')
            ->where('outb_ord_dtls.item_id', $itemId)
            ->where('outb_ord_dtls.location_id', $locationId)
            ->whereNotExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('doc_txn_flows')
                        ->where('doc_txn_flows.proc_type', ProcType::$MAP['PICK_LIST_01'])
                        ->where('doc_txn_flows.fr_doc_hdr_type', \App\OutbOrdHdr::class)
                        ->whereRaw('doc_txn_flows.fr_doc_hdr_id = outb_ord_hdrs.id')
                        ->where('doc_txn_flows.is_closed', 1);
                })
            ->where('outb_ord_hdrs.site_flow_id', $siteFlowId)
            ->where('outb_ord_hdrs.company_id', $companyId)
            ->where('outb_ord_hdrs.doc_status', DocStatus::$MAP['COMPLETE'])
            ->get();
        foreach($outbOrdDtls as $outbOrdDtl)
        {
            $rsvdUnitQty = $outbOrdDtl->rsvd_unit_qty;
        }
                
        return $rsvdUnitQty;
    }

    static public function findAllNotExistPickList01Txn($divisionIds, $docStatus, $sorts, $filters = array(), $pageSize = 20) 
	{
        $leftJoinHash = array();
        $outbOrdHdrs = OutbOrdHdr::select('outb_ord_hdrs.*')
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('doc_txn_flows')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['PICK_LIST_01'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\OutbOrdHdr::class)
                    ->whereRaw('doc_txn_flows.fr_doc_hdr_id = outb_ord_hdrs.id')
                    ->where('doc_txn_flows.is_closed', 1);
            })
            ->whereIn('outb_ord_hdrs.division_id', $divisionIds)
            ->where('outb_ord_hdrs.doc_status', $docStatus)
            ->where('outb_ord_hdrs.proc_type', ProcType::$MAP['OUTB_ORD_01']);
        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'division') == 0)
            {
                $leftJoinHash['divisions'] = 'divisions';
                $outbOrdHdrs->where('divisions.code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $outbOrdHdrs->where(function($q) use($filter) {
                    $q->where('delivery_points.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('delivery_points.company_name_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'salesman') == 0)
            {
                $leftJoinHash['users'] = 'users';
                $outbOrdHdrs->where('users.username', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point_area') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $leftJoinHash['areas'] = 'areas';
                $outbOrdHdrs->where(function($q) use($filter) {
                    $q->where('areas.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('areas.desc_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $outbOrdHdrs->where('doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'division_code') == 0)
            {
                $leftJoinHash['divisions'] = 'divisions';
                $outbOrdHdrs->orderBy('divisions.code', $sort['order']);
            }
            if(strcmp($sort['field'], 'delivery_point_area_code') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $outbOrdHdrs->orderBy('delivery_points.area_code', $sort['order']);
            }
            if(strcmp($sort['field'], 'delivery_point_code') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $outbOrdHdrs->orderBy('delivery_points.code', $sort['order']);
            }
            if(strcmp($sort['field'], 'salesman_username') == 0)
            {
                $leftJoinHash['users'] = 'users';
                $outbOrdHdrs->orderBy('users.username', $sort['order']);
            }
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $outbOrdHdrs->orderBy('outb_ord_hdrs.doc_code', $sort['order']);
            }
            if(strcmp($sort['field'], 'doc_date') == 0)
            {
                $outbOrdHdrs->orderBy('outb_ord_hdrs.doc_date', $sort['order']);
            }
            if(strcmp($sort['field'], 'est_del_date') == 0)
            {
                $outbOrdHdrs->orderBy('outb_ord_hdrs.est_del_date', $sort['order']);
            }
            if(strcmp($sort['field'], 'net_amt') == 0)
            {
                $outbOrdHdrs->orderBy('outb_ord_hdrs.net_amt', $sort['order']);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'divisions') == 0)
            {
                $outbOrdHdrs->leftJoin('divisions', 'divisions.id', '=', 'outb_ord_hdrs.division_id');
            }
            if(strcmp($field, 'delivery_points') == 0)
            {
                $outbOrdHdrs->leftJoin('delivery_points', 'delivery_points.id', '=', 'outb_ord_hdrs.delivery_point_id');
            }
            if(strcmp($field, 'users') == 0)
            {
                $outbOrdHdrs->leftJoin('users', 'users.id', '=', 'outb_ord_hdrs.salesman_id');
            }
            if(strcmp($field, 'areas') == 0)
            {
                $outbOrdHdrs->leftJoin('areas', 'areas.id', '=', 'delivery_points.area_id');
            }
        }

        return $outbOrdHdrs
            ->paginate($pageSize);
    }

    static public function findAllNotExistPrfOrFailDel01Txn($siteFlowId, $docStatus, $loadListStatus = 100, $sorts, $filters = array(), $pageSize = 20) 
	{
        $notExistProcTypes = array();
        $notExistProcTypes[] = ProcType::$MAP['PRF_DEL_01'];
        $notExistProcTypes[] = ProcType::$MAP['FAIL_DEL_01'];
        $outbOrdHdrs = OutbOrdHdr::select('outb_ord_hdrs.*')
            ->whereNotExists(function ($query) use ($notExistProcTypes) {
                $query->select(DB::raw(1))
                    ->from('doc_txn_flows')
                    ->whereIn('doc_txn_flows.proc_type', $notExistProcTypes)
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\OutbOrdHdr::class)
                    ->whereRaw('doc_txn_flows.fr_doc_hdr_id = outb_ord_hdrs.id')
                    ->where('doc_txn_flows.is_closed', 1);
            })
            ->when($loadListStatus > 0, function ($query) use ($loadListStatus) {
                return $query->whereExists(function ($query) use ($loadListStatus) {
                    $query->select(DB::raw(1))
                        ->from('load_list_dtls')
                        ->leftJoin('load_list_hdrs', 'load_list_hdrs.id', '=', 'load_list_dtls.hdr_id')
                        ->where('load_list_hdrs.doc_status', $loadListStatus)
                        ->whereRaw('load_list_dtls.outb_ord_hdr_id = outb_ord_hdrs.id');
                });
            })
            ->where('site_flow_id', $siteFlowId)
            ->where('doc_status', $docStatus);
        foreach($filters as $key => $value)
        {
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $outbOrdHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        return $outbOrdHdrs
            ->paginate($pageSize);
            //->toSql();
    }

    static public function findAllNotExistLoadList01Txn($siteFlowId, $docStatus, $sorts, $filters = array(), $pageSize = 20) 
	{
        $outbOrdHdrs = OutbOrdHdr::select('outb_ord_hdrs.*')
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('doc_txn_flows')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['LOAD_LIST_01'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\OutbOrdHdr::class)
                    ->whereRaw('doc_txn_flows.fr_doc_hdr_id = outb_ord_hdrs.id')
                    ->where('doc_txn_flows.is_closed', 1);
            })
            ->whereIn('proc_type', array(ProcType::$MAP['OUTB_ORD_01']))
            ->where('site_flow_id', $siteFlowId)
            ->where('doc_status', $docStatus)
            ->where(function ($query) {
                return $query->orWhereRaw('del_ord_hdr_id > 0')
                    ->orWhereRaw('sls_inv_hdr_id > 0')
                    ->orWhereRaw('sls_rtn_hdr_id > 0');
            });

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $outbOrdHdrs->orderBy('doc_code', $sort['order']);
            }
        }
        
        return $outbOrdHdrs
            ->paginate($pageSize);
            //->toSql();
    }

    static public function findByPk($hdrId) 
	{
        $outbOrdHdr = OutbOrdHdr::where('id', $hdrId)
            ->first();

        return $outbOrdHdr;
    }

    static public function findAllByIds($hdrIds) 
	{
        $outbOrdHdrs = OutbOrdHdr::whereIn('id', $hdrIds)
            ->get();

        return $outbOrdHdrs;
    }

    static public function updateDetails($outbOrdHdrData, $outbOrdDtlArray, $delOutbOrdDtlArray) 
	{
        foreach($delOutbOrdDtlArray as $delOutbOrdDtlData)
        {
            $delOutbOrdDtlIds[] = $delOutbOrdDtlData['id'];
        }

        //delete detail id
        $delOutbOrdDtlIds = array();
        foreach($delOutbOrdDtlArray as $delOutbOrdDtlData)
        {
            $delOutbOrdDtlIds[] = $delOutbOrdDtlData['id'];
        }
        
        $result = DB::transaction
        (
            function() use ($outbOrdHdrData, $outbOrdDtlArray, $delOutbOrdDtlIds)
            {
                $outbOrdHdrIdHashByUuid = array();
                $outbOrdDtlIdHashByUuid = array();
                //update outbOrdHdr
                $outbOrdHdrModel = OutbOrdHdr::lockForUpdate()->find($outbOrdHdrData['id']);
                if($outbOrdHdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    $exc = new ApiException(__('OutbOrd.doc_status_is_complete', ['docCode'=>$outbOrdHdrModel->doc_code]));
                    $exc->addData(\App\OutbOrdHdr::class, $outbOrdHdrModel->id);
                    throw $exc;
                }
                $outbOrdHdrModel = RepositoryUtils::dataToModel($outbOrdHdrModel, $outbOrdHdrData);
                $outbOrdHdrModel->save();

                //update outbOrdDtl array
                $outbOrdDtlModels = array();
                foreach($outbOrdDtlArray as $outbOrdDtlData)
                {
                    $outbOrdDtlModel = null;
                    if (strpos($outbOrdDtlData['id'], 'NEW') !== false) 
                    {
                        $uuid = $outbOrdDtlData['id'];
                        $outbOrdDtlModel = new OutbOrdDtl;
                        if(array_key_exists($uuid, $outbOrdHdrIdHashByUuid))
                        {
                            $outbOrdDtlModel->outb_ord_hdr_id = $outbOrdHdrIdHashByUuid[$uuid];
                        }
                        if(array_key_exists($uuid, $outbOrdDtlIdHashByUuid))
                        {
                            $outbOrdDtlModel->outb_ord_dtl_id = $outbOrdDtlIdHashByUuid[$uuid];
                        }
                        unset($outbOrdDtlData['id']);
                    }
                    else
                    {
                        $outbOrdDtlModel = OutbOrdDtl::lockForUpdate()->find($outbOrdDtlData['id']);
                    }
                    
                    $outbOrdDtlModel = RepositoryUtils::dataToModel($outbOrdDtlModel, $outbOrdDtlData);
                    $outbOrdDtlModel->hdr_id = $outbOrdHdrModel->id;
                    $outbOrdDtlModel->save();
                    $outbOrdDtlModels[] = $outbOrdDtlModel;
                }

                if(!empty($delOutbOrdDtlIds))
                {
                    OutbOrdDtl::destroy($delOutbOrdDtlIds);
                }

                return array(
                    'hdrModel' => $outbOrdHdrModel,
                    'dtlModels' => $outbOrdDtlModels
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function createHeader($procType, $docNoId, $hdrData) 
	{
        $hdrModel = DB::transaction
        (
            function() use ($procType, $docNoId, $hdrData)
            {
                $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

                $hdrModel = new OutbOrdHdr;
                $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
                $hdrModel->doc_code = $newCode;
                $hdrModel->proc_type = $procType;
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];

                $hdrModel->gross_amt = 0;
                $hdrModel->gross_local_amt = 0;
                $hdrModel->disc_amt = 0;
                $hdrModel->disc_local_amt = 0;
                $hdrModel->tax_amt = 0;
                $hdrModel->tax_local_amt = 0;
                $hdrModel->is_round_adj = 0;
                $hdrModel->round_adj_amt = 0;
                $hdrModel->round_adj_local_amt = 0;
                $hdrModel->net_amt = 0;
                $hdrModel->net_local_amt = 0;
                
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitToComplete($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = OutbOrdHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();
                    
                if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('OutbOrd.doc_status_is_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\OutbOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                if($hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('OutbOrd.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\InbOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }

                $dtlModels = OutbOrdDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->orderBy('line_no')
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('OutbOrd.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\OutbOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
        
                //1) update docStatus to COMPLETE
                $hdrModel->doc_status = DocStatus::$MAP['COMPLETE'];
                $hdrModel->save();
                
                return $hdrModel;              
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function createProcess($pickingCriteriasHashByItemId, $procType, $docNoId, $hdrData, $dtlDataHashByDtlId, $frDocHdrModel, $frDocDtlModelHashByDtlId, $docTxnFlowDataList) 
	{
        if(empty($dtlDataHashByDtlId))
        {
            $exc = new ApiException(__('OutbOrd.item_details_not_found', ['docCode'=>$hdrData['docCode']]));
            //$exc->addData(\App\OutbOrdHdr::class, $hdrData['docCode']);
            throw $exc;
        }

        //get the siteId for quantBal inventory checking
        $siteFlowModel = SiteFlow::where('id', $hdrData['site_flow_id'])->first();
        $hdrModel = DB::transaction
        (
            function() use ($pickingCriteriasHashByItemId, $siteFlowModel, $procType, $docNoId, $hdrData, $dtlDataHashByDtlId, $frDocHdrModel, $frDocDtlModelHashByDtlId, $docTxnFlowDataList)
            {
                //verify the site inventory
                $unitQtyHashByLocationIdAndItemId = array();
                $dtlDataListHashByItemId = array();
                foreach($dtlDataHashByDtlId as $dtlId => $dtlData)
                {
                    $key = $dtlData['location_id'].'/'.$dtlData['item_id'];
                    $dtlUnitQty = bcmul($dtlData['qty'], $dtlData['uom_rate'], 5);

                    $unitQty = 0;
                    if(array_key_exists($key, $unitQtyHashByLocationIdAndItemId))
                    {
                        $unitQty = $unitQtyHashByLocationIdAndItemId[$key];
                    }
                    $unitQty = bcadd($unitQty, $dtlUnitQty, 5);                    
                    $unitQtyHashByLocationIdAndItemId[$key] = $unitQty;

                    $tmpDtlDataList = array();
                    if(array_key_exists($dtlData['item_id'], $dtlDataListHashByItemId))
                    {
                        $tmpDtlDataList = $dtlDataListHashByItemId[$dtlData['item_id']];
                    }
                    $tmpDtlDataList[] = $dtlData;
                    $dtlDataListHashByItemId[$dtlData['item_id']] = $tmpDtlDataList;
                }
                foreach($unitQtyHashByLocationIdAndItemId as $key => $unitQty)
                {
                    $keyParts = explode('/',$key);
                    $locationId = $keyParts[0];
                    $itemId = $keyParts[1];

                    $pickingCriterias = array();
                    if(array_key_exists($itemId, $pickingCriteriasHashByItemId))
                    {
                        $pickingCriterias = $pickingCriteriasHashByItemId[$itemId];
                    }

                    //DB::connection()->enableQueryLog();
                    $availUnitQty = QuantBalRepository::queryAvailUnitQtyBySiteIdAndLocationId(
                        $hdrData['company_id'],
                        $itemId,
                        $siteFlowModel->site_id,
                        $siteFlowModel->id,
                        $locationId,
                        $pickingCriterias
                    );
                    //dd(DB::getQueryLog());

                    //DB::connection()->enableQueryLog();
                    $rsvdUnitQty = self::queryRsvdUnitQtyBySiteFlowId(
                        $hdrData['company_id'], 
                        $itemId, 
                        $siteFlowModel->id,
                        $locationId
                    );
                    //dd(DB::getQueryLog());

                    $availUnitQty = bcsub($availUnitQty, $rsvdUnitQty);

                    if(bccomp($unitQty, $availUnitQty, 5) > 0)
                    {
                        $dtlDataList = $dtlDataListHashByItemId[$itemId];
                        $lineNo = '';
                        $dtlDesc = '';
                        for($a = 0; $a < count($dtlDataList); $a++)
                        {
                            $tmpDtlData = $dtlDataList[$a];
                            if($a == 0)
                            {
                                $lineNo = $tmpDtlData['line_no'];
                            }
                            else
                            {
                                $lineNo = $lineNo.', '.$tmpDtlData['line_no'];
                            }
                            $dtlDesc = $tmpDtlData['desc_01'];
                        }
                        $exc = new ApiException(__('OutbOrd.out_of_stock', [
                            'docCode'=>$frDocHdrModel->doc_code,
                            'lineNo'=>$lineNo, 
                            'desc'=>$dtlDesc, 
                            'reqUnitQty'=>number_format($unitQty, config('scm.decimal_scale')), 
                            'availUnitQty'=>number_format($availUnitQty, config('scm.decimal_scale'))
                        ]));
                        //$exc->addData(get_class($frDocHdrModel), array($frDocHdrModel));
                        throw $exc;
                    }
                }

                $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

                $hdrModel = new OutbOrdHdr;
                $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
                $hdrModel->doc_code = $newCode;
                $hdrModel->proc_type = $procType;
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                //set the frDocHdr to point to this OutbOrd
                $frDocHdrModel->outb_ord_hdr_id = $hdrModel->id;
                $frDocHdrModel->save();

                foreach($dtlDataHashByDtlId as $dtlId => $dtlData)
                {
                    $dtlModel = new OutbOrdDtl;
                    $dtlModel = RepositoryUtils::dataToModel($dtlModel, $dtlData);
                    $dtlModel->hdr_id = $hdrModel->id;
                    $dtlModel->save();

                    //create a carbon copy
                    $cDtlModel = new OutbOrdCDtl;
                    $cDtlModel = RepositoryUtils::dataToModel($cDtlModel, $dtlData);
                    $cDtlModel->hdr_id = $hdrModel->id;
                    $cDtlModel->save();

                    //set the frDocDtl to point to this OutbOrd
                    $frDocDtlModel = $frDocDtlModelHashByDtlId[$dtlId];
                    $frDocDtlModel->outb_ord_hdr_id = $hdrModel->id;
                    $frDocDtlModel->outb_ord_dtl_id = $dtlModel->id;
                    $frDocDtlModel->save();
                }

                //process docTxnFlowDataList
                foreach($docTxnFlowDataList as $docTxnFlowData)
                {
                    $frDocHdrModel = $docTxnFlowData['fr_doc_hdr_type']::where('id', $docTxnFlowData['fr_doc_hdr_id'])
                    ->lockForUpdate()
                    ->first();
                    if($frDocHdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                    {
                        $exc = new ApiException(__('OutbOrd.fr_doc_is_not_complete', ['docType'=>$docTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$docTxnFlowData['fr_doc_hdr_code']]));
                        //$exc->addData($docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_code']);
                        throw $exc;
                    }

                    //verify the frDoc is not closed for this procType
                    $tmpDocTxnFlow = DocTxnFlowRepository::findByProcTypeAndFrHdrId($procType, $docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_id'], 1);
                    if(!empty($tmpDocTxnFlow))
                    {
                        $exc = new ApiException(__('OutbOrd.fr_doc_is_closed', ['docType'=>$docTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$docTxnFlowData['fr_doc_hdr_code']]));
                        //$exc->addData($docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_code']);
                        throw $exc;
                    }

                    $docTxnFlowModel = new DocTxnFlow;
                    $docTxnFlowModel = RepositoryUtils::dataToModel($docTxnFlowModel, $docTxnFlowData);
                    $docTxnFlowModel->proc_type = $procType;
                    $docTxnFlowModel->to_doc_hdr_type = \App\OutbOrdHdr::class;
                    $docTxnFlowModel->to_doc_hdr_id = $hdrModel->id;
                    $docTxnFlowModel->save();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertDraftToVoid($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = OutbOrdHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('OutbOrd.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\OutbOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to VOID
                $hdrModel->doc_status = DocStatus::$MAP['VOID'];
                $hdrModel->save();

                $frDocTxnFlows = $hdrModel->frDocTxnFlows;
                foreach($frDocTxnFlows as $frDocTxnFlow)
                {
                    //move to voidTxn
                    $txnVoidModel = new DocTxnVoid;
                    $txnVoidModel->id = $frDocTxnFlow->id;
                    $txnVoidModel->proc_type = $frDocTxnFlow->proc_type;
                    $txnVoidModel->fr_doc_hdr_type = $frDocTxnFlow->fr_doc_hdr_type;
                    $txnVoidModel->fr_doc_hdr_id = $frDocTxnFlow->fr_doc_hdr_id;
                    $txnVoidModel->fr_doc_hdr_code = $frDocTxnFlow->fr_doc_hdr_code;
                    $txnVoidModel->to_doc_hdr_type = $frDocTxnFlow->to_doc_hdr_type;
                    $txnVoidModel->to_doc_hdr_id = $frDocTxnFlow->to_doc_hdr_id;
                    $txnVoidModel->is_closed = $frDocTxnFlow->is_closed;
                    $txnVoidModel->save();
        
                    //delete the txn
                    $frDocTxnFlow->delete();
                }

                //update the associated sales order also
                $slsOrdHdrModel = SlsOrdHdr::where('outb_ord_hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->first();
                if(!empty($slsOrdHdrModel))
                {
                    $slsOrdHdrModel->outb_ord_hdr_id = 0;
                    $slsOrdHdrModel->save();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertCompleteToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = OutbOrdHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                {
                    $exc = new ApiException(__('OutbOrd.doc_status_is_not_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\OutbOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertWipToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = OutbOrdHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['WIP'])
                {
                    $exc = new ApiException(__('OutbOrd.doc_status_is_not_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\OutbOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitVoidToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = OutbOrdHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['VOID'])
                {
                    //only VOID can transition to DRAFT
                    $exc = new ApiException(__('OutbOrd.doc_status_is_not_void', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\OutbOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                $frDocTxnVoids = $hdrModel->frDocTxnVoids;
                foreach($frDocTxnVoids as $frDocTxnVoid)
                {
                    //verify first
                    $docTxnFlow = DocTxnFlow::where(array(
                        'proc_type' => $frDocTxnVoid->proc_type,
                        'fr_doc_hdr_type' => $frDocTxnVoid->fr_doc_hdr_type,
                        'fr_doc_hdr_id' => $frDocTxnVoid->fr_doc_hdr_id,
                        'to_doc_hdr_type' => $frDocTxnVoid->to_doc_hdr_type
                        ))
                        ->first();
                    if(!empty($docTxnFlow))
                    {
                        $exc = new ApiException(__('OutbOrd.fr_doc_is_closed', ['docCode'=>$frDocTxnVoid->fr_doc_hdr_code]));
                        $exc->addData(\App\OutbOrdHdr::class, $hdrModel->id);
                        throw $exc;
                    }

                    //move to txnFlow
                    $txnFlowModel = new DocTxnFlow;
                    $txnFlowModel->id = $frDocTxnVoid->id;
                    $txnFlowModel->proc_type = $frDocTxnVoid->proc_type;
                    $txnFlowModel->fr_doc_hdr_type = $frDocTxnVoid->fr_doc_hdr_type;
                    $txnFlowModel->fr_doc_hdr_id = $frDocTxnVoid->fr_doc_hdr_id;
                    $txnFlowModel->fr_doc_hdr_code = $frDocTxnVoid->fr_doc_hdr_code;
                    $txnFlowModel->to_doc_hdr_type = $frDocTxnVoid->to_doc_hdr_type;
                    $txnFlowModel->to_doc_hdr_id = $frDocTxnVoid->to_doc_hdr_id;
                    $txnFlowModel->is_closed = $frDocTxnVoid->is_closed;
                    $txnFlowModel->save();
        
                    //delete the txnVoid
                    $frDocTxnVoid->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitToWip($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = OutbOrdHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('OutbOrd.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\OutbOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }

                $dtlModels = OutbOrdDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->orderBy('line_no')
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('OutbOrd.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\OutbOrdHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to WIP
                $hdrModel->doc_status = DocStatus::$MAP['WIP'];
                $hdrModel->save();
                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function findAllByPickListHdrIdAndItemId($pickListHdrId, $itemId) 
	{
        $outbOrdHdrs = OutbOrdHdr::select('outb_ord_hdrs.*')
            ->leftJoin('pick_list_outb_ords', 'outb_ord_hdrs.id', '=', 'pick_list_outb_ords.outb_ord_hdr_id')
            ->where('pick_list_outb_ords.hdr_id', $pickListHdrId)
            ->whereExists(function ($query) use ($itemId) {
                $query->select(DB::raw(1))
                    ->from('outb_ord_dtls')
                    ->whereRaw('outb_ord_dtls.hdr_id = outb_ord_hdrs.id')
                    ->where('outb_ord_dtls.item_id', $itemId);
            })
            ->get();

        return $outbOrdHdrs;
    }
}