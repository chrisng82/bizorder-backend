<?php

namespace App\Repositories;

use App\Services\Utils\RepositoryUtils;
use App\BinTrfDtl;
use App\BinTrfHdr;
use App\BinTrfCDtl;
use App\Uom;
use App\DocTxnFlow;
use App\Services\Env\ProcType;
use App\Services\Env\DocStatus;
use Illuminate\Support\Facades\DB;
use App\Services\Utils\ApiException;

class BinTrfHdrRepository 
{
    static public function findByPk($hdrId) 
	{
        $binTrfHdr = BinTrfHdr::where('id', $hdrId)
            ->first();

        return $binTrfHdr;
    }

    static public function txnFindByPk($hdrId) 
	{
        $model = DB::transaction
        (
            function() use ($hdrId)
            {
                $binTrfHdr = BinTrfHdr::where('id', $hdrId)
                    ->first();
                return $binTrfHdr;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function createProcess($procType, $docNoId, $hdrData, $dtlDataList) 
	{
        $binTrfHdr = DB::transaction
        (
            function() use ($procType, $docNoId, $hdrData, $dtlDataList)
            {
                $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

                $hdrModel = new BinTrfHdr;
                $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
                $hdrModel->doc_code = $newCode;
                $hdrModel->proc_type = $procType;
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                foreach($dtlDataList as $dtlData)
                {
                    $dtlModel = new BinTrfDtl;
                    $dtlModel = RepositoryUtils::dataToModel($dtlModel, $dtlData);
                    $dtlModel->hdr_id = $hdrModel->id;
                    $dtlModel->save();

                    //create a carbon copy
                    $cDtlModel = new BinTrfCDtl;
                    $cDtlModel = RepositoryUtils::dataToModel($cDtlModel, $dtlData);
                    $cDtlModel->hdr_id = $hdrModel->id;
                    $cDtlModel->save();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $binTrfHdr;
    }

    static public function findAllBinTrf02($siteFlowId, $docStatuses, $sorts, $filters = array(), $pageSize = 20) 
	{
        $binTrfHdrs = BinTrfHdr::select('bin_trf_hdrs.*')
            ->where('site_flow_id', $siteFlowId)
            ->where('proc_type', ProcType::$MAP['BIN_TRF_02'])
            ->whereIn('doc_status', $docStatuses);

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $binTrfHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        return $binTrfHdrs
            ->paginate($pageSize);
    }

    static public function commitToWip($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = BinTrfHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('BinTrf.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\BinTrfHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to WIP
                $hdrModel->doc_status = DocStatus::$MAP['WIP'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitToComplete($hdrId, $isNonZeroBal)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId, $isNonZeroBal)
            {
                $hdrModel = BinTrfHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();
                    
                if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('BinTrf.doc_status_is_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\BinTrfHdr::class, $hdrModel->id);
                    throw $exc;
                }
                if($hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('BinTrf.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\BinTrfHdr::class, $hdrModel->id);
                    throw $exc;
                }

                $siteFlow = $hdrModel->siteFlow;
        
                //1) update docStatus to COMPLETE
                $hdrModel->doc_status = DocStatus::$MAP['COMPLETE'];
                $hdrModel->save();

                $dtlModels = BinTrfDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('BinTrf.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\BinTrfHdr::class, $hdrModel->id);
                    throw $exc;
                }
                $dtlModels->load('item');
                foreach($dtlModels as $dtlModel)
                {                    
                    if($dtlModel->uom_id == Uom::$PALLET
                    && bccomp($dtlModel->uom_rate, 0, 5) == 0)
                    {
                        //this is pallet(handling unit) transfer
                        $quantBal = null;
                        if($dtlModel->quant_bal_id == 0)
                        {
                            //storage_bin_id and handling_unit_id is compulsory
                            $quantBal = QuantBalRepository::findActiveByStorageBinIdAndHandlingUnitId(
                                $dtlModel->storage_bin_id,
                                $dtlModel->handling_unit_id
                            );
                        }
                        else
                        {
                            $quantBal = $dtlModel->quantBal;
                        }
                        if(empty($quantBal))
                        {
                            $exc = new ApiException(__('BinTrf.pallet_transfer_quant_bal_not_found', ['lineNo'=>$dtlModel->line_no]));
                            $exc->addData(\App\BinTrfDtl::class, $dtlModel->id);
                            throw $exc;
                        }

                        QuantBalRepository::commitHandlingUnitTransfer(
                            $quantBal->storage_bin_id, $quantBal->handling_unit_id, 
                            $dtlModel->to_storage_bin_id, $dtlModel->to_handling_unit_id, 
                            $hdrModel->doc_date, \App\BinTrfHdr::class, $hdrModel->id, $dtlModel->id);
                    }
                    else
                    {
                        //this is stock transfer
                        //item_id is compulsory
                        $item = $dtlModel->item;
                        if(empty($item))
                        {
                            $exc = new ApiException(__('BinTrf.item_is_required', ['lineNo'=>$dtlModel->line_no]));
                            $exc->addData(\App\BinTrfDtl::class, $dtlModel->id);
                            throw $exc;
                        }
                        
                        $quantBal = null;
                        if($dtlModel->quant_bal_id == 0)
                        {
                            $storageBin = $dtlModel->storageBin;

                            //1) search for existing active quants
                            $quantBal = null;
                            $quantBals = QuantBalRepository::findAllActiveByStorageBinIdAndHandlingUnitIdAndItemId(
                                $dtlModel->storage_bin_id,
                                $dtlModel->handling_unit_id,
                                $dtlModel->item_id
                            );
                            $quantBals->load('itemBatch');
                            foreach($quantBals as $tmpQuantBal)
                            {
                                $tmpItemBatch = $tmpQuantBal->itemBatch;
                                if($item->batch_serial_control > 0)
                                {
                                    if(strcmp($tmpItemBatch->batch_serial_no, $dtlModel->batch_serial_no) == 0
                                    && strcmp($tmpItemBatch->expiry_date, $dtlModel->expiry_date) == 0)
                                    {
                                        $quantBal = $tmpQuantBal;
                                    }
                                }
                                else
                                {
                                    if(strcmp($tmpItemBatch->expiry_date, $dtlModel->expiry_date) == 0)
                                    {
                                        $quantBal = $tmpQuantBal;
                                    }
                                }
                            }

                            //2) if fail to search, only create the new Batch or new Quant
                            if(empty($quantBal))
                            {
                                //need to find/create itemBatch
                                $itemBatch = ItemBatchRepository::firstOrCreate(
                                    $item, 
                                    $dtlModel->batch_serial_no, 
                                    $dtlModel->expiry_date, 
                                    $dtlModel->receipt_date
                                );

                                //need to find/create quantBal
                                $quantBal = QuantBalRepository::firstOrCreate(
                                    $siteFlow->site_id, 
                                    $dtlModel->company_id, 
                                    $dtlModel->handling_unit_id, 
                                    $item, 
                                    $itemBatch, 
                                    $storageBin
                                );
                            }
                        }
                        else
                        {
                            $quantBal = $dtlModel->quantBal;
                        }
                        
                        //this is stock transfer
                        QuantBalRepository::commitStockTransfer(
                            $quantBal->storage_bin_id, 
                            $isNonZeroBal, $item, 
                            $quantBal->id, $dtlModel->to_storage_bin_id, $dtlModel->to_handling_unit_id, 
                            $hdrModel->doc_date, \App\BinTrfHdr::class, $hdrModel->id, 
                            $dtlModel->id, $dtlModel->uom_id, $dtlModel->uom_rate, $dtlModel->qty);
                    }
                }
                return $hdrModel;              
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertCompleteToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = BinTrfHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                {
                    $exc = new ApiException(__('BinTrf.doc_status_is_not_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\BinTrfHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                QuantBalRepository::revertStockTransaction(\App\BinTrfHdr::class, $hdrModel->id);
                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertWipToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = BinTrfHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['WIP'])
                {
                    $exc = new ApiException(__('BinTrf.doc_status_is_not_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\BinTrfHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function findAllByIds($ids) 
	{
        $binTrfHdrs = BinTrfHdr::whereIn('id', $ids)
            ->get();

        return $binTrfHdrs;
    }

    static public function findAllNotExistWhseJob1702Txn($siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $whseJob1702TxnFlows = DocTxnFlow::select('doc_txn_flows.fr_doc_hdr_id')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['WHSE_JOB_17_02'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\BinTrfHdr::class)
                    ->where('doc_txn_flows.is_closed', 1)
                    ->distinct();

        $qStatuses = array(DocStatus::$MAP['DRAFT'], DocStatus::$MAP['WIP']); 
        $binTrfHdrs = BinTrfHdr::select('bin_trf_hdrs.*')
            ->leftJoinSub($whseJob1702TxnFlows, 'whse_job_17_02_txn_flows', function ($join) {
                $join->on('bin_trf_hdrs.id', '=', 'whse_job_17_02_txn_flows.fr_doc_hdr_id');
            })
            ->whereNull('whse_job_17_02_txn_flows.fr_doc_hdr_id')
            ->where('site_flow_id', $siteFlowId)
            ->whereIn('doc_status', $qStatuses);

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $binTrfHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        return $binTrfHdrs
            ->paginate($pageSize);
    }

    static public function findAllByHdrIds($hdrIds)
    {
        $binTrfHdrs = BinTrfHdr::whereIn('id', $hdrIds)
            ->orderBy('doc_code', 'ASC')
            ->get();

        return $binTrfHdrs;
    }

    static public function createHeader($procType, $docNoId, $hdrData) 
	{
        $hdrModel = DB::transaction
        (
            function() use ($procType, $docNoId, $hdrData)
            {
                $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

                $hdrModel = new BinTrfHdr;
                $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
                $hdrModel->doc_code = $newCode;
                $hdrModel->proc_type = $procType;
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function updateDetails($binTrfHdrData, $binTrfDtlArray, $delBinTrfDtlArray) 
	{
        $delBinTrfDtlIds = array();
        foreach($delBinTrfDtlArray as $delBinTrfDtlData)
        {
            $delBinTrfDtlIds[] = $delBinTrfDtlData['id'];
        }
        
        $result = DB::transaction
        (
            function() use ($binTrfHdrData, $binTrfDtlArray, $delBinTrfDtlIds)
            {
                //update binTrfHdr
                $binTrfHdrModel = BinTrfHdr::lockForUpdate()->find($binTrfHdrData['id']);
                if($binTrfHdrModel->doc_status >= DocStatus::$MAP['WIP'])
                {
                    //WIP and COMPLETE status can not be updated
                    $exc = new ApiException(__('BinTrf.doc_status_is_wip_or_complete', ['docCode'=>$binTrfHdrModel->doc_code]));
                    $exc->addData(\App\BinTrfHdr::class, $binTrfHdrModel->id);
                    throw $exc;
                }
                $binTrfHdrModel = RepositoryUtils::dataToModel($binTrfHdrModel, $binTrfHdrData);
                $binTrfHdrModel->save();

                //update binTrfDtl array
                $binTrfDtlModels = array();
                foreach($binTrfDtlArray as $binTrfDtlData)
                {
                    $binTrfDtlModel = null;
                    if (strpos($binTrfDtlData['id'], 'NEW') !== false) 
                    {
                        $uuid = $binTrfDtlData['id'];
                        $binTrfDtlModel = new BinTrfDtl;
                        unset($binTrfDtlData['id']);
                    }
                    else
                    {
                        $binTrfDtlModel = BinTrfDtl::lockForUpdate()->find($binTrfDtlData['id']);
                    }
                    
                    $binTrfDtlModel = RepositoryUtils::dataToModel($binTrfDtlModel, $binTrfDtlData);
                    $binTrfDtlModel->hdr_id = $binTrfHdrModel->id;
                    $binTrfDtlModel->save();
                    $binTrfDtlModels[] = $binTrfDtlModel;
                }

                if(!empty($delBinTrfDtlIds))
                {
                    BinTrfDtl::destroy($delBinTrfDtlIds);
                }

                return array(
                    'hdrModel' => $binTrfHdrModel,
                    'dtlModels' => $binTrfDtlModels
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function findAll($siteFlowId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $binTrfHdrs = BinTrfHdr::select('bin_trf_hdrs.*')
            ->where('site_flow_id', $siteFlowId);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $binTrfHdrs->where('bin_trf_hdrs.doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $binTrfHdrs->orderBy('doc_code', $sort['order']);
            }
        }
        
        if($pageSize > 0)
        {
            return $binTrfHdrs
                ->paginate($pageSize);
        }
        else
        {
            return $binTrfHdrs
                ->paginate(PHP_INT_MAX);
        }
    }

    static public function revertDraftToVoid($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = BinTrfHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('BinTrf.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\BinTrfHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to VOID
                $hdrModel->doc_status = DocStatus::$MAP['VOID'];
                $hdrModel->save();

                $frDocTxnFlows = $hdrModel->frDocTxnFlows;
                foreach($frDocTxnFlows as $frDocTxnFlow)
                {
                    //move to voidTxn
                    $txnVoidModel = new DocTxnVoid;
                    $txnVoidModel->id = $frDocTxnFlow->id;
                    $txnVoidModel->proc_type = $frDocTxnFlow->proc_type;
                    $txnVoidModel->fr_doc_hdr_type = $frDocTxnFlow->fr_doc_hdr_type;
                    $txnVoidModel->fr_doc_hdr_id = $frDocTxnFlow->fr_doc_hdr_id;
                    $txnVoidModel->fr_doc_hdr_code = $frDocTxnFlow->fr_doc_hdr_code;
                    $txnVoidModel->to_doc_hdr_type = $frDocTxnFlow->to_doc_hdr_type;
                    $txnVoidModel->to_doc_hdr_id = $frDocTxnFlow->to_doc_hdr_id;
                    $txnVoidModel->is_closed = $frDocTxnFlow->is_closed;
                    $txnVoidModel->save();
        
                    //delete the txn
                    $frDocTxnFlow->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }
}