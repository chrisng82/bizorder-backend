<?php

namespace App\Repositories;

use App\Services\Utils\RepositoryUtils;
use App\SlsInvHdr;
use App\SlsInvDtl;
use App\SlsInvCDtl;
use App\DocTxnFlow;
use App\OutbOrdHdr;
use App\InbOrdHdr;
use App\RtnRcptHdr;
use App\RtnRcptDtl;
use App\RtnRcptCDtl;
use App\Services\Env\DocStatus;
use App\Services\Env\ResType;
use Illuminate\Support\Facades\DB;
use App\Services\Utils\ApiException;

class InvDocHdrRepository 
{
    static protected function createSlsInv($procType, $docNoId, $hdrData, $dtlArray, $frDocTxnFlowData, $outbOrdHdrId)
    {
        $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

        $hdrModel = new SlsInvHdr;
        $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
        $hdrModel->doc_code = $newCode;
        $hdrModel->proc_type = $procType;
        $hdrModel->outb_ord_hdr_id = $outbOrdHdrId;
        $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
        $hdrModel->save();

        foreach($dtlArray as $lineNo => $dtlData)
        {
            $dtlModel = new SlsInvDtl;
            $dtlModel = RepositoryUtils::dataToModel($dtlModel, $dtlData);
            $dtlModel->hdr_id = $hdrModel->id;
            $dtlModel->save();

            //create a carbon copy
            $cDtlModel = new SlsInvCDtl;
            $cDtlModel = RepositoryUtils::dataToModel($cDtlModel, $dtlData);
            $cDtlModel->hdr_id = $hdrModel->id;
            $cDtlModel->save();
        }

        //process frDocTxnFlowData
        /*
        //verify the frDoc is not closed for this procType
        $tmpDocTxnFlow = DocTxnFlowRepository::findByProcTypeAndFrHdrId($procType, $frDocTxnFlowData['fr_doc_hdr_type'], $frDocTxnFlowData['fr_doc_hdr_id'], 1);
        if(!empty($tmpDocTxnFlow))
        {
            dd($tmpDocTxnFlow);
            $exc = new ApiException(__('SlsInv.fr_doc_is_closed', ['docType'=>$frDocTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$frDocTxnFlowData['fr_doc_hdr_code']]));
            $exc->addData($frDocTxnFlowData['fr_doc_hdr_type'], $frDocTxnFlowData['fr_doc_hdr_code']);
            throw $exc;
        }
        */

        $docTxnFlowModel = new DocTxnFlow;
        $docTxnFlowModel = RepositoryUtils::dataToModel($docTxnFlowModel, $frDocTxnFlowData);
        $docTxnFlowModel->proc_type = $procType;
        $docTxnFlowModel->to_doc_hdr_type = \App\SlsInvHdr::class;
        $docTxnFlowModel->to_doc_hdr_id = $hdrModel->id;
        $docTxnFlowModel->save();

        $outbOrdHdr = OutbOrdHdr::lockForUpdate()->find($outbOrdHdrId);
        if($outbOrdHdr->sls_inv_hdr_id > 0)
        {
            $exc = new ApiException(__('OutbOrd.invoice_already_issued', ['docCode'=>$outbOrdHdr->doc_code]));
            $exc->addData(App\OutbOrdHdr::class, $outbOrdHdr->id);
            throw $exc;
        }

        $outbOrdHdr->sls_inv_hdr_id = $hdrModel->id;
        $outbOrdHdr->sls_inv_hdr_code = $hdrModel->doc_code;
        $outbOrdHdr->cur_res_type = ResType::$MAP['SLS_INV'];
        $outbOrdHdr->save();

        return $hdrModel;
    }

    static protected function createDelOrd($procType, $docNoId, $hdrData, $dtlArray, $docTxnFlowData, $outbOrdHdrId)
    {
        $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

        $hdrModel = new DelOrdHdr;
        $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
        $hdrModel->doc_code = $newCode;
        $hdrModel->proc_type = $procType;
        $hdrModel->outb_ord_hdr_id = $outbOrdHdrId;
        $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
        $hdrModel->save();

        foreach($dtlArray as $lineNo => $dtlData)
        {
            $dtlModel = new DelOrdDtl;
            $dtlModel = RepositoryUtils::dataToModel($dtlModel, $dtlData);
            $dtlModel->hdr_id = $hdrModel->id;
            $dtlModel->save();

            //create a carbon copy
            $cDtlModel = new DelOrdCDtl;
            $cDtlModel = RepositoryUtils::dataToModel($cDtlModel, $dtlData);
            $cDtlModel->hdr_id = $hdrModel->id;
            $cDtlModel->save();
        }

        /*
        //process docTxnFlowData
        //verify the frDoc is not closed for this procType
        $tmpDocTxnFlow = DocTxnFlowRepository::findByProcTypeAndFrHdrId($procType, $docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_id'], 1);
        if(!empty($tmpDocTxnFlow))
        {
            $exc = new ApiException(__('DelOrd.fr_doc_is_closed', ['docType'=>$docTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$docTxnFlowData['fr_doc_hdr_code']]));
            $exc->addData($docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_code']);
            throw $exc;
        }
        */
        
        $docTxnFlowModel = new DocTxnFlow;
        $docTxnFlowModel = RepositoryUtils::dataToModel($docTxnFlowModel, $docTxnFlowData);
        $docTxnFlowModel->proc_type = $procType;
        $docTxnFlowModel->to_doc_hdr_type = \App\SlsInvHdr::class;
        $docTxnFlowModel->to_doc_hdr_id = $hdrModel->id;
        $docTxnFlowModel->save();

        $outbOrdHdr = OutbOrdHdr::lockForUpdate()->find($id);
        if($outbOrdHdr->del_ord_hdr_id > 0)
        {
            $exc = new ApiException(__('OutbOrd.delivery_order_already_issued', ['docCode'=>$outbOrdHdr->doc_code]));
            $exc->addData(App\OutbOrdHdr::class, $outbOrdHdr->id);
            throw $exc;
        }

        $outbOrdHdr->del_ord_hdr_id = $hdrModel->id;
        $outbOrdHdr->del_ord_hdr_code = $hdrModel->doc_code;
        $outbOrdHdr->cur_res_type = ResType::$MAP['DEL_ORD'];
        $outbOrdHdr->save();

        return $hdrModel;
    }

    static public function createProcess($procType, $invDocArray, $toDocTxnFlowArray) 
	{
        $invDocHdrArray = DB::transaction
        (
            function() use ($procType, $invDocArray, $toDocTxnFlowArray)
            {
                $invDocHdrArray = array();
                foreach($invDocArray as $invDocInfo)
                {
                    if($invDocInfo['doc_type'] == \App\SlsInvHdr::class)
                    {
                        $invDocHdrModel = self::createSlsInv($procType, $invDocInfo['doc_no_id'], $invDocInfo['hdr_data'], $invDocInfo['dtl_array'], $invDocInfo['doc_txn_flow_data'], $invDocInfo['outb_ord_hdr_id']);
                        $invDocHdrArray[] = $invDocHdrModel;
                    }
                    elseif($invDocInfo['doc_type'] == \App\DelOrdHdr::class)
                    {
                        $invDocHdrModel = self::createDelOrd($procType, $invDocInfo['doc_no_id'], $invDocInfo['hdr_data'], $invDocInfo['dtl_array'], $invDocInfo['doc_txn_flow_data'], $invDocInfo['outb_ord_hdr_id']);
                        $invDocHdrArray[] = $invDocHdrModel;
                    }
                    elseif($invDocInfo['doc_type'] == \App\RtnRcptHdr::class)
                    {
                        $invDocHdrModel = self::createRtnRcpt($procType, $invDocInfo['doc_no_id'], $invDocInfo['hdr_data'], $invDocInfo['dtl_array'], $invDocInfo['doc_txn_flow_data'], $invDocInfo['inb_ord_hdr_id']);
                        $invDocHdrArray[] = $invDocHdrModel;
                    }
                }

                foreach($toDocTxnFlowArray as $toDocTxnFlowData)
                {
                    $docTxnFlowModel = new DocTxnFlow;
                    $docTxnFlowModel = RepositoryUtils::dataToModel($docTxnFlowModel, $toDocTxnFlowData);
                    $docTxnFlowModel->save();
                }

                return $invDocHdrArray;
            }, 
            5 //reattempt times
        );
        return $invDocHdrArray;
    }

    static protected function createRtnRcpt($procType, $docNoId, $hdrData, $dtlArray, $frDocTxnFlowData, $inbOrdHdrId)
    {
        $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

        $hdrModel = new RtnRcptHdr;
        $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
        $hdrModel->doc_code = $newCode;
        $hdrModel->proc_type = $procType;
        $hdrModel->inb_ord_hdr_id = $inbOrdHdrId;
        $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
        $hdrModel->save();

        foreach($dtlArray as $lineNo => $dtlData)
        {
            $dtlModel = new RtnRcptDtl;
            $dtlModel = RepositoryUtils::dataToModel($dtlModel, $dtlData);
            $dtlModel->hdr_id = $hdrModel->id;
            $dtlModel->save();

            //create a carbon copy
            $cDtlModel = new RtnRcptCDtl;
            $cDtlModel = RepositoryUtils::dataToModel($cDtlModel, $dtlData);
            $cDtlModel->hdr_id = $hdrModel->id;
            $cDtlModel->save();
        }

        //process frDocTxnFlowData
        /*
        //verify the frDoc is not closed for this procType
        $tmpDocTxnFlow = DocTxnFlowRepository::findByProcTypeAndFrHdrId($procType, $frDocTxnFlowData['fr_doc_hdr_type'], $frDocTxnFlowData['fr_doc_hdr_id'], 1);
        if(!empty($tmpDocTxnFlow))
        {
            dd($tmpDocTxnFlow);
            $exc = new ApiException(__('SlsInv.fr_doc_is_closed', ['docType'=>$frDocTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$frDocTxnFlowData['fr_doc_hdr_code']]));
            $exc->addData($frDocTxnFlowData['fr_doc_hdr_type'], $frDocTxnFlowData['fr_doc_hdr_code']);
            throw $exc;
        }
        */

        $docTxnFlowModel = new DocTxnFlow;
        $docTxnFlowModel = RepositoryUtils::dataToModel($docTxnFlowModel, $frDocTxnFlowData);
        $docTxnFlowModel->proc_type = $procType;
        $docTxnFlowModel->to_doc_hdr_type = \App\RtnRcptHdr::class;
        $docTxnFlowModel->to_doc_hdr_id = $hdrModel->id;
        $docTxnFlowModel->save();

        $inbOrdHdr = InbOrdHdr::lockForUpdate()->find($inbOrdHdrId);
        if($inbOrdHdr->rtn_rcpt_hdr_id > 0)
        {
            $exc = new ApiException(__('InbOrd.return_receipt_already_issued', ['docCode'=>$inbOrdHdr->doc_code]));
            $exc->addData(App\OutbOrdHdr::class, $outbOrdHdr->id);
            throw $exc;
        }

        $inbOrdHdr->rtn_rcpt_hdr_id = $hdrModel->id;
        $inbOrdHdr->rtn_rcpt_hdr_code = $hdrModel->doc_code;
        $inbOrdHdr->cur_res_type = ResType::$MAP['RTN_RCPT'];
        $inbOrdHdr->save();

        return $hdrModel;
    }
}