<?php

namespace App\Repositories;

use App\Services\Env\ResStatus;
use App\Services\Env\DeliveryPointType;
use App\Services\Utils\RepositoryUtils;
use App\DebtorGroup01;
use App\DebtorGroup02;
use App\DebtorGroup03;
use App\Area;
use App\DeliveryPoint;
use App\Debtor;
use App\Repositories\DebtorRepository;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class DeliveryPointRepository 
{
    static public function findRandomByType($deliveryPointType) 
	{
        $model = DeliveryPoint::where('status', ResStatus::$MAP['ACTIVE'])
            ->where('delivery_point_type', $deliveryPointType)
            ->orderByRaw('RAND()')
            ->first();

        return $model;
    }

    static public function findByPk($id) 
	{
        $deliveryPoint = DeliveryPoint::where('id', $id)
            ->first();

        return $deliveryPoint;
    }

    static public function findByCode($code) 
	{
        $deliveryPoint = DeliveryPoint::where('code', $code)
            ->first();

        return $deliveryPoint;
    }
    
    static public function findAllByDebtorId($debtorId) 
	{
        $deliveryPoints = DeliveryPoint::where('debtor_id', $debtorId)
            ->orderBy('code', 'asc')
            ->get();

        return $deliveryPoints;
    }

    static public function findAll($sorts, $filters = array(), $pageSize = 20) 
	{
        $deliveryPoints = DeliveryPoint::select('delivery_points.*');

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'debtor_id') == 0)
            {
                $deliveryPoints->where('delivery_points.debtor_id', $filter['value']);
            }
            if(strcmp($filter['field'], 'code') == 0)
            {
                $cartHdrs->where('delivery_points.code', 'LIKE', '%'.$filter['value'].'%');
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'code') == 0)
            {
                $deliveryPoints->orderBy('code', $sort['order']);
            }
        }

        if($pageSize > 0)
        {
            return $deliveryPoints
            ->paginate($pageSize);
        }
        else
        {
            return $deliveryPoints
            ->paginate(PHP_INT_MAX);
        }
    }

    static protected function syncDebtorSync01($data)
    {
        $code = $data['code'];
        if(!empty($data['parent_code']))
        {
            $code = $data['parent_code'];
        }
        $debtorModel = Debtor::firstOrNew(['code' => $code]);
        if(empty($data['parent_code']))
        {
            //update the full details if this is parent record
            $debtorModel->ref_code_01 = $data['ref_code'];
            $debtorModel->company_name_01 = $data['company_name'];
            $debtorModel->company_name_02 = $data['company_name_01'];
            $debtorModel->cmpy_register_no = $data['registration_no'];
            $debtorModel->tax_register_no = $data['gst_reg_no'];
            $debtorModel->area_id = 0;
            $debtorModel->area_code = '';

            $debtorModel->unit_no = $data['address_01'];
            $debtorModel->building_name = $data['address_02'];
            $debtorModel->street_name = $data['address_03'];
            $debtorModel->district_01 = $data['address_04'];
            $debtorModel->district_02 = '';
            $debtorModel->postcode = $data['postcode'];
            $debtorModel->state_name = '';
            $debtorModel->country_name = '';
            $debtorModel->attention = $data['attention'];
            $debtorModel->phone_01 = $data['phone_01'];
            $debtorModel->phone_02 = $data['phone_02'];
            $debtorModel->fax_01 = $data['fax_01'];
            $debtorModel->fax_02 = $data['fax_02'];
            $debtorModel->email_01 = $data['email'];
            $debtorModel->email_02 = '';
            $debtorModel->status = strcmp($data['status'], '1') == 0 ? ResStatus::$MAP['ACTIVE'] : ResStatus::$MAP['INACTIVE'];
        }
        $debtorModel->save();
        
        return $debtorModel;
    }

    static public function syncDeliveryPointSync01($data) 
	{
        $deliveryPointModel = DB::transaction
        (
            function() use ($data)
            {
                //process debtor
                $debtorModel = self::syncDebtorSync01($data);

                $deliveryPointModel = DeliveryPoint::firstOrNew(['code' => $data['code']]);
                $deliveryPointModel->ref_code_01 = $data['ref_code'];
                $deliveryPointModel->company_name_01 = $data['company_name'];
                $deliveryPointModel->company_name_02 = $data['company_name_01'];

                if($deliveryPointModel->id > 0)
                {
                    //update record
                }
                else
                {
                    //insert record
                    //process the debtor_group_code
                    $attributes = array('code'=>$data['customer_category_code']);
                    $values = array('desc_01'=>$data['customer_category_description']);
                    $debtorGroup01Model = DebtorGroup01::firstOrCreate($attributes, $values);

                    $attributes = array('code'=>$data['customer_category1_code']);
                    $values = array('desc_01'=>$data['customer_category1_description']);
                    $debtorGroup02Model = DebtorGroup02::firstOrCreate($attributes, $values);

                    $attributes = array('code'=>$data['customer_category2_code']);
                    $values = array('desc_01'=>$data['customer_category2_description']);
                    $debtorGroup03Model = DebtorGroup03::firstOrCreate($attributes, $values);

                    //process the area_code
                    $attributes = array('code'=>$data['area_code']);
                    $values = array('desc_01'=>$data['area_description']);
                    $areaModel = Area::firstOrCreate($attributes, $values);

                    //default value for EfiChain
                    $deliveryPointModel->delivery_point_type = DeliveryPointType::$MAP['DEBTOR'];
                    $deliveryPointModel->debtor_id = $debtorModel->id;
                    $deliveryPointModel->debtor_group_01_id = $debtorGroup01Model->id;
                    $deliveryPointModel->debtor_group_02_id = $debtorGroup02Model->id;
                    $deliveryPointModel->debtor_group_03_id = $debtorGroup03Model->id;
                    $deliveryPointModel->area_id = $areaModel->id;
                    $deliveryPointModel->area_code = $areaModel->code;
                    
                    $deliveryPointModel->unit_no = $data['del_address_01'];
                    $deliveryPointModel->building_name = $data['del_address_02'];
                    $deliveryPointModel->street_name = $data['del_address_03'];
                    $deliveryPointModel->district_01 = $data['del_address_04'];
                    $deliveryPointModel->district_02 = '';
                    $deliveryPointModel->postcode = $data['del_postcode'];
                    $deliveryPointModel->state_name = '';
                    $deliveryPointModel->country_name = '';
                    $deliveryPointModel->attention = $data['del_attention'];
                    $deliveryPointModel->phone_01 = $data['del_phone_01'];
                    $deliveryPointModel->phone_02 = $data['del_phone_02'];
                    $deliveryPointModel->fax_01 = $data['del_fax_01'];
                    $deliveryPointModel->fax_02 = $data['del_fax_02'];
                    $deliveryPointModel->email_01 = $data['email'];
                    $deliveryPointModel->email_02 = '';
                    $deliveryPointModel->status = strcmp($data['status'], '1') == 0 ? ResStatus::$MAP['ACTIVE'] : ResStatus::$MAP['INACTIVE'];

                    //process divisions
                    foreach($data['details'] as $itemUomData)
                    {
                        
                    }

                    $deliveryPointModel->save();
                }
                return $deliveryPointModel;
            }, 
            5 //reattempt times
        );
        return $deliveryPointModel;
    }

    static public function syncSlsOrdSync01($division, $data) 
	{
        $model = DB::transaction
        (
            function() use ($division, $data)
            {
                //check area
                $area = Area::where('code', $data['area_code'])
                    ->first();
                if(empty($area))
                {
                    $area = new Area();
                    $area->code = $data['area_code'];
                    $area->desc_01 = $data['area_description'];
                    $area->save();
                }

                //check customer_category_code
                $debtorGroup01 = DebtorGroup01::where('code', $data['customer_category_code'])
                    ->first();
                if(empty($debtorGroup01))
                {
                    $debtorGroup01 = new DebtorGroup01();
                    $debtorGroup01->code = $data['customer_category_code'];
                    $debtorGroup01->desc_01 = $data['customer_category_code'];
                    $debtorGroup01->save();
                }

                //check customer_category_01_code
                $debtorGroup02 = DebtorGroup02::where('code', $data['customer_category_01_code'])
                    ->first();
                if(empty($debtorGroup02))
                {
                    $debtorGroup02 = new DebtorGroup02();
                    $debtorGroup02->code = $data['customer_category_01_code'];
                    $debtorGroup02->desc_01 = $data['customer_category_01_code'];
                    $debtorGroup02->save();
                }

                //check customer_category_02_code
                $debtorGroup03 = DebtorGroup03::where('code', $data['customer_category_02_code'])
                    ->first();
                if(empty($debtorGroup03))
                {
                    $debtorGroup03 = new DebtorGroup03();
                    $debtorGroup03->code = $data['customer_category_02_code'];
                    $debtorGroup03->desc_01 = $data['customer_category_02_code'];
                    $debtorGroup03->save();
                }

                //check debtor
                $debtor = Debtor::where('code', $data['customer_code'])
                    ->first();
                if(empty($debtor))
                {
                    $debtor = new Debtor;
                    $debtor->code = $data['customer_code'];
                    $debtor->ref_code_01 = $data['customer_ref_code'];
                    $debtor->company_name_01 = $data['customer_name'];
                    $debtor->company_name_02 = $data['customer_name_01'];
                    $debtor->cmpy_register_no = $data['customer_registration_no'];
                    $debtor->tax_register_no = $data['customer_gst_reg_no'];
                    $debtor->area_id = $area->id;
                    $debtor->area_code = $area->code;
                    $debtor->unit_no = '';
                    $debtor->building_name = $data['bill_address_01'];
                    $debtor->street_name = $data['bill_address_02'];
                    $debtor->district_01 = $data['bill_address_03'];
                    $debtor->district_02 = $data['bill_address_04'];
                    $debtor->postcode = $data['bill_postcode'];
                    $debtor->state_name = '';
                    $debtor->country_name = '';
                    $debtor->attention = $data['bill_attention'];
                    $debtor->phone_01 = $data['bill_phone_01'];
                    $debtor->phone_02 = $data['bill_phone_02'];
                    $debtor->fax_01 = $data['bill_fax_01'];
                    $debtor->fax_02 = $data['bill_fax_01'];
                    $debtor->email_01 = $data['bill_email'];
                    $debtor->email_02 = '';
                    $debtor->save();
                }
                else
                {
                    /*
                    $debtor->company_name_01 = $data['customer_name'];
                    $debtor->company_name_02 = $data['customer_name_01'];
                    $debtor->save();
                    */
                }

                $deliveryPointCode = $data['customer_code'];
                if(intval($data['branch_id']) > 0)
                {
                    $deliveryPointCode = $data['customer_code'].'|'.$data['branch_id'];
                }
                //check deliveryPoint
                $deliveryPoint = DeliveryPoint::where('code', $deliveryPointCode)
                    ->first();
                if(empty($deliveryPoint))
                {
                    $deliveryPoint = new DeliveryPoint;
                    $deliveryPoint->delivery_point_type = DeliveryPointType::$MAP['DEBTOR'];
                    $deliveryPoint->debtor_id = $debtor->id;
                    $deliveryPoint->price_group_id = 0;
                    $deliveryPoint->code = $deliveryPointCode;
                    $deliveryPoint->ref_code_01 = $data['customer_ref_code'];
                    $deliveryPoint->company_name_01 = $data['customer_name'];
                    $deliveryPoint->company_name_02 = $data['customer_name_01'];
                    $deliveryPoint->area_id = $area->id;
                    $deliveryPoint->area_code = $area->code;
                    $deliveryPoint->debtor_group_01_id = $debtorGroup01->id;
                    $deliveryPoint->debtor_group_02_id = $debtorGroup02->id;
                    $deliveryPoint->debtor_group_03_id = $debtorGroup03->id;
                    $deliveryPoint->unit_no = '';
                    $deliveryPoint->building_name = $data['del_address_01'];
                    $deliveryPoint->street_name = $data['del_address_02'];
                    $deliveryPoint->district_01 = $data['del_address_03'];
                    $deliveryPoint->district_02 = $data['del_address_04'];
                    $deliveryPoint->postcode = $data['del_postcode'];
                    $deliveryPoint->state_name = '';
                    $deliveryPoint->country_name = '';
                    $deliveryPoint->attention = $data['del_attention'];
                    $deliveryPoint->phone_01 = $data['del_phone_01'];
                    $deliveryPoint->phone_02 = $data['del_phone_02'];
                    $deliveryPoint->fax_01 = $data['del_fax_01'];
                    $deliveryPoint->fax_02 = $data['del_fax_01'];
                    $deliveryPoint->email_01 = '';
                    $deliveryPoint->email_02 = '';
                    $deliveryPoint->status = ResStatus::$MAP['ACTIVE'];
                    $deliveryPoint->save();
                }
                else
                {
                    /*
                    $deliveryPoint->company_name_01 = $data['customer_name'];
                    $deliveryPoint->company_name_02 = $data['customer_name_01'];
                    $deliveryPoint->save();
                    */
                    $deliveryPoint->area_id = $area->id;
                    $deliveryPoint->area_code = $area->code;
                    $deliveryPoint->debtor_group_01_id = $debtorGroup01->id;
                    $deliveryPoint->debtor_group_02_id = $debtorGroup02->id;
                    $deliveryPoint->debtor_group_03_id = $debtorGroup03->id;
                    $deliveryPoint->save();
                }

                return $deliveryPoint;
            }
        );
        return $model;
    }

    static public function select2($search, $filters, $pageSize = 10) 
	{
        $deliveryPoints = DeliveryPoint::select('delivery_points.*')
        ->where(function($q) use($search) {
            $q->orWhere('company_name_01', 'LIKE', '%' . $search . '%')
            ->orWhere('company_name_02', 'LIKE', '%' . $search . '%')
            ->orWhere('code', 'LIKE', '%' . $search . '%');
        });

        return $deliveryPoints
            ->orderBy('code', 'ASC')
            ->paginate($pageSize);
    }
    
    static public function createModel($data) 
	{
        $model = DB::transaction
        (
            function() use ($data)
            {
                $model = new DeliveryPoint;
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return $model;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function updateModel($data) 
	{
        $result = DB::transaction
        (
            function() use ($data)
            {
                //update Item
                $model = DeliveryPoint::lockForUpdate()->find($data['id']);
                $model = RepositoryUtils::dataToModel($model, $data);
                $model->save();

                return array(
                    'model' => $model
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function deleteModel($deliveryPointId) 
	{
        $result = DB::transaction
        (
            function() use ($deliveryPointId)
            {
                //update Item
                $model = DeliveryPoint::where('id', '=', $deliveryPointId)
                            ->where('debtor_id', '=', Auth::user()->debtor_id)
                            ->first();
                if(!is_null($model)){
                    DeliveryPoint::destroy($deliveryPointId);
                } else {
                    throw new ApiException($message = "Unauthorized action.", $code = 403);
                }

                return array(
                    'model' => $model
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }
}