<?php

namespace App\Repositories;

use App\CountAdjDtl;
use App\CountAdjHdr;

class CountAdjDtlRepository 
{
    static public function findAllByHdrId($hdrId) 
	{
        $countAdjDtls = CountAdjDtl::where('hdr_id', $hdrId)
            ->orderBy('line_no')
            ->get();

        return $countAdjDtls;
    }

    static public function findAllByHdrIds($hdrIds) 
	{
        $countAdjDtls = CountAdjDtl::whereIn('hdr_id', $hdrIds)
            ->get();

        return $countAdjDtls;
    }

    static public function findAllByIds($ids) 
	{
        $countAdjDtls = CountAdjDtl::whereIn('id', $ids)
            ->get();

        return $countAdjDtls;
    }
}