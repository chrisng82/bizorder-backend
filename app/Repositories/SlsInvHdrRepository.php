<?php

namespace App\Repositories;

use App\Services\Utils\RepositoryUtils;
use App\SlsInvHdr;
use App\SlsInvDtl;
use App\SlsOrdHdr;
use App\DocTxnFlow;
use App\Services\Env\DocStatus;
use App\Services\Env\ProcType;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\DB;

class SlsInvHdrRepository 
{
    static public function findByPk($hdrId) 
	{
        $slsInvHdr = SlsInvHdr::where('id', $hdrId)
            ->first();

        return $slsInvHdr;
    }

    static public function createDocTxnFlow($procType, $toSlsInvHdrId, $docTxnFlowData) 
	{
        $docTxnFlowModel = new DocTxnFlow;
        $docTxnFlowModel->proc_type = $procType;
        $docTxnFlowModel->to_doc_hdr_type = \App\SlsInvHdr::class;
        $docTxnFlowModel->to_doc_hdr_id = $toSlsInvHdrId;
        $docTxnFlowModel->save();
    }

    static public function createProcess($procType, $docNoId, $hdrData, $dtlDataList, $docTxnFlowDataList) 
	{
        $slsInvHdr = DB::transaction
        (
            function() use ($procType, $docNoId, $hdrData, $dtlDataList, $docTxnFlowDataList)
            {
                $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

                $hdrModel = new SlsInvHdr;
                $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
                $hdrModel->doc_code = $newCode;
                $hdrModel->proc_type = $procType;
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                $txnHdrModelHash = array();
                foreach($dtlDataList as $lineNo => $dtlData)
                {
                    $dtlModel = new SlsInvDtl;
                    $dtlModel = RepositoryUtils::dataToModel($dtlModel, $dtlData);
                    $dtlModel->hdr_id = $hdrModel->id;
                    $dtlModel->save();

                    //create a carbon copy
                    $cDtlModel = new SlsInvCDtl;
                    $cDtlModel = RepositoryUtils::dataToModel($cDtlModel, $dtlData);
                    $cDtlModel->hdr_id = $hdrModel->id;
                    $cDtlModel->save();
                }

                //process docTxnFlowDataList
                foreach($docTxnFlowDataList as $docTxnFlowData)
                {
                    $frDocHdrModel = $docTxnFlowData['fr_doc_hdr_type']::where('id', $docTxnFlowData['fr_doc_hdr_id'])
                    ->lockForUpdate()
                    ->first();
                    if($frDocHdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                    {
                        $exc = new ApiException(__('SlsInv.fr_doc_is_not_complete', ['docType'=>$docTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$docTxnFlowData['fr_doc_hdr_code']]));
                        $exc->addData($docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_code']);
                        throw $exc;
                    }

                    //verify the frDoc is not closed for this procType
                    $tmpDocTxnFlow = DocTxnFlowRepository::findByProcTypeAndFrHdrId($procType, $docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_id']);
                    if(!empty($tmpDocTxnFlow))
                    {
                        $exc = new ApiException(__('SlsInv.fr_doc_is_closed', ['docType'=>$docTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$docTxnFlowData['fr_doc_hdr_code']]));
                        $exc->addData($docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_code']);
                        throw $exc;
                    }
                    
                    $docTxnFlowModel = new DocTxnFlow;
                    $docTxnFlowModel = RepositoryUtils::dataToModel($docTxnFlowModel, $docTxnFlowData);
                    $docTxnFlowModel->proc_type = $procType;
                    $docTxnFlowModel->to_doc_hdr_type = \App\SlsInvHdr::class;
                    $docTxnFlowModel->to_doc_hdr_id = $hdrModel->id;
                    $docTxnFlowModel->save();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $slsInvHdr;
    }

    static public function txnFindByPk($hdrId) 
	{
        $model = DB::transaction
        (
            function() use ($hdrId)
            {
                $slsInvHdr = SlsInvHdr::where('id', $hdrId)
                    ->first();
                return $slsInvHdr;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function commitToComplete($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = SlsInvHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();
                    
                if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('SlsInv.doc_status_is_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsInvHdr::class, $hdrModel->id);
                    throw $exc;
                }
                if($hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('SlsInv.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsInvHdr::class, $hdrModel->id);
                    throw $exc;
                }

                $dtlModels = SlsInvDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->orderBy('line_no')
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('SlsInv.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsInvHdr::class, $hdrModel->id);
                    throw $exc;
                }
        
                //1) update docStatus to COMPLETE
                $hdrModel->doc_status = DocStatus::$MAP['COMPLETE'];
                $hdrModel->save();

                return $hdrModel;              
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitToWip($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = SlsInvHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('SlsInv.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsInvHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                $dtlModels = SlsInvDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->orderBy('line_no')
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('SlsInv.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsInvHdr::class, $hdrModel->id);
                    throw $exc;
                }

                //1) update docStatus to WIP
                $hdrModel->doc_status = DocStatus::$MAP['WIP'];
                $hdrModel->save();
                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertCompleteToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = SlsInvHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                {
                    $exc = new ApiException(__('SlsInv.doc_status_is_not_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsInvHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertWipToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = SlsInvHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['WIP'])
                {
                    $exc = new ApiException(__('SlsInv.doc_status_is_not_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsInvHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function updateDetails($slsInvHdrData, $slsInvDtlArray, $delSlsInvDtlArray, $outbOrdHdrData, $outbOrdDtlArray, $delOutbOrdDtlArray) 
	{
        $delSlsInvDtlIds = array();
        foreach($delSlsInvDtlArray as $delSlsInvDtlData)
        {
            $delSlsInvDtlIds[] = $delSlsInvDtlData['id'];
        }

        $delOutbOrdDtlIds = array();
        foreach($delOutbOrdDtlArray as $delOutbOrdDtlData)
        {
            $delOutbOrdDtlIds[] = $delOutbOrdDtlData['id'];
        }
        
        $result = DB::transaction
        (
            function() use ($slsInvHdrData, $slsInvDtlArray, $delSlsInvDtlIds, $outbOrdHdrData, $outbOrdDtlArray, $delOutbOrdDtlIds)
            {
                //update outbOrdHdr
                $outbOrdHdrIdHashByUuid = array();
                $outbOrdDtlIdHashByUuid = array();
                if(!empty($outbOrdHdrData))
                {
                    $outbOrdHdrModel = OutbOrdHdr::lockForUpdate()->find($outbOrdHdrData['id']);
                    if($outbOrdHdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                    {
                        //only DRAFT or above can transition to COMPLETE
                        $exc = new ApiException(__('OutbOrd.doc_status_is_complete', ['docCode'=>$outbOrdHdrModel->doc_code]));
                        $exc->addData(\App\OutbOrdHdr::class, $outbOrdHdrModel->id);
                        throw $exc;
                    }
                    $outbOrdHdrModel = RepositoryUtils::dataToModel($outbOrdHdrModel, $outbOrdHdrData);
                    $outbOrdHdrModel->save();

                    //update outbOrdDtl array
                    foreach($outbOrdDtlArray as $outbOrdDtlData)
                    {
                        $uuid = '';
                        $outbOrdDtlModel = null;
                        if (strpos($outbOrdDtlData['id'], 'NEW') !== false) 
                        {
                            $uuid = $outbOrdDtlData['id'];
                            unset($outbOrdDtlData['id']);
                            $outbOrdDtlModel = new OutbOrdDtl;
                        }
                        else
                        {
                            $outbOrdDtlModel = OutbOrdDtl::lockForUpdate()->find($outbOrdDtlModel['id']);
                        }
                        
                        $outbOrdDtlModel = RepositoryUtils::dataToModel($outbOrdDtlModel, $outbOrdDtlData);
                        $outbOrdDtlModel->hdr_id = $outbOrdHdrModel->id;
                        $outbOrdDtlModel->save();

                        if(!empty($uuid))
                        {
                            $outbOrdHdrIdHashByUuid[$uuid] = $outbOrdDtlModel->hdr_id;
                            $outbOrdDtlIdHashByUuid[$uuid] = $outbOrdDtlModel->id;
                        }
                    }

                    if(!empty($delOutbOrdDtlIds))
                    {
                        OutbOrdDtl::destroy($delOutbOrdDtlIds);
                    }
                }

                //update slsInvHdr
                $slsInvHdrModel = SlsInvHdr::lockForUpdate()->find($slsInvHdrData['id']);
                if($slsInvHdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('SlsInv.doc_status_is_complete', ['docCode'=>$slsInvHdrModel->doc_code]));
                    $exc->addData(\App\SlsInvHdr::class, $slsInvHdrModel->id);
                    throw $exc;
                }
                $slsInvHdrModel = RepositoryUtils::dataToModel($slsInvHdrModel, $slsInvHdrData);
                $slsInvHdrModel->save();

                //update slsInvDtl array
                $slsInvDtlModels = array();
                foreach($slsInvDtlArray as $slsInvDtlData)
                {
                    $slsInvDtlModel = null;
                    if (strpos($slsInvDtlData['id'], 'NEW') !== false) 
                    {
                        $uuid = $slsInvDtlData['id'];
                        $slsInvDtlModel = new SlsInvDtl;
                        if(array_key_exists($uuid, $outbOrdHdrIdHashByUuid))
                        {
                            $slsInvDtlModel->outb_ord_hdr_id = $outbOrdHdrIdHashByUuid[$uuid];
                        }
                        if(array_key_exists($uuid, $outbOrdDtlIdHashByUuid))
                        {
                            $slsInvDtlModel->outb_ord_dtl_id = $outbOrdDtlIdHashByUuid[$uuid];
                        }
                        unset($slsInvDtlData['id']);
                    }
                    else
                    {
                        $slsInvDtlModel = SlsInvDtl::lockForUpdate()->find($slsInvDtlData['id']);
                    }
                    
                    $slsInvDtlModel = RepositoryUtils::dataToModel($slsInvDtlModel, $slsInvDtlData);
                    $slsInvDtlModel->hdr_id = $slsInvHdrModel->id;
                    $slsInvDtlModel->save();
                    $slsInvDtlModels[] = $slsInvDtlModel;
                }

                if(!empty($delSlsInvDtlIds))
                {
                    SlsInvDtl::destroy($delSlsInvDtlIds);
                }

                return array(
                    'hdrModel' => $slsInvHdrModel,
                    'dtlModels' => $slsInvDtlModels
                );
            }, 
            5 //reattempt times
        );
        return $result;
    }

    static public function findAllNotExistSlsInvSync01Txn($divisionId, $sorts, $filters = array(), $pageSize = 20) 
	{
        //$qStatuses = array(DocStatus::$MAP['COMPLETE']);
        $leftJoinHash = array();
        $slsInvHdrs = SlsInvHdr::select('sls_inv_hdrs.*')
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('doc_txn_flows')
                    ->where('doc_txn_flows.proc_type', ProcType::$MAP['SLS_INV_SYNC_01'])
                    ->where('doc_txn_flows.fr_doc_hdr_type', \App\SlsInvHdr::class)
                    ->whereRaw('doc_txn_flows.fr_doc_hdr_id = sls_inv_hdrs.id')
                    ->where('doc_txn_flows.is_closed', 1);
            })
            ->where('doc_status', DocStatus::$MAP['COMPLETE'])
            ->where('division_id', $divisionId);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $slsInvHdrs->where('doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'salesman_username') == 0)
            {
                $leftJoinHash['users'] = 'users';
                $slsInvHdrs->where('users.username', 'LIKE', '%'.$filter['value'].'%');
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $slsInvHdrs->orderBy('doc_code', $sort['order']);
            }
            if(strcmp($sort['field'], 'delivery_point_area_code') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $slsInvHdrs->orderBy('delivery_points.area_code', $sort['order']);
            }
            if(strcmp($sort['field'], 'salesman_username') == 0)
            {
                $leftJoinHash['users'] = 'users';
                $slsInvHdrs->orderBy('users.username', $sort['order']);
            }
            if(strcmp($sort['field'], 'delivery_point_code') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $slsInvHdrs->orderBy('delivery_points.code', $sort['order']);
            }         
            if(strcmp($sort['field'], 'doc_date') == 0)
            {
                $slsInvHdrs->orderBy('doc_date', $sort['order']);
            }
            if(strcmp($sort['field'], 'net_amt') == 0)
            {
                $slsInvHdrs->orderBy('net_amt', $sort['order']);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'users') == 0)
            {
                $slsInvHdrs->leftJoin('users', 'users.id', '=', 'sls_inv_hdrs.salesman_id');
            }
            if(strcmp($field, 'delivery_points') == 0)
            {
                $slsInvHdrs->leftJoin('delivery_points', 'delivery_points.id', '=', 'sls_inv_hdrs.delivery_point_id');
            }
        }
        
        if($pageSize > 0)
        {
            return $slsInvHdrs
            ->paginate($pageSize);
        }
        else
        {
            return $slsInvHdrs
            ->paginate(PHP_INT_MAX);
        }
    }

    static public function findAll($divisionId, $sorts, $filters = array(), $pageSize = 20) 
	{
        $leftJoinHash = array();
        $slsInvHdrs = SlsInvHdr::select('sls_inv_hdrs.*')
            ->where('division_id', $divisionId);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'doc_code') == 0)
            {
                $slsInvHdrs->where('sls_inv_hdrs.doc_code', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $slsInvHdrs->where(function($q) use($filter) {
                    $q->where('delivery_points.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('delivery_points.company_name_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'salesman') == 0)
            {
                $leftJoinHash['users'] = 'users';
                $slsInvHdrs->where('users.username', 'LIKE', '%'.$filter['value'].'%');
            }
            if(strcmp($filter['field'], 'delivery_point_area') == 0)
            {
                $leftJoinHash['delivery_points'] = 'delivery_points';
                $leftJoinHash['areas'] = 'areas';
                $slsInvHdrs->where(function($q) use($filter) {
                    $q->where('areas.code', 'LIKE', '%'.$filter['value'].'%')
                    ->orWhere('areas.desc_01', 'LIKE', '%'.$filter['value'].'%');
                });
            }
            if(strcmp($filter['field'], 'status') == 0)
            {
                if($filter['value'] > 0)
                {
                    $slsInvHdrs->where('sls_inv_hdrs.doc_status', $filter['value']);
                }
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'doc_code') == 0)
            {
                $slsInvHdrs->orderBy('doc_code', $sort['order']);
            }
        }

        foreach($leftJoinHash as $field)
        {
            if(strcmp($field, 'delivery_points') == 0)
            {
                $slsInvHdrs->leftJoin('delivery_points', 'delivery_points.id', '=', 'sls_inv_hdrs.delivery_point_id');
            }
            if(strcmp($field, 'users') == 0)
            {
                $slsInvHdrs->leftJoin('users', 'users.id', '=', 'sls_inv_hdrs.salesman_id');
            }
            if(strcmp($field, 'areas') == 0)
            {
                $slsInvHdrs->leftJoin('areas', 'areas.id', '=', 'delivery_points.area_id');
            }
        }
        
        if($pageSize > 0)
        {
            return $slsInvHdrs
                ->paginate($pageSize);
        }
        else
        {
            return $slsInvHdrs
                ->paginate(PHP_INT_MAX);
        }
    }

    static public function revertDraftToVoid($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = SlsInvHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('SlsInv.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\SlsInvHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to VOID
                $hdrModel->doc_status = DocStatus::$MAP['VOID'];
                $hdrModel->save();

                $frDocTxnFlows = $hdrModel->frDocTxnFlows;
                foreach($frDocTxnFlows as $frDocTxnFlow)
                {
                    //move to voidTxn
                    $txnVoidModel = new DocTxnVoid;
                    $txnVoidModel->id = $frDocTxnFlow->id;
                    $txnVoidModel->proc_type = $frDocTxnFlow->proc_type;
                    $txnVoidModel->fr_doc_hdr_type = $frDocTxnFlow->fr_doc_hdr_type;
                    $txnVoidModel->fr_doc_hdr_id = $frDocTxnFlow->fr_doc_hdr_id;
                    $txnVoidModel->fr_doc_hdr_code = $frDocTxnFlow->fr_doc_hdr_code;
                    $txnVoidModel->to_doc_hdr_type = $frDocTxnFlow->to_doc_hdr_type;
                    $txnVoidModel->to_doc_hdr_id = $frDocTxnFlow->to_doc_hdr_id;
                    $txnVoidModel->is_closed = $frDocTxnFlow->is_closed;
                    $txnVoidModel->save();
        
                    //delete the txn
                    $frDocTxnFlow->delete();
                }

                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function syncSlsInvSync02($company, $division, $data) 
	{
        $siteFlow = $division->siteFlow;

        //find associated sales order
        $slsOrdHdrModel = SlsOrdHdr::where('doc_code', $data['so_code'])
            ->lockForUpdate()
            ->first();
        
        //sales order not found, return null
        if (empty($slsOrdHdrModel)) {
            return null;
        }

        $slsInvHdrModel = DB::transaction
        (
            function() use ($siteFlow, $company, $division, $data, $slsOrdHdrModel)
            {

                $isDirty = false;
                $model = SlsInvHdr::firstOrCreate(['doc_code' => $data['doc_code']]);
                
                $model->proc_type = ProcType::$MAP['SLS_INV_SYNC_02'];
                if($model->doc_status == DocStatus::$MAP['NULL'])
                {
                    //initialize with DRAFT status
                    $model->doc_status = DocStatus::$MAP['DRAFT'];
                }             
                $model->ref_code_01 = $data['ref_code'];
                $model->doc_date = $data['doc_date'];
                $model->est_del_date = $data['userdate_01'];
                $model->desc_01 = $data['remark_01'];
                $model->desc_02 = $data['remark_02'];
                
                $model->shipment_code = $data['shipment_code'];
                $model->shipment_date = $data['shipment_date'];
                $model->transport_code = $data['transport_code'];
                $model->driver_name = $data['driver_name'];
                // $model->shipment_remark = $data['shipment_remark'];

                $model->division_id = $division->id;
                $model->site_flow_id = $division->site_flow_id;
                $model->company_id = $company->id;

                //currency
                $model->currency_id = 1;
                $model->currency_rate = $data['currency_rate'];
                $currency = CurrencyRepository::syncSlsOrdSync01($data);
                if(!empty($currency))
                {
                    $model->currency_id = $currency->id;
                }
                
                //creditTerm
                $model->credit_term_id = 0;
                if(strcmp($data['sales_type'], 'c') == 0)
                {
                    //if sales order sales_type = 0, always set to CASH term
                    //so will generate another invoice code
                    $model->credit_term_id = 1;
                }
                else
                {
                    $creditTerm = CreditTermRepository::syncSlsOrdSync01($data);
                    if(!empty($creditTerm))
                    {
                        $model->credit_term_id = $creditTerm->id;
                    }
                }

                //salesman
                // $salesman = UserRepository::syncSlsOrdSync01($division, $data);
                $model->salesman_id = $slsOrdHdrModel->salesman_id;

                //delivery point                
                // $deliveryPoint = DeliveryPointRepository::syncSlsOrdSync01($division, $data);
                $model->debtor_id = $slsOrdHdrModel->debtor_id;
                //$model->delivery_point_id = $slsOrdHdrModel->delivery_point_id;

                $model->gross_amt = $data['order_amt'];
                $model->gross_local_amt = $data['order_local_amt'];
                $model->disc_amt = $data['disc_amt'];
                $model->disc_local_amt = $data['disc_local_amt'];
                $model->tax_amt = $data['tax_amt'];
                $model->tax_local_amt = $data['tax_local_amt'];
                $model->is_round_adj = 0;
                $model->round_adj_amt = 0;
                $model->round_adj_local_amt = 0;
                $model->net_amt = $data['net_amt'];
                $model->net_local_amt = $data['net_local_amt'];

                $slsInvDtls = SlsInvDtl::where('hdr_id', $model->id)
                                ->get();
                $slsInvDtlsHashByLineNo = array();
                $updatedSlsInvDtls = array();
                $deletedSlsInvDtls = array();
                foreach($slsInvDtls as $slsInvDtl)
                {
                    if(array_key_exists($slsInvDtl->line_no, $slsInvDtlsHashByLineNo))
                    {
                        $deletedSlsInvDtls[] = $slsInvDtl;
                    }
                    else
                    {
                        $slsInvDtlsHashByLineNo[$slsInvDtl->line_no] = $slsInvDtl;
                    }
                }

                $details = $data['details'];
                $delDate = $data['doc_date'];
                foreach($details as $detail)
                {
                    $delDate = $detail['del_date'];

                    $slsInvDtl = null;
                    if(array_key_exists($detail['line_no'], $slsInvDtlsHashByLineNo))
                    {
                        $slsInvDtl = $slsInvDtlsHashByLineNo[$detail['line_no']];
                        unset($slsInvDtlsHashByLineNo[$detail['line_no']]);
                    }
                    else
                    {
                        $slsInvDtl = new SlsInvDtl();
                        $slsInvDtl->hdr_id = $model->id;
                    }

                    $slsInvDtl->line_no = $detail['line_no'];

                    // $location = LocationRepository::syncSlsOrdSync01($siteFlow->site_id, $detail);
                    // $slsInvDtl->location_id = $location->id;

                    $slsInvDtl->location_id = 0;

                    $uom = UomRepository::syncSlsOrdSync01($division, $detail);
                    
                    $item = ItemRepository::syncSlsOrdSync01($division, $uom, $detail);
                    $slsInvDtl->item_id = $item->id;
                    $slsInvDtl->desc_01 = $detail['description'];
                    $slsInvDtl->desc_02 = '';
                    $slsInvDtl->uom_id = $uom->id;
                    $slsInvDtl->uom_rate = $detail['uom_rate'];
                    if(bccomp($detail['list_price'], $detail['price'], 2) > 0)
                    {
                        $slsInvDtl->sale_price = $detail['list_price'];
                        $slsInvDtl->price_disc = bcsub($detail['list_price'], $detail['price'], 2);
                    }
                    else
                    {
                        $slsInvDtl->sale_price = $detail['price'];
                        $slsInvDtl->price_disc = 0;
                    }
                    $slsInvDtl->qty = $detail['qty'];
                    $slsInvDtl->dtl_disc_perc_01 = $detail['disc_percent_01'] >= 0 ? $detail['disc_percent_01'] : 0; //for efichain disc amt
                    $slsInvDtl->dtl_disc_val_01 = 0;
                    if(bccomp($detail['disc_percent_01'], -1) == 0)
                    {
                        $slsInvDtl->dtl_disc_val_01 = $detail['usernumber_01'];
                    }
                    $slsInvDtl->dtl_disc_perc_02 = $detail['disc_percent_02'];
                    $slsInvDtl->dtl_disc_perc_03 = $detail['disc_percent_03'];
                    $slsInvDtl->dtl_disc_perc_04 = $detail['disc_percent_04'];
                    $slsInvDtl->dtl_disc_amt = $detail['disc_amt'];
                    $slsInvDtl->dtl_disc_local_amt = $detail['disc_local_amt'];
                    $slsInvDtl->hdr_disc_perc_01 = $detail['disc_percent_05'];
                    $slsInvDtl->hdr_disc_perc_02 = $detail['disc_percent_06'];
                    $slsInvDtl->hdr_disc_perc_03 = $detail['disc_percent_07'];
                    $slsInvDtl->hdr_disc_perc_04 = $detail['disc_percent_08'];
                    $slsInvDtl->dtl_tax_perc_01 = $detail['tax_percent_01'];
                    $slsInvDtl->dtl_tax_perc_02 = $detail['tax_percent_02'];
                    $slsInvDtl->dtl_tax_perc_03 = $detail['tax_percent_03'];
                    $slsInvDtl->dtl_tax_perc_04 = $detail['tax_percent_04'];
                    $slsInvDtl->dtl_tax_amt_01 = $detail['tax_amt'];
                    $slsInvDtl->dtl_tax_local_amt_01 = $detail['tax_local_amt'];
                    $slsInvDtl->gross_amt = $detail['order_amt'];
                    $slsInvDtl->gross_local_amt = $detail['order_local_amt'];
                    $slsInvDtl->net_amt = $detail['net_amt'];
                    $slsInvDtl->net_local_amt = $detail['net_local_amt'];

                    if($slsInvDtl->isDirty() == true)
                    {
                        $isDirty = $slsInvDtl->isDirty();
                    }

                    $updatedSlsInvDtls[] = $slsInvDtl;
                }
                $model->est_del_date = $delDate;

                if($model->isDirty() == true)
                {
                    $isDirty = $model->isDirty();
                }
                if(count($slsInvDtlsHashByLineNo) > 0)
                {
                    $isDirty = true;
                }
                if(count($deletedSlsInvDtls) > 0)
                {
                    $isDirty = true;
                }

                if($isDirty == true)
                {
                    //will check for models dirty, if is dirty, then only will revert the model to DRAFT
                    if($model->doc_status >= DocStatus::$MAP['WIP'])
                    {
                        //return to draft, so can update the details
                        if($model->doc_status == DocStatus::$MAP['COMPLETE'])
                        {
                            $tmpModel = self::revertCompleteToDraft($model->id);
                            $model->doc_status = $tmpModel->doc_status;
                        }
                        elseif($model->doc_status == DocStatus::$MAP['WIP'])
                        {
                            $tmpModel = self::revertWipToDraft($model->id);
                            $model->doc_status = $tmpModel->doc_status;
                        }
                    }

                    foreach($updatedSlsInvDtls as $updatedSlsInvDtl)
                    {
                        $updatedSlsInvDtl->save();
                    }
                    foreach($slsInvDtlsHashByLineNo as $lineNo => $slsInvDtl)
                    {
                        $slsInvDtl->delete();
                    }
                    foreach($deletedSlsInvDtls as $slsInvDtl)
                    {
                        $slsInvDtl->delete();
                    }
                    $model->save();
                }

                if($model->doc_status < DocStatus::$MAP['COMPLETE'])
                {
                    $model = self::commitToComplete($model->id);
                }

                if($slsOrdHdrModel->doc_status < DocStatus::$MAP['COMPLETE'])
                {
                    $slsOrdHdrModel = SlsOrdHdrRepository::commitToComplete($slsOrdHdrModel->id);
                }

                // if($data['status'] == 3)
                // {
                //     //set to COMPLETE
                //     if($model->doc_status == DocStatus::$MAP['VOID'])
                //     {
                //         $model = self::commitVoidToDraft($model->id);
                //     }

                //     if($model->doc_status < DocStatus::$MAP['COMPLETE'])
                //     {
                //         $model = self::commitToComplete($model->id, true);
                //     }
                // }
                // else
                // {
                //     //set to WIP
                //     if($model->doc_status == DocStatus::$MAP['COMPLETE'])
                //     {
                //         $model = self::revertCompleteToDraft($model->id);
                //     }
                //     if($model->doc_status == DocStatus::$MAP['DRAFT'])
                //     {
                //         $model = self::commitToWip($model->id);
                //     }
                // }

                $model->slsOrdHdr = $slsOrdHdrModel;
                
                return $model;
            }, 
            5 //reattempt times
        );
        return $slsInvHdrModel;
    }
}