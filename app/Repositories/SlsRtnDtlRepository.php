<?php

namespace App\Repositories;

use App\SlsRtnDtl;
use App\SlsRtnHdr;

class SlsRtnDtlRepository 
{
    static public function findAllByHdrId($hdrId) 
	{
        $slsRtnDtls = SlsRtnDtl::where('hdr_id', $hdrId)
            ->orderBy('line_no', 'ASC')
            ->get();

        return $slsRtnDtls;
    }

    static public function findAllByHdrIds($hdrIds) 
	{
        $slsRtnDtls = SlsRtnDtl::whereIn('hdr_id', $hdrIds)
            ->get();

        return $slsRtnDtls;
    }

    static public function findAllByIds($ids) 
	{
        $slsRtnDtls = SlsRtnDtl::whereIn('id', $ids)
            ->get();

        return $slsRtnDtls;
    }

    static public function findTopByHdrId($hdrId)
    {
        $slsRtnDtl = SlsRtnDtl::where('hdr_id', $hdrId)
            ->orderBy('line_no')
            ->first();

        return $slsRtnDtl;
    }
}