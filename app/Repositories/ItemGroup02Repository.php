<?php

namespace App\Repositories;

use App\ItemGroup02;
use App\Item;
use App\Services\Utils\ApiException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use JD\Cloudder\Facades\Cloudder;

class ItemGroup02Repository
{
    static public function findByPk($id)
    {
        return ItemGroup02::find($id);
    }

    static public function findByCode($code) 
	{
        $group = ItemGroup02::where('code', $code)
        ->first();
        return $group;
    }

    static public function select2($search, $filters, $pageSize = 10) 
	{
        $itemGroup02s = ItemGroup02::select('item_group02s.*')
        ->where(function($q) use($search) {
            $q->orWhere('desc_01', 'LIKE', '%' . $search . '%')
            ->orWhere('desc_02', 'LIKE', '%' . $search . '%')
            ->orWhere('code', 'LIKE', '%' . $search . '%');
        });
        if(count($filters) >= 1)
        {
            $itemGroup02s = $itemGroup02s->where($filters);
        }
        return $itemGroup02s
            ->orderBy('code', 'ASC')
            ->paginate($pageSize);
    }
	
    static public function select2ByDivision($divisionID, $search, $filters, $pageSize = 100) 
	{
        $divisionItemGroups = DB::select( DB::raw("SELECT DISTINCT i.item_group_02_id FROM item_divisions id LEFT JOIN items i
            ON i.id = id.item_id
            WHERE id.division_id = :division_id"), 
            array(
            'division_id' => $divisionID,
            ));

        //$array = json_decode(json_encode($divisionItemGroups), True);
        foreach ($divisionItemGroups as $value) 
            $array[] = $value->item_group_02_id;

        $itemGroup02s = ItemGroup02::select('item_group02s.*')
        ->whereIn('id', $array)
        ->where(function($q) use($search) {
            $q->orWhere('desc_01', 'LIKE', '%' . $search . '%')
            ->orWhere('desc_02', 'LIKE', '%' . $search . '%')
            ->orWhere('code', 'LIKE', '%' . $search . '%');
        });

        if(count($filters) >= 1)
        {
            $itemGroup02s = $itemGroup02s->where($filters);
        }

        return $itemGroup02s
            ->orderBy('code', 'ASC')
            ->paginate($pageSize);
    }

    static public function findAll($sorts, $filters = array(), $pageSize = 20) 
	{
        $items = Item::select('item_group02s.*')
                ->distinct()
                ->leftJoin('item_divisions', 'item_divisions.item_id', '=', 'items.id')
                ->leftJoin('item_group02s', 'items.item_group_02_id', '=', 'item_group02s.id')
                ->leftJoin('item_group01s', 'items.item_group_01_id', '=', 'item_group01s.id')
                ->whereNotNull('item_group02s.id')
                ->where('items.status', '=', 100);

        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'division_id') == 0)
            {
                $items->where('item_divisions.division_id', '=', $filter['value']);
            }
            if(strcmp($filter['field'], 'item_group_01_id') == 0)
            {
                $items->where('item_group01s.id', '=', $filter['value']);
            }
        }

        foreach($sorts as $sort)
        {
            if(strcmp($sort['field'], 'code') == 0)
            {
                $items->orderBy('code', $sort['order']);
            }
            elseif(strcmp($sort['field'], 'desc_01') == 0)
            {
                $items->orderBy('desc_01', $sort['order']);
            }
            elseif(strcmp($sort['field'], 'desc_02') == 0)
            {
                $items->orderBy('desc_02', $sort['order']);
            }
        }
        
        if($pageSize > 0)
        {
            return $items
                ->paginate($pageSize);
        }
        else
        {
            return $items
                ->paginate(PHP_INT_MAX);
        }
    }

    static public function savePhoto($data) 
	{
        $group = DB::transaction
        (
            function() use ($data)
            {
				$group = ItemGroup02::find($data['id']);
				$group->image_desc_01 = $data['desc_01'];
				$group->image_desc_02 = $data['desc_02'];
				list($width, $height) = getimagesize($data['filename']);

                Cloudder::upload($data['filename'], null);
                $public_id = Cloudder::getPublicId();
                $image_url= Cloudder::show($public_id, ["width" => $width, "height"=>$height]);
                
				//save to uploads directory
                $image = $data['blob'];
                $image->move(public_path("uploads"), $group->desc_01);

                $group->image_path = $image_url;
                $group->image_desc_02 = $public_id;
				$group->save();

                return $group;
            }, 
            5 //reattempt times
        );
        return $group;
    }

    static public function deletePhoto($id) 
	{
        $group = DB::transaction
        (
            function() use ($id)
            {
				$group = ItemGroup02::find($id);

				$public_id = $group->image_desc_02;
                if(!empty($public_id))
                {
                    Cloudder::destroyImage($public_id, array());

					$group->image_desc_01 = '';
					$group->image_desc_02 = '';
					$group->image_path = '';
					$group->save();
                }
				
				return $group;
            }, 
            5 //reattempt times
        );

        return $group;
    }
}