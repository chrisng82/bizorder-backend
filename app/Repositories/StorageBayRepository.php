<?php

namespace App\Repositories;

use App\StorageBay;
use App\StorageBin;
use App\Services\Utils\RepositoryUtils;
use Illuminate\Support\Facades\DB;

class StorageBayRepository 
{
    static public function findBySiteIdAndCode($siteId, $code) 
	{
        $storageBay = StorageBay::where('site_id', $siteId)
        ->where('code', $code)
        ->first();

        return $storageBay;
    }

    static public function findAllByStorageRowId($storageRowId) 
	{
        $storageBays = StorageBay::where('storage_row_id', $storageRowId)
        ->orderBy('code', 'ASC')
        ->get();

        return $storageBays;
    }

    static public function findAllByStorageRowIdDesc($storageRowId) 
	{
        $storageBays = StorageBay::where('storage_row_id', $storageRowId)
        ->orderBy('code', 'DESC')
        ->get();

        return $storageBays;
    }

    static public function updateBaySequence($storageBay, $baySequence) 
	{
        $model = DB::transaction
        (
            function() use ($storageBay, $baySequence)
            {
                $storageBay->bay_sequence = $baySequence;
                $storageBay->save();

                //query all storageBin of this bay
                $storageBins = StorageBin::where('storage_bay_id', $storageBay->id)
                ->get();
                foreach($storageBins as $storageBin)
                {
                    $storageBin->bay_sequence = $baySequence;
                    $storageBin->save();
                }

                return $storageBay;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function select2($search, $filters, $pageSize = 10) 
	{
        $storageBays = StorageBay::select('storage_bays.*')
        ->where(function($q) use($search) {
            $q->orWhere('desc_01', 'LIKE', '%' . $search . '%')
            ->orWhere('desc_02', 'LIKE', '%' . $search . '%')
            ->orWhere('code', 'LIKE', '%' . $search . '%');
        });
        if(count($filters) >= 1)
        {
            $storageBays = $storageBays->where($filters);
        }
        return $storageBays
            ->orderBy('code', 'ASC')
            ->paginate($pageSize);
    }

    static public function plainCreate($data) 
	{
        $model = new StorageBay();
        $model = RepositoryUtils::dataToModel($model, $data);
        $model->save();

        return $model;
    }
}