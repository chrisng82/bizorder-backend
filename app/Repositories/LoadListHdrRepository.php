<?php

namespace App\Repositories;

use App\Services\Utils\RepositoryUtils;
use App\LoadListHdr;
use App\LoadListDtl;
use App\LoadListCDtl;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\DB;

class LoadListHdrRepository 
{
    static public function txnFindByPk($hdrId) 
	{
        $model = DB::transaction
        (
            function() use ($hdrId)
            {
                $loadListHdr = LoadListHdr::where('id', $hdrId)
                    ->first();
                return $loadListHdr;
            }, 
            5 //reattempt times
        );
        return $model;
    }

    static public function commitToWip($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = LoadListHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT can transition to WIP
                    $exc = new ApiException(__('LoadList.doc_status_is_not_draft', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\LoadListHdr::class, $hdrModel->id);
                    throw $exc;
                }

                $dtlModels = LoadListDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->orderBy('line_no')
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('LoadList.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\LoadListHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to WIP
                $hdrModel->doc_status = DocStatus::$MAP['WIP'];
                $hdrModel->save();

                return $hdrModel;  
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function commitToComplete($hdrId, $isNonZeroBal)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId, $isNonZeroBal)
            {
                $hdrModel = LoadListHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();
                    
                if($hdrModel->doc_status >= DocStatus::$MAP['COMPLETE'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('LoadList.doc_status_is_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\LoadListHdr::class, $hdrModel->id);
                    throw $exc;
                }
                if($hdrModel->doc_status < DocStatus::$MAP['DRAFT'])
                {
                    //only DRAFT or above can transition to COMPLETE
                    $exc = new ApiException(__('LoadList.doc_status_is_not_draft_or_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\LoadListHdr::class, $hdrModel->id);
                    throw $exc;
                }
        
                //1) update docStatus to COMPLETE
                $hdrModel->doc_status = DocStatus::$MAP['COMPLETE'];
                $hdrModel->save();

                $dtlModels = LoadListDtl::where('hdr_id', $hdrId)
                    ->lockForUpdate()
                    ->get();
                if(count($dtlModels) == 0)
                {
                    $exc = new ApiException(__('LoadList.item_details_not_found', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\LoadListHdr::class, $hdrModel->id);
                    throw $exc;
                }
                $dtlModels->load('item');
                foreach($dtlModels as $dtlModel)
                {
                    $item = $dtlModel->item;
                    
                    //this is stock transfer
                    QuantBalRepository::commitStockTransfer(
                        0, $isNonZeroBal, $item, $dtlModel->quant_bal_id, 
                        $dtlModel->to_storage_bin_id, $dtlModel->to_handling_unit_id, 
                        $hdrModel->doc_date, \App\LoadListHdr::class, $hdrModel->id, 
                        $dtlModel->id, $dtlModel->uom_id, $dtlModel->uom_rate, $dtlModel->qty);
                }     
                return $hdrModel;           
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertWipToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = LoadListHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['WIP'])
                {
                    $exc = new ApiException(__('LoadList.doc_status_is_not_wip', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\LoadListHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();
                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return $hdrModel;
    }

    static public function revertCompleteToDraft($hdrId)
    {
        $hdrModel = DB::transaction
        (
            function() use ($hdrId)
            {
                $hdrModel = LoadListHdr::where('id', $hdrId)
                    ->lockForUpdate()
                    ->first();

                if($hdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                {
                    $exc = new ApiException(__('LoadList.doc_status_is_not_complete', ['docCode'=>$hdrModel->doc_code]));
                    $exc->addData(\App\LoadListHdr::class, $hdrModel->id);
                    throw $exc;
                }
                
                //1) update docStatus to DRAFT
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                QuantBalRepository::revertStockTransaction(\App\LoadListHdr::class, $hdrModel->id);
                return $hdrModel;
            }, 
            5 //reattempt times
        );

        return $hdrModel;
    }

    static public function createProcess($procType, $docNoId, $hdrData, $dtlDataList, $docTxnFlowDataList) 
	{
        $loadListHdr = DB::transaction
        (
            function() use ($procType, $docNoId, $hdrData, $dtlDataList, $docTxnFlowDataList)
            {
                $newCode = DocNoRepository::generateNewCode($docNoId, $hdrData['doc_date']);

                $hdrModel = new LoadListHdr;
                $hdrModel = RepositoryUtils::dataToModel($hdrModel, $hdrData);
                $hdrModel->doc_code = $newCode;
                $hdrModel->proc_type = $procType;
                $hdrModel->doc_status = DocStatus::$MAP['DRAFT'];
                $hdrModel->save();

                $txnHdrModelHash = array();
                foreach($dtlDataList as $lineNo => $dtlData)
                {
                    $dtlModel = new LoadListDtl;
                    $dtlModel = RepositoryUtils::dataToModel($dtlModel, $dtlData);
                    $dtlModel->hdr_id = $hdrModel->id;
                    $dtlModel->save();

                    //create a carbon copy
                    $cDtlModel = new LoadListCDtl;
                    $cDtlModel = RepositoryUtils::dataToModel($cDtlModel, $dtlData);
                    $cDtlModel->hdr_id = $hdrModel->id;
                    $cDtlModel->save();
                }

                //process docTxnFlowDataList
                foreach($docTxnFlowDataList as $docTxnFlowData)
                {
                    $frDocHdrModel = $docTxnFlowData['fr_doc_hdr_type']::where('id', $docTxnFlowData['fr_doc_hdr_id'])
                    ->lockForUpdate()
                    ->first();
                    if($frDocHdrModel->doc_status != DocStatus::$MAP['COMPLETE'])
                    {
                        $exc = new ApiException(__('LoadList.fr_doc_is_not_complete', ['docType'=>$docTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$docTxnFlowData['fr_doc_hdr_code']]));
                        $exc->addData($docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_code']);
                        throw $exc;
                    }

                    //verify the frDoc is not closed for this procType
                    $tmpDocTxnFlow = DocTxnFlowRepository::findByProcTypeAndFrHdrId($procType, $docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_id']);
                    if(!empty($tmpDocTxnFlow))
                    {
                        $exc = new ApiException(__('LoadList.fr_doc_is_closed', ['docType'=>$docTxnFlowData['fr_doc_hdr_type'], 'docCode'=>$docTxnFlowData['fr_doc_hdr_code']]));
                        $exc->addData($docTxnFlowData['fr_doc_hdr_type'], $docTxnFlowData['fr_doc_hdr_id']);
                        throw $exc;
                    }
                    
                    $docTxnFlowModel = new DocTxnFlow;
                    $docTxnFlowModel = RepositoryUtils::dataToModel($docTxnFlowModel, $docTxnFlowData);
                    $docTxnFlowModel->proc_type = $procType;
                    $docTxnFlowModel->to_doc_hdr_type = \App\LoadListHdr::class;
                    $docTxnFlowModel->to_doc_hdr_id = $hdrModel->id;
                    $docTxnFlowModel->save();
                }
                
                return $hdrModel;
            }, 
            5 //reattempt times
        );
        return loadListHdr;
    }
}