<?php

namespace App\Repositories;

use App\ItemBatch;
class ItemBatchRepository 
{
    static public function firstOrCreate($item, $batchSerialNo, $expiryDate, $receiptDate) 
	{
        $itemBatch = null;
        if(strcmp($receiptDate, '0000-00-00') == 0)
        {
            $receiptDate = date('Y-m-d');
        }
        if($item->batch_serial_control > 0)
        {
            $attributes = array(
                'item_id' => $item->id,
                'batch_serial_no' => $batchSerialNo,
                'expiry_date' => $expiryDate, 
            );
            $values = array(
                'receipt_date' => $receiptDate
            );
            $itemBatch = ItemBatch::firstOrCreate(
                $attributes,
                $values
            );
        }
        else
        {
            $attributes = array(
                'item_id' => $item->id,
                'expiry_date' => $expiryDate, 
            );
            $values = array(
                'batch_serial_no' => '',
                'receipt_date' => $receiptDate
            );
            $itemBatch = ItemBatch::firstOrCreate(
                $attributes,
                $values
            );
        }
        return $itemBatch;
    }

    static public function findByAttributes($item, $batchSerialNo, $expiryDate, $receiptDate)
	{
        $itemBatch = null;
        if($item->batch_serial_control > 0)
        {
            $itemBatch = ItemBatch::where(array(
                'item_id' => $item->id,
                'batch_serial_no' => $batchSerialNo,
                'expiry_date' => $expiryDate,
            ))
            ->where('expiry_date', '<>', '0000-00-00')
            ->first();
        }
        else
        {
            $itemBatch = ItemBatch::where(array(
                'item_id' => $item->id,
                'expiry_date' => $expiryDate, 
            ))
            ->where('expiry_date', '<>', '0000-00-00')
            ->first();
        }
        return $itemBatch;
    }

    static public function findByPk($id) 
	{
        $itemBatch = ItemBatch::where('id', $id)
            ->first();

        return $itemBatch;
    }

    static public function select2($search, $filters, $pageSize = 10)
    {
        $itemBatches = ItemBatch::select('item_batches.*')
        ->where(function($q) use($search) {
            $q->orWhere('item_batches.batch_serial_no', 'LIKE', '%' . $search . '%')
            ->orWhere('item_batches.expiry_date', 'LIKE', '%' . $search . '%');
        });
        foreach($filters as $filter)
        {
            if(strcmp($filter['field'], 'item_id') == 0)
            {
                $itemBatches->where('item_batches.item_id', $filter['value']);
            }
        }
        return $itemBatches
            ->orderBy('expiry_date', 'DESC')
            ->paginate($pageSize);
    }
}