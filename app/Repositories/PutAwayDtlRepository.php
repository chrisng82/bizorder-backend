<?php

namespace App\Repositories;

use App\PutAwayHdr;
use App\PutAwayDtl;
use App\Repositories\DocNoRepository;
use App\Services\Env\DocStatus;
use Illuminate\Support\Facades\DB;

class PutAwayDtlRepository 
{
    static public function findAllByHdrId($hdrId)
    {
        $putAwayDtls = PutAwayDtl::where('hdr_id', $hdrId)
            ->orderBy('line_no')
            ->get();

        return $putAwayDtls;
    }

    static public function findAllByHdrIds($hdrIds) 
	{
        $putAwayDtls = PutAwayDtl::whereIn('hdr_id', $hdrIds)
            ->get();

        return $putAwayDtls;
    }
}