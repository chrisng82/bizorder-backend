<?php

namespace App\Repositories;

use App\SiteFlow;
use Illuminate\Support\Facades\DB;

class SiteFlowRepository 
{
    static public function findByPk($id) 
	{
        $siteFlow = SiteFlow::where('id', $id)
            ->first();

        return $siteFlow;
    }

    static public function findAllByUserId($userId) 
	{
        $siteFlows = SiteFlow::select('site_flows.*')
        ->whereExists(function ($query) use($userId) {
            $query->select(DB::raw(1))
                ->from('user_site_flows')
                ->whereRaw('site_flow_id = site_flows.id')
                ->where('user_id', $userId);
        })
        ->get();
        return $siteFlows;
    }
}