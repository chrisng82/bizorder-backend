<?php

namespace App\Repositories;

use App\State;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class StateRepository 
{
    static public function select2($search, $filters, $pageSize = 10) 
	{
        $states = State::where(function($q) use($search) {
            $q->where('code', 'LIKE', '%' . $search . '%')
            ->orWhere('desc_01', 'LIKE', '%' . $search . '%');
        });
        if(count($filters) >= 1)
        {
            $states = $states->where($filters);
        }
        return $states
            ->orderBy('code', 'ASC')
            ->paginate($pageSize);
    }

    static public function findByPk($id) 
	{
        $state = State::where('id', $id)
            ->first();

        return $state;
    }	
}