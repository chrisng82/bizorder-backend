<?php

namespace App\Repositories;

use App\MobileProfile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class MobileProfileRepository 
{
    static public function findByPk($id) 
	{
        $mobileProfile = MobileProfile::find($id);

        return $mobileProfile;
    }

    static public function findByUuidAndMobileApp($deviceUuid, $mobileApp) 
	{
        $mobileProfile = MobileProfile::where('device_uuid', $deviceUuid)
            ->where('mobile_app', $mobileApp)
            ->first();

        return $mobileProfile;
    }

    static public function create($deviceUuid, $mobileApp, $mobileOs, $osVersion, $appVersion) 
	{
        $mobileProfile = DB::transaction
        (
            function() use ($deviceUuid, $mobileApp, $mobileOs, $osVersion, $appVersion)
            {
                $mobileProfile = new MobileProfile;
                $mobileProfile->device_uuid = $deviceUuid;
                $mobileProfile->mobile_app = $mobileApp;
                $mobileProfile->mobile_os = $mobileOs;
                $mobileProfile->os_version = $osVersion;
                $mobileProfile->app_version = $appVersion;
                $mobileProfile->synced_at = Carbon::now();
                $mobileProfile->save();

                return $mobileProfile;
            }, 
            5 //reattempt times
        );
        return $mobileProfile;
    }

    static public function update($id, $mobileOs, $osVersion, $appVersion) 
	{
        $mobileProfile = DB::transaction
        (
            function() use ($id, $mobileOs, $osVersion, $appVersion)
            {
                $mobileProfile = MobileProfile::lockForUpdate()->find($id);
                if(empty($mobileProfile))
                {
                    $exc = new ApiException(__('Mobile.mobile_profile_not_found', ['mobileProfileId'=>$id]));
                    $exc->addData(\App\MobileProfile::class, $id);
                    throw $exc;
                }

                $mobileProfile->mobile_os = $mobileOs;
                $mobileProfile->os_version = $osVersion;
                $mobileProfile->app_version = $appVersion;
                $mobileProfile->synced_at = Carbon::now();
                $mobileProfile->save();

                return $mobileProfile;
            }, 
            5 //reattempt times
        );
        return $mobileProfile;
    }
}