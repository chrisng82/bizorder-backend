<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PutAwayDtl extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $casts = [
        'uom_rate' => 'decimal:6',
        'qty' => 'decimal:6',
    ];
    
    public function putAwayHdr()
    {
        return $this->belongsTo(\App\PutAwayHdr::class, 'hdr_id', 'id');
    }

    public function quantBal()
    {
        return $this->belongsTo(\App\QuantBal::class, 'quant_bal_id', 'id');
    }

    public function item()
    {
        return $this->belongsTo(\App\Item::class, 'item_id', 'id');
    }

    public function uom()
    {
        return $this->belongsTo(\App\Uom::class, 'uom_id', 'id');
    }

    public function toStorageBin()
    {
        return $this->belongsTo(\App\StorageBin::class, 'to_storage_bin_id', 'id');
    }

    public function itemCond01()
    {
        return $this->belongsTo(\App\ItemCond01::class, 'item_cond_01_id', 'id');
    }

    public function generateTags(): array
    {
        $docCode = $this->putAwayHdr->doc_code;
        unset($this->putAwayHdr);
        return array(
            $docCode,
        );
    }
}
