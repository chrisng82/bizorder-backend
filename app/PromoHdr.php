<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PromoHdr extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    public function promoDtls()
    {
        return $this->hasMany(\App\PromoDtl::class);
    }

    public function promoDebtors()
    {
        return $this->hasMany(\App\PromoDebtor::class);
    }

    public function promoDivisions()
    {
        return $this->hasMany(\App\PromoDivision::class);
    }

    public function generateTags(): array
    {
        return array(
            $this->code
        );
    }
}
