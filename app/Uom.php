<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Uom extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $fillable = ['code','ref_code_01','desc_01','desc_02'];
    
    public static $PALLET = 1;

    public static $CASE = 2;

    public static $UNIT = 3;

    public function generateTags(): array
    {
        return array(
            $this->code
        );
    }
}
