<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class GdsRcptInbOrd extends Model implements Auditable
{
    //
    use \OwenIt\Auditing\Auditable;

    public function inbOrdHdr()
    {
        return $this->belongsTo(\App\InbOrdHdr::class, 'inb_ord_hdr_id', 'id');
    }

    public function gdsRcptHdr()
    {
        return $this->belongsTo(\App\GdsRcptHdr::class, 'hdr_id', 'id');
    }

    public function generateTags(): array
    {
        $docCode = $this->gdsRcptHdr->doc_code;
        unset($this->gdsRcptHdr);
        return array(
            $docCode
        );
    }
}
