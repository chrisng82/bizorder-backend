<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class DebtorGroup04 extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $fillable = ['code'];

    public function generateTags(): array
    {
        return array(
            $this->code
        );
    }
}
