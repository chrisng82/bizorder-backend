<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ItemGroup01 extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $fillable = ['code','desc_01','desc_02'];

    public function brand()
    {
        return $this->belongsTo(\App\ItemGroup01::class, 'brand_id', 'id');
    }

    public function siteFlow()
    {
        return $this->belongsTo(\App\SiteFlow::class, 'site_flow_id', 'id');
    }

    public function generateTags(): array
    {
        return array(
            $this->code
        );
    }
}
