<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class SiteDocNo extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //
    public function docNo()
    {
        return $this->belongsTo(\App\DocNo::class, 'doc_no_id', 'id');
    }

    public function site()
    {
        return $this->belongsTo(\App\Site::class, 'site_id', 'id');
    }

    public function generateTags(): array
    {
        $code = $this->site->code;
        unset($this->site);
        return array(
            $code,
        );
    }
}
