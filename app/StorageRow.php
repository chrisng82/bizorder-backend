<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class StorageRow extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $casts = [
        'layout_width' => 'decimal:8',
        'layout_depth' => 'decimal:8',
        'layout_height' => 'decimal:8',
        'layout_x' => 'decimal:8',
        'layout_y' => 'decimal:8',
    ];
    
    public function site()
    {
        return $this->belongsTo(\App\Site::class, 'site_id', 'id');
    }

    public function generateTags(): array
    {
        return array(
            $this->code
        );
    }
}
