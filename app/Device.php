<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Device extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $fillable = ['fcm_token', 'user_id', 'device_id'];

    public function generateTags(): array
    {
        return array(
            $this->device_id,
        );
    }

    public function users() {
        return $this->belongsTo(\App\User::class);
    }
}
