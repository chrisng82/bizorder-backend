<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PickListDtl extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    public function pickListHdr()
    {
        return $this->belongsTo(\App\PickListHdr::class, 'hdr_id', 'id');
    }

    public function item()
    {
        return $this->belongsTo(\App\Item::class, 'item_id', 'id');
    }

    public function uom()
    {
        return $this->belongsTo(\App\Uom::class, 'uom_id', 'id');
    }

    public function storageBin()
    {
        return $this->belongsTo(\App\StorageBin::class, 'storage_bin_id', 'id');
    }

    public function quantBal()
    {
        return $this->belongsTo(\App\QuantBal::class, 'quant_bal_id', 'id');
    }

    public function toStorageBin()
    {
        return $this->belongsTo(\App\StorageBin::class, 'to_storage_bin_id', 'id');
    }

    public function generateTags(): array
    {
        $docCode = $this->pickListHdr->doc_code;
        unset($this->pickListHdr);
        return array(
            $docCode
        );
    }
}
