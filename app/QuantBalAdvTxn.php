<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuantBalAdvTxn extends Model
{
    //
    protected $casts = [
        'uom_rate' => 'decimal:6',
        'unit_qty' => 'decimal:6',
    ];

    public function docHdr()
    {
        return $this->morphTo();
    }
}
