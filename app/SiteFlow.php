<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class SiteFlow extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //

    public function site()
    {
        return $this->belongsTo(\App\Site::class, 'site_id', 'id');
    }

    public function generateTags(): array
    {
        return array(
            $this->code,
        );
    }
}
