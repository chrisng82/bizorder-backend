<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class DelOrdDtl extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $casts = [
        'uom_rate' => 'decimal:6',
        'sale_price' => 'decimal:8',
        'price_disc' => 'decimal:8',
        'qty' => 'decimal:6',
        'gross_amt' => 'decimal:8',
        'gross_local_amt' => 'decimal:8',
        'dtl_disc_val_01' => 'decimal:8',
        'dtl_disc_perc_01' => 'decimal:5',
        'dtl_disc_val_02' => 'decimal:8',
        'dtl_disc_perc_02' => 'decimal:5',
        'dtl_disc_val_03' => 'decimal:8',
        'dtl_disc_perc_03' => 'decimal:5',
        'dtl_disc_val_04' => 'decimal:8',
        'dtl_disc_perc_04' => 'decimal:5',
        'dtl_disc_val_05' => 'decimal:8',
        'dtl_disc_perc_05' => 'decimal:5',
        'dtl_disc_amt' => 'decimal:8',
        'dtl_disc_local_amt' => 'decimal:8',
        'hdr_disc_val_01' => 'decimal:8',
        'hdr_disc_perc_01' => 'decimal:5',
        'hdr_disc_val_02' => 'decimal:8',
        'hdr_disc_perc_02' => 'decimal:5',
        'hdr_disc_val_03' => 'decimal:8',
        'hdr_disc_perc_03' => 'decimal:5',
        'hdr_disc_val_04' => 'decimal:8',
        'hdr_disc_perc_04' => 'decimal:5',
        'hdr_disc_val_05' => 'decimal:8',
        'hdr_disc_perc_05' => 'decimal:5',
        'hdr_disc_amt' => 'decimal:8',
        'hdr_disc_local_amt' => 'decimal:8',
        'dtl_taxable_amt_01' => 'decimal:8',
        'dtl_tax_val_01' => 'decimal:8',
        'dtl_tax_perc_01' => 'decimal:5',
        'dtl_tax_adj_01' => 'decimal:8',
        'dtl_tax_amt_01' => 'decimal:8',
        'dtl_tax_local_amt_01' => 'decimal:8',
        'dtl_taxable_amt_02' => 'decimal:8',
        'dtl_tax_val_02' => 'decimal:8',
        'dtl_tax_perc_02' => 'decimal:5',
        'dtl_tax_adj_02' => 'decimal:8',
        'dtl_tax_amt_02' => 'decimal:8',
        'dtl_tax_local_amt_02' => 'decimal:8',
        'dtl_taxable_amt_03' => 'decimal:8',
        'dtl_tax_val_03' => 'decimal:8',
        'dtl_tax_perc_03' => 'decimal:5',
        'dtl_tax_adj_03' => 'decimal:8',
        'dtl_tax_amt_03' => 'decimal:8',
        'dtl_tax_local_amt_03' => 'decimal:8',
        'dtl_taxable_amt_04' => 'decimal:8',
        'dtl_tax_val_04' => 'decimal:8',
        'dtl_tax_perc_04' => 'decimal:5',
        'dtl_tax_adj_04' => 'decimal:8',
        'dtl_tax_amt_04' => 'decimal:8',
        'dtl_tax_local_amt_04' => 'decimal:8',
        'dtl_taxable_amt_05' => 'decimal:8',
        'dtl_tax_val_05' => 'decimal:8',
        'dtl_tax_perc_05' => 'decimal:5',
        'dtl_tax_adj_05' => 'decimal:8',
        'dtl_tax_amt_05' => 'decimal:8',
        'dtl_tax_local_amt_05' => 'decimal:8',
        'hdr_taxable_amt_01' => 'decimal:8',
        'hdr_tax_val_01' => 'decimal:8',
        'hdr_tax_perc_01' => 'decimal:5',
        'hdr_tax_adj_01' => 'decimal:8',
        'hdr_tax_amt_01' => 'decimal:8',
        'hdr_tax_local_amt_01' => 'decimal:8',
        'hdr_taxable_amt_02' => 'decimal:8',
        'hdr_tax_val_02' => 'decimal:8',
        'hdr_tax_perc_02' => 'decimal:5',
        'hdr_tax_adj_02' => 'decimal:8',
        'hdr_tax_amt_02' => 'decimal:8',
        'hdr_tax_local_amt_02' => 'decimal:8',
        'hdr_taxable_amt_03' => 'decimal:8',
        'hdr_tax_val_03' => 'decimal:8',
        'hdr_tax_perc_03' => 'decimal:5',
        'hdr_tax_adj_03' => 'decimal:8',
        'hdr_tax_amt_03' => 'decimal:8',
        'hdr_tax_local_amt_03' => 'decimal:8',
        'hdr_taxable_amt_04' => 'decimal:8',
        'hdr_tax_val_04' => 'decimal:8',
        'hdr_tax_perc_04' => 'decimal:5',
        'hdr_tax_adj_04' => 'decimal:8',
        'hdr_tax_amt_04' => 'decimal:8',
        'hdr_tax_local_amt_04' => 'decimal:8',
        'hdr_taxable_amt_05' => 'decimal:8',
        'hdr_tax_val_05' => 'decimal:8',
        'hdr_tax_perc_05' => 'decimal:5',
        'hdr_tax_adj_05' => 'decimal:8',
        'hdr_tax_amt_05' => 'decimal:8',
        'hdr_tax_local_amt_05' => 'decimal:8',
        'net_amt' => 'decimal:8',
        'net_local_amt' => 'decimal:8',
    ];
    
    public function delOrdHdr()
    {
        return $this->belongsTo(\App\DelOrdHdr::class, 'hdr_id', 'id');
    }

    public function item()
    {
        return $this->belongsTo(\App\Item::class, 'item_id', 'id');
    }

    public function generateTags(): array
    {
        $docCode = $this->delOrdHdr->doc_code;
        unset($this->delOrdHdr);
        return array(
            $docCode,
        );
    }
}
