<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class UserSiteFlow extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id', 'id');
    }

    public function generateTags(): array
    {
        $username = $this->user->username;
        unset($this->user);
        return array(
            $username,
        );
    }
}
