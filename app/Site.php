<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Site extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //
    protected $casts = [
        'layout_width' => 'decimal:8',
        'layout_depth' => 'decimal:8',
        'layout_height' => 'decimal:8',
        'latitude' => 'decimal:8',
        'longitude' => 'decimal:8',
    ];

    public function generateTags(): array
    {
        return array(
            $this->code
        );
    }
}
