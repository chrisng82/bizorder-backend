<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Item extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $table = 'items';

    protected $fillable = ['code'];

    protected $casts = [
        'case_uom_rate' => 'decimal:6',
        'pallet_uom_rate' => 'decimal:6',
        'case_gross_weight' => 'decimal:4',
    ];

    public function itemUoms()
    {
        return $this->hasMany(\App\ItemUom::class, 'item_id','id')->orderBy('uom_rate');
    }

    public function unitUom()
    {
        return $this->belongsTo(\App\Uom::class, 'unit_uom_id', 'id');
    }

    public function caseUom()
    {
        return $this->belongsTo(\App\Uom::class, 'case_uom_id', 'id');
    }

    public function itemPrices()
    {
        return $this->hasMany(\App\ItemSalePrice::class, 'item_id','id');
    }

    public function itemPhotos()
    {
        return $this->hasMany(\App\ItemPhoto::class, 'item_id','id');
    }

    public function itemGroup01()
    {
        return $this->belongsTo(\App\ItemGroup01::class, 'item_group_01_id', 'id');
    }

    public function itemGroup02()
    {
        return $this->belongsTo(\App\ItemGroup02::class, 'item_group_02_id', 'id');
    }

    public function itemGroup03()
    {
        return $this->belongsTo(\App\ItemGroup03::class, 'item_group_03_id', 'id');
    }

    public function itemGroup04()
    {
        return $this->belongsTo(\App\ItemGroup04::class, 'item_group_04_id', 'id');
    }

    public function itemGroup05()
    {
        return $this->belongsTo(\App\ItemGroup05::class, 'item_group_05_id', 'id');
    }

    public function generateTags(): array
    {
        return array(
            $this->code
        );
    }
}
