<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ItemCond01 extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //

    public function generateTags(): array
    {
        return array(
            $this->code
        );
    }
}
