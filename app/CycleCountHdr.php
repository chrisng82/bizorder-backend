<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class CycleCountHdr extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $casts = [
        'doc_date' => 'date'
    ];
    
    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id', 'id');
    }

    public function siteFlow()
    {
        return $this->belongsTo(\App\SiteFlow::class, 'site_flow_id', 'id');
    }

    public function toDocTxnFlows()
    {
        return $this->morphMany(\App\DocTxnFlow::class, 'fr_doc_hdr');
    }

    public function frDocTxnVoids()
    {
        return $this->morphMany(\App\DocTxnVoid::class, 'to_doc_hdr');
    }

    public function frDocTxnFlows()
    {
        return $this->morphMany(\App\DocTxnFlow::class, 'to_doc_hdr');
    }

    public function cycleCountDtls()
    {
        return $this->hasMany(\App\CycleCountDtl::class, 'hdr_id', 'id');
    }

    public function generateTags(): array
    {
        return array(
            $this->doc_code
        );
    }
}
