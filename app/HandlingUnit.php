<?php

namespace App;

use App\Services\Env\ResType;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class HandlingUnit extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //
    protected $fillable = ['ref_code_01', 'ref_code_02', 'site_flow_id', 'handling_type'];

    public function getBarcode()
    {
        return ResType::$MAP['HANDLING_UNIT'].str_pad($this->id, 11, '0', STR_PAD_LEFT);
    }
}
