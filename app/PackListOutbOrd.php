<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PackListOutbOrd extends Model implements Auditable
{
    //
    use \OwenIt\Auditing\Auditable;

    public function packListHdr()
    {
        return $this->belongsTo(\App\PackListHdr::class, 'hdr_id', 'id');
    }

    public function generateTags(): array
    {
        $docCode = $this->packListHdr->doc_code;
        unset($this->packListHdr);
        return array(
            $docCode
        );
    }
}
