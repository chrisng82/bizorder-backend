<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class SlsOrdDtlPromotion extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //
    protected $casts = [
        'sale_price' => 'decimal:8',
        'price_disc' => 'decimal:8',
        'dtl_disc_val_01' => 'decimal:8',
        'dtl_disc_perc_01' => 'decimal:5',
        'dtl_disc_val_02' => 'decimal:8',
        'dtl_disc_perc_02' => 'decimal:5',
        'dtl_disc_val_03' => 'decimal:8',
        'dtl_disc_perc_03' => 'decimal:5',
        'dtl_disc_val_04' => 'decimal:8',
        'dtl_disc_perc_04' => 'decimal:5',
    ];

    public function slsOrdDtl()
    {
        return $this->belongsTo(\App\SlsOrdDtl::class, 'sls_ord_dtl_id', 'id');
    }

    public function promotion()
    {
        return $this->belongsTo(\App\Models\Promotion::class, 'promotion_id', 'id');
    }
}
