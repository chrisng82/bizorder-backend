<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PromoDebtor extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    public function promoHdr()
    {
        return $this->belongsTo(\App\PromoHdr::class, 'hdr_id', 'id');
    }

    public function generateTags(): array
    {
        $code = $this->promoHdr->code;
        unset($this->promoHdr);
        return array(
            $code
        );
    }
}
