<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SPalletType extends Model
{
    //
    protected $casts = [
        'pallet_weight' => 'decimal:4',
    ];
}
