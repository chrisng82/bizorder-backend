<?php

namespace App\Exports;

use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\BeforeWriting;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Contracts\View\View;

class StockBalanceExport implements FromView, WithEvents
{
    use RegistersEventListeners;

    public static $HEADING = array(
        'STORAGE BIN',
        'STORAGE ROW',
        'STORAGE BAY',
        'ITEM CODE',
        'DESCRIPTION',
        'BATCH NO',
        'EXPIRY DATE',
        'RECEIPT DATE',
        'CASE QTY',
        'CASE UOM',
        'CASE RATE',
        'LOOSE QTY',
        'LOOSE UOM',
        'LOOSE RATE',
        'TTL UNIT QTY'
    );

    private $quantBals = array();

    public function __construct($quantBals) {
        $this->quantBals = $quantBals;
    }

    public function view(): View
    {
        $ttlRows = array();

        foreach($this->quantBals as $quantBal)
        {
            $ttlRows[] = array(
                array('data'=>$quantBal->storage_bin_code), //STORAGE BIN
                array('data'=>$quantBal->storage_row_code), //STORAGE ROW
                array('data'=>$quantBal->storage_bay_code), //STORAGE BAY
                array('data'=>$quantBal->item_code), //ITEM CODE
                array('data'=>$quantBal->item_desc_01), //DESCRIPTION
                array('data'=>$quantBal->batch_serial_no), //BATCH NO
                array('data'=>$quantBal->expiry_date), //EXPIRY DATE
                array('data'=>$quantBal->receipt_date), //RECEIPT DATE
                array('data'=>$quantBal->case_qty), //CASE QTY
                array('data'=>$quantBal->item_case_uom_code), //CASE UOM
                array('data'=>$quantBal->case_uom_rate), //CASE RATE
                array('data'=>$quantBal->loose_qty), //LOOSE QTY
                array('data'=>$quantBal->item_loose_uom_code), //LOOSE UOM
                array('data'=>$quantBal->loose_uom_rate), //LOOSE RATE
                array('data'=>$quantBal->balance_unit_qty), //TTL UNIT QTY
            );
        }

        return view('excels.stockBalance', [
            'heading' => self::$HEADING,
            'rows' => $ttlRows
        ]);
    }
    
    public function columnFormats(): array
    {
        return [
            //'A' => NumberFormat::FORMAT_TEXT,
        ];
    }

    public static function beforeExport(BeforeExport $event)
    {
    }

    public static function beforeSheet(BeforeSheet $event)
    {
    }

    public static function afterSheet(AfterSheet $event)
    {
    }

    public static function beforeWriting(BeforeWriting $event)
    {
    }    
}
