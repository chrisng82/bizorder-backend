<?php

namespace App\Exports;

use App\Services\Env\LocationType;
use App\Services\Env\RackType;
use App\Services\Env\BinType;
use App\Services\Env\HandlingType;
use App\Services\Env\ResStatus;
use App\Services\Env\ResType;
use App\Repositories\ItemRepository;
use App\Repositories\BatchJobStatusRepository;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\BeforeWriting;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Support\Facades\DB;

class StorageBinExcel01Export implements FromQuery, WithMapping, WithHeadings, WithColumnFormatting, WithEvents
{
    use RegistersEventListeners;

    private $userId;

    private $batchJobStatus;

    private $headings;

    private $total = 1;

    private $rowNumber = 0;

    public function __construct($userId, $batchJobStatus, $headings) {
        $this->userId = $userId;
        $this->batchJobStatus = $batchJobStatus;
        $this->headings = $headings;
    }

    public function query()
    {
        $query = DB::table('storage_bins')
            ->selectRaw('storage_bins.*')
            ->selectRaw('locations.code AS location_code')
            ->selectRaw('locations.location_type AS location_type')
            ->selectRaw('storage_rows.code AS storage_row_code')
            ->selectRaw('storage_rows.aisle_code AS aisle_code')
            ->selectRaw('storage_rows.aisle_sequence AS aisle_sequence')
            ->selectRaw('storage_rows.aisle_position AS aisle_position')
            ->selectRaw('storage_bays.code AS storage_bay_code')
            ->selectRaw('storage_types.code AS storage_type_code')
            ->selectRaw('storage_types.handling_type AS handling_type')
            ->selectRaw('storage_types.bin_type AS bin_type')
            ->selectRaw('storage_types.rack_type AS rack_type')
            ->selectRaw('storage_types.hu_min_load_length AS hu_min_load_length')
            ->selectRaw('storage_types.hu_min_load_width AS hu_min_load_width')
            ->selectRaw('storage_types.hu_min_load_height AS hu_min_load_height')
            ->selectRaw('storage_types.hu_min_load_weight AS hu_min_load_weight')
            ->selectRaw('storage_types.hu_max_load_length AS hu_max_load_length')
            ->selectRaw('storage_types.hu_max_load_width AS hu_max_load_width')
            ->selectRaw('storage_types.hu_max_load_height AS hu_max_load_height')
            ->selectRaw('storage_types.hu_max_load_weight AS hu_max_load_weight')
            ->selectRaw('storage_types.hu_max_count AS hu_max_count')
            ->leftJoin('locations', 'locations.id', '=', 'storage_bins.location_id')
            ->leftJoin('storage_rows', 'storage_rows.id', '=', 'storage_bins.storage_row_id')
            ->leftJoin('storage_bays', 'storage_bays.id', '=', 'storage_bins.storage_bay_id')
            ->leftJoin('storage_types', 'storage_types.id', '=', 'storage_bins.storage_type_id')
            ->orderBy('storage_bins.code', 'ASC');

        $this->total = $query->count();

        return $query;
    }

    public function headings(): array
    {
        return array_keys($this->headings);
    }

    public function map($model): array
    {
        $this->rowNumber++;
        $statusNumber = bcmul(bcdiv($this->rowNumber, $this->total, 8), 100, 2);
        BatchJobStatusRepository::updateStatusNumber($this->batchJobStatus->id, $statusNumber);

        $data = array(
            $model->code,
            ResType::$MAP['STORAGE_BIN'].str_pad($model->id, 6, '0', STR_PAD_LEFT),
            $model->desc_01,
            $model->desc_02,
            ResStatus::$MAP[$model->bin_status],
            $model->aisle_code,
            $model->aisle_sequence,
            $model->aisle_position,
            $model->storage_row_code,
            $model->storage_bay_code,
            $model->floor,
            $model->level,
            $model->location_code,
            LocationType::$MAP[$model->location_type],
            $model->storage_type_code,
            HandlingType::$MAP[$model->handling_type],
            BinType::$MAP[$model->bin_type],
            RackType::$MAP[$model->rack_type],
            $model->hu_min_load_length,
            $model->hu_min_load_width,
            $model->hu_min_load_height,
            $model->hu_min_load_weight,
            $model->hu_max_load_length,
            $model->hu_max_load_width,
            $model->hu_max_load_height,
            $model->hu_max_load_weight,
            $model->hu_max_count
        );

        return $data;
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
        ];
    }

    public static function beforeExport(BeforeExport $event)
    {
    }

    public static function beforeSheet(BeforeSheet $event)
    {
    }

    public static function afterSheet(AfterSheet $event)
    {
    }

    public static function beforeWriting(BeforeWriting $event)
    {
    }    
}
