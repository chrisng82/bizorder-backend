<?php

namespace App\Exports;

use App\Uom;
use App\SiteFlow;
use App\StorageBin;
use App\QuantBal;
use App\Services\Env\RetrievalMethod;
use App\Services\Env\ScanMode;
use App\Services\Env\ItemType;
use App\Services\Env\StorageClass;
use App\Services\Env\ResStatus;
use App\Services\ItemService;
use App\Repositories\QuantBalRepository;
use App\Repositories\BatchJobStatusRepository;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ItemExcel01Export implements WithMultipleSheets
{
    public static $ITEM_EXCEL_01 = array(
        'CODE',
        'REF_CODE_01',
        'DESC_01',
        'DESC_02',
        'STATUS',
        'STORAGE_CLASS',
        'BRAND',
        'CATEGORY',
        'MANUFACTURER',
        'ITEM_TYPE',
        'SCAN_MODE',
        'RETRIEVAL',
        'CASE_EXT_LENGTH(MM)',
        'CASE_EXT_WIDTH(MM)',
        'CASE_EXT_HEIGHT(MM)',
        'CASE_GROSS_WEIGHT(KG)',
        'CASES_PER_LENGTH',
        'CASES_PER_WIDTH',
        'NO_OF_LAYERS',
        'QTY_SCALE',
        'PHOTO_COUNT',
        'UNIT_UOM',
        'UNIT_BARCODE',
        'CASE_UOM',
        'CASE_BARCODE',
        'CASE_RATE',
        'PALLET_BARCODE',
        'PALLET_RATE',
        'OTHER_UOM_1',
        'OTHER_BARCODE_1',
        'OTHER_RATE_1',
        'OTHER_UOM_2',
        'OTHER_BARCODE_2',
        'OTHER_RATE_2',
        'OTHER_UOM_3',
        'OTHER_BARCODE_3',
        'OTHER_RATE_3',
        'OTHER_UOM_4',
        'OTHER_BARCODE_4',
        'OTHER_RATE_4',
        'OTHER_UOM_5',
        'OTHER_BARCODE_5',
        'OTHER_RATE_5',
    );

    private $sheets = null;

    private $userId;

    private $batchJobStatus;

    private $total = 0;

    private $rowNumber = 0;

    public function __construct($userId, $batchJobStatus) {
        $this->userId = $userId;
        $this->batchJobStatus = $batchJobStatus;
    }

    public function sheets(): array
    {
        if(!empty($this->sheets))
        {
            //dun know why the sheets() function will be called 2 times, 
            //so here will return if the sheets already built
            return $this->sheets;
        }

        $this->sheets = array();
        
        $itemsHashByItemGroup01 = array();
        $itemModels = DB::table('items')
            ->selectRaw('items.*')
            ->selectRaw('unit_uoms.code AS unit_uom_code')
            ->selectRaw('case_uoms.code AS case_uom_code')
            ->selectRaw('item_group01s.code AS item_group_01_code')
            ->selectRaw('item_group02s.code AS item_group_02_code')
            ->selectRaw('item_group03s.code AS item_group_03_code')
            ->selectRaw('item_group04s.code AS item_group_04_code')
            ->selectRaw('item_group05s.code AS item_group_05_code')
            ->leftJoin('uoms AS unit_uoms', 'unit_uoms.id', '=', 'items.unit_uom_id')
            ->leftJoin('uoms AS case_uoms', 'case_uoms.id', '=', 'items.case_uom_id')
            ->leftJoin('item_group01s', 'item_group01s.id', '=', 'items.item_group_01_id')
            ->leftJoin('item_group02s', 'item_group02s.id', '=', 'items.item_group_02_id')
            ->leftJoin('item_group03s', 'item_group03s.id', '=', 'items.item_group_03_id')
            ->leftJoin('item_group04s', 'item_group04s.id', '=', 'items.item_group_04_id')
            ->leftJoin('item_group05s', 'item_group05s.id', '=', 'items.item_group_05_id')
            ->orderBy('item_group01s.code')
            ->orderBy('items.code')
            ->where('items.item_type', ItemType::$MAP['STOCK_ITEM']) //exclude the PALLET
            ->get();

        $this->total = count($itemModels);
        foreach($itemModels as $itemModel)
        {
            $this->rowNumber++;
            $statusNumber = bcmul(bcdiv($this->rowNumber, $this->total, 8), 100, 2);
            BatchJobStatusRepository::updateStatusNumber($this->batchJobStatus->id, $statusNumber);

            $unitBarcode = '';
            $caseBarcode = '';
            $palletBarcode = '';
            $otherItemUoms = array();
            $itemUomModels = DB::table('item_uoms')
                ->selectRaw('item_uoms.*')
                ->selectRaw('uoms.code AS uom_code')
                ->leftJoin('uoms', 'uoms.id', '=', 'item_uoms.uom_id')
                ->where('item_id', $itemModel->id)
                ->orderBy('item_uoms.uom_rate')
                ->get();
            foreach($itemUomModels as $itemUomModel)
            {
                if($itemUomModel->uom_id == $itemModel->unit_uom_id)
                {
                    $unitBarcode = $itemUomModel->barcode;
                }
                elseif($itemUomModel->uom_id == $itemModel->case_uom_id)
                {
                    $caseBarcode = $itemUomModel->barcode;
                }
                elseif($itemUomModel->uom_id == Uom::$PALLET)
                {
                    $palletBarcode = $itemUomModel->barcode;
                }
                else
                {
                    $otherItemUoms[] = $itemUomModel;
                }
            }

            //DB::connection()->enableQueryLog();
            $itemPhotoCount = DB::table('item_photos')
                ->where('item_id', $itemModel->id)
                ->count();
            //Log::error(DB::getQueryLog());

            $itemData = array(
                array('data'=>$itemModel->code),
                array('data'=>$itemModel->ref_code_01),
                array('data'=>$itemModel->desc_01),
                array('data'=>$itemModel->desc_02),
                array('data'=>ResStatus::$MAP[$itemModel->status]),
                array('data'=>StorageClass::$MAP[$itemModel->storage_class]),
                array('data'=>$itemModel->item_group_01_code),
                array('data'=>$itemModel->item_group_02_code),
                array('data'=>$itemModel->item_group_03_code),
                array('data'=>ItemType::$MAP[$itemModel->item_type]),
                array('data'=>ScanMode::$MAP[$itemModel->scan_mode]),
                array('data'=>RetrievalMethod::$MAP[$itemModel->retrieval_method]),
                array('data'=>$itemModel->case_ext_length),
                array('data'=>$itemModel->case_ext_width),
                array('data'=>$itemModel->case_ext_height),
                array('data'=>$itemModel->case_gross_weight),
                array('data'=>$itemModel->cases_per_pallet_length),
                array('data'=>$itemModel->cases_per_pallet_width),
                array('data'=>$itemModel->no_of_layers),
                array('data'=>$itemModel->qty_scale),
                array('data'=>$itemPhotoCount),
                array('data'=>$itemModel->unit_uom_code),
                array('data'=>$unitBarcode),
                array('data'=>$itemModel->case_uom_code),
                array('data'=>$caseBarcode),
                array('data'=>$itemModel->case_uom_rate),
                array('data'=>$itemModel->pallet_uom_rate),
                array('data'=>$palletBarcode),
            );

            foreach($otherItemUoms as $otherItemUom)
            {
                $itemData[] = array('data'=>$otherItemUom->uom_code);
                $itemData[] = array('data'=>$otherItemUom->barcode);
                $itemData[] = array('data'=>$otherItemUom->uom_rate);
            }
            
            $tmpItemArray = array();
            if(array_key_exists($itemModel->item_group_01_code, $itemsHashByItemGroup01))
            {
                $tmpItemArray = $itemsHashByItemGroup01[$itemModel->item_group_01_code];
            }
            $tmpItemArray[] = $itemData;
            $itemsHashByItemGroup01[$itemModel->item_group_01_code] = $tmpItemArray;
        }

        foreach($itemsHashByItemGroup01 as $itemGroup01Code => $itemDataArray)
        {
            if(empty($itemGroup01Code)) 
            {
                $itemGroup01Code = '-';
            }
            $this->sheets[] = new ItemExcel01Sheet(self::$ITEM_EXCEL_01, $itemGroup01Code, $itemDataArray);
        }

        return $this->sheets;
    }
}
