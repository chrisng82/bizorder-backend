<?php

namespace App\Exports;

use App\SiteFlow;
use App\StorageBin;
use App\QuantBal;
use App\Services\ItemService;
use App\Repositories\QuantBalRepository;
use App\Repositories\BatchJobStatusRepository;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Illuminate\Support\Facades\DB;

class CycleCountExcel01Export implements WithMultipleSheets
{
    public static $QUANT_BAL_LABEL = array(
        'COMPANY CODE',
        'PALLET ID',
        'PALLET REF',
        'PRODUCT CODE',
        'DESC 01',
        'CTN',
        'QTY',
        'UOM',
        'EXPIRY DATE'
    );

    public static $BACKGROUND_COLOR = array(
        '#FFFF66', //light yellow
        '#FFA07A', //lightsalmon
    );

    public static $FONT_COLOR = array(
        '#0000FF', //blue
        '#FF0000', //red
    );

    private $sheets = null;

    private $userId;

    private $batchJobStatus;

    private $siteFlowId;

    public function __construct($userId, $batchJobStatus, $siteFlowId) {
        $this->userId = $userId;
        $this->batchJobStatus = $batchJobStatus;
        $this->siteFlowId = $siteFlowId;
    }

    public function sheets(): array
    {
        if(!empty($this->sheets))
        {
            //dun know why the sheets() function will be called 2 times, 
            //so here will return if the sheets already built
            return $this->sheets;
        }

        $this->sheets = array();

        $siteFlow = SiteFlow::where('id', $this->siteFlowId)
            ->first();
        $storageBins = StorageBin::get();
        $storageBins->load('storageBay', 'storageRow');

        //format to hash by rowCode
        $storageRowHash = array();
        $storageBaysHashByRow = array();
        $storageLevelsHashByRow = array();
        $storageBinsHashByRow = array();
        foreach($storageBins as $storageBin)
        {
            $storageRow = $storageBin->storageRow;
            $storageBay = $storageBin->storageBay;

            $storageRowHash[$storageRow->code] = $storageRow;

            //format bays by row code
            $tmpStorageBays = array();
            if(array_key_exists($storageRow->code, $storageBaysHashByRow))
            {
                $tmpStorageBays = $storageBaysHashByRow[$storageRow->code];
            }
            $tmpStorageBays[$storageBay->code] = $storageBay;
            $storageBaysHashByRow[$storageRow->code] = $tmpStorageBays;

            //format level by row code
            $tmpLevels = array();
            if(array_key_exists($storageRow->code, $storageLevelsHashByRow))
            {
                $tmpLevels = $storageLevelsHashByRow[$storageRow->code];
            }
            $tmpLevels[$storageBin->level] = $storageBin->level;
            $storageLevelsHashByRow[$storageRow->code] = $tmpLevels;

            //format bin by row code and bay code
            $tmpStorageBins = array();
            $key = $storageRow->code;
            if(array_key_exists($key, $storageBinsHashByRow))
            {
                $tmpStorageBins = $storageBinsHashByRow[$key];
            }
            $tmpStorageBins[] = $storageBin;
            $storageBinsHashByRow[$key] = $tmpStorageBins;
        }

        //second loop to remove the duplicate entry
        $storageRows = array();
        foreach($storageRowHash as $rowCode => $storageRow)
        {
            $storageRows[] = $storageRow;
        }

        foreach($storageBaysHashByRow as $rowCode => $storageBays)
        {
            $storageBaysHash = array();
            foreach($storageBays as $storageBay)
            {
                $storageBaysHash[$storageBay->code] = $storageBay;
            }
            $storageBaysHashByRow[$rowCode] = array_values($storageBaysHash);
        }

        foreach($storageLevelsHashByRow as $rowCode => $storageLevels)
        {
            $storageLevelsHash = array();
            foreach($storageLevels as $storageLevel)
            {
                $storageLevelsHash[$storageLevel] = $storageLevel;
            }
            $storageLevelsHashByRow[$rowCode] = array_values($storageLevelsHash);
        }

        //here start to process each row to rows for excel
        foreach($storageRows as $storageRow)
        {
            $storageBays = $storageBaysHashByRow[$storageRow->code];
            $storageLevels = $storageLevelsHashByRow[$storageRow->code];
            $storageBins = $storageBinsHashByRow[$storageRow->code];
            
            //keep track the level max quantBal count
            $maxQuantBalCountByLevel = array();
            foreach($storageLevels as $storageLevel) 
            {
                $maxQuantBalCountByLevel[$storageLevel] = 1;
            }

            //keep track the level max bin count
            $maxStorageBinCountByBay = array();
            foreach($storageBays as $storageBay) 
            {
                $maxStorageBinCountByBay[$storageBay->code] = 0;
            }

            $binCodesHashByBay = array();
            foreach($storageBins as $storageBin)
            {
                $binBay = $storageBin->storageBay;
                $binCodesHash = array();
                if(array_key_exists($binBay->code, $binCodesHashByBay))
                {
                    $binCodesHash = $binCodesHashByBay[$binBay->code];
                }
                $binCodesHash[$storageBin->getAbbreviation()] = $storageBin->getAbbreviation();
                $binCodesHashByBay[$binBay->code] = $binCodesHash;

                $storageBinCount = count($binCodesHash);
                $maxStorageBinCount = $maxStorageBinCountByBay[$binBay->code];
                if(bccomp($storageBinCount, $maxStorageBinCount) > 0)
                {
                    $maxStorageBinCountByBay[$binBay->code] = $storageBinCount;
                }
            }

            $quantBals = QuantBalRepository::findAllActiveByStorageRowId($storageRow->id);
            $quantBals->load('company', 'storageBay', 'storageBin', 'item', 'handlingUnit', 'itemBatch');

            //format quantBals to hash by key
            $quantBalsHashByKey = array();
            foreach($quantBals as $quantBal)
            {
                $quantBalBay = $quantBal->storageBay;
                $quantBalBin = $quantBal->storageBin;
                //RowCode;BayCode;Level;A
                $key = $storageRow->code .';'.$quantBalBay->code.';'.$quantBal->level.';'.$quantBalBin->getAbbreviation();
                $tmpQuantBals = array();
                if(array_key_exists($key, $quantBalsHashByKey))
                {
                    $tmpQuantBals = $quantBalsHashByKey[$key];
                }
                $tmpQuantBals[] = $quantBal;
                $quantBalsHashByKey[$key] = $tmpQuantBals;

                $qCountByLevel = count($tmpQuantBals);
                $maxQCountByLevel = $maxQuantBalCountByLevel[$quantBal->level];
                if(bccomp($qCountByLevel, $maxQCountByLevel) > 0)
                {
                    $maxQuantBalCountByLevel[$quantBal->level] = $qCountByLevel;
                }
            }

            //first 3 rows is for row bay, bin, and label
            $row0Labels = array();
            $row1Labels = array();
            $row2Labels = array();

            $col0Labels = array();
            $col0Labels[] = '';
            $col0Labels[] = '';
            $col0Labels[] = '';
            krsort($maxQuantBalCountByLevel);
            foreach($maxQuantBalCountByLevel as $level => $maxQuantBalCount)
            {
                for($a = 0; $a < $maxQuantBalCount; $a++)
                {
                    $col0Labels[] = $level;
                }                
            }

            //process the labels for first 3 rows
            $rowColumnCount = 1;
            $row0Labels[] = array('data'=>$storageRow->code,'attrs'=>array('nodename'=> 'strong', 'style'=>'background-color:#00FF00', 'colspan'=>1, 'rowspan'=>1));
            $row1Labels[] = array('data'=>'', 'attrs'=>array('nodename'=> 'strong', 'style'=>'background-color:#00FF00', 'colspan'=>1, 'rowspan'=>1));
            $row2Labels[] = array('data'=>'', 'attrs'=>array('nodename'=> 'strong', 'style'=>'', 'colspan'=>1, 'rowspan'=>1));
            $bayIndex = 0;
            foreach($maxStorageBinCountByBay as $bayCode => $storageBinCount)
            {
                $bayBackgroundColor = self::$BACKGROUND_COLOR[$bayIndex % count(self::$BACKGROUND_COLOR)];

                $bayCodeParts = explode('-', $bayCode);
                $bayAbbreviation = $bayCodeParts[count($bayCodeParts) - 1];

                $binCodeHash = $binCodesHashByBay[$bayCode];

                $bayColspan = bcmul($storageBinCount, count(self::$QUANT_BAL_LABEL));
                $binColspan = count(self::$QUANT_BAL_LABEL);

                $columnNum = 0;

                $prevBayAbbreviation = '';
                $prevBinCode = '';
                for($a = 0; $a < $storageBinCount; $a++)
                {
                    $binBackgroundColor = self::$BACKGROUND_COLOR[$a % count(self::$BACKGROUND_COLOR)];
                    $binFontColor = self::$FONT_COLOR[$a % count(self::$FONT_COLOR)];

                    $binCodes = array_values($binCodeHash);
                    $binCode = $binCodes[$a];
                    for($b = 0; $b < count(self::$QUANT_BAL_LABEL); $b++)
                    {
                        $qLabel = self::$QUANT_BAL_LABEL[$b];
                        if(strcmp($prevBayAbbreviation, $bayAbbreviation) != 0)
                        {
                            $row0Labels[] = array('data'=>$bayAbbreviation, 'attrs'=>array('nodename'=> 'strong', 'style'=>'background-color:'.$bayBackgroundColor, 'colspan'=>$bayColspan, 'rowspan'=>1));
                            $prevBayAbbreviation = $bayAbbreviation;
                        }
                        
                        if(strcmp($prevBinCode, $binCode) != 0)
                        {
                            $row1Labels[] = array('data'=>$binCode, 'attrs'=>array('nodename'=> 'strong', 'style'=>'background-color:'.$binBackgroundColor, 'colspan'=>$binColspan, 'rowspan'=>1));
                            $prevBinCode = $binCode;
                        }

                        $row2Labels[] = array('data'=>$qLabel, 'attrs'=>array('nodename'=> 'strong', 'style'=>'color:'.$binFontColor, 'colspan'=>1, 'rowspan'=>1));

                        $columnNum++;
                    }
                }

                $rowColumnCount = bcadd($rowColumnCount, $columnNum);

                $bayIndex++;
            }

            //get the total rows and total columns
            $rowRows = array();
            $rowRows[] = $row0Labels;
            $rowRows[] = $row1Labels;
            $rowRows[] = $row2Labels;
            for($a = 3; $a < count($col0Labels); $a++)
            {
                $level = $col0Labels[$a];

                $rowCols = array();
                $rowCols[] = array('data'=>$level, 'attrs'=>array('nodename'=> 'div', 'style'=>'', 'colspan'=>1, 'rowspan'=>1)); //level
                foreach($maxStorageBinCountByBay as $bayCode => $storageBinCount)
                {
                    $binCodeHash = $binCodesHashByBay[$bayCode];

                    for($b = 0; $b < $storageBinCount; $b++)
                    {
                        $binCodes = array_values($binCodeHash);
                        $binCode = $binCodes[$b];

                        //RowCode;BayCode;Level;A
                        $key = $storageRow->code .';'.$bayCode.';'.$level.';'.$binCode;
                        $quantBals = array();
                        if(array_key_exists($key, $quantBalsHashByKey))
                        {
                            $quantBals = $quantBalsHashByKey[$key];
                        }
                        $quantBal = null;
                        if(count($quantBals) >= 1)
                        {
                            //pop the first element, so next loop will not print it
                            $quantBal = array_shift($quantBals);
                            $quantBalsHashByKey[$key] = $quantBals;
                        }

                        if(!empty($quantBal))
                        {
                            $company = $quantBal->company;
                            $item = $quantBal->item;
                            $itemBatch = $quantBal->itemBatch;
                            $handlingUnit = $quantBal->handlingUnit;

                            $quantBal = ItemService::processCaseLoose($quantBal, $item, 1);
                            $rowCols[] = array('data'=>$company->code, 'attrs'=>array('nodename'=> 'div', 'style'=>'', 'colspan'=>1, 'rowspan'=>1)); //COMPANY CODE
                            $rowCols[] = array('data'=>!empty($handlingUnit) ? $handlingUnit->getBarcode() : '', 'attrs'=>array('nodename'=> 'div', 'style'=>'', 'colspan'=>1, 'rowspan'=>1)); //PALLET ID
                            $rowCols[] = array('data'=>!empty($handlingUnit) ? $handlingUnit->ref_code_01 : '', 'attrs'=>array('nodename'=> 'div', 'style'=>'', 'colspan'=>1, 'rowspan'=>1)); //PALLET REF
                            $rowCols[] = array('data'=>$item->code, 'attrs'=>array('nodename'=> 'div', 'style'=>'', 'colspan'=>1, 'rowspan'=>1)); //PRODUCT CODE
                            $rowCols[] = array('data'=>$item->desc_01, 'attrs'=>array('nodename'=> 'div', 'style'=>'', 'colspan'=>1, 'rowspan'=>1)); //DESC 01
                            $rowCols[] = array('data'=>number_format($quantBal->case_qty, config('scm.decimal_scale')), 'attrs'=>array('nodename'=> 'div', 'style'=>'', 'colspan'=>1, 'rowspan'=>1)); //CTN
                            $rowCols[] = array('data'=>number_format($quantBal->loose_qty, config('scm.decimal_scale')), 'attrs'=>array('nodename'=> 'div', 'style'=>'', 'colspan'=>1, 'rowspan'=>1)); //QTY
                            $rowCols[] = array('data'=>$quantBal->item_loose_uom_code, 'attrs'=>array('nodename'=> 'div', 'style'=>'', 'colspan'=>1, 'rowspan'=>1)); //UOM
                            $rowCols[] = array('data'=>$itemBatch->expiry_date, 'attrs'=>array('nodename'=> 'div', 'style'=>'', 'colspan'=>1, 'rowspan'=>1)); //EXPIRY DATE
                        }
                        else
                        {
                            for($c = 0; $c < count(self::$QUANT_BAL_LABEL); $c++)
                            {
                                $rowCols[] = array('data'=>'', 'attrs'=>array('nodename'=> 'div', 'style'=>'', 'colspan'=>1, 'rowspan'=>1));
                            }   
                        }         
                    }
                }

                $rowRows[] = $rowCols;
            }

            //add 3 blank row
            $rowRows[] = array(array('data'=>'', 'attrs'=>array('nodename'=> 'div', 'style'=>'', 'colspan'=>1, 'rowspan'=>1)));
            $rowRows[] = array(array('data'=>'', 'attrs'=>array('nodename'=> 'div', 'style'=>'', 'colspan'=>1, 'rowspan'=>1)));
            $rowRows[] = array(array('data'=>'', 'attrs'=>array('nodename'=> 'div', 'style'=>'', 'colspan'=>1, 'rowspan'=>1)));
            
            $this->sheets[] = new CycleCountExcel01Sheet($storageRow, $rowRows);
        }

        return $this->sheets;
    }
}
