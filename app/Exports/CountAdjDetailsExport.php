<?php

namespace App\Exports;

use App\Services\Env\LocationType;
use App\Services\Env\RackType;
use App\Services\Env\BinType;
use App\Services\Env\HandlingType;
use App\Services\Env\ResStatus;
use App\Services\Env\ResType;
use App\Repositories\ItemRepository;
use App\Repositories\BatchJobStatusRepository;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\BeforeWriting;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Support\Facades\DB;

class CountAdjDetailsExport implements FromQuery, WithMapping, WithHeadings, WithColumnFormatting, WithEvents
{
    use RegistersEventListeners;

    private $hdrId;

    private $batchJobStatus;

    private $headings;

    private $total = 1;

    private $rowNumber = 0;

    public function __construct($hdrId, $batchJobStatus) {
        $this->hdrId = $hdrId;
        $this->batchJobStatus = $batchJobStatus;
    }

    public function query()
    {
        $query = DB::table('count_adj_dtls')
            ->selectRaw('count_adj_dtls.*')
            ->selectRaw('storage_bins.code AS storage_bin_code')
            ->selectRaw('count_adj_dtls.handling_unit_id AS handling_unit_id')
            ->selectRaw('items.code AS item_code')
            ->selectRaw('items.desc_01 AS item_desc_01')
            ->selectRaw('items.desc_02 AS item_desc_02')
            ->selectRaw('count_adj_dtls.desc_01 AS desc_01')
            ->selectRaw('count_adj_dtls.desc_02 AS desc_02')
            ->selectRaw('item_batches.batch_serial_no AS batch_serial_no')
            ->selectRaw('item_batches.expiry_date AS expiry_date')
            ->selectRaw('item_batches.receipt_date AS receipt_date')
            ->selectRaw('count_adj_dtls.sign AS sign')
            ->selectRaw('count_adj_dtls.qty AS qty')
            ->selectRaw('uoms.code AS uom_code')
            ->selectRaw('count_adj_dtls.uom_rate AS uom_rate')
            ->leftJoin('count_adj_hdrs', 'count_adj_hdrs.id', '=', 'count_adj_dtls.hdr_id')
            ->leftJoin('storage_bins', 'storage_bins.id', '=', 'count_adj_dtls.storage_bin_id')
            ->leftJoin('items', 'items.id', '=', 'count_adj_dtls.item_id')
            ->leftJoin('uoms', 'uoms.id', '=', 'count_adj_dtls.uom_id')
            ->leftJoin('item_batches', 'item_batches.id', '=', 'count_adj_dtls.item_batch_id')
            ->where('count_adj_dtls.hdr_id', $this->hdrId)
            ->orderBy('count_adj_dtls.line_no', 'ASC');

        $this->total = $query->count();

        return $query;
    }

    public function headings(): array
    {
        return array(
            'LINE_NO',
            'STORAGE_BIN_CODE',
            'HANDLING_UNIT',
            'ITEM_CODE',
            'DESC_01',
            'DESC_02',
            'BATCH_NO',
            'EXPIRY_DATE',
            'RECEIPT_DATE',
            'SIGN',
            'QTY',
            'UOM_CODE',
            'UOM_RATE',
        );
    }

    public function map($model): array
    {
        $this->rowNumber++;
        $statusNumber = bcmul(bcdiv($this->rowNumber, $this->total, 8), 100, 2);
        BatchJobStatusRepository::updateStatusNumber($this->batchJobStatus->id, $statusNumber);

        $handlingUnitBarcode = '';
        if($model->handling_unit_id > 0) 
        {
            $handlingUnitBarcode = ResType::$MAP['HANDLING_UNIT'].str_pad($model->handling_unit_id, 11, '0', STR_PAD_LEFT);
        }
        
        $data = array(
            $model->line_no,
            $model->storage_bin_code,
            $handlingUnitBarcode,
            $model->item_code,
            $model->desc_01,
            $model->desc_02,
            $model->batch_serial_no,
            $model->expiry_date,
            $model->receipt_date,
            $model->sign,
            $model->qty,
            $model->uom_code,
            $model->uom_rate
        );

        return $data;
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_NUMBER,
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => NumberFormat::FORMAT_TEXT,
            'D' => NumberFormat::FORMAT_TEXT,
            'H' => NumberFormat::FORMAT_DATE_YYYYMMDD2,
            'I' => NumberFormat::FORMAT_DATE_YYYYMMDD2,
        ];
    }

    public static function beforeExport(BeforeExport $event)
    {
    }

    public static function beforeSheet(BeforeSheet $event)
    {
    }

    public static function afterSheet(AfterSheet $event)
    {
    }

    public static function beforeWriting(BeforeWriting $event)
    {
    }    
}
