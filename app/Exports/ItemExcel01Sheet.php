<?php

namespace App\Exports;

use App\SiteFlow;
use App\StorageBin;
use App\QuantBal;
use App\Services\ItemService;
use App\Repositories\QuantBalRepository;
use App\Repositories\BatchJobStatusRepository;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\BeforeWriting;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;

class ItemExcel01Sheet implements FromView, WithEvents, WithTitle, WithColumnFormatting
{
    use RegistersEventListeners;

    private $heading;

    private $itemGroup01Code;

    private $itemDataArray;

    public function __construct($heading, $itemGroup01Code, $itemDataArray) {
        $this->heading = $heading;
        $this->itemGroup01Code = $itemGroup01Code;
        $this->itemDataArray = $itemDataArray;
    }

    public function view(): View
    {
        return view('excels.itemExcel01', [
            'heading' => $this->heading,
            'rows' => $this->itemDataArray
        ]);
    }

    public static function beforeExport(BeforeExport $event)
    {
    }

    public static function beforeSheet(BeforeSheet $event)
    {
    }

    public static function afterSheet(AfterSheet $event)
    {
        $sheet = $event->sheet;
        $sheet->getDelegate()->freezePane('B2');
    }

    public static function beforeWriting(BeforeWriting $event)
    {
    }    

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
            'W' => NumberFormat::FORMAT_TEXT,
        ];
    }

    public function title(): string
    {
        return $this->itemGroup01Code;
    }   
}
