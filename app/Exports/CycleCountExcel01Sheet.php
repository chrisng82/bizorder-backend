<?php

namespace App\Exports;

use App\SiteFlow;
use App\StorageBin;
use App\QuantBal;
use App\Services\ItemService;
use App\Repositories\QuantBalRepository;
use App\Repositories\BatchJobStatusRepository;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\BeforeWriting;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;

class CycleCountExcel01Sheet implements FromView, WithEvents, WithTitle
{
    use RegistersEventListeners;

    private $storageRow;

    private $ttlRows;

    public function __construct($storageRow, $ttlRows) {
        $this->storageRow = $storageRow;
        $this->ttlRows = $ttlRows;
    }

    public function view(): View
    {
        return view('excels.cycleCountExcel01', [
            'rows' => $this->ttlRows
        ]);
    }

    public static function beforeExport(BeforeExport $event)
    {
    }

    public static function beforeSheet(BeforeSheet $event)
    {
    }

    public static function afterSheet(AfterSheet $event)
    {
        $sheet = $event->sheet;
        $sheet->getDelegate()->freezePane('B2');
    }

    public static function beforeWriting(BeforeWriting $event)
    {
    }    

    public function title(): string
    {
        return $this->storageRow->code;
    }
}
