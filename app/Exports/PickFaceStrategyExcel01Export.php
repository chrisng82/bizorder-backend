<?php

namespace App\Exports;

use App\Services\Env\LocationType;
use App\Services\Env\RackType;
use App\Services\Env\BinType;
use App\Services\Env\HandlingType;
use App\Services\Env\ResStatus;
use App\Services\Env\ResType;
use App\Repositories\ItemRepository;
use App\Repositories\BatchJobStatusRepository;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\BeforeWriting;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Support\Facades\DB;

class PickFaceStrategyExcel01Export implements FromQuery, WithMapping, WithHeadings, WithColumnFormatting, WithEvents
{
    use RegistersEventListeners;

    private $userId;

    private $batchJobStatus;

    private $total = 1;

    private $rowNumber = 0;

    public function __construct($userId, $batchJobStatus) {
        $this->userId = $userId;
        $this->batchJobStatus = $batchJobStatus;
    }

    public function query()
    {
        $query = DB::table('pick_face_strategies')
            ->selectRaw('pick_face_strategies.*')
            ->selectRaw('storage_bins.code AS storage_bin_code')
            ->selectRaw('items.code AS item_code')
            ->selectRaw('items.desc_01 AS item_desc_01')
            ->selectRaw('items.desc_02 AS item_desc_02')
            //->leftJoin('pick_face_strategies', 'storage_bins.id', '=', 'pick_face_strategies.storage_bin_id')
            ->leftJoin('storage_bins', 'storage_bins.id', '=', 'pick_face_strategies.storage_bin_id')
            ->leftJoin('items', 'items.id', '=', 'pick_face_strategies.item_id')
            ->orderBy('storage_bins.code', 'ASC')
            ->orderBy('items.code', 'ASC');

        $this->total = $query->count();

        return $query;
    }

    public function headings(): array
    {
        return array(
            'LINE_NO',
            'STORAGE_BIN_CODE',
            'ITEM_CODE',
            'DESC_01',
            'DESC_02',
        );
    }

    public function map($model): array
    {
        $this->rowNumber++;
        $statusNumber = bcmul(bcdiv($this->rowNumber, $this->total, 8), 100, 2);
        BatchJobStatusRepository::updateStatusNumber($this->batchJobStatus->id, $statusNumber);

        $data = array(
            $model->line_no,
            $model->storage_bin_code,
            $model->item_code,
            $model->item_desc_01,
            $model->item_desc_02,
        );

        return $data;
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
        ];
    }

    public static function beforeExport(BeforeExport $event)
    {
    }

    public static function beforeSheet(BeforeSheet $event)
    {
    }

    public static function afterSheet(AfterSheet $event)
    {
    }

    public static function beforeWriting(BeforeWriting $event)
    {
    }    
}
