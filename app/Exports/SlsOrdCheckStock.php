<?php

namespace App\Exports;

use App\Uom;
use App\Services\Env\RetrievalMethod;
use App\Services\Env\ScanMode;
use App\Services\Env\ItemType;
use App\Services\Env\StorageClass;
use App\Services\Env\ResStatus;
use App\Repositories\ItemRepository;
use App\Repositories\SlsOrdHdrRepository;
use App\Repositories\SlsOrdDtlRepository;
use App\Repositories\SiteFlowRepository;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\BeforeWriting;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\AfterSheet;
use App\Services\SlsOrdService;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;

class SlsOrdCheckStock implements FromView, WithEvents
{
    use RegistersEventListeners;

    private $userId;

    private $headings;

    private $total = 0;

    private $rowNumber = 0;

    public function __construct($userId, $hdrIds) {
        $this->userId = $userId;
        $this->hdrIds = $hdrIds;
    }

    public function view(): View
    {
        $ttlRows = array();
        $ttlRows[] = array(
            'DOC CODE' => 'DOC CODE',
            'AREA' => 'AREA',
            'CUSTOMER' => 'CUSTOMER',
            'NAME' => 'NAME',
            'LINE NO' => 'LINE NO',
            'ITEM' => 'ITEM',
            'DESC' => 'DESC',
            'CASE RATE' => 'CASE RATE',
            'REQUEST QTY' => 'REQUEST QTY',
            'AVAILABLE QTY' => 'AVAILABLE QTY'
        );

        $slsOrdHdrs = SlsOrdHdrRepository::findAllByHdrIds($this->hdrIds);
		//Log::error(DB::getQueryLog());
		$slsOrdHdrs->load(
            'siteFlow',
			'slsOrdDtls', 'slsOrdDtls.item',
			'division', 'deliveryPoint', 'salesman'
        );

        foreach($slsOrdHdrs as $slsOrdHdr)
        {
            $siteFlow = $slsOrdHdr->siteFlow;
            $deliveryPoint = $slsOrdHdr->deliveryPoint;
            
            $slsOrdDtls = $slsOrdHdr->slsOrdDtls;
            $slsOrdDtls = SlsOrdService::checkStock($siteFlow, $slsOrdHdr, $slsOrdDtls);
            
            foreach($slsOrdDtls as $slsOrdDtl)
            {
                if(bccomp($slsOrdDtl->request_qty, $slsOrdDtl->available_qty, 5) > 0)
                {
                    $slsOrdDtl = SlsOrdService::processOutgoingDetail($slsOrdDtl);

                    $item = $slsOrdDtl->item;

                    $ttlRows[] = array(
                        'DOC CODE' => $slsOrdHdr->doc_code,
                        'AREA' => $deliveryPoint->area_code,
                        'CUSTOMER' => $deliveryPoint->code,
                        'NAME' => $deliveryPoint->company_name_01,
                        'LINE NO' => $slsOrdDtl->line_no,                        
                        'ITEM' => $item->code,
                        'DESC' => $slsOrdDtl->desc_01,
                        'CASE RATE' => $item->case_uom_rate,
                        'REQUEST QTY' => number_format($slsOrdDtl->request_qty, config('scm.decimal_scale')),
                        'AVAILABLE QTY' => number_format($slsOrdDtl->available_qty, config('scm.decimal_scale'))
                    );
                }
            }
        }

        return view('excels.slsOrdCheckStock', [
            'rows' => $ttlRows
        ]);
    }

    public function headings(): array
    {
        return array_keys($this->headings);
    }

    public function map($model): array
    {
        $this->rowNumber++;
        $statusNumber = bcmul(bcdiv($this->rowNumber, $this->total, 8), 100, 2);
        BatchJobStatusRepository::updateStatusNumber($this->batchJobStatus->id, $statusNumber);

        $unitBarcode = '';
        $caseBarcode = '';
        $palletBarcode = '';
        $otherItemUoms = array();
        $itemUomModels = DB::table('item_uoms')
            ->selectRaw('item_uoms.*')
            ->selectRaw('uoms.code AS uom_code')
            ->leftJoin('uoms', 'uoms.id', '=', 'item_uoms.uom_id')
            ->where('item_id', $model->id)
            ->orderBy('item_uoms.uom_rate')
            ->get();
        foreach($itemUomModels as $itemUomModel)
        {
            if($itemUomModel->uom_id == $model->unit_uom_id)
            {
                $unitBarcode = $itemUomModel->barcode;
            }
            elseif($itemUomModel->uom_id == $model->case_uom_id)
            {
                $caseBarcode = $itemUomModel->barcode;
            }
            elseif($itemUomModel->uom_id == Uom::$PALLET)
            {
                $palletBarcode = $itemUomModel->barcode;
            }
            else
            {
                $otherItemUoms[] = $itemUomModel;
            }
        }

        $itemPhotoCount = DB::table('item_photos')
            ->where('item_id', $model->id)
            ->count();

        $data = array(
            $model->code,
            $model->ref_code_01,
            $model->desc_01,
            $model->desc_02,
            ResStatus::$MAP[$model->status],
            StorageClass::$MAP[$model->storage_class],
            $model->item_group_01_code,
            $model->item_group_02_code,
            $model->item_group_03_code,
            ItemType::$MAP[$model->item_type],
            ScanMode::$MAP[$model->scan_mode],
            RetrievalMethod::$MAP[$model->retrieval_method],
            $model->case_ext_length,
            $model->case_ext_width,
            $model->case_ext_height,
            $model->case_gross_weight,
            $model->cases_per_pallet_length,
            $model->cases_per_pallet_width,
            $model->no_of_layers,
            $model->qty_scale,
            $itemPhotoCount,
            $model->unit_uom_code,
            $unitBarcode,
            $model->case_uom_code,
            $caseBarcode,
            $model->case_uom_rate,
            $model->pallet_uom_rate,
            $palletBarcode,
        );

        foreach($otherItemUoms as $otherItemUom)
        {
            $data[] = $otherItemUom->uom_code;
            $data[] = $otherItemUom->barcode;
            $data[] = $otherItemUom->uom_rate;
        }

        return $data;
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
        ];
    }

    public static function beforeExport(BeforeExport $event)
    {
    }

    public static function beforeSheet(BeforeSheet $event)
    {
    }

    public static function afterSheet(AfterSheet $event)
    {
    }

    public static function beforeWriting(BeforeWriting $event)
    {
    }    
}
