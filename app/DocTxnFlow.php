<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocTxnFlow extends Model
{
    public function frDocHdr()
    {
        return $this->morphTo();
    }

    public function toDocHdr()
    {
        return $this->morphTo();
    }
}
