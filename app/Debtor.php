<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Debtor extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $fillable = ['code'];

    public function generateTags(): array
    {
        return array(
            $this->code
        );
    }

    public function debtorDivisions()
    {
        return $this->hasMany(\App\DebtorDivision::class);
    }

    public function deliveryPoints()
    {
        return $this->hasMany(\App\DeliveryPoint::class);
    }
}
