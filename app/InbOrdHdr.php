<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class InbOrdHdr extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $casts = [
        'doc_date' => 'date',
        'est_del_date' => 'date',
        'currency_rate' => 'decimal:8',
        'gross_amt' => 'decimal:8',
        'gross_local_amt' => 'decimal:8',
        'disc_amt' => 'decimal:8',
        'disc_local_amt' => 'decimal:8',
        'tax_amt' => 'decimal:8',
        'tax_local_amt' => 'decimal:8',
        'round_adj_amt' => 'decimal:8',
        'round_adj_local_amt' => 'decimal:8',
        'net_amt' => 'decimal:8',
        'net_local_amt' => 'decimal:8',
    ];
    
    public function inbOrdDtls()
    {
        return $this->hasMany(\App\InbOrdDtl::class,'hdr_id','id' );
    }

    public function division()
    {
        return $this->belongsTo(\App\Division::class, 'division_id', 'id');
    }

    public function bizPartner()
    {
        return $this->belongsTo(\App\BizPartner::class, 'biz_partner_id', 'id');
    }

    public function purchaser()
    {
        return $this->belongsTo(\App\User::class, 'purchaser_id', 'id');
    }

    public function toDocTxnFlows()
    {
        return $this->morphMany(\App\DocTxnFlow::class, 'fr_doc_hdr');
    }

    public function siteFlow()
    {
        return $this->belongsTo(\App\SiteFlow::class, 'site_flow_id', 'id');
    }

    public function deliveryPoint()
    {
        return $this->belongsTo(\App\DeliveryPoint::class, 'delivery_point_id', 'id');
    }

    public function salesman()
    {
        return $this->belongsTo(\App\User::class, 'salesman_id', 'id');
    }

    public function company()
    {
        return $this->belongsTo(\App\Company::class, 'company_id', 'id');
    }

    public function frDocTxnVoids()
    {
        return $this->morphMany(\App\DocTxnVoid::class, 'to_doc_hdr');
    }

    public function frDocTxnFlows()
    {
        return $this->morphMany(\App\DocTxnFlow::class, 'to_doc_hdr');
    }

    public function generateTags(): array
    {
        return array(
            $this->doc_code
        );
    }
}
