<?php

namespace App\Channels;

use Illuminate\Notifications\Notification;
use App\Repositories\DeviceRepository;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class MobileChannel {
    public function send($notifiable, Notification $notification) {
        $id = $notifiable->getNotifiableForLog($notifiable);

        $data = $notification->toMobile($notifiable);
        
        $tokens = $this->getTokensByUserId($data['user_id']); 

        if(count($tokens) > 0) {
            $push = $this->getPush($data['title'], $data['content'], $data['payload']);

            $response = $this->build( $tokens, $push );
        }

        return true;
    }

    protected function getTokensByUserId($receiver_id) {
        $tokens = DeviceRepository::findTokensByUserId($receiver_id);

        $serialize = array();

        foreach($tokens as $token) {
            array_push($serialize, $token['fcm_token']);
        }

        return $serialize;
    }

    //Sending push message to multiple users by firebase registration ids
    protected function build( $tokens, $message ) {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder();
        $notificationBuilder->setTitle($message['data']['title'])
                            ->setBody($message['data']['content'])
                            ->setSound('default');

        $payload = json_decode($message['data']['payload']);
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData([
            'id' => $payload->id,
            'navigation' => $payload->navigation
        ]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        //Query tokens here using repository

        return $this->sendPushNotification( $tokens, $option, $notification, $data );
    }

    //Brozot/Laravel-FCM request to firebase servers
    protected function sendPushNotification( $tokens, $option, $notification, $data ) {
        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

        $response = array();
        $response['success'] = $downstreamResponse->numberSuccess();
        $response['failure'] = $downstreamResponse->numberFailure();
        $response['modification'] = $downstreamResponse->numberModification();
        $response['tokens_to_delete'] = $downstreamResponse->tokensToDelete();
        $response['tokens_to_modify'] = $downstreamResponse->tokensToModify();
        $response['tokens_to_retry'] = $downstreamResponse->tokensToRetry();
        $response['tokens_with_error'] = $downstreamResponse->tokensWithError();

        return $response;
    }

    protected function getPush($title, $message, $data) {
        $response                      = array();
        $response['data']['title']     = $title;
        $response['data']['content']   = $message;
        $response['data']['payload']   = $data;
        $response['data']['timestamp'] = date( 'Y-m-d G:i:s' );
    
        return $response;
    }

    
}