<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class DivisionDocNo extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //
    public function docNo()
    {
        return $this->belongsTo(\App\DocNo::class, 'doc_no_id', 'id');
    }

    public function division()
    {
        return $this->belongsTo(\App\Division::class, 'division_id', 'id');
    }

    public function generateTags(): array
    {
        $code = $this->division->code;
        unset($this->division);
        return array(
            $code,
        );
    }
}
