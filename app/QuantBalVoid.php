<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuantBalVoid extends Model
{
    //
    protected $casts = [
        'uom_rate' => 'decimal:6',
        'unit_qty' => 'decimal:6',
    ];
}
