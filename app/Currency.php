<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Currency extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //
    protected $casts = [
        'currency_rate' => 'decimal:8',
    ];

    protected $fillable = ['code','symbol','currency_rate'];

    public function generateTags(): array
    {
        return array(
            $this->code
        );
    }
}
