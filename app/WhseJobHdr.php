<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class WhseJobHdr extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $casts = [
        'doc_date' => 'date'
    ];
    
    public function siteFlow()
    {
        return $this->belongsTo(\App\SiteFlow::class, 'site_flow_id', 'id');
    }

    public function worker01()
    {
        return $this->belongsTo(\App\User::class, 'worker_01_id', 'id');
    }

    public function worker02()
    {
        return $this->belongsTo(\App\User::class, 'worker_02_id', 'id');
    }

    public function worker03()
    {
        return $this->belongsTo(\App\User::class, 'worker_03_id', 'id');
    }

    public function worker04()
    {
        return $this->belongsTo(\App\User::class, 'worker_04_id', 'id');
    }

    public function worker05()
    {
        return $this->belongsTo(\App\User::class, 'worker_05_id', 'id');
    }

    public function frDocTxnFlows()
    {
        return $this->morphMany(\App\DocTxnFlow::class, 'to_doc_hdr');
    }

    public function frDocTxnVoids()
    {
        return $this->morphMany(\App\DocTxnVoid::class, 'to_doc_hdr');
    }

    public function generateTags(): array
    {
        return array(
            $this->doc_code
        );
    }
}
