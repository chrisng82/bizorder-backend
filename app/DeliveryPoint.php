<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class DeliveryPoint extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $fillable = ['code'];
    
    public function debtor()
    {
        return $this->belongsTo(\App\Debtor::class, 'debtor_id', 'id');
    }

    public function debtorGroup01()
    {
        return $this->belongsTo(\App\DebtorGroup01::class, 'debtor_group_01_id', 'id');
    }

    public function debtorGroup02()
    {
        return $this->belongsTo(\App\DebtorGroup02::class, 'debtor_group_02_id', 'id');
    }

    public function debtorGroup03()
    {
        return $this->belongsTo(\App\DebtorGroup03::class, 'debtor_group_03_id', 'id');
    }

    public function debtorGroup04()
    {
        return $this->belongsTo(\App\DebtorGroup04::class, 'debtor_group_04_id', 'id');
    }

    public function debtorGroup05()
    {
        return $this->belongsTo(\App\DebtorGroup05::class, 'debtor_group_05_id', 'id');
    }

    public function state()
    {
        return $this->belongsTo(\App\State::class, 'state_id', 'id');
    }

    public function area()
    {
        return $this->belongsTo(\App\Area::class, 'area_id', 'id');
    }

    public function generateTags(): array
    {
        return array(
            $this->code
        );
    }
}
