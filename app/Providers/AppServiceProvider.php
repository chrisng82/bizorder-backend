<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use App\SlsOrdHdr;
use App\Models\Message;
use App\Observers\SlsOrdHdrObserver;
use App\Observers\MessageObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        SlsOrdHdr::observe(SlsOrdHdrObserver::class);
        Message::observe(MessageObserver::class);

        /*
        \DB::listen(function($query) {
            var_dump($query->sql);
            var_dump($query->bindings);
            var_dump($query->time);
        });
        */
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
