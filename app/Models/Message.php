<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Notifications\Notifiable;
use App\Services\Env\ResStatus;

class Message extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use Notifiable;

    protected $casts = [
        'payload' => 'json',
    ];
    
    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id', 'id');
    }
    
    public function division()
    {
        return $this->belongsTo(\App\Division::class, 'division_id', 'id');
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Company::class, 'company_id', 'id');
    }

    //Notification purposes
    public function getNotifiableForLog() {
        return $this->id;
    }
}