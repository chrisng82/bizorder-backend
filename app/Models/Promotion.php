<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use App\Services\Env\ResStatus;

class Promotion extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $casts = [
        'config' => 'json',
    ];
    
    public function rules()
    {
        return $this->hasMany(\App\Models\PromotionRule::class);
    }
    
    public function actions()
    {
        return $this->hasMany(\App\Models\PromotionAction::class);
    }

    public function variants()
    {
        return $this->hasMany(\App\Models\PromotionVariant::class);
    }

    public function promoDebtors()
    {
        return $this->hasMany(\App\PromoDebtor::class);
    }

    public function promoDivisions()
    {
        return $this->hasMany(\App\PromoDivision::class);
    }

    /**
     * Scope a query to only include active promotions.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', ResStatus::$MAP['ACTIVE'])
            ->whereDate('valid_from', '<=', date('Y-m-d'))
            ->whereDate('valid_to', '>=', date('Y-m-d'));
    }
}
