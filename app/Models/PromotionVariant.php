<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PromotionVariant extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $casts = [
        'rest' => 'json',
    ];
    
    public function promotion()
    {
        return $this->belongsTo(\App\Models\Promotion::class, 'promotion_id', 'id');
    }
    
    public function item()
    {
        return $this->belongsTo(\App\Item::class, 'item_id', 'id');
    }

    public function itemGroup01()
    {
        return $this->belongsTo(\App\ItemGroup01::class, 'item_group_01_id', 'id');
    }

    public function itemGroup02()
    {
        return $this->belongsTo(\App\ItemGroup02::class, 'item_group_02_id', 'id');
    }

    public function itemGroup03()
    {
        return $this->belongsTo(\App\ItemGroup03::class, 'item_group_03_id', 'id');
    }

    /**
     * Scope a query to only include active promotions.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeEnabled($query)
    {
        return $query->where('enabled', 1);
    }
}
