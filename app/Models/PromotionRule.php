<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PromotionRule extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $casts = [
        'config' => 'json',
    ];
    
    public function promotion()
    {
        return $this->belongsTo(\App\Models\Promotion::class, 'promotion_id', 'id');
    }
}
