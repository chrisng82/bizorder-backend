<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuantBalTxn extends Model
{
    protected $casts = [
        'uom_rate' => 'decimal:6',
        'unit_qty' => 'decimal:6',
    ];
    
    public function quantBal()
    {
        return $this->belongsTo(\App\QuantBal::class, 'quant_bal_id', 'id');
    }

    public function handlingUnit()
    {
        return $this->belongsTo(\App\HandlingUnit::class, 'handling_unit_id', 'id');
    }

    public function item()
    {
        return $this->belongsTo(\App\Item::class, 'item_id', 'id');
    }

    public function docHdr()
    {
        return $this->morphTo();
    }
}
