<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class SyncSettingHdr extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    public function syncSettingDtls()
    {
        return $this->hasMany(\App\SyncSettingDtl::class, 'hdr_id', 'id');
    }
}
