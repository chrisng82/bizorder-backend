<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class AdvShipHdr extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $casts = [
        'doc_date' => 'date',
        'est_del_date' => 'date',
        'currency_rate' => 'decimal:8',
        'gross_amt' => 'decimal:8',
        'gross_local_amt' => 'decimal:8',
        'disc_amt' => 'decimal:8',
        'disc_local_amt' => 'decimal:8',
        'tax_amt' => 'decimal:8',
        'tax_local_amt' => 'decimal:8',
        'round_adj_amt' => 'decimal:8',
        'round_adj_local_amt' => 'decimal:8',
        'net_amt' => 'decimal:8',
        'net_local_amt' => 'decimal:8',
    ];

    protected $fillable = ['doc_code'];
    
    public function advShipDtls()
    {
        return $this->hasMany(\App\AdvShipDtl::class, 'hdr_id', 'id');
    }

    public function company()
    {
        return $this->belongsTo(\App\Company::class, 'company_id', 'id');
    }

    public function division()
    {
        return $this->belongsTo(\App\Division::class, 'division_id', 'id');
    }

    public function siteFlow()
    {
        return $this->belongsTo(\App\SiteFlow::class, 'site_flow_id', 'id');
    }

    public function purchaser()
    {
        return $this->belongsTo(\App\User::class, 'purchaser_id', 'id');
    }

    public function bizPartner()
    {
        return $this->belongsTo(\App\BizPartner::class, 'biz_partner_id', 'id');
    }

    public function inbOrdHdr()
    {
        return $this->belongsTo(\App\InbOrdHdr::class, 'inb_ord_hdr_id', 'id');
    }

    public function generateTags(): array
    {
        return array(
            $this->doc_code
        );
    }
}
