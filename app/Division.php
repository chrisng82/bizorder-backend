<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Division extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    public function company()
    {
        return $this->belongsTo(\App\Company::class, 'company_id', 'id');
    }

    public function siteFlow()
    {
        return $this->belongsTo(\App\SiteFlow::class, 'site_flow_id', 'id');
    }

    public function generateTags(): array
    {
        return array(
            $this->code
        );
    }
}
