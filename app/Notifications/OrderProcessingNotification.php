<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Carbon\Carbon;

use App\SlsOrdHdr;
use App\Channels\MobileChannel;
use App\Repositories\MessageRepository;
use App\Services\NotificationService;
use App\Services\Env\NotificationPriority;
use App\Services\Env\NotificationStatus;

class OrderProcessingNotification extends Notification
{
    use Queueable;

    private $slsOrdHdr;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(SlsOrdHdr $slsOrdHdr)
    {
        $this->slsOrdHdr = $slsOrdHdr;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [MobileChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    public function toMobile($notifiable) {
        $data = [
            'user_id' => $this->slsOrdHdr->debtor_id,
            'division_id' => $this->slsOrdHdr->division_id,
            'company_id' => $this->slsOrdHdr->company_id,
            'title' => '',
            'content' => '',
            'payload' => json_encode([
                'id' => $this->slsOrdHdr->id,
                'navigation' => 'OrderScreen'
            ]),
            'priority' => NotificationPriority::$MAP['NORMAL'],
            'status' => NotificationStatus::$MAP['UNREAD']
        ];

        if($this->slsOrdHdr->doc_status == 100) {
            $data['title'] = 'Order Completed!';
            $data['content'] = 'Your order ['.$this->slsOrdHdr->doc_code.'] has been completed!'; 
        } else if($this->slsOrdHdr->doc_status == 50) {
            $data['title'] = 'Order Processed!';
            $data['content'] = 'Your order ['.$this->slsOrdHdr->doc_code.'] has been processed!'; 
        }

        MessageRepository::createModel($data);

        return $data;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
