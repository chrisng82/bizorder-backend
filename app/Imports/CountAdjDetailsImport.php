<?php

namespace App\Imports;

use App\CountAdjHdr;
use App\CountAdjDtl;
use App\Services\Env\BinType;
use App\Services\Env\DocStatus;
use App\Repositories\CountAdjHdrRepository;
use App\Repositories\CountAdjDtlRepository;
use App\Repositories\StorageBinRepository;
use App\Repositories\ItemUomRepository;
use App\Repositories\BatchJobStatusRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use App\Services\Utils\ApiException;

class CountAdjDetailsImport implements ToCollection, WithHeadingRow
{
    private $hdrId;

    private $userId;

    private $batchJobStatus;

    private $total = 0;

    private $rowNumber = 0;

    private $countAdjDetailRows = array();

    public function __construct($hdrId, $userId, $batchJobStatus) {
        $this->hdrId = $hdrId;
        $this->userId = $userId;
        $this->batchJobStatus = $batchJobStatus;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function getCountAdjDetailRows()
    {
        return $this->countAdjDetailRows;
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {
        $this->rowNumber = 0;
        $this->total = count($rows);
        $this->countAdjDetailRows = $rows;
        /*
        $documentHeader = self::processOutgoingHeader($hdrModel);
        $documentDetails = array();
        foreach($dtlModels as $dtlModel)
        {
			$dtlModel = self::processOutgoingDetail($dtlModel);
            $documentDetails[] = $dtlModel;
        }

        $message = __('CountAdj.details_successfully_deleted', []);

		return array(
			'data' => array(
                'timestamp'=>time(),
                'document_header'=>$documentHeader,
                'document_details'=>$documentDetails,
                'deleted_details'=>$delCountAdjDtlArray
            ),
			'message' => $message
        );
        */
    }
}
