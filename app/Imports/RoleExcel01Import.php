<?php

namespace App\Imports;


use App\Role;
use App\Repositories\RoleRepository;
use App\Repositories\BatchJobStatusRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use App\Services\Utils\ApiException;

class RoleExcel01Import implements ToCollection, WithHeadingRow
{
    private $siteId;

    private $userId;

    private $batchJobStatus;

    private $total = 0;

    private $rowNumber = 0;

    private $roleDataHash = array();

    private $permissionDataHash = array();

    public function __construct($siteId, $userId, $batchJobStatus) {
        $this->siteId = $siteId;
        $this->userId = $userId;
        $this->batchJobStatus = $batchJobStatus;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function getRoleDataHash()
    {
        return $this->roleDataHash;
    }

    public function getPermissionDataHash()
    {
        return $this->permissionDataHash;
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {
        $this->rowNumber = 0;
        $this->total = count($rows);
        foreach($rows as $row)
        {
            //skip if code column is empty
            if(empty(trim($row['role_name'])))
            {
                continue;
            }

            $this->rowNumber++;
            $statusNumber = bcmul(bcdiv($this->rowNumber, $this->total, 8), 100, 2);
            BatchJobStatusRepository::updateStatusNumber($this->batchJobStatus->id, $statusNumber);

            $roleData = array();
            $roleData['name'] = $row['role_name'];

            $this->roleDataHash[$roleData['name']] = $roleData;

            $permissionData = array();
            $permissionData['role_name'] = $row['role_name'];
            $permissionData['name'] = $row['permission_name'];

            $permissionDataArray = array();
            if(array_key_exists($permissionData['role_name'], $this->permissionDataHash))
            {
                $permissionDataArray = $this->permissionDataHash[$permissionData['role_name']];
            }
            $permissionDataArray[] = $permissionData;
            $this->permissionDataHash[$permissionData['role_name']] = $permissionDataArray;
        }
    }
}
