<?php

namespace App\Imports;

use App\Location;
use App\StorageType;
use App\StorageRow;
use App\StorageBay;
use App\StorageBin;
use App\Services\Env\BinType;
use App\Services\Env\RackType;
use App\Services\Env\HandlingType;
use App\Services\Env\ResStatus;
use App\Services\Env\LocationType;
use App\Repositories\LocationRepository;
use App\Repositories\StorageRowRepository;
use App\Repositories\StorageBayRepository;
use App\Repositories\StorageTypeRepository;
use App\Repositories\StorageBinRepository;
use App\Repositories\BatchJobStatusRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;

class StorageBinExcel01Import implements ToCollection, WithHeadingRow
{
    private $siteId;

    private $userId;

    private $batchJobStatus;

    private $total = 0;

    private $rowNumber = 0;

    public function __construct($siteId, $userId, $batchJobStatus) {
        $this->siteId = $siteId;
        $this->userId = $userId;
        $this->batchJobStatus = $batchJobStatus;
    }

    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {
        $this->rowNumber = 0;
        $this->total = count($rows);
        foreach($rows as $row)
        {
            //skip if code column is empty
            if(empty(trim($row['code'])))
            {
                continue;
            }

            $this->rowNumber++;
            $statusNumber = bcmul(bcdiv($this->rowNumber, $this->total, 8), 100, 2);
            BatchJobStatusRepository::updateStatusNumber($this->batchJobStatus->id, $statusNumber);
            
            //create or find location
            $location = LocationRepository::findBySiteIdAndCode($this->siteId, $row['location']);
            if(empty($location))
            {
                if(empty($row['location']))
                {
                    //get the default location
                    $location = Location::where('id', 1)
                        ->first();
                }
                else
                {
                    $locationType = LocationType::$MAP[$row['location_type']];
                    
                    $locationData = array();
                    $locationData['location_type'] = $locationType;
                    $locationData['code'] = $row['location'];
                    $locationData['ref_code_01'] = '';
                    $locationData['desc_01'] = '';
                    $locationData['desc_02'] = '';
                    $locationData['site_id'] = $this->siteId;                    
                    $location = LocationRepository::plainCreate($locationData);
                }
            }

            //create or find storageRow
            $storageRow = StorageRowRepository::findBySiteIdAndCode($this->siteId, $row['row']);
            if(empty($storageRow))
            {
                if(empty($row['row']))
                {
                    //get the default StorageRow
                    $storageRow = StorageRow::where('id', 1)
                        ->first();
                }
                else
                {
                    $storageRowData = array();
                    $storageRowData['code'] = $row['row'];
                    $storageRowData['desc_01'] = '';
                    $storageRowData['desc_02'] = '';
                    $storageRowData['floor'] = $row['floor'];
                    $storageRowData['aisle_code'] = $row['aisle'];
                    $storageRowData['aisle_sequence'] = $row['aisle_seq'];
                    $storageRowData['aisle_position'] = $row['aisle_pos'];
                    $storageRowData['layout_width'] = 0;
                    $storageRowData['layout_depth'] = 0;
                    $storageRowData['layout_height'] = 0;
                    $storageRowData['layout_x'] = 0;
                    $storageRowData['layout_y'] = 0;
                    $storageRowData['site_id'] = $this->siteId;
                    $storageRow = StorageRowRepository::plainCreate($storageRowData);
                }
            }
            else
            {
                if(bccomp($row['aisle_seq'], 0) > 0)
                {
                    if(strcmp($row['aisle'],$storageRow->aisle_code) != 0
                    || bccomp($row['aisle_seq'], $storageRow->aisle_sequence) != 0
                    || bccomp($row['aisle_pos'], $storageRow->aisle_position) != 0)
                    {
                        $storageRowData = array();
                        $storageRowData['aisle_code'] = $row['aisle'];
                        $storageRowData['aisle_sequence'] = $row['aisle_seq'];
                        $storageRowData['aisle_position'] = $row['aisle_pos'];
                        $storageRow = StorageRowRepository::plainUpdate($storageRow->id, $storageRowData);
                    }
                }
            }

            //create of find storageBay
            $storageBay = StorageBayRepository::findBySiteIdAndCode($this->siteId, $row['bay']);
            if(empty($storageBay))
            {
                if(empty($row['bay']))
                {
                    //get the default StorageBay
                    $storageBay = StorageBay::where('id', 1)
                        ->first();
                }
                else
                {
                    $storageBayData = array();
                    $storageBayData['code'] = $row['bay'];
                    $storageBayData['desc_01'] = '';
                    $storageBayData['desc_02'] = '';
                    $storageBayData['max_level'] = 10;
                    $storageBayData['bay_sequence'] = 0;
                    $storageBayData['layout_width'] = 0;
                    $storageBayData['layout_depth'] = 0;
                    $storageBayData['layout_height'] = 0;
                    $storageBayData['layout_x'] = 0;
                    $storageBayData['layout_y'] = 0;
                    $storageBayData['site_id'] = $this->siteId;
                    $storageBayData['storage_row_id'] = $storageRow->id;

                    $storageBay = StorageBayRepository::plainCreate($storageBayData);
                }
            }

            //create of find storageType
            $storageType = StorageTypeRepository::findBySiteIdAndCode($this->siteId, $row['storage_type']);
            if(empty($storageType))
            {
                if(empty($row['storage_type']))
                {
                    //get the default StorageType
                    $storageType = StorageType::where('id', 1)
                        ->first();
                }
                else
                {
                    $storageTypeData = array();
                    $storageTypeData['code'] = $row['storage_type'];
                    $storageTypeData['desc_01'] = '';
                    $storageTypeData['desc_02'] = '';
                    $storageTypeData['handling_type'] = HandlingType::$MAP[$row['handling_type']];
                    $storageTypeData['bin_type'] = BinType::$MAP[$row['bin_type']];
                    $storageTypeData['rack_type'] = RackType::$MAP[$row['rack_type']];
                    $storageTypeData['hu_min_load_length'] = empty($row['min_load_length']) ? 0 : $row['min_load_length'];
                    $storageTypeData['hu_min_load_width'] = empty($row['min_load_width']) ? 0 : $row['min_load_width'];
                    $storageTypeData['hu_min_load_height'] = empty($row['min_load_height']) ? 0 : $row['min_load_width'];
                    $storageTypeData['hu_min_load_length'] = empty($row['min_load_weight']) ? 0 : $row['min_load_weight'];
                    $storageTypeData['hu_max_load_length'] = empty($row['max_load_length']) ? 0 : $row['max_load_length'];
                    $storageTypeData['hu_max_load_width'] = empty($row['max_load_width']) ? 0 : $row['max_load_width'];
                    $storageTypeData['hu_max_load_height'] = empty($row['max_load_height']) ? 0 : $row['max_load_height'];
                    $storageTypeData['hu_max_load_weight'] = empty($row['max_load_weight']) ? 0 : $row['max_load_weight'];
                    $storageTypeData['hu_max_count'] = empty($row['max_pallet']) ? 0 : $row['max_pallet'];
                    $storageTypeData['site_id'] = $this->siteId;

                    $storageType = StorageTypeRepository::plainCreate($storageTypeData);
                }
            }
            
            $storageBin = StorageBinRepository::findBySiteIdAndCode($this->siteId, $row['code']);
            
            $storageBinData = array();
            if(!empty($row['barcode']))
            {
                $storageBin['id'] = substr($row['barcode'], 2);
            }
            $storageBinData['code'] = $row['code'];
            $storageBinData['site_id'] = $this->siteId;
            $storageBinData['desc_01'] = !empty($row['desc_01']) ? $row['desc_01'] : '';
            $storageBinData['desc_02'] = !empty($row['desc_02']) ? $row['desc_02'] : '';
            $storageBinData['location_id'] = $location->id;
            $storageBinData['storage_type_id'] = $storageType->id;
            $storageBinData['storage_bay_id'] = $storageBay->id;
            $storageBinData['bin_status'] = ResStatus::$MAP[$row['status']];
            $storageBinData['storage_row_id'] = $storageRow->id;
            $storageBinData['floor'] = $storageRow->floor;            
            $storageBinData['level'] = $row['level'];
            /*
            $binCodeParts = explode('-', $row['code']);
            if(count($binCodeParts) >= 4)
            {
                $levelPart = $binCodeParts[2];
                if(substr($levelPart, 0, 1) === "L")
                {
                    //if start with L
                    $storageBinData['level'] = substr($levelPart, 1);
                }
            }
            */
            $storageBinData['bay_sequence'] = $storageBay->bay_sequence;
            
            if(empty($storageBin))
            {
                $storageBin = StorageBinRepository::plainCreate($storageBinData);
            }
            else
            {
                $storageBin = StorageBinRepository::plainUpdate($storageBin->id, $storageBinData);
            }
        }
    }
}
