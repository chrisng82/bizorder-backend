<?php

namespace App\Imports;

use App\Uom;
use App\Item;
use App\ItemUom;
use App\ItemGroup01;
use App\ItemGroup02;
use App\ItemGroup03;
use App\ItemGroup04;
use App\ItemGroup05;
use App\Services\Env\RetrievalMethod;
use App\Services\Env\ScanMode;
use App\Services\Env\ItemType;
use App\Services\Env\StorageClass;
use App\Services\Env\ResStatus;
use App\Repositories\ItemRepository;
use App\Repositories\UomRepository;
use App\Repositories\ItemGroup01Repository;
use App\Repositories\ItemGroup02Repository;
use App\Repositories\ItemGroup03Repository;
use App\Repositories\ItemUomRepository;
use App\Repositories\BatchJobStatusRepository;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;

class ItemExcel01Import implements ToModel, WithHeadingRow
{
    private $isCount = false;

    private $siteId;

    private $userId;

    private $batchJobStatus;

    private $total = 0;

    private $rowNumber = 0;

    public function __construct($siteId, $userId, $batchJobStatus) {
        $this->siteId = $siteId;
        $this->userId = $userId;
        $this->batchJobStatus = $batchJobStatus;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function setIsCount($isCount)
    {
        $this->isCount = $isCount;
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        //skip if code column is empty
        if(!array_key_exists('code', $row))
        {
            return null;
        }
        if(empty(trim($row['code'])))
        {
            return null;
        }
        if($this->isCount)
        {
            $this->total++;
            return null;
        }
        if(ResStatus::$MAP[$row['status']] <= 0)
        {
            //skip the item if status is NULL
            return null;
        }

        $this->rowNumber++;
        $statusNumber = bcmul(bcdiv($this->rowNumber, $this->total, 8), 100, 2);
        BatchJobStatusRepository::updateStatusNumber($this->batchJobStatus->id, $statusNumber);
        
        //create or find ItemGroup01
        $itemGroup01 = null;
        if(isset($row['brand']))
        {
            $itemGroup01 = ItemGroup01Repository::findByCode($row['brand']);
            if(empty($itemGroup01))
            {
                $itemGroup01 = new ItemGroup01();
                $itemGroup01->code = $row['brand'];
                $itemGroup01->desc_01 = '';
                $itemGroup01->desc_02 = '';
                $itemGroup01->save();
            }
        }        

        //create or find ItemGroup02
        $itemGroup02 = null;
        if(isset($row['category']))
        {
            $itemGroup02 = ItemGroup02Repository::findByCode($row['category']);
            if(empty($itemGroup02))
            {
                $itemGroup02 = new ItemGroup02();
                $itemGroup02->code = $row['category'];
                $itemGroup02->desc_01 = '';
                $itemGroup02->desc_02 = '';
                $itemGroup02->save();
            }
        }

        //create or find ItemGroup03
        $itemGroup03 = null;
        if(isset($row['manufacturer']))
        {
            $itemGroup03 = ItemGroup03Repository::findByCode($row['manufacturer']);
            if(empty($itemGroup03))
            {
                $itemGroup03 = new ItemGroup03();
                $itemGroup03->code = $row['manufacturer'];
                $itemGroup03->desc_01 = '';
                $itemGroup03->desc_02 = '';
                $itemGroup03->save();
            }
        }
        
        $item = ItemRepository::findByCode($row['code']);
        if(empty($item))
        {
            $item = new Item();
            $item->code = $row['code'];
            $item->save();
        }
        $item->ref_code_01 = isset($row['ref_code_01']) ? $row['ref_code_01'] : '';
        $item->desc_01 = isset($row['desc_01']) ? $row['desc_01'] : '';
        $item->desc_02 = isset($row['desc_02']) ? $row['desc_02'] : '';
        $item->status = isset($row['status']) ? ResStatus::$MAP[$row['status']] : ResStatus::$MAP['INACTIVE'];

        $item->item_group_01_id = isset($itemGroup01) ? $itemGroup01->id : 0;
        $item->item_group_01_code = isset($itemGroup01) ? $itemGroup01->code : '';
        $item->item_group_02_id = isset($itemGroup02) ? $itemGroup02->id : 0;
        $item->item_group_02_code = isset($itemGroup02) ? $itemGroup02->code : '';
        $item->item_group_03_id = isset($itemGroup03) ? $itemGroup03->id : 0;
        $item->item_group_03_code = isset($itemGroup03) ? $itemGroup03->code : '';
        $item->item_group_04_id = 0;
        $item->item_group_05_id = 0;

        $item->storage_class = isset($row['storage_class']) ? StorageClass::$MAP[$row['storage_class']] : StorageClass::$MAP['AMBIENT_01'];
        $item->item_type = isset($row['item_type']) ? ItemType::$MAP[$row['item_type']] : ItemType::$MAP['STOCK_ITEM'];
        $item->scan_mode = isset($row['scan_mode']) ? ScanMode::$MAP[$row['scan_mode']] : ScanMode::$MAP['ENTER_QTY'];
        $item->retrieval_method = isset($row['retrieval']) ? RetrievalMethod::$MAP[$row['retrieval']] : RetrievalMethod::$MAP['FIRST_EXPIRED_FIRST_OUT'];
        $item->case_ext_length = isset($row['case_ext_lengthmm']) ? $row['case_ext_lengthmm'] : 0;
        $item->case_ext_width = isset($row['case_ext_widthmm']) ? $row['case_ext_widthmm'] : 0;
        $item->case_ext_height = isset($row['case_ext_heightmm']) ? $row['case_ext_heightmm'] : 0;
        $item->case_gross_weight = isset($row['case_gross_weightkg']) ? $row['case_gross_weightkg'] : 0;

        $item->case_uom_id = 0;
        $item->case_uom_rate = 35000;
        
        $casesPerPalletLength = 1;
        $casesPerPalletWidth = 1;        
        if(array_key_exists('base_ctns', $row))
        {
            //for TLS file
            //base_ctns and case_per_pallet_base is same
            if(isset($row['base_ctns']))
            {
                $casesPerPalletLength = $row['base_ctns'];
            }            
            $casesPerPalletWidth = 1;
        }
        elseif(array_key_exists('case_per_pallet_base', $row))
        {
            //base_ctns and case_per_pallet_base is same
            if(isset($row['case_per_pallet_base']))
            {
                $casesPerPalletLength = $row['case_per_pallet_base'];
            }            
            $casesPerPalletWidth = 1;
        }
        else
        {
            if(isset($row['cases_per_length']))
            {
                $casesPerPalletLength = $row['cases_per_length'];
            }
            if(isset($row['cases_per_width']))
            {
                $casesPerPalletWidth = $row['cases_per_width'];
            }
        }
        $item->cases_per_pallet_length = $casesPerPalletLength;
        $item->cases_per_pallet_width = $casesPerPalletWidth;
        
        $noOfLayers = 1;
        if(isset($row['no_of_layers']))
        {
            $noOfLayers = $row['no_of_layers'];
        }
        $item->no_of_layers = $noOfLayers;

        $item->qty_scale = isset($row['qty_scale']) ? $row['qty_scale'] : 0;

        $itemUoms = ItemUomRepository::findAllByItemId($item->id);
        //process unit uom
        if(isset($row['unit_uom']) && !empty($row['unit_uom']))
        {
            $uom = UomRepository::findByCode($row['unit_uom']);
            if(empty($uom))
            {
                $uom = new Uom();
                $uom->code = $row['unit_uom'];
                $uom->ref_code_01 = '';
                $uom->desc_01 = '';
                $uom->desc_02 = '';
                $uom->save();
            }

            $unitItemUom = null;
            foreach($itemUoms as $a => $itemUom)
            {
                if($itemUom->uom_id == $uom->id)
                {
                    $unitItemUom = $itemUom;
                    unset($itemUoms[$a]);
                    break;
                }
            }

            if(empty($unitItemUom))
            {
                $unitItemUom = new ItemUom;
                $unitItemUom->item_id = $item->id;
                $unitItemUom->uom_id = $uom->id;
            }
            $unitItemUom->uom_rate = 1;
            $unitItemUom->barcode = isset($row['unit_barcode']) ? str_replace(' ','',$row['unit_barcode']) : '';
            $unitItemUom->save();

            $item->unit_uom_id = $uom->id;
        }

        //process case uom
        if(isset($row['case_uom']) 
        && !empty($row['case_uom']))
        {
            $uom = UomRepository::findByCode($row['case_uom']);
            if(empty($uom))
            {
                $uom = new Uom();
                $uom->code = $row['case_uom'];
                $uom->ref_code_01 = '';
                $uom->desc_01 = '';
                $uom->desc_02 = '';
                $uom->save();
            }

            if(strcmp($row['case_uom'], $row['unit_uom']) == 0)
            {
                //some item, case_uom is unit_uom, like bulky item pet food
                //in this case case_uom_rate is 1
                $item->case_uom_id = $uom->id;
                $item->case_uom_rate = 1;
            }
            else
            {
                $caseItemUom = null;
                foreach($itemUoms as $a => $itemUom)
                {
                    if($itemUom->uom_id == $uom->id)
                    {
                        $caseItemUom = $itemUom;
                        unset($itemUoms[$a]);
                        break;
                    }
                }

                if(empty($caseItemUom))
                {
                    $caseItemUom = new ItemUom;
                    $caseItemUom->item_id = $item->id;
                    $caseItemUom->uom_id = $uom->id;
                    $caseItemUom->uom_rate = 35000;
                }            
                
                if(!empty($row['case_rate']))
                {
                    $caseItemUom->uom_rate = $row['case_rate'];
                }
                $caseItemUom->barcode = isset($row['case_barcode']) ? str_replace(' ','',$row['case_barcode']) : '';
                $caseItemUom->save();     
                
                $item->case_uom_id = $uom->id;
                $item->case_uom_rate = $caseItemUom->uom_rate;
            }
        }

        //process other uom 1
        if(isset($row['other_uom_1']) && !empty($row['other_uom_1']))
        {
            $uom = UomRepository::findByCode($row['other_uom_1']);
            if(empty($uom))
            {
                $uom = new Uom();
                $uom->code = $row['other_uom_1'];
                $uom->ref_code_01 = '';
                $uom->desc_01 = '';
                $uom->desc_02 = '';
                $uom->save();
            }

            $otherItemUom = null;
            foreach($itemUoms as $a => $itemUom)
            {
                if($itemUom->uom_id == $uom->id)
                {
                    $otherItemUom = $itemUom;
                    unset($itemUoms[$a]);
                    break;
                }
            }

            if(empty($otherItemUom))
            {
                $otherItemUom = new ItemUom;
                $otherItemUom->item_id = $item->id;
                $otherItemUom->uom_id = $uom->id;
                $otherItemUom->uom_rate = 35000;
            }
            if(!empty($row['other_rate_1']))
            {
                $otherItemUom->uom_rate = $row['other_rate_1'];
            }
            $otherItemUom->barcode = isset($row['other_barcode_1']) ? str_replace(' ','',$row['other_barcode_1']) : '';
            $otherItemUom->save();
        }

        //process other uom 2
        if(isset($row['other_uom_2']) && !empty($row['other_uom_2']))
        {
            $uom = UomRepository::findByCode($row['other_uom_2']);
            if(empty($uom))
            {
                $uom = new Uom();
                $uom->code = $row['other_uom_2'];
                $uom->ref_code_01 = '';
                $uom->desc_01 = '';
                $uom->desc_02 = '';
                $uom->save();
            }

            $otherItemUom = null;
            foreach($itemUoms as $a => $itemUom)
            {
                if($itemUom->uom_id == $uom->id)
                {
                    $otherItemUom = $itemUom;
                    unset($itemUoms[$a]);
                    break;
                }
            }

            if(empty($otherItemUom))
            {
                $otherItemUom = new ItemUom;
                $otherItemUom->item_id = $item->id;
                $otherItemUom->uom_id = $uom->id;
                $otherItemUom->uom_rate = 35000;
            }
            if(!empty($row['other_rate_2']))
            {
                $otherItemUom->uom_rate = $row['other_rate_2'];
            }
            $otherItemUom->barcode = isset($row['other_barcode_2']) ? str_replace(' ','',$row['other_barcode_2']) : '';
            $otherItemUom->save();
        }

        //process other uom 3
        if(isset($row['other_uom_3']) && !empty($row['other_uom_3']))
        {
            $uom = UomRepository::findByCode($row['other_uom_3']);
            if(empty($uom))
            {
                $uom = new Uom();
                $uom->code = $row['other_uom_3'];
                $uom->ref_code_01 = '';
                $uom->desc_01 = '';
                $uom->desc_02 = '';
                $uom->save();
            }

            $otherItemUom = null;
            foreach($itemUoms as $a => $itemUom)
            {
                if($itemUom->uom_id == $uom->id)
                {
                    $otherItemUom = $itemUom;
                    unset($itemUoms[$a]);
                    break;
                }
            }

            if(empty($otherItemUom))
            {
                $otherItemUom = new ItemUom;
                $otherItemUom->item_id = $item->id;
                $otherItemUom->uom_id = $uom->id;
                $otherItemUom->uom_rate = 35000;
            }
            if(!empty($row['other_rate_3']))
            {
                $otherItemUom->uom_rate = $row['other_rate_3'];
            }
            $otherItemUom->barcode = isset($row['other_barcode_3']) ? str_replace(' ','',$row['other_barcode_3']) : '';
            $otherItemUom->save();
        }

        //process other uom 4
        if(isset($row['other_uom_4']) && !empty($row['other_uom_4']))
        {
            $uom = UomRepository::findByCode($row['other_uom_4']);
            if(empty($uom))
            {
                $uom = new Uom();
                $uom->code = $row['other_uom_4'];
                $uom->ref_code_01 = '';
                $uom->desc_01 = '';
                $uom->desc_02 = '';
                $uom->save();
            }

            $otherItemUom = null;
            foreach($itemUoms as $a => $itemUom)
            {
                if($itemUom->uom_id == $uom->id)
                {
                    $otherItemUom = $itemUom;
                    unset($itemUoms[$a]);
                    break;
                }
            }

            if(empty($otherItemUom))
            {
                $otherItemUom = new ItemUom;
                $otherItemUom->item_id = $item->id;
                $otherItemUom->uom_id = $uom->id;
                $otherItemUom->uom_rate = 35000;
            }            
            if(!empty($row['other_rate_4']))
            {
                $otherItemUom->uom_rate = $row['other_rate_4'];
            }
            $otherItemUom->barcode = isset($row['other_barcode_4']) ? str_replace(' ','',$row['other_barcode_4']) : '';
            $otherItemUom->save();
        }

        //process other uom 5
        if(isset($row['other_uom_5']) && !empty($row['other_uom_5']))
        {
            $uom = UomRepository::findByCode($row['other_uom_5']);
            if(empty($uom))
            {
                $uom = new Uom();
                $uom->code = $row['other_uom_5'];
                $uom->ref_code_01 = '';
                $uom->desc_01 = '';
                $uom->desc_02 = '';
                $uom->save();
            }

            $otherItemUom = null;
            foreach($itemUoms as $a => $itemUom)
            {
                if($itemUom->uom_id == $uom->id)
                {
                    $otherItemUom = $itemUom;
                    unset($itemUoms[$a]);
                    break;
                }
            }

            if(empty($otherItemUom))
            {
                $otherItemUom = new ItemUom;
                $otherItemUom->item_id = $item->id;
                $otherItemUom->uom_id = $uom->id;
                $otherItemUom->uom_rate = 35000;
            }
            if(!empty($row['other_rate_5']))
            {
                $otherItemUom->uom_rate = $row['other_rate_5'];
            }
            $otherItemUom->barcode = isset($row['other_barcode_5']) ? str_replace(' ','',$row['other_barcode_5']) : '';
            $otherItemUom->save();
        }

        foreach($itemUoms as $a => $itemUom)
        {
            $itemUom->delete();
        }

        return $item;
    }
}
