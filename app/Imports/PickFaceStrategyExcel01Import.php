<?php

namespace App\Imports;


use App\StorageBin;
use App\Services\Env\BinType;
use App\Repositories\StorageBinRepository;
use App\Repositories\PickFaceStrategyRepository;
use App\Repositories\ItemRepository;
use App\Repositories\BatchJobStatusRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use App\Services\Utils\ApiException;

class PickFaceStrategyExcel01Import implements ToCollection, WithHeadingRow
{
    private $siteId;

    private $userId;

    private $batchJobStatus;

    private $total = 0;

    private $rowNumber = 0;

    public function __construct($siteId, $userId, $batchJobStatus) {
        $this->siteId = $siteId;
        $this->userId = $userId;
        $this->batchJobStatus = $batchJobStatus;
    }

    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {
        $this->rowNumber = 0;
        $this->total = count($rows);
        foreach($rows as $row)
        {
            //skip if code column is empty
            if(empty(trim($row['line_no'])))
            {
                continue;
            }

            $this->rowNumber++;
            $statusNumber = bcmul(bcdiv($this->rowNumber, $this->total, 8), 100, 2);
            BatchJobStatusRepository::updateStatusNumber($this->batchJobStatus->id, $statusNumber);

            $item = ItemRepository::findByCode($row['item_code']);
            /*
            if(empty($item))
            {
                $exc = new ApiException(__('PickFaceStrategy.item_is_required', ['lineNo'=>$row['line_no']]));
                //$exc->addData(\App\PickFaceStrategy::class, $row['id']);
                throw $exc;
            }
            */

            $storageBin = StorageBinRepository::findBySiteIdAndCode($this->siteId, $row['storage_bin_code']);
            if(empty($storageBin))
            {
                $exc = new ApiException(__('PickFaceStrategy.storage_bin_is_required', ['lineNo'=>$row['line_no']]));
                //$exc->addData(\App\PickFaceStrategy::class, $row['id']);
                throw $exc;
            }

            $storageType = $storageBin->storageType;
            if($storageType->isPickFace() == false)
            {
                $exc = new ApiException(__('PickFaceStrategy.storage_bin_is_not_pick_face', ['lineNo'=>$row['line_no']]));
                //$exc->addData(\App\PickFaceStrategy::class, $row['id']);
                throw $exc;
            }

            //create or find pickFaceStrategy
            $pickFaceStrategy = PickFaceStrategyRepository::findBySiteIdAndLineNo($this->siteId, $row['line_no']);
            
            $pickFaceStrategyData = array();
            $pickFaceStrategyData['site_id'] = $this->siteId;
            $pickFaceStrategyData['line_no'] = $row['line_no'];
            $pickFaceStrategyData['location_id'] = 0;
            $pickFaceStrategyData['storage_type_id'] = 0;
            $pickFaceStrategyData['storage_row_id'] = 0;
            $pickFaceStrategyData['storage_bay_id'] = 0;
            $pickFaceStrategyData['storage_bin_id'] = $storageBin->id;
            $pickFaceStrategyData['storage_class'] = 0;
            $pickFaceStrategyData['item_group_01_id'] = 0;
            $pickFaceStrategyData['item_group_02_id'] = 0;
            $pickFaceStrategyData['item_group_03_id'] = 0;
            $pickFaceStrategyData['item_group_04_id'] = 0;
            $pickFaceStrategyData['item_group_05_id'] = 0;
            $pickFaceStrategyData['item_id'] = empty($item) ? 0 : $item->id;
            
            if(empty($pickFaceStrategy))
            {
                $pickFaceStrategy = PickFaceStrategyRepository::plainCreate($pickFaceStrategyData);
            }
            else
            {
                $pickFaceStrategy = PickFaceStrategyRepository::plainUpdate($pickFaceStrategy->id, $pickFaceStrategyData);
            }            
        }
    }
}
