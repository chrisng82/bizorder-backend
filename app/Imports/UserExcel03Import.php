<?php

namespace App\Imports;


use App\Role;
use App\Repositories\RoleRepository;
use App\Repositories\BatchJobStatusRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use App\Services\Utils\ApiException;
use App\Services\Env\ResStatus;

class UserExcel03Import implements ToCollection, WithHeadingRow
{
    private $divisionId;

    private $userId;

    private $batchJobStatus;

    private $total = 0;

    private $rowNumber = 0;

    private $userDataHash = array();

    public function __construct($divisionId, $userId, $batchJobStatus) {
        $this->divisionId = $divisionId;
        $this->userId = $userId;
        $this->batchJobStatus = $batchJobStatus;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function getUserDataHash()
    {
        return $this->userDataHash;
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {
        $this->rowNumber = 0;
        $this->total = count($rows);
        foreach($rows as $row)
        {
            //skip if code column is empty
            if(empty(trim($row['username'])))
            {
                continue;
            }
            if(empty(trim($row['debtor_code'])))
            {
                continue;
            }

            $this->rowNumber++;
            $statusNumber = bcmul(bcdiv($this->rowNumber, $this->total, 8), 100, 2);
            BatchJobStatusRepository::updateStatusNumber($this->batchJobStatus->id, $statusNumber);

            $userData = array();
            $userData['username'] = $row['username'];
            $userData['debtor_code'] = $row['debtor_code'];
            $userData['email'] = $row['email'];
            $userData['first_name'] = $row['first_name'];
            $userData['last_name'] = $row['last_name'];
            $userData['login_type'] = 'c';
            $userData['status'] = ResStatus::$MAP['ACTIVE'];
            $userData['timezone'] = 'Asia/Singapore';

            $this->userDataHash[$userData['username']] = $userData;

        }
    }
}
