<?php

namespace App\Imports;

use App\Services\CycleCountService;
use App\Services\Env\DocStatus;
use App\Services\Env\HandlingType;
use App\Services\Env\ProcType;
use App\Services\Env\WhseJobType;
use App\Repositories\CompanyRepository;
use App\Repositories\DivisionRepository;
use App\Repositories\ItemRepository;
use App\Repositories\ItemBatchRepository;
use App\Repositories\HandlingUnitRepository;
use App\Repositories\StorageBinRepository;
use App\Repositories\BatchJobStatusRepository;
use App\Repositories\SiteDocNoRepository;
use App\Repositories\WhseTxnFlowRepository;
use App\Repositories\CycleCountHdrRepository;
use App\Repositories\ItemUomRepository;
use App\Repositories\UomRepository;
use App\Services\Utils\ApiException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class CycleCountExcel01Import implements ToCollection
{
    private $siteId;

    private $siteFlowId;

    private $userId;

    private $batchJobStatus;

    private $dtlDataListHashByCompanyId = array();

    private $storageBinIdHashByStorageBayId = array();

    private $storageBinIdHashByStorageRowId = array();

    public static $STATE = array(
        'ROW_BAY' => 1,
        'BIN' => 2,
        'LABEL' => 3,
        'LEVEL' => 4
    );

    public function __construct($siteId, $siteFlowId, $userId, $batchJobStatus) {
        $this->siteId = $siteId;
        $this->siteFlowId = $siteFlowId;
        $this->userId = $userId;
        $this->batchJobStatus = $batchJobStatus;
    }

    public function getDtlDataListHashByCompanyId()
    {
        return $this->dtlDataListHashByCompanyId;
    }

    public function getStorageBinIdHashByStorageBayId()
    {
        return $this->storageBinIdHashByStorageBayId;
    }

    public function getStorageBinIdHashByStorageRowId()
    {
        return $this->storageBinIdHashByStorageRowId;
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {
        //get user default division
        $division = DivisionRepository::findTopByUserId($this->userId); 

        $rowNumber = 0;

        //start by looking for ROW BAY row
        $state = self::$STATE['ROW_BAY'];
        $storageRow = '';
        $bayColumns = array();
        $binColumns = array();
        $labelColumns = array();
        $level = 0;
        $rawCycleCountDtls = array();
        foreach($rows as $row)
        {
            $rowNumber++;
            $statusNumber = bcmul(bcdiv($rowNumber, count($rows), 8), 100, 2);
            BatchJobStatusRepository::updateStatusNumber($this->batchJobStatus->id, $statusNumber);
            
            if($state == self::$STATE['ROW_BAY'])
            {
                if(!empty($row[0]))
                {
                    //change to ROW_BAY row state
                    $bay = '';
                    for($a = 0; $a < count($row); $a++)
                    {
                        $cell = trim($row[$a]);
                        if($a == 0)
                        {
                            //first column always is $storageRow code
                            $storageRow = $cell;
                        }
                        else
                        {
                            if(!empty($cell))
                            {
                                //start of bay
                                if(is_int($cell))
                                {
                                    $bay = intval($cell);
                                }
                                else
                                {
                                    $bay = $cell;
                                }                                
                            }
                            $bayColumns[$a] = $bay;
                        }
                    }
                    $state = self::$STATE['BIN'];
                }
            }
            elseif($state == self::$STATE['BIN'])
            {
                if(!empty($row[1]))
                {
                    //detect the bin code
                    $bin = '';
                    for($a = 0; $a < count($row); $a++)
                    {
                        $cell = trim($row[$a]);
                        if(!empty($cell))
                        {
                            //start of bay
                            $bin = $cell;
                        }
                        $binColumns[$a] = $bin;
                    }
                    $state = self::$STATE['LABEL'];
                }
            }
            elseif($state == self::$STATE['LABEL'])
            {
                $isLabelDetected = false;
                //detect the level
                $label = '';
                for($a = 0; $a < count($row); $a++)
                {
                    $cell = trim($row[$a]);
                    if(!empty($cell))
                    {
                        //start of label
                        $label = strtoupper($cell);
                        $isLabelDetected = true;
                    }
                    else
                    {
                        if($a >= 1)
                        {
                            //after the $storageRow label column and before end of columns, the label cell can not be empty
                            $exc = new ApiException(__('CycleCount.label_is_not_set', [
                                'row'=>$storageRow,
                                'colNum'=>($a+1)
                                ]
                            ));
                            //$exc->addData(\App\CycleCountHdr::class, $itemCode);
                            throw $exc;
                        }
                    }
                    $labelColumns[$a] = $label;
                }
                if($isLabelDetected == true)
                {
                    $state = self::$STATE['LEVEL'];
                }
            }
            elseif($state == self::$STATE['LEVEL'])
            {
                if(!empty($row[0]))
                {
                    //detect the level
                    $level = intval($row[0]);

                    //process the level columns
                    $startLabel = '';
                    $detailVariables = array();
                    for($a = 0; $a < count($row); $a++)
                    {
                        if($a == 0)
                        {
                            //skip the first column
                            continue;
                        }
                        $cell = trim($row[$a]);

                        $curBay = $bayColumns[$a];
                        $curBin = $binColumns[$a];
                        $curLabel = strtoupper($labelColumns[$a]);

                        if(!empty($curLabel))
                        {
                            if(strcmp($startLabel, $curLabel) == 0)
                            {
                                //this is new detail column
                                //save all previous variable to detail
                                $rawCycleCountDtls[] = $detailVariables;

                                //and clear the variable, and start again
                                $detailVariables = array();   
                                $detailVariables['ROW'] = $storageRow;
                                $detailVariables['BAY'] = $curBay;
                                $detailVariables['BIN'] = $curBin;
                                $detailVariables['LEVEL'] = $level;                             
                                $detailVariables[$curLabel] = $cell;
                            }
                            elseif($a == (count($row) - 1))
                            {
                                //this is end of row column
                                //save all previous variable to detail
                                //assign the current column value
                                $detailVariables[$curLabel] = $cell;
                                $rawCycleCountDtls[] = $detailVariables;

                                //and clear the variable, and start again
                                $detailVariables = array();   
                                $detailVariables['ROW'] = $storageRow;
                                $detailVariables['BAY'] = $curBay;
                                $detailVariables['BIN'] = $curBin;
                                $detailVariables['LEVEL'] = $level;                             
                                $detailVariables[$curLabel] = $cell;
                            }
                            else
                            {
                                //collect the variable for creating detail later
                                $detailVariables['ROW'] = $storageRow;
                                $detailVariables['BAY'] = $curBay;
                                $detailVariables['BIN'] = $curBin;
                                $detailVariables['LEVEL'] = $level;     
                                $detailVariables[$curLabel] = $cell;
                            }

                            if(empty($startLabel))
                            {
                                //mark this as start label, eg. PALLET REF
                                $startLabel = strtoupper($curLabel);

                                //start label must be COMPANY CODE
                                if(strcmp($startLabel, 'COMPANY CODE') != 0)
                                {
                                    $exc = new ApiException(__('CycleCount.start_label_invalid', ['startLabel'=>$startLabel]));
                                    //$exc->addData(\App\CycleCountHdr::class, $itemCode);
                                    throw $exc;
                                }
                            }
                        }
                    }
                }
                else
                {
                    if($level > 0)
                    {
                        //clear the variables and return to ROW_BAY
                        $state = self::$STATE['ROW_BAY'];
                        $storageRow = '';
                        $bayColumns = array();
                        $binColumns = array();
                        $labelColumns = array();
                        $level = 0;
                        $state = self::$STATE['ROW_BAY'];
                    }
                }
            }
        }

        //process the rawCycleCountDtls
        foreach($rawCycleCountDtls as $rawCycleCountDtl)
        {
            //process the label fields here

            //process item
            $itemCode = '';
            if(array_key_exists('PRODUCT CODE', $rawCycleCountDtl))
            {
                $itemCode = $rawCycleCountDtl['PRODUCT CODE'];
            }
            elseif(array_key_exists('ITEM CODE', $rawCycleCountDtl))
            {
                $itemCode = $rawCycleCountDtl['ITEM CODE'];
            }
            if(empty($itemCode))
            {
                continue;
            }

            //process the company
            $companyId = $division->company_id; //default
            if(array_key_exists('COMPANY CODE', $rawCycleCountDtl))
            {
                $company = CompanyRepository::findByCode($rawCycleCountDtl['COMPANY CODE']);
                if(!empty($company))
                {
                    $companyId = $company->id;
                }
            }

            $item = ItemRepository::findByCode($itemCode);
            if(empty($item))
            {
                $exc = new ApiException(__('CycleCount.item_not_found', ['row'=>$rawCycleCountDtl['ROW'],'itemCode'=>$itemCode]));
                //$exc->addData(\App\CycleCountHdr::class, $itemCode);
                throw $exc;
            }

            //DB::connection()->enableQueryLog();
            $storageBin = StorageBinRepository::findBySiteIdAndAttributes(
                $this->siteId, 
                $rawCycleCountDtl['ROW'],
                $rawCycleCountDtl['BAY'],
                $rawCycleCountDtl['LEVEL'],
                $rawCycleCountDtl['BIN']
            );
            if(empty($storageBin))
            {
                $binCode = $rawCycleCountDtl['ROW'].'-'.$rawCycleCountDtl['BAY'].'-'.$rawCycleCountDtl['LEVEL'].'-'.$rawCycleCountDtl['BIN'];
                $exc = new ApiException(__('CycleCount.storage_bin_not_found', ['storageBinCode'=>$binCode]));
                //$exc->addData(\App\CycleCountHdr::class, $itemCode);
                throw $exc;
            }
            //dd(DB::getQueryLog());

            //process handlingUnit
            $handlingUnit = null;
            $palletId = '';
            if(array_key_exists('PALLET ID', $rawCycleCountDtl))
            {
                $palletId = $rawCycleCountDtl['PALLET ID'];
            }
            $palletRefCode = '';
            if(array_key_exists('PALLET REF', $rawCycleCountDtl))
            {
                $palletRefCode = $rawCycleCountDtl['PALLET REF'];
            }
            if(empty($palletId))
            {
                if(!empty($palletRefCode))
                {
                    $handlingUnit = HandlingUnitRepository::firstOrCreateByRefCode01($palletRefCode, $this->siteFlowId, $storageBin->code, HandlingType::$MAP['PALLET']);                   
                }
            }
            else
            {
                //remove the pallet id prefix
                $palletId = substr($palletId, 2);
                $palletId = intval($palletId);
                $handlingUnit = HandlingUnitRepository::firstOrCreate(
                    $palletId, 
                    $this->siteId,
                    HandlingType::$MAP['PALLET']
                );
            }

            //if bin level is 1, handlingUnitId is 0
            $handlingUnitId = 0;
            if(!empty($handlingUnit))
            {
                $handlingUnitId = $handlingUnit->id;
            }

            //process itemBatch
            $expiryDate = '';
            if(array_key_exists('EXPIRY DATE', $rawCycleCountDtl))
            {
                //remove the ' if have
                $rawCycleCountDtl['EXPIRY DATE'] = str_replace("'", '', $rawCycleCountDtl['EXPIRY DATE']);
                
                $expiryDate = $rawCycleCountDtl['EXPIRY DATE'];
                if(is_numeric($expiryDate))
                {
                    //$exc = new ApiException(__('CycleCount.date_format_invalid', ['row'=>$rawCycleCountDtl['ROW'],'itemCode'=>$itemCode]));
                    //$exc->addData(\App\CycleCountHdr::class, $itemCode);
                    //throw $exc;
                    $expiryDate = date('Y-m-d', Date::excelToTimestamp($expiryDate));
                }
                if (strpos($expiryDate, '.') !== false) {
                    //this is date format 21.03.19, format to 19-03-21
                    $dateParts = explode('.',$expiryDate);
                    if(count($dateParts) < 3)
                    {
                        $exc = new ApiException(__('CycleCount.date_format_incomplete', ['row'=>$rawCycleCountDtl['ROW'],'itemCode'=>$itemCode,'date'=>$expiryDate]));
                        //$exc->addData(\App\CycleCountHdr::class, $itemCode);
                        throw $exc;
                    }
                    $expiryDate = $dateParts[2].'-'.$dateParts[1].'-'.$dateParts[0];
                }
            }
            if(empty($expiryDate))
            {
                $exc = new ApiException(__('CycleCount.item_expiry_date_is_compulsory', ['row'=>$rawCycleCountDtl['ROW'],'itemCode'=>$itemCode]));
                //$exc->addData(\App\CycleCountHdr::class, $itemCode);
                throw $exc;
            }
            $batchSerialNo = '0';
            if(array_key_exists('BATCH NO', $rawCycleCountDtl))
            {
                $batchSerialNo = $rawCycleCountDtl['BATCH NO'];
            }
            $receiptDate = '2019-05-01';
            if(array_key_exists('RECEIPT DATE', $rawCycleCountDtl))
            {
                //remove the ' if have
                $rawCycleCountDtl['RECEIPT DATE'] = str_replace("'", "", $rawCycleCountDtl['RECEIPT DATE']);
                $receiptDate = $rawCycleCountDtl['RECEIPT DATE'];
                if(is_numeric($receiptDate))
                {
                    $exc = new ApiException(__('CycleCount.date_format_invalid', ['row'=>$rawCycleCountDtl['ROW'],'itemCode'=>$itemCode]));
                    //$exc->addData(\App\CycleCountHdr::class, $itemCode);
                    throw $exc;
                }
                if (strpos($receiptDate, '.') !== false) {
                    //this is date format 21.03.19, format to 19-03-21
                    $dateParts = explode('.',$receiptDate);
                    if(count($dateParts) < 3)
                    {
                        $exc = new ApiException(__('CycleCount.date_format_incomplete', ['row'=>$rawCycleCountDtl['ROW'],'itemCode'=>$itemCode,'date'=>$receiptDate]));
                        //$exc->addData(\App\CycleCountHdr::class, $itemCode);
                        throw $exc;
                    }
                    $receiptDate = $dateParts[2].'-'.$dateParts[1].'-'.$dateParts[0];
                }
            }

            //process qty and uom
            $qty = 0;
            $uomId = 0; //NULL id
            $uomRate = 0;
            if(array_key_exists('QTY', $rawCycleCountDtl))
            {
                $qty = $rawCycleCountDtl['QTY'];
            }
            if($qty > 0
            && array_key_exists('UOM', $rawCycleCountDtl))
            {
                //if loose qty got fill in number, UOM must match
                $uomCode = $rawCycleCountDtl['UOM'];
                if(!empty($uomCode))
                {
                    $uomDB = UomRepository::findByCode($uomCode);
                    if(empty($uomDB))
                    {
                        $exc = new ApiException(__('CycleCount.uom_not_found', ['row'=>$rawCycleCountDtl['ROW'],'itemCode'=>$itemCode,'uomCode'=>$uomCode]));
                        //$exc->addData(\App\CycleCountHdr::class, $itemCode);
                        throw $exc;
                    }
                    $itemUomDB = ItemUomRepository::findByItemIdAndUomId($item->id, $uomDB->id);
                    if(!empty($itemUomDB))
                    {
                        $uomId = $uomDB->id;
                        $uomRate = $itemUomDB->uom_rate;
                    }
                    else
                    {
                        //if cell UOM fill in UNIT/EA, then will stick to smallest
                        if (strcmp($uomCode, 'UNIT') == 0
                        || strcmp($uomCode, 'EA') == 0
                        || strcmp($uomCode, 'PKT') == 0
                        || strcmp($uomCode, 'BOT') == 0
                        || strcmp($uomCode, 'BTL') == 0) 
                        {
                            $itemUomDB = ItemUomRepository::findByItemIdAndUomRate($item->id, 1);
                            if(!empty($itemUomDB))
                            {
                                $uomId = $itemUomDB->uom_id;
                                $uomRate = $itemUomDB->uom_rate;
                            }
                        }
                    }
                }
                else
                {
                    //user didnt fill in the UOM, but got qty, this case consider as smallest uom also
                    $itemUomDB = ItemUomRepository::findByItemIdAndUomRate($item->id, 1);
                    if(!empty($itemUomDB))
                    {
                        $uomId = $itemUomDB->uom_id;
                        $uomRate = $itemUomDB->uom_rate;
                    }
                }

                if(empty($uomId))
                {
                    $exc = new ApiException(__('CycleCount.uom_incomplete', ['row'=>$rawCycleCountDtl['ROW'],'itemCode'=>$itemCode,'uom'=>$uomCode]));
                    //$exc->addData(\App\CycleCountHdr::class, $itemCode);
                    throw $exc;
                }
            }
            //CTN label will be more priority
            if(array_key_exists('CTN', $rawCycleCountDtl))
            {
                if(!empty($rawCycleCountDtl['CTN']))
                {
                    if(bccomp($qty, 0, 10) == 0)
                    {
                        $qty = $rawCycleCountDtl['CTN'];
                        $uomId = $item->case_uom_id;
                        $uomRate = $item->case_uom_rate;
                    }
                    else
                    {
                        //already have qty, add to existing qty
                        $caseQty = $rawCycleCountDtl['CTN'];
                        $caseUnitQty = bcmul($caseQty, $item->case_uom_rate, 10);
                        $qty = bcadd($qty, $caseUnitQty, 10);                        
                    }
                }
            }
            elseif(array_key_exists('CASE', $rawCycleCountDtl))
            {
                if(!empty($rawCycleCountDtl['CASE']))
                {
                    if(bccomp($qty, 0, 10) == 0)
                    {
                        $qty = $rawCycleCountDtl['CASE'];
                        $uomId = $item->case_uom_id;
                        $uomRate = $item->case_uom_rate;
                    }
                    else
                    {
                        //already have qty, add to existing qty
                        $caseQty = $rawCycleCountDtl['CASE'];
                        $caseUnitQty = bcmul($caseQty, $item->case_uom_rate, 10);
                        $qty = bcadd($qty, $caseUnitQty, 10);                        
                    }
                }
            }    

            $storageType = $storageBin->storageType;
            $whseJobType = WhseJobType::$MAP['CYCLE_COUNT_BIN_LOOSE'];
            if($storageType->handling_type == HandlingType::$MAP['PALLET']
            || $storageType->handling_type == HandlingType::$MAP['TOTE_BOX']
            || $storageType->handling_type == HandlingType::$MAP['CASE_BOX'])
            {
                $whseJobType = WhseJobType::$MAP['CYCLE_COUNT_BIN_PALLET'];
            }
            
            $dtlData = array(
                'line_no' => 0,
                'storage_bin_id' => $storageBin->id,
                'item_id' => $item->id,
                'item_batch_id' => 0,
                'batch_serial_no' => $batchSerialNo,
                'expiry_date' => $expiryDate,
                'receipt_date' => $receiptDate,
                'handling_unit_id' => $handlingUnitId,
                'company_id' => $companyId,
                'desc_01' => $item->desc_01,
                'desc_02' => $item->desc_02,
                'uom_id' => $uomId,
                'uom_rate' => $uomRate,
                'qty' => $qty,
                'whse_job_type' => $whseJobType,
                'whse_job_code' => '',
                'count_seq' => 1,
                'to_cycle_count_hdr_id' => 0,
                'to_cycle_count_dtl_id' => 0
            );

            $dtlDataList = array();
            if(array_key_exists($companyId, $this->dtlDataListHashByCompanyId))
            {
                $dtlDataList = $this->dtlDataListHashByCompanyId[$companyId];
            }
            $dtlDataList[] = $dtlData;
            $this->dtlDataListHashByCompanyId[$companyId] = $dtlDataList;

            //update the storageBinIdHashByStorageBayId
            $storageBinIdHash = array();
            if(array_key_exists($storageBin->storage_bay_id, $this->storageBinIdHashByStorageBayId))
            {
                $storageBinIdHash = $this->storageBinIdHashByStorageBayId[$storageBin->storage_bay_id];
            }
            $storageBinIdHash[$storageBin->id] = $storageBin->id;
            $this->storageBinIdHashByStorageBayId[$storageBin->storage_bay_id] = $storageBinIdHash;

            //update the storageBinIdHashByStorageRowId
            $storageBinIdHash = array();
            if(array_key_exists($storageBin->storage_row_id, $this->storageBinIdHashByStorageRowId))
            {
                $storageBinIdHash = $this->storageBinIdHashByStorageRowId[$storageBin->storage_row_id];
            }
            $storageBinIdHash[$storageBin->id] = $storageBin->id;
            $this->storageBinIdHashByStorageRowId[$storageBin->storage_row_id] = $storageBinIdHash;
        }
    }
}
