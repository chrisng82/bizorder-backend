<?php

namespace App\Libs\Promotion;

use App\Repositories\DebtorRepository;

/**
 * 有很多的通用方法 如getOrder,getPromotionOrderItems等. 
 * 因此我创建了一个基类checker来实现interface和通用方法
 */
class DebtorGroupChecker implements Checker
{
    private $data;

    // Constructor
    public function __construct(array $data) {
        $this->data = $data;
    }

    public function isEligible(array $configuration): bool
    {
        $debtor = DebtorRepository::findByPk($this->data['debtor_id']);
        $debtorGroupIds = explode(',', $configuration['debtor_group_01_id']);
        return in_array($debtor->debtor_category_01_id, $debtorGroupIds);
    }
}