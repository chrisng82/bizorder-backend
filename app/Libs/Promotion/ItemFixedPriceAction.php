<?php

namespace App\Libs\Promotion;

class ItemFixedPriceAction implements Action
{
    private $data;

    // Constructor
    public function __construct(array $data) {
        $this->data = $data;
    }

    public function execute(array $configuration)
    {
        $unitPrice = bcdiv($this->data['sale_price'], $this->data['uom_rate'], 10);
        $unitPriceDisc = bcsub($unitPrice, $configuration['discountPrice'], 10);
        $priceDisc = bcmul($unitPriceDisc, $this->data['uom_rate'], config('scm.decimal_scale'));
        $this->data['price_disc'] = $priceDisc;
        return $this->data;
    }
}