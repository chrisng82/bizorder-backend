<?php

namespace App\Libs\Promotion;

class VariantItemChecker implements Checker
{
    private $data;

    // Constructor
    public function __construct(array $data) {
        $this->data = $data;
    }

    public function isEligible($variants): bool
    {
        $variants = $variants->filter(function ($variant) {
            if($variant->variant_type == $this->data['variant_type']) {
                return $variant;
            }
        });

        $item_id_array = [];

        if($variants->isEmpty()) {
            return true;
        } else {
            foreach($variants as $variant) {
                $item_id_array[] = $variant->item_id;
            }
    
            return in_array($this->data['item_id'], $item_id_array);
        }
    }
}