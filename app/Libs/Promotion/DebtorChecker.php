<?php

namespace App\Libs\Promotion;

/**
 * 有很多的通用方法 如getOrder,getPromotionOrderItems等. 
 * 因此我创建了一个基类checker来实现interface和通用方法
 */
class DebtorChecker implements Checker
{
    private $data;

    // Constructor
    public function __construct(array $data) {
        $this->data = $data;
    }

    public function isEligible(array $configuration): bool
    {
        $customerIds = explode(',', $configuration['debtor_id']);
        return in_array($this->data['debtor_id'], $customerIds);
    }
}