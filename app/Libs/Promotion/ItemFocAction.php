<?php
namespace App\Libs\Promotion;

use App\CartDtl;
use App\Repositories\PromotionVariantRepository;
use App\Repositories\ItemRepository;
use App\Repositories\ItemUomRepository;
use App\Repositories\ItemSalePriceRepository;

class ItemFocAction implements Action
{
    private $data;

    // Constructor
    public function __construct(array $data) {
        $this->data = $data;
    }

    public function execute(array $configuration)
    {
        $dtlDataArray = array();

        $fulfilled_qty = bcdiv($configuration['total_qty'], $configuration['buy_qty'], 10);
        $fulfilled_qty = floor($fulfilled_qty);

        if ($fulfilled_qty == 0)
        {
            return $dtlDataArray;
        }

        // $factor = bcdiv($configuration['buy_qty'], $configuration['foc_qty'], 10);
        $total_foc_qty = bcmul($fulfilled_qty, $configuration['foc_qty']);

        $addOnVariants = PromotionVariantRepository::findAllEnabledAddOn($configuration['promotion_id']);
        foreach ($addOnVariants as $addOnVariant) {
            //creata foc item
            $cartDtl = new CartDtl;
            $cartDtl['hdr_id'] = $this->data['hdr_id'];
            $cartDtl['item_id'] = $addOnVariant->item_id;
            $item = ItemRepository::findByPk($cartDtl['item_id']);
            $cartDtl['desc_01'] = $item->desc_01;
            $cartDtl['desc_02'] = $item->desc_02;
            // $cartDtl['desc_02'] = $fulfilled_qty . ' ' . $factor . ' ' . $total_foc_qty;
            // $cartDtl['promo_hdr_id'] = $configuration['promotion_id'];
            $cartDtl['promo_uuid'] = $configuration['promo_uuid'];
            $cartDtl['qty'] = $total_foc_qty;
            $itemUom = ItemUomRepository::findSmallestByItemId($cartDtl['item_id']);
            $cartDtl['uom_id'] = $itemUom->uom_id;
            $cartDtl['uom_rate'] = $itemUom->uom_rate;
            $itemSalePrice = ItemSalePriceRepository::findByItemIdAndUomId(now(),0,1,$cartDtl['item_id'], $cartDtl['uom_id']);
            if ($itemSalePrice) {
                $cartDtl['sale_price'] = $itemSalePrice->sale_price;
            }
            else {
                $cartDtl['sale_price'] = 0.00;
            }
            $cartDtl['price_disc'] = 0.00;
            $cartDtl['dtl_disc_perc_01'] = 100; //100% discount = foc
            $dtlDataArray[] = $cartDtl->toArray();
        }
        
        return $dtlDataArray;
    }
}