<?php

namespace App\Libs\Promotion;

interface Checker
{
    public function isEligible(array $configuration): bool;
}
