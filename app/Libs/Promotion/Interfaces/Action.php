<?php

namespace App\Libs\Promotion;

interface Action
{
    public function execute(array $configuration);
}