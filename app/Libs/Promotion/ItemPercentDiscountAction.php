<?php

namespace App\Libs\Promotion;

class ItemPercentDiscountAction implements Action
{
    private $data;

    // Constructor
    public function __construct(array $data) {
        $this->data = $data;
    }

    public function execute(array $configuration)
    {
        if ($configuration['discount01'] > 0.00) {
            $this->data['dtl_disc_perc_01'] = $configuration['discount01'];
        }
        if ($configuration['discount02'] > 0.00) {
            $this->data['dtl_disc_perc_02'] = $configuration['discount02'];
        }
        if ($configuration['discount03'] > 0.00) {
            $this->data['dtl_disc_perc_03'] = $configuration['discount03'];
        }
        if ($configuration['discount04'] > 0.00) {
            $this->data['dtl_disc_perc_04'] = $configuration['discount04'];
        }
        return $this->data;
    }
}