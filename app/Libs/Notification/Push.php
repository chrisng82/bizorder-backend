<?php

class Push {
    private $title;
    private $message;
    private $data;

    public function __construct() {
        $this->title = "";
        $this->message = "";
        $this->data = "";
    }

    public function setTitle( $title ) {
        $this->title = $title;
    }

    public function setMessage( $message ) {
        $this->message = $message;
    }

    public function setPayload( $data ) {
        $this->data = $data;
    }

    public function getPush() {
        $response                      = array();
        $response['data']['title']     = $this->title;
        $response['data']['message']   = $this->message;
        $response['data']['payload']   = $this->data;
        $response['data']['timestamp'] = date( 'Y-m-d G:i:s' );
    
        return $response;
    }
}