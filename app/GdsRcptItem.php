<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class GdsRcptItem extends Model implements Auditable
{
    //
    use \OwenIt\Auditing\Auditable;

    protected $casts = [
        'case_uom_rate' => 'decimal:6',
    ];

    public function gdsRcptHdr()
    {
        return $this->belongsTo(\App\GdsRcptHdr::class, 'hdr_id', 'id');
    }

    public function generateTags(): array
    {
        $docCode = $this->gdsRcptHdr->doc_code;
        unset($this->gdsRcptHdr);
        return array(
            $docCode
        );
    }
}
