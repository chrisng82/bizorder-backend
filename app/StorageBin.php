<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class StorageBin extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    public function site()
    {
        return $this->belongsTo(\App\Site::class, 'site_id', 'id');
    }

    public function location()
    {
        return $this->belongsTo(\App\Location::class, 'location_id', 'id');
    }

    public function storageBay()
    {
        return $this->belongsTo(\App\StorageBay::class, 'storage_bay_id', 'id');
    }

    public function storageRow()
    {
        return $this->belongsTo(\App\StorageRow::class, 'storage_row_id', 'id');
    }

    public function storageType()
    {
        return $this->belongsTo(\App\StorageType::class, 'storage_type_id', 'id');
    }

    public function getAbbreviation()
    {
        //process the bin code to be A, B, always show the last part as abbreviation
        $binCodeParts = explode('-', $this->code);
        return $binCodeParts[count($binCodeParts) - 1];
    }

    public function generateTags(): array
    {
        return array(
            $this->code
        );
    }
}
