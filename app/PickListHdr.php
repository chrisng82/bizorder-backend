<?php

namespace App;

use App\SiteFlow;
use App\PickListOutbOrd;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PickListHdr extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $casts = [
        'doc_date' => 'date'
    ];
    
    protected $fillable = ['name'];

    public function siteFlow()
    {
        return $this->belongsTo(\App\SiteFlow::class, 'site_flow_id', 'id');
    }

    public function pickListOutbOrds()
    {
        return $this->hasMany(\App\PickListOutbOrd::class, 'hdr_id', 'id');
    }

    public function pickListDtls()
    {
        return $this->hasMany(\App\PickListDtl::class, 'hdr_id', 'id');
    }

    public function toDocTxnFlows()
    {
        return $this->morphMany(\App\DocTxnFlow::class, 'fr_doc_hdr');
    }

    public function frDocTxnVoids()
    {
        return $this->morphMany(\App\DocTxnVoid::class, 'to_doc_hdr');
    }

    public function frDocTxnFlows()
    {
        return $this->morphMany(\App\DocTxnFlow::class, 'to_doc_hdr');
    }

    public function generateTags(): array
    {
        return array(
            $this->doc_code
        );
    }
}
