<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class BinTrfHdr extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //
    protected $casts = [
        'doc_date' => 'date'
    ];

    public function siteFlow()
    {
        return $this->belongsTo(\App\SiteFlow::class, 'site_flow_id', 'id');
    }

    public function binTrfDtls()
    {
        return $this->hasMany(\App\BinTrfDtl::class, 'hdr_id', 'id');
    }

    public function toDocTxnFlows()
    {
        return $this->morphMany(\App\DocTxnFlow::class, 'fr_doc_hdr');
    }

    public function frDocTxnVoids()
    {
        return $this->morphMany(\App\DocTxnVoid::class, 'to_doc_hdr');
    }

    public function frDocTxnFlows()
    {
        return $this->morphMany(\App\DocTxnFlow::class, 'to_doc_hdr');
    }

    public function generateTags(): array
    {
        return array(
            $this->doc_code
        );
    }
}
