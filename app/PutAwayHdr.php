<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PutAwayHdr extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $casts = [
        'doc_date' => 'date'
    ];
    
    public function siteFlow()
    {
        return $this->belongsTo(\App\SiteFlow::class, 'site_flow_id', 'id');
    }

    public function putAwayInbOrds()
    {
        return $this->hasMany(\App\PutAwayInbOrd::class, 'hdr_id', 'id');
    }

    public function putAwayDtls()
    {
        return $this->hasMany(\App\PutAwayDtl::class, 'hdr_id', 'id');
    }

    public function frDocTxnVoids()
    {
        return $this->morphMany(\App\DocTxnVoid::class, 'to_doc_hdr');
    }

    public function frDocTxnFlows()
    {
        return $this->morphMany(\App\DocTxnFlow::class, 'to_doc_hdr');
    }

    public function generateTags(): array
    {
        return array(
            $this->doc_code
        );
    }
}
