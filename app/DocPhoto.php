<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class DocPhoto extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //

    public function docHdr()
    {
        return $this->morphTo();
    }

    public function whseJobHdr()
    {
        return $this->belongsTo(\App\WhseJobHdr::class, 'whse_job_hdr_id', 'id');
    }

    public function generateTags(): array
    {
        $docHdrDocCode = $this->docHdr->doc_code;
        $whseJobHdrDocCode = $this->whseJobHdr->doc_code;
        unset($this->docHdr);
        unset($this->whseJobHdr);
        return array(
            $docHdrDocCode,
            $whseJobHdrDocCode,
        );
    }
}
