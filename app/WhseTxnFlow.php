<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class WhseTxnFlow extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //

    public function siteFlow()
    {
        return $this->belongsTo(\App\SiteFlow::class, 'site_flow_id', 'id');
    }

    public function generateTags(): array
    {
        $code = $this->siteFlow->code;
        unset($this->siteFlow);
        return array(
            $code,
        );
    }
}
