<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DebtorDivision extends Model
{
    //
    public function division()
    {
        return $this->belongsTo(\App\Division::class, 'division_id', 'id');
    }

    public function debtor()
    {
        return $this->belongsTo(\App\Debtor::class, 'debtor_id', 'id');
    }
}
