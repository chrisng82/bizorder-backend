<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => false,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAAB4MaMao:APA91bH78exyc5vlx1VFZscKPtfGdZFTGrtzn6oKoX5qRvVKCsRjQHDUqyXaYyHUDIE20G2BLLziKeNPhISaV67dSF3k6vGfJVhljOGXivMWQddBuvjjCyhm9KgKz8KtdDkCPchThuk8'),
        'sender_id' => env('FCM_SENDER_ID', '32264303018'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
