<?php

return [
    'db_date_format'=>'Y-m-d',
    'db_time_format'=>'Y-m-d H:i:s',
    'date_format'=>'d-m-Y',
    'min_date_time'=>'1970-01-01 00:00:00',
    'max_date_time'=>'2038-01-19 00:00:00'
];