<html>
<style>
    @page { margin: 0px 0px; }
</style>
<body>
    @foreach ($data as $docHdr)
        
        @include($docHdr->view_name, ['docHdr'=>$docHdr, 'printedAt'=>$printedAt, 'isLastLoop'=>$loop->last, 'isPageBreakAfter'=>$isPageBreakAfter])

    @endforeach
</body>

</html>
