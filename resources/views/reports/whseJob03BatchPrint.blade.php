<style>
div.page
{
    margin-top: 0pt;
    margin-bottom: 0pt;
    margin-right: 0pt;
    margin-left: 0pt;
}
.table1 td, th {
    border: 1px solid black;
}
.table1 {
    border: 1px solid black;
    border-collapse: collapse;
}
</style>

<?php $rowsPerPage = 10; ?>
<?php $ttlRows = count($docHdr->details); ?>
<?php $pageCount = $ttlRows % $rowsPerPage == 0 ? (int) ($ttlRows / $rowsPerPage) : (int) ($ttlRows / $rowsPerPage) + 1 ?>
<?php $pageCount = $pageCount + 1 //add one more page for outbound orders summary ?>
@for ($pageNum = 1; $pageNum <= $pageCount; $pageNum++)    
    <div class="page">
        <!-- header/doc barcode -->
        @if ($pageNum == 1)
        <table style="top:30pt;left:130pt;position:fixed;">
            <tr>
                <td style="width:100pt;height:50pt;font-size:1.2em;font-weight:bold;text-align:left;overflow:hidden;">
                {{$docHdr->whse_job_type_desc}}
                @foreach ($docHdr->frDocHdrs as $frDocHdr)
                <br/>{{$frDocHdr['doc_code']}}
                @endforeach
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo \Milon\Barcode\DNS1D::getBarcodeHTML($docHdr->barcode, "C39"); ?>
                </td>
            </tr>
        </table>
        @endif

        <!-- document info/page no -->
        <table style="top:50pt;left:410pt;position:fixed;">
            <tr>
                <td>
                    <div style="width:60pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                    Page
                    </div>
                </td>
                <td>
                    <div style="width:80pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$pageNum}} / {{$pageCount}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:60pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                    Doc Code
                    </div>
                </td>
                <td>
                    <div style="width:80pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$docHdr->doc_code}}
                    </div>
                </td>
            </tr>
        </table>

        <!-- document details -->
        @if ($pageNum < $pageCount)
        <table class="table1" style="table-layout:fixed;width:550pt;top:130pt;left:20pt;position:fixed;">
            <tr>
                <th style="width:3%">
                #
                </th>
                <th style="width:15%;">  
                ITEM            
                </th>
                <th style="width:44%;">   
                DESCRIPTION         
                </th>
                <th style="width:9%;">
                EXPIRY
                </th>
                <th style="width:10%;">
                CASE
                </th>
                <th style="width:10%;">
                SPLIT
                </th>
                <th style="width:10%;">
                LOOSE
                </th>
            </tr>
            <?php $startRowNum = (($pageNum - 1) * $rowsPerPage); ?>
            <?php $endRowNum = (($pageNum - 1) * $rowsPerPage) + $rowsPerPage; ?>
            @for ($rowNum = $startRowNum; $rowNum < ( $endRowNum < $ttlRows ? $endRowNum : $ttlRows ); $rowNum++)
            <tr>
                <td>
                    <div style="width:13pt;height:57pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$rowNum + 1}}
                    </div>
                </td>
                <td>
                    <div style="width:82pt;height:57pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$docHdr->details[$rowNum]['item_code']}}
                    @if (!empty($docHdr->details[$rowNum]['item_unit_barcode']))
                        <br/>{{$docHdr->details[$rowNum]['item_unit_barcode']}}
                    @endif
                    @if (!empty($docHdr->details[$rowNum]['item_case_barcode']))
                        <br/>{{$docHdr->details[$rowNum]['item_case_barcode']}}
                    @endif  
                    <br/>
                    </div>                
                </td>
                <td>
                    <div style="width:245pt;height:57pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$docHdr->details[$rowNum]['item_desc_01']}}
                    <br/>
                    {{$docHdr->details[$rowNum]['storage_bin_code']}}  =>  {{$docHdr->details[$rowNum]['to_storage_bin_code']}}
                    @if ($docHdr->details[$rowNum]['handling_unit_id'] > 0)
                    <br/>
                    {{$docHdr->details[$rowNum]['handling_unit_barcode']}}
                    <div style="padding-left:5pt;">
                    <?php echo \Milon\Barcode\DNS1D::getBarcodeHTML('ZY'.str_pad($docHdr->details[$rowNum]['handling_unit_id'], 5, '0', STR_PAD_LEFT), "C39"); ?>
                    </div>
                    @endif
                    </div>                
                </td>
                <td>
                    <div style="width:49pt;height:57pt;font-size:0.8em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$docHdr->details[$rowNum]['expiry_date']}}
                    @if (!empty($docHdr->details[$rowNum]['item_batch']))
                        <br/>{{$docHdr->details[$rowNum]['item_batch']}}
                    @endif
                    </div>
                </td>
                <td>
                    <div style="width:52pt;height:57pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    @if ($docHdr->details[$rowNum]['is_split'] < 1 && $docHdr->details[$rowNum]['case_qty'] > 0)
                        {{number_format($docHdr->details[$rowNum]['case_qty'],2)}}
                        <br/>{{$docHdr->details[$rowNum]['item_case_uom_code']}}
                    @endif
                    </div>
                </td>
                <td>
                    <div style="width:52pt;height:57pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    @if ($docHdr->details[$rowNum]['is_split'] > 0 && $docHdr->details[$rowNum]['case_qty'] > 0)
                        {{number_format($docHdr->details[$rowNum]['case_qty'],2)}}
                        <br/>{{$docHdr->details[$rowNum]['item_case_uom_code']}}
                    @endif
                    </div>
                </td>
                <td>
                    <div style="width:52pt;height:57pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    @if ($docHdr->details[$rowNum]['loose_qty'] > 0)
                        {{number_format($docHdr->details[$rowNum]['loose_qty'],2)}}
                        <br/>{{$docHdr->details[$rowNum]['item_loose_uom_code']}}
                    @endif
                    </div>
                </td>
            </tr>
            @endfor
            @if ($pageNum == $pageCount - 1)
            <tr>
                <td colspan="4">
                    <div style="width:300pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                    Total (CTN/M3/KG) :
                    </div>
                </td>
                <td>
                    <div style="width:52pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                    {{number_format($docHdr->case_qty,2)}}
                    </div>
                </td>
                <td>
                    <div style="width:52pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                    {{number_format($docHdr->cubic_meter,2)}}
                    </div>
                </td>
                <td>
                    <div style="width:52pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                    {{number_format($docHdr->gross_weight,2)}}
                    </div>
                </td>
            </tr>
            @endif
        </table>
        @endif

        @if ($pageNum == $pageCount)
        <!-- Outbound Orders Summary -->
        <table class="table1" style="table-layout:fixed;width:550pt;top:130pt;left:20pt;position:fixed;">
            <tr>
                <th style="width:15%;">
                AREA
                </th>
                <th style="width:50%;">
                NAME
                </th>
                <th style="width:20%;">
                S/O
                </th>
                <th style="width:15%;">
                NET AMT
                </th>
            </tr>

            <?php 
            //sort by areaCode, customerCode
            $tmpOutbOrdHdrs = $docHdr->outbOrdHdrs;
            usort($tmpOutbOrdHdrs, function ($a, $b) {
                if($a['delivery_point_area_code'] == $b['delivery_point_area_code'])
                {
                    return ($a['delivery_point_code'] < $b['delivery_point_code']) ? -1 : 1;
                }
                return ($a['delivery_point_area_code'] < $b['delivery_point_area_code']) ? -1 : 1;
            });
            ?>
            @foreach ($tmpOutbOrdHdrs as $outbOrdHdr)
            <tr>
                <td>
                    <div style="width:60pt;height:50pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$outbOrdHdr['delivery_point_area_desc_01']}}
                    </div>
                </td>
                <td>
                    <div style="width:275pt;height:50pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    <b>{{$outbOrdHdr['delivery_point_code']}}</b>
                    <br />{{$outbOrdHdr['delivery_point_company_name_01']}}
                    @if (!empty($outbOrdHdr['delivery_point_company_name_02']))
                        <br/>{{$outbOrdHdr['delivery_point_company_name_02']}}
                    @endif
                    @if (!empty($outbOrdHdr['desc_01']))
                        <br/>{{$outbOrdHdr['desc_01']}}
                    @endif
                    @if (!empty($outbOrdHdr['desc_02']))
                        <br/>{{$outbOrdHdr['desc_02']}}
                    @endif
                    </div>
                </td>                
                <td>
                    <div style="width:110pt;height:50pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$outbOrdHdr['sls_ord_hdr_code']}}
                    </div>
                </td>
                <td>
                    <div style="width:79pt;height:50pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{number_format($outbOrdHdr['net_amt'],2)}}
                    </div>
                </td>
            </tr>
            @endforeach
            <tr>
                <td colspan="2">
                    <div style="width:300pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    @foreach ($docHdr->frDocHdrs as $frDocHdr)
                    {{$frDocHdr['doc_code']}}
                    @endforeach
                    </div>
                </td>
                <td>
                    <div style="width:50pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                    Total :
                    </div>
                </td>
                <td>
                    <div style="width:79pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                    {{number_format($docHdr->net_amt,2)}}
                    </div>
                </td>
            </tr>
        </table>
        @endif

        <!-- Signature -->
        @if ($pageNum == $pageCount)
        <table style="table-layout:fixed;width:520pt;top:630pt;left:30pt;position:fixed;">
            @if ($docHdr->whse_job_type_desc == 'Pick Face Replenishment')
            <tr>
                <td style="width:25%">
                    <div style="width:125pt;height:30pt;font-size:0.9em;font-weight:bold;text-align:center;overflow:hidden;">
                    <hr />
                    <br />STOREKEEPER/DATE
                    </div>
                </td>
                <td style="width:25%">
                    <div style="width:125pt;height:30pt;font-size:0.9em;font-weight:bold;text-align:center;overflow:hidden;">
                    
                    </div>
                </td>
                <td style="width:25%">
                    <div style="width:125pt;height:30pt;font-size:0.9em;font-weight:bold;text-align:center;overflow:hidden;">
                    
                    </div>
                </td>
                <td style="width:25%">
                    <div style="width:125pt;height:30pt;font-size:0.9em;font-weight:bold;text-align:center;overflow:hidden;">
                    
                    </div>
                </td>
            </tr>
            @else
            <tr>
                <td>
                    <div style="width:125pt;height:30pt;font-size:0.9em;font-weight:bold;text-align:center;overflow:hidden;">
                    <hr />
                    <br />PICKER/DATE
                    </div>
                </td>
                <td>
                    <div style="width:125pt;height:30pt;font-size:0.9em;font-weight:bold;text-align:center;overflow:hidden;">
                    <hr />
                    <br />STOREKEEPER/DATE
                    </div>
                </td>
                <td>
                    <div style="width:125pt;height:30pt;font-size:0.9em;font-weight:bold;text-align:center;overflow:hidden;">
                    <hr />
                    <br />SUPERVISOR/DATE
                    </div>
                </td>
                <td>
                    <div style="width:125pt;height:30pt;font-size:0.9em;font-weight:bold;text-align:center;overflow:hidden;">
                    <hr />
                    <br />DELIVERY BY
                    </div>
                </td>
            </tr>
            @endif
        </table>
        @endif

        <!-- Print By/Time -->
        <table style="top:110pt;left:450pt;position:fixed;">
            <tr>
                <td>
                    <div style="width:100pt;height:14pt;font-size:0.7em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$printedBy['username']}}   {{$printedAt->setTimezone(env('APP_TIMEZONE', 'UTC'))}}
                    </div>
                </td>
            </tr>
        </table>
    </div>

    @if ($isLastLoop && $pageNum == $pageCount)
        {{-- last of the loop --}}
    @elseif ($isPageBreakAfter)
        <div style="page-break-after: always;"></div>
    @endif
@endfor
