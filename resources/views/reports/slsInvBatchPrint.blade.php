<style>
div.page
{
    margin-top: 0pt;
    margin-bottom: 0pt;
    margin-right: 0pt;
    margin-left: 0pt;
}
</style>

<?php $rowsPerPage = 9; ?>
<?php $ttlRows = count($docHdr->details); ?>
<?php $pageCount = $ttlRows % $rowsPerPage == 0 ? (int) ($ttlRows / $rowsPerPage) : (int) ($ttlRows / $rowsPerPage) + 1 ?>
@for ($pageNum = 1; $pageNum <= $pageCount; $pageNum++)    
    <div class="page">
        <!-- Bill To -->
        <table style="top:120pt;left:34pt;position:fixed;">
            <tr>
                <td>
                    <div style="width:180pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    Bill To:
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:180pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$docHdr->debtor_code}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:180pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$docHdr->debtor_company_name_01}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:180pt;height:70pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$docHdr->debtor_unit_no}}
                    {{$docHdr->debtor_building_name}}<br />
                    {{$docHdr->debtor_street_name}}<br />
                    {{$docHdr->debtor_district_01}}<br />
                    {{$docHdr->debtor_district_02}}<br />
                    {{$docHdr->debtor_postcode}}<br />
                    {{$docHdr->debtor_state_name}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:180pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    TEL : {{$docHdr->debtor_phone_01}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:180pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    FAX : {{$docHdr->debtor_fax_01}}
                    </div>
                </td>
            </tr>
        </table>

        <!-- Deliver To -->
        <table style="top:120pt;left:230pt;position:fixed;">
            <tr>
                <td>
                    <div style="width:180pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    Deliver To:
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:180pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:180pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:180pt;height:70pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$docHdr->delivery_point_unit_no}}
                    {{$docHdr->delivery_point_building_name}}<br />
                    {{$docHdr->delivery_point_street_name}}<br />
                    {{$docHdr->delivery_point_district_01}}<br />
                    {{$docHdr->delivery_point_district_02}}<br />
                    {{$docHdr->delivery_point_postcode}}<br />
                    {{$docHdr->delivery_point_state_name}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:180pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    TEL : {{$docHdr->delivery_point_phone_01}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:180pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    FAX : {{$docHdr->delivery_point_fax_01}}
                    </div>
                </td>
            </tr>
        </table>

        <!-- Picking/So No -->
        <table style="top:110pt;left:430pt;position:fixed;">
            <tr>
                <td>
                    <div style="width:60pt;height:10pt;font-size:0.7em;font-weight:bold;text-align:left;overflow:hidden;">
                    PICKING
                    </div>
                </td>
                <td>
                    <div style="width:80pt;height:10pt;font-size:0.7em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$docHdr->pick_list_hdr_doc_code}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:60pt;height:10pt;font-size:0.7em;font-weight:bold;text-align:left;overflow:hidden;">
                    SO NO
                    </div>
                </td>
                <td>
                    <div style="width:80pt;height:10pt;font-size:0.7em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$docHdr->sls_ord_hdr_doc_code}}
                    </div>
                </td>
            </tr>
        </table>

        <!-- Doc No/Date/Ref No -->
        <table style="top:140pt;left:430pt;position:fixed;">
            <tr>
                <td>
                    <div style="width:60pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                    Invoice No
                    </div>
                </td>
                <td>
                    <div style="width:80pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$docHdr->doc_code}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:60pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                    Date
                    </div>
                </td>
                <td>
                    <div style="width:80pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$docHdr->doc_date}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:60pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                    Ref Code
                    </div>
                </td>
                <td>
                    <div style="width:80pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$docHdr->ref_code_01}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:60pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                    Salesman
                    </div>
                </td>
                <td>
                    <div style="width:80pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$docHdr->salesman_code}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:60pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                    Terms
                    </div>
                </td>
                <td>
                    <div style="width:80pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$docHdr->credit_term_code}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:60pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                    Page
                    </div>
                </td>
                <td>
                    <div style="width:80pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$pageNum}} / {{$pageCount}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:60pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                    Collector
                    </div>
                </td>
                <td>
                    <div style="width:80pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$docHdr->salesman_code}}
                    </div>
                </td>
            </tr>
        </table>

        <!-- Document Details -->
        <table style="table-layout:fixed;width:542pt;top:270pt;left:33pt;position:fixed;">
            <tr>
                <td style="width:100%">
                    <!-- heading -->
                    <table style="table-layout:fixed;width:542pt;border-bottom:1px solid black;">
                        <tr>
                            <td>
                                <div style="width:20pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                                </div>
                            </td>
                            <td>
                                <div style="width:95pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                                Item Code
                                </div>                
                            </td>
                            <td colspan="7">
                                <div style="top:0pt;left:160pt;position:relative;width:380pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                                Quantity
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:5%">
                                <div style="width:20pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                                No
                                </div>
                            </td>
                            <td style="width:18%">
                                <div style="width:95pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                                Barcode
                                </div>                
                            </td>
                            <td style="width:10%">
                                <div style="width:40pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                                Loc
                                </div>
                            </td>
                            <td style="width:10%">
                                <div style="width:50pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                                Price
                                </div>
                            </td>
                            <td style="width:10%">
                                <div style="width:50pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                                Case
                                </div>
                            </td>
                            <td style="width:10%">
                                <div style="width:50pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                                Loose
                                </div>
                            </td>
                            <td style="width:14%">
                                <div style="width:60pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                                Disc %
                                </div>
                            </td>
                            <td style="width:10%">
                                <div style="width:50pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                                Ttl Disc
                                </div>
                            </td>
                            <td style="width:13%">
                                <div style="width:68pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                                Total
                                </div>
                            </td>
                        </tr>
                    </table>
                    <!-- heading -->
                </td>
            </tr>
            <?php $startRowNum = (($pageNum - 1) * $rowsPerPage); ?>
            <?php $endRowNum = (($pageNum - 1) * $rowsPerPage) + $rowsPerPage; ?>
            @for ($rowNum = $startRowNum; $rowNum < ( $endRowNum < $ttlRows ? $endRowNum : $ttlRows ); $rowNum++)
            <tr>
                <td style="width:100%">
                    <!-- each detail -->
                    <table style="table-layout:fixed;width:542pt;">
                        <tr>
                            <td>
                                <div style="width:20pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                                {{$docHdr->details[$rowNum]['line_no']}}
                                </div>
                            </td>
                            <td>
                                <div style="width:95pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                                {{$docHdr->details[$rowNum]['item_code']}}
                                </div>                
                            </td>
                            <td colspan="7">
                                <div style="width:380pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                                {{$docHdr->details[$rowNum]['item_desc_01']}} {{$docHdr->details[$rowNum]['item_desc_02']}}
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:5%">
                                <div style="width:20pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                                </div>
                            </td>
                            <td style="width:18%">
                                <div style="width:95pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                                {{$docHdr->details[$rowNum]['item_unit_barcode']}}
                                </div>                
                            </td>
                            <td style="width:10%">
                                <div style="width:40pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                                {{$docHdr->details[$rowNum]['location_code']}}
                                </div>
                            </td>
                            <td style="width:10%">
                                <div style="width:50pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                                {{number_format($docHdr->details[$rowNum]['sale_price'],2)}}
                                </div>
                            </td>
                            <td style="width:10%">
                                <div style="width:50pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                                @if ($docHdr->details[$rowNum]['case_qty'] > 0)
                                    {{$docHdr->details[$rowNum]['case_qty']}}  {{$docHdr->details[$rowNum]['item_case_uom_code']}}
                                @endif
                                </div>
                            </td>
                            <td style="width:10%">
                                <div style="width:50pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                                @if ($docHdr->details[$rowNum]['loose_qty'] > 0)
                                    {{number_format($docHdr->details[$rowNum]['loose_qty'],0)}}  {{$docHdr->details[$rowNum]['item_loose_uom_code']}}
                                @endif
                                </div>
                            </td>
                            <td style="width:14%">
                                <div style="width:60pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:visible;">
                                @if ($docHdr->details[$rowNum]['dtl_disc_amt'] > 0 && $docHdr->details[$rowNum]['net_amt'] <> 0)
                                    @if ($docHdr->details[$rowNum]['dtl_disc_perc_01'] > 0)
                                        {{round($docHdr->details[$rowNum]['dtl_disc_perc_01'],2)}}%
                                    @endif
                                    @if ($docHdr->details[$rowNum]['dtl_disc_perc_02'] > 0)
                                        {{round($docHdr->details[$rowNum]['dtl_disc_perc_02'],2)}}%
                                    @endif
                                    @if ($docHdr->details[$rowNum]['dtl_disc_perc_03'] > 0)
                                        {{round($docHdr->details[$rowNum]['dtl_disc_perc_03'],2)}}%
                                    @endif
                                    @if ($docHdr->details[$rowNum]['dtl_disc_perc_04'] > 0)
                                        {{round($docHdr->details[$rowNum]['dtl_disc_perc_04'],2)}}% 
                                    @endif
                                @endif
                                @if ($docHdr->details[$rowNum]['net_amt'] == 0)
                                    FOC 
                                @endif
                                </div>
                            </td>
                            <td style="width:10%">
                                <div style="width:50pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                                {{number_format($docHdr->details[$rowNum]['ttl_dtl_disc_amt'],2)}}
                                </div>
                            </td>
                            <td style="width:13%">
                                <div style="width:67pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                                {{number_format($docHdr->details[$rowNum]['net_amt'],2)}}
                                </div>
                            </td>
                        </tr>
                    </table>
                    <!-- each detail -->
                </td>
            </tr>
            @endfor
            @if ($pageNum == $pageCount)
            <tr>
                <td style="width:100%">
                    <table style="table-layout:fixed;width:542pt;border-top:1pt solid black;">
                        <tr>
                            <td style="width:77%">
                                <div style="width:400pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                                Total (RM) :
                                </div>
                            </td>
                            <td style="width:10%">
                                <div style="width:50pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                                {{number_format($docHdr->ttl_dtl_disc_amt,2)}}
                                </div>
                            </td>
                            <td style="width:13%">
                                <div style="width:67pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                                {{number_format($docHdr->net_amt,2)}}
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            @endif
        </table>

        <!-- Print desc -->
        <table style="top:673pt;left:34pt;position:fixed;">
            <tr>
                <td>
                    <div style="width:550pt;height:30pt;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                    {{$docHdr->desc_01}}
                    </div>
                </td>
            </tr>
        </table>
        <!-- Print Ringgit Malaysia -->
        @if ($pageNum == $pageCount)
        <table style="top:690pt;left:34pt;position:fixed;">
            <tr>
                <td>
                    <div style="width:550pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                    Ringgit Malaysia : {{$docHdr->net_amt_english}} CENT ONLY
                    </div>
                </td>
            </tr>
        </table>
        @endif

        <!-- Print Invoice Barcode -->
        @if ($pageNum == 1)
        <table style="top:705pt;left:310pt;position:fixed;">
            <tr>
                <td>
                    <div style="width:400pt;height:25pt;font-size:1em;font-weight:normal;text-align:left;overflow:hidden;">
                    <?php echo \Milon\Barcode\DNS1D::getBarcodeHTML('04'.str_pad($docHdr->id, 7, '0', STR_PAD_LEFT), "C39"); ?>
                    </div>
                </td>
            </tr>
        </table>
        @endif

        <!-- Print Computer Signature -->
        <table style="top:727pt;left:485pt;position:fixed;">
            <tr>
                <td>
                    <div style="width:550pt;height:10pt;font-size:0.75em;font-weight:bold;text-align:left;overflow:hidden;">
                    This is computer
                    </div>
                    <div style="width:550pt;height:10pt;font-size:0.75em;font-weight:bold;text-align:left;overflow:hidden;">
                    generated invoice
                    </div>
                    <div style="width:550pt;height:10pt;font-size:0.75em;font-weight:bold;text-align:left;overflow:hidden;">
                    no signature required
                    </div>
                </td>
            </tr>
        </table>

        <!-- Print By/Time -->
        <table style="top:100pt;left:430pt;position:fixed;">
            <tr>
                <td>
                    <div style="width:100pt;height:14pt;font-size:0.7em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$printedBy['username']}}   {{$printedAt->setTimezone(env('APP_TIMEZONE', 'UTC'))}}
                    </div>
                </td>
            </tr>
        </table>
    </div>

    @if ($isLastLoop && $pageNum == $pageCount)
        {{-- last of the loop --}}
    @elseif ($isPageBreakAfter)
        <div style="page-break-after: always;"></div>
    @endif
@endfor
