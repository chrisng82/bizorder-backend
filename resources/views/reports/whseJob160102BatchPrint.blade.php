<style>
div.page
{
    margin-top: 0pt;
    margin-bottom: 0pt;
    margin-right: 0pt;
    margin-left: 0pt;
}
.table1 td, th {
    border: 1px solid black;
}
.table1 {
    border: 1px solid black;
    border-collapse: collapse;
}
</style>

<?php 
$rowsPerPage = 10;
$ttlDetailsRows = count($docHdr->whse_job_hdrs);
$pageCount = $ttlDetailsRows % $rowsPerPage == 0 ? (int) ($ttlDetailsRows / $rowsPerPage) : (int) ($ttlDetailsRows / $rowsPerPage) + 1;

$ttlPageCount = $pageCount;
?>

@for ($pageNum = 1; $pageNum <= ($ttlPageCount); $pageNum++)    
    <div class="page">
        <!-- summary header -->
        <table style="table-layout:fixed;width:500pt;top:30pt;left:30pt;position:fixed;">
            <tr>
                <td style="width:500pt;height:10pt;font-size:1.5em;font-weight:bold;text-align:left;overflow:hidden;">
                {{$docHdr->doc_code}} CYCLE COUNT
                </td>
            </tr>
        </table>

        <!-- document info/page no -->
        <table style="table-layout:fixed;width:80pt;top:30pt;left:450pt;position:fixed;">
            <tr>
                <td>
                    <div style="width:78pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$pageNum}} / {{$ttlPageCount}}
                    </div>
                </td>
            </tr>
        </table>

        <!-- document details -->
        <table class="table1" style="table-layout:fixed;width:530pt;top:65pt;left:30pt;position:fixed;">
        <tr>
                <th style="width:3%">
                #
                </th>
                <th style="width:17%;">  
                Barcode            
                </th>
                <th style="width:80%;">   
                DESCRIPTION         
                </th>
            </tr>
            <?php $startRowNum = (($pageNum - 1) * $rowsPerPage); ?>
            <?php $endRowNum = (($pageNum - 1) * $rowsPerPage) + $rowsPerPage; ?>
            @for ($rowNum = $startRowNum; $rowNum < ( $endRowNum < $ttlDetailsRows ? $endRowNum : $ttlDetailsRows ); $rowNum++)
            <tr>
                <td>
                    <div style="width:13pt;height:57pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$rowNum + 1}}
                    </div>
                </td>
                <td>
                    <div style="width:82pt;height:60pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    <?php echo \Milon\Barcode\DNS2D::getBarcodeHTML($docHdr->whse_job_hdrs[$rowNum]['barcode'], "QRCODE", 3, 3); ?>
                    {{$docHdr->whse_job_hdrs[$rowNum]['barcode']}}
                    </div>                
                </td>
                <td>
                    <div style="width:245pt;height:17pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$docHdr->whse_job_hdrs[$rowNum]['desc_01']}}
                    </div>  
                    <div style="width:245pt;height:40pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$docHdr->whse_job_hdrs[$rowNum]['desc_02']}}
                    </div>              
                </td>
            </tr>
            @endfor
        </table>

        <!-- Print By/Time -->
        <table style="top:50pt;left:450pt;position:fixed;">
            <tr>
                <td>
                    <div style="width:100pt;height:14pt;font-size:0.7em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$printedBy['username']}}   {{$printedAt->setTimezone(env('APP_TIMEZONE', 'UTC'))}}
                    </div>
                </td>
            </tr>
        </table>
    </div>

    @if ($isLastLoop && $pageNum == $ttlPageCount)
        {{-- last of the loop --}}
    @elseif ($isPageBreakAfter)
        <div style="page-break-after: always;"></div>
    @endif
@endfor
