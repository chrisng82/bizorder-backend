<style>
div.page
{
    margin-top: 0pt;
    margin-bottom: 0pt;
    margin-right: 0pt;
    margin-left: 0pt;
}
.table1 td, th {
    border: 1px solid black;
}
.table1 {
    border: 1px solid black;
    border-collapse: collapse;
}
</style>

<?php 
$rowsPerPage = 8;
$ttlDetailsRows = count($docHdr->details);
$pageCount = $ttlDetailsRows % $rowsPerPage == 0 ? (int) ($ttlDetailsRows / $rowsPerPage) : (int) ($ttlDetailsRows / $rowsPerPage) + 1;

$ttlPageCount = $pageCount;
?>

@for ($pageNum = 1; $pageNum <= ($ttlPageCount); $pageNum++)    
    <div class="page">
        <!-- detail and order summary header -->
        @if ($pageNum <= $pageCount)
        <table style="table-layout:fixed;width:500pt;top:30pt;left:30pt;position:fixed;">
            <tr>
                <td style="width:500pt;height:10pt;font-size:1.5em;font-weight:bold;text-align:left;overflow:hidden;">
                {{$docHdr->doc_code}} RETURN RECEIPT
                </td>
            </tr>
            <tr>
                <td style="width:500pt;height:30pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                @foreach ($docHdr->delivery_points as $deliveryPoint)
                <b>{{$deliveryPoint->code}}</b> {{$deliveryPoint->company_name_01}} /
                @endforeach
                </td>
            </tr>
            <tr>
                <td style="width:500pt;height:20pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                @foreach ($docHdr->inb_ord_hdrs as $inb_ord_hdr)
                {{$inb_ord_hdr->sls_rtn_hdr_code}}
                @endforeach
                </td>
            </tr>
            <tr>
                <td style="width:500pt;height:35pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                @foreach ($docHdr->salesmans as $salesman)
                {{$salesman->username}} /
                @endforeach
                <?php 
                $remarks = array();
                foreach($docHdr->inb_ord_hdrs as $inb_ord_hdr)
                {
                    $remarks[] = $inb_ord_hdr->desc_01;
                }
                $remark = implode('/',array_unique($remarks));
                echo $remark;
                ?>
                </td>
            </tr>
        </table>
        @endif

        <!-- document info/page no -->
        <table style="table-layout:fixed;width:80pt;top:30pt;left:500pt;position:fixed;">
            <tr>
                <td>
                    <div style="width:78pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$pageNum}} / {{$ttlPageCount}}
                    </div>
                </td>
            </tr>
        </table>

        <!-- document details -->
        @if ($pageNum <= $pageCount)
        <table class="table1" style="table-layout:fixed;width:550pt;top:165pt;left:20pt;position:fixed;">
            <tr>
                <th style="width:3%">
                #
                </th>
                <th style="width:15%;">  
                ITEM            
                </th>
                <th style="width:44%;">   
                DESCRIPTION         
                </th>
                <th style="width:9%;">
                COND
                </th>
                <th style="width:10%;">
                CASE
                </th>
                <th style="width:10%;">
                SPLIT
                </th>
                <th style="width:10%;">
                LOOSE
                </th>
            </tr>
            <?php $startRowNum = (($pageNum - 1) * $rowsPerPage); ?>
            <?php $endRowNum = (($pageNum - 1) * $rowsPerPage) + $rowsPerPage; ?>
            @for ($rowNum = $startRowNum; $rowNum < ( $endRowNum < $ttlDetailsRows ? $endRowNum : $ttlDetailsRows ); $rowNum++)
            <tr>
                <td>
                    <div style="width:13pt;height:57pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$rowNum + 1}}
                    </div>
                </td>
                <td>
                    <div style="width:82pt;height:57pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    <b>{{$docHdr->details[$rowNum]['whse_job_type_label']}}</b>
                    <br/>{{$docHdr->details[$rowNum]['item_code']}}
                    @if (!empty($docHdr->details[$rowNum]['item_unit_barcode']))
                        <br/>{{$docHdr->details[$rowNum]['item_unit_barcode']}}
                    @endif
                    @if (!empty($docHdr->details[$rowNum]['item_case_barcode']))
                        <br/>{{$docHdr->details[$rowNum]['item_case_barcode']}}
                    @endif  
                    <br/>
                    </div>                
                </td>
                <td>
                    <div style="width:243pt;height:57pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$docHdr->details[$rowNum]['item_desc_01']}}
                    <br/>
                    <br/>
                    BIN : ___________  EXP : ____________
                    </div>                
                </td>
                <td>
                    <div style="width:49pt;height:57pt;font-size:0.8em;font-weight:normal;text-align:left;overflow:hidden;">
                    GOOD / BAD
                    </div>
                </td>
                <td>
                    <div style="width:52pt;height:57pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    @if ($docHdr->details[$rowNum]['is_split'] < 1 && $docHdr->details[$rowNum]['case_qty'] > 0)
                        {{number_format($docHdr->details[$rowNum]['case_qty'],2)}}
                        <br/>{{$docHdr->details[$rowNum]['item_case_uom_code']}}
                    @endif
                    </div>
                </td>
                <td>
                    <div style="width:52pt;height:57pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    @if ($docHdr->details[$rowNum]['is_split'] > 0 && $docHdr->details[$rowNum]['case_qty'] > 0)
                        {{number_format($docHdr->details[$rowNum]['case_qty'],2)}}
                        <br/>{{$docHdr->details[$rowNum]['item_case_uom_code']}}
                    @endif
                    </div>
                </td>
                <td>
                    <div style="width:52pt;height:57pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    @if ($docHdr->details[$rowNum]['loose_qty'] > 0)
                        {{number_format($docHdr->details[$rowNum]['loose_qty'],2)}}
                        <br/>{{$docHdr->details[$rowNum]['item_loose_uom_code']}}
                    @endif
                    </div>
                </td>
            </tr>
            @endfor
            @if ($pageNum == $pageCount - 1)
            <tr>
                <td colspan="4">
                    <div style="width:300pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                    Total (CTN/M3/KG) :
                    </div>
                </td>
                <td>
                    <div style="width:52pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                    {{number_format($docHdr->case_qty,2)}}
                    </div>
                </td>
                <td>
                    <div style="width:52pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                    
                    </div>
                </td>
                <td>
                    <div style="width:52pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                    
                    </div>
                </td>
            </tr>
            @endif
        </table>
        @endif

        <!-- Signature -->
        @if ($pageNum == $pageCount)
        <table style="table-layout:fixed;width:520pt;top:700pt;left:30pt;position:fixed;">
            <tr>
                <td>
                </td>
                <td>
                    <div style="width:125pt;height:30pt;font-size:0.9em;font-weight:bold;text-align:center;overflow:hidden;">
                    <hr />
                    <br />STOREKEEPER/DATE
                    </div>
                </td>
                <td>
                    <div style="width:125pt;height:30pt;font-size:0.9em;font-weight:bold;text-align:center;overflow:hidden;">
                    <hr />
                    <br />SUPERVISOR/DATE
                    </div>
                </td>
                <td>
                </td>
            </tr>
        </table>
        @endif

        <!-- Print By/Time -->
        <table style="top:150pt;left:450pt;position:fixed;">
            <tr>
                <td>
                    <div style="width:100pt;height:14pt;font-size:0.7em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$printedBy['username']}}   {{$printedAt->setTimezone(env('APP_TIMEZONE', 'UTC'))}}
                    </div>
                </td>
            </tr>
        </table>
    </div>

    @if ($isLastLoop && $pageNum == $ttlPageCount)
        {{-- last of the loop --}}
    @elseif ($isPageBreakAfter)
        <div style="page-break-after: always;"></div>
    @endif
@endfor
