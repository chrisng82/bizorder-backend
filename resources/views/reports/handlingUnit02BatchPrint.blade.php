<style>
div.page
{
    margin-top: 0pt;
    margin-bottom: 0pt;
    margin-right: 20pt;
    margin-left: 20pt;
    height: 510pt; 
    width: 801.89pt;
}
div.table
{
    display: table;
}
div.title
{
    display: table-caption;
    text-align: center;
    font-weight: bold;
    font-size: larger;
}
div.heading
{
    display: table-row;
    font-weight: bold;
    text-align: center;
}
div.row
{
    display: table-row;
}
div.cell
{
    display: table-cell;
    padding-left: 0pt;
    padding-right: 0pt;
}

div.footer-table
{
    display: table;
}
div.footer-title
{
    display: table-caption;
    text-align: center;
    font-weight: bold;
    font-size: larger;
}
div.footer-heading
{
    display: table-row;
    font-weight: bold;
    text-align: center;
}
div.footer-row
{
    display: table-row;
}
div.footer-cell
{
    display: table-cell;
    padding-left: 0pt;
    padding-right: 0pt;
}
</style>

<div class="page">
    <div class="table" style="top:30pt;left:50pt;position:fixed;width:200pt;font-size:1.25em">
        <div class="row">
            <div class="cell">
            P/I :
            </div>
            <div class="cell">
            {{$docHdr->adv_ship_hdr_code}}
            </div>
        </div>
        <div class="row">
            <div class="cell">
            INV :
            </div>
            <div class="cell">
            {{$docHdr->ref_code_02}}
            </div>
        </div>
        <div class="row">
            <div class="cell">
            CON :
            </div>
            <div class="cell">
            {{$docHdr->ref_code_04}}
            </div>
        </div>
    </div>
    <div class="table" style="top:30pt;left:630pt;position:fixed;width:130pt;font-size:1.25em">
        <div class="row">
            <div class="cell">
            <?php echo \Milon\Barcode\DNS2D::getBarcodeHTML($docHdr->barcode, "QRCODE"); ?>
            </div>
        </div>
        <div class="row">
            <div class="cell" style="text-align:center">
            {{$docHdr->barcode}}
            </div>
        </div>
    </div>
    <div class="table" style="top:150pt;left:50pt;position:fixed;page-break-inside:avoid;">
        <div class="row">
            <div class="cell">
                <div style="width:130pt;height:70pt;font-size:3.5em;font-weight:bold;overflow:hidden;">
                RECV:
                </div>
            </div>
            <div class="cell">
                <div style="width:600pt;height:70pt;font-size:4.3em;font-weight:bold;overflow:hidden;">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="cell">
                <div style="width:130pt;height:195pt;font-size:3.5em;font-weight:bold;overflow:hidden;">
                SKU:
                </div>
            </div>
            <div class="cell">
                <div style="width:600pt;height:195pt;font-size:4.3em;font-weight:normal;overflow:hidden;">
                {{$docHdr->item_code}} {{$docHdr->item_desc_01}}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="cell">
                <div style="width:130pt;height:70pt;font-size:3.5em;font-weight:bold;overflow:hidden;">
                QTY:
                </div>
            </div>
            <div class="cell">
                <div style="width:600pt;height:70pt;font-size:4.3em;font-weight:bold;overflow:hidden;">
                {{$docHdr->case_qty}}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="cell">
                <div style="overflow:hidden;width:130pt;height:70pt;font-size:3.5em;font-weight:bold">
                EXP:
                </div>
            </div>
            <div class="cell">
                <div style="overflow:hidden;width:600pt;height:70pt;font-size:4.3em;font-weight:bold;">
                </div>
            </div>
        </div>
    </div>
    <div class="table" style="top:550pt;left:630pt;position:fixed;width:200pt;font-size:1.25em">
        <div class="row">
            <div class="cell">{{$printedBy['username']}}   {{$printedAt->setTimezone(env('APP_TIMEZONE', 'UTC'))}}</div>
        </div>
    </div>
</div>

