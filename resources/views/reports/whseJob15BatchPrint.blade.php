<style>
div.page-top
{
    margin-top: 20pt;
    margin-bottom: 0pt;
    margin-right: 20pt;
    margin-left: 20pt;
    height: 60pt; 
    width: 555.28pt;
    border: solid;
    border-width: thin;
}
div.page-body
{
    margin-top: 0pt;
    margin-bottom: 0pt;
    margin-right: 20pt;
    margin-left: 20pt;
    height: 673.89pt; 
    width: 555.28pt;
    border: solid;
    border-width: thin;
}
div.page-bottom
{
    margin-top: 0pt;
    margin-bottom: 20pt;
    margin-right: 20pt;
    margin-left: 20pt;
    height: 30pt; 
    width: 555.28pt;
    border: solid;
    border-width: thin;
}
div.header-table
{
    width: 555.28pt;
    display: table;
}
div.header-row
{
    display: table-row;
}
div.header-cell-1
{
    width: 100pt;
    display: table-cell;
    padding-left: 0pt;
    padding-right: 0pt;
}
div.header-cell-2
{
    width: 250pt;
    display: table-cell;
    padding-left: 0pt;
    padding-right: 0pt;
}
div.header-cell-3
{
    width: 200pt;
    display: table-cell;
    padding-left: 0pt;
    padding-right: 0pt;
}

div.header-customer-table
{
    display: table;
}
div.header-customer-row
{
    display: table-row;
}
div.header-customer-cell
{
    display: table-cell;
    border: solid;
    border-width: thin;
    padding-left: 0pt;
    padding-right: 0pt;
}

div.header-document-table
{
    display: table;
}
div.header-document-row
{
    display: table-row;
}
div.header-document-cell
{
    display: table-cell;
    padding-left: 0pt;
    padding-right: 0pt;
}

div.details-table
{
    width: 555.28pt;
    display: table;
}
div.details-title
{
    display: table-caption;
    text-align: center;
    font-weight: bold;
    font-size: larger;
}
div.details-heading
{
    display: table-row;
    font-weight: bold;
    text-align: center;
}
div.details-row
{
    display: table-row;
}
div.details-cell
{
    display: table-cell;
    border: solid;
    border-width: thin;
    padding-left: 0pt;
    padding-right: 0pt;
}

div.footer-table
{
    width: 555.28pt;
    display: table;
}
div.footer-title
{
    display: table-caption;
    text-align: center;
    font-weight: bold;
    font-size: larger;
}
div.footer-heading
{
    display: table-row;
    font-weight: bold;
    text-align: center;
}
div.footer-row
{
    display: table-row;
}
div.footer-cell
{
    display: table-cell;
    border: solid;
    border-width: thin;
    padding-left: 0pt;
    padding-right: 0pt;
}
</style>

<?php $rowsPerPage = 10; ?>
<?php $ttlRows = count($docHdr->details); ?>
<?php $pageCount = $ttlRows % $rowsPerPage == 0 ? (int) ($ttlRows / $rowsPerPage) : (int) ($ttlRows / $rowsPerPage) + 1 ?>
@for ($pageNum = 1; $pageNum <= $pageCount; $pageNum++)    
    <div class="page-top">
        <div class="header-table">
            <div class="header-row">
                <div class="header-cell-1">
                </div>
                <div class="header-cell-2">
                {{$title}}
                @if ($pageNum == 1)
                    <?php echo \Milon\Barcode\DNS1D::getBarcodeHTML($docHdr->barcode, "C39"); ?>
                @endif
                </div>
                <div class="header-cell-3">
                    <div class="header-document-table">
                        <div class="header-document-row">
                            <div class="header-document-cell">Page</div>
                            <div class="header-document-cell">{{$pageNum}} of {{$pageCount}}</div>
                        </div>
                        <div class="header-document-row">
                            <div class="header-document-cell">Doc Code&nbsp;</div>
                            <div class="header-document-cell">{{$docHdr->doc_code}}</div>
                        </div>
                        <div class="header-document-row">
                            <div class="header-document-cell"></div>
                            <div class="header-document-cell">
                            @foreach ($docHdr->frDocHdrs as $frDocHdr)
                            {{$frDocHdr['doc_code']}}
                            @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="details-table">
            <div class="details-row">
                <div class="details-cell">#</div>
                <div class="details-cell">ITEM</div>
                <div class="details-cell">DESCRIPTION</div>
                <div class="details-cell">BATCH</div>
                <div class="details-cell">CASE</div>
                <div class="details-cell">LOOSE</div>
            </div>
            <?php $startRowNum = (($pageNum - 1) * $rowsPerPage); ?>
            <?php $endRowNum = (($pageNum - 1) * $rowsPerPage) + $rowsPerPage; ?>
            @for ($rowNum = $startRowNum; $rowNum < ( $endRowNum < $ttlRows ? $endRowNum : $ttlRows ); $rowNum++)
                <div class="details-row">
                    <div class="details-cell" style="text-align:left">
                        <div style="width:15pt">
                        {{$rowNum + 1}}
                        </div>
                    </div>
                    <div class="details-cell">
                        <div style="width:85pt">
                        {{$docHdr->details[$rowNum]['item_code']}}
                        @if (!empty($docHdr->details[$rowNum]['item_unit_barcode']))
                            <br/>{{$docHdr->details[$rowNum]['item_unit_barcode']}}
                        @endif
                        @if (!empty($docHdr->details[$rowNum]['item_case_barcode']))
                            <br/>{{$docHdr->details[$rowNum]['item_case_barcode']}}
                        @endif
                        @if ($docHdr->details[$rowNum]['handling_unit_id'] > 0)
                            <br/>
                            <?php echo 'ZY'.str_pad($docHdr->details[$rowNum]['handling_unit_id'], 5, '0', STR_PAD_LEFT); ?>
                        @endif
                        </div>
                    </div>
                    <div class="details-cell">
                        <div style="width:250">
                        {{$docHdr->details[$rowNum]['item_desc_01']}}
                        
                        </div>
                    </div>
                    <div class="details-cell">
                        <div style="width:70pt">
                        {{$docHdr->details[$rowNum]['expiry_date']}}
                        @if (!empty($docHdr->details[$rowNum]['item_batch']))
                            <br/>{{$docHdr->details[$rowNum]['item_batch']}}
                        @endif
                        @if ($docHdr->details[$rowNum]['handling_unit_id'] > 0)
                            <br/>{{$docHdr->details[$rowNum]['handling_unit_ref_code_01']}}
                        @endif
                        </div>
                    </div>
                    <div class="details-cell" style="text-align:right">
                        <div style="width:50pt">
                        @if ($docHdr->details[$rowNum]['case_qty'] > 0)
                            {{$docHdr->details[$rowNum]['case_qty']}}
                            <br/>{{$docHdr->details[$rowNum]['item_case_uom_code']}}
                        @endif
                        </div>
                    </div>
                    <div class="details-cell" style="text-align:right">
                        <div style="width:50pt">
                        @if ($docHdr->details[$rowNum]['loose_qty'] > 0)
                            {{round($docHdr->details[$rowNum]['loose_qty'],2)}}
                            <br/>{{$docHdr->details[$rowNum]['item_loose_uom_code']}}
                        @endif
                        </div>
                    </div>
                </div>
            @endfor
        </div>
    </div>
    <div class="page-bottom">
        <div class="footer-table">
            <div class="footer-row">
            <div class="footer-cell"><div style="width:100%;text-align:right">{{$printedBy['username']}}   {{$printedAt->setTimezone(env('APP_TIMEZONE', 'UTC'))}}</div></div>
            </div>
        </div>
    </div>

    @if ($isLastLoop && $pageNum == $pageCount)
        {{-- last of the loop --}}
    @elseif ($isPageBreakAfter)
        <div style="page-break-after: always;"></div>
    @endif
@endfor
