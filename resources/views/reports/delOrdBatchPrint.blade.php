<style>
div.header-table
{
    height: 200pt; 
    width: 612.00pt;
    display: table;
}
div.header-row
{
    display: table-row;
}
div.header-cell-1
{
    width: 400pt;
    display: table-cell;
    border: solid;
    border-width: thin;
    padding-left: 0pt;
    padding-right: 0pt;
}
div.header-cell-2
{
    width: 195.28pt;
    display: table-cell;
    border: solid;
    border-width: thin;
    padding-left: 0pt;
    padding-right: 0pt;
}

div.header-customer-table
{
    //height: 200pt; 
    //width: 300pt;
    display: table;
}
div.header-customer-row
{
    display: table-row;
}
div.header-customer-cell
{
    display: table-cell;
    border: solid;
    border-width: thin;
    padding-left: 0pt;
    padding-right: 0pt;
}

div.header-document-table
{
    //height: 200pt; 
    //width: 300pt;
    display: table;
}
div.header-document-row
{
    display: table-row;
}
div.header-document-cell
{
    display: table-cell;
    border: solid;
    border-width: thin;
    padding-left: 0pt;
    padding-right: 0pt;
}

div.details-table
{
    height: 375pt; 
    width: 612.00pt;
    display: table;
}
div.details-title
{
    display: table-caption;
    text-align: center;
    font-weight: bold;
    font-size: larger;
}
div.details-heading
{
    display: table-row;
    font-weight: bold;
    text-align: center;
}
div.details-row
{
    display: table-row;
}
div.details-cell
{
    display: table-cell;
    border: solid;
    border-width: thin;
    padding-left: 0pt;
    padding-right: 0pt;
}

div.footer-table
{
    height: 200pt; 
    width: 612.00pt;
    display: table;
}
div.footer-title
{
    display: table-caption;
    text-align: center;
    font-weight: bold;
    font-size: larger;
}
div.footer-heading
{
    display: table-row;
    font-weight: bold;
    text-align: center;
}
div.footer-row
{
    display: table-row;
}
div.footer-cell
{
    display: table-cell;
    border: solid;
    border-width: thin;
    padding-left: 0pt;
    padding-right: 0pt;
}
</style>

<?php $rowsPerPage = 10; ?>
<?php $ttlRows = count($docHdr->details); ?>
<?php $pageCount = $ttlRows % $rowsPerPage == 0 ? (int) ($ttlRows / $rowsPerPage) : (int) ($ttlRows / $rowsPerPage) + 1 ?>
@for ($pageNum = 1; $pageNum <= $pageCount; $pageNum++)    
    <div class="header-table">
        <div class="header-row">
            <div class="header-cell-1">
            </div>
            <div class="header-cell-2">
                <div class="header-document-table">
                    <div class="header-document-row">
                        <div class="header-document-cell">{{$pageNum}} of {{$pageCount}}</div>
                    </div>
                    <div class="header-document-row">
                        <div class="header-document-cell">Doc Code {{$docHdr->doc_code}}</div>
                    </div>
                    <div class="header-document-row">
                        <div class="header-document-cell"><?php echo \Milon\Barcode\DNS1D::getBarcodeHTML('11'.str_pad($docHdr->id, 5, '0', STR_PAD_LEFT), "EAN13"); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="details-table">
        <div class="details-row">
            <div class="details-cell">Line Id</div>
            <div class="details-cell">Item Id</div>
            <div class="details-cell">Description</div>
            <div class="details-cell">Uom Rate</div>
            <div class="details-cell">Quantity</div>
        </div>
        <?php $startRowNum = (($pageNum - 1) * $rowsPerPage); ?>
        <?php $endRowNum = (($pageNum - 1) * $rowsPerPage) + $rowsPerPage; ?>
        @for ($rowNum = $startRowNum; $rowNum < ( $endRowNum < $ttlRows ? $endRowNum : $ttlRows ); $rowNum++)
            <div class="details-row">
                <div class="details-cell">{{$docHdr->details[$rowNum]['id']}}</div>
                <div class="details-cell">{{$docHdr->details[$rowNum]['item_id']}}</div>
                <div class="details-cell">{{$docHdr->details[$rowNum]['desc_01']}}</div>
                <div class="details-cell">{{$docHdr->details[$rowNum]['uom_rate']}}</div>
                <div class="details-cell">{{$docHdr->details[$rowNum]['qty']}}</div>
            </div>
        @endfor
    </div>
    <div class="footer-table">
        <div class="footer-row">
            <div class="footer-cell">{{$printedAt->setTimezone(env('APP_TIMEZONE', 'UTC'))}}</div>
        </div>
    </div>

    @if ($isLastLoop && $pageNum == $pageCount)
        {{-- last of the loop --}}
    @elseif ($isPageBreakAfter)
        <div style="page-break-after: always;"></div>
    @endif
@endfor
