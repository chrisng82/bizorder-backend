<style>
div.page
{
    margin-top: 0pt;
    margin-bottom: 0pt;
    margin-right: 0pt;
    margin-left: 0pt;
}
.table1 td, th {
    border: 1px solid black;
}
.table1 {
    border: 1px solid black;
    border-collapse: collapse;
}
</style>

<?php 
$rowsPerPage = 8;

$ttlDetailsRows = count($docHdr->details);
$dtlPageCount = $ttlDetailsRows % $rowsPerPage == 0 ? (int) ($ttlDetailsRows / $rowsPerPage) : (int) ($ttlDetailsRows / $rowsPerPage) + 1;
$dtlStartPageNum = 1;
$dtlEndPageNum = $dtlPageCount;

//add one more page for outbound orders summary
$sumPageCount = 1;
$sumStartPageNum = $dtlEndPageNum + 1;
$sumEndPageNum = $sumStartPageNum + $sumPageCount - 1;

//add one more page for pick face replenishment
$pfPageCount = 0;
$ttlPfDetailsRows = count($docHdr->pick_face_replenishments);
$pfPageCount = $ttlPfDetailsRows % $rowsPerPage == 0 ? (int) ($ttlPfDetailsRows / $rowsPerPage) : (int) ($ttlPfDetailsRows / $rowsPerPage) + 1;
$pfStartPageNum = $sumEndPageNum + 1;
$pfEndPageNum = $pfStartPageNum + $pfPageCount - 1;

$ttlPageCount = $dtlPageCount + $sumPageCount + $pfPageCount;
?>

@for ($pageNum = 1; $pageNum <= ($ttlPageCount); $pageNum++)    
    <div class="page">
        <!-- detail and order summary header -->
        @if ($pageNum <= $sumEndPageNum)
        <table style="table-layout:fixed;width:500pt;top:30pt;left:30pt;position:fixed;">
            <tr>
                <td style="width:500pt;height:10pt;font-size:1.5em;font-weight:bold;text-align:left;overflow:hidden;">
                {{$docHdr->doc_code}} PICKING
                @foreach ($docHdr->areas as $area)
                <span style="font-size:0.9em;font-weight:normal;">
                &nbsp;{{$area->desc_01}}
                @endforeach
                </span>
                </td>
            </tr>
            <tr>
                <td style="width:500pt;height:30pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                @foreach ($docHdr->delivery_points as $deliveryPoint)
                {{$deliveryPoint->company_name_01}}/
                @endforeach
                </td>
            </tr>
            <tr>
                <td style="width:500pt;height:20pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                @foreach ($docHdr->outb_ord_hdrs as $outb_ord_hdr)
                {{$outb_ord_hdr->sls_ord_hdr_code}}
                @endforeach
                </td>
            </tr>
            <tr>
                <td style="width:500pt;height:35pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                <?php 
                $remarks = array();
                foreach($docHdr->outb_ord_hdrs as $outb_ord_hdr)
                {
                    $remarks[] = $outb_ord_hdr->desc_01;
                }
                $remark = implode('/',array_unique($remarks));
                echo $remark;
                ?>
                </td>
            </tr>
        </table>
        @elseif ($pageNum >= $pfStartPageNum)
        <!-- replenishment header -->
        <table style="table-layout:fixed;width:500pt;top:30pt;left:30pt;position:fixed;">
            <tr>
                <td style="width:500pt;height:12pt;font-size:1.5em;font-weight:bold;text-align:left;overflow:hidden;">
                {{$docHdr->doc_code}} REPLENISHMENT
                </td>
            </tr>
            <tr>
                <td style="width:500pt;height:25pt;font-size:1.5em;font-weight:bold;text-align:left;overflow:hidden;">
                
                </td>
            </tr>
        </table>
        @endif

        <!-- document info/page no -->
        <table style="table-layout:fixed;width:80pt;top:30pt;left:500pt;position:fixed;">
            <tr>
                <td>
                    <div style="width:78pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$pageNum}} / {{$ttlPageCount}}
                    </div>
                </td>
            </tr>
            @if ($pageNum <= $sumEndPageNum)
                @foreach ($docHdr->picking_whse_job_hdrs as $pickingWhseJobHdr)
                <tr>
                    <td>
                        <div style="width:78pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                        {{$pickingWhseJobHdr->doc_code}}
                        </div>
                    </td>
                </tr>
                @endforeach
                @if ($pageNum == 1)
                    @foreach ($docHdr->picking_whse_job_hdrs as $pickingWhseJobHdr)
                    <tr>
                        <td>
                            <div style="left:-150pt;position:relative;">
                            <?php echo \Milon\Barcode\DNS1D::getBarcodeHTML($pickingWhseJobHdr->barcode, "C39"); ?>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                @endif
            @elseif ($pageNum >= $pfStartPageNum)
                @foreach ($docHdr->replenish_whse_job_hdrs as $replenishWhseJobHdr)
                <tr>
                    <td>
                        <div style="width:78pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                        {{$replenishWhseJobHdr->doc_code}}
                        </div>
                    </td>
                </tr>
                @endforeach
                @if ($pageNum == $pfStartPageNum)
                    @foreach ($docHdr->replenish_whse_job_hdrs as $replenishWhseJobHdr)
                    <tr>
                        <td>
                            <div style="left:-150pt;position:relative;">
                            <?php echo \Milon\Barcode\DNS1D::getBarcodeHTML($replenishWhseJobHdr->barcode, "C39"); ?>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                @endif
            @endif
        </table>

        <!-- document details -->
        @if ($pageNum <= $dtlEndPageNum)
        <table class="table1" style="table-layout:fixed;width:550pt;top:165pt;left:20pt;position:fixed;">
            <tr>
                <th style="width:3%">
                #
                </th>
                <th style="width:15%;">  
                ITEM            
                </th>
                <th style="width:44%;">   
                DESCRIPTION         
                </th>
                <th style="width:9%;">
                EXPIRY
                </th>
                <th style="width:10%;">
                CASE
                </th>
                <th style="width:10%;">
                SPLIT
                </th>
                <th style="width:10%;">
                LOOSE
                </th>
            </tr>
            <?php $startRowNum = (($pageNum - 1) * $rowsPerPage); ?>
            <?php $endRowNum = (($pageNum - 1) * $rowsPerPage) + $rowsPerPage; ?>
            @for ($rowNum = $startRowNum; $rowNum < ( $endRowNum < $ttlDetailsRows ? $endRowNum : $ttlDetailsRows ); $rowNum++)
            <tr>
                <td>
                    <div style="width:13pt;height:57pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$rowNum + 1}}
                    </div>
                </td>
                <td>
                    <div style="width:82pt;height:57pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    <b>{{$docHdr->details[$rowNum]['whse_job_type_label']}}</b>
                    <br/>{{$docHdr->details[$rowNum]['item_code']}}
                    @if (!empty($docHdr->details[$rowNum]['item_unit_barcode']))
                        <br/>{{$docHdr->details[$rowNum]['item_unit_barcode']}}
                    @endif
                    @if (!empty($docHdr->details[$rowNum]['item_case_barcode']))
                        <br/>{{$docHdr->details[$rowNum]['item_case_barcode']}}
                    @endif  
                    <br/>
                    </div>                
                </td>
                <td>
                    <div style="width:240pt;height:57pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$docHdr->details[$rowNum]['item_desc_01']}}
                    <br/>
                    {{$docHdr->details[$rowNum]['storage_bin_code']}}  
                        =>  
                        {{$docHdr->details[$rowNum]['to_storage_bin_code']}}
                    @if ($docHdr->details[$rowNum]['handling_unit_id'] > 0)
                        ({{$docHdr->details[$rowNum]['handling_unit_barcode']}})
                    @endif
                    @if (!empty($docHdr->details[$rowNum]['rep_pick_list_doc_codes']))
                    <div style="font-size:0.8em;font-weight:normal;">
                    {{$docHdr->details[$rowNum]['rep_pick_list_doc_codes']}}
                    </div>
                    @endif
                    
                    @if ($docHdr->details[$rowNum]['handling_unit_id'] > 0 
                    && $docHdr->details[$rowNum]['whse_job_type'] != 4)
                    <div style="padding-left:5pt;">
                    <?php echo \Milon\Barcode\DNS1D::getBarcodeHTML('ZY'.str_pad($docHdr->details[$rowNum]['handling_unit_id'], 5, '0', STR_PAD_LEFT), "C39"); ?>
                    </div>
                    @endif
                    
                    </div>                
                </td>
                <td>
                    <div style="width:49pt;height:57pt;font-size:0.8em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$docHdr->details[$rowNum]['expiry_date']}}
                    @if (!empty($docHdr->details[$rowNum]['item_batch']))
                        <br/>{{$docHdr->details[$rowNum]['item_batch']}}
                    @endif
                    </div>
                </td>
                <td>
                    <div style="width:52pt;height:57pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    @if ($docHdr->details[$rowNum]['is_split'] < 1 && $docHdr->details[$rowNum]['case_qty'] > 0)
                        {{number_format($docHdr->details[$rowNum]['case_qty'],2)}}
                        <br/>{{$docHdr->details[$rowNum]['item_case_uom_code']}}
                    @endif
                    </div>
                </td>
                <td>
                    <div style="width:52pt;height:57pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    @if ($docHdr->details[$rowNum]['is_split'] > 0 && $docHdr->details[$rowNum]['case_qty'] > 0)
                        {{number_format($docHdr->details[$rowNum]['case_qty'],2)}}
                        <br/>{{$docHdr->details[$rowNum]['item_case_uom_code']}}
                    @endif
                    </div>
                </td>
                <td>
                    <div style="width:52pt;height:57pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    @if ($docHdr->details[$rowNum]['loose_qty'] > 0)
                        {{number_format($docHdr->details[$rowNum]['loose_qty'],2)}}
                        <br/>{{$docHdr->details[$rowNum]['item_loose_uom_code']}}
                    @endif
                    </div>
                </td>
            </tr>
            @endfor
            @if ($pageNum == $dtlEndPageNum)
            <tr>
                <td colspan="4">
                    <div style="width:300pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                    Total (CTN/M3/KG) :
                    </div>
                </td>
                <td>
                    <div style="width:52pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                    {{number_format($docHdr->case_qty,2)}}
                    </div>
                </td>
                <td>
                    <div style="width:52pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                    
                    </div>
                </td>
                <td>
                    <div style="width:52pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                    
                    </div>
                </td>
            </tr>
            @endif
        </table>
        @endif

        @if ($pageNum == $sumStartPageNum)
        <!-- Outbound Orders Summary -->
        <table class="table1" style="table-layout:fixed;width:550pt;top:165pt;left:20pt;position:fixed;">
            <tr>
                <th style="width:15%;">
                AREA
                </th>
                <th style="width:50%;">
                NAME
                </th>
                <th style="width:20%;">
                S/O
                </th>
                <th style="width:15%;">
                NET AMT
                </th>
            </tr>

            <?php 
            //sort by areaCode, customerCode
            $tmpOutbOrdHdrs = $docHdr->outb_ord_hdrs;
            usort($tmpOutbOrdHdrs, function ($a, $b) {
                if($a['delivery_point_area_code'] == $b['delivery_point_area_code'])
                {
                    return ($a['delivery_point_code'] < $b['delivery_point_code']) ? -1 : 1;
                }
                return ($a['delivery_point_area_code'] < $b['delivery_point_area_code']) ? -1 : 1;
            });
            ?>
            @foreach ($tmpOutbOrdHdrs as $outbOrdHdr)
            <tr>
                <td>
                    <div style="width:60pt;height:50pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$outbOrdHdr['delivery_point_area_desc_01']}}
                    </div>
                </td>
                <td>
                    <div style="width:275pt;height:50pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    <b>{{$outbOrdHdr['delivery_point_code']}}</b>
                    <br />{{$outbOrdHdr['delivery_point_company_name_01']}}
                    @if (!empty($outbOrdHdr['delivery_point_company_name_02']))
                        <br/>{{$outbOrdHdr['delivery_point_company_name_02']}}
                    @endif
                    @if (!empty($outbOrdHdr['desc_01']))
                        <br/>{{$outbOrdHdr['desc_01']}}
                    @endif
                    @if (!empty($outbOrdHdr['desc_02']))
                        <br/>{{$outbOrdHdr['desc_02']}}
                    @endif
                    </div>
                </td>                
                <td>
                    <div style="width:110pt;height:50pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$outbOrdHdr['sls_ord_hdr_code']}}
                    </div>
                </td>
                <td>
                    <div style="width:79pt;height:50pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{number_format($outbOrdHdr['net_amt'],2)}}
                    </div>
                </td>
            </tr>
            @endforeach
            <tr>
                <td colspan="2">
                    <div style="width:300pt;height:14pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">

                    </div>
                </td>
                <td>
                    <div style="width:50pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                    Total :
                    </div>
                </td>
                <td>
                    <div style="width:79pt;height:14pt;font-size:0.9em;font-weight:bold;text-align:right;overflow:hidden;">
                    {{number_format($docHdr->net_amt,2)}}
                    </div>
                </td>
            </tr>
        </table>
        @endif

        <!-- Signature -->
        @if ($pageNum == $dtlEndPageNum)
        <table style="table-layout:fixed;width:520pt;top:700pt;left:30pt;position:fixed;">
            <tr>
                <td>
                    <div style="width:125pt;height:30pt;font-size:0.9em;font-weight:bold;text-align:center;overflow:hidden;">
                    <hr />
                    <br />PICKER/DATE
                    </div>
                </td>
                <td>
                    <div style="width:125pt;height:30pt;font-size:0.9em;font-weight:bold;text-align:center;overflow:hidden;">
                    <hr />
                    <br />STOREKEEPER/DATE
                    </div>
                </td>
                <td>
                    <div style="width:125pt;height:30pt;font-size:0.9em;font-weight:bold;text-align:center;overflow:hidden;">
                    <hr />
                    <br />SUPERVISOR/DATE
                    </div>
                </td>
                <td>
                    <div style="width:125pt;height:30pt;font-size:0.9em;font-weight:bold;text-align:center;overflow:hidden;">
                    <hr />
                    <br />DELIVERY BY
                    </div>
                </td>
            </tr>
        </table>
        @endif

        <!-- pick face replenishment -->
        @if ($pageNum >= $pfStartPageNum && $pageNum <= $pfEndPageNum)
        <table class="table1" style="table-layout:fixed;width:550pt;top:165pt;left:20pt;position:fixed;">
            <tr>
                <th style="width:3%">
                #
                </th>
                <th style="width:15%;">  
                ITEM            
                </th>
                <th style="width:44%;">   
                DESCRIPTION         
                </th>
                <th style="width:9%;">
                EXPIRY
                </th>
                <th style="width:30%;">
                WAITING PICK LIST
                </th>
            </tr>
            <?php $startRowNum = (($pageNum - $pfStartPageNum) * $rowsPerPage); ?>
            <?php $endRowNum = (($pageNum - $pfStartPageNum) * $rowsPerPage) + $rowsPerPage; ?>
            @for ($rowNum = $startRowNum; $rowNum < ( $endRowNum < $ttlPfDetailsRows ? $endRowNum : $ttlPfDetailsRows ); $rowNum++)
            <tr>
                <td>
                    <div style="width:13pt;height:57pt;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$rowNum + 1}}
                    </div>
                </td>
                <td>
                    <div style="width:82pt;height:57pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$docHdr->pick_face_replenishments[$rowNum]['item_code']}}
                    @if (!empty($docHdr->pick_face_replenishments[$rowNum]['item_unit_barcode']))
                        <br/>{{$docHdr->pick_face_replenishments[$rowNum]['item_unit_barcode']}}
                    @endif
                    @if (!empty($docHdr->pick_face_replenishments[$rowNum]['item_case_barcode']))
                        <br/>{{$docHdr->pick_face_replenishments[$rowNum]['item_case_barcode']}}
                    @endif  
                    <br/>
                    </div>                
                </td>
                <td>
                    <div style="width:245pt;height:57pt;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$docHdr->pick_face_replenishments[$rowNum]['item_desc_01']}}
                    <br/>
                    {{$docHdr->pick_face_replenishments[$rowNum]['storage_bin_code']}}  =>  {{$docHdr->pick_face_replenishments[$rowNum]['to_storage_bin_code']}}
                    @if ($docHdr->pick_face_replenishments[$rowNum]['handling_unit_id'] > 0)
                    <br/>
                    {{$docHdr->pick_face_replenishments[$rowNum]['handling_unit_barcode']}}
                    <div style="padding-left:5pt;">
                    <?php echo \Milon\Barcode\DNS1D::getBarcodeHTML('ZY'.str_pad($docHdr->pick_face_replenishments[$rowNum]['handling_unit_id'], 5, '0', STR_PAD_LEFT), "C39"); ?>
                    </div>
                    @endif
                    </div>                
                </td>
                <td>
                    <div style="width:49pt;height:57pt;font-size:0.8em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$docHdr->pick_face_replenishments[$rowNum]['expiry_date']}}
                    @if (!empty($docHdr->pick_face_replenishments[$rowNum]['item_batch']))
                        <br/>{{$docHdr->pick_face_replenishments[$rowNum]['item_batch']}}
                    @endif
                    </div>
                </td>
                <td>
                    <div style="width:156pt;height:57pt;font-size:0.9em;font-weight:normal;overflow:hidden;">
                    {{$docHdr->pick_face_replenishments[$rowNum]['pf_pick_list_doc_codes']}}
                    </div>
                </td>
            </tr>
            @endfor
        </table>
        @endif

        <!-- Print By/Time -->
        <table style="top:150pt;left:450pt;position:fixed;">
            <tr>
                <td>
                    <div style="width:100pt;height:14pt;font-size:0.7em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$printedBy['username']}}   {{$printedAt->setTimezone(env('APP_TIMEZONE', 'UTC'))}}
                    </div>
                </td>
            </tr>
        </table>
    </div>

    @if ($isLastLoop && $pageNum == $ttlPageCount)
        {{-- last of the loop --}}
    @elseif ($isPageBreakAfter)
        <div style="page-break-after: always;"></div>
    @endif
@endfor
