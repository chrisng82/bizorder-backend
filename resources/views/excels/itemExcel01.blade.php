<table>
    <tbody>
        <tr>
            @foreach($heading as $label)
                <td style='background-color:#FFFF00'>
                    {{ $label }}
                </td>
            @endforeach
        </tr>
        @foreach($rows as $row)
        <tr>
            @foreach($row as $cell)
                <td>
                    {{ $cell['data'] }}
                </td>
            @endforeach
        </tr>
        @endforeach
    </tbody>
</table>