<table>
    <tbody>
        @foreach($rows as $row)
        <tr>
            @foreach($row as $cell)
                @if ($cell['attrs']['colspan'] > 1)
                <td 
                style="{{ $cell['attrs']['style'] }}"
                colspan="{{ $cell['attrs']['colspan'] }}" 
                rowspan="{{ $cell['attrs']['rowspan'] }}">
                    @if ($cell['attrs']['nodename'] == 'b')
                    <b>{{ $cell['data'] }}</b>
                    @elseif ($cell['attrs']['nodename'] == 'strong')
                    <strong>{{ $cell['data'] }}</strong>
                    @elseif ($cell['attrs']['nodename'] == 'i')
                    <i>{{ $cell['data'] }}</i>
                    @elseif ($cell['attrs']['nodename'] == 'div')
                    <div>{{ $cell['data'] }}</div>
                    @elseif ($cell['attrs']['nodename'] == 'h1')
                    <h1>{{ $cell['data'] }}</h1>
                    @elseif ($cell['attrs']['nodename'] == 'h2')
                    <h2>{{ $cell['data'] }}</h2>
                    @else
                    {{ $cell['data'] }}
                    @endif
                </td>
                @else
                <td 
                style="{{ $cell['attrs']['style'] }}">
                    @if ($cell['attrs']['nodename'] == 'b')
                    <b>{{ $cell['data'] }}</b>
                    @elseif ($cell['attrs']['nodename'] == 'strong')
                    <strong>{{ $cell['data'] }}</strong>
                    @elseif ($cell['attrs']['nodename'] == 'i')
                    <i>{{ $cell['data'] }}</i>
                    @elseif ($cell['attrs']['nodename'] == 'div')
                    <div>{{ $cell['data'] }}</div>
                    @elseif ($cell['attrs']['nodename'] == 'h1')
                    <h1>{{ $cell['data'] }}</h1>
                    @elseif ($cell['attrs']['nodename'] == 'h2')
                    <h2>{{ $cell['data'] }}</h2>
                    @else
                    {{ $cell['data'] }}
                    @endif
                </td>
                @endif
            @endforeach
        </tr>
        @endforeach
    </tbody>
</table>