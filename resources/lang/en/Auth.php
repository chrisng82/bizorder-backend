<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    //message
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login_successfully' => 'Login successful',
    'username_or_password_wrong' => 'Username or password wrong',
    'logout_successful' => 'Logout successful',
    'token_cannot_be_validated' => 'Token cannot be validated. Please reauthenticate',
    'token_expired' => 'Token expired',
    'token_invalid' => 'Token invalid',
    'token_absent' => 'Token absent',
    'or' => 'or',
    'permission_required' => ':names permission required',
    'your_current_password_is_incorrect' => 'Your current password is incorrect',
    'you_must_set_a_new_password' => 'You must set a new password',
    'password_expired' => 'Password expired',
    'your_password_has_been_changed_successfully' => 'Your password has been changed successfully'

    //label
];
