<?php

return [
    'mobile_profile_not_found' => 'Mobile profile [:mobileProfileId] not found',
    'mobile_schema_not_found' => 'Mobile Schema [:tableName] for [:mobileApp] not found',
    'mobile_history_not_found' => 'Mobile history [:tableName] record id [:recordId] for profile [:mobileProfileId] not found',
    'process_history_error' => 'Process mobile history error, :msg',
    'mobile_history_duplicate' => 'Mobile history [:recordId] is duplicate in a table [:tableName]',
    'operation_invalid' => 'Mobile history [:recordId] operation [:operation] is invalid, only accept "INSERT", "UPDATE", "DELETE"'
];
