<?php

return [
    //message
    'document_successfully_created' => ':docCode successfully created',
    'doc_status_is_complete' => 'Proof of Delivery [:docCode] status is COMPLETE',
    'doc_status_is_not_draft_or_wip' => 'Proof of Delivery [:docCode] status is not DRAFT or WIP',
    'doc_status_is_not_wip_or_complete' => 'Proof of Delivery [:docCode] status is not WIP or COMPLETE',
    'doc_status_is_not_complete' => 'Proof of Delivery [:docCode] status is not COMPLETE',
    'doc_status_is_not_wip' => 'Proof of Delivery [:docCode] status is not WIP',
    'doc_status_is_not_draft' => 'Proof of Delivery [:docCode] status is not DRAFT',
    'item_details_not_found' => ':docCode details not found',
    'fr_doc_is_not_complete' => 'This document [:docCode] is status is not COMPLETE, can not be used to create Proof of Delivery',
    'fr_doc_is_closed' => 'This document [:docCode] is closed, can not be used to create Proof of Delivery',
    'commit_to_wip_error' => ':docCode commit to WIP error',
    'commit_to_complete_error' => ':docCode commit to complete error',
];
