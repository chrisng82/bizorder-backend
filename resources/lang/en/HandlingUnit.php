<?php

return [
    //message
    'do_you_want_to_print_labels' => 'Do you want to print labels',
    'yes_print_the_pallets' => 'Yes, print the pallets',
    'pallets_successfully_created' => ':noOfPallets pallets successfully created',

    //label
    'pallets' => 'Pallets',
    'pallet_labels' => 'Pallet Labels',
    'barcode' => 'Barcode',
    'ref_code' => 'Ref Code',
    'created_at' => 'Created At',
    'print_count' => 'Print',
    'first_printed_at' => 'First Printed At',
    'last_printed_at' => 'Last Printed At',
    'generate_new_pallet_label' => 'Generate New Pallet Label',
    'no_of_pallets' => 'No of Pallets',
    'manage_pallet_labels' => 'Manage Pallet Labels',
    'print' => 'Print',
    'create' => 'Create'
];
