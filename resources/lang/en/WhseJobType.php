<?php

return [
    'pick_full_pallet_label' => 'Full Pallet',
    'pick_full_pallet_desc' => 'Full Pallet Picking',
    'pick_broken_pallet_label' => 'Broken Pallet',
    'pick_broken_pallet_desc' => 'Pallet Loose Picking',
    'pick_face_replenishment_label' => 'Repln.',
    'pick_face_replenishment_desc' => 'Replenishment',
    'waiting_replenishment_label' => 'Wait.',
    'waiting_replenishment_desc' => 'Waiting',
    'pick_full_case_label' => 'P/F Full Case',
    'pick_full_case_desc' => 'Full Case Picking',
    'pick_broken_case_label' => 'Loose',
    'pick_broken_case_desc' => 'Loose Picking',
    'pick_not_fulfilled_label' => 'No Stock',
    'pick_not_fulfilled_desc' => 'No Stock',
    'goods_receipt_pallet_label' => 'Receive Pallet',
    'goods_receipt_pallet_desc' => 'Receive Pallet',
    'goods_receipt_loose_label' => 'Receive Loose',
    'goods_receipt_loose_desc' => 'Receive Loose',
];
