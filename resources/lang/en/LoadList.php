<?php

return [
    //message
    'document_successfully_created' => ':docCode successfully created',
    'doc_status_is_complete' => 'Load List [:docCode] status is COMPLETE',
    'doc_status_is_not_draft_or_wip' => 'Load List [:docCode] status is not DRAFT or WIP',
    'doc_status_is_not_wip_or_complete' => 'Load List [:docCode] status is not WIP or COMPLETE',
    'doc_status_is_not_complete' => 'Load List [:docCode] status is not COMPLETE',
    'doc_status_is_not_wip' => 'Load List [:docCode] status is not WIP',
    'doc_status_is_not_draft' => 'Load List [:docCode] status is not DRAFT',
    'quant_bal_txn_not_found' => 'Load List [:docCode] detail id [:dtlId] quant transaction not found',
    'outb_ord_item_not_tally' => 'Load List [:docCode] item [:itemCode] quantity [:loadUnitQty] does not match the quantity defined in outbound orders [:ordUnitQty]',
    'fr_doc_is_not_complete' => 'This document [:docCode] is status is not COMPLETE, can not be used to create Load List',
    'fr_doc_is_closed' => 'This document [:docCode] is closed, can not be used to create Load List',
    'commit_to_wip_error' => ':docCode commit to WIP error',
    'commit_to_complete_error' => ':docCode commit to complete error',
    'item_details_not_found' => ':docCode details not found',

    //label
    'void' => 'VOID',
    'draft' => 'DRAFT',
    'wip' => 'WIP',
    'complete' => 'COMPLETE',
    'create' => 'Create',
    'back' => 'Back',
    'reset' => 'Reset',
    'update' => 'Update',
];
