<?php

return [
    //message
    'select_file_to_upload' => 'Please select a file to upload',
    'select_site_flow' => 'Please select a site flow',
    'select_division' => 'Please select a division',
    'username_is_required' => 'Username is required',
    'password_is_required' => 'Password is required',
    'current_password_is_required' => 'Current password is required',
    'new_password_is_required' => 'New password is required',
    'passwords_do_not_match' => 'Passwords do not match',
    
    //label
    'login_title' => 'LuuWu WMS Login',
    'site_flow' => 'Site Flow',
    'division' => 'Division',
    'submit' => 'Submit',
    'create' => 'Create',
    'delete' => 'Delete',
    'update' => 'Update',
    'username' => 'Username',
    'password' => 'Password',
    'remember_me' => 'Remember me',
    'current_password' => 'Current Password',
    'new_password' => 'New Password',
    'retype_new_password' => 'Retype New Password',
    'change_password' => 'Change Password',
    'logout' => 'Logout',
];
