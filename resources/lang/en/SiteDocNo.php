<?php

return [
    'doc_no_not_found' => '[:docType] Doc Number for site [:siteId] not found'
];
