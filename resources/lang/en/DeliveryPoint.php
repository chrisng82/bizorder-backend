<?php

return [
    //message
    'fail_to_sync' => 'Fail to sync with :url',
    'sync_setting_not_found' => 'Sync setting for site flow[:siteFlowId] not found',

    //label
    'debtor_code' => 'Debtor Code',
    'code' => 'Delivery Code',
    'ref_code' => 'Ref Code',
    'name' => 'Name',
    'debtor_group_01' => 'Category',
    'debtor_group_02' => 'Chain',
    'debtor_group_03' => 'Channel',
    'delivery_points' => 'Delivery Points',
    'import_export_excel' => 'Import/Export Excel',
    'sync_efichain' => 'Sync EfiChain',
    'url' => 'URL',
    'last_synced_at' => 'Last Synced At',
    'page_size' => 'Page Size',
    'username' => 'Username',
    'password' => 'Password',
    'save' => 'Save',
    'edit' => 'Edit',
    'sync_now' => 'Sync Now',
    'browse' => 'Browse',
    'upload' => 'Upload',
    'download' => 'Download',
];
