<?php

return [

    //message
    'resource_type_not_defined' => 'Resource type :resType not defined',

    //label
    'back' => 'Back',
    'audit_trail' => 'Audit Trail',
    'all' => 'ALL',
    'updated' => 'UPDATED',
    'created' => 'CREATED',
    'deleted' => 'DELETED',
    'date_time' => 'Date',
    'event' => 'Event',
    'type' => 'Type',
    'url' => 'URL',
    'ip_address' => 'IP Address',
    'user_agent' => 'User Agent',
    'tags' => 'Tags',
    'old_values' => 'Old Values',
    'new_values' => 'New Values',
    'field' => 'Field',
    'value' => 'Value'
];
