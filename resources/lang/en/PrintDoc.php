<?php

return [
    'report_template_not_found' => 'Document type [:docType] report template not found'
];
