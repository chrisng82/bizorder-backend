<?php

return [
    //message
    'file_successfully_uploaded' => 'File successfully uploaded',
    'do_you_want_to_remove_this_permission' => 'Do you want to remove this permission?',
    'yes_delete_it' => 'Yes, delete it!',
    'permissions_successfully_updated' => 'Permissions successfully updated',
    'permissions_successfully_deleted' => 'Permissions successfully deleted',
    'permission_is_required' => 'Permission is required',
    
    //label
    'roles' => 'Role(s)',
    'pemissions' => 'Pemission(s)',
    'import_export_excel' => 'Import/Export Excel',
    'create_role' => 'Create Role',
    'name' => 'Name',
    'description' => 'Description',
    'browse' => 'Browse',
    'upload' => 'Upload',
    'download' => 'Download',
    'add' => 'Add',

    'admin_clerk_desc_01' => 'Admin Clerk',
    'admin_manager_desc_01' => 'Admin Manager',
    'sales_clerk_desc_01' => 'Sales Clerk',
    'sales_manager_desc_01' => 'Sales Manager',
    'warehouse_operator_desc_01' => 'Warehouse Operator',
    'warehouse_clerk_desc_01' => 'Warehouse Clerk',
    'warehouse_manager_desc_01' => 'Warehouse Manager',
    'system_admin_desc_01' => 'System Administrator',
];
