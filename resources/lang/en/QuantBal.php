<?php

return [
    'balance_is_not_enough' => ':storageBinCode Item [:itemCode] :itemDesc01 The balance is insufficient, required = :reqUnitQty, after operation = :afBalanceUnitQty',
    'storage_bins_is_locked' => 'Storage Bins :storageBinCodes is locked by document :docCode :docDate',
];
