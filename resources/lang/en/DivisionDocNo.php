<?php

return [
    'doc_no_not_found' => '[:docType] Doc Number for division [:divisionId] not found'
];
