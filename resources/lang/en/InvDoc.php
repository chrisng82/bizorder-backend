<?php

return [
    //message
    'document_successfully_created' => ':docCode successfully created',
    'do_you_want_to_remove_this_document' => 'Do you want to remove this document?',
    'yes_delete_it' => 'Yes, delete it!',

    'status_is_not_complete' => 'status is not COMPLETE',
    'status_is_not_wip' => 'status is not WIP',

    //label
    'create_inv_doc_01_01' => 'Print Shipping Documents',
    'create_inv_doc_02' => 'Create Return Receipt',
    'sales_orders' => 'Sales Order(s)',
    'pick_lists' => 'Pick List(s)',
    'goods_receipts' => 'Goods Receipt(s)',
    'area' => 'Area',
    'salesman' => 'Salesman',
    'debtor' => 'Debtor',
    'division' => 'Division',
    'doc_code' => 'Doc Code',
    'status' => 'Status',
    'doc_date' => 'Doc Date',
    'est_del_date' => 'Est. Del. Date',
    'desc' => 'Description',
    'ref_code' => 'Ref Code',
    'pick_qty' => 'Pick Qty',
    'order_qty' => 'Order Qty',
    'total' => 'Total',
    'new_documents' => 'New Invoice(s)/Delivery Order(s)',
    'new_return_receipts' => 'New Return Receipt(s)',
    'create' => 'Create',
    'create_inv_doc_01' => 'Create Invoice/DO from Pick List',
    'print' => 'Print',
    'documents' => 'Document(s)',
    'net_amt' => 'Net Amt',
    'case_qty' => 'Case',
    'qty' => 'Qty',
    'gross_weight' => 'kg',
    'cubic_meter' => 'm3',
];
