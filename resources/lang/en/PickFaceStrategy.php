<?php

return [
    'file_successfully_uploaded' => 'File successfully uploaded',
    'item_is_required' => 'Line No [:lineNo] item is not assigned',
    'storage_bin_is_required' => 'Line No [:lineNo] storage bin is not assigned',
    'storage_bin_is_not_pick_face' => 'Line No [:lineNo] storage bin is not pick face',

    //label
    'pick_face_strategies' => 'Pick Face Strategy(s)',
    'status' => 'Status',
    'import_export_excel' => 'Import/Export Excel',
    'browse' => 'Browse',
    'upload' => 'Upload',
    'download' => 'Download',
    'line_no' => '#',
    'item_code' => 'Item Code',
    'item_desc_01' => 'Desc 1',
    'item_desc_02' => 'Desc 2',
    'storage_bin_code' => 'Bin Code',
];
