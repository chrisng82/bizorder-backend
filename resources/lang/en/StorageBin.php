<?php

return [
    'id_not_found' => 'Storage bin [:id] not found.',
    'file_successfully_uploaded' => 'File successfully uploaded',

    //label
    'warehouse_map' => 'Warehouse Map',
    'warehouse_setup' => 'Warehouse Setup',
    'storage_bins' => 'Storage Bins',
    'row' => 'Row',
    'bay' => 'Bay',
    'level' => 'Level',
    'level_abbreviation' => 'L',
    'code' => 'Code',
    'description' => 'Description',
    'type' => 'Type',
    'status' => 'Status',
    'import_export_excel' => 'Import/Export Excel',
    'browse' => 'Browse',
    'upload' => 'Upload',
    'download' => 'Download',
    'last_count_date' => 'Last Count Date',
    'item' => 'Item',
    'item_batch' => 'Batch',
    'pallet_id' => 'Pallet ID',
    'case' => 'Case',
    'loose' => 'Loose',
    'gross_weight' => 'kg',
    'cubic_meter' => 'm3',
    'total' => 'Total'
];
